package com.nviz.tests;

import static org.mockito.Mockito.when;

import java.io.File;
import java.io.PrintWriter;

import junit.framework.TestCase;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import atg.core.io.FileUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

import com.nviz.droplet.WelcomeDroplet;



public class WelcomeDropletTest extends TestCase {
	@Mock private DynamoHttpServletRequest request;
	@Mock private DynamoHttpServletResponse response;
	
	public WelcomeDropletTest(){
		MockitoAnnotations.initMocks(this);
	}
		
	

	public void testGreetingMessage() throws Exception{
		String expectedResult ="Welcome to Nvizion Reference Store";
		String greeting ="Welcome to Nvizion Reference Store";
		final PrintWriter pWriter = new PrintWriter("c:\\temp\\printwriter.txt");
		
		final WelcomeDroplet wdInstance = new WelcomeDroplet();
		wdInstance.setGreetings(greeting);
		when(request.serviceLocalParameter("output", request, response)).thenAnswer(new Answer<Boolean>() {
		     public Boolean answer(InvocationOnMock invocation) throws Throwable {
		    	 pWriter.write(wdInstance.getGreetings());
		    	 pWriter.flush();
		    	 return true;
		    	 
		     }
		});
		
		wdInstance.service(request, response);
		assertTrue(FileUtils.readFileString(new File("c:\\temp\\printwriter.txt"), "UTF-8")
                 .equalsIgnoreCase(expectedResult));
	}
}
