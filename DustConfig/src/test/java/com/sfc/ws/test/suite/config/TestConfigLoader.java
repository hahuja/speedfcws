package com.sfc.ws.test.suite.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.Vector;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import atg.nucleus.Nucleus;
import atg.nucleus.NucleusTestUtils;

/**
 * This is a singleton class to initialize ATG Nucleus globally.
 * 
 * @author Himkar
 * 
 */
public class TestConfigLoader {
	/**
	 * This variable used to hold Logger instance
	 */
	private static Logger mLogger = Logger.getLogger(TestConfigLoader.class);

	/**
	 * This variable used to hold Nucleus instance
	 */
	private static Nucleus mNucleus = null;

	private static Properties mProperties = null;
	
	private static Vector<String> runningTestServiceIds;

	/**
	 * This private constructor prevent users not to instantiate this class
	 * directly
	 */
	private TestConfigLoader() {

	}

	/**
	 * This method is used to retrive nucleus instance, if nucleus has been
	 * instantiated, will return same otherwise it will create new and return to
	 * caller
	 * 
	 * @return Nucleus - this is ATG Nucleus instance to resolve all the
	 *         components
	 */
	private static Nucleus getNucleusInstance() {
		if (mNucleus == null) {
			mLogger.log(Level.DEBUG, "Nucleus Starting...");
			try {
				String modules = TestConfigLoader
						.getPropertyValue("startup-modules");
				String[] moduleList = modules.split(",");
				mNucleus = NucleusTestUtils.startNucleusWithModules(moduleList,
						NucleusTestUtils.class, null,
						TestConfigLoader.getPropertyValue("initial-component"));
				mLogger.log(Level.DEBUG, "Nucleus Started");
			} catch (Exception e) {
				mLogger.log(Level.DEBUG, "Error occured..." + e.getMessage());
				mLogger.error(e.getMessage());
			}
		}

		return mNucleus;
	}
	/**
	 * this method is used to retrieve system property value
	 * @param propertyKey - property key
	 * @return property value
	 */
	public static String getPropertyValue(String propertyKey) {
		if (mProperties == null) {
			mProperties = System.getProperties();
		}
		String propertyValue = mProperties.getProperty(propertyKey);

		return propertyValue;

	}

	/**
	 * This method is used to shutdown Nucleus
	 * 
	 * @return boolean - return ture if it shutdown properly
	 */
	public static boolean shutdownNucleus() {
		boolean flag = false;
		try {
			if(TestConfigLoader.isNucleusReadyForShutdown()){
				mLogger.log(Level.DEBUG, "Nucleus shutdown process started...");
				NucleusTestUtils.shutdownNucleus(TestConfigLoader.mNucleus);
				flag = true;
				mLogger.log(Level.DEBUG, "Nucleus shutdown...");
			}
		} catch (Exception e) {
			mLogger.log(Level.DEBUG, "Error occured..." + e.getMessage());
			e.printStackTrace();
		}
		return flag;
	}

	/**
	 * This method is used to resolve requested component from ATG Nucleus
	 * 
	 * @return Object - instance of the component
	 */
	public static Object resolveComponent(String componentPath) {
		return TestConfigLoader.getNucleusInstance().resolveName(componentPath);
	}
	/**
	 * This method is used to set required system properties for the testing framework
	 */
	public static void setDefaultSystemProperties() {
		Properties sysProps = System.getProperties();
		Properties localProps = new Properties();

		try {
			// load a properties file
			localProps.load(new FileInputStream("C:/NvizProjects/NOVA/NvizionReferenceStore/ModuleBuilder/build-scripts/junit.properties"));
			
			sysProps.setProperty("atg.dynamo.root", localProps.getProperty("junit-dynamo-root"));
			String dustHome=localProps.getProperty("junit-dynamo-root")+"/"+localProps.getProperty("junit-project-path")+"/"+localProps.getProperty("junit-dust-module-name");
			sysProps.setProperty("atg.test.data.directory", dustHome + "/testconfig/data");			
			sysProps.setProperty("configPath", dustHome + "/testconfig");
			sysProps.setProperty("derby.locks.deadlockTrace", localProps.getProperty("derby.locks.deadlockTrace"));
			sysProps.setProperty("DUST_HOME", dustHome);
			sysProps.setProperty("initial-component", localProps.getProperty("initial-component"));
			sysProps.setProperty("service-ids", localProps.getProperty("service-ids"));
			sysProps.setProperty("startup-modules", localProps.getProperty("startup-modules"));
			

		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
	
	/**
	 * Check whether the nucleus is ideal and ready for shutdown
	 * @return true if Nucleus is ready for shutdown
	 */
	public static boolean isNucleusReadyForShutdown(){
		boolean flag = true;
		if(runningTestServiceIds != null && runningTestServiceIds.size() > 0){
			flag = false;
		}		
		return flag;
	}
	/**
	 * Notify nucleus about the test service has been initiated
	 * @param serviceName
	 */
	public static void addTestService(String serviceName){
		if(runningTestServiceIds == null){
			runningTestServiceIds = new Vector<String>();
		}
		runningTestServiceIds.add(serviceName);
	}
	/**
	 * Notify nucleus about the test service has finished it's task
	 * @param serviceName
	 */
	public static void removeTestService(String serviceName){
		if(runningTestServiceIds != null && runningTestServiceIds.size() > 0){
			runningTestServiceIds.remove(serviceName);
		}
		runningTestServiceIds.remove(serviceName);
	}
}
