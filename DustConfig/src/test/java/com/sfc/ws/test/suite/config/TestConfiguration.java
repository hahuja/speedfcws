package com.sfc.ws.test.suite.config;

import java.util.ArrayList;
import java.util.List;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;



/**
 * This class is used to configure all the pre-requisite configuration which are used by junit framework.
 * We load this class component via ATG Nucleus, and tells test suite to create test cases based on the
 * the map value which has been configured here.
 * 
 * @author Himkar
 *
 */

public class TestConfiguration extends GenericService {

	/**
	 * this variable keeps the list of services and their configuration xml path.
	 */
	private List<String> appliedServiceIds;

	/**
	 * This method is used to load all the service Ids from junit.properties file
	 */
	private void loadServiceIds(){
		String serviceIds = TestConfigLoader.getPropertyValue("service-ids");		
		if(!StringUtils.isBlank(serviceIds)){
			String[] ids = serviceIds.split(",");
			for(String id: ids){
				this.getAppliedServiceIds().add(id);
			}
		}
	}
	/**
	 * This method is used to get all the service ids for which the test cases need to be created
	 * @return the appliedServiceIds
	 */
	public List<String> getAppliedServiceIds() {
		if(appliedServiceIds == null){
			this.setAppliedServiceIds(new ArrayList<String>());
			this.loadServiceIds();
		}
		return appliedServiceIds;
	}

	/**
	 * This method is used to set all the service ids for which the test cases need to be created
	 * @param appliedServiceIds the appliedServiceIds to set
	 */
	public void setAppliedServiceIds(List<String> appliedServiceIds) {
		this.appliedServiceIds = appliedServiceIds;
	}
	/**
	 * This method is used to create absolute path for test case xml file 
	 * @param appliedServiceIds - service id
	 * @param sufix
	 * @return
	 */
	private String createXMLPath(String appliedServiceIds, String sufix){
		String path = TestConfigLoader.getPropertyValue("configPath").concat("/xmls/").concat(appliedServiceIds).concat("/").concat(appliedServiceIds).concat("_").concat(sufix).concat(".xml");
		
		return path;
	}
	/**
	 * This method is used to create absolute path for test case request xml file list
	 * 
	 * @param appliedServiceIds - service id
	 * @param operationName  - operation name
	 * @param requestCount - how many request parameter this operation required
	 * @return - list of request file path
	 */
	public String[] createRequestXMLPath(String appliedServiceIds, String operationName, String requestCount){
		int fileCount = 0;
		String[] files = null;
		if(this.isLoggingDebug()){
			this.logDebug("TestConfiguration : createRequestXMLPath : Start");
		}
		try{
			/**
			 * Check if request count is not null or blank
			 */
			if(!StringUtils.isBlank(requestCount)){
				fileCount = Integer.parseInt(requestCount);
				files = new String[fileCount];
				if(fileCount == 0){
					
				}else if(fileCount == 1){
					if(this.isLoggingDebug()){
						this.logDebug("TestConfiguration : createRequestXMLPath : Request found for single parameter");
					}
					files[0] = this.createXMLPath(appliedServiceIds, operationName+"_Request"); 
				}else{
					if(this.isLoggingDebug()){
						this.logDebug("TestConfiguration : createRequestXMLPath : Request found for multi parameter");
					}
					for(int counter = 0; counter < fileCount; counter++){					
						files[counter] = this.createXMLPath(appliedServiceIds, operationName+"_Request"+(counter+1)); 					
					}
				}
			}						
		}catch(NumberFormatException numberFormatException){
			if(this.isLoggingDebug()){
				this.logDebug("TestConfiguration : createRequestXMLPath : error occured while creating request : "+numberFormatException.getMessage());
			}
			throw numberFormatException;
		}
		if(this.isLoggingDebug()){
			this.logDebug("TestConfiguration : createRequestXMLPath : End "+files);
		}
		return files;
	}
	/**
	 * This method is used to create absolute path for test case response xml file
	 * 
	 * @param appliedServiceIds - service id
	 * @param operationName - operation name
	 * @return String absolute file path
	 */
	public String createResponseXMLPath(String appliedServiceIds, String operationName){		
		return this.createXMLPath(appliedServiceIds, operationName+"_Response");
	}
	/**
	 * This method is used to create absolute path for test case configuration xml file
	 * 
	 * @param appliedServiceIds - service id
	 * @return String absolute file path
	 */
	public String createResourceXMLPath(String appliedServiceIds){
		return "/".concat(this.createXMLPath(appliedServiceIds, "Resource"));
	}
	
}
