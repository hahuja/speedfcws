


--  @author Doug Kenyon 
--  @version $Id: //application/service/version/10.1/common/src/sql/solution_logical_organization_ddl.xml#1 $$Change: 683854 $ 
--  @updated $DateTime: 2012/01/09 09:43:34 $$Author: jsiddaga $ 
--       Profile groups (segments) assigned to a solution logical organization (owning group and internal audience).      These segments are used to show/hide particular values from an author when assigning them to the solution      owningGroup and internalAudience intrinsic properties.    

create table svc_solnorg_seg (
	segment_name	varchar2(255)	not null,
	logical_org_id	varchar2(40)	not null
,constraint svcsolnorgseg_p primary key (segment_name,logical_org_id)
,constraint svcsolnorgseg_fk1 foreign key (logical_org_id) references dlo_logical_org (logical_org_id));

create index svcsolnorgseg_idx1 on svc_solnorg_seg (logical_org_id);



