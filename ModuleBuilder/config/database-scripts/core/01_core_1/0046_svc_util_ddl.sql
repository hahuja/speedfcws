


--     Table: svc_spell_dics, Repository Item: SpellingDictionary    Table Space Type: STATIC  

create table svc_spell_dics (
	id	varchar2(40)	not null,
	language	varchar2(40)	not null
,constraint svc_spell_dics_p primary key (id));

--     Table: svc_spell_words, Repository Item: SpellingDictionary.words    Table Space Type: STATIC  

create table svc_spell_words (
	dic_id	varchar2(40)	not null,
	word	varchar2(255)	not null
,constraint svc_spell_words_p primary key (dic_id,word)
,constraint svc_spelldics_fk1 foreign key (dic_id) references svc_spell_dics (id));




