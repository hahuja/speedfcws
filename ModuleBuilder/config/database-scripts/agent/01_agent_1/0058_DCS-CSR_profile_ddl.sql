


--  @version $Id: //application/DCS-CSR/version/10.1/src/sql/DCS-CSR_profile_ddl.xml#1 $$Change: 683854 $

create table csr_agent_app_limit (
	agent_id	varchar2(40)	not null,
	currency	varchar2(3)	not null,
	app_limit	number(19,7)	null
,constraint app_limit_p primary key (agent_id,currency)
,constraint app_limit_f foreign key (agent_id) references dpi_user (id));




