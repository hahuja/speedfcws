


--     Table: svc_config_objct, Repository Item: ConfigurationObject    Table Space Type: STATIC    The shared configuration object description.  

create table svc_config_objct (
	config_object_id	varchar2(40)	not null,
	config_type	number(10)	not null,
	user_id	varchar2(40)	not null,
	enabled_yn	number(1)	not null
,constraint svccfgobj_p primary key (config_object_id));

--     Table: svc_framewrk_cfg, Repository Item: FrameworkConfig    Table Space Type: STATIC    The framework configuration per user.  

create table svc_framewrk_cfg (
	frmwk_cfg_id	varchar2(40)	not null,
	framework_id	varchar2(80)	not null,
	current_tab_id	varchar2(80)	null
,constraint svcfwcfg_p primary key (frmwk_cfg_id));

create index svcfwcfg1_ix on svc_framewrk_cfg (framework_id);
--     Table: svc_content_cfg, Repository Item: ContentConfig    Table Space Type: STATIC    The content configuration per user.  

create table svc_content_cfg (
	content_cfg_id	varchar2(40)	not null,
	content_id	varchar2(80)	not null,
	mime_type	varchar2(80)	not null,
	body_text	clob	null,
	url	varchar2(1024)	null,
	url_yn	number(1)	not null,
	other_context	varchar2(128)	null
,constraint svc_cntntcfg_p primary key (content_cfg_id));

create index svccntcfg1_ix on svc_content_cfg (content_id);
--     Table: svc_template_cfg, Repository Item: TemplateConfig    Table Space Type: STATIC    The template configuration per user.  

create table svc_template_cfg (
	template_cfg_id	varchar2(40)	not null,
	template_id	varchar2(80)	not null,
	url	varchar2(1024)	null,
	other_context	varchar2(128)	null
,constraint svc_templt_cfg_p primary key (template_cfg_id));

create index svctmplcfg1_ix on svc_template_cfg (template_id);
--     Table: svc_skin_cfg, Repository Item: SkinConfig    Table Space Type: STATIC    The skin configuration per user.  

create table svc_skin_cfg (
	skin_cfg_id	varchar2(40)	not null,
	skin_id	varchar2(80)	not null
,constraint svc_skin_cfg_p primary key (skin_cfg_id));

create index svcskncfg1_ix on svc_skin_cfg (skin_id);
--     Table: svc_tab_cfg, Repository Item: TabConfig    Table Space Type: STATIC    The tab configuration per user.  

create table svc_tab_cfg (
	tab_cfg_id	varchar2(40)	not null,
	tab_id	varchar2(80)	not null,
	visible_yn	number(1)	not null
,constraint svc_tab_cfg_p primary key (tab_cfg_id));

create index svctabcfg1_ix on svc_tab_cfg (tab_id);
--     Table: svc_cell_cfg, Repository Item: CellConfig    Table Space Type: STATIC    The column configuration per user.  

create table svc_cell_cfg (
	cell_cfg_id	varchar2(40)	not null,
	cell_id	varchar2(80)	not null,
	cell_open_yn	number(1)	not null
,constraint svc_cel_cfg_p primary key (cell_cfg_id));

create index svccelcfg1_ix on svc_cell_cfg (cell_id);
--     Table: svc_pstack_cfg, Repository Item: PanelStackConfig    Table Space Type: STATIC    The panel stack configuration per user.  

create table svc_pstack_cfg (
	pstack_cfg_id	varchar2(40)	not null,
	panel_stack_id	varchar2(80)	not null
,constraint svcpstackcfg_p primary key (pstack_cfg_id));

create index svcpstackcfg1_ix on svc_pstack_cfg (panel_stack_id);
--     Table: svc_panel_cfg, Repository Item: PanelConfig    Table Space Type: STATIC    The panel configuration per user.  

create table svc_panel_cfg (
	panel_cfg_id	varchar2(40)	not null,
	panel_id	varchar2(80)	not null,
	visible_yn	number(1)	not null,
	panel_open_yn	number(1)	not null,
	available_yn	number(1)	not null,
	content_open_yn	number(1)	not null,
	tabbed_yn	number(1)	not null,
	current_panel_id	varchar2(80)	null,
	tab_scroll_index	number(10)	null
,constraint svcpanelcfg_p primary key (panel_cfg_id));

create index svcpanelcfg1_ix on svc_panel_cfg (panel_id);
--     Link Table: svc_fw_tab_cfg, Repository Item: FrameworkConfig.tabIds    Table Space Type: STATIC    The tabs for a given framework in order per user.  

create table svc_fw_tab_cfg (
	frmwk_cfg_id	varchar2(40)	not null,
	tab_id	varchar2(80)	not null,
	sequence_num	number(10)	not null
,constraint svcfwtabcfg_p primary key (frmwk_cfg_id,sequence_num));

--     Link Table: svc_ps_pnl_cfg, Repository Item: PanelStackConfig.panelIds    Table Space Type: STATIC    The panels for a given panel stack in order per user.  

create table svc_ps_pnl_cfg (
	pstack_cfg_id	varchar2(40)	not null,
	panel_id	varchar2(80)	not null,
	sequence_num	number(10)	not null
,constraint svcpspnlcfg_p primary key (pstack_cfg_id,sequence_num));

--     Link Table: svc_tab_pnl_cfg, Repository Item: PanelConfig.tabbedPanelIds    Table Space Type: STATIC    The tabbed panels for a given panel per user.  

create table svc_tab_pnl_cfg (
	panel_cfg_id	varchar2(40)	not null,
	tabbed_panel_id	varchar2(80)	not null,
	sequence_num	number(10)	not null
,constraint svctbpnlcfg_p primary key (panel_cfg_id,sequence_num));




