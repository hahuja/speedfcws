


--     @version $Id: //application/service/version/10.1/common-pub/src/sql/svc_publishing_ddl.xml#1 $$Change: 683854 $  
--     Service Solution TaskInfo  

create table svc_soln_taskinfo (
	id	varchar2(40)	not null,
	soln_id	varchar2(40)	not null,
	soln_version	number(19)	not null,
	soln_class_version	number(19)	null,
	soln_title	varchar2(2048)	null,
	soln_stitle	varchar2(40)	null,
	soln_owninggrp_id	varchar2(40)	not null,
	soln_org_id	varchar2(40)	not null,
	soln_org_name	varchar2(254)	not null,
	soln_status_id	varchar2(40)	not null,
	soln_status_name	varchar2(255)	not null,
	task_id	varchar2(40)	not null,
	ws_name	varchar2(40)	not null,
	date_start	timestamp	not null,
	date_due	date	null
,constraint svcsolntskinfo_p primary key (id)
,constraint svcsolntskinfo_su unique (soln_id,soln_version));

create index svcsoltiorgid_ix on svc_soln_taskinfo (soln_org_id);
create index svcsoltistsid_ix on svc_soln_taskinfo (soln_status_id);
create index svcsoltistitle_ix on svc_soln_taskinfo (soln_stitle);
create index svcsolti_tid_ix on svc_soln_taskinfo (task_id);



