


--  @version $Id: //application/DCS-CSR/version/10.1/src/sql/DCS-CSR_approvals_ddl.xml#1 $$Change: 683854 $

create table csr_approval (
	approval_id	varchar2(40)	not null,
	ticket_id	varchar2(40)	not null,
	agent_id	varchar2(40)	not null,
	approver_id	varchar2(40)	null,
	type	number(10)	not null,
	approval_state	number(10)	not null,
	site_id	varchar2(40)	null,
	customer_id	varchar2(40)	null,
	creation_date	timestamp	not null,
	completion_date	timestamp	null
,constraint csr_approval_p primary key (approval_id));


create table csr_order_approval (
	approval_id	varchar2(40)	not null,
	order_id	varchar2(40)	not null,
	customer_email	varchar2(40)	null,
	appeasement_total	number(19,7)	not null,
	order_total	number(19,7)	not null
,constraint order_appr_p primary key (approval_id)
,constraint csrorderappr_f foreign key (approval_id) references csr_approval (approval_id));




