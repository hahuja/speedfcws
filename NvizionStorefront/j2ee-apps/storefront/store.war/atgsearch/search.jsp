  <%-- 
  This page configures and then executes a search based on the supplied parameters.
       
  NB.  This page must be executed prior to any other code which is dependent on the latest search
  results, and it must be executed before any other code which may result in the Response object 
  being marked as committed, as defined by the servlet spec, so that the Search form handler can
  redirect or forward if it wishes.
         
  Required Parameters:
    (Either)
    q
      The search question.  (Required when doing a query search)
    (Or)
    categoryId
      When doing a category search, specifies the category under which to search. (Required when 
      doing a category search).
    
  Optional Parameters:
    p
      The page of search results to render.
    sort
      How the results are sorted.
    sId
      The ID of a site whose content should also be included in the search.  There may be multiple
      parameters with this name, which indicates multiple sites should be searched.
    pageSize
      The number of results to display per page.
    facetTrail
      Records the currently supplied faceting on the search results.
    addFacet
      Specifies a new facet which should be applied, and added to the facet trail.
    token
      This specifies a request chain token.  This is used by ATG Search, allowing it to access a 
      previous search's results.  This is used for paging so that a new search does not need to be
      executed each time the user switches pages on the same set of search results.
    viewAll
      Indicates all results should be displayed on the page
  --%>
<dsp:page>
  <dsp:importbean bean="/atg/commerce/search/catalog/QueryFormHandler"/>
  <dsp:importbean bean="/atg/search/repository/FacetSearchTools"/>
  <dsp:importbean bean="/atg/commerce/catalog/ProductLookup"/> 
  <dsp:importbean bean="/atg/commerce/catalog/CategoryLookup"/>
  <dsp:importbean bean="/atg/targeting/TargetingFirst"/>
  <dsp:importbean bean="/atg/commerce/search/StoreBrowseConstraints"/>
  <dsp:importbean bean="/atg/commerce/search/refinement/CommerceFacetTrailDroplet"/>
  <dsp:importbean bean="/atg/multisite/Site"/>
  <dsp:importbean bean="/atg/store/profile/SessionBean"/>

  <dsp:getvalueof var="query" param = "q" />
  <dsp:getvalueof var="sort" param = "sort" />
  <dsp:getvalueof var="pageSize" param = "pageSize" />
  <dsp:getvalueof var="pageNum"  param = "p" />
  <dsp:getvalueof var="facetTrail" param="q_facetTrail" />
  <dsp:getvalueof var="addFacet" param="addFacet" />
  <dsp:getvalueof var="categoryId" param="categoryId" />
  <dsp:getvalueof var="viewAll" param="viewAll" />
  <dsp:getvalueof var="token" param="token"/>
  <dsp:getvalueof var="siteIDs" value="${paramValues.sid}"/>
    
  <c:set var="isCategorySearch" value="${! empty categoryId}" />

  <%-- if no facet trail is specified, let's build one for the category --%>
  <c:if test="${isCategorySearch && empty addFacet && empty facetTrail}">
    <%-- 
      TargetingFirst is used to perform a targeting operation with 
      the help of its targeter. We pick the first item from the array
      returned by the targeting operation. We use this to retrieve ID
      of the global category refineElement.
                
      Input Parameters:
        targeter - Specifies the targeter service that will perform
                             the targeting
                             
        howMany - the maximum number of target items to display
                  
      Open Parameters:
        empty - No targets were found
                  
        output - At least 1 target was found
                 
      Output Parameters:
        element - the result of a target operation
    --%>
    <dsp:droplet name="TargetingFirst">
      <dsp:param name="howMany" value="1" />
      <dsp:param name="targeter" 
                 bean="/atg/registry/RepositoryTargeters/RefinementRepository/GlobalCategoryFacet" />
      <%-- We have a refinement. --%>
      <dsp:oparam name="output">
        <dsp:getvalueof id="refElemRepId" idtype="String" param="element.repositoryId" />
        <c:set var="topLevelCategoryFacet" value="${refElemRepId}:${categoryId}"/>
      </dsp:oparam>
    </dsp:droplet>
  
    <c:set var="facetTrail" value="${topLevelCategoryFacet}" />        
  </c:if>
  
  <dsp:getvalueof var="pageSize" vartype="java.lang.Object" bean="Site.defaultPageSize"/>

  <c:if test="${empty pageSize}">
    <c:set var="pageSize" value="12"/>
  </c:if>
  
  <c:if test="${!empty query || isCategorySearch}">
    <%-- Execute query using 'q' URL parameter as question --%>
    <dsp:setvalue bean="QueryFormHandler.searchRequest.question" value="${query}"/>
    <dsp:setvalue bean="QueryFormHandler.sortSelection" value="${sort}"/>   
    <dsp:setvalue bean="QueryFormHandler.searchRequest.pageSize" value="${pageSize}"/>
    <dsp:setvalue bean="QueryFormHandler.searchRequest.facetSearchRequest" value="${true}"/>
    <dsp:setvalue bean="QueryFormHandler.searchRequest.documentSetConstraints" beanvalue="StoreBrowseConstraints" valueishtml="true"/>
    <dsp:setvalue bean="QueryFormHandler.searchRequest.multiSearchSession" value="true"/>
    <dsp:setvalue bean="QueryFormHandler.searchRequest.saveRequest" value="true"/>
    <dsp:setvalue bean="QueryFormHandler.searchRequest.dynamicTargetSpecifier.siteIdsArray" value="${siteIDs}"/>
       
    <%--
      Generates a FacetTrail bean based on the input parameters
     
      Input Parameters:
        trail - The facet trail string to be converted to a bean
        refineConfig - the RefineConfig to use for this particular category
      
      Open Parameters:
        output - Serviced when no errors occur
        error  - Serviced when errors occur
      
      Output Parameters:
        facetTrail - The generated FacetTrail bean.  
    --%> 
    <dsp:droplet name="CommerceFacetTrailDroplet">
      <dsp:param name="trail" value="${facetTrail}" />
      <dsp:param name="refineConfig" param="element.refineConfig" />
      <dsp:oparam name="error">
        <fmt:message key="common.facetSrchErrorMessage"/>
      </dsp:oparam>
      <dsp:oparam name="output">
        <dsp:getvalueof var="facetTrail" param="facetTrail" vartype="java.lang.Object"/>
        <c:choose>
          <c:when test="${not empty facetTrail}">
            <dsp:setvalue bean="FacetSearchTools.facetTrail" value="${facetTrail.trailString}"/>
          </c:when>
          <c:otherwise>
            <dsp:setvalue bean="FacetSearchTools.facetTrail" value="${addFacet}" />   
          </c:otherwise>
        </c:choose>              
      </dsp:oparam>
    </dsp:droplet>
    
    <%-- 
      An empty page number indicates that a new search should be executed.  When switching pages, 
      the desired page number will be present in the 'p' parameter and we just use that to call
      goToPage.  In this case we are dependent on the token parameter being available.  If it is 
      not, goToPage will trigger a new search rather than using an old one identified by token. 
    --%>
    <c:choose>
      <c:when test="${not empty pageNum && not empty token }">
        <dsp:setvalue bean="QueryFormHandler.searchRequest.requestChainToken" value="${token}"/>
        <dsp:setvalue bean="QueryFormHandler.goToPage" value="${pageNum}" /> 
      </c:when>
      <c:otherwise>
        <%-- 
          Incase we want to goto a page other than 1 for a new search, this pageNum must be zero
          indexed because we aren't using goToPage. goToPage handles zero indexing
        --%>
        <c:if test="${not empty pageNum}">
          <c:if test="${pageNum >= 1 }">
            <fmt:formatNumber value="${pageNum-1}" type="number" var="pageNum"/>
          </c:if>
          <dsp:setvalue bean="QueryFormHandler.searchRequest.pageNum" value="${pageNum}"/>
        </c:if>
        <dsp:setvalue bean="QueryFormHandler.search" value="" />
        <c:if test="${empty viewAll || viewAll eq 'false'}">
          <dsp:getvalueof var="token" bean="QueryFormHandler.searchResponse.requestChainToken" />
        </c:if>     
      </c:otherwise>
    </c:choose>
  </c:if>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/atgsearch/search.jsp#1 $$Change: 683854 $--%>
