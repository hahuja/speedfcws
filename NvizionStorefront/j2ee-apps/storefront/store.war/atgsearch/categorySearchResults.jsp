<%-- 
  This page contains all the elements of a category search results page.  The passed in parameters
  are not examined by this page, instead, they are inherited by the included pages.  For their usage
  within those pages, view the comments on the included pages.
       
  - atgSearchResultsRenderer displays the actual search results, along with sorting and pagination 
    options.
  - facetPanelContainer displays the faceting panel.
  - targetingRandom displays the promotional content.   
         
  Required Parameters:
    category
      This specifies the category under which to search.
      
  Optional Parameters:
    p
      The page of search results to render.
    sort
      How the results should be sorted sorted.
    sId
      The ID of a site whose content should also be included in the search.  There may be multiple
      parameters with this name, which indicates multiple sites should be searched.
    q_facetTrail
      Records the currently supplied faceting on the search results.
    addFacet
      Specifies a new facet which should be applied, and added to the facet trail.  
    includeRecommendations 
      Specifies if recommendation should be displayed.
--%> 
<dsp:page>
  <dsp:importbean bean="/atg/commerce/catalog/CategoryLookup" />
  <dsp:importbean bean="/atg/search/repository/FacetSearchTools"/>
  <dsp:importbean bean="/atg/store/StoreConfiguration" />
  <dsp:importbean bean="/atg/targeting/TargetingFirst" />

  <dsp:getvalueof var="trailSizeVar" param="trailSize" />
  <dsp:getvalueof var="facetTrail" param="q_facetTrail" />
  <dsp:getvalueof var="addFacet" param="addFacet" />
  <dsp:getvalueof var="category" param="category" />
  <dsp:getvalueof var="categoryId" param="category.id" />
  <dsp:getvalueof var="pageNum" param="p" />
  <dsp:getvalueof var="sort" param="sort" />
  <dsp:getvalueof var="featuredProducts" param="category.relatedProducts" />
  <dsp:getvalueof var="useSearchForSubcategoryProductList" 
                  bean="StoreConfiguration.useSearchForSubcategoryProductList" />
  <dsp:getvalueof var="includeRecommendations" param="includeRecommendations" />
  
  <%-- 
    NB. search.jsp MUST be prior to any code that may result in the response
    being committed, for example, dsp:include tags.  Therefore it must be before the pageContainer 
    tag, and it must use an include directive to include the search.jsp file.  The reason this is
    important is to allow server-side redirects to work.  A response can only be set to send a 
    redirect when the response is uncommitted. If you have configured a redirect to occur when 
    someone searches for a particular term, for example, then the BaseSearchFormHandler needs to
    have an uncommitted response so it can redirect correctly.
  --%>
  <%@include file="/atgsearch/search.jsp"%>
  
  <%--
    Before we render the page container determine what page we are on (category/subcategory) so
    we can render the correct skip nav link targets as well as the correct content on the page
  --%>
  <c:set var="topLevelCategoryFacet" value="" />
  <%-- 
    TargetingFirst is used to perform a targeting operation with 
    the help of its targeter. We pick the first item from the array
    returned by the targeting operation. We use this to retrieve ID
    of the global category refineElement.
                
    Input Parameters:
      targeter - Specifies the targeter service that will perform
                 the targeting
                             
      howMany - the maximum number of target items to display
                  
    Open Parameters:
      empty - No targets were found
                  
      output - At least 1 target was found
                
    Output Parameters:
      element - the result of a target operation
  --%>
  <dsp:droplet name="TargetingFirst">
    <dsp:param name="howMany" value="1" />
    <dsp:param name="targeter" 
               bean="/atg/registry/RepositoryTargeters/RefinementRepository/GlobalCategoryFacet" />
    <%-- We have a refinement. --%>
    <dsp:oparam name="output">
      <dsp:getvalueof id="refElemRepId" idtype="String" param="element.repositoryId" />
      <c:set var="topLevelCategoryFacet" value="${refElemRepId}:${categoryId}"/>
    </dsp:oparam>
  </dsp:droplet>
  
  <%--
    This is used to determine how we retrieve the products to be displayed. If we have an empty
    facetTrail or the top level category facet then we aren't navigating a facet and should not use
    ATG Search to return products.
  --%>
  <dsp:getvalueof var="originalFacetTrail" param="q_facetTrail" />
  <c:if test="${empty originalFacetTrail || originalFacetTrail eq topLevelCategoryFacet}">
    <c:set var="navigatingDefaultFacet" value="true"/>
  </c:if>
  
  <%-- Pass non-default skip nav links into the page container --%>
  <c:set var="a11yContentDiv" 
         value="${navigatingDefaultFacet eq 'true' && not empty featuredProducts ? 'atg_store_featured_prodList' : 'atg_store_prodList' }"/>

  <crs:pageContainer bodyClass="category atg_store_leftCol" contentClass="page_apparel"
                     a11yContentDiv="${a11yContentDiv}" a11yContentTextKey="skipnav.skipToContent"
                     a11yNavDiv="atg_store_categories" a11yNavTextKey="skipnav.skipToSideMenu">
    
    <jsp:attribute name="SEOTagRenderer">
      <dsp:include page="/global/gadgets/metaDetails.jsp">
        <dsp:param name="catalogItem" param="category"/>
      </dsp:include>
    </jsp:attribute>
    
    <jsp:body>
      <dsp:getvalueof var="item" param="category"/>
      <preview:repositoryItem item="${item}">
        <%-- Display the category header --%>
        <dsp:include page="/browse/gadgets/itemHeader.jsp">
          <dsp:param name="displayName" param="category.displayName"/>
          <dsp:param name="category" param="category"/>
        </dsp:include>
      </preview:repositoryItem>
      
      <%-- Set a default addFacet param --%>
      <c:if test="${empty addFacet && empty facetTrail && not empty topLevelCategoryFacet}">
        <c:set var="addFacet" value="${topLevelCategoryFacet}" />
      </c:if>
      
      <div class="atg_store_main">
        <div id="ajaxContainer" >
          <div divId="ajaxRefreshableContent">
            <c:choose>
              <%-- 
                When the facet trail is empty or default, it means we're in the a category page
                with no faceting being applied. If this category has related products display them.
                CRS currently doesn't support displaying both a categories featured and child products.
              --%>
              <c:when test="${navigatingDefaultFacet eq 'true' && not empty featuredProducts}">
                <dsp:include page="/browse/gadgets/featuredProducts.jsp">
                  <dsp:param name="category" param="category"/>
                </dsp:include>
                
                <%-- 
                  Include the HTML structure from the subcategory page so when a facet is clicked we
                  can load the results into this structure without creating it in the JS.
                --%>
                <div id="atg_store_searchResults" style="display:none">
                  <div id="atg_store_catSubProdList">
                    <dsp:include page="/global/gadgets/sortDisplay.jsp"/>
                    <div class="atg_store_pagination">
                      <ul id="atg_store_pagination"></ul>
                    </div>
                    <div id="atg_store_prodList">
                      <ul class="atg_store_product" id="atg_store_product"></ul>
                    </div>
                    <div class="atg_store_pagination">
                      <ul id="atg_store_pagination"></ul>
                    </div>
                  </div>
                </div>
                                
                <%-- 
                  Include Recommendations container if includeRecommendations 
                  parameter is set to true
                --%>
                <c:if test="${includeRecommendations}">
                  <dsp:include page="/browse/gadgets/categoryRecommendationsContainer.jsp">
                    <dsp:param name="category" param="category"/>
                  </dsp:include>
                </c:if>
                
              </c:when>
              <%-- 
                This will display the category's child products.
              --%>
              <c:when test="${navigatingDefaultFacet eq 'true' && !useSearchForSubcategoryProductList}">
                <div id="atg_store_searchResults">
                  <dsp:include page="/browse/gadgets/categoryProducts.jsp">
                    <dsp:param name="category" param="category" />
                    <dsp:param name="pageNum" value="${pageNum}" />
                  </dsp:include>
                </div>
              </c:when>
              <%--
                Display results using the search engine 
                (e.g when navigating a facet on the category page).
              --%>
              <c:otherwise>
                <dsp:include page="/atgsearch/atgSearchResultsRenderer.jsp"/>
              </c:otherwise>
            </c:choose>
          </div>
          <div name="transparentLayer" id="transparentLayer">
          </div>
            
          <div name="ajaxSpinner" id="ajaxSpinner">
          </div>
            
        </div>
      </div>
      
      <div class="aside">
         
        <%-- Render sub-categories--%>
        <dsp:include page="/browse/gadgets/categoryPanel.jsp" />
        
        <%-- Render faceting--%>
        <dsp:include page="/facet/gadgets/facetPanelContainer.jsp">
          <dsp:param name="facetTrail" value="${facetTrail}" />
          <dsp:param name="trailSize" value="2" />
          <dsp:param name="facetSearchResponse" param="refinement" />
          <dsp:param name="categoryId" value="${categoryId}" />
          <dsp:param name="addFacet" value="${addFacet}" />
        </dsp:include>
        <%-- Render promotions--%>
        <dsp:include page="/global/gadgets/targetingRandom.jsp">
          <dsp:param name="targeter" bean="/atg/registry/Slots/CategoryPromotionContent1"/>
          <dsp:param name="renderer" value="/promo/gadgets/promotionalContentTemplateRenderer.jsp"/>
          <dsp:param name="elementName" value="promotionalContent"/>
        </dsp:include>
        <dsp:include page="/global/gadgets/targetingRandom.jsp">
          <dsp:param name="targeter" bean="/atg/registry/Slots/CategoryPromotionContent2"/>
          <dsp:param name="renderer" value="/promo/gadgets/promotionalContentTemplateRenderer.jsp"/>
          <dsp:param name="elementName" value="promotionalContent"/>
        </dsp:include>
        
        <%-- Render click to call link --%>
        <dsp:include page="/navigation/gadgets/clickToCallLink.jsp">
          <dsp:param name="pageName" value="category"/>
        </dsp:include>
      </div>
      
      <script type="text/javascript"> 
        // Browser back button suport
        dojo.require("dojo.back");
        dojo.back.init();
        dojo.back.setInitialState(new HistoryState(""));
      </script>
      
    </jsp:body>
  </crs:pageContainer>
</dsp:page>

<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/atgsearch/categorySearchResults.jsp#2 $$Change: 684979 $ --%>
