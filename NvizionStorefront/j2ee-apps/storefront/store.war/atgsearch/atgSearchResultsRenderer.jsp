<%-- 
  This page configures and then executes a search based on the supplied parameters.  It will then
  display the search results (unless supplied with the displaySearchResults parameter == false).
       
  NB.  This page MUST be executed prior to any other code which is dependant on the latest search
  results.
         
    Required Parameters:
      (Either)
      q          - The search question.  (Required when doing a query search)
      (Or)
      categoryId - When doing a category search, specifies the category under which to search. (Required when doing
                   a category search).
  
      
    Optional Parameters:
      p             - The page of search results to render.
      pageSize      - The number of results to display per page.
      sort          - The property by which to sort the search results.
      sId           - The ID of a site whose content should also be included in the search.  There may be multiple
                      parameters with this name, which indicates multiple sites should be searched.
      sortOrder     - specifies the order of the sort, i.e. ascending or descending.
      facetTrail    - Records the currently supplied faceting on the search results.
      addFacet      - Specifies a new facet which should be applied, and added to the facet trail.

--%>

<%@ taglib prefix="dsp" uri="http://www.atg.com/taglibs/daf/dspjspTaglib1_1" %>
<dsp:page>

  <%-- imports --%>
  <dsp:importbean bean="/atg/commerce/search/catalog/QueryFormHandler" var="formHandler"/>
  <dsp:importbean bean="/atg/search/repository/FacetSearchTools"/>
  <dsp:importbean bean="/atg/commerce/catalog/ProductLookup"/>
  <dsp:importbean bean="/atg/commerce/catalog/CategoryLookup"/>
  <dsp:importbean bean="/atg/commerce/search/StoreBrowseConstraints"/>
  <dsp:importbean bean="/atg/commerce/search/refinement/CommerceFacetTrailDroplet"/>
  <dsp:importbean bean="/atg/multisite/Site"/>
  <dsp:importbean bean="/atg/search/droplet/GetClickThroughId"/>
  <dsp:importbean bean="/atg/store/profile/RequestBean" var="requestBean"/>
  <dsp:importbean bean="/atg/commerce/search/catalog/QueryFormHandler"/>
  
  <dsp:getvalueof var="categoryId" param="categoryId"/>
  <dsp:getvalueof var="token" param="token"/>
    
  <dsp:getvalueof var="pageSize" param="pageSize"/>
  <c:if test="${empty pageSize}">
    <dsp:getvalueof var="pageSize" vartype="java.lang.Object" bean="Site.defaultPageSize"/>
  </c:if>
    
  <%-- Display the search results --%>    
  
  <%-- The query response --%>
  <dsp:getvalueof bean="QueryFormHandler.searchResponse" var="response"/>
  <dsp:getvalueof var="size"  value="${response.groupCount}"/> 
  
  <%-- Display the number of search results if this is a query search --%>
  <c:if test="${empty categoryId && (not empty response.question)}">
    <div id="atg_store_mainHeader">
      <div class="atg_store_searchResultsCount">
        <c:choose>
          <c:when test="${size == 1}">
            <fmt:message var="resultsMessage" key="facet.facetGlossaryContainer.oneResultFor"/>
          </c:when>
          <c:otherwise>
            <fmt:message var="resultsMessage" key="facet.facetGlossaryContainer.resultFor"/>
          </c:otherwise>
        </c:choose>
        <%-- Escape XML specific characters in search term to prevent using it for XSS attacks. --%>
        <span id="resultsCount"><c:out value="${size}"/></span>&nbsp;<c:out value="${resultsMessage}"/>&nbsp;<span class="searchTerm">${fn:escapeXml(response.question)}</span>
      </div>
    </div>
  </c:if>

  <%--Set the number of product columns to render--%>
  <c:set var="columnCount" value="4"/>
  
  <div id="atg_store_catSubProdList">
    <c:choose>
      <%-- No Results --%>
      <c:when test="${empty size || size == 0 }">
        <crs:messageContainer optionalClass="atg_store_noMatchingItem" titleKey="facet_facetSearchResults.noMatchingItem"/>            
      </c:when>
      <%-- Display Results --%>
      <c:otherwise>
        <%--Display top pagination options--%>
        <dsp:include page="/global/gadgets/productListRangePagination.jsp">
          <dsp:param name="size" value="${size}" />
          <dsp:param name="top" value="true" />
          <dsp:param name="arraySplitSize" value="${pageSize}" />
          <dsp:param name="token" value="${token}"/>
        </dsp:include>
    
        <%-- Display sort options --%>
        <dsp:include page="/global/gadgets/sortDisplay.jsp">
          <dsp:param name="arraySplitSize" value="${pageSize}" />
          <dsp:param name="token" value="${token}"/>
        </dsp:include>
    
        <div id="atg_store_prodList">
          <ul class="atg_store_product" id="atg_store_product">
            <%--
              Loop though each product returned.  Since the search is aware of our desired
              pagination the results will be the correct results for the current page
            --%>
            <c:forEach var="element" items="${response.results}" varStatus="loopStatus">
              <dsp:getvalueof var="index" value="${loopStatus.index}"/>
              <dsp:getvalueof var="count" value="${loopStatus.count}"/>
              <dsp:param name="element" value="${element}"/>
              <dsp:getvalueof var="productId" param="element.document.properties.$repositoryId" />

              <%--
                Generates unique search click identifier for each search result. The generated search click
                identifier will be appended to product links on search results page as URL parameter and will
                notify reporting service. This parameter tells reporting service that user navigated to the product
                page from the search results and which search query returned the product.
                
                Input Parameters:
                  result
                    Search result to generate search click identifier for.
                    
                Open Parameters:
                  output
                    Always serviced.
                    
                Output Parameters:
                  searchClickId
                    Generated search click identifier. 
                
               --%>
              <dsp:droplet name="GetClickThroughId">
                <dsp:param name="result" param="element"/>
                <dsp:oparam name="output">

                  <%--
                    Get the product according to the ID returned from the ATG Search results
               
                    Input Parameters:
                      id - The ID of the product we want to look up
                      filterBySite - The site to filter by, or false if no filter should be applied
                      filterByCatalog - The catalog to filter by, or false if no filter should be applied
                  
                    Open Parameters:
                      output - Serviced when no errors occur
                      error - Serviced when an error was encountered when looking up the product
                      empty - Serviced when no product is found
                  
                    Output Parameters:
                      element - The product whose ID matches the 'id' input parameter  
                  --%>
                  <dsp:droplet name="ProductLookup">
                    <dsp:param bean="/OriginatingRequest.requestLocale.locale" name="repositoryKey"/>
                    <dsp:param name="id" value="${productId}"/>
                    <dsp:param name="filterBySite" value="false"/>
                    <dsp:param name="filterByCatalog" value="false"/>
                
                    <dsp:oparam name="output">
                      <dsp:setvalue param="product" paramvalue="element"/>
                      <%-- Get the correct class for the <li> based on this loop's current index --%>
                      <dsp:getvalueof var="additionalClasses" vartype="java.lang.String" 
                                      value="${(count % columnCount) == 1 ? 'prodListBegin' : ((count % columnCount) == 0 ? 'prodListEnd':'')}"/>
                      
                      <li class="<crs:listClass count="${count}" size="${size}" selected="false"/>${empty additionalClasses ? '' : ' '}${additionalClasses}">  
                        <%-- 
                          Figure out if this product belongs to the current site, if not, 
                          display the site indicator in productListRangeRow 
                        --%>
                        <dsp:getvalueof var="productSites" param="product.siteIds" />
                        <dsp:getvalueof var="siteId" bean="Site.id" />
                        <dsp:contains var="productFromCurrentSite" values="${productSites}" object="${siteId}"/>
                      
                        <%-- Display the product --%>
                        <dsp:include page="/global/gadgets/productListRangeRow.jsp">
                          <dsp:param name="categoryId" param="product.parentCategory.id" />                      
                          <dsp:param name="product" param="product" />
                          <dsp:param name="categoryNav" value="false" />
                          <dsp:param name="displaySiteIndicator" value="${!productFromCurrentSite}" />
                          <dsp:param name="mode" value="name"/>
                          <dsp:param name="searchClickId" param="searchClickId"/>
                        </dsp:include>
                      </li>
                    </dsp:oparam>
                  </dsp:droplet>
                </dsp:oparam>
              </dsp:droplet>
            </c:forEach>
          </ul>
        </div>
      
        <%--Display bottom pagination options--%>
        <dsp:include page="/global/gadgets/productListRangePagination.jsp">
          <dsp:param name="size" value="${size}" />
          <dsp:param name="top" value="true" />
          <dsp:param name="arraySplitSize" value="${pageSize}" />
          <dsp:param name="token" value="${token}"/>
        </dsp:include>
      </c:otherwise>
    </c:choose>
  </div>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/atgsearch/atgSearchResultsRenderer.jsp#1 $$Change: 683854 $--%>
