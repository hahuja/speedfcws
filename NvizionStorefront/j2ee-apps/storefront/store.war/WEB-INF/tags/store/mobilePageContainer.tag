<%--
  Tag that acts as a container for all top level mobile pages, including all relevant
  header, footer and nav elements.

  Required Parameters:
    titleString - the title of the current page
  
  Optional Parameters:
    displayModal - if present, modal dialog will be visible on page load
  
  Page Fragments:
    modalContent (optional) - renders modal dialog / popup content for the page
--%>
<%@ include file="/includes/taglibs.jspf" %>
<%@ include file="/includes/context.jspf" %>

<%@ tag language="java" %>

<%@ attribute name="titleString" required="true" %>
<%@ attribute name="modalContent" fragment="true" %>
<%@ attribute name="displayModal" required="false" %>

<%-- mobile page container --%>
  <dsp:include page="/mobile/includes/pageStart.jsp">
    <dsp:param name="titleString" value="${titleString}"/>
  </dsp:include>
  <div id="mobile_store_pageContainer">
    <%@ include file="/mobile/includes/header.jspf" %>
    <div id="mobile_store_container">
      <div id="mobile_store_contentContainer">
        <div id="mobile_store_content">
          <jsp:doBody/>
        </div> 
      </div>

    </div>

    <%@include file="/mobile/includes/footer.jspf" %>
    
    <%-- Include modal overlay --%>
    <div id="mobile_store_modalOverlay" class="${((!empty displayModal) && displayModal) ? '' : 'hidden'}" onclick="toggleModal(false);">
      <div class="shadow">
        <!-- Accessibility hint to allow user to dismiss the modal overlay
          br elements move the text down the page; anything other than plain text will not be read
        -->
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <fmt:message key="mobile.accessibility.overlay"/>
      </div>
      <div id="mobile_store_contactInfo">
        <ul class="mobile_store_mobileCRSRedirectTable">
          <li><fmt:message key="navigation_tertiaryNavigation.contactUs"/></li>
          <li><dsp:include page="/mobile/includes/gadgets/phone.jsp" /></li>
          <li><dsp:include page="/mobile/includes/gadgets/email.jsp" /></li>
        </ul>
      </div>
      <jsp:invoke fragment="modalContent"/>
    </div>
    
  </div>
  <dsp:include page="/mobile/includes/pageEnd.jsp"/>
<%-- mobile page container --%>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/WEB-INF/tags/store/mobilePageContainer.tag#3 $$Change: 692002 $ --%>
