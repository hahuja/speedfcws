<%--
  Renders the drop down containing the different sorting options.
   
  Required Parameters:
    (Either)
    q
      The search question.  (Required when doing a query search)
    (Or)
    categoryId
      When doing a category search, specifies the category under which to search. (Required when
      doing a category search).
   
  Optional Parameters:
    p
      The page of search results to render.
    pageSize
      The number of results to display per page.
    sort
      How the results are ordered.
    facetTrail  
      Records the currently supplied faceting on the search results.
    addFacet
      Specifies a new facet which should be applied, and added to the facet trail.
    viewAll
      Whether all search results should be displayed on a single page
--%>
 
 <dsp:page>  
  <dsp:getvalueof var="categoryNavIds" param="categoryNavIds"/>
  <dsp:getvalueof var="addFacet" param="addFacet"/>
  <dsp:getvalueof var="facetOrder" param="facetOrder"/>
  <dsp:getvalueof var="facetTrail" param="q_facetTrail"/>
  <dsp:getvalueof var="q" param="q"/>
  <dsp:getvalueof var="token" param="token"/>
  <dsp:getvalueof var="categoryId" param="categoryId"/>
  <dsp:getvalueof var="p" param="p"/>
  <dsp:getvalueof var="pageSize" param="pageSize"/>  
  <dsp:getvalueof var="viewAll" param="viewAll"/>
  <dsp:getvalueof var="sort" param="sort"/>
  <dsp:getvalueof var="_requestid" param="_requestid"/>
    
  <dsp:getvalueof var="originatingRequestURL" bean="/OriginatingRequest.requestURI"/>
  <dsp:getvalueof id="contextRoot" bean = "/OriginatingRequest.contextPath"/>
  
  <%-- Create a sid string which determines which sites we search, its a multi valued parameter --%>
  <c:set var="siteIds" value=""/>
  <c:forEach var="value" items="${paramValues.sid}" varStatus="loopStatus">
    <c:choose>
      <c:when test="${loopStatus.count == 1}">
        <c:set var="siteIds" value="${value}"/>
      </c:when>
      <c:otherwise>
        <c:set var="siteIds" value="${siteIds}&sid=${value}"/>
      </c:otherwise>
    </c:choose>
  </c:forEach>
    
  <div class="atg_store_filter">

    <dsp:form id="sortByForm" action="${contextPath}${originatingRequestURL}">
      <label for="sortBySelect">
        <fmt:message key="common.sortBy" />:
      </label>
      
      <select id="sortBySelect" name="sort">
              
        <option value="" ${(empty sort) ? 'selected="selected"' : ''}>
          <fmt:message key="common.topPicks"/>
        </option>
        
        <option value="displayName:ascending" ${sort=='displayName:ascending' ? 'selected="selected"' : ''}>
          <fmt:message key="sort.nameAZ"/>
        </option>
        
        <option value="displayName:descending" ${sort=='displayName:descending' ? 'selected="selected"' : ''}>
          <fmt:message key="sort.nameZA"/>
        </option>
        
        <option value="price:ascending" ${sort=='price:ascending' ? 'selected="selected"' : ''}>
          <fmt:message key="sort.priceLH"/>
        </option>
        
        <option value="price:descending" ${sort=='price:descending' ? 'selected="selected"' : ''}>
          <fmt:message key="sort.priceHL"/>
        </option>
      </select>
      
      <input type="hidden" value="" name="p"/>
      
      <c:if test="${not empty categoryNavIds}">
        <%--
          Escape XML specific characters in categoryNavIds parameter as it is taken directly from
          URL parameter and can be used in XSS attacks.
        --%>
        <input type="hidden" value="${fn:escapeXml(categoryNavIds)}" name="categoryNavIds"/>
      </c:if>
      <c:if test="${not empty facetOrder}">
        <input type="hidden" value="${facetOrder}" name="facetOrder"/>
      </c:if>
      <c:if test="${not empty facetTrail}">
        <input type="hidden" value="${facetTrail}" name="q_facetTrail"/>
      </c:if>
      <c:if test="${not empty q}">
        <input type="hidden" value="${q}" name="q"/>
      </c:if>
      <c:if test="${not empty categoryId}">
        <input type="hidden" value="${categoryId}" name="categoryId"/>
      </c:if>
      <c:if test="${not empty pageSize}">
        <input type="hidden" value="${pageSize}" name="pageSize"/>
      </c:if>
      <c:if test="${not empty viewAll}">
        <%-- Escape XML specific characters to prevent XSS attacks. --%>
        <input type="hidden" value="${fn:escapeXml(viewAll)}" name="viewAll"/>
      </c:if>
      <c:if test="${not empty siteIds}">
        <input type="hidden" value="${siteIds}" name="sid"/>
      </c:if>
      <c:if test="${not empty siteIds}">
        <input type="hidden" value="${siteIds}" name="sid"/>
      </c:if>
      <c:if test="${not empty _requestid}">
        <input type="hidden" value="${_requestid}" name="_requestid"/>
      </c:if>
      
      <noscript>
        <span class="atg_store_sortButton">
          <input value="Sort" type="submit"/>
        </span>
      </noscript>
      
    </dsp:form>
    
  </div>
 </dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/global/gadgets/sortDisplay.jsp#1 $$Change: 683854 $--%>