<%-- 
  This page renders the facet links on the left navigation bar when ATG Search is installed.
  
  Required Parameters:
    facetTrail
      Records the currently supplied faceting on the search results.
    trailSize
      The trail size (number of facets traversed) upto this point
    numResults
      Number of search results
    categoryId
      The category under consideration

  Optional Parameters:
    addFacet
      The facet value for which refinement is requested, this will be added to the facetTrail
--%>
<dsp:page>
  <dsp:importbean bean="/atg/search/repository/FacetSearchTools"/>

  <dsp:getvalueof var="numResults" param="numResults"/>
  <dsp:getvalueof var="categoryId" param="categoryId"/>
  <dsp:getvalueof var="addFacet" param="addFacet"/>
  <dsp:getvalueof var="facetTrail" param="facetTrail"/>
  <dsp:getvalueof var="trailSize" param="trailSize"/>
  <dsp:getvalueof var="removeFacet" param="removeFacet"/>

  <%-- Render Facets --%>
  <dsp:getvalueof var="facetHolders" bean="FacetSearchTools.facets"/>
  <c:if test="${(not empty facetHolders) || (not empty facetTrail)}">
    <div id="atg_store_facets" class="atg_store_facetsGroup">
      <noscript>
        <dsp:include page="/facet/gadgets/facetNavigationRenderer.jsp" />
      </noscript>
    </div>
    
    <script type="text/javascript" charset="utf-8">
      var jsonFacets = <%@include file="/facetjson/facetData.jsp" %>;
      atg_facetManager = new atg.store.widget.facetManager({
        facetData: jsonFacets,
        facetsNode: dojo.byId("atg_store_facets"),
        id:"atg_store_facetManager"
      });
    </script>
  </c:if>
  
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/facet/gadgets/facetPanelContainer.jsp#1 $$Change: 683854 $--%>