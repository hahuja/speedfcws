<%-- 
  This page lays out the category page and acts as the delegate for category.jsp when running
  without ATG Search.
     
  Required parameters:
    category
      The category to be displayed
     
  Optional parameters:
    p
      The page number of the category products to display
    viewAll
      Should all products be displayed on a single page?
    includeRecommendations
      Should recommendation be displayed?
--%>
<dsp:page>
  <dsp:importbean bean="/atg/commerce/catalog/ProductSearch"/>
  <dsp:importbean bean="/atg/commerce/catalog/ProductLookup"/>
  <dsp:importbean bean="/atg/dynamo/droplet/Range"/>
  
  <dsp:getvalueof var="category" param="category" />
  <dsp:getvalueof var="children" param="category.childProducts" />
  <dsp:getvalueof var="featuredProducts" param="category.relatedProducts" />
  <dsp:getvalueof var="pageNum"  param = "p" />
  <dsp:getvalueof var="viewAll"  param = "viewAll" />
  <dsp:getvalueof var="includeRecommendations"  param = "includeRecommendations" />
  
  <%-- Pass non-default skip nav links into the page container --%>
  <c:set var="a11yContentDiv" 
         value="${not empty featuredProducts ? 'atg_store_featured_prodList' : 'atg_store_prodList' }"/>

  <crs:pageContainer divId="atg_store_searchResultsIntro" index="false" follow="false"
                     bodyClass="category atg_store_searchResults atg_store_leftCol"
                     a11yContentDiv="${a11yContentDiv}" a11yContentTextKey="skipnav.skipToContent"
                     a11yNavDiv="atg_store_categories" a11yNavTextKey="skipnav.skipToSideMenu">
                     
    <jsp:attribute name="SEOTagRenderer">
      <dsp:include page="/global/gadgets/metaDetails.jsp">
        <dsp:param name="catalogItem" param="category"/>
      </dsp:include>
    </jsp:attribute>

    <jsp:body>
      <preview:repositoryItem item="${category}">
        <%-- The category header --%>
        <dsp:include page="/browse/gadgets/itemHeader.jsp">
          <dsp:param name="displayName" param="category.displayName"/>
          <dsp:param name="category" param="category"/>
        </dsp:include>
      </preview:repositoryItem>
      
      <div class="atg_store_main">
        <div id="ajaxContainer">
        <div divId="ajaxRefreshableContent">

          <c:choose>
            <%-- Display the featured products --%>
            <c:when test="${not empty featuredProducts}">
              <dsp:include page="/browse/gadgets/featuredProducts.jsp">
                <dsp:param name="category" param="category"/>
              </dsp:include>
            </c:when>
            <c:otherwise>
                            
              <c:choose>
                <%-- Display the category's products --%>
                <c:when test="${not empty children}">
                  <div id="atg_store_searchResults">
                    <dsp:include page="/browse/gadgets/categoryProducts.jsp">
                      <dsp:param name="category" param="category" />
                      <dsp:param name="pageNum" value="${pageNum}" />
                    </dsp:include>
                  </div>
                </c:when>
                <c:otherwise>
                  <crs:messageContainer optionalClass="atg_store_noMatchingItem"
                    titleKey="facet_facetSearchResults.noMatchingItem"/>
                </c:otherwise>
              </c:choose>
              
            </c:otherwise>
          </c:choose>
          
          <%-- 
            Include Recommendations container if includeRecommendations 
            parameter is set to true
          --%>
          <c:if test="${includeRecommendations}">
            <dsp:include page="/browse/gadgets/categoryRecommendationsContainer.jsp">
              <dsp:param name="category" param="category"/>
            </dsp:include> 
          </c:if>
          
          <div name="transparentLayer" id="transparentLayer"></div>
          <div name="ajaxSpinner" id="ajaxSpinner"></div>
        </div>
        </div>
      </div>

      <div class="aside">
        <%-- Render sub-categories--%>
        <dsp:include page="/browse/gadgets/categoryPanel.jsp"/>
      
        <%-- Render promotions--%>
        <dsp:include page="/global/gadgets/targetingRandom.jsp">
          <dsp:param name="targeter" bean="/atg/registry/Slots/CategoryPromotionContent1"/>
          <dsp:param name="renderer" value="/promo/gadgets/promotionalContentTemplateRenderer.jsp"/>
          <dsp:param name="elementName" value="promotionalContent"/>
        </dsp:include>
        
        <dsp:include page="/global/gadgets/targetingRandom.jsp">
          <dsp:param name="targeter" bean="/atg/registry/Slots/CategoryPromotionContent2"/>
          <dsp:param name="renderer" value="/promo/gadgets/promotionalContentTemplateRenderer.jsp"/>
          <dsp:param name="elementName" value="promotionalContent"/>
        </dsp:include>
        
        <%-- Render click to call link --%>
        <dsp:include page="/navigation/gadgets/clickToCallLink.jsp">
          <dsp:param name="pageName" value="category"/>
        </dsp:include>
      </div> 
    </jsp:body>
  </crs:pageContainer>
  
  <script type="text/javascript">
    // Browser back button suport
    dojo.require("dojo.back");
    dojo.back.init();
    dojo.back.setInitialState(new HistoryState(""));
    
    // if this page is loaded from a bookmark we need to parse window.location.hash
    // to retrieve the parameters for this search.
    // handlePageLoadFromBookmark(); 
  </script>

</dsp:page>

<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/search/categorySearchResults.jsp#2 $$Change: 684979 $--%>
