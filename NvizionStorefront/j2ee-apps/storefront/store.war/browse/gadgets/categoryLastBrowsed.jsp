<%--
  This gadget updates category last browsed property on the current profile.
  If no category ID specified, default value would be taken from the CatalogNavHistory collection.

  Required parameters:
    None.

  Optional parameters:
    lastBrowsedCategory
      Category ID to update profile's property with.
--%>

<dsp:page>
  <dsp:importbean bean="/atg/commerce/catalog/CatalogNavHistory"/>
  <dsp:importbean bean="/atg/userprofiling/Profile" />

  <dsp:getvalueof var="lastBrowsedCategory" param="lastBrowsedCategory"/>
  <c:if test="${empty lastBrowsedCategory}">
    <%-- No ID specified. Get top level category from navigation history component. --%>
    <dsp:getvalueof var="navHistory" bean="CatalogNavHistory.navHistory"/>
    <c:if test="${not empty navHistory && fn:length(navHistory) > 1}">
      <dsp:getvalueof var="lastBrowsedCategory" bean="CatalogNavHistory.navHistory[1].repositoryId"/>
    </c:if>     
  </c:if>

  <%-- Update profile's property. --%>
  <c:if test="${not empty lastBrowsedCategory}">
    <dsp:setvalue bean="Profile.categoryLastBrowsed" value="${lastBrowsedCategory}"/>
  </c:if>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/browse/gadgets/categoryLastBrowsed.jsp#1 $$Change: 683854 $--%>
