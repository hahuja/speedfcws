<%--
  This gadget renders all recommended products for the currently viewed product.
  Only products from the current site group will be displayed.

  Required parameters:
    product
      Specifies a product whose related items should be displayed.

  Optional parameters:
    None.
--%>

<dsp:page>
  <dsp:getvalueof var="product" param="product" />

  <dsp:importbean  bean="/atg/commerce/collections/filter/droplet/ExcludeItemsInCartFilterDroplet" />
  <dsp:importbean  bean="/atg/commerce/collections/filter/droplet/CartSharingFilterDroplet" />

  <%--
    This droplet filters out product catalog items already added to the shopping cart.

    Input parameters:
      collection
        Collection of catalog items to be filtered.

    Output parameters:
      filteredCollection
        Collection of catalog items that not added to the cart.

    Open parameters:
      output
        Rendered when filteredCollection is not empty.
  --%>
  <dsp:droplet name="ExcludeItemsInCartFilterDroplet">
    <dsp:param name="collection" param="product.relatedProducts" />
    <dsp:oparam name="output">
      <%--
        This droplet filters out catalog items that are located on the wrong site. I.e. items from another site group.

        Input parameters:
          collection
            Collection of catalog items to be filtered.

        Output parameters:
          filteredCollection
            Already processed collection.

        Open parameters:
          output
            Rendered when filteredCollection is not empty.
      --%>
      <dsp:droplet name="CartSharingFilterDroplet">
        <dsp:param name="collection" param="filteredCollection"/>
        <dsp:oparam name="output">
          <dsp:getvalueof var="filteredCollection" param="filteredCollection"/>
          
          <%-- Display content only if we have recommended products after we've applied all filters. --%>
          <c:if test="${not empty filteredCollection}">
            <div id="atg_store_recommendedProducts">
              <h3>
                <fmt:message key="browse_recommendedProducts.ourDesignersSuggest" /><fmt:message key="common.labelSeparator" />
              </h3>
              <ul class="atg_store_product">
                <dsp:getvalueof var="size" value="${fn:length(filteredCollection)}"/>
                <%-- Only show five of them. --%>
                <c:forEach var="relatedProduct" items="${filteredCollection}" varStatus="status" begin="0" end="4">
                  <dsp:param name="relatedProduct" value="${relatedProduct}"/>
                  <dsp:getvalueof var="templateUrl" param="relatedProduct.template.url" />
                  <%-- Display only products with properly configured template. --%>
                  <c:if test="${not empty templateUrl}">
                    <fmt:message var="linkTitle" key="browse_recommendedProducts.productLinkTitle" />
                    <li class="<crs:listClass count="${status.count}" size="${ size < 5 ? size : 5}" selected="false"/>">
                      <%-- Render product related product info. --%>
                      <dsp:include page="/global/gadgets/promotedProductRenderer.jsp">
                        <dsp:param name="product" param="relatedProduct" />
                        <dsp:param name="imagesize" value="medium"/>
                      </dsp:include>
                    </li>
                  </c:if> <%-- End is template empty --%>
                </c:forEach><%-- End For Each related product --%>
              </ul>
            </div>
          </c:if>
        </dsp:oparam>
      </dsp:droplet>
    </dsp:oparam>
  </dsp:droplet><%-- End ExcludeItemsInCartFilterDroplet --%>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/browse/gadgets/recommendedProducts.jsp#1 $$Change: 683854 $ --%>
