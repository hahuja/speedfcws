<%--
  This page displays an item defined by the 'itemIndex' parameter 
  and links it to it's parent. If the CatalogNavHistory is 
  Home->Category2->Category3 and itemIndex is 1, it will display
  
  <a href="[link to home]">Category2</a>
  
  Required parameters:       
    itemIndex 
      A zero-based index of the item to be displayed, must be greater or equal to one
      
  Optional parameters:
    None.   
--%>
<dsp:page>
  <dsp:getvalueof var="navItemShift" vartype="java.lang.Integer" param="itemIndex"/>
  <c:if test="${navItemShift > 0}">
    <dsp:getvalueof var="navHistory" 
                    vartype="java.util.Collection" 
                    scope="page" 
                    bean="/atg/commerce/catalog/CatalogNavHistory.navHistory"/>
                    
    <dsp:param name="displayNameCategory" value="${navHistory[navItemShift]}"/>
    <c:choose>
      <c:when test="${navItemShift == 1}">
        <%-- Parent is a home page --%>
        <c:url var="url" scope="page" value="/index.jsp"/>
      </c:when>
      <c:otherwise>
      
        <%--
          This droplet calculates a default site-aware URL 
          for the parent category.
  
          Input parameters:
            item
              Product catalog item an URL should be calculated for.
  
          Output parameters:
            url
              URL calculated.
  
          Open parameters:
            output
              Always rendered.
        --%>
        <dsp:droplet name="/atg/repository/seo/CatalogItemLink">
          <dsp:param name="item" value="${navHistory[navItemShift - 1]}"/>
          <dsp:oparam name="output">
            <dsp:getvalueof var="url" vartype="java.lang.String" scope="page" param="url"/>
            
              <%--
                This droplet determines user's browser type.
              
                Input parameters:
                  None.
              
                Output parameters:
                  browserType
                    Specifies a user's browser type.
              
                Open parameters:
                  output
                    Always rendered.
              --%>
              <dsp:droplet name="/atg/repository/seo/BrowserTyperDroplet">
                <dsp:oparam name="output">
                  <dsp:getvalueof var="browserType" param="browserType"/>
                  <c:set var="isIndirectUrl" value="${browserType eq 'robot'}"/>
                </dsp:oparam>
              </dsp:droplet>
              
              <dsp:getvalueof var="atgSearchInstalled" bean="/atg/store/StoreConfiguration.atgSearchInstalled"/>
              <c:url var="url" scope="page" value="${url}">
            
              <c:if test="${not isIndirectUrl}">
                <c:param name="q_docSort"><dsp:valueof param="q_docSort"/></c:param>
                <c:if test="${atgSearchInstalled == 'true'}">
                  
                  <%--
                    TargetingFirst is used to perform a targeting operation with
                    the help of its targeter. We pick the first item from the array
                    returned by the targeting operation. We use this to retrieve ID
                    of the global category refineElement.
                     
                    Input Parameters:
                      howMany
                        Indicates the maximum number of items to display 
                      targeter
                        Specifies the targeter service that will perform the targeting.                          
                      elementName
                        The name of the targetedProduct
                     
                    Open Parameters:
                      output
                        At least 1 target was found.
                     
                    Output Parameters:
                      element
                        The result of a target operation.
                  --%>
                  <dsp:droplet name="/atg/targeting/TargetingFirst">
                    <dsp:param name="howMany" value="1"/>
                    <dsp:param name="targeter" bean="/atg/registry/RepositoryTargeters/RefinementRepository/GlobalCategoryFacet"/>
                    <dsp:oparam name="output">
                      <dsp:getvalueof var="refElemRepId" vartype="java.lang.String" scope="page" param="element.repositoryId"/>
                    </dsp:oparam>
                  </dsp:droplet>
                  <c:param name="addFacet" value="${refElemRepId}:${navHistory[navItemShift - 1].repositoryId}"/>
                </c:if>
              </c:if>
            </c:url>
          </dsp:oparam>
        </dsp:droplet>
      </c:otherwise>
    </c:choose>
    <%--varible item is used in repositoryItem tag--%>
    <dsp:getvalueof var="item" param="displayNameCategory"/>
    <preview:repositoryItem item="${item}">
      <a href="${url}">
        <dsp:valueof param="displayNameCategory.displayName"/>
      </a>
    </preview:repositoryItem>
  </c:if>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/browse/gadgets/linkToNavItem.jsp#1 $$Change: 683854 $--%>
