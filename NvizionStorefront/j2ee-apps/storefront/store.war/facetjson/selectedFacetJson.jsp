<%--
  This page returns JSON data for facets that are currently selected (FacetValue objects)
  
  Required parameters:
    facetValue
      A selected facet (FacetValue)
  
  Optional Parameters:
    None.
--%>
<dsp:page>
  <dsp:importbean bean="/atg/commerce/search/refinement/CommerceFacetTrailDroplet" />
  <dsp:importbean bean="/atg/search/repository/FacetSearchTools"/>

  <dsp:getvalueof var="facetValue" param="facetValue"/>
  <dsp:getvalueof var="facetName" vartype="java.lang.String" value="${facetValue.facet.label}"/>
  <dsp:getvalueof var="facetId" vartype="java.lang.String" value="${facetValue.facet.id}"/>
        
  <json:object>            
    
    <%-- Facet Name (e.g Price) --%>
    <json:property name="name">
      <fmt:message key="${facetName}"/>
    </json:property>
    
    <%-- Facet Repository Id --%>
    <json:property name="id">
     <dsp:valueof value="${facetId}"/>
    </json:property>
              
    <%-- Is this facet applied? --%>
    <json:property name="selected">${true}</json:property>           
    
    <%-- Facet Value options --%>
    <json:array name="options">
      <json:object>
      
        <%-- Applied facet name (e.g $50-$100) --%>
        <json:property name="facetName">
          <%--
            Used to get human readable strings from a facet.
            
            Input Parameters:
              refinementValue - The facet
              
              refinementId - Facet id
              
              locale - The desired locale
            
            Open Parameters:
              output - Rendered when there are no errors.
              
            Output Parameters:
              displayValue - The facet refinements display value
          --%>
          <dsp:droplet name="/atg/commerce/search/refinement/RefinementValueDroplet">
            <dsp:param name="refinementValue" value="${facetValue.value}"/>
            <dsp:param name="refinementId" value="${facetValue.facet.id}"/>
            <dsp:param name="locale" bean="/atg/userprofiling/Profile.PriceList.locale"/>
                
            <dsp:oparam name="output">
              <dsp:valueof param="displayValue" valueishtml="true"/>
            </dsp:oparam>
          </dsp:droplet>
        </json:property>
        
        <%-- FacetTrail String --%>        
        <json:property name="urlFacet" escapeXml="false">
          <%--
            Generates a FacetTrail bean based on the input parameters
   
            Input Parameters:
             trail - The facet trail string to be converted to a bean
             refineConfig - the RefineConfig to use
    
            Open Parameters:
              output - Serviced when no errors occur
    
            Output Parameters:
              facetTrail - The generated FacetTrail bean.  
          --%>
          <dsp:droplet name="CommerceFacetTrailDroplet">
            <dsp:param name="trail" bean="FacetSearchTools.facetTrail" />
            <dsp:param name="removeFacet" param="facetValue" />
            <dsp:param name="refineConfig" param="catRC" />
            <dsp:oparam name="output">
              <dsp:getvalueof param="facetTrail.trailString" var="facetTrailString"/>
              <c:out value="${facetTrailString}" escapeXml="false"/>            
            </dsp:oparam>
          </dsp:droplet>
        </json:property>
                
      </json:object>
    </json:array>
  </json:object>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/facetjson/selectedFacetJson.jsp#1 $$Change: 683854 $--%>
