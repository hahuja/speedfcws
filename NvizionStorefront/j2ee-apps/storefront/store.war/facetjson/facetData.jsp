<%--
  This page returns JSON data containing facets from a search query.
  This page does not do the actual querying of the search engine.
  
  Required parameters:
    None.

  Optional parameters:
    categoryId
      repository id of category that is being browsed (Required for category search)
--%>
<dsp:page>
  <dsp:importbean bean="/atg/commerce/search/catalog/QueryFormHandler"/>
  <dsp:importbean bean="/atg/search/repository/FacetSearchTools"/>
  <dsp:importbean bean="/atg/userprofiling/Profile" />
  <dsp:importbean bean="/atg/commerce/search/refinement/CommerceFacetTrailDroplet" />
  <dsp:importbean bean="/atg/commerce/catalog/CategoryLookup" />
  <dsp:importbean bean="/atg/store/droplet/FacetDisplayDroplet"/>
  <dsp:importbean bean="/atg/targeting/TargetingFirst" />
  
  <dsp:getvalueof var="categoryId" param="categoryId" />
  <dsp:getvalueof var="question" bean="QueryFormHandler.searchRequest.question" />
  <dsp:getvalueof var="resultsCount" bean="QueryFormHandler.searchResponse.groupCount" />
      
  <%-- 
    TargetingFirst is used to perform a targeting operation with 
    the help of its targeter. We pick the first item from the array
    returned by the targeting operation. We use this to retrieve ID
    of the global category refineElement.
                
    Input Parameters:
      targeter - Specifies the targeter service that will perform
                             the targeting
                             
      howMany - the maximum number of target items to display
                  
    Open Parameters:
      empty - No targets were found
                  
      output - At least 1 target was found
                  
    Output Parameters:
      element - the result of a target operation
  --%>
  <dsp:droplet name="TargetingFirst">
    <dsp:param name="howMany" value="1" />
    <dsp:param name="targeter"
               bean="/atg/registry/RepositoryTargeters/RefinementRepository/GlobalCategoryFacet" />

    <dsp:oparam name="output">
      <dsp:getvalueof var="globalCategoryRefinementId" 
                        vartype="java.lang.String" param="element.repositoryId" />
    </dsp:oparam>
  </dsp:droplet>
    
  <c:if test="${not empty categoryId }">
    <%--
      Retrieve the current category repository item from the categoryId string.
  
      Input Parameters:
        id - The categoryId string
  
      Open Parameters:
        output - Rendered when there are no errors
    
      Output Parameters:
        element - The category repository item
    --%>
    <dsp:droplet name="CategoryLookup">
      <dsp:param name="id" param="categoryId"/>
      <dsp:oparam name="output">
        <dsp:setvalue param="catRC" paramvalue="element.refineConfig"/>
        <dsp:getvalueof var="catRC" param="element.refineConfig"/>
      </dsp:oparam>
    </dsp:droplet>
  </c:if>
  
  <%--
    Generates a FacetTrail bean based on the input parameters
   
    Input Parameters:
      trail - The facet trail string to be converted to a bean
      refineConfig - the RefineConfig to use
    
    Open Parameters:
      output - Serviced when no errors occur
    
    Output Parameters:
      facetTrail - The generated FacetTrail bean.  
  --%>
  <dsp:droplet name="CommerceFacetTrailDroplet">
    <dsp:param name="trail" bean="FacetSearchTools.facetTrail" />
    <dsp:param name="refineConfig" param="catRC" />
    <dsp:oparam name="output">
      <dsp:getvalueof var="currentTrail" param="facetTrail" />
    </dsp:oparam>
  </dsp:droplet>
        
  <%--
    Use the FacetDisplayDroplet to order facets by their priority and retrieve information
    on which facets shouldnt have the more/less pagination applied.
    
    Input Parameters:
      appliedFacets - The currently applied facets i.e facetTrail
    
    Open Parameters:
      output - Rendered if there are no errors
    
    Output Parameters:
      facetOptions - A List of FacetHolders and FacetValues
      displayAll - A list of refinements whose facets shouldnt have the More/Less pagination applied
   --%>
  <dsp:droplet name="FacetDisplayDroplet">
    <dsp:param name="appliedFacets" value="${currentTrail}" />
    <dsp:oparam name="output">
      <dsp:getvalueof var="facetOptions" param="facetOptions" />
      <dsp:getvalueof var="displayAll" param="displayAll"/>
    </dsp:oparam>
  </dsp:droplet> 
  
  <%-- Create the JSON --%>  
  <dsp:include page="/facetjson/facetJson.jsp">
    <dsp:param name="facetOptions" value="${facetOptions}"/>
    <dsp:param name="displayAll" value="${displayAll}"/>
    <dsp:param name="question" value="${question}"/>
    <dsp:param name="resultsCount" value="${resultsCount}"/>
    <dsp:param name="categoryId" value="${categoryId}"/>
  </dsp:include>
    
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/facetjson/facetData.jsp#1 $$Change: 683854 $--%>
