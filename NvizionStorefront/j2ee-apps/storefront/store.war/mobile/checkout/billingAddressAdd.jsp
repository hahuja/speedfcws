<%--
  This page fragment renders create new billing address page for user when he creates new credit card

  Page includes:
    /mobile/myaccount/gadgets/addressAddEdit.jsp renderer of address form

  Required parameters:
    None

  Optional parameters:
    None
--%>
<dsp:page>
  <dsp:importbean bean="/atg/store/mobile/order/purchase/MobileBillingFormHandler"/>
  <dsp:importbean bean="/atg/userprofiling/Profile"/>
  <dsp:importbean bean="/atg/store/order/purchase/CouponFormHandler"/>

  <dsp:getvalueof var="init" param="init" vartype="java.lang.Boolean"/>

  <c:if test="${init}">
    <dsp:setvalue bean="MobileBillingFormHandler.resetAddressData" value=""/>
  </c:if>

  <fmt:message key="mobile.common.newBillingAddress" var="pageTitle"/>
  <crs:mobilePageContainer titleString="${pageTitle}">
    <jsp:body>
      <dsp:getvalueof var="formExceptions" vartype="java.lang.Object" bean="MobileBillingFormHandler.formExceptions"/>

      <%-- Handle form exceptions --%>
      <jsp:useBean id="errorMap" class="java.util.HashMap"/>
      <c:if test="${not empty formExceptions}">
        <c:forEach var="formException" items="${formExceptions}">
          <dsp:param name="formException" value="${formException}"/>
          <%-- Check the error message code to see what we should do --%>
          <dsp:getvalueof var="errorCode" vartype="java.lang.String" param="formException.errorCode"/>

          <c:choose>
            <c:when test="${'stateIsIncorrect' eq errorCode}">
              <c:set target="${errorMap}" property="state" value="invalid"/>
              <c:set target="${errorMap}" property="country" value="invalid"/>
            </c:when>
            <c:when test="${'missingRequiredValue' eq errorCode}">
              <dsp:getvalueof var="propertyName" param="formException.propertyName"/>
              <c:set target="${errorMap}" property="${propertyName}" value="missing"/>
            </c:when>
          </c:choose>
        </c:forEach>
      </c:if>

      <div class="mobile_store_displayList" id="mobile_store_newBillingAddress">
        <h2>
          <span><fmt:message key="mobile.common.newBillingAddress"/></span>
        </h2>
        <div class="mobile_store_register">
          <dsp:form id="mobile_store_paymentInfoAddNewBillingAddressForm" formid="mobile_store_paymentInfoAddNewBillingAddressForm"
                    action="${originatingRequest.requestURI}" method="post">

            <dsp:input type="hidden" bean="MobileBillingFormHandler.createBillingAddressSuccessURL" value="billingCVV.jsp"/>
            <dsp:input type="hidden" bean="MobileBillingFormHandler.createBillingAddressErrorURL" value="billingAddressAdd.jsp?preFillValues=true"/>

            <%-- Coupon code --%>
            <dsp:getvalueof var="couponCode" bean="CouponFormHandler.currentCouponCode"/>
            <dsp:input bean="CouponFormHandler.couponCode" priority="10" type="hidden" id="atg_store_promotionCodeInput" value="${couponCode}"/>

            <ul class="mobile_store_mobileList">
              <%-- Start inclusion of addressAddEdit for rendering parameters of the form --%>
              <dsp:include page="/mobile/myaccount/gadgets/addressAddEdit.jsp">
                <dsp:param name="formHandlerComponent" value="/atg/store/mobile/order/purchase/MobileBillingFormHandler.creditCard.billingAddress"/>
                <dsp:param name="restrictionDroplet" value="/atg/store/droplet/ShippingRestrictionsDroplet"/>
                <dsp:param name="errorMap" value="${errorMap}"/>
              </dsp:include>

              <dsp:getvalueof var="transient" bean="Profile.transient"/>

              <%-- If the shopper is transient (a guest shopper) we don't offer to save the address.
                   Otherwise set saveBillingAddress to false to override default value of true --%>
              <c:choose>
                <c:when test="${not transient}">
                  <li class="clear">
                    <div class="content">
                      <dsp:input type="checkbox" bean="MobileBillingFormHandler.saveBillingAddress"
                                 checked="true" id="mobile_store_saveBillingAddress" name="saveBillingAddress"/>
                      <label for="mobile_store_saveBillingAddress" onclick="">
                        <fmt:message key="checkout_addressAdd.saveAddress"/>
                      </label>
                    </div>
                  </li>
                </c:when>
                <c:otherwise>
                  <dsp:input type="hidden" bean="MobileBillingFormHandler.saveBillingAddress"
                             id="mobile_store_saveBillingAddress" name="saveBillingAddress" value="false"/>
                </c:otherwise>
              </c:choose>
            </ul>
            <div class="mobile_store_formActions">
              <span class="mobile_store_basicButton">
                <fmt:message var="shipToButtonText" key="mobile.common.done"/>
                <dsp:input id="mobile_store_createShippingAddress" type="submit" bean="MobileBillingFormHandler.createBillingAddress"
                           value="${shipToButtonText}" class="mainActionBtn"/>
              </span>
            </div>
          </dsp:form>
        </div>
      </div>
    </jsp:body>
  </crs:mobilePageContainer>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/checkout/billingAddressAdd.jsp#4 $$Change: 692002 $--%>
