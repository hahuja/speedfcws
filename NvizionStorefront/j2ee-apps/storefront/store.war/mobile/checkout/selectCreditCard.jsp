<%--
  This page fragment renders list of saved credit cards

  Page includes:
    /mobile/checkout/gadgets/checkoutErrorMessages.jsp error messages renderer
    /mobile/checkout/gadgets/billingFormParams.jsp billing for parameters
    /mobile/checkout/gadgets/savedCreditCards.jsp list of saved credit cards

  Required parameters:
    None

  Optional parameters:
    None
--%>

<dsp:page>
  <dsp:importbean bean="/atg/store/mobile/order/purchase/MobileBillingFormHandler"/>
  <dsp:importbean bean="/atg/userprofiling/MobileB2CProfileFormHandler"/>
  <dsp:importbean bean="/atg/store/order/purchase/CouponFormHandler"/>

  <fmt:message key="checkout_billing.billingInformation" var="pageTitle"/>

  <crs:mobilePageContainer titleString="${pageTitle}">
    <jsp:body>
      <div class="mobile_store_displayList" id="mobile_store_selectPaymentMethod">
        <h2>
          <span><fmt:message key="mobile.checkout_confirmPaymentOptions.payment"/>:</span>
        </h2>
        <dsp:include page="/mobile/checkout/gadgets/checkoutErrorMessages.jsp">
          <dsp:param name="formHandler" bean="MobileBillingFormHandler"/>
        </dsp:include>
          <dsp:form id="mobile_store_checkoutBilling" formid="mobile_store_checkoutBilling"
                    action="${pageContext.request.requestURI}" method="post">
            <div class="mobile_store_chooseCreditCard">
              <dsp:include page="gadgets/billingFormParams.jsp" flush="true"/>
              
              <dsp:include page="gadgets/savedCreditCards.jsp" flush="false">
                <dsp:param name="areaPath" value="/mobile/checkout"/>
                <dsp:param name="selectable" value="true"/>
                <dsp:param name="displayDefaultLabeled" value="false"/>
                <dsp:param name="selectProperty" value="MobileBillingFormHandler.storedCreditCardName"/>
              </dsp:include>

              <%-- Include hidden form params --%>
              <dsp:input type="hidden" bean="MobileBillingFormHandler.selectCreditCardSuccessURL" value="billingCVV.jsp"/>
              <dsp:input type="hidden" bean="MobileBillingFormHandler.selectCreditCardErrorURL" value="billing.jsp"/>
              <dsp:input type="hidden" bean="MobileBillingFormHandler.selectCreditCard" value="selectCreditCard" priority="-10"/>

              <%-- Coupon code --%>
              <dsp:getvalueof var="couponCode" bean="CouponFormHandler.currentCouponCode"/>
              <dsp:input bean="CouponFormHandler.couponCode" priority="10" type="hidden" id="atg_store_promotionCodeInput" value="${couponCode}"/>

            </div>
          </dsp:form>
        <script type="text/javascript">
          $(document).ready(function() {
            $("#mobile_store_creditCardList").delayedSubmit();
          });
        </script>
      </div>
    </jsp:body>
  </crs:mobilePageContainer>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/checkout/selectCreditCard.jsp#3 $$Change: 692002 $--%>
