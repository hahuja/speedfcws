<%--
  This page fragment renders page to create new card

  Page includes:
    /mobile/checkout/gadgets/creditCardAddForm.jsp credit card add form renderer

  Required parameters:
    None

  Optional parameters:
    None
--%>

<dsp:page>
  <dsp:importbean bean="/atg/store/mobile/order/purchase/MobileBillingFormHandler"/>
  <dsp:importbean bean="/atg/store/order/purchase/CouponFormHandler"/>
  <dsp:importbean bean="/atg/commerce/order/purchase/RepriceOrderDroplet"/>

  <fmt:message key="myaccount_newCreditCard.title" var="pageTitle"/>

  <crs:mobilePageContainer titleString="${pageTitle}">
    <jsp:body>
	  <div class="mobile_store_displayList">
      <h2>
        <span><fmt:message key="checkout_billing.newCreditCard"/></span>
      </h2>

      <div class="mobile_store_register">
        <dsp:form id="mobile_store_paymentInfoAddNewCardForm" formid="mobile_store_paymentInfoAddNewCardForm"
                  action="${originatingRequest.requestURI}" method="post">

          <dsp:input type="hidden" bean="MobileBillingFormHandler.createCardSuccessURL" value="creditCardAddressSelect.jsp"/>
          <dsp:input type="hidden" bean="MobileBillingFormHandler.createCardErrorURL" value="newCreditCard.jsp?preFillValues=true"/>

          <%-- Coupon code --%>
          <dsp:getvalueof var="couponCode" bean="CouponFormHandler.currentCouponCode"/>
          <dsp:input bean="CouponFormHandler.couponCode" priority="10" type="hidden" id="atg_store_promotionCodeInput" value="${couponCode}"/>

          <%-- Empty credit card fields if we don't need to prefill them and they are could be already stored in order --%>
          <c:if test="${!param.preFillValues}">
            <dsp:setvalue bean="MobileBillingFormHandler.creditCard.expirationYear" value=""/>
            <dsp:setvalue bean="MobileBillingFormHandler.creditCard.expirationMonth" value=""/>
            <dsp:setvalue bean="MobileBillingFormHandler.creditCard.creditCardType" value=""/>
            <dsp:setvalue bean="MobileBillingFormHandler.creditCard.creditCardNumber" value=""/>
          </c:if>

          <%--
            If there are errors during form submition the current order is invalidated and doesn't contain priceInfo.
            Will reprice whole order in this case
          --%>
          <dsp:getvalueof var="formExceptions" vartype="java.lang.Object" bean="MobileBillingFormHandler.formExceptions"/>
          <c:if test="${not empty formExceptions}">
            <dsp:droplet name="RepriceOrderDroplet">
              <dsp:param name="pricingOp" value="ORDER_TOTAL"/>
            </dsp:droplet>
          </c:if>

          <dsp:include page="gadgets/creditCardAddForm.jsp" flush="false">
            <dsp:param name="formHandler" value="/atg/store/mobile/order/purchase/MobileBillingFormHandler"/>
            <dsp:param name="cardParamsMap" value="/atg/store/mobile/order/purchase/MobileBillingFormHandler.creditCard"/>
            <dsp:param name="saveCard" value="true"/>
          </dsp:include>
        </dsp:form>
      </div>

    </div>
	</jsp:body>
  </crs:mobilePageContainer>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/checkout/newCreditCard.jsp#3 $$Change: 692002 $--%>
