<%--
  This page renders the form that allow the shopper to add a new shipping address.

  Page includes:
    /mobile/myaccount/gadgets/addressAddEdit.jsp renderer of address form

  Required parameters:
    selectedAddress
      nickname of selected address

  Optional parameters:
    None
--%>
<dsp:page>
  <dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupFormHandler"/>
  <dsp:importbean bean="/atg/userprofiling/Profile"/>
  <dsp:importbean bean="/atg/userprofiling/B2CProfileFormHandler"/>

  <fmt:message key="common.foot.shipping" var="pageTitle"/>
  <crs:mobilePageContainer titleString="${pageTitle}">
    <jsp:attribute name="modalContent">
      <div class="mobile_store_moveDialog" id="removeItemDialog">
        <div class="mobile_store_moveItems">
          <ul>
            <li class="remove">
              <fmt:message var="removeAddressTitle" key="mobile.checkout_shippingAddressEdit.deleteText"/>
              <dsp:getvalueof var="shippingGroup" param="nickName"/>
              <dsp:a title="${removeAddressTitle}" bean="B2CProfileFormHandler.removeAddress"
                     href="shipping.jsp" value="${shippingGroup}">
                <fmt:message key="common.button.deleteText"/>
              </dsp:a>
            </li>
          </ul>
        </div>
      </div>
    </jsp:attribute>

    <jsp:body>
      <dsp:getvalueof var="addressPropertyNameMap" vartype="java.util.Map" bean="ShippingGroupFormHandler.shippingHelper.addressPropertyNameMap"/>
      <dsp:getvalueof var="nickName" vartype="java.lang.String" param="nickName"/>
      <dsp:getvalueof var="selectedAddress" vartype="java.lang.String" param="selectedAddress"/>

      <%-- Handle form exceptions --%>
      <dsp:getvalueof var="formExceptions" vartype="java.lang.Object" bean="ShippingGroupFormHandler.formExceptions"/>
      <jsp:useBean id="errorMap" class="java.util.HashMap"/>
      <c:if test="${not empty formExceptions}">
        <c:forEach var="formException" items="${formExceptions}">
          <dsp:param name="formException" value="${formException}"/>
          <%-- Check the error message code to see what we should do --%>
          <dsp:getvalueof var="errorCode" vartype="java.lang.String" param="formException.errorCode"/>
          <dsp:getvalueof var="propertyPath" vartype="java.lang.String" param="formException.propertyPath"/>

          <c:choose>
            <c:when test="${'duplicateNickname' eq errorCode}">
              <c:set target="${errorMap}" property="nickName" value="inUse"/>
            </c:when>
            <c:when test="${'stateIsIncorrect' eq propertyPath}">
              <c:set target="${errorMap}" property="state" value="invalid"/>
              <c:set target="${errorMap}" property="country" value="invalid"/>
            </c:when>
            <c:when test="${'missingRequiredValue' eq errorCode}">
              <dsp:getvalueof var="propertyName" param="formException.propertyName"/>
              <c:if test="${'shippingAddressNewNickName' eq propertyName}">
                <c:set var="propertyName" value="nickName"/>
              </c:if>
              <c:set target="${errorMap}" property="${propertyName}" value="missing"/>
            </c:when>
          </c:choose>
        </c:forEach>
      </c:if>

      <c:if test="${not empty nickName}">
        <dsp:setvalue bean="ShippingGroupFormHandler.editShippingAddressNickName" paramvalue="nickName"/>
        <dsp:setvalue bean="ShippingGroupFormHandler.shippingAddressNewNickName" paramvalue="nickName"/>
      </c:if>
      <dsp:setvalue bean="ShippingGroupFormHandler.initEditAddressForm" value=""/>

      <div class="mobile_store_displayList" id="mobile_store_shippingAddressEdit">
        <div class="mobile_store_checkoutHeader">
          <h2><fmt:message key="checkout_shippingAddressEdit.title"/></h2>
        </div>

        <dsp:form action="${pageContext.request.requestURI}" method="post" formid="shippingaddresseditform">
          <dsp:input type="hidden" bean="ShippingGroupFormHandler.editShippingAddressSuccessURL" value="shipping.jsp"/>
          <dsp:input type="hidden" bean="ShippingGroupFormHandler.editShippingAddressErrorURL"
                     value="${pageContext.request.requestURI}?nickName=${nickName}&amp;selectedAddress=${selectedAddress}"/>
          <dsp:input type="hidden" bean="ShippingGroupFormHandler.cancelURL" value="shipping.jsp"/>

          <dsp:input type="hidden" bean="ShippingGroupFormHandler.shipToAddressName" paramvalue="selectedAddress"/>

          <div class="mobile_store_editShippingAddress">
            <ul class="mobile_store_mobileList">
              <%-- Check for errors in "Nick Name" --%>
              <c:if test="${not empty errorMap['nickName']}">
                <c:set var="liClassNick" value="mobile_store_error_state"/>
              </c:if>
              <li class="${liClassNick}">
                <div class="content">
                  <dsp:input type="hidden" bean="ShippingGroupFormHandler.editShippingAddressNickName"/>
                  <dsp:input type="text" bean="ShippingGroupFormHandler.shippingAddressNewNickName"
                             iclass="mobile_store_textBoxForm"
                             maxlength="42" required="true" id="mobile_store_addressNameInput">
                    <fmt:message var="nickPlace" key="common.addressNickname"/>
                    <dsp:tagAttribute name="placeholder" value="${nickPlace}"/>
                  </dsp:input>
                </div>
                <c:if test="${not empty errorMap['nickName']}">
                  <span class="errorMessage">
                    <fmt:message key="mobile.form.validation.${errorMap['nickName']}"/>
                  </span>
                </c:if>
              </li>

              <%-- Start inclusion of addressAddEdit for rendering parameters of the form --%>
              <dsp:include page="/mobile/myaccount/gadgets/addressAddEdit.jsp">
                <dsp:param name="formHandlerComponent" value="/atg/commerce/order/purchase/ShippingGroupFormHandler.editAddress"/>
                <dsp:param name="restrictionDroplet" value="/atg/store/droplet/ShippingRestrictionsDroplet"/>
                <dsp:param name="errorMap" value="${errorMap}"/>
              </dsp:include>

              <dsp:getvalueof var="transient" bean="Profile.transient"/>
              <c:if test="${not transient}">
                <li>
                  <dsp:getvalueof var="defaultAddressId" bean="Profile.shippingAddress.repositoryId"/>
                  <dsp:getvalueof var="secondaryAddresses" bean="Profile.secondaryAddresses"/>
                  <c:set var="currentAddressId" value="${secondaryAddresses[nickName].repositoryId}"/>
                  <div class="content">
                    <dsp:input type="checkbox" name="useShippingAddressAsDefault" id="addresses_useShippingAddressAsDefault"
                               bean="/atg/userprofiling/B2CProfileFormHandler.useShippingAddressAsDefault" checked="${defaultAddressId == currentAddressId}"/>
                    <label for="addresses_useShippingAddressAsDefault" onclick="">
                      <fmt:message key="mobile.myaccount_addresses.default"/>
                    </label>
                  </div>
                </li>
              </c:if>

              <li>
                <div class="deleteContainer">
                  <fmt:message key="mobile.checkout_shippingAddressEdit.deleteText" var="deleteText"/>
                  <div id="mobile_store_removeAddress" onclick="removeItemDialog($(this).parent());" title="${deleteText}">
                    ${deleteText}
                  </div>
                </div>
              </li>

            </ul>

            <div class="mobile_store_formActions">
              <span class="mobile_store_basicButton">
                <fmt:message var="saveButtonText" key="mobile.common.done"/>
                <dsp:input id="mobile_store_saveShippingAddress" iclass="mainActionBtn" type="submit"
                           bean="ShippingGroupFormHandler.editShippingAddress" value="${saveButtonText}"/>
              </span>
            </div>
          </div>
        </dsp:form>
      </div>
    </jsp:body>
  </crs:mobilePageContainer>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/checkout/shippingAddressEdit.jsp#4 $$Change: 692002 $--%>