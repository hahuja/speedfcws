<%-- 
  This container page contains hidden input param for shipping info form

  Page includes:
    None

  Required parameters:
    None

  Optional parameters:
    None
--%>

<dsp:page>
  <dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupFormHandler"/>
  <dsp:importbean bean="/atg/userprofiling/Profile"/>
  <dsp:importbean bean="/atg/store/droplet/ProfileSecurityStatus"/>
  <dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>

  <%--
      If user is anonymous and the session has expired, the cart looses its contents,
      so the page gets redirected to the home page else it will be redirected to the
      checkout login page.
     --%>
  <dsp:droplet name="ProfileSecurityStatus">
    <dsp:oparam name="anonymous">
      <%-- User is anonymous --%>
      <dsp:input type="hidden"
                 bean="ShippingGroupFormHandler.sessionExpirationURL"
                 value="${originatingRequest.contextPath}/index.jsp"/>
    </dsp:oparam>
    <dsp:oparam name="default">
      <dsp:input type="hidden"
                 bean="ShippingGroupFormHandler.sessionExpirationURL"
                 value="${originatingRequest.contextPath}/checkout/login.jsp"/>
    </dsp:oparam>
  </dsp:droplet>

  <dsp:input type="hidden" bean="ShippingGroupFormHandler.address.email" beanvalue="Profile.email"/>

  <%-- Redirection URLs --%>
  <dsp:input type="hidden" bean="ShippingGroupFormHandler.shipToExistingAddressSuccessURL" value="shippingMethod.jsp"/>
  <dsp:input type="hidden" bean="ShippingGroupFormHandler.shipToExistingAddressErrorURL" value="shipping.jsp"/>

  <dsp:input type="hidden" bean="ShippingGroupFormHandler.updateShippingMethodSuccessURL" value="billing.jsp"/>
  <dsp:input type="hidden" bean="ShippingGroupFormHandler.updateShippingMethodErrorURL" value="shippingMethod.jsp"/>

  <dsp:input type="hidden" bean="ShippingGroupFormHandler.shipToNewAddressSuccessURL" value="shippingMethod.jsp"/>
  <dsp:input type="hidden" bean="ShippingGroupFormHandler.shipToNewAddressErrorURL" value="shippingAddressAdd.jsp"/>

</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/checkout/shippingFormParams.jsp#3 $$Change: 692002 $--%>