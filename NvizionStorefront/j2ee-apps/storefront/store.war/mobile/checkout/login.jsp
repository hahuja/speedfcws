<%--
  This page includes a gadget that will check if the user is already logged in.
  If so, it redirects to the shipping page

  Page includes:
    /mobile/global/login.jsp login form renderer

  Required parameters:
    None

  Optional parameters:
    passwordSent
      if returning from the passwordReset.jsp, set this variable to true
  --%>

<dsp:page>
  <dsp:importbean bean="/atg/dynamo/droplet/Compare"/>
  <dsp:importbean bean="/atg/userprofiling/Profile"/>
  <dsp:importbean bean="/atg/userprofiling/PropertyManager"/>
  
  <dsp:getvalueof var="passwordSent" param="passwordSent"/>

  <dsp:droplet name="Compare">
    <dsp:param bean="Profile.securityStatus" name="obj1"/>
    <dsp:param bean="PropertyManager.securityStatusLogin" name="obj2"/>
    <dsp:oparam name="equal">
      <dsp:include page="shipping.jsp"/>
    </dsp:oparam>
    <dsp:oparam name="default">
      <dsp:include page="../global/login.jsp">
        <dsp:param name="checkoutLogin" value="true"/>
        <c:if test="${passwordSent == 'true'}">
          <dsp:param name="passwordSent" value="${passwordSent}"/>
        </c:if>
      </dsp:include>
    </dsp:oparam>
  </dsp:droplet>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/checkout/login.jsp#3 $$Change: 692002 $--%>
