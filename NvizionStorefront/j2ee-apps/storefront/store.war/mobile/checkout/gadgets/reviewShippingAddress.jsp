<%-- 
  Renders order's shipping into info on review page.

  Page includes:
    /mobile/global/util/displayAddress.jsp renderer of address info

  Required parameters:
    order
      order which payment data need to be displayed
    address
      address item to be displayed

  Optional parameters:
    isCheckout
      indicator if this order is about to checkout or has been already placed
      Possible values: true - is about to be checkout; false - placed order
--%>

<dsp:page>

  <dsp:getvalueof var="isCheckout" param="isCheckout" />
  <fmt:message var="title" key="checkout_confirmPaymentOptions.shipTo"/>
  <div class="mobile_store_shipping">
    <h2>
      <c:choose>
        <c:when test="${isCheckout}">
          <c:set var="linkId" value="editShippingAddress"/>
          <dsp:a id="${linkId}" page="../../checkout/shipping.jsp">${title}</dsp:a>
        </c:when>
        <c:otherwise>
          ${title}
        </c:otherwise>
      </c:choose>
    </h2>
    
    
    <div class="mobile_store_detail" onclick="editReviewOrderBlock('${linkId}');">
      <dsp:include page="../../global/util/displayAddress.jsp">
        <dsp:param name="address" param="address"/>
      </dsp:include>
      <c:if test="${isCheckout}">
      	<dsp:a class="withArrow" page="../../checkout/shipping.jsp" title="${title}"></dsp:a>
      </c:if>
    </div>
  </div>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/checkout/gadgets/reviewShippingAddress.jsp#3 $$Change: 692002 $--%>
