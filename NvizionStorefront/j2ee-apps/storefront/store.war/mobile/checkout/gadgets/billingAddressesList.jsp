<dsp:page>
<%--
  This page renders list of billing addresses

  Page includes:
    /mobile/global/util/displayAddress.jsp renderer of address info

  Required parameters:
    selectedBillingAddressId
      actual only in internal mode; id of address that should be marked as selected one

  Optional parameters:
    paramMode
      display mode: internal (used on edit credit card page) or global one
      Possible values: [internal|*]
--%>
  <dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupContainerService"/>
  <dsp:importbean bean="/atg/store/droplet/AvailableBillingAddresses"/>
  <dsp:importbean bean="/atg/store/mobile/order/purchase/MobileBillingFormHandler"/>
  <dsp:importbean bean="/atg/userprofiling/MobileB2CProfileFormHandler"/>
  <dsp:importbean bean="/atg/userprofiling/Profile"/>

  <%-- Get mode (internal block inside form/main form on the page) that this page is invoked with --%>
  <dsp:getvalueof var="paramMode" param="mode"/>
  <dsp:getvalueof var="selectedBillingAddressId" param="selectedBillingAddressId"/>

  <dsp:getvalueof var="shippingGroupMap" vartype="java.lang.Object" bean="ShippingGroupContainerService.shippingGroupMap"/>
  <c:if test="${not empty shippingGroupMap}">
    <dsp:droplet name="AvailableBillingAddresses">
      <dsp:param name="map" value="${shippingGroupMap}"/>
      <dsp:param name="defaultId" bean="Profile.shippingAddress.repositoryId"/>
      <dsp:param name="sortByKeys" value="true"/>
      <dsp:oparam name="output">
        <dsp:getvalueof var="permittedAddresses" vartype="java.lang.Object" param="permittedAddresses"/>
      </dsp:oparam>
    </dsp:droplet>
  </c:if>

  <c:if test="${not empty permittedAddresses}">
    <c:choose>
      <c:when test="${paramMode == 'internal'}">
        <dsp:input id="editBillAddressKey" bean="MobileB2CProfileFormHandler.editBillAddress" type="hidden"/>
        <dsp:input id="createNewAddress" bean="MobileB2CProfileFormHandler.createNewAddress" type="hidden"/>

        <c:set var="ulClass" value="internalBillingAddresses"/>
      </c:when>
      <c:otherwise>
        <c:set var="ulClass" value="mobile_store_mobileList"/>
      </c:otherwise>
    </c:choose>

    <ul id="mobile_store_savedBillingAddresses" class="${ulClass}">

      <c:forEach var="billingAddress" items="${permittedAddresses}">
        <li onclick="" class="mobile_store_address">
          <dsp:getvalueof var="shippingGroupAddress" value="${billingAddress.value.shippingAddress}"/>
          <dsp:getvalueof var="shippingGroupNickname" value="${billingAddress.key}"/>
          <div class="layout">
            <div class="content">
              <dsp:input type="radio" name="address" value="${shippingGroupNickname}"
                         id="${shippingGroupAddress.repositoryItem.repositoryId}"
                         checked="${not empty selectedBillingAddressId and selectedBillingAddressId == shippingGroupAddress.repositoryItem.repositoryId}"
                         bean="MobileBillingFormHandler.storedAddressSelection"/>
              <label for="${shippingGroupAddress.repositoryItem.repositoryId}">
                <dsp:include page="/mobile/global/util/displayAddress.jsp" flush="false">
                  <dsp:param name="address" value="${shippingGroupAddress}"/>
                  <dsp:param name="private" value="false"/>
                </dsp:include>
              </label>
            </div>

            <%-- Display Edit Link --%>
            <span class="mobile_store_storedAddressActions">
              <fmt:message var="accountAddressEditTitle" key="myaccount_accountAddressEdit.title"/>
              <c:choose>
                <c:when test="${paramMode == 'internal'}">
                  <a href="javascript:void(0);" title="${accountAddressEditTitle}" onClick="creditCardSelectAddressToEdit('${shippingAddress.key}');"></a>
                </c:when>
                <c:otherwise>
                  <dsp:a title="${accountAddressEditTitle}"
                         page="../billingAddressEdit.jsp" value="${shippingGroupNickname}">
						        <dsp:param name="nickName" value="${shippingGroupNickname}"/>
                  </dsp:a>
                </c:otherwise>
              </c:choose>
            </span>
          </div>
        </li>
        <c:if test="${paramMode == 'internal'}">
          <li class="mobile_store_border mobile_store_shadowedBorder"/>
        </c:if>
      </c:forEach>

    <%-- Create new shipping address page --%>
    <li class="mobile_store_formActions">
      <div class="content">
        <c:choose>
          <c:when test="${paramMode == 'internal'}">
            <a href="javascript:void(0);" onclick="creditCardCreateNewAddress()">
              <fmt:message key="mobile.common.newBillingAddress"/>
            </a>
          </c:when>
          <c:otherwise>
            <dsp:a page="../billingAddressAdd.jsp?init=true" iclass="listLink">
              <fmt:message key="mobile.common.newBillingAddress"/>
            </dsp:a>
          </c:otherwise>
        </c:choose>
      </div>
    </li>

    </ul>

    <c:choose>
      <c:when test="${paramMode == 'internal'}">
        <script type="text/javascript">
          $(document).ready(function() {
            $("#mobile_store_savedBillingAddresses").dropdownList();
          });
        </script>
      </c:when>
    </c:choose>
  </c:if>

</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/checkout/gadgets/billingAddressesList.jsp#3 $$Change: 692002 $--%>
