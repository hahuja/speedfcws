<%--
  This page displays Shipping Rates of the Merchant

  Page includes:
    None

  Required Parameters:
    None

  Optional Parameters:
    None
--%>
<dsp:page>
  <fmt:message key="mobile.company_shippingAndReturns.pageTitle" var="pageTitle" />
  <crs:mobilePageContainer titleString="${pageTitle}">
    <jsp:body>
      <div class="mobile_store_infoHeader">
        <fmt:message key="company_shipping.title"/>
      </div>
      <div class="mobile_store_infoContent">
        <fmt:message key="mobile.company_shippingAndReturns.text"/>
      </div>
    </jsp:body>
  </crs:mobilePageContainer>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/company/shipping.jsp#3 $$Change: 692002 $ --%>