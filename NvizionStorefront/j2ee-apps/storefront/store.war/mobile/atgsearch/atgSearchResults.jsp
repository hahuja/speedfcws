<%--
  Search results page

  Page includes:
    /mobile/atgsearch/gadgets/search.jspf search execution
    /mobile/atgsearch/gadgets/refinements.jsp renderer of search refinements subheader
    /mobile/atgsearch/gadgets/searchResultsRenderer.jsp search result renderer

  Required parameters:
    q
      the executed search query
    token
      the token for the executed query

  Optional Parameters:
    facetTrail
      specifies the current facet trail. If present, the search will be resubmitted
--%>
<dsp:page>

  <dsp:importbean bean="/atg/commerce/search/catalog/QueryFormHandler" var="qfhbean"/>
  <dsp:importbean bean="/atg/search/repository/FacetSearchTools"/>
  <dsp:importbean bean="/atg/targeting/TargetingFirst"/>
  <dsp:importbean bean="/atg/multisite/Site"/>

  <fmt:message key="search_searchResults.title" var="pageTitle"/>  

	<crs:mobilePageContainer titleString="${pageTitle}">
		<jsp:body>
			
			<script>
			  $(document).ready(initFacets);
			</script>
			
			<dsp:getvalueof var="query" param="q"/>
			<dsp:getvalueof var="token" param="token"/>
			
			<%-- The facet trail is of the form <facet>:<value>[|<value> ...]:...
			  Ex. Selecting Category>Men, Color>Beige, and Color>Blue yields 9004:catMens:1002:Beige|Blue
			 --%>
      <dsp:getvalueof var="searchExec" bean="QueryFormHandler.initialSearch"/>
			<dsp:getvalueof var="facetTrail" param="facetTrail"/>

            <%-- If this is a faceted request or search hasn't been executed in this request (refresh), we will need to resubmit the search --%>
            <c:if test="${(!empty facetTrail) || (searchExec ne true && not empty query)}">
			
              <%-- We need to first grab the available facets from the raw search --%>
              <dsp:setvalue bean="QueryFormHandler.searchRequest.pageSize" value="${pageSize}"/>
              <dsp:setvalue bean="QueryFormHandler.searchRequest.question" value="${query}"/>
              <dsp:setvalue bean="QueryFormHandler.searchRequest.multiSearchSession" value="true"/>
              <dsp:setvalue bean="QueryFormHandler.searchRequest.saveRequest" value="${true}"/>
              <dsp:setvalue bean="QueryFormHandler.searchRequest.facetSearchRequest" value="${false}"/>
              <dsp:setvalue bean="QueryFormHandler.searchRequest.requestChainToken" value="${token}"/>
              <dsp:setvalue bean="QueryFormHandler.goToPage" value="${pageNum}" />
				
              <%-- Commerce Root should always be on the Facet Trail. Otherwise, we will see it as an available facet value --%>
              <dsp:getvalueof var="rootCategoryId" bean="Site.defaultCatalog.rootNavigationCategory.id"/>
              <dsp:droplet name="TargetingFirst">
		        <dsp:param name="howMany" value="1" />
		        <dsp:param name="targeter" 
		                 bean="/atg/registry/RepositoryTargeters/RefinementRepository/GlobalCategoryFacet" />
                <dsp:oparam name="output">
                  <dsp:getvalueof id="refElemRepId" idtype="String" param="element.repositoryId" />
                  <dsp:setvalue bean="FacetSearchTools.facetTrail" value="${refElemRepId}:${rootCategoryId}"/>
                </dsp:oparam>
              </dsp:droplet>
		
              <%-- Execute the search --%>
              <dsp:setvalue bean="QueryFormHandler.search" value="get facets"/>
				
              <%-- Get the returned facets --%>
              <dsp:getvalueof var="facetHolders" bean="FacetSearchTools.facets"/>
				
              <%-- Now execute the actual search, using the facets specified on the facetTrail param --%>
			  <%@include file="gadgets/search.jspf"%>
			
		      <dsp:getvalueof var="token" bean="QueryFormHandler.searchResponse.requestChainToken" />
			</c:if>
			
			<c:if test="${empty facetHolders}"> <%-- No facet trail present, use the current facets --%>
			  <dsp:getvalueof var="facetHolders" bean="FacetSearchTools.facets"/>
			</c:if>
			
			<%-- Don't show subheader if there are no results --%>
			<c:if test="${!empty query && qfhbean.searchResponse.groupCount > 0}">
				<%-- search subheader + facets--%>
				<dsp:include page="/mobile/atgsearch/gadgets/refinements.jsp">
				  <dsp:param name="token" param="token"/>
				  <dsp:param name="facetHolders" value="${facetHolders}"/>
				</dsp:include>
		    </c:if>
		
			<%-- search results --%>
			<div id="mobile_store_results">
			  <ul class="searchResults">
		        <dsp:include page="gadgets/searchResultsRenderer.jsp">
		          <dsp:param name="token" param="token"/>
		        </dsp:include>
              </ul>
			</div>
		</jsp:body>
	</crs:mobilePageContainer>
</dsp:page>

<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/atgsearch/atgSearchResults.jsp#3 $$Change: 692002 $ --%>
