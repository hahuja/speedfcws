<%-- 
  Renders a "Show More" entry at the bottom of search results.
  Calculates how many more items should be shown. If there aren't
  any more results, displays nothing.

  Required parameters:
    pageNum
      the current page number
    pageSize
      the size of a page of search results
    token
      the token for the previously-executed search

  Optional parameters:
    None
 --%>
<dsp:page>
  <dsp:importbean bean="/atg/commerce/search/catalog/QueryFormHandler"/>

  <dsp:getvalueof var="pageNum" param="pageNum"/>
  <dsp:getvalueof var="pageSize" vartype="java.lang.Integer" param="pageSize"/>
  <dsp:getvalueof var="token" param="token"/>

  <%-- Determine whether there are more results to display and, if so, how many --%>
	<dsp:droplet name="/atg/dynamo/droplet/Compare">
		<dsp:param name="obj1" value="${pageNum * pageSize}"/>
		<dsp:param name="obj2" bean="QueryFormHandler.searchResponse.groupCount"/>
		<dsp:oparam name="lessthan">
		  <dsp:getvalueof var="numResults" bean="QueryFormHandler.searchResponse.groupCount"/>
		  <dsp:getvalueof var="remaining" value="${(numResults - (pageNum * pageSize))}"/>
		  <c:choose>
		    <c:when test="${remaining < pageSize}">
		      <%-- There is less than a full page of results remaining --%>
		      <dsp:getvalueof var="remainingResults" value="${remaining}"/>
		    </c:when>
		    <c:otherwise>
		      <dsp:getvalueof var="remainingResults" value="${pageSize}"/>
		    </c:otherwise>
		  </c:choose>
		  <%-- Escape XML specific characters in search term to prevent using it for XSS attacks. --%>
			<li class="moreResults" onclick="loadPageFull(this,${fn:escapeXml(pageNum + 1)},'${fn:escapeXml(token)}');">
				<span>
					<fmt:message key="mobile.search.showMore">
					  <fmt:param value="${remainingResults}"/>
					</fmt:message>
			  </span>
			</li>
	  </dsp:oparam>
	</dsp:droplet>
</dsp:page>

<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/atgsearch/gadgets/showMore.jsp#3 $$Change: 692002 $ --%>