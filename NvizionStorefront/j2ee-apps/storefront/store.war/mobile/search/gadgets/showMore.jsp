<%--
  Don't show a "More" button if there are no more search results

  Required parameters:
    None

  Optional parameters:
    None
--%>
<dsp:page>

  <dsp:importbean bean="/atg/commerce/catalog/ProductSearch"/>

  <dsp:getvalueof bean="ProductSearch.resultsPerPage" var="pageSize"/>

	<dsp:importbean bean="/atg/commerce/catalog/ProductSearch"/>
	<dsp:getvalueof bean="ProductSearch.currentPage" var="pageNum"/>
	<dsp:droplet name="/atg/dynamo/droplet/Compare">
		<dsp:param name="obj1" value="${pageNum}"/>
		<dsp:param name="obj2" bean="ProductSearch.resultPageCount"/>
		<dsp:oparam name="lessthan">
		  <dsp:getvalueof var="numResults" bean="ProductSearch.searchResultsSize"/>
		  <dsp:getvalueof var="remaining" value="${(numResults - (pageNum * pageSize))}"/>
		  <c:choose>
		    <c:when test="${remaining < pageSize}">
		      <%-- There is less than a full page of results remaining --%>
		      <dsp:getvalueof var="remainingResults" value="${remaining}"/>
		    </c:when>
		    <c:otherwise>
		      <dsp:getvalueof var="remainingResults" value="${pageSize}"/>
		    </c:otherwise>
		  </c:choose>
			<li class="moreResults" onclick="loadPageSimple(this,${pageNum + 1});">
				<span>
					<fmt:message key="mobile.search.showMore">
			    <fmt:param value="${remainingResults}"/>
			    </fmt:message>
			  </span>
			</li>
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/search/gadgets/showMore.jsp#3 $$Change: 692002 $--%>
