<%--
  This JSP renders the search results and the Featured Search form

  Required parameters:
    None

  Optional parameters:
    None
--%>
<dsp:page>

  <fmt:message key="search_searchResults.title" var="pageTitle"/>

  <crs:mobilePageContainer titleString="${pageTitle}">
    <jsp:body>
	    <dsp:importbean bean="/atg/commerce/catalog/ProductSearch"/>
	    <dsp:importbean bean="/atg/dynamo/droplet/For"/>
	    <dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
	
	    <div id="mobile_store_refine_header">
				<dsp:getvalueof bean="ProductSearch.searchResultsSize" var="totalCountVar"/>
        <c:if test="${empty totalCountVar}"><c:set var="totalCountVar" value="0"/></c:if>
        <button id="mobile_store_toggleSearchResults" disabled>
           <fmt:message key="mobile.search.results">
             <fmt:param value="${totalCountVar}"/>
           </fmt:message>
        </button>
    	</div>

      <div id="mobile_store_results">
				<ul class="searchResults">
					<dsp:droplet name="ForEach">
						<dsp:param name="array" bean="ProductSearch.searchResults"/>
						<dsp:oparam name="output">
						<li>
							<%-- Pass off to the same jsp used by other pages for pricing items --%>
							<dsp:getvalueof var="mobileStorePrefix" bean="/atg/store/StoreConfiguration.mobileStorePrefix"/>
							<dsp:include page="${mobileStorePrefix}/browse/gadgets/productListRow.jsp" flush="true">
							 <dsp:param name="product" param="element"/>
							</dsp:include>
							<div class="shadow">
								&nbsp;
							</div>
						</li>
						</dsp:oparam>
						<dsp:oparam name="empty">
							<li>
							  <h4 class="mobile_store_pageHeader"><fmt:message key="mobile.search.refine.noResults"/></h4>
							</li>
							<li></li>
							<li></li>
							<li></li>
						</dsp:oparam>
						<dsp:oparam name="outputEnd">
						    <dsp:getvalueof var="numResults" param="count"/>
						    <%-- Add extra padding to the results page if there are < 4 results --%>
						    <c:if test="${(numResults < 4)}">
						      <dsp:droplet name="For">
				            <dsp:param name="howMany" value="${4 - numResults}"/>
				            <dsp:oparam name="output">
				              <li></li>
				            </dsp:oparam>
						      </dsp:droplet>
						    </c:if>
								<dsp:include page="/mobile/search/gadgets/showMore.jsp" />
						</dsp:oparam>
					</dsp:droplet>
			  </ul>
		  </div>
		</jsp:body>
	</crs:mobilePageContainer>
</dsp:page>

<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/search/searchResults.jsp#3 $$Change: 692002 $--%>