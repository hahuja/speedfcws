<%--
  Return products' details from the targeters

  Page includes:
    /mobile/promo/gadgets/promotionalTargeterJSON.jsp targeter wrapper

  Required parameters:
    None

  Optional parameters:
    None
--%>
<%@page contentType="application/json"%>
<%@page trimDirectiveWhitespaces="true"%>

<dsp:page>
	<c:set var="atg.taglib.json.escapeXml" value="${false}" scope="page" />
	<json:object>
		<json:array name="targeterResults">
			<dsp:getvalueof var="mobileStorePrefix"
				bean="/atg/store/StoreConfiguration.mobileStorePrefix" />
			<dsp:include
				page="${mobileStorePrefix}/promo/gadgets/promotionalTargeterJSON.jsp">
				<dsp:param name="targeterPath"
					value="/atg/registry/RepositoryTargeters/ProductCatalog/MobilePromotionChild1" />
			</dsp:include>
			<dsp:include
				page="${mobileStorePrefix}/promo/gadgets/promotionalTargeterJSON.jsp">
				<dsp:param name="targeterPath"
					value="/atg/registry/RepositoryTargeters/ProductCatalog/MobilePromotionChild2" />
			</dsp:include>
			<dsp:include
				page="${mobileStorePrefix}/promo/gadgets/promotionalTargeterJSON.jsp">
				<dsp:param name="targeterPath"
					value="/atg/registry/RepositoryTargeters/ProductCatalog/MobilePromotionChild3" />
			</dsp:include>
		</json:array>
	</json:object>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/promo/gadgets/homePromotionalProductsJSON.jsp#3 $$Change: 692002 $--%>
