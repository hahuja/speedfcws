<%--
  Renders products if Rec isn't installed

  Page includes:
    /mobile/promo/gadgets/promotionalTargeterJSON.jsp targeter wrapper

  Required parameters:
    None

  Optional parameters:
    None
--%>
<%@page contentType="application/json"%>
<%@page trimDirectiveWhitespaces="true"%>

<dsp:page>
  <dsp:importbean bean="/atg/targeting/TargetingRandom" />
    <dsp:getvalueof var="mobileStorePrefix"
    bean="/atg/store/StoreConfiguration.mobileStorePrefix" />
	<c:set var="atg.taglib.json.escapeXml" value="${false}" scope="page" />
	<json:object>
		<json:array name="targeterResults">
			<json:object>
				<json:array name="products">
					<dsp:droplet name="TargetingRandom" var="myParams">
						<dsp:param bean="/atg/registry/Slots/FeaturedProduct1"
							name="targeter" />
						<dsp:param name="fireViewItemEvent" value="false" />
						<dsp:param name="howMany" value="1" />
						<dsp:oparam name="output">
							<dsp:include
								page="${mobileStorePrefix}/promo/gadgets/promotionalProductsJSON.jsp">
								<dsp:param name="product" param="element" />
								<dsp:param name="productParams" converter="map"
									value="${myParams}" />
							</dsp:include>
						</dsp:oparam>
					</dsp:droplet>
					<dsp:droplet name="TargetingRandom" var="myParams">
						<dsp:param bean="/atg/registry/Slots/FeaturedProduct2"
							name="targeter" />
						<dsp:param name="fireViewItemEvent" value="false" />
						<dsp:param name="howMany" value="1" />
						<dsp:oparam name="output">
							<dsp:include
								page="${mobileStorePrefix}/promo/gadgets/promotionalProductsJSON.jsp">
								<dsp:param name="product" param="element" />
								<dsp:param name="productParams" converter="map"
									value="${myParams}" />
							</dsp:include>
						</dsp:oparam>
					</dsp:droplet>
					<dsp:droplet name="TargetingRandom" var="myParams">
						<dsp:param bean="/atg/registry/Slots/FeaturedProduct3"
							name="targeter" />
						<dsp:param name="fireViewItemEvent" value="false" />
						<dsp:param name="howMany" value="1" />
						<dsp:oparam name="output">
							<dsp:include
								page="${mobileStorePrefix}/promo/gadgets/promotionalProductsJSON.jsp">
								<dsp:param name="product" param="element" />
								<dsp:param name="productParams" converter="map"
									value="${myParams}" />
							</dsp:include>
						</dsp:oparam>
					</dsp:droplet>
				</json:array>
			</json:object>
		</json:array>
	</json:object>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/promo/gadgets/homeNoRecsProductsJSON.jsp#3 $$Change: 692002 $--%>
