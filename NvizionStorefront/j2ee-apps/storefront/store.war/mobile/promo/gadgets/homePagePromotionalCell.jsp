<%--
  Render of promotional item image

  Required Parameters:
    promotionalContent
      prmotional item
    childTargeter
      used targeter

  Optional Parameters:
    None
--%>
<dsp:page>
	<dsp:getvalueof var="promotionalContent" param="promotionalContent" />
	<dsp:getvalueof var="childTargeter" param="childTargeter" />
	<div class='cell'>
		<div class='mobile_store_homePromotionalWrap'>
			<img src="${promotionalContent.derivedImage}" alt="${promotionalContent.displayName}" class="mobile_store_homePromotionalImage" style="background-image:url(${promotionalContent.description})"/>
			<input type="hidden" class="targeterPath" value="${childTargeter}"/>
		</div>
	</div>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/promo/gadgets/homePagePromotionalCell.jsp#3 $$Change: 692002 $--%>
