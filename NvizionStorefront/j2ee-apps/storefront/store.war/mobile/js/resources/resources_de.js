/* Provides format strings for localization of page text.

Strings should be added to the RESOURCES object in the format:

  '<resource name>':'<resource content>',

NOTE: <resource content> must be HTML-escaped to avoid parsing issues

@version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/js/resources/resources_de.js#1 $$Change: 683854 $ --%>
*/
var RESOURCES = {
  'mobile.search.facets.selected':'{0} wurde ausgew&#228;hlt',
  'mobile.search.refine.popup':'Veredelung...',
  'mobile.search.refine.cancel':'Abbrechen',
  'mobile.search.refine.done':'OK',
  'mobile.search.loading':'Loading...',
  'common.button.addToCartText':'In den Warenkorb',
  'mobile.productDetails.updatecart':'Warenkorb aktualisieren',
  'mobile.button.preorderLabel':'Vorbestellung',
  'mobile.button.preorderText':'Available soon',
  'mobile.button.backorderText':'Nachbestellung',
  'mobile.button.emailMeText':'Eine E-Mail an mich senden',
  'navigation_shoppingCart.viewCart':'Warenkorb anzeigen',
  'mobile.notifyMe_whenInStock':'Wenn verf&#252;gbar\:',
  'mobile.passwordReset.email':'E-Mail-Adresse',
  'mobile.notifyMe_title':'Danke f&#252;r Ihr Interesse',
  'mobile.notifyMe_thankYouMsg':'Sobald der Artikel verf&#252;gbar ist, werden wir Ihnen eine E-Mail senden.',
  'common.temporarilyOutOfStock':'Nicht auf Lager',
  'common.price.old':'war',
};