<%--
  Renders commerce item information

  Page includes:
    /mobile/cart/gadgets/cartItemImg.jsp item image renderer
    /global/gadgets/productLinkGenerator.jsp product link generator
    /mobile/global/util/displaySkuProperties.jsp display list of sku properties
    /mobile/checkout/gadgets/confirmDetailedItemPrice.jsp display price details of the item

  Required parameters:
    currentItem
      item to be displayed

  Optional parameters:
    isCheckout
      indicator if this order is about to checkout or has been already placed
      Possible values: true - is about to be checkout; false - placed order
 --%>
<dsp:page>

  <dsp:getvalueof var="currentItem" vartype="atg.commerce.order.CommerceItem" param="currentItem"/>
  <dsp:getvalueof param="currentItem.auxiliaryData.productRef.NavigableProducts" var="navigable"
                  vartype="java.lang.Boolean"/>
  <dsp:getvalueof var="isCheckout" param="isCheckout"/>

  <div class="mobile_store_cartItem">
    <div class="image">
      <dsp:include page="../../cart/gadgets/cartItemImg.jsp">
        <dsp:param name="commerceItem" param="currentItem"/>
      </dsp:include>
    </div>
    <div class="item">
      <p class="name">
        <%-- Link back to the product detail page --%>
        <dsp:include page="/global/gadgets/productLinkGenerator.jsp">
          <dsp:param name="product" param="currentItem.auxiliaryData.productRef"/>
          <dsp:param name="siteId" param="currentItem.auxiliaryData.siteId"/>
        </dsp:include> 
        <c:choose>
          <c:when test="${(not empty productUrl) and (not isCheckout)}">
            <%-- We do not need to readd any parameters as order is already placed. --%>
            <dsp:a href="${fn:escapeXml(productUrl)}">
              <dsp:valueof param="currentItem.auxiliaryData.productRef.displayName">
                <fmt:message key="common.noDisplayName"/>
              </dsp:valueof>
            </dsp:a>
          </c:when>
          <c:otherwise>
            <dsp:valueof param="currentItem.auxiliaryData.productRef.displayName">
              <fmt:message key="common.noDisplayName"/>
            </dsp:valueof>
          </c:otherwise>
        </c:choose>
      </p>

      <p class="properties">
        <dsp:include page="../../global/util/displaySkuProperties.jsp">
          <dsp:param name="product" param="currentItem.auxiliaryData.productRef"/>
          <dsp:param name="sku" param="currentItem.auxiliaryData.catalogRef"/>
        </dsp:include>
      </p>
    </div>
    <div class="price">
      <div class="priceContent">
        <dsp:include page="../../checkout/gadgets/confirmDetailedItemPrice.jsp">
          <dsp:param name="commerceItem" value="${currentItem}"/>
        </dsp:include>
      </div>
    </div>
  </div>
</dsp:page>

<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/global/gadgets/orderItemsRenderer.jsp#3 $$Change: 692002 $--%>