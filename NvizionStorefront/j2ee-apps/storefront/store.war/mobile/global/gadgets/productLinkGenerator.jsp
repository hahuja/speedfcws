<%--
  Calculates a valid URL to be displayed for the specified product.

  Page includes:
    None

  Required parameters:
    product
      The product repository item to build URL for

  Optional parameters:
    None

  Output parameters:
    productUrl
      request-scoped variable with generated URL
--%>
<dsp:page>
  <dsp:getvalueof var="templateUrl" param="product.template.url"/>
  <c:choose>
    <c:when test="${empty templateUrl}">
      <c:set var="productUrl" scope="request" value=""/>
    </c:when>
    <c:otherwise>
      <c:url var="productUrl" value="${templateUrl}" scope="request">
        <c:param name="productId">
          <dsp:valueof param="product.repositoryId"/>
        </c:param>
      </c:url>
    </c:otherwise>
  </c:choose>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/global/gadgets/productLinkGenerator.jsp#3 $$Change: 692002 $--%>
