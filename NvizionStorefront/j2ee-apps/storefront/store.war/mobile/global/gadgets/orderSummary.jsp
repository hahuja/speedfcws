<%-- 
  This gadget renders the order summary on the checkout pages 

  Page includes:
    /mobile/checkout/gadgets/reviewOrderItems.jsp display order items
    /mobile/checkout/gadgets/reviewPayment renderer of payment info
    /mobile/checkout/gadgets/reviewShippingAddress.jsp renderer of shipping address
    /mobile/checkout/gadgets/reviewShippingMethod.jsp renderer of shipping method

  Required parameters:
    order
      an order item (as for view submitted order as for review order for checkout)
    hardgoodShippingGroup
      a correct single hardgood shipping group without giftWrapItems

  Optional parameters:
    isCheckout
      should be true if checkout, false or absent if viewing submitted order
--%>

<dsp:page>
  <dsp:getvalueof var="isCheckout" param="isCheckout" />
  <dsp:getvalueof var="headerText" param="headerText" />
  <%-- Render order items with order summary box --%>
  <dsp:include page="../../checkout/gadgets/reviewOrderItems.jsp">
    <dsp:param name="order" param="order"/>
    <dsp:param name="isCheckout" value="${isCheckout}"/>
  </dsp:include>
  <%-- Pay With (Payment Method and Billing Address) --%>
  <dsp:include page="../../checkout/gadgets/reviewPayment.jsp">
    <dsp:param name="order" param="order"/>
    <dsp:param name="isCheckout" value="${isCheckout}"/>
  </dsp:include>
  <%-- Ship To --%>
  <dsp:include page="../../checkout/gadgets/reviewShippingAddress.jsp">
    <dsp:param name="order" param="order"/>
    <dsp:param name="address" param="hardgoodShippingGroup.shippingAddress"/>
    <dsp:param name="isCheckout" value="${isCheckout}"/>
  </dsp:include>
  <%-- Shipping method --%>
  <dsp:include page="../../checkout/gadgets/reviewShippingMethod.jsp">
    <dsp:param name="order" param="order"/>
    <dsp:param name="isCheckout" value="${isCheckout}"/>
  </dsp:include>        
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/global/gadgets/orderSummary.jsp#4 $$Change: 692002 $--%>
