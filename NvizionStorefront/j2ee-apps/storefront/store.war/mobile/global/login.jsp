<%--
  This page renders a form with three different possible options:
    Login
    Signup
    Skip Registration

  Each different form is contained in its own row, and clicking on that
  row causes a child row to expand containing the form.

  Required parameters:
    checkoutLogin
      If true, sets the formhandler, error/success URLs, and proper form
      inputs to the CheckoutProfileFormHandler.  Otherwise,
      B2CProfileFormHandler is used.

  Optional parameters:
    passwordSent
      tells us whether the customer has had a temporary password
      sent to their email
    loginErrors
      If true, we know there are errors on the login form, and will show it
      by default.
    registrationErrors
      If true, we know there are errors on the registration form, and will show it
      by default.
--%>

<dsp:page>
  <dsp:importbean bean="/atg/userprofiling/Profile"/>
  <dsp:importbean bean="/atg/store/droplet/ProfileSecurityStatus"/>
  <dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>

  <dsp:getvalueof var="passwordSent" param="passwordSent"/>
  <dsp:getvalueof var="checkoutLogin" param="checkoutLogin"/>
  <dsp:getvalueof var="loginErrors" param="loginErrors"/>
  <dsp:getvalueof var="registrationErrors" param="registrationErrors"/>
  <dsp:getvalueof var="currentLocale" vartype="java.lang.String" bean="/atg/dynamo/servlet/RequestLocale.localeString"/>

  <fmt:message var="title" key="myaccount_login.title"/>
  <crs:mobilePageContainer titleString="${title}">

    <br/>
    <ul class="mobile_store_list">
      <%-- Login row --%>
      <%-- Update the arrow if the login row will open by default --%>
      <c:if test="${loginErrors == 'true'}">
        <c:set var="clicked" value="imageClicked"/>
      </c:if>
      <li class="${clicked}" onclick="loginPageClick('loginRow', this);">
        <a href="javascript:void(0);" onclick="">
          <span class="parentSpan">
            <span class="title">
              <fmt:message key="navigation_loginLogout.login"/>
            </span>
            <%-- Get the first name if the user is auto logged in --%>
            <dsp:droplet name="ProfileSecurityStatus">
              <dsp:oparam name="autoLoggedIn">
                <span class="mobile_store_formFieldText username">
                  <dsp:valueof bean="Profile.firstName"/>
                  <c:set var="logoutShow" value="true"/>
                </span>
              </dsp:oparam>
            </dsp:droplet>
            <span class="rightContentContainer">
              <span class="username">
                <div class="arrow"></div>
              </span>
            </span>
          </span>
        </a>
      </li>
      <%-- Login Form row --%>
      <%-- If we don't have errors in the login row, hide by default --%>
      <c:if test="${!(loginErrors == 'true' || passwordSent == 'true' || logoutShow == 'true')}">
        <c:set var="loginHidden" value="hidden"/>
      </c:if>
      <li id="loginRow" class="${loginHidden} expandable">
        <div>
          <dsp:include page="../global/gadgets/loginPage.jsp">
            <c:if test="${checkoutLogin == 'true'}">
              <dsp:param name="checkoutLogin" value="${checkoutLogin}"/>
              <dsp:param name="passwordSent" value="${passwordSent}"/>
            </c:if>
          </dsp:include>
        </div>
      </li>
      <%-- If logged in, display a logout row, otherwise display a registration row --%>
      <c:choose>
        <c:when test="${logoutShow == 'true'}">
          <li>
            <dsp:importbean bean="/atg/userprofiling/B2CProfileFormHandler"/>

            <dsp:a page="/">
            <span class="parentSpan">
              <%-- Determine which login page to redirect to --%>
              <c:url value="${pageContext.request.requestURI}" var="successURL" context="/">
                <c:if test="${checkoutLogin == 'true'}">
                  <c:param name="checkoutLogin" value="true"/>
                </c:if>
                <c:param name="userLocale" value="${currentLocale}"/>
              </c:url>

              <dsp:property bean="B2CProfileFormHandler.logoutSuccessURL" value="${successURL}"/>
              <dsp:property bean="B2CProfileFormHandler.logout" value="true"/>

              <span class="title">
                <fmt:message key="navigation_loginLogout.logout"/>
              </span>

              <span class="rightContentContainer">
                <div class="arrow"></div>
              </span>
            </span>
            </dsp:a>
          </li>
        </c:when>
        <c:otherwise>
          <%-- Registration row --%>
          <%-- Update the arrow if the login row will open by default --%>
          <c:choose>
            <c:when test="${registrationErrors == 'true'}">
              <c:set var="clicked" value="imageClicked"/>
            </c:when>
            <c:otherwise>
              <c:set var="registrationHidden" value="hidden"/>
            </c:otherwise>
          </c:choose>
          <li class="${clicked}" onclick="loginPageClick('regRow', this);">
            <a href="javascript:void(0);" onclick="">
              <span class="parentSpan">
                <span class="title">
                  <fmt:message key="mobile.myaccount_login.button.create"/>
                </span>
                <span class="rightContentContainer">
                  <div class="arrow"></div>
                </span>
              </span>
            </a>
          </li>
          <%-- Registration form row --%>
          <li id="regRow" class="${registrationHidden} expandable">
            <dsp:include page="../global/gadgets/registration.jsp">
              <%-- Send checkout parameter to loginPage --%>
              <c:if test="${checkoutLogin == 'true'}">
                <dsp:param name="checkoutLogin" value="true"/>
              </c:if>
            </dsp:include>
          </li>
        </c:otherwise>
      </c:choose>
      <%-- Display the Skip Login row if using the checkout workflow --%>
      <c:if test="${checkoutLogin == 'true'}">
        <li onclick="document.anonymousForm.submit();">
          <a href="javascript:void(0);" onclick="">
            <dsp:importbean bean="/atg/store/profile/CheckoutProfileFormHandler"/>
            <dsp:form action="${originatingRequest.requestURI}" method="post"
                      name="anonymousForm" formid="${formId}">
              <c:url value="../checkout/shipping.jsp" var="successURL" scope="page">
                <c:param name="locale" value="${currentLocale}"/>
              </c:url>
              <dsp:input bean="CheckoutProfileFormHandler.loginSuccessURL" type="hidden"
                         value="${successURL}"/>
              <fmt:message key="mobile.myaccount_login.button.anonymous" var="anonymousCaption"/>
              <dsp:input bean="CheckoutProfileFormHandler.anonymousCustomer" type="hidden"
                         value="${anonymousCaption}"/>
            </dsp:form>
            <span class="parentSpan">
              <span class="title">
                <fmt:message key="mobile.myaccount_login.button.anonymous"/>
              </span>
              <span class="rightContentContainer">
                 <div class="arrowRight"></div>
              </span>
            </span>
          </a>
        </li>
      </c:if>
    </ul>
    <br/>
  </crs:mobilePageContainer>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/global/login.jsp#3 $$Change: 692002 $--%>
