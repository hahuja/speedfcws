<%--
  This gadget renders a price accordingly to the locale specified.

  Required parameters:
    price
      Price to be formatted.

  Optional parameters:
    priceListLocale
      Specifies a locale in which to format the price. If nothing specified, locale will be taken from profile's price list.
    saveFormattedPrice
      If true, the price rendered will not be displayed to user, it will be saved into formattedPrice request-scoped variable instead.
--%>

<dsp:page>
  <dsp:importbean bean="/atg/commerce/pricing/CurrencyCodeDroplet"/>
  <dsp:importbean bean="/atg/userprofiling/Profile"/>
  <dsp:importbean bean="/atg/dynamo/servlet/RequestLocale"/>
  <dsp:importbean bean="/atg/multisite/SiteContext"/>
  <dsp:importbean bean="/atg/store/StoreConfiguration" />

  <dsp:getvalueof var="priceListLocale" vartype="java.lang.String" bean="Profile.priceList.locale"/>
  
  <dsp:getvalueof var="requestLocale" vartype="java.lang.String" bean="RequestLocale.locale"/>
  <dsp:getvalueof var="saveFormattedPrice" param="saveFormattedPrice"/>

  <%--
    This droplet calculates a currency code for the locale specified.

    Input parameters:
      locale
        Specifies a locale to calculate the code from.

    Output parameters:
      currencyCode
        The resulting currency code.

    Open parameters:
      output
        Always rendered.
  --%>
  <dsp:droplet name="CurrencyCodeDroplet">
    <dsp:param name="locale" value="${priceListLocale}"/>
    <dsp:oparam name="output">
      <dsp:getvalueof var="currencyCode" vartype="java.lang.String" param="currencyCode"/>
      <dsp:getvalueof var="price" vartype="java.lang.Double" param="price"/>
      
      <c:if test="${priceListLocale != requestLocale}">
        <%-- Current locale is not the same as set on price list. Replace current locale with price list's one. --%>
        <c:if test="${fn:length(priceListLocale)>5 }">
          <c:set var="variant" value="${fn:substring(priceListLocale,6,fn:length(priceListLocale))}"/>
          <c:set var="priceListLocale" value="${fn:substring(priceListLocale,0,5)}"/>
        </c:if>
        <fmt:setLocale value="${priceListLocale}" variant="${variant}"/>
        
        <%-- Update resource bundle's locale too. --%>
        <dsp:getvalueof var="resourceBundle" bean="SiteContext.site.resourceBundle" />
        <dsp:getvalueof var="defaultResourceBundle" bean="StoreConfiguration.defaultResourceBundle" />
        <c:choose>
          <c:when test="${not empty resourceBundle}">
            <fmt:setBundle basename="${resourceBundle}"/>
          </c:when>
          <c:otherwise>
            <fmt:setBundle basename="${defaultResourceBundle}"/>
          </c:otherwise>
        </c:choose>
      </c:if>
      
      <%-- Display price formatted according to the pricelist locale. --%>
      <dsp:getvalueof var="currencyCode" value="${currencyCode}" scope="request"/>
    </dsp:oparam>
  </dsp:droplet><%-- End CurrencyCode Droplet --%>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/global/util/currencyCode.jsp#3 $$Change: 692002 $--%>
