<%--
  This gadget checks to see if a user is logged in.
       
  Required parameters;
    None.
      
  Optional parameters;
    None.
 --%>
<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:droplet name="Switch">
		<dsp:param bean="Profile.transient" name="value" />
		<dsp:oparam name="false">
		  <dsp:getvalueof var="loggedIn" value="true" scope="request"/>
   </dsp:oparam>
   <dsp:oparam name="true">
      <dsp:getvalueof var="loggedIn" value="false" scope="request"/>
   </dsp:oparam>
	</dsp:droplet>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/global/util/loggedIn.jsp#3 $$Change: 692002 $--%>