<%--
 /*<ORACLECOPYRIGHT>
 * Copyright (C) 1994-2012 Oracle and/or its affiliates. All rights reserved.
 * Oracle and Java are registered trademarks of Oracle and/or its affiliates. 
 * Other names may be trademarks of their respective owners.
 * UNIX is a registered trademark of The Open Group.
 *
 * This software and related documentation are provided under a license agreement 
 * containing restrictions on use and disclosure and are protected by intellectual property laws. 
 * Except as expressly permitted in your license agreement or allowed by law, you may not use, copy, 
 * reproduce, translate, broadcast, modify, license, transmit, distribute, exhibit, perform, publish, 
 * or display any part, in any form, or by any means. Reverse engineering, disassembly, 
 * or decompilation of this software, unless required by law for interoperability, is prohibited.
 *
 * The information contained herein is subject to change without notice and is not warranted to be error-free. 
 * If you find any errors, please report them to us in writing.
 *
 * U.S. GOVERNMENT RIGHTS Programs, software, databases, and related documentation and technical data delivered to U.S. 
 * Government customers are "commercial computer software" or "commercial technical data" pursuant to the applicable 
 * Federal Acquisition Regulation and agency-specific supplemental regulations. 
 * As such, the use, duplication, disclosure, modification, and adaptation shall be subject to the restrictions and 
 * license terms set forth in the applicable Government contract, and, to the extent applicable by the terms of the 
 * Government contract, the additional rights set forth in FAR 52.227-19, Commercial Computer Software License 
 * (December 2007). Oracle America, Inc., 500 Oracle Parkway, Redwood City, CA 94065.
 *
 * This software or hardware is developed for general use in a variety of information management applications. 
 * It is not developed or intended for use in any inherently dangerous applications, including applications that 
 * may create a risk of personal injury. If you use this software or hardware in dangerous applications, 
 * then you shall be responsible to take all appropriate fail-safe, backup, redundancy, 
 * and other measures to ensure its safe use. Oracle Corporation and its affiliates disclaim any liability for any 
 * damages caused by use of this software or hardware in dangerous applications.
 *
 * This software or hardware and documentation may provide access to or information on content, 
 * products, and services from third parties. Oracle Corporation and its affiliates are not responsible for and 
 * expressly disclaim all warranties of any kind with respect to third-party content, products, and services. 
 * Oracle Corporation and its affiliates will not be responsible for any loss, costs, 
 * or damages incurred due to your access to or use of third-party content, products, or services.
 </ORACLECOPYRIGHT>*/
--%>

<%--
  This page displays an overview of the logged in user's profile information. It includes links to edit
    personal info, change pw, view orders, view addresses, view credit cards, and profile defaults.

  Page includes:
    /mobile/myaccount/gadgets/subheaderAccounts.jsp display subheader items

  Required parameters:
    None

  Optional parameters:
    None
--%>

<dsp:page>
  <dsp:importbean bean="/atg/userprofiling/Profile"/>
  <dsp:importbean bean="/atg/commerce/order/OrderLookup"/>
  <dsp:importbean bean="/atg/commerce/util/MapToArrayDefaultFirst"/>
  <dsp:importbean bean="/atg/commerce/pricing/AvailableShippingMethods"/>
  <dsp:importbean bean="/atg/commerce/ShoppingCart"/>

  <fmt:message var="title" key="mobile.myaccount_accountInformation.title" />
  <crs:mobilePageContainer titleString="${title}">
    <dsp:include page="./gadgets/subheaderAccounts.jsp"/>
      <div class="mobile_store_displayList">
        <ul class="mobile_store_mobileList profile" role="presentation">
          <li id="mobile_store_myaccountLogout" class="withArrow">
          <ul>
            <li>
              <%-- Logout --%>
		      <dsp:importbean bean="/atg/userprofiling/B2CProfileFormHandler"/>
		      <dsp:importbean bean="/atg/dynamo/servlet/RequestLocale"/>

		      <fmt:message var="logout" key="navigation_loginLogout.logout" />
		      <dsp:getvalueof id="userLocale" vartype="java.lang.String"  bean="RequestLocale.locale"/>
		      <dsp:a page="/" title="${logout}">
		        <dsp:property bean="B2CProfileFormHandler.logoutSuccessURL"
		                      value="mobile/myaccount/login.jsp?locale=${userLocale}"/>
		        <dsp:property bean="B2CProfileFormHandler.logout" value="true"/>
		        <c:out value="${logout}"/>
		      </dsp:a>
            </li>
            <%-- Change Password --%>
            <li>
	          <dsp:a page="/mobile/myaccount/profilePasswordEdit.jsp">
                <fmt:message key="mobile.myaccount_changePassword.text"/>
              </dsp:a>
	        </li>
          </ul>
        </li>
        <li class="withArrow">
          <%-- Personal Info --%>
          <dsp:a page="/mobile/myaccount/accountProfileEdit.jsp">
            <div class="content">
	          <fmt:message key="myaccount_profileMyInfo.myInformation"/>
              <span class="mobile_store_formFieldText">
                <dsp:valueof bean="Profile.firstName"/>
                <dsp:valueof bean="Profile.lastName"/><br/>
                <dsp:valueof bean="Profile.email"/><br/>
                <dsp:getvalueof var="postalCode" bean="Profile.homeAddress.postalCode"/>
                <c:choose>
                  <c:when test="${empty postalCode}">
                    <fmt:message key="mobile.myaccount_accountInformation.noPostal" />
                  </c:when>
                  <c:otherwise>
                    ${postalCode}
                  </c:otherwise>
                </c:choose>
              </span>
            </div>
          </dsp:a>
        </li>
        <li class="withArrow">
          <div class="content">
          <dsp:a page="/mobile/myaccount/myOrders.jsp">
            <fmt:message key="myaccount_myAccountMenu.myOrders"/><fmt:message key="common.labelSeparator"/>
            <span class="mobile_store_formFieldMajorText">
              <%-- Get the total number of orders for the user --%>
              <dsp:droplet name="OrderLookup">
                <dsp:param name="userId" bean="Profile.id"/>
                <dsp:param name="state" value="closed"/>
                <dsp:param name="queryTotalOnly" value="true"/>
                <dsp:param name="numOrders" value="-1"/>
                <dsp:oparam name="output">
                  <dsp:getvalueof var="totalCount" param="totalCount"/>
                  <fmt:message key="mobile.myaccount_accountInformation.numberSaved">
                    <fmt:param value="${totalCount}"/>
                  </fmt:message>
                </dsp:oparam>
              </dsp:droplet>
            </span>
          </dsp:a>
          </div>
        </li>
        <li class="withArrow">
          <div class="content">
          <dsp:a page="/mobile/myaccount/addressBook.jsp">
            <fmt:message key="mobile.myaccount_accountInformation.addresses"/>
            <span class="mobile_store_formFieldMajorText">
              <%-- Get the total number of addresses for the user --%>
              <dsp:droplet name="MapToArrayDefaultFirst">
                <dsp:param name="defaultId" bean="Profile.shippingAddress.repositoryId"/>
                <dsp:param name="map" bean="Profile.secondaryAddresses"/>
                <dsp:oparam name="empty">
                  <fmt:message key="mobile.myaccount_accountInformation.numberSaved">
                    <fmt:param value="0"/>
                  </fmt:message>
                </dsp:oparam>
                <dsp:oparam name="output">
                  <c:set var="counterAddress" value="0"/>
                  <dsp:getvalueof var="sortedArray" vartype="java.lang.Object" param="sortedArray"/>
                  <c:forEach var="shippingAddress" items="${sortedArray}">
                    <dsp:setvalue param="shippingAddress" value="${shippingAddress}"/>
                    <c:if test="${not empty shippingAddress}">
                      <c:set var="counterAddress" value="${counterAddress + 1}"/>
                    </c:if>
                  </c:forEach>
                  <%-- Make text dynamic --%>
                  <fmt:message key="mobile.myaccount_accountInformation.numberSaved">
                    <fmt:param value="${counterAddress}"/>
                  </fmt:message>
                </dsp:oparam>
              </dsp:droplet>
            </span>
          </dsp:a>
          </div>
        </li>
        <li class="withArrow">
          <div class="content">
          <dsp:a page="/mobile/myaccount/paymentInfo.jsp">
            <fmt:message key="mobile.myaccount_accountInformation.cc"/>
            <span class="mobile_store_formFieldMajorText">
              <%-- Get the total number of Credit Cards for the user --%>
              <dsp:droplet name="MapToArrayDefaultFirst">
                <dsp:param name="defaultId" bean="Profile.defaultCreditCard.repositoryId"/>
                <dsp:param name="map" bean="Profile.creditCards"/>
                <dsp:oparam name="empty">
                  <fmt:message key="mobile.myaccount_accountInformation.numberSaved">
                    <fmt:param value="0"/>
                  </fmt:message>
                </dsp:oparam>
                <dsp:oparam name="output">
                  <c:set var="counterCard" value="0"/>
                  <dsp:getvalueof var="sortedArray" vartype="java.lang.Object" param="sortedArray"/>
                  <c:forEach var="creditCard" items="${sortedArray}">
                    <dsp:setvalue param="creditCard" value="${creditCard}"/>
                    <c:if test="${not empty creditCard}">
                      <c:set var="counterCard" value="${counterCard + 1}"/>
                    </c:if>
                  </c:forEach>
                  <fmt:message key="mobile.myaccount_accountInformation.numberSaved">
                    <fmt:param value="${counterCard}"/>
                  </fmt:message>
                </dsp:oparam>
              </dsp:droplet>
            </span>
          </dsp:a>
          </div>
        </li>
      </ul>
    </div>
    <br/>
  </crs:mobilePageContainer>
</dsp:page>

<%-- @updated $DateTime: 2012/03/07 18:24:16 $$Author: jsiddaga $ --%>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/myaccount/profile.jsp#3 $$Change: 692002 $--%>
