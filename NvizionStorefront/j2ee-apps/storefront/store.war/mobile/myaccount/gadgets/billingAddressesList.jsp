<dsp:page>
<%--
  This page renders list of billing addresses

  Page includes:
    /global/util/displayAddress.jsp renderer of address info

  Required parameters:
    selectedBillingAddressId
      actual only in internal mode; id of address that should be marked as selected one

  Optional parameters:
    paramMode
      display mode: internal (used on edit credit card page) or global one
      Possible values: [internal|*]
--%>
  <dsp:importbean bean="/atg/commerce/util/MapToArrayDefaultFirst"/>
  <dsp:importbean bean="/atg/userprofiling/Profile"/>
  <dsp:importbean bean="/atg/dynamo/droplet/Compare"/>
  <dsp:importbean bean="/atg/userprofiling/MobileB2CProfileFormHandler"/>

  <%-- Get mode (internal block inside form/main form on the page) that this page is invoked with --%>
  <dsp:getvalueof var="paramMode" param="mode"/>
  <dsp:getvalueof var="selectedBillingAddressId" param="selectedBillingAddressId"/>
  <dsp:getvalueof var="currentBillingAddress" param="currentBillingAddress"/>

  <c:choose>
    <c:when test="${paramMode == 'internal'}">
      <dsp:input id="createNewAddress" bean="MobileB2CProfileFormHandler.createNewAddress" type="hidden"/>
      <dsp:input id="matchedSecondaryAddressId"
                 bean="MobileB2CProfileFormHandler.matchedSecondaryAddressId"
                 type="hidden"/>

      <c:set var="ulClass" value="internalBillingAddresses"/>
    </c:when>
    <c:otherwise>
      <c:set var="ulClass" value="mobile_store_mobileList"/>
    </c:otherwise>
  </c:choose>

  <ul id="mobile_store_savedBillingAddresses" class="${ulClass}">
    <dsp:getvalueof var="currentBillingAddressDisplayed" bean="MobileB2CProfileFormHandler.currentBillingAddressDisplayed"/>
    <c:if test="${paramMode == 'internal' && currentBillingAddressDisplayed}">
      <li onclick="" id="currentBillingAddress" class="mobile_store_address">
        <div class="layout">
          <div class="content">
            <dsp:input type="radio" checked="true"
                                 id="${selectedBillingAddressId}" value="${selectedBillingAddressId}"
                                 bean="MobileB2CProfileFormHandler.selectedBillingAddress"/>

            <label for="${selectedBillingAddressId}" onclick="">
              <dsp:include page="/global/util/displayAddress.jsp" flush="false">
                <dsp:param name="address" param="currentBillingAddress"/>
                <dsp:param name="private" value="false"/>
              </dsp:include>
            </label>
            <dsp:input bean="MobileB2CProfileFormHandler.editBillAddress" type="radio" value="" onclick="creditCardSelectAddressToEdit(event);" iclass="mobile_store_storedAddressActions" class="editBillingAddress"/>
          </div>
        </div>
      </li>
      <li class="mobile_store_border mobile_store_shadowedBorder"></li>
    </c:if>
    <dsp:droplet name="MapToArrayDefaultFirst">
      <dsp:param name="map" bean="Profile.secondaryAddresses"/>
      <dsp:param name="defaultId" bean="Profile.shippingAddress.repositoryId"/>

      <dsp:oparam name="output">
        <dsp:getvalueof var="sortedArray" vartype="java.lang.Object" param="sortedArray"/>
        <c:if test="${not empty sortedArray}">
          <c:set var="counter" value="0"/>
          <dsp:getvalueof var="defaultAddressId" vartype="java.lang.String" bean="Profile.shippingAddress.repositoryId"/>        

          <c:forEach var="shippingAddress" items="${sortedArray}" varStatus="status">
            <dsp:param name="shippingAddress" value="${shippingAddress}"/>
            <c:if test="${not empty shippingAddress}">
              <c:set var="count" value="0"/>
              <li onclick="" class="mobile_store_address">
                <dsp:getvalueof var="addressKey" vartype="java.util.String" param="shippingAddress.key"/>

                <div class="layout">
                  <div class="content">
                    <c:choose>
                      <c:when test="${paramMode != 'internal'}">
                        <dsp:getvalueof var="selectorValue" param="shippingAddress.key"/>
                      </c:when>
                      <c:otherwise>
                        <dsp:getvalueof var="selectorValue" param="shippingAddress.value.repositoryId"/>
                      </c:otherwise>
                    </c:choose>
                    <dsp:droplet name="/atg/store/droplet/BillingRestrictionsDroplet">
                      <dsp:param name="countryCode" param="shippingAddress.value.country"/>
                      <dsp:oparam name="true">
                        <dsp:input type="radio" value="${selectorValue}" disabled="true"
                                   id="${shippingAddress.value.repositoryId}"
                                   bean="MobileB2CProfileFormHandler.selectedBillingAddress"/>
                      </dsp:oparam>
                      <dsp:oparam name="false">
                        <dsp:input type="radio" value="${selectorValue}"
                                   id="${shippingAddress.value.repositoryId}"
                                   checked="${not empty selectedBillingAddressId and selectedBillingAddressId == shippingAddress.value.repositoryId}"
                                   bean="MobileB2CProfileFormHandler.selectedBillingAddress"/>
                      </dsp:oparam>
                    </dsp:droplet>

                    <label for="${shippingAddress.value.repositoryId}" onclick="">
                      <dsp:include page="/mobile/global/util/displayAddress.jsp" flush="false">
                        <dsp:param name="address" param="shippingAddress.value"/>
                        <dsp:param name="private" value="false"/>
                      </dsp:include>
                    </label>
                  </div>
                  
                  <%-- Display Edit Link --%>
                  <span class="mobile_store_storedAddressActions">
                    <fmt:message var="accountAddressEditTitle" key="myaccount_accountAddressEdit.title"/>
                    <c:choose>
                      <c:when test="${paramMode == 'internal'}">
                        <a href="javascript:void(0);" title="${accountAddressEditTitle}" class="mobile_store_storedAddressActions" onclick="checkAddressRadio(event); creditCardSelectAddressToEdit(event);"></a>
                        <dsp:input bean="MobileB2CProfileFormHandler.editBillAddress" type="radio" value="${shippingAddress.key}" class="editBillingAddress"/>
                      </c:when>
                      <c:otherwise>
                        <dsp:a title="${accountAddressEditTitle}" bean="MobileB2CProfileFormHandler.editBillAddress" iclass="mobile_store_storedAddressActions"
                               page="/mobile/myaccount/creditCardAddressEdit.jsp" paramvalue="shippingAddress.key">
                        </dsp:a>
                      </c:otherwise>
                    </c:choose>
                  </span>
                </div>
              </li>
              <c:if test="${paramMode == 'internal'}">
                <li class="mobile_store_border mobile_store_shadowedBorder"/>
              </c:if>
            </c:if>
          </c:forEach>      
        </c:if>
      </dsp:oparam>
    </dsp:droplet>

    <%-- Create new shipping address page --%>
    <li class="mobile_store_formActions">
      <div class="content">
        <c:choose>
          <c:when test="${paramMode == 'internal'}">
            <a href="javascript:void(0);" onclick="creditCardCreateNewAddress()">
              <fmt:message key="mobile.common.newBillingAddress"/>
            </a>
          </c:when>
          <c:otherwise>
            <dsp:a page="/mobile/myaccount/creditCardAddressAdd.jsp" class="listLink">
              <fmt:message key="mobile.common.newBillingAddress"/>
            </dsp:a>
          </c:otherwise>
        </c:choose>
      </div>
    </li>

  </ul>

  <c:choose>
    <c:when test="${paramMode == 'internal'}">
      <script type="text/javascript">
        $(document).ready(function() {
          $("#mobile_store_savedBillingAddresses").dropdownList();
        });
      </script>
    </c:when>
  </c:choose>

</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/myaccount/gadgets/billingAddressesList.jsp#3 $$Change: 692002 $--%>
