<%--
  This page renders the Select Default Shipping Method page of the Checkout Defaults

  Page includes:
    /mobile/myaccount/gadgets/subheaderAccounts.jsp display subheader items

  Required parameters:
    None

  Optional parameters:
    None
--%>

<dsp:page>
  <dsp:importbean bean="/atg/userprofiling/Profile"/>
  <dsp:importbean bean="/atg/userprofiling/MobileB2CProfileFormHandler"/>
  <dsp:importbean bean="/atg/commerce/pricing/AvailableShippingMethods"/>
  <dsp:importbean bean="/atg/commerce/ShoppingCart"/>

  <fmt:message key="mobile.myaccount_payment.defaultShippingMethod" var="pageTitle"/>

  <crs:mobilePageContainer titleString="${pageTitle}">
    <jsp:body>
      <%-- Subeader --%>
      <fmt:message var="defaultShippingMethodText" key="common.defaultShippingMethod"/>
      <fmt:message var="defaultsText" key="mobile.myaccount_checkoutDefaults.defaults"/>
      <dsp:include page="/mobile/myaccount/gadgets/subheaderAccounts.jsp">
        <dsp:param name="centerText" value="${defaultsText}" />
        <dsp:param name="centerURL" value="/mobile/myaccount/profileCheckOutPrefs.jsp" />

        <dsp:param name="rightText" value="${defaultShippingMethodText}" />

        <dsp:param name="highlight" value="right" />
      </dsp:include>
      <div class="mobile_store_checkoutDefaults">
        <dsp:form id="mobile_store_checkoutDefaultShippingMethod" action="${pageContext.request.requestURI}" method="post" formid="mobile_store_checkoutDefaultShippingMethod">
          <ul id="mobile_store_defaultShippingMethod" class="mobile_store_mobileList">

            <dsp:droplet name="AvailableShippingMethods">
              <dsp:param name="shippingGroup" bean="ShoppingCart.current.shippingGroups[0]"/>
              <dsp:oparam name="output">
                <dsp:getvalueof var="availableShippingMethods" vartype="java.lang.Object" param="availableShippingMethods"/>
                <dsp:getvalueof var="defaultShippingMethod" vartype="java.lang.String" bean="Profile.defaultCarrier"/>

                <li>
                  <div class="content">
                    <dsp:input
                      id="notSpecified"
                      name="defaultCarrier"
                      type="radio"
                      onclick="$('#mobile_store_checkoutDefaultShippingMethod').submit();"
                      bean="MobileB2CProfileFormHandler.value.defaultCarrier"
                      value=""
                      checked="${empty defaultShippingMethod}"/>
                    <label for="notSpecified" onclick="">
                      <fmt:message key="common.notSpecified"/>
                    </label>
                  </div>
                </li>

                <c:forEach var="availableShippingMethod" items="${availableShippingMethods}" varStatus="shipping_counter">
                  <li>
                    <div class="content">
                      <dsp:input
                        id="shipping_method_${shipping_counter.count}"
                        name="defaultCarrier"
                        type="radio"
                        onclick="$('#mobile_store_checkoutDefaultShippingMethod').submit();"
                        bean="MobileB2CProfileFormHandler.value.defaultCarrier"
                        value="${availableShippingMethod}"
                        checked="${defaultShippingMethod == availableShippingMethod}"/>
                      <label for="shipping_method_${shipping_counter.count}" onclick="">
                        <fmt:message key="checkout_shipping.delivery${fn:replace(availableShippingMethod, ' ', '')}"/>
                      </label>
                    </div>
                  </li>
                </c:forEach>

              </dsp:oparam>
            </dsp:droplet>
          </ul>

          <dsp:input type="hidden" bean="MobileB2CProfileFormHandler.updateSuccessURL" value="profileCheckOutPrefs.jsp"/>
          <dsp:input type="hidden" value="" bean="MobileB2CProfileFormHandler.checkoutDefaultCarrier" priority="-10"/>

        </dsp:form>
      </div>
    </jsp:body>
  </crs:mobilePageContainer>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/myaccount/profilePickDefaultShippingMethod.jsp#3 $$Change: 692002 $--%>
