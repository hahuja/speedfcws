<%--
  This page renders user's saved payment methods

  Page includes:
    /mobile/myaccount/gadgets/subheaderAccounts.jsp display subheader items
    /mobile/myaccount/gadgets/savedCreditCards.jsp list of saved credit cards

  Required parameters:
    None

  Optional parameters:
    None
--%>

<dsp:page>

  <fmt:message key="myaccount_myAccountMenu.paymentInfo" var="pageTitle"/>

  <crs:mobilePageContainer titleString="${pageTitle}">
    <jsp:body>
      <dsp:include page="./gadgets/subheaderAccounts.jsp" >
        <dsp:param name="centerText" value="${pageTitle}" />
        <dsp:param name="highlight" value="center" />
      </dsp:include>

      <div class="mobile_store_displayList">
        <dsp:include page="../checkout/gadgets/savedCreditCards.jsp" flush="false">
          <dsp:param name="areaPath" value="/mobile/myaccount"/>
          <dsp:param name="selectable" value="false"/>
          <dsp:param name="displayDefaultLabeled" value="true"/>
        </dsp:include>
      </div>
    </jsp:body>
  </crs:mobilePageContainer>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/myaccount/paymentInfo.jsp#3 $$Change: 692002 $--%>
