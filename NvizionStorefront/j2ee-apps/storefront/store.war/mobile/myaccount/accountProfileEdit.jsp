<%--
 /*<ORACLECOPYRIGHT>
 * Copyright (C) 1994-2012 Oracle and/or its affiliates. All rights reserved.
 * Oracle and Java are registered trademarks of Oracle and/or its affiliates. 
 * Other names may be trademarks of their respective owners.
 * UNIX is a registered trademark of The Open Group.
 *
 * This software and related documentation are provided under a license agreement 
 * containing restrictions on use and disclosure and are protected by intellectual property laws. 
 * Except as expressly permitted in your license agreement or allowed by law, you may not use, copy, 
 * reproduce, translate, broadcast, modify, license, transmit, distribute, exhibit, perform, publish, 
 * or display any part, in any form, or by any means. Reverse engineering, disassembly, 
 * or decompilation of this software, unless required by law for interoperability, is prohibited.
 *
 * The information contained herein is subject to change without notice and is not warranted to be error-free. 
 * If you find any errors, please report them to us in writing.
 *
 * U.S. GOVERNMENT RIGHTS Programs, software, databases, and related documentation and technical data delivered to U.S. 
 * Government customers are "commercial computer software" or "commercial technical data" pursuant to the applicable 
 * Federal Acquisition Regulation and agency-specific supplemental regulations. 
 * As such, the use, duplication, disclosure, modification, and adaptation shall be subject to the restrictions and 
 * license terms set forth in the applicable Government contract, and, to the extent applicable by the terms of the 
 * Government contract, the additional rights set forth in FAR 52.227-19, Commercial Computer Software License 
 * (December 2007). Oracle America, Inc., 500 Oracle Parkway, Redwood City, CA 94065.
 *
 * This software or hardware is developed for general use in a variety of information management applications. 
 * It is not developed or intended for use in any inherently dangerous applications, including applications that 
 * may create a risk of personal injury. If you use this software or hardware in dangerous applications, 
 * then you shall be responsible to take all appropriate fail-safe, backup, redundancy, 
 * and other measures to ensure its safe use. Oracle Corporation and its affiliates disclaim any liability for any 
 * damages caused by use of this software or hardware in dangerous applications.
 *
 * This software or hardware and documentation may provide access to or information on content, 
 * products, and services from third parties. Oracle Corporation and its affiliates are not responsible for and 
 * expressly disclaim all warranties of any kind with respect to third-party content, products, and services. 
 * Oracle Corporation and its affiliates will not be responsible for any loss, costs, 
 * or damages incurred due to your access to or use of third-party content, products, or services.
 </ORACLECOPYRIGHT>*/
--%>

<%--
  This page displays the profile information for the user and allows
  the user the ability to edit that information.

  Page includes:
    /mobile/myaccount/gadgets/subheaderAccounts.jsp display subheader items

  Required parameters:
    none

  Optional parameters:
    none
--%>

<dsp:page>
  <dsp:importbean bean="/atg/userprofiling/Profile"/>
  <dsp:importbean bean="/atg/store/profile/RegistrationFormHandler"/>
  <dsp:importbean bean="/atg/dynamo/droplet/PossibleValues"/>
  <dsp:importbean bean="/atg/userprofiling/ProfileAdapterRepository"/>
  <dsp:importbean bean="/atg/store/droplet/IsEmailRecipient"/>
  <dsp:importbean bean="/atg/core/i18n/LocaleTools"/>

  <fmt:message var="title" key="myaccount_profileMyInfo.myInformation"/>
  <crs:mobilePageContainer titleString="${title}">
    <%-- Handle form exceptions --%>
    <dsp:getvalueof var="formExceptions" vartype="java.lang.Object" bean="RegistrationFormHandler.formExceptions"/>
    <jsp:useBean id="errorMap" class="java.util.HashMap"/>
    <c:if test="${not empty formExceptions}">
      <c:forEach var="formException" items="${formExceptions}">
        <dsp:param name="formException" value="${formException}"/>
        <%-- Check the error message code to see what we should do --%>
        <dsp:getvalueof var="errorCode" vartype="java.lang.String" param="formException.errorCode"/>
        <c:choose>
          <c:when test="${'missingRequiredValue' eq errorCode}">
            <dsp:getvalueof var="propertyName" param="formException.propertyName"/>
            <c:set target="${errorMap}" property="${propertyName}" value="missing"/>
          </c:when>
          <c:when test="${'invalidDateFormat' eq errorCode}">
            <c:set target="${errorMap}" property="dateOfBirth" value="invalid"/>
          </c:when>
          <c:when test="${'postalCode' eq errorCode}">
            <c:set target="${errorMap}" property="postalCode" value="invalid"/>
          </c:when>
        </c:choose>
      </c:forEach>
    </c:if>

    <%-- Set the Date format --%>
    <dsp:getvalueof var="dateFormat" bean="LocaleTools.userFormattingLocaleHelper.datePatterns.shortWith4DigitYear"/>
    <dsp:setvalue bean="RegistrationFormHandler.dateFormat" value="${dateFormat}"/>
    <dsp:setvalue bean="RegistrationFormHandler.extractDefaultValuesFromProfile" value="true"/>

    <fmt:message var="editProf" key="mobile.myaccount_personalInformation.editProfile"/>
    <dsp:include page="./gadgets/subheaderAccounts.jsp">
      <dsp:param name="rightText" value="${editProf}"/>
      <dsp:param name="highlight" value="right"/>
    </dsp:include>

    <div class="mobile_store_myaccount_accountEditGrad">
      <dsp:form formid="mobile_store_profileEdit" action="${originatingRequest.requestURI}" method="post">
        <dsp:input bean="RegistrationFormHandler.updateSuccessURL" type="hidden" value="profile.jsp"/>
        <dsp:input bean="RegistrationFormHandler.updateErrorURL" type="hidden" beanvalue="/OriginatingRequest.requestURI"/>
        <div class="mobile_store_displayList">
          <ul class="mobile_store_mobileList" role="presentation">
            <fmt:message var="required" key="mobile.form.validation.missing"/>
            <%-- Check for errors in "First Name" --%>
            <c:if test="${not empty errorMap['firstName']}">
              <c:set var="liClassFirst" value="mobile_store_error_state"/>
            </c:if>
            <li class="${liClassFirst}">
              <fmt:message var="firstNamePlace" key="common.firstName"/>
              <div class="content">
                <dsp:input iclass="mobile_store_textBoxForm" id="mobile_store_editFirstName"
                           maxlength="40" bean="RegistrationFormHandler.value.firstName" type="text" required="true">
                  <dsp:tagAttribute name="placeholder" value="${firstNamePlace}"/>
                </dsp:input>
              </div>
              <c:if test="${not empty errorMap['firstName']}">
                <span class="errorMessage">${required}</span>
              </c:if>
            </li>
            <%-- Check for errors in "Last Name" --%>
            <c:if test="${not empty errorMap['lastName']}">
              <c:set var="liClassLast" value="mobile_store_error_state"/>
            </c:if>
            <li class="${liClassLast}">
              <fmt:message var="lastNamePlace" key="common.lastName"/>
              <div class="content">
                <dsp:input iclass="mobile_store_textBoxForm" id="mobile_store_editLastName"
                           maxlength="40" bean="RegistrationFormHandler.value.lastName" type="text" required="true">
                  <dsp:tagAttribute name="placeholder" value="${lastNamePlace}"/>
                </dsp:input>
              </div>
              <c:if test="${not empty errorMap['lastName']}">
                <span class="errorMessage">${required}</span>
              </c:if>
            </li>
            <%-- Check for errors in "Email" --%>
            <c:if test="${not empty errorMap['email']}">
              <c:set var="liClassMail" value="mobile_store_error_state"/>
            </c:if>
            <li class="${liClassMail}">
              <fmt:message var="emailPlace" key="common.email"/>
              <div class="content">
                <dsp:input iclass="mobile_store_textBoxForm" id="mobile_store_editEmail"
                           maxlength="40" bean="RegistrationFormHandler.value.email"
                           type="email" required="true" autocapitalize="off">
                  <dsp:tagAttribute name="placeholder" value="${emailPlace}"/>
                </dsp:input>
              </div>
              <c:if test="${not empty errorMap['email']}">
                <span class="errorMessage">${required}</span>
              </c:if>
            </li>
            <li>
              <fmt:message var="zipPlace" key="common.postalCode"/>
              <%-- Check for errors in "Postal Code" --%>
              <c:if test="${not empty errorMap['postalCode']}">
                <c:set var="divClassPostalCode" value="bottom mobile_store_error_state"/>
              </c:if>
              <div class="left ${divClassPostalCode}">
                <div class="content">
                  <dsp:input iclass="mobile_store_textBoxForm" id="mobile_store_editZip" maxlength="10" size="10"
                             bean="RegistrationFormHandler.value.homeAddress.postalCode"
                             type="tel" required="false">
                    <dsp:tagAttribute name="placeholder" value="${zipPlace}"/>
                  </dsp:input>
                </div>
              </div>
              <div class="right">
                <%-- "Phone" --%>
                <fmt:message var="phonePlace" key="common.phone"/>
                <div class="contentAlternative">
                  <dsp:input iclass="mobile_store_textBoxForm" id="mobile_store_editPhone" maxlength="15" size="10"
                             bean="RegistrationFormHandler.value.homeAddress.phoneNumber"
                             type="tel" required="false">
                    <dsp:tagAttribute name="placeholder" value="${phonePlace}"/>
                  </dsp:input>
                </div>
              </div>
            </li>
            <li>
              <%-- "Gender" --%>
              <div class="left">
                <div class="content">
                  <dsp:getvalueof var="selectedGender" bean="RegistrationFormHandler.value.gender"/>
                  <dsp:getvalueof var="selectStyle" value="${(selectedGender == 'unknown') ? ' default' : ''}"/>
                  <%-- No permanent styles should be applied to this select using a class --%>
                  <dsp:select onchange="changeDropdown(event);" iclass="mobile_store_selectForm${selectStyle}"
                              name="genderSelect" id="mobile_store_editGender" bean="RegistrationFormHandler.value.gender">
                    <%-- Get genders from droplet PossibleValues need to pass parameter itemDescriptorName as user and propertyName as gender --%>
                    <dsp:droplet name="PossibleValues">
                      <dsp:param name="itemDescriptorName" value="user"/>
                      <dsp:param name="propertyName" value="gender"/>
                      <dsp:param name="repository" bean="ProfileAdapterRepository"/>
                      <dsp:param name="returnValueObjects" value="true"/>
                      <dsp:param name="comparator" bean="/atg/userprofiling/ProfileEnumOptionsComparator"/>
                      <dsp:param name="bundle" bean="/atg/multisite/Site.resourceBundle"/>
                      <dsp:oparam name="output">
                        <%-- Get the output parameter of PossibleValues Droplet and put it into the forEach --%>
                        <dsp:getvalueof var="values" vartype="java.lang.Object" param="values"/>
                        <c:forEach var="gender" items="${values}">
                          <c:choose>
                            <%-- If value is unknown, show title --%>
                            <c:when test="${gender.settableValue == 'unknown'}">
                              <dsp:option iclass="default" value="${gender.settableValue}">
                                <fmt:message key="common.selectGender"/>
                              </dsp:option>
                            </c:when>
                            <%-- Otherwise get possible values from repository --%>
                            <c:otherwise>
                              <dsp:option value="${gender.settableValue}">
                                <c:out value="${gender.localizedLabel}"/>
                              </dsp:option>
                            </c:otherwise>
                          </c:choose>
                        </c:forEach>
                      </dsp:oparam>
                    </dsp:droplet>
                  </dsp:select>
                </div>
              </div>
              <%-- Check for errors in "DateOfBirth" --%>
              <c:if test="${not empty errorMap['dateOfBirth']}">
                <c:set var="divClassDob" value="mobile_store_error_state"/>
              </c:if>
              <div class="right ${divClassDob}">
                <fmt:message var="dobPlace" key="common.DOB"/>
                <div class="contentAlternative">
                  <dsp:input iclass="mobile_store_textBoxForm" id="mobile_store_editDob"
                           maxlength="10" size="15" bean="RegistrationFormHandler.dateOfBirth" type="text" required="false">
                    <dsp:tagAttribute name="placeholder" value="${dobPlace}"/>
                  </dsp:input>
                </div>
                <dsp:input type="hidden" bean="RegistrationFormHandler.dateFormat" value="${dateFormat}"/>
                <c:if test="${not empty errorMap['dateOfBirth']}">
                  <span class="errorMessage noWrap">
                    <dsp:getvalueof var="exampleDate" bean="StoreConfiguration.epochDate"/>
                    <fmt:formatDate var="formattedExampleDate" type="date" value="${exampleDate}" pattern="${dateFormat}"/>
                    <fmt:message key="common.dateFormatDisplay">
                      <fmt:param value="${formattedExampleDate}"/>
                    </fmt:message>
                  </span>
                </c:if>
              </div>
            </li>
            <li>
              <div class="content withQuestion">
                <div class="checkbox">
                  <%-- Get initial checkbox value and set new value --%>
                  <dsp:droplet name="IsEmailRecipient">
                    <dsp:param name="email" bean="RegistrationFormHandler.value.email"/>
                    <dsp:oparam name="true">
                      <dsp:input bean="RegistrationFormHandler.previousOptInStatus" type="hidden" value="true"/>
                      <c:choose>
                        <c:when test="${empty formExceptions}">
                          <dsp:input id="mobile_store_email" type="checkbox" bean="RegistrationFormHandler.emailOptIn" checked="true"/>
                        </c:when>
                        <c:otherwise>
                          <dsp:input id="mobile_store_email" type="checkbox" bean="RegistrationFormHandler.emailOptIn"/>
                        </c:otherwise>
                      </c:choose>
                    </dsp:oparam>
                    <dsp:oparam name="false">
                      <dsp:input bean="RegistrationFormHandler.previousOptInStatus" type="hidden" value="false"/>
                      <c:choose>
                        <c:when test="${empty formExceptions}">
                          <dsp:input id="mobile_store_email" type="checkbox" bean="RegistrationFormHandler.emailOptIn" checked="false"/>
                        </c:when>
                        <c:otherwise>
                          <dsp:input id="mobile_store_email" type="checkbox" bean="RegistrationFormHandler.emailOptIn"/>
                        </c:otherwise>
                      </c:choose>
                    </dsp:oparam>
                  </dsp:droplet>
                  <span class="mobile_store_myaccount_questionImage">
                    <fmt:message var="privacyTitle" key="mobile.company_privacyAndTerms.pageTitle"/>
                    <dsp:a page="/mobile/company/terms.jsp" title="${privacyTitle}"/>
                  </span>
                  <label class="mobile_store_myaccount_personalCheckBox" for="mobile_store_email" onclick="">
                    <fmt:message key="common.receiveEmails"/>
                  </label>
                </div>
              </div>
            </li>
          </ul>
        </div>

        <%-- "Submit" button --%>
        <div class="mobile_store_myaccount_button">
          <div class="mobile_store_basicButton">
            <fmt:message var="done" key="mobile.common.done"/>
            <dsp:input id="mobile_store_editProfileButton" bean="RegistrationFormHandler.update"
                       class="mainActionBtn profileEdit" type="submit" value="${done}"/>
          </div>
        </div>
      </dsp:form>
      <br/>
    </div>
  </crs:mobilePageContainer>
</dsp:page>
<%-- @updated $DateTime: 2012/03/07 18:24:16 $$Author: jsiddaga $ --%>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/myaccount/accountProfileEdit.jsp#5 $$Change: 692002 $--%>
