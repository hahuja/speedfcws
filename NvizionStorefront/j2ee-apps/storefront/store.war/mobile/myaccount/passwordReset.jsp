<%--
  Layout page for reset the password.

  Page includes:
    None

  Required Parameters:
    checkoutLogin
      If true, this takes place in the checkout workflow.  If empty or false,
      this takes place on the normal login page.

  Optional parameters:
    None
--%>
<dsp:page>
  <dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>
  <dsp:importbean bean="/atg/userprofiling/ForgotPasswordHandler"/>

  <dsp:getvalueof var="formExceptions" vartype="java.lang.Object" bean="ForgotPasswordHandler.formExceptions"/>
  <dsp:getvalueof var="checkoutLogin" param="checkoutLogin" />

  <%-- Handle form exceptions --%>
  <jsp:useBean id="errorMap" class="java.util.HashMap"/>
  <c:if test="${not empty formExceptions}">
    <c:forEach var="formException" items="${formExceptions}">
      <dsp:param name="formException" value="${formException}"/>
      <%-- Check the error message code to see what we should do --%>
      <dsp:getvalueof var="errorCode" vartype="java.lang.String" param="formException.errorCode"/>

      <c:choose>
        <c:when test="${'missingRequiredValue' eq errorCode}"><c:set target="${errorMap}" property="email" value="missing"/></c:when>
        <c:when test="${'noSuchProfileError' eq errorCode}"><c:set target="${errorMap}" property="email" value="invalid"/></c:when>
      </c:choose>
    </c:forEach>
  </c:if>

  <fmt:message key="myaccount_login.title" var="pageTitle"/>

  <crs:mobilePageContainer titleString="${pageTitle}">
    <jsp:body>
      <div id="mobile_store_passwordReset" class="mobile_store_displayList">
        <h2><fmt:message key="common.button.passwordResetText"/></h2>

        <p id="mobile_store_passwordResetIntro">
          <fmt:message key="mobile.passwordReset.intro"/>
        </p>

        <dsp:form id="mobile_store_passwordResetForm"
                  formid="passwordresetform"
                  action="${originatingRequest.requestURI}"
                  method="post"
                  onsubmit="copyEmailToUrl('mobile_store_passwordResetEmail', 'successUrl')">
          <c:choose>
            <c:when test="${checkoutLogin == 'true'}" >
              <dsp:input bean="ForgotPasswordHandler.forgotPasswordSuccessURL" type="hidden" id="successUrl"
                         value="../checkout/login.jsp?checkoutLogin=true&passwordSent=true&loginErrors=true"/>
              <dsp:input type="hidden" bean="ForgotPasswordHandler.forgotPasswordErrorURL"
                         value="passwordReset.jsp?checkoutLogin=true"/>
            </c:when>
            <c:otherwise>
              <dsp:input bean="ForgotPasswordHandler.forgotPasswordSuccessURL" type="hidden" id="successUrl"
                         value="login.jsp?passwordSent=true&loginErrors=true"/>
              <dsp:input type="hidden" bean="ForgotPasswordHandler.forgotPasswordErrorURL"
                         value="passwordReset.jsp"/>
            </c:otherwise>
          </c:choose>
          <div class="enter_info">
            <ul class="mobile_store_mobileList">
              <%-- Check for errors in email --%>
              <c:if test="${not empty errorMap['email']}">
                <c:set var="liClassEmail" value="mobile_store_error_state"/>
              </c:if>
              <li class="${liClassEmail}">
                <div class="content">
                  <fmt:message var="nickEmail" key="mobile.passwordReset.email"/>
                  <dsp:input type="email" bean="ForgotPasswordHandler.value.email" size="35" required="true"
                             name="mobile_store_passwordResetEmail" id="mobile_store_passwordResetEmail" 
                             placeholder="${nickEmail}" autocapitalize="off"/>
                </div>
                <c:if test="${not empty errorMap['email']}">
                  <span class="errorMessage">
                    <fmt:message key="mobile.form.validation.${errorMap['email']}"/>
                  </span>
                </c:if>
              </li>
            </ul>

            <div class="mobile_store_formActions">
              <fmt:message var="submitText" key="common.button.sendText"/>
              <dsp:input bean="ForgotPasswordHandler.forgotPassword" type="submit" name="mobile_store_passwordResetSubmit"
                         id="mobile_store_passwordResetSubmit" value="${submitText}"/>
            </div>
          </div>
        </dsp:form>
      </div>
    </jsp:body>
  </crs:mobilePageContainer>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/myaccount/passwordReset.jsp#3 $$Change: 692002 $--%>
