<%--
  Renders product details to the page
  
  Required parameters:
    picker
      the path to the JSP that will render buttons for this product
    product
      the product repository item whose details being displayed
  
  Optional parameters:
    selectedSku
      The currently selected SKU. If present, signifies that this is a single-SKU product
    selectedQty
      The currently selected quantity
    selectedSize
      The currently selected size
    selectedColor
      The currently selected color
    availableSizes
      Available sizes for this product
    availableColors
      Available colors for this product
    oneSize
      If true, this product only has one size
    oneColor
      If true, this product only has one color
    isUpdateCart
      If true, changes default button text
--%>
<dsp:page>
  <dsp:importbean bean="/atg/store/droplet/CatalogItemFilterDroplet"/>
  <dsp:importbean bean="/atg/store/mobile/droplet/CurrentSiteItemFilterDroplet"/>
  <dsp:importbean bean="/atg/store/droplet/SkuAvailabilityLookup"/>
  <dsp:getvalueof var="selectedQty" param="selectedQty"/>
  <dsp:getvalueof var="selectedSku" param="selectedSku"/>
  <dsp:getvalueof var="displayName" param="product.displayName"/>
  
  <ul class="mobile_store_productDetails">
    <li class="mobile_store_productTitle" id="mobile_store_productTitle">
      <a href="javascript:history.back(1)" class="icon-arrowleft">
        <c:out value="${displayName}"/>
      </a>
    </li>
    <li class="mobile_store_pickers ${(empty selectedSku) ? '' : 'expanded'}">
      <div class="mobile_store_imageContainer">
        <dsp:getvalueof var="url" param="product.largeImage.url" idtype="java.lang.String"/>	
        <c:if test="${empty url}">
        	<c:set var="url" value="/nrsdocroot/content/images/products/large/MissingProduct_large.jpg"/>
        </c:if>
        <a href="javascript:toggleProductView()">
          <img id="mobile_store_productImage" src="${url}" alt="${displayName}"/>
        </a>
      </div>
      <div id="mobile_store_pickersContent" onclick="expandPickers();">
        <dsp:include page="gadgets/productPrice.jsp">
          <dsp:param name="product" param="product"/>
          <dsp:param name="selectedSku" param="selectedSku"/>
        </dsp:include>
        <ul>
          <dsp:getvalueof var="picker" param="picker"/>
          <c:if test="${!empty picker}">
            <dsp:include page="${picker}">
              <dsp:param name="product" param="product"/>
              <dsp:param name="selectedSku" param="selectedSku"/>
              <dsp:param name="selectedSize" param="selectedSize"/>
              <dsp:param name="selectedColor" param="selectedColor"/>
              <dsp:param name="availableSizes" param="availableSizes"/>
              <dsp:param name="availableColors" param="availableColors"/>
              <dsp:param name="oneSize" param="oneSize"/>
              <dsp:param name="oneColor" param="oneColor"/>
            </dsp:include>
          </c:if>
          <li id="mobile_store_qtyContainer">
            <dsp:include page="gadgets/quantityPickerList.jsp">
              <dsp:param name="selectedValue" param="selectedQty"/>
              <dsp:param name="id" value="mobile_store_qtySelect"/>
            </dsp:include>
          </li>
        </ul>
        <div id="mobile_store_productActionsContainer">
          <div id="mobile_store_addToCartButton">
            <%-- Determine default button text --%>
            <dsp:getvalueof var="isUpdateCart" param="isUpdateCart"/>

			<%-- Default values --%>
			<c:choose>
				<c:when test="${empty isUpdateCart || !isUpdateCart}">
					<fmt:message var="buttonLabel" key="common.button.addToCartText"/>
				</c:when>
				<c:otherwise>
					<fmt:message var="buttonLabel" key="mobile.productDetails.backtocart" />
				</c:otherwise>
			</c:choose>
        	
        	<c:set var="buttonOnClick" value="skuFunctions.cartLinkClick(event);"/>
       		<c:set var="buttonText" value=""/>

			<%-- Customization --%>
			<c:if test="${not empty selectedSku}">
                <dsp:droplet name="SkuAvailabilityLookup">
                  <dsp:param name="product" param="product"/>
                  <dsp:param name="skuId" value="${selectedSku.repositoryId}"/>
                  <dsp:oparam name="preorderable">
                     <fmt:message key="mobile.button.preorderText" var="buttonText" />

                  	 <c:if test="${empty isUpdateCart || !isUpdateCart}">                         
                     	<fmt:message key="mobile.button.preorderLabel" var="buttonLabel"/>
               	     </c:if>
                  </dsp:oparam>
                  <dsp:oparam name="backorderable">
                     <fmt:message key="mobile.button.backorderText" var="buttonText"/>
                  </dsp:oparam>
                  <dsp:oparam name="unavailable">
                     <fmt:message key="mobile.button.emailMeText" var="buttonLabel"/>
                     <fmt:message key="common.temporarilyOutOfStock" var="buttonText"/>
                     <c:set var="buttonOnClick" value="skuFunctions.emailMe();"/>
                  </dsp:oparam>
                </dsp:droplet>
            </c:if> 
            
            <span id="mobile_store_buttonText">
              <c:out value="${buttonText}"/>
            </span>
            <button class="mainActionBtn" onclick="event.stopPropagation(); ${buttonOnClick}" <c:if test="${empty selectedSku}">disabled</c:if> > 
               <c:out value="${buttonLabel}"/>
            </button>            
          </div>
        </div>
      </div>
    </li>
    <li class="mobile_store_details">

      <p class="mobile_store_detailsTitles"><fmt:message key="browse_moreDetails.details"/></p>

      <p class="mobile_store_detailsContent"><dsp:valueof param="product.description"/></p>

      <dsp:getvalueof var="features" param="product.features"/>
      <%-- Don't display "Features" header if there are no features --%>
      <c:if test="${!empty features}">
        <p class="mobile_store_detailsTitles"><fmt:message key="common.features"/></p>

        <p class="mobile_store_detailsContent">
          <c:forEach var="feature" items="${features}">
            <dsp:param name="feature" value="${feature}"/>
            <dsp:getvalueof var="longDescription" param="feature.longDescription"/>
            <c:choose>
              <c:when test="${empty longDescription}">
                <dsp:valueof param="feature.description"/>
              </c:when>
              <c:otherwise>
                <dsp:valueof param="feature.longDescription"/>
              </c:otherwise>
            </c:choose>
          </c:forEach>
        </p>
      </c:if>
      <%-- Don't show "Related Items" if product has no related items --%>
      <dsp:droplet name="CatalogItemFilterDroplet">
        <dsp:param name="collection" param="product.relatedProducts"/>
        <dsp:oparam name="output">
          <dsp:droplet name="CurrentSiteItemFilterDroplet">
            <dsp:param name="collection" param="filteredCollection"/>
            <dsp:oparam name="output">
              <dsp:getvalueof var="relatedProducts" param="filteredCollection"/>
            </dsp:oparam>
          </dsp:droplet>
        </dsp:oparam>
      </dsp:droplet>
      <c:if test="${not empty relatedProducts}">
        <%-- include related products title --%>
        <p class="mobile_store_detailsTitles"><fmt:message key="mobile.productDetails.related"/></p>
      </c:if>
    </li>
    <%-- include related products --%>
    <c:if test="${not empty relatedProducts}">
      <li class="mobile_store_relatedItems">
        <dsp:include page="gadgets/relatedProducts.jsp">
          <dsp:param name="relatedProducts" value="${relatedProducts}"/>
        </dsp:include>
      </li>
    </c:if>
  </ul>
  
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/browse/productDetailsContainer.jsp#3 $$Change: 692002 $--%>
