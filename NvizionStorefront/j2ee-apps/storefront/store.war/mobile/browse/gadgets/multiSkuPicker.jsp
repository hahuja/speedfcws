<%--
  Renders pickers for a multi-SKU product

  Page includes:
    /mobile/browse/gadgets/pickerList.jsp
    
  Required parameters:
    product
      the current product
    selectedSku
      the currently-selected SKU
    
  Unused parameters:
    availableSizes
    availableColors
    selectedSize
    selectedColor
    oneSize
    oneColor
--%>
<dsp:page>
  <dsp:importbean bean="/atg/store/droplet/CatalogItemFilterDroplet"/>
  <fmt:message key="facet.label.Feature" var="featureLabel"/>

  <%-- 
    By this point, CatalogItemFilterDroplet has already been used
    in productDetailMultiSku.jsp. However, passing the resulting
    filteredCollection would mean adding another parameter to the
    growing list. Instead, we opt to use CatalogItemFilterDroplet again.
  --%>
  <dsp:droplet name="CatalogItemFilterDroplet">
    <dsp:param name="collection" param="product.childSKUs"/>
    <dsp:oparam name="output">
      <li>
        <dsp:include page="pickerList.jsp">
          <dsp:param name="id" value="mobile_store_featureSelect"/>
          <dsp:param name="collection" param="filteredCollection"/>
          <dsp:param name="type" value="feature"/>
          <dsp:param name="callback" value="skuFunctions.multiSkuPickerSelect"/>
          <dsp:param name="selectedValue" param="selectedSku.repositoryId"/>
          <dsp:param name="valueProperty" value="repositoryId"/>
          <dsp:param name="labelProperty" value="displayName"/>
          <dsp:param name="defaultLabel" value="${featureLabel}"/>
        </dsp:include>
      </li>
    </dsp:oparam>
  </dsp:droplet>
</dsp:page>