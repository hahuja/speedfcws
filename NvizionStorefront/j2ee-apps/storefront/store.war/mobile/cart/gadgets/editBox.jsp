<%--
  This page generates a commerce item editing box template.

  Page includes:
    None

  Required parameters:
    None

  Optional parameters:
    None
--%>
<dsp:page>
  <dsp:importbean bean="/atg/store/order/purchase/CartFormHandler"/>  

  <%-- Edit cart template --%>
  <div class="mobile_store_editCartBox">
    <%-- Placeholder for product image --%>
    <div class="image">
    </div>

    <div class="cover">
    </div>

    <%-- Placeholder for product name --%>
    <p class="name">
    </p>

    <dsp:a href="javascript:void(0);" iclass="mobile_store_productDetailPageLink">
      <span class="qty">
        <fmt:message key="common.quantity" var="quantityAbbrExpansion"/>
        <abbr title="${quantityAbbrExpansion}"><fmt:message key="common.qty"/></abbr>
      </span>
      <span class="property mobile_store_quantity">
      </span>
    </dsp:a>

    <fmt:message key="mobile.button.share" var="shareLink"/>
    <dsp:a href="javascript:void(0);" title="${shareLink}" iclass="mobile_store_shareLink"></dsp:a>

    <fmt:message key="mobile.button.removeText" var="removeText"/>
    <dsp:a href="javascript:void(0);" title="${removeText}" iclass="mobile_store_moveLink"></dsp:a>
  </div>



  <dsp:input id="mobile_store_removeItemFromOrder" type="submit" bean="CartFormHandler.removeItemFromOrder"/>


</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/cart/gadgets/editBox.jsp#3 $$Change: 692002 $--%>
