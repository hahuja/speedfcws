<%--
  This gadget renders either the text box to submit a promotion code, or a message displaying
  the applied promotion code.

  Form Condition:
    This gadget must be contained inside of a form.
    CartFormHandler must be invoked from a submit button in the form for these fields to be processed

  Required parameters:
    None

  Optional parameters:
    None
  --%>
<dsp:page>
  <dsp:importbean bean="/atg/store/order/purchase/CouponFormHandler"/>
  <dsp:importbean bean="/atg/commerce/ShoppingCart"/>

  <dsp:getvalueof var="couponCode" bean="CouponFormHandler.currentCouponCode"/>
  <dsp:getvalueof var="formExceptions" bean="CouponFormHandler.formExceptions"/>

  <fmt:message var="promotionCode" key="common.cart.promotionCode"/>

  <div class="couponCode">
    <input type="hidden" id="mobile_store_promotionPreviousCode" value="${couponCode}"/>
    <c:choose>
      <c:when test="${not empty formExceptions}">
        <dsp:input bean="CouponFormHandler.couponCode" priority="10"
                   type="text" id="mobile_store_promotionCodeInput"
                   iclass="error" autocomplete="off" size="10" onblur="applyCoupon();" onkeypress="applyNewCouponOnEnter();">
          <dsp:tagAttribute name="placeholder" value="${promotionCode}"/>
        </dsp:input>
      </c:when>
      <c:otherwise>
        <dsp:input bean="CouponFormHandler.couponCode" priority="10"
                   type="text" id="mobile_store_promotionCodeInput"
                   value="${couponCode}" autocomplete="off" size="10" onblur="applyCoupon();" onkeypress="applyNewCouponOnEnter();">
          <dsp:tagAttribute name="placeholder" value="${promotionCode}"/>
        </dsp:input>
		<label for="mobile_store_promotionCodeInput"/>
      </c:otherwise>
    </c:choose>

    <dsp:input id="mobile_store_applyCoupon" bean="CouponFormHandler.claimCoupon" type="submit" value="updateTotalText"/>
  </div>

  <dsp:input bean="CouponFormHandler.applyCouponSuccessURL" type="hidden" value="${requestURI}"/>
  <dsp:input bean="CouponFormHandler.applyCouponErrorURL" type="hidden" value="${requestURI}"/>
  <dsp:input bean="CouponFormHandler.editCouponSuccessURL" type="hidden" value="${requestURI}"/>
  <dsp:input bean="CouponFormHandler.editCouponErrorURL" type="hidden" value="${requestURI}"/>

</dsp:page>

<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/cart/gadgets/promotionCode.jsp#5 $$Change: 692002 $--%>