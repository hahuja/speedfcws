<%--
  This page renders empty shopping cart.

  Page includes:
    /mobile/global/gadgets/formattedPrice.jsp price formatter
    /mobile/cart/gadgets/getTargeterProduct.jsp return product from the targeter
    /global/gadgets/productLinkGenerator.jsp product link generator

  Required parameters:
    None

  Optional parameters:
    None
--%>
<dsp:page>
  <dsp:importbean bean="/atg/targeting/TargetingRandom"/>
  <dsp:importbean bean="/atg/store/catalog/ItemSiteFilter"/>

  <div id="mobile_store_cartContainer">
    <div class="mobile_store_cart empty_cart">
      <div class="mobile_store_orderSummary">
        <div class="mobile_store_orderInfo">
          <dl>
            <dt class="total"><fmt:message key="common.total"/><fmt:message key="common.labelSeparator"/></dt>
            <dd class="total">
              <dsp:include page="/global/gadgets/formattedPrice.jsp">
                <dsp:param name="price" value="0"/>
              </dsp:include>
            </dd>
          </dl>
        </div>
      </div>
    </div>

    <jsp:useBean id="featuredProducts" class="java.util.HashMap"/>
    <dsp:include page="/mobile/cart/gadgets/getTargeterProduct.jsp">
      <dsp:param name="targeter" bean="/atg/registry/Slots/FeaturedProduct1"/>
    </dsp:include>
    <c:set target="${featuredProducts}" property="FeaturedProduct1" value="${requestScope.product}"/>
    <dsp:include page="/mobile/cart/gadgets/getTargeterProduct.jsp">
      <dsp:param name="targeter" bean="/atg/registry/Slots/FeaturedProduct2"/>
    </dsp:include>
    <c:set target="${featuredProducts}" property="FeaturedProduct2" value="${requestScope.product}"/>
    <dsp:include page="/mobile/cart/gadgets/getTargeterProduct.jsp">
      <dsp:param name="targeter" bean="/atg/registry/Slots/FeaturedProduct3"/>
    </dsp:include>
    <c:set target="${featuredProducts}" property="FeaturedProduct3" value="${requestScope.product}"/>
    <dsp:include page="/mobile/cart/gadgets/getTargeterProduct.jsp">
      <dsp:param name="targeter" bean="/atg/registry/Slots/FeaturedProduct4"/>
    </dsp:include>
    <c:set target="${featuredProducts}" property="FeaturedProduct4" value="${requestScope.product}"/>
    <dsp:include page="/mobile/cart/gadgets/getTargeterProduct.jsp">
      <dsp:param name="targeter" bean="/atg/registry/Slots/FeaturedProduct5"/>
    </dsp:include>
    <c:set target="${featuredProducts}" property="FeaturedProduct5" value="${requestScope.product}"/>

    <c:if test="${not empty featuredProducts}">
      <div class="mobile_store_featuredItems">
        <p class="mobile_store_detailsTitles">
          <fmt:message key="browse_featuredProducts.featuredItemTitle"/>
        </p>
        <div class="strip"></div>
        <div id="mobile_store_featuredItemsContainer">
          <c:forEach var="featuredProductEntry" items="${featuredProducts}">
            <div class="cell">
              <dsp:param name="featuredProduct" value="${featuredProductEntry.value}"/>
              <dsp:getvalueof var="featuredImageUrl" param="featuredProduct.smallImage.url"/>
              <dsp:getvalueof var="featuredProductId" param="featuredProduct.repositoryId"/>
              <dsp:getvalueof var="featuredProductName" param="featuredProduct.displayName"/>
               <dsp:tomap var="mapFeaturedProduct" param="featuredProduct"/>
              <dsp:include page="/global/gadgets/productLinkGenerator.jsp">
                <dsp:param name="product" param="featuredProduct"/>
                <dsp:param name="siteId" param="${mapFeaturedProduct.auxiliaryData.siteId}"/>
              </dsp:include>
              <a href="${fn:escapeXml(productUrl)}">
                <img src="${featuredImageUrl}" alt="${featuredProductName}"/>
              </a>
            </div>
          </c:forEach>
        </div>
        <script type="text/javascript">
          $(document).ready(initFeaturedItemsSlidePanel);
        </script>
      </div>
    </c:if>
  </div>

</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/cart/emptyCart.jsp#3 $$Change: 692002 $ --%>