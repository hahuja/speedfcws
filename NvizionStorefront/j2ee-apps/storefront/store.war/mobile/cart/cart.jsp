<%--
  Shopping cart page

  Page includes:
    /mobile/cart/emptyCart.jsp renderer of empty cart
    /mobile/cart/cartContent.jsp renderer of cart with items

  Required parameters:
    None

  Optional parameters:
    None
--%>

<dsp:page>
  <dsp:importbean bean="/atg/commerce/ShoppingCart"/>
  <dsp:importbean bean="/atg/commerce/order/purchase/RepriceOrderDroplet"/>
  <dsp:importbean bean="/atg/multisite/Site"/>

  <fmt:message key="common.cart.shoppingCart" var="pageTitle"/>
  <crs:mobilePageContainer titleString="${pageTitle}">
    <jsp:attribute name="modalContent">
      <%-- Move dialog template --%>
      <div class="mobile_store_moveDialog">
        <div class="mobile_store_moveItems">
          <ul>
            <li class="remove">
              <fmt:message key="mobile.button.removeText" var="removeText"/>
              <dsp:a iclass="mobile_store_removeLink" href="javascript:void(0);" title="${removeText}">${removeText}</dsp:a>
            </li>
          </ul>
        </div>
      </div>
      <%-- Share dialog template --%>
      <div class="mobile_store_shareDialog">
        <div class="mobile_store_shareItems">
          <ul>
            <li class="email">
              <fmt:message var="emailSubject" key="emailtemplates_emailAFriend.subject">
                <fmt:param>
                  <dsp:valueof bean="Site.name"/>
                </fmt:param>
              </fmt:message>
              <a href="mailto:?subject=<c:out value="${emailSubject}" escapeXml="true"/>&amp;body=<c:out value="${emailSubject}" escapeXml="true"/>"><fmt:message key="browse_emailAFriend.title"/></a>
            </li>
          </ul>
        </div>
      </div>
    </jsp:attribute>
    <jsp:body>

      <dsp:droplet name="RepriceOrderDroplet">
        <dsp:param name="pricingOp" value="ORDER_SUBTOTAL"/>
      </dsp:droplet>

      <dsp:getvalueof var="items" vartype="java.lang.Object" bean="ShoppingCart.current.commerceItems"/>
      <c:choose>
        <c:when test="${empty items}">
          <dsp:include page="emptyCart.jsp"/>
        </c:when>

        <c:otherwise>
          <dsp:include page="cartContent.jsp"/>
        </c:otherwise>
      </c:choose>

    </jsp:body>
  </crs:mobilePageContainer>

</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/cart/cart.jsp#3 $$Change: 692002 $ --%>