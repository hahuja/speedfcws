<%--
  Shows a phone link

  Required parameters:
    None

  Optional parameters:
    None
 --%>
<dsp:page>
  <fmt:message var="phoneLabel" key="common.phone" />
  <a class="contact" href="tel:<fmt:message key='mobile.contactUs.phoneNumber'/>" title="${phoneLabel}">
    <img src="/nrsdocroot/content/mobile/images/icon-phoneDial.png" alt=""/>
    <span>${phoneLabel}</span>
  </a>
</dsp:page>

<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/10.1/Storefront/j2ee/store.war/mobile/includes/gadgets/phone.jsp#1 $$Change: 683854 $ --%>