
import javax.xml.namespace.QName;
import javax.xml.rpc.ParameterMode;

import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.apache.axis.encoding.XMLType;

public class Client
{
  public static void main(String [] args)
  {
    try {
    	
      String endpointURL = "http://lcws01:9591/CustomerAccountService";
      String msg = null;

      Service service = new Service();
      Call call = (Call) service.createCall();

      call.setTargetEndpointAddress(new java.net.URL(endpointURL));
      call.setOperationName(new QName("CustomerAccountService", "getAccountInformation"));
      call.addParameter("mesg", XMLType.XSD_STRING, ParameterMode.IN);
      call.setReturnType(org.apache.axis.encoding.XMLType.XSD_STRING);

      String reply = (String)call.invoke(new Object[] {msg});

      System.out.println("Reply: "+reply);
    }
    catch (Exception e) {
      System.err.println(e.toString());
    }
  }
}