/**
 * GetDrugPrice.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri.ScriptSaveServices.Processor;

public class GetDrugPrice  implements java.io.Serializable {
    private java.lang.String groupId;

    private java.lang.String NCPDP_NPI;

    private java.lang.String labelName;

    private java.lang.String NDC11;

    private double quantity;

    private double unC;

    private double daysSupply;

    public GetDrugPrice() {
    }

    public GetDrugPrice(
           java.lang.String groupId,
           java.lang.String NCPDP_NPI,
           java.lang.String labelName,
           java.lang.String NDC11,
           double quantity,
           double unC,
           double daysSupply) {
           this.groupId = groupId;
           this.NCPDP_NPI = NCPDP_NPI;
           this.labelName = labelName;
           this.NDC11 = NDC11;
           this.quantity = quantity;
           this.unC = unC;
           this.daysSupply = daysSupply;
    }


    /**
     * Gets the groupId value for this GetDrugPrice.
     * 
     * @return groupId
     */
    public java.lang.String getGroupId() {
        return groupId;
    }


    /**
     * Sets the groupId value for this GetDrugPrice.
     * 
     * @param groupId
     */
    public void setGroupId(java.lang.String groupId) {
        this.groupId = groupId;
    }


    /**
     * Gets the NCPDP_NPI value for this GetDrugPrice.
     * 
     * @return NCPDP_NPI
     */
    public java.lang.String getNCPDP_NPI() {
        return NCPDP_NPI;
    }


    /**
     * Sets the NCPDP_NPI value for this GetDrugPrice.
     * 
     * @param NCPDP_NPI
     */
    public void setNCPDP_NPI(java.lang.String NCPDP_NPI) {
        this.NCPDP_NPI = NCPDP_NPI;
    }


    /**
     * Gets the labelName value for this GetDrugPrice.
     * 
     * @return labelName
     */
    public java.lang.String getLabelName() {
        return labelName;
    }


    /**
     * Sets the labelName value for this GetDrugPrice.
     * 
     * @param labelName
     */
    public void setLabelName(java.lang.String labelName) {
        this.labelName = labelName;
    }


    /**
     * Gets the NDC11 value for this GetDrugPrice.
     * 
     * @return NDC11
     */
    public java.lang.String getNDC11() {
        return NDC11;
    }


    /**
     * Sets the NDC11 value for this GetDrugPrice.
     * 
     * @param NDC11
     */
    public void setNDC11(java.lang.String NDC11) {
        this.NDC11 = NDC11;
    }


    /**
     * Gets the quantity value for this GetDrugPrice.
     * 
     * @return quantity
     */
    public double getQuantity() {
        return quantity;
    }


    /**
     * Sets the quantity value for this GetDrugPrice.
     * 
     * @param quantity
     */
    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }


    /**
     * Gets the unC value for this GetDrugPrice.
     * 
     * @return unC
     */
    public double getUnC() {
        return unC;
    }


    /**
     * Sets the unC value for this GetDrugPrice.
     * 
     * @param unC
     */
    public void setUnC(double unC) {
        this.unC = unC;
    }


    /**
     * Gets the daysSupply value for this GetDrugPrice.
     * 
     * @return daysSupply
     */
    public double getDaysSupply() {
        return daysSupply;
    }


    /**
     * Sets the daysSupply value for this GetDrugPrice.
     * 
     * @param daysSupply
     */
    public void setDaysSupply(double daysSupply) {
        this.daysSupply = daysSupply;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetDrugPrice)) return false;
        GetDrugPrice other = (GetDrugPrice) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.groupId==null && other.getGroupId()==null) || 
             (this.groupId!=null &&
              this.groupId.equals(other.getGroupId()))) &&
            ((this.NCPDP_NPI==null && other.getNCPDP_NPI()==null) || 
             (this.NCPDP_NPI!=null &&
              this.NCPDP_NPI.equals(other.getNCPDP_NPI()))) &&
            ((this.labelName==null && other.getLabelName()==null) || 
             (this.labelName!=null &&
              this.labelName.equals(other.getLabelName()))) &&
            ((this.NDC11==null && other.getNDC11()==null) || 
             (this.NDC11!=null &&
              this.NDC11.equals(other.getNDC11()))) &&
            this.quantity == other.getQuantity() &&
            this.unC == other.getUnC() &&
            this.daysSupply == other.getDaysSupply();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGroupId() != null) {
            _hashCode += getGroupId().hashCode();
        }
        if (getNCPDP_NPI() != null) {
            _hashCode += getNCPDP_NPI().hashCode();
        }
        if (getLabelName() != null) {
            _hashCode += getLabelName().hashCode();
        }
        if (getNDC11() != null) {
            _hashCode += getNDC11().hashCode();
        }
        _hashCode += new Double(getQuantity()).hashCode();
        _hashCode += new Double(getUnC()).hashCode();
        _hashCode += new Double(getDaysSupply()).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetDrugPrice.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/ScriptSaveServices/Processor", ">getDrugPrice"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("groupId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/ScriptSaveServices/Processor", "GroupId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NCPDP_NPI");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/ScriptSaveServices/Processor", "NCPDP_NPI"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("labelName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/ScriptSaveServices/Processor", "LabelName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NDC11");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/ScriptSaveServices/Processor", "NDC11"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("quantity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/ScriptSaveServices/Processor", "Quantity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("unC");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/ScriptSaveServices/Processor", "UnC"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("daysSupply");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/ScriptSaveServices/Processor", "DaysSupply"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
