/**
 * Processor.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri.ScriptSaveServices.Processor;

public interface Processor extends javax.xml.rpc.Service {
    public java.lang.String getProcessorSoapAddress();

    public org.tempuri.ScriptSaveServices.Processor.ProcessorSoap getProcessorSoap() throws javax.xml.rpc.ServiceException;

    public org.tempuri.ScriptSaveServices.Processor.ProcessorSoap getProcessorSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
    public java.lang.String getProcessorSoap12Address();

    public org.tempuri.ScriptSaveServices.Processor.ProcessorSoap getProcessorSoap12() throws javax.xml.rpc.ServiceException;

    public org.tempuri.ScriptSaveServices.Processor.ProcessorSoap getProcessorSoap12(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
