/**
 * ProcessorTestCase.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri.ScriptSaveServices.Processor;

public class ProcessorTestCase extends junit.framework.TestCase {
    public ProcessorTestCase(java.lang.String name) {
        super(name);
    }

    public void testProcessorSoapWSDL() throws Exception {
        javax.xml.rpc.ServiceFactory serviceFactory = javax.xml.rpc.ServiceFactory.newInstance();
        java.net.URL url = new java.net.URL(new org.tempuri.ScriptSaveServices.Processor.ProcessorLocator().getProcessorSoapAddress() + "?WSDL");
        javax.xml.rpc.Service service = serviceFactory.createService(url, new org.tempuri.ScriptSaveServices.Processor.ProcessorLocator().getServiceName());
        assertTrue(service != null);
    }

    public void test1ProcessorSoapGetClosestPharmacies() throws Exception {
        org.tempuri.ScriptSaveServices.Processor.ProcessorSoapStub binding;
        try {
            binding = (org.tempuri.ScriptSaveServices.Processor.ProcessorSoapStub)
                          new org.tempuri.ScriptSaveServices.Processor.ProcessorLocator().getProcessorSoap();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        org.tempuri.ScriptSaveServices.Processor.GetClosestPharmaciesResponseGetClosestPharmaciesResult value = null;
        value = binding.getClosestPharmacies(new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), 0, 0, 0);
        // TBD - validate results
    }

    public void test2ProcessorSoapGetPharmacy() throws Exception {
        org.tempuri.ScriptSaveServices.Processor.ProcessorSoapStub binding;
        try {
            binding = (org.tempuri.ScriptSaveServices.Processor.ProcessorSoapStub)
                          new org.tempuri.ScriptSaveServices.Processor.ProcessorLocator().getProcessorSoap();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        org.tempuri.ScriptSaveServices.Processor.GetPharmacyResponseGetPharmacyResult value = null;
        value = binding.getPharmacy(new java.lang.String(), new java.lang.String());
        // TBD - validate results
    }

    public void test3ProcessorSoapGetDrugBasicNames() throws Exception {
        org.tempuri.ScriptSaveServices.Processor.ProcessorSoapStub binding;
        try {
            binding = (org.tempuri.ScriptSaveServices.Processor.ProcessorSoapStub)
                          new org.tempuri.ScriptSaveServices.Processor.ProcessorLocator().getProcessorSoap();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        java.lang.String[] value = null;
        value = binding.getDrugBasicNames(new java.lang.String(), new java.lang.String());
        // TBD - validate results
    }

    public void test4ProcessorSoapGetDrugLabelNames() throws Exception {
        org.tempuri.ScriptSaveServices.Processor.ProcessorSoapStub binding;
        try {
            binding = (org.tempuri.ScriptSaveServices.Processor.ProcessorSoapStub)
                          new org.tempuri.ScriptSaveServices.Processor.ProcessorLocator().getProcessorSoap();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        org.tempuri.ScriptSaveServices.Processor.GetDrugLabelNamesResponseGetDrugLabelNamesResult value = null;
        value = binding.getDrugLabelNames(new java.lang.String(), new java.lang.String());
        // TBD - validate results
    }

    public void test5ProcessorSoapGetDrugPrice() throws Exception {
        org.tempuri.ScriptSaveServices.Processor.ProcessorSoapStub binding;
        try {
            binding = (org.tempuri.ScriptSaveServices.Processor.ProcessorSoapStub)
                          new org.tempuri.ScriptSaveServices.Processor.ProcessorLocator().getProcessorSoap();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        java.lang.String[] value = null;
        value = binding.getDrugPrice(new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), 0, 0, 0);
        // TBD - validate results
    }

    public void test6ProcessorSoapEnrollMember() throws Exception {
        org.tempuri.ScriptSaveServices.Processor.ProcessorSoapStub binding;
        try {
            binding = (org.tempuri.ScriptSaveServices.Processor.ProcessorSoapStub)
                          new org.tempuri.ScriptSaveServices.Processor.ProcessorLocator().getProcessorSoap();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        java.lang.String[] value = null;
        value = binding.enrollMember(new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), true, true, new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String());
        // TBD - validate results
    }

    public void testProcessorSoap12WSDL() throws Exception {
        javax.xml.rpc.ServiceFactory serviceFactory = javax.xml.rpc.ServiceFactory.newInstance();
        java.net.URL url = new java.net.URL(new org.tempuri.ScriptSaveServices.Processor.ProcessorLocator().getProcessorSoap12Address() + "?WSDL");
        javax.xml.rpc.Service service = serviceFactory.createService(url, new org.tempuri.ScriptSaveServices.Processor.ProcessorLocator().getServiceName());
        assertTrue(service != null);
    }

    public void test7ProcessorSoap12GetClosestPharmacies() throws Exception {
        org.tempuri.ScriptSaveServices.Processor.ProcessorSoap12Stub binding;
        try {
            binding = (org.tempuri.ScriptSaveServices.Processor.ProcessorSoap12Stub)
                          new org.tempuri.ScriptSaveServices.Processor.ProcessorLocator().getProcessorSoap12();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        org.tempuri.ScriptSaveServices.Processor.GetClosestPharmaciesResponseGetClosestPharmaciesResult value = null;
        value = binding.getClosestPharmacies(new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), 0, 0, 0);
        // TBD - validate results
    }

    public void test8ProcessorSoap12GetPharmacy() throws Exception {
        org.tempuri.ScriptSaveServices.Processor.ProcessorSoap12Stub binding;
        try {
            binding = (org.tempuri.ScriptSaveServices.Processor.ProcessorSoap12Stub)
                          new org.tempuri.ScriptSaveServices.Processor.ProcessorLocator().getProcessorSoap12();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        org.tempuri.ScriptSaveServices.Processor.GetPharmacyResponseGetPharmacyResult value = null;
        value = binding.getPharmacy(new java.lang.String(), new java.lang.String());
        // TBD - validate results
    }

    public void test9ProcessorSoap12GetDrugBasicNames() throws Exception {
        org.tempuri.ScriptSaveServices.Processor.ProcessorSoap12Stub binding;
        try {
            binding = (org.tempuri.ScriptSaveServices.Processor.ProcessorSoap12Stub)
                          new org.tempuri.ScriptSaveServices.Processor.ProcessorLocator().getProcessorSoap12();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        java.lang.String[] value = null;
        value = binding.getDrugBasicNames(new java.lang.String(), new java.lang.String());
        // TBD - validate results
    }

    public void test10ProcessorSoap12GetDrugLabelNames() throws Exception {
        org.tempuri.ScriptSaveServices.Processor.ProcessorSoap12Stub binding;
        try {
            binding = (org.tempuri.ScriptSaveServices.Processor.ProcessorSoap12Stub)
                          new org.tempuri.ScriptSaveServices.Processor.ProcessorLocator().getProcessorSoap12();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        org.tempuri.ScriptSaveServices.Processor.GetDrugLabelNamesResponseGetDrugLabelNamesResult value = null;
        value = binding.getDrugLabelNames(new java.lang.String(), new java.lang.String());
        // TBD - validate results
    }

    public void test11ProcessorSoap12GetDrugPrice() throws Exception {
        org.tempuri.ScriptSaveServices.Processor.ProcessorSoap12Stub binding;
        try {
            binding = (org.tempuri.ScriptSaveServices.Processor.ProcessorSoap12Stub)
                          new org.tempuri.ScriptSaveServices.Processor.ProcessorLocator().getProcessorSoap12();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        java.lang.String[] value = null;
        value = binding.getDrugPrice(new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), 0, 0, 0);
        // TBD - validate results
    }

    public void test12ProcessorSoap12EnrollMember() throws Exception {
        org.tempuri.ScriptSaveServices.Processor.ProcessorSoap12Stub binding;
        try {
            binding = (org.tempuri.ScriptSaveServices.Processor.ProcessorSoap12Stub)
                          new org.tempuri.ScriptSaveServices.Processor.ProcessorLocator().getProcessorSoap12();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        java.lang.String[] value = null;
        value = binding.enrollMember(new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), true, true, new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String());
        // TBD - validate results
    }

}
