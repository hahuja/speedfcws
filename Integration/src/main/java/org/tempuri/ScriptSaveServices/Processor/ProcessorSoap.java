/**
 * ProcessorSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri.ScriptSaveServices.Processor;

public interface ProcessorSoap extends java.rmi.Remote {
    public org.tempuri.ScriptSaveServices.Processor.GetClosestPharmaciesResponseGetClosestPharmaciesResult getClosestPharmacies(java.lang.String groupId, java.lang.String address, java.lang.String city, java.lang.String state, java.lang.String zip, double latitude, double longitude, int numberToReturn) throws java.rmi.RemoteException;
    public org.tempuri.ScriptSaveServices.Processor.GetPharmacyResponseGetPharmacyResult getPharmacy(java.lang.String groupId, java.lang.String NCPDP_NPI) throws java.rmi.RemoteException;
    public java.lang.String[] getDrugBasicNames(java.lang.String groupId, java.lang.String search) throws java.rmi.RemoteException;
    public org.tempuri.ScriptSaveServices.Processor.GetDrugLabelNamesResponseGetDrugLabelNamesResult getDrugLabelNames(java.lang.String groupId, java.lang.String basicName) throws java.rmi.RemoteException;
    public java.lang.String[] getDrugPrice(java.lang.String groupId, java.lang.String NCPDP_NPI, java.lang.String labelName, java.lang.String NDC11, double quantity, double unC, double daysSupply) throws java.rmi.RemoteException;
    public java.lang.String[] enrollMember(java.lang.String groupId, java.lang.String cardholderId, java.lang.String firstName, java.lang.String middleInit, java.lang.String lastName, java.lang.String DOB, java.lang.String relationshipCode, java.lang.String gender, java.lang.String effectiveDate, java.lang.String terminationDate, boolean signature1, boolean signature2, java.lang.String storeNumber, java.lang.String address1, java.lang.String address2, java.lang.String city, java.lang.String state, java.lang.String zip, java.lang.String phone, java.lang.String email, java.lang.String externalId) throws java.rmi.RemoteException;
}
