/**
 * ProcessorLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri.ScriptSaveServices.Processor;

public class ProcessorLocator extends org.apache.axis.client.Service implements org.tempuri.ScriptSaveServices.Processor.Processor {

    public ProcessorLocator() {
    }


    public ProcessorLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public ProcessorLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for ProcessorSoap
    private java.lang.String ProcessorSoap_address = "https://services.infotransfer.com/ScriptSaveServices/Processor.asmx";

    public java.lang.String getProcessorSoapAddress() {
        return ProcessorSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String ProcessorSoapWSDDServiceName = "ProcessorSoap";

    public java.lang.String getProcessorSoapWSDDServiceName() {
        return ProcessorSoapWSDDServiceName;
    }

    public void setProcessorSoapWSDDServiceName(java.lang.String name) {
        ProcessorSoapWSDDServiceName = name;
    }

    public org.tempuri.ScriptSaveServices.Processor.ProcessorSoap getProcessorSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(ProcessorSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getProcessorSoap(endpoint);
    }

    public org.tempuri.ScriptSaveServices.Processor.ProcessorSoap getProcessorSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            org.tempuri.ScriptSaveServices.Processor.ProcessorSoapStub _stub = new org.tempuri.ScriptSaveServices.Processor.ProcessorSoapStub(portAddress, this);
            _stub.setPortName(getProcessorSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setProcessorSoapEndpointAddress(java.lang.String address) {
        ProcessorSoap_address = address;
    }


    // Use to get a proxy class for ProcessorSoap12
    private java.lang.String ProcessorSoap12_address = "https://services.infotransfer.com/ScriptSaveServices/Processor.asmx";

    public java.lang.String getProcessorSoap12Address() {
        return ProcessorSoap12_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String ProcessorSoap12WSDDServiceName = "ProcessorSoap12";

    public java.lang.String getProcessorSoap12WSDDServiceName() {
        return ProcessorSoap12WSDDServiceName;
    }

    public void setProcessorSoap12WSDDServiceName(java.lang.String name) {
        ProcessorSoap12WSDDServiceName = name;
    }

    public org.tempuri.ScriptSaveServices.Processor.ProcessorSoap getProcessorSoap12() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(ProcessorSoap12_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getProcessorSoap12(endpoint);
    }

    public org.tempuri.ScriptSaveServices.Processor.ProcessorSoap getProcessorSoap12(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            org.tempuri.ScriptSaveServices.Processor.ProcessorSoap12Stub _stub = new org.tempuri.ScriptSaveServices.Processor.ProcessorSoap12Stub(portAddress, this);
            _stub.setPortName(getProcessorSoap12WSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setProcessorSoap12EndpointAddress(java.lang.String address) {
        ProcessorSoap12_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     * This service has multiple ports for a given interface;
     * the proxy implementation returned may be indeterminate.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (org.tempuri.ScriptSaveServices.Processor.ProcessorSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                org.tempuri.ScriptSaveServices.Processor.ProcessorSoapStub _stub = new org.tempuri.ScriptSaveServices.Processor.ProcessorSoapStub(new java.net.URL(ProcessorSoap_address), this);
                _stub.setPortName(getProcessorSoapWSDDServiceName());
                return _stub;
            }
            if (org.tempuri.ScriptSaveServices.Processor.ProcessorSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                org.tempuri.ScriptSaveServices.Processor.ProcessorSoap12Stub _stub = new org.tempuri.ScriptSaveServices.Processor.ProcessorSoap12Stub(new java.net.URL(ProcessorSoap12_address), this);
                _stub.setPortName(getProcessorSoap12WSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("ProcessorSoap".equals(inputPortName)) {
            return getProcessorSoap();
        }
        else if ("ProcessorSoap12".equals(inputPortName)) {
            return getProcessorSoap12();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://tempuri.org/ScriptSaveServices/Processor", "Processor");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://tempuri.org/ScriptSaveServices/Processor", "ProcessorSoap"));
            ports.add(new javax.xml.namespace.QName("http://tempuri.org/ScriptSaveServices/Processor", "ProcessorSoap12"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("ProcessorSoap".equals(portName)) {
            setProcessorSoapEndpointAddress(address);
        }
        else 
if ("ProcessorSoap12".equals(portName)) {
            setProcessorSoap12EndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
