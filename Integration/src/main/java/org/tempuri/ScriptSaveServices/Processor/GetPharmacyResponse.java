/**
 * GetPharmacyResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri.ScriptSaveServices.Processor;

public class GetPharmacyResponse  implements java.io.Serializable {
    private org.tempuri.ScriptSaveServices.Processor.GetPharmacyResponseGetPharmacyResult getPharmacyResult;

    public GetPharmacyResponse() {
    }

    public GetPharmacyResponse(
           org.tempuri.ScriptSaveServices.Processor.GetPharmacyResponseGetPharmacyResult getPharmacyResult) {
           this.getPharmacyResult = getPharmacyResult;
    }


    /**
     * Gets the getPharmacyResult value for this GetPharmacyResponse.
     * 
     * @return getPharmacyResult
     */
    public org.tempuri.ScriptSaveServices.Processor.GetPharmacyResponseGetPharmacyResult getGetPharmacyResult() {
        return getPharmacyResult;
    }


    /**
     * Sets the getPharmacyResult value for this GetPharmacyResponse.
     * 
     * @param getPharmacyResult
     */
    public void setGetPharmacyResult(org.tempuri.ScriptSaveServices.Processor.GetPharmacyResponseGetPharmacyResult getPharmacyResult) {
        this.getPharmacyResult = getPharmacyResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetPharmacyResponse)) return false;
        GetPharmacyResponse other = (GetPharmacyResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getPharmacyResult==null && other.getGetPharmacyResult()==null) || 
             (this.getPharmacyResult!=null &&
              this.getPharmacyResult.equals(other.getGetPharmacyResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetPharmacyResult() != null) {
            _hashCode += getGetPharmacyResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetPharmacyResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/ScriptSaveServices/Processor", ">getPharmacyResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getPharmacyResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/ScriptSaveServices/Processor", "getPharmacyResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/ScriptSaveServices/Processor", ">>getPharmacyResponse>getPharmacyResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
