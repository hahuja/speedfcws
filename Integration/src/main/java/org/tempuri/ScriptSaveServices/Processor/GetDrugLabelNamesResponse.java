/**
 * GetDrugLabelNamesResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri.ScriptSaveServices.Processor;

public class GetDrugLabelNamesResponse  implements java.io.Serializable {
    private org.tempuri.ScriptSaveServices.Processor.GetDrugLabelNamesResponseGetDrugLabelNamesResult getDrugLabelNamesResult;

    public GetDrugLabelNamesResponse() {
    }

    public GetDrugLabelNamesResponse(
           org.tempuri.ScriptSaveServices.Processor.GetDrugLabelNamesResponseGetDrugLabelNamesResult getDrugLabelNamesResult) {
           this.getDrugLabelNamesResult = getDrugLabelNamesResult;
    }


    /**
     * Gets the getDrugLabelNamesResult value for this GetDrugLabelNamesResponse.
     * 
     * @return getDrugLabelNamesResult
     */
    public org.tempuri.ScriptSaveServices.Processor.GetDrugLabelNamesResponseGetDrugLabelNamesResult getGetDrugLabelNamesResult() {
        return getDrugLabelNamesResult;
    }


    /**
     * Sets the getDrugLabelNamesResult value for this GetDrugLabelNamesResponse.
     * 
     * @param getDrugLabelNamesResult
     */
    public void setGetDrugLabelNamesResult(org.tempuri.ScriptSaveServices.Processor.GetDrugLabelNamesResponseGetDrugLabelNamesResult getDrugLabelNamesResult) {
        this.getDrugLabelNamesResult = getDrugLabelNamesResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetDrugLabelNamesResponse)) return false;
        GetDrugLabelNamesResponse other = (GetDrugLabelNamesResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getDrugLabelNamesResult==null && other.getGetDrugLabelNamesResult()==null) || 
             (this.getDrugLabelNamesResult!=null &&
              this.getDrugLabelNamesResult.equals(other.getGetDrugLabelNamesResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetDrugLabelNamesResult() != null) {
            _hashCode += getGetDrugLabelNamesResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetDrugLabelNamesResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/ScriptSaveServices/Processor", ">getDrugLabelNamesResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getDrugLabelNamesResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/ScriptSaveServices/Processor", "getDrugLabelNamesResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/ScriptSaveServices/Processor", ">>getDrugLabelNamesResponse>getDrugLabelNamesResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
