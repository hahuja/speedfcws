/**
 * CUSTOMERCUSTOMER_ADDRESS.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.xmlns.ei.CUSTOMER;

public class CUSTOMERCUSTOMER_ADDRESS  implements java.io.Serializable {
    private java.math.BigInteger CUSTOMER_ADDRESS_ID;

    private java.math.BigInteger CUSTOMER_ID;

    private java.lang.String ADDRESS_TYPE_CODE;

    private java.lang.String ADDRESS_1_TEXT;

    private java.lang.String ADDRESS_2_TEXT;

    private java.lang.String ADDRESS_3_TEXT;

    private java.lang.String ADDRESS_4_TEXT;

    private java.lang.String CITY_TEXT;

    private java.lang.String ZIP_CODE;

    private java.lang.String ZIP_CODE_EXTENSION;

    private java.lang.String POSTAL_CODE;

    private java.math.BigDecimal LONGITUDE_K;

    private java.math.BigDecimal LATITUDE_K;

    private java.lang.String COUNTY_NAME;

    private java.lang.String STATE_NAME;

    private java.lang.String STATE_CODE;

    private java.lang.String ZIP_10_CODE;

    private java.math.BigInteger COUNTRY_ID;

    private java.lang.String FIPS_CODE;

    private java.lang.String RESIDENCE_TYPE_CODE;

    private java.lang.String MAILABLE_STATUS_SWITCH;

    private java.lang.String MAILABLE_STATUS_REASON_CODE;

    private java.lang.String CUSTOMER_SOURCE_CODE;

    private java.lang.String CREATE_TIMESTAMP;

    private java.lang.String LAST_UPDATE_DATE;

    private java.lang.String LAST_UPDATE_USER_ID;

    private java.math.BigInteger EXTERNAL_ID;

    private java.lang.String FIPS_COUNTY_CODE;

    private java.lang.String FIPS_STATE_CODE;

    private java.lang.String CENS_TRACT_CODE;

    private java.lang.String CENS_BLOCK_CODE;

    public CUSTOMERCUSTOMER_ADDRESS() {
    }

    public CUSTOMERCUSTOMER_ADDRESS(
           java.math.BigInteger CUSTOMER_ADDRESS_ID,
           java.math.BigInteger CUSTOMER_ID,
           java.lang.String ADDRESS_TYPE_CODE,
           java.lang.String ADDRESS_1_TEXT,
           java.lang.String ADDRESS_2_TEXT,
           java.lang.String ADDRESS_3_TEXT,
           java.lang.String ADDRESS_4_TEXT,
           java.lang.String CITY_TEXT,
           java.lang.String ZIP_CODE,
           java.lang.String ZIP_CODE_EXTENSION,
           java.lang.String POSTAL_CODE,
           java.math.BigDecimal LONGITUDE_K,
           java.math.BigDecimal LATITUDE_K,
           java.lang.String COUNTY_NAME,
           java.lang.String STATE_NAME,
           java.lang.String STATE_CODE,
           java.lang.String ZIP_10_CODE,
           java.math.BigInteger COUNTRY_ID,
           java.lang.String FIPS_CODE,
           java.lang.String RESIDENCE_TYPE_CODE,
           java.lang.String MAILABLE_STATUS_SWITCH,
           java.lang.String MAILABLE_STATUS_REASON_CODE,
           java.lang.String CUSTOMER_SOURCE_CODE,
           java.lang.String CREATE_TIMESTAMP,
           java.lang.String LAST_UPDATE_DATE,
           java.lang.String LAST_UPDATE_USER_ID,
           java.math.BigInteger EXTERNAL_ID,
           java.lang.String FIPS_COUNTY_CODE,
           java.lang.String FIPS_STATE_CODE,
           java.lang.String CENS_TRACT_CODE,
           java.lang.String CENS_BLOCK_CODE) {
           this.CUSTOMER_ADDRESS_ID = CUSTOMER_ADDRESS_ID;
           this.CUSTOMER_ID = CUSTOMER_ID;
           this.ADDRESS_TYPE_CODE = ADDRESS_TYPE_CODE;
           this.ADDRESS_1_TEXT = ADDRESS_1_TEXT;
           this.ADDRESS_2_TEXT = ADDRESS_2_TEXT;
           this.ADDRESS_3_TEXT = ADDRESS_3_TEXT;
           this.ADDRESS_4_TEXT = ADDRESS_4_TEXT;
           this.CITY_TEXT = CITY_TEXT;
           this.ZIP_CODE = ZIP_CODE;
           this.ZIP_CODE_EXTENSION = ZIP_CODE_EXTENSION;
           this.POSTAL_CODE = POSTAL_CODE;
           this.LONGITUDE_K = LONGITUDE_K;
           this.LATITUDE_K = LATITUDE_K;
           this.COUNTY_NAME = COUNTY_NAME;
           this.STATE_NAME = STATE_NAME;
           this.STATE_CODE = STATE_CODE;
           this.ZIP_10_CODE = ZIP_10_CODE;
           this.COUNTRY_ID = COUNTRY_ID;
           this.FIPS_CODE = FIPS_CODE;
           this.RESIDENCE_TYPE_CODE = RESIDENCE_TYPE_CODE;
           this.MAILABLE_STATUS_SWITCH = MAILABLE_STATUS_SWITCH;
           this.MAILABLE_STATUS_REASON_CODE = MAILABLE_STATUS_REASON_CODE;
           this.CUSTOMER_SOURCE_CODE = CUSTOMER_SOURCE_CODE;
           this.CREATE_TIMESTAMP = CREATE_TIMESTAMP;
           this.LAST_UPDATE_DATE = LAST_UPDATE_DATE;
           this.LAST_UPDATE_USER_ID = LAST_UPDATE_USER_ID;
           this.EXTERNAL_ID = EXTERNAL_ID;
           this.FIPS_COUNTY_CODE = FIPS_COUNTY_CODE;
           this.FIPS_STATE_CODE = FIPS_STATE_CODE;
           this.CENS_TRACT_CODE = CENS_TRACT_CODE;
           this.CENS_BLOCK_CODE = CENS_BLOCK_CODE;
    }


    /**
     * Gets the CUSTOMER_ADDRESS_ID value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @return CUSTOMER_ADDRESS_ID
     */
    public java.math.BigInteger getCUSTOMER_ADDRESS_ID() {
        return CUSTOMER_ADDRESS_ID;
    }


    /**
     * Sets the CUSTOMER_ADDRESS_ID value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @param CUSTOMER_ADDRESS_ID
     */
    public void setCUSTOMER_ADDRESS_ID(java.math.BigInteger CUSTOMER_ADDRESS_ID) {
        this.CUSTOMER_ADDRESS_ID = CUSTOMER_ADDRESS_ID;
    }


    /**
     * Gets the CUSTOMER_ID value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @return CUSTOMER_ID
     */
    public java.math.BigInteger getCUSTOMER_ID() {
        return CUSTOMER_ID;
    }


    /**
     * Sets the CUSTOMER_ID value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @param CUSTOMER_ID
     */
    public void setCUSTOMER_ID(java.math.BigInteger CUSTOMER_ID) {
        this.CUSTOMER_ID = CUSTOMER_ID;
    }


    /**
     * Gets the ADDRESS_TYPE_CODE value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @return ADDRESS_TYPE_CODE
     */
    public java.lang.String getADDRESS_TYPE_CODE() {
        return ADDRESS_TYPE_CODE;
    }


    /**
     * Sets the ADDRESS_TYPE_CODE value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @param ADDRESS_TYPE_CODE
     */
    public void setADDRESS_TYPE_CODE(java.lang.String ADDRESS_TYPE_CODE) {
        this.ADDRESS_TYPE_CODE = ADDRESS_TYPE_CODE;
    }


    /**
     * Gets the ADDRESS_1_TEXT value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @return ADDRESS_1_TEXT
     */
    public java.lang.String getADDRESS_1_TEXT() {
        return ADDRESS_1_TEXT;
    }


    /**
     * Sets the ADDRESS_1_TEXT value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @param ADDRESS_1_TEXT
     */
    public void setADDRESS_1_TEXT(java.lang.String ADDRESS_1_TEXT) {
        this.ADDRESS_1_TEXT = ADDRESS_1_TEXT;
    }


    /**
     * Gets the ADDRESS_2_TEXT value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @return ADDRESS_2_TEXT
     */
    public java.lang.String getADDRESS_2_TEXT() {
        return ADDRESS_2_TEXT;
    }


    /**
     * Sets the ADDRESS_2_TEXT value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @param ADDRESS_2_TEXT
     */
    public void setADDRESS_2_TEXT(java.lang.String ADDRESS_2_TEXT) {
        this.ADDRESS_2_TEXT = ADDRESS_2_TEXT;
    }


    /**
     * Gets the ADDRESS_3_TEXT value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @return ADDRESS_3_TEXT
     */
    public java.lang.String getADDRESS_3_TEXT() {
        return ADDRESS_3_TEXT;
    }


    /**
     * Sets the ADDRESS_3_TEXT value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @param ADDRESS_3_TEXT
     */
    public void setADDRESS_3_TEXT(java.lang.String ADDRESS_3_TEXT) {
        this.ADDRESS_3_TEXT = ADDRESS_3_TEXT;
    }


    /**
     * Gets the ADDRESS_4_TEXT value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @return ADDRESS_4_TEXT
     */
    public java.lang.String getADDRESS_4_TEXT() {
        return ADDRESS_4_TEXT;
    }


    /**
     * Sets the ADDRESS_4_TEXT value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @param ADDRESS_4_TEXT
     */
    public void setADDRESS_4_TEXT(java.lang.String ADDRESS_4_TEXT) {
        this.ADDRESS_4_TEXT = ADDRESS_4_TEXT;
    }


    /**
     * Gets the CITY_TEXT value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @return CITY_TEXT
     */
    public java.lang.String getCITY_TEXT() {
        return CITY_TEXT;
    }


    /**
     * Sets the CITY_TEXT value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @param CITY_TEXT
     */
    public void setCITY_TEXT(java.lang.String CITY_TEXT) {
        this.CITY_TEXT = CITY_TEXT;
    }


    /**
     * Gets the ZIP_CODE value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @return ZIP_CODE
     */
    public java.lang.String getZIP_CODE() {
        return ZIP_CODE;
    }


    /**
     * Sets the ZIP_CODE value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @param ZIP_CODE
     */
    public void setZIP_CODE(java.lang.String ZIP_CODE) {
        this.ZIP_CODE = ZIP_CODE;
    }


    /**
     * Gets the ZIP_CODE_EXTENSION value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @return ZIP_CODE_EXTENSION
     */
    public java.lang.String getZIP_CODE_EXTENSION() {
        return ZIP_CODE_EXTENSION;
    }


    /**
     * Sets the ZIP_CODE_EXTENSION value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @param ZIP_CODE_EXTENSION
     */
    public void setZIP_CODE_EXTENSION(java.lang.String ZIP_CODE_EXTENSION) {
        this.ZIP_CODE_EXTENSION = ZIP_CODE_EXTENSION;
    }


    /**
     * Gets the POSTAL_CODE value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @return POSTAL_CODE
     */
    public java.lang.String getPOSTAL_CODE() {
        return POSTAL_CODE;
    }


    /**
     * Sets the POSTAL_CODE value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @param POSTAL_CODE
     */
    public void setPOSTAL_CODE(java.lang.String POSTAL_CODE) {
        this.POSTAL_CODE = POSTAL_CODE;
    }


    /**
     * Gets the LONGITUDE_K value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @return LONGITUDE_K
     */
    public java.math.BigDecimal getLONGITUDE_K() {
        return LONGITUDE_K;
    }


    /**
     * Sets the LONGITUDE_K value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @param LONGITUDE_K
     */
    public void setLONGITUDE_K(java.math.BigDecimal LONGITUDE_K) {
        this.LONGITUDE_K = LONGITUDE_K;
    }


    /**
     * Gets the LATITUDE_K value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @return LATITUDE_K
     */
    public java.math.BigDecimal getLATITUDE_K() {
        return LATITUDE_K;
    }


    /**
     * Sets the LATITUDE_K value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @param LATITUDE_K
     */
    public void setLATITUDE_K(java.math.BigDecimal LATITUDE_K) {
        this.LATITUDE_K = LATITUDE_K;
    }


    /**
     * Gets the COUNTY_NAME value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @return COUNTY_NAME
     */
    public java.lang.String getCOUNTY_NAME() {
        return COUNTY_NAME;
    }


    /**
     * Sets the COUNTY_NAME value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @param COUNTY_NAME
     */
    public void setCOUNTY_NAME(java.lang.String COUNTY_NAME) {
        this.COUNTY_NAME = COUNTY_NAME;
    }


    /**
     * Gets the STATE_NAME value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @return STATE_NAME
     */
    public java.lang.String getSTATE_NAME() {
        return STATE_NAME;
    }


    /**
     * Sets the STATE_NAME value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @param STATE_NAME
     */
    public void setSTATE_NAME(java.lang.String STATE_NAME) {
        this.STATE_NAME = STATE_NAME;
    }


    /**
     * Gets the STATE_CODE value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @return STATE_CODE
     */
    public java.lang.String getSTATE_CODE() {
        return STATE_CODE;
    }


    /**
     * Sets the STATE_CODE value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @param STATE_CODE
     */
    public void setSTATE_CODE(java.lang.String STATE_CODE) {
        this.STATE_CODE = STATE_CODE;
    }


    /**
     * Gets the ZIP_10_CODE value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @return ZIP_10_CODE
     */
    public java.lang.String getZIP_10_CODE() {
        return ZIP_10_CODE;
    }


    /**
     * Sets the ZIP_10_CODE value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @param ZIP_10_CODE
     */
    public void setZIP_10_CODE(java.lang.String ZIP_10_CODE) {
        this.ZIP_10_CODE = ZIP_10_CODE;
    }


    /**
     * Gets the COUNTRY_ID value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @return COUNTRY_ID
     */
    public java.math.BigInteger getCOUNTRY_ID() {
        return COUNTRY_ID;
    }


    /**
     * Sets the COUNTRY_ID value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @param COUNTRY_ID
     */
    public void setCOUNTRY_ID(java.math.BigInteger COUNTRY_ID) {
        this.COUNTRY_ID = COUNTRY_ID;
    }


    /**
     * Gets the FIPS_CODE value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @return FIPS_CODE
     */
    public java.lang.String getFIPS_CODE() {
        return FIPS_CODE;
    }


    /**
     * Sets the FIPS_CODE value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @param FIPS_CODE
     */
    public void setFIPS_CODE(java.lang.String FIPS_CODE) {
        this.FIPS_CODE = FIPS_CODE;
    }


    /**
     * Gets the RESIDENCE_TYPE_CODE value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @return RESIDENCE_TYPE_CODE
     */
    public java.lang.String getRESIDENCE_TYPE_CODE() {
        return RESIDENCE_TYPE_CODE;
    }


    /**
     * Sets the RESIDENCE_TYPE_CODE value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @param RESIDENCE_TYPE_CODE
     */
    public void setRESIDENCE_TYPE_CODE(java.lang.String RESIDENCE_TYPE_CODE) {
        this.RESIDENCE_TYPE_CODE = RESIDENCE_TYPE_CODE;
    }


    /**
     * Gets the MAILABLE_STATUS_SWITCH value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @return MAILABLE_STATUS_SWITCH
     */
    public java.lang.String getMAILABLE_STATUS_SWITCH() {
        return MAILABLE_STATUS_SWITCH;
    }


    /**
     * Sets the MAILABLE_STATUS_SWITCH value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @param MAILABLE_STATUS_SWITCH
     */
    public void setMAILABLE_STATUS_SWITCH(java.lang.String MAILABLE_STATUS_SWITCH) {
        this.MAILABLE_STATUS_SWITCH = MAILABLE_STATUS_SWITCH;
    }


    /**
     * Gets the MAILABLE_STATUS_REASON_CODE value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @return MAILABLE_STATUS_REASON_CODE
     */
    public java.lang.String getMAILABLE_STATUS_REASON_CODE() {
        return MAILABLE_STATUS_REASON_CODE;
    }


    /**
     * Sets the MAILABLE_STATUS_REASON_CODE value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @param MAILABLE_STATUS_REASON_CODE
     */
    public void setMAILABLE_STATUS_REASON_CODE(java.lang.String MAILABLE_STATUS_REASON_CODE) {
        this.MAILABLE_STATUS_REASON_CODE = MAILABLE_STATUS_REASON_CODE;
    }


    /**
     * Gets the CUSTOMER_SOURCE_CODE value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @return CUSTOMER_SOURCE_CODE
     */
    public java.lang.String getCUSTOMER_SOURCE_CODE() {
        return CUSTOMER_SOURCE_CODE;
    }


    /**
     * Sets the CUSTOMER_SOURCE_CODE value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @param CUSTOMER_SOURCE_CODE
     */
    public void setCUSTOMER_SOURCE_CODE(java.lang.String CUSTOMER_SOURCE_CODE) {
        this.CUSTOMER_SOURCE_CODE = CUSTOMER_SOURCE_CODE;
    }


    /**
     * Gets the CREATE_TIMESTAMP value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @return CREATE_TIMESTAMP
     */
    public java.lang.String getCREATE_TIMESTAMP() {
        return CREATE_TIMESTAMP;
    }


    /**
     * Sets the CREATE_TIMESTAMP value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @param CREATE_TIMESTAMP
     */
    public void setCREATE_TIMESTAMP(java.lang.String CREATE_TIMESTAMP) {
        this.CREATE_TIMESTAMP = CREATE_TIMESTAMP;
    }


    /**
     * Gets the LAST_UPDATE_DATE value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @return LAST_UPDATE_DATE
     */
    public java.lang.String getLAST_UPDATE_DATE() {
        return LAST_UPDATE_DATE;
    }


    /**
     * Sets the LAST_UPDATE_DATE value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @param LAST_UPDATE_DATE
     */
    public void setLAST_UPDATE_DATE(java.lang.String LAST_UPDATE_DATE) {
        this.LAST_UPDATE_DATE = LAST_UPDATE_DATE;
    }


    /**
     * Gets the LAST_UPDATE_USER_ID value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @return LAST_UPDATE_USER_ID
     */
    public java.lang.String getLAST_UPDATE_USER_ID() {
        return LAST_UPDATE_USER_ID;
    }


    /**
     * Sets the LAST_UPDATE_USER_ID value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @param LAST_UPDATE_USER_ID
     */
    public void setLAST_UPDATE_USER_ID(java.lang.String LAST_UPDATE_USER_ID) {
        this.LAST_UPDATE_USER_ID = LAST_UPDATE_USER_ID;
    }


    /**
     * Gets the EXTERNAL_ID value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @return EXTERNAL_ID
     */
    public java.math.BigInteger getEXTERNAL_ID() {
        return EXTERNAL_ID;
    }


    /**
     * Sets the EXTERNAL_ID value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @param EXTERNAL_ID
     */
    public void setEXTERNAL_ID(java.math.BigInteger EXTERNAL_ID) {
        this.EXTERNAL_ID = EXTERNAL_ID;
    }


    /**
     * Gets the FIPS_COUNTY_CODE value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @return FIPS_COUNTY_CODE
     */
    public java.lang.String getFIPS_COUNTY_CODE() {
        return FIPS_COUNTY_CODE;
    }


    /**
     * Sets the FIPS_COUNTY_CODE value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @param FIPS_COUNTY_CODE
     */
    public void setFIPS_COUNTY_CODE(java.lang.String FIPS_COUNTY_CODE) {
        this.FIPS_COUNTY_CODE = FIPS_COUNTY_CODE;
    }


    /**
     * Gets the FIPS_STATE_CODE value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @return FIPS_STATE_CODE
     */
    public java.lang.String getFIPS_STATE_CODE() {
        return FIPS_STATE_CODE;
    }


    /**
     * Sets the FIPS_STATE_CODE value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @param FIPS_STATE_CODE
     */
    public void setFIPS_STATE_CODE(java.lang.String FIPS_STATE_CODE) {
        this.FIPS_STATE_CODE = FIPS_STATE_CODE;
    }


    /**
     * Gets the CENS_TRACT_CODE value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @return CENS_TRACT_CODE
     */
    public java.lang.String getCENS_TRACT_CODE() {
        return CENS_TRACT_CODE;
    }


    /**
     * Sets the CENS_TRACT_CODE value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @param CENS_TRACT_CODE
     */
    public void setCENS_TRACT_CODE(java.lang.String CENS_TRACT_CODE) {
        this.CENS_TRACT_CODE = CENS_TRACT_CODE;
    }


    /**
     * Gets the CENS_BLOCK_CODE value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @return CENS_BLOCK_CODE
     */
    public java.lang.String getCENS_BLOCK_CODE() {
        return CENS_BLOCK_CODE;
    }


    /**
     * Sets the CENS_BLOCK_CODE value for this CUSTOMERCUSTOMER_ADDRESS.
     * 
     * @param CENS_BLOCK_CODE
     */
    public void setCENS_BLOCK_CODE(java.lang.String CENS_BLOCK_CODE) {
        this.CENS_BLOCK_CODE = CENS_BLOCK_CODE;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CUSTOMERCUSTOMER_ADDRESS)) return false;
        CUSTOMERCUSTOMER_ADDRESS other = (CUSTOMERCUSTOMER_ADDRESS) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.CUSTOMER_ADDRESS_ID==null && other.getCUSTOMER_ADDRESS_ID()==null) || 
             (this.CUSTOMER_ADDRESS_ID!=null &&
              this.CUSTOMER_ADDRESS_ID.equals(other.getCUSTOMER_ADDRESS_ID()))) &&
            ((this.CUSTOMER_ID==null && other.getCUSTOMER_ID()==null) || 
             (this.CUSTOMER_ID!=null &&
              this.CUSTOMER_ID.equals(other.getCUSTOMER_ID()))) &&
            ((this.ADDRESS_TYPE_CODE==null && other.getADDRESS_TYPE_CODE()==null) || 
             (this.ADDRESS_TYPE_CODE!=null &&
              this.ADDRESS_TYPE_CODE.equals(other.getADDRESS_TYPE_CODE()))) &&
            ((this.ADDRESS_1_TEXT==null && other.getADDRESS_1_TEXT()==null) || 
             (this.ADDRESS_1_TEXT!=null &&
              this.ADDRESS_1_TEXT.equals(other.getADDRESS_1_TEXT()))) &&
            ((this.ADDRESS_2_TEXT==null && other.getADDRESS_2_TEXT()==null) || 
             (this.ADDRESS_2_TEXT!=null &&
              this.ADDRESS_2_TEXT.equals(other.getADDRESS_2_TEXT()))) &&
            ((this.ADDRESS_3_TEXT==null && other.getADDRESS_3_TEXT()==null) || 
             (this.ADDRESS_3_TEXT!=null &&
              this.ADDRESS_3_TEXT.equals(other.getADDRESS_3_TEXT()))) &&
            ((this.ADDRESS_4_TEXT==null && other.getADDRESS_4_TEXT()==null) || 
             (this.ADDRESS_4_TEXT!=null &&
              this.ADDRESS_4_TEXT.equals(other.getADDRESS_4_TEXT()))) &&
            ((this.CITY_TEXT==null && other.getCITY_TEXT()==null) || 
             (this.CITY_TEXT!=null &&
              this.CITY_TEXT.equals(other.getCITY_TEXT()))) &&
            ((this.ZIP_CODE==null && other.getZIP_CODE()==null) || 
             (this.ZIP_CODE!=null &&
              this.ZIP_CODE.equals(other.getZIP_CODE()))) &&
            ((this.ZIP_CODE_EXTENSION==null && other.getZIP_CODE_EXTENSION()==null) || 
             (this.ZIP_CODE_EXTENSION!=null &&
              this.ZIP_CODE_EXTENSION.equals(other.getZIP_CODE_EXTENSION()))) &&
            ((this.POSTAL_CODE==null && other.getPOSTAL_CODE()==null) || 
             (this.POSTAL_CODE!=null &&
              this.POSTAL_CODE.equals(other.getPOSTAL_CODE()))) &&
            ((this.LONGITUDE_K==null && other.getLONGITUDE_K()==null) || 
             (this.LONGITUDE_K!=null &&
              this.LONGITUDE_K.equals(other.getLONGITUDE_K()))) &&
            ((this.LATITUDE_K==null && other.getLATITUDE_K()==null) || 
             (this.LATITUDE_K!=null &&
              this.LATITUDE_K.equals(other.getLATITUDE_K()))) &&
            ((this.COUNTY_NAME==null && other.getCOUNTY_NAME()==null) || 
             (this.COUNTY_NAME!=null &&
              this.COUNTY_NAME.equals(other.getCOUNTY_NAME()))) &&
            ((this.STATE_NAME==null && other.getSTATE_NAME()==null) || 
             (this.STATE_NAME!=null &&
              this.STATE_NAME.equals(other.getSTATE_NAME()))) &&
            ((this.STATE_CODE==null && other.getSTATE_CODE()==null) || 
             (this.STATE_CODE!=null &&
              this.STATE_CODE.equals(other.getSTATE_CODE()))) &&
            ((this.ZIP_10_CODE==null && other.getZIP_10_CODE()==null) || 
             (this.ZIP_10_CODE!=null &&
              this.ZIP_10_CODE.equals(other.getZIP_10_CODE()))) &&
            ((this.COUNTRY_ID==null && other.getCOUNTRY_ID()==null) || 
             (this.COUNTRY_ID!=null &&
              this.COUNTRY_ID.equals(other.getCOUNTRY_ID()))) &&
            ((this.FIPS_CODE==null && other.getFIPS_CODE()==null) || 
             (this.FIPS_CODE!=null &&
              this.FIPS_CODE.equals(other.getFIPS_CODE()))) &&
            ((this.RESIDENCE_TYPE_CODE==null && other.getRESIDENCE_TYPE_CODE()==null) || 
             (this.RESIDENCE_TYPE_CODE!=null &&
              this.RESIDENCE_TYPE_CODE.equals(other.getRESIDENCE_TYPE_CODE()))) &&
            ((this.MAILABLE_STATUS_SWITCH==null && other.getMAILABLE_STATUS_SWITCH()==null) || 
             (this.MAILABLE_STATUS_SWITCH!=null &&
              this.MAILABLE_STATUS_SWITCH.equals(other.getMAILABLE_STATUS_SWITCH()))) &&
            ((this.MAILABLE_STATUS_REASON_CODE==null && other.getMAILABLE_STATUS_REASON_CODE()==null) || 
             (this.MAILABLE_STATUS_REASON_CODE!=null &&
              this.MAILABLE_STATUS_REASON_CODE.equals(other.getMAILABLE_STATUS_REASON_CODE()))) &&
            ((this.CUSTOMER_SOURCE_CODE==null && other.getCUSTOMER_SOURCE_CODE()==null) || 
             (this.CUSTOMER_SOURCE_CODE!=null &&
              this.CUSTOMER_SOURCE_CODE.equals(other.getCUSTOMER_SOURCE_CODE()))) &&
            ((this.CREATE_TIMESTAMP==null && other.getCREATE_TIMESTAMP()==null) || 
             (this.CREATE_TIMESTAMP!=null &&
              this.CREATE_TIMESTAMP.equals(other.getCREATE_TIMESTAMP()))) &&
            ((this.LAST_UPDATE_DATE==null && other.getLAST_UPDATE_DATE()==null) || 
             (this.LAST_UPDATE_DATE!=null &&
              this.LAST_UPDATE_DATE.equals(other.getLAST_UPDATE_DATE()))) &&
            ((this.LAST_UPDATE_USER_ID==null && other.getLAST_UPDATE_USER_ID()==null) || 
             (this.LAST_UPDATE_USER_ID!=null &&
              this.LAST_UPDATE_USER_ID.equals(other.getLAST_UPDATE_USER_ID()))) &&
            ((this.EXTERNAL_ID==null && other.getEXTERNAL_ID()==null) || 
             (this.EXTERNAL_ID!=null &&
              this.EXTERNAL_ID.equals(other.getEXTERNAL_ID()))) &&
            ((this.FIPS_COUNTY_CODE==null && other.getFIPS_COUNTY_CODE()==null) || 
             (this.FIPS_COUNTY_CODE!=null &&
              this.FIPS_COUNTY_CODE.equals(other.getFIPS_COUNTY_CODE()))) &&
            ((this.FIPS_STATE_CODE==null && other.getFIPS_STATE_CODE()==null) || 
             (this.FIPS_STATE_CODE!=null &&
              this.FIPS_STATE_CODE.equals(other.getFIPS_STATE_CODE()))) &&
            ((this.CENS_TRACT_CODE==null && other.getCENS_TRACT_CODE()==null) || 
             (this.CENS_TRACT_CODE!=null &&
              this.CENS_TRACT_CODE.equals(other.getCENS_TRACT_CODE()))) &&
            ((this.CENS_BLOCK_CODE==null && other.getCENS_BLOCK_CODE()==null) || 
             (this.CENS_BLOCK_CODE!=null &&
              this.CENS_BLOCK_CODE.equals(other.getCENS_BLOCK_CODE())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCUSTOMER_ADDRESS_ID() != null) {
            _hashCode += getCUSTOMER_ADDRESS_ID().hashCode();
        }
        if (getCUSTOMER_ID() != null) {
            _hashCode += getCUSTOMER_ID().hashCode();
        }
        if (getADDRESS_TYPE_CODE() != null) {
            _hashCode += getADDRESS_TYPE_CODE().hashCode();
        }
        if (getADDRESS_1_TEXT() != null) {
            _hashCode += getADDRESS_1_TEXT().hashCode();
        }
        if (getADDRESS_2_TEXT() != null) {
            _hashCode += getADDRESS_2_TEXT().hashCode();
        }
        if (getADDRESS_3_TEXT() != null) {
            _hashCode += getADDRESS_3_TEXT().hashCode();
        }
        if (getADDRESS_4_TEXT() != null) {
            _hashCode += getADDRESS_4_TEXT().hashCode();
        }
        if (getCITY_TEXT() != null) {
            _hashCode += getCITY_TEXT().hashCode();
        }
        if (getZIP_CODE() != null) {
            _hashCode += getZIP_CODE().hashCode();
        }
        if (getZIP_CODE_EXTENSION() != null) {
            _hashCode += getZIP_CODE_EXTENSION().hashCode();
        }
        if (getPOSTAL_CODE() != null) {
            _hashCode += getPOSTAL_CODE().hashCode();
        }
        if (getLONGITUDE_K() != null) {
            _hashCode += getLONGITUDE_K().hashCode();
        }
        if (getLATITUDE_K() != null) {
            _hashCode += getLATITUDE_K().hashCode();
        }
        if (getCOUNTY_NAME() != null) {
            _hashCode += getCOUNTY_NAME().hashCode();
        }
        if (getSTATE_NAME() != null) {
            _hashCode += getSTATE_NAME().hashCode();
        }
        if (getSTATE_CODE() != null) {
            _hashCode += getSTATE_CODE().hashCode();
        }
        if (getZIP_10_CODE() != null) {
            _hashCode += getZIP_10_CODE().hashCode();
        }
        if (getCOUNTRY_ID() != null) {
            _hashCode += getCOUNTRY_ID().hashCode();
        }
        if (getFIPS_CODE() != null) {
            _hashCode += getFIPS_CODE().hashCode();
        }
        if (getRESIDENCE_TYPE_CODE() != null) {
            _hashCode += getRESIDENCE_TYPE_CODE().hashCode();
        }
        if (getMAILABLE_STATUS_SWITCH() != null) {
            _hashCode += getMAILABLE_STATUS_SWITCH().hashCode();
        }
        if (getMAILABLE_STATUS_REASON_CODE() != null) {
            _hashCode += getMAILABLE_STATUS_REASON_CODE().hashCode();
        }
        if (getCUSTOMER_SOURCE_CODE() != null) {
            _hashCode += getCUSTOMER_SOURCE_CODE().hashCode();
        }
        if (getCREATE_TIMESTAMP() != null) {
            _hashCode += getCREATE_TIMESTAMP().hashCode();
        }
        if (getLAST_UPDATE_DATE() != null) {
            _hashCode += getLAST_UPDATE_DATE().hashCode();
        }
        if (getLAST_UPDATE_USER_ID() != null) {
            _hashCode += getLAST_UPDATE_USER_ID().hashCode();
        }
        if (getEXTERNAL_ID() != null) {
            _hashCode += getEXTERNAL_ID().hashCode();
        }
        if (getFIPS_COUNTY_CODE() != null) {
            _hashCode += getFIPS_COUNTY_CODE().hashCode();
        }
        if (getFIPS_STATE_CODE() != null) {
            _hashCode += getFIPS_STATE_CODE().hashCode();
        }
        if (getCENS_TRACT_CODE() != null) {
            _hashCode += getCENS_TRACT_CODE().hashCode();
        }
        if (getCENS_BLOCK_CODE() != null) {
            _hashCode += getCENS_BLOCK_CODE().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CUSTOMERCUSTOMER_ADDRESS.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", ">>CUSTOMER>CUSTOMER_ADDRESS"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CUSTOMER_ADDRESS_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "CUSTOMER_ADDRESS_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CUSTOMER_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "CUSTOMER_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ADDRESS_TYPE_CODE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "ADDRESS_TYPE_CODE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ADDRESS_1_TEXT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "ADDRESS_1_TEXT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ADDRESS_2_TEXT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "ADDRESS_2_TEXT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ADDRESS_3_TEXT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "ADDRESS_3_TEXT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ADDRESS_4_TEXT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "ADDRESS_4_TEXT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CITY_TEXT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "CITY_TEXT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ZIP_CODE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "ZIP_CODE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ZIP_CODE_EXTENSION");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "ZIP_CODE_EXTENSION"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("POSTAL_CODE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "POSTAL_CODE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LONGITUDE_K");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "LONGITUDE_K"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LATITUDE_K");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "LATITUDE_K"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("COUNTY_NAME");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "COUNTY_NAME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("STATE_NAME");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "STATE_NAME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("STATE_CODE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "STATE_CODE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ZIP_10_CODE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "ZIP_10_CODE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("COUNTRY_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "COUNTRY_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("FIPS_CODE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "FIPS_CODE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("RESIDENCE_TYPE_CODE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "RESIDENCE_TYPE_CODE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MAILABLE_STATUS_SWITCH");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "MAILABLE_STATUS_SWITCH"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MAILABLE_STATUS_REASON_CODE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "MAILABLE_STATUS_REASON_CODE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CUSTOMER_SOURCE_CODE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "CUSTOMER_SOURCE_CODE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CREATE_TIMESTAMP");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "CREATE_TIMESTAMP"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LAST_UPDATE_DATE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "LAST_UPDATE_DATE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LAST_UPDATE_USER_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "LAST_UPDATE_USER_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("EXTERNAL_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "EXTERNAL_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("FIPS_COUNTY_CODE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "FIPS_COUNTY_CODE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("FIPS_STATE_CODE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "FIPS_STATE_CODE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CENS_TRACT_CODE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "CENS_TRACT_CODE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CENS_BLOCK_CODE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "CENS_BLOCK_CODE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
