/**
 * SendMessageRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.xmlns.ei.Salt3;

public class SendMessageRequest  implements java.io.Serializable {
    private java.lang.String apiKey;

    private java.lang.String keyword;

    private java.lang.String destinationMdn;

    private java.lang.String inReplyTo;

    private java.util.Calendar expirationDate;

    private com.heb.xmlns.ei.Salt3.SendMessageRequestTemplate template;

    public SendMessageRequest() {
    }

    public SendMessageRequest(
           java.lang.String apiKey,
           java.lang.String keyword,
           java.lang.String destinationMdn,
           java.lang.String inReplyTo,
           java.util.Calendar expirationDate,
           com.heb.xmlns.ei.Salt3.SendMessageRequestTemplate template) {
           this.apiKey = apiKey;
           this.keyword = keyword;
           this.destinationMdn = destinationMdn;
           this.inReplyTo = inReplyTo;
           this.expirationDate = expirationDate;
           this.template = template;
    }


    /**
     * Gets the apiKey value for this SendMessageRequest.
     * 
     * @return apiKey
     */
    public java.lang.String getApiKey() {
        return apiKey;
    }


    /**
     * Sets the apiKey value for this SendMessageRequest.
     * 
     * @param apiKey
     */
    public void setApiKey(java.lang.String apiKey) {
        this.apiKey = apiKey;
    }


    /**
     * Gets the keyword value for this SendMessageRequest.
     * 
     * @return keyword
     */
    public java.lang.String getKeyword() {
        return keyword;
    }


    /**
     * Sets the keyword value for this SendMessageRequest.
     * 
     * @param keyword
     */
    public void setKeyword(java.lang.String keyword) {
        this.keyword = keyword;
    }


    /**
     * Gets the destinationMdn value for this SendMessageRequest.
     * 
     * @return destinationMdn
     */
    public java.lang.String getDestinationMdn() {
        return destinationMdn;
    }


    /**
     * Sets the destinationMdn value for this SendMessageRequest.
     * 
     * @param destinationMdn
     */
    public void setDestinationMdn(java.lang.String destinationMdn) {
        this.destinationMdn = destinationMdn;
    }


    /**
     * Gets the inReplyTo value for this SendMessageRequest.
     * 
     * @return inReplyTo
     */
    public java.lang.String getInReplyTo() {
        return inReplyTo;
    }


    /**
     * Sets the inReplyTo value for this SendMessageRequest.
     * 
     * @param inReplyTo
     */
    public void setInReplyTo(java.lang.String inReplyTo) {
        this.inReplyTo = inReplyTo;
    }


    /**
     * Gets the expirationDate value for this SendMessageRequest.
     * 
     * @return expirationDate
     */
    public java.util.Calendar getExpirationDate() {
        return expirationDate;
    }


    /**
     * Sets the expirationDate value for this SendMessageRequest.
     * 
     * @param expirationDate
     */
    public void setExpirationDate(java.util.Calendar expirationDate) {
        this.expirationDate = expirationDate;
    }


    /**
     * Gets the template value for this SendMessageRequest.
     * 
     * @return template
     */
    public com.heb.xmlns.ei.Salt3.SendMessageRequestTemplate getTemplate() {
        return template;
    }


    /**
     * Sets the template value for this SendMessageRequest.
     * 
     * @param template
     */
    public void setTemplate(com.heb.xmlns.ei.Salt3.SendMessageRequestTemplate template) {
        this.template = template;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SendMessageRequest)) return false;
        SendMessageRequest other = (SendMessageRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.apiKey==null && other.getApiKey()==null) || 
             (this.apiKey!=null &&
              this.apiKey.equals(other.getApiKey()))) &&
            ((this.keyword==null && other.getKeyword()==null) || 
             (this.keyword!=null &&
              this.keyword.equals(other.getKeyword()))) &&
            ((this.destinationMdn==null && other.getDestinationMdn()==null) || 
             (this.destinationMdn!=null &&
              this.destinationMdn.equals(other.getDestinationMdn()))) &&
            ((this.inReplyTo==null && other.getInReplyTo()==null) || 
             (this.inReplyTo!=null &&
              this.inReplyTo.equals(other.getInReplyTo()))) &&
            ((this.expirationDate==null && other.getExpirationDate()==null) || 
             (this.expirationDate!=null &&
              this.expirationDate.equals(other.getExpirationDate()))) &&
            ((this.template==null && other.getTemplate()==null) || 
             (this.template!=null &&
              this.template.equals(other.getTemplate())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getApiKey() != null) {
            _hashCode += getApiKey().hashCode();
        }
        if (getKeyword() != null) {
            _hashCode += getKeyword().hashCode();
        }
        if (getDestinationMdn() != null) {
            _hashCode += getDestinationMdn().hashCode();
        }
        if (getInReplyTo() != null) {
            _hashCode += getInReplyTo().hashCode();
        }
        if (getExpirationDate() != null) {
            _hashCode += getExpirationDate().hashCode();
        }
        if (getTemplate() != null) {
            _hashCode += getTemplate().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SendMessageRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Salt3", ">send-message-request"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("apiKey");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Salt3", "api-key"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("keyword");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Salt3", "keyword"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("destinationMdn");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Salt3", "destination-mdn"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("inReplyTo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Salt3", "in-reply-to"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("expirationDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Salt3", "expiration-date"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("template");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Salt3", "template"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Salt3", ">>send-message-request>template"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
