/**
 * UpdateProfile_Request.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.xmlns.ei.UpdateProfile_Request;

public class UpdateProfile_Request  implements java.io.Serializable {
    private com.heb.xmlns.ei.PROGRAM_ACCOUNT.PROGRAM_ACCOUNT PROGRAM_ACCOUNT;

    private com.heb.xmlns.ei.CUSTOMER.CUSTOMER CUSTOMER;

    private com.heb.xmlns.ei.STG_PROFILE_UPDATE_ATG.STG_PROFILE_UPDATE_ATG STG_PROFILE_UPDATE_ATG;

    private java.math.BigInteger ENROLMENT_STORE_ID;

    private com.heb.xmlns.ei.PROGRAM_PARTICIPATION.PROGRAM_PARTICIPATION PROGRAM_PARTICIPATION;

    private com.heb.xmlns.ei.Authentication.Authentication authentication;

    public UpdateProfile_Request() {
    }

    public UpdateProfile_Request(
           com.heb.xmlns.ei.PROGRAM_ACCOUNT.PROGRAM_ACCOUNT PROGRAM_ACCOUNT,
           com.heb.xmlns.ei.CUSTOMER.CUSTOMER CUSTOMER,
           com.heb.xmlns.ei.STG_PROFILE_UPDATE_ATG.STG_PROFILE_UPDATE_ATG STG_PROFILE_UPDATE_ATG,
           java.math.BigInteger ENROLMENT_STORE_ID,
           com.heb.xmlns.ei.PROGRAM_PARTICIPATION.PROGRAM_PARTICIPATION PROGRAM_PARTICIPATION,
           com.heb.xmlns.ei.Authentication.Authentication authentication) {
           this.PROGRAM_ACCOUNT = PROGRAM_ACCOUNT;
           this.CUSTOMER = CUSTOMER;
           this.STG_PROFILE_UPDATE_ATG = STG_PROFILE_UPDATE_ATG;
           this.ENROLMENT_STORE_ID = ENROLMENT_STORE_ID;
           this.PROGRAM_PARTICIPATION = PROGRAM_PARTICIPATION;
           this.authentication = authentication;
    }


    /**
     * Gets the PROGRAM_ACCOUNT value for this UpdateProfile_Request.
     * 
     * @return PROGRAM_ACCOUNT
     */
    public com.heb.xmlns.ei.PROGRAM_ACCOUNT.PROGRAM_ACCOUNT getPROGRAM_ACCOUNT() {
        return PROGRAM_ACCOUNT;
    }


    /**
     * Sets the PROGRAM_ACCOUNT value for this UpdateProfile_Request.
     * 
     * @param PROGRAM_ACCOUNT
     */
    public void setPROGRAM_ACCOUNT(com.heb.xmlns.ei.PROGRAM_ACCOUNT.PROGRAM_ACCOUNT PROGRAM_ACCOUNT) {
        this.PROGRAM_ACCOUNT = PROGRAM_ACCOUNT;
    }


    /**
     * Gets the CUSTOMER value for this UpdateProfile_Request.
     * 
     * @return CUSTOMER
     */
    public com.heb.xmlns.ei.CUSTOMER.CUSTOMER getCUSTOMER() {
        return CUSTOMER;
    }


    /**
     * Sets the CUSTOMER value for this UpdateProfile_Request.
     * 
     * @param CUSTOMER
     */
    public void setCUSTOMER(com.heb.xmlns.ei.CUSTOMER.CUSTOMER CUSTOMER) {
        this.CUSTOMER = CUSTOMER;
    }


    /**
     * Gets the STG_PROFILE_UPDATE_ATG value for this UpdateProfile_Request.
     * 
     * @return STG_PROFILE_UPDATE_ATG
     */
    public com.heb.xmlns.ei.STG_PROFILE_UPDATE_ATG.STG_PROFILE_UPDATE_ATG getSTG_PROFILE_UPDATE_ATG() {
        return STG_PROFILE_UPDATE_ATG;
    }


    /**
     * Sets the STG_PROFILE_UPDATE_ATG value for this UpdateProfile_Request.
     * 
     * @param STG_PROFILE_UPDATE_ATG
     */
    public void setSTG_PROFILE_UPDATE_ATG(com.heb.xmlns.ei.STG_PROFILE_UPDATE_ATG.STG_PROFILE_UPDATE_ATG STG_PROFILE_UPDATE_ATG) {
        this.STG_PROFILE_UPDATE_ATG = STG_PROFILE_UPDATE_ATG;
    }


    /**
     * Gets the ENROLMENT_STORE_ID value for this UpdateProfile_Request.
     * 
     * @return ENROLMENT_STORE_ID
     */
    public java.math.BigInteger getENROLMENT_STORE_ID() {
        return ENROLMENT_STORE_ID;
    }


    /**
     * Sets the ENROLMENT_STORE_ID value for this UpdateProfile_Request.
     * 
     * @param ENROLMENT_STORE_ID
     */
    public void setENROLMENT_STORE_ID(java.math.BigInteger ENROLMENT_STORE_ID) {
        this.ENROLMENT_STORE_ID = ENROLMENT_STORE_ID;
    }


    /**
     * Gets the PROGRAM_PARTICIPATION value for this UpdateProfile_Request.
     * 
     * @return PROGRAM_PARTICIPATION
     */
    public com.heb.xmlns.ei.PROGRAM_PARTICIPATION.PROGRAM_PARTICIPATION getPROGRAM_PARTICIPATION() {
        return PROGRAM_PARTICIPATION;
    }


    /**
     * Sets the PROGRAM_PARTICIPATION value for this UpdateProfile_Request.
     * 
     * @param PROGRAM_PARTICIPATION
     */
    public void setPROGRAM_PARTICIPATION(com.heb.xmlns.ei.PROGRAM_PARTICIPATION.PROGRAM_PARTICIPATION PROGRAM_PARTICIPATION) {
        this.PROGRAM_PARTICIPATION = PROGRAM_PARTICIPATION;
    }


    /**
     * Gets the authentication value for this UpdateProfile_Request.
     * 
     * @return authentication
     */
    public com.heb.xmlns.ei.Authentication.Authentication getAuthentication() {
        return authentication;
    }


    /**
     * Sets the authentication value for this UpdateProfile_Request.
     * 
     * @param authentication
     */
    public void setAuthentication(com.heb.xmlns.ei.Authentication.Authentication authentication) {
        this.authentication = authentication;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UpdateProfile_Request)) return false;
        UpdateProfile_Request other = (UpdateProfile_Request) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.PROGRAM_ACCOUNT==null && other.getPROGRAM_ACCOUNT()==null) || 
             (this.PROGRAM_ACCOUNT!=null &&
              this.PROGRAM_ACCOUNT.equals(other.getPROGRAM_ACCOUNT()))) &&
            ((this.CUSTOMER==null && other.getCUSTOMER()==null) || 
             (this.CUSTOMER!=null &&
              this.CUSTOMER.equals(other.getCUSTOMER()))) &&
            ((this.STG_PROFILE_UPDATE_ATG==null && other.getSTG_PROFILE_UPDATE_ATG()==null) || 
             (this.STG_PROFILE_UPDATE_ATG!=null &&
              this.STG_PROFILE_UPDATE_ATG.equals(other.getSTG_PROFILE_UPDATE_ATG()))) &&
            ((this.ENROLMENT_STORE_ID==null && other.getENROLMENT_STORE_ID()==null) || 
             (this.ENROLMENT_STORE_ID!=null &&
              this.ENROLMENT_STORE_ID.equals(other.getENROLMENT_STORE_ID()))) &&
            ((this.PROGRAM_PARTICIPATION==null && other.getPROGRAM_PARTICIPATION()==null) || 
             (this.PROGRAM_PARTICIPATION!=null &&
              this.PROGRAM_PARTICIPATION.equals(other.getPROGRAM_PARTICIPATION()))) &&
            ((this.authentication==null && other.getAuthentication()==null) || 
             (this.authentication!=null &&
              this.authentication.equals(other.getAuthentication())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPROGRAM_ACCOUNT() != null) {
            _hashCode += getPROGRAM_ACCOUNT().hashCode();
        }
        if (getCUSTOMER() != null) {
            _hashCode += getCUSTOMER().hashCode();
        }
        if (getSTG_PROFILE_UPDATE_ATG() != null) {
            _hashCode += getSTG_PROFILE_UPDATE_ATG().hashCode();
        }
        if (getENROLMENT_STORE_ID() != null) {
            _hashCode += getENROLMENT_STORE_ID().hashCode();
        }
        if (getPROGRAM_PARTICIPATION() != null) {
            _hashCode += getPROGRAM_PARTICIPATION().hashCode();
        }
        if (getAuthentication() != null) {
            _hashCode += getAuthentication().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(UpdateProfile_Request.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/UpdateProfile_Request", ">UpdateProfile_Request"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PROGRAM_ACCOUNT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PROGRAM_ACCOUNT", "PROGRAM_ACCOUNT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PROGRAM_ACCOUNT", ">PROGRAM_ACCOUNT"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CUSTOMER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "CUSTOMER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", ">CUSTOMER"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("STG_PROFILE_UPDATE_ATG");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/STG_PROFILE_UPDATE_ATG", "STG_PROFILE_UPDATE_ATG"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/STG_PROFILE_UPDATE_ATG", ">STG_PROFILE_UPDATE_ATG"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ENROLMENT_STORE_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/UpdateProfile_Request", "ENROLMENT_STORE_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PROGRAM_PARTICIPATION");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PROGRAM_PARTICIPATION", "PROGRAM_PARTICIPATION"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PROGRAM_PARTICIPATION", ">PROGRAM_PARTICIPATION"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authentication");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Authentication", "Authentication"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Authentication", ">Authentication"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
