/**
 * CUSTOMER.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.xmlns.ei.CUSTOMER;

public class CUSTOMER  implements java.io.Serializable {
    private java.math.BigInteger CUSTOMER_ID;

    private java.math.BigInteger MODELED_UNIT_ID;

    private java.lang.String TITLE_NAME;

    private java.lang.String FIRST_NAME;

    private java.lang.String LAST_NAME;

    private java.lang.String SUFFIX_NAME;

    private java.lang.String BIRTH_DATE;

    private java.lang.String MIDDLE_NAME;

    private java.lang.String CUSTOMER_TYPE_CODE;

    private java.lang.String PREFERENCE_LANGUAGE_TYPE_CODE;

    private java.lang.String FIRST_PURCHASE_DATE;

    private java.lang.String LAST_PURCHASE_DATE;

    private java.lang.String NO_CALL_SWITCH;

    private java.lang.String NO_MAIL_SWITCH;

    private java.lang.String OFFER_EMAIL_OPTION_IN_SWITCH;

    private java.lang.String REWARD_EMAIL_OPTION_IN_SWITCH;

    private java.lang.String SEND_ALL_MAIL_SWITCH;

    private java.lang.String WEB_REWARDS_OPTION_IN_SWITCH;

    private java.lang.String WEB_REWARDS_OPTION_IN_DATE;

    private java.lang.String CUSTOMER_SOURCE_CODE;

    private java.lang.String CUSTOMER_ACCOUNT_ALPHA_ID;

    private java.math.BigInteger CUSTOMER_ACCOUNT_NUMERIC_ID;

    private java.lang.String CUSTOMER_ACCOUNT_TYPE;

    private java.lang.String COMMUNICATION_METHOD_CODE;

    private java.lang.String CUSTOMER_STATUS_CODE;

    private java.lang.String CREATE_TIMESTAMP;

    private java.lang.String LAST_UPDATE_DATE;

    private java.lang.String LAST_UPDATE_USER_ID;

    private java.math.BigInteger EXTERNAL_ID;

    private java.lang.String PRIMARY_CUSTOMER_SWITCH;

    private java.math.BigInteger PRIMARY_CUSTOMER_ID;

    private java.math.BigInteger APPEND_ATTEMPT_COUNT;

    private java.util.Date LAST_APPEND_DATE;

    private com.heb.xmlns.ei.CUSTOMER.CUSTOMERCUSTOMER_ADDRESS[] CUSTOMER_ADDRESS;

    private com.heb.xmlns.ei.CUSTOMER.CUSTOMERCUSTOMER_PHONE[] CUSTOMER_PHONE;

    private com.heb.xmlns.ei.CUSTOMER.CUSTOMERCUSTOMER_EMAIL_ADDRESS CUSTOMER_EMAIL_ADDRESS;

    public CUSTOMER() {
    }

    public CUSTOMER(
           java.math.BigInteger CUSTOMER_ID,
           java.math.BigInteger MODELED_UNIT_ID,
           java.lang.String TITLE_NAME,
           java.lang.String FIRST_NAME,
           java.lang.String LAST_NAME,
           java.lang.String SUFFIX_NAME,
           java.lang.String BIRTH_DATE,
           java.lang.String MIDDLE_NAME,
           java.lang.String CUSTOMER_TYPE_CODE,
           java.lang.String PREFERENCE_LANGUAGE_TYPE_CODE,
           java.lang.String FIRST_PURCHASE_DATE,
           java.lang.String LAST_PURCHASE_DATE,
           java.lang.String NO_CALL_SWITCH,
           java.lang.String NO_MAIL_SWITCH,
           java.lang.String OFFER_EMAIL_OPTION_IN_SWITCH,
           java.lang.String REWARD_EMAIL_OPTION_IN_SWITCH,
           java.lang.String SEND_ALL_MAIL_SWITCH,
           java.lang.String WEB_REWARDS_OPTION_IN_SWITCH,
           java.lang.String WEB_REWARDS_OPTION_IN_DATE,
           java.lang.String CUSTOMER_SOURCE_CODE,
           java.lang.String CUSTOMER_ACCOUNT_ALPHA_ID,
           java.math.BigInteger CUSTOMER_ACCOUNT_NUMERIC_ID,
           java.lang.String CUSTOMER_ACCOUNT_TYPE,
           java.lang.String COMMUNICATION_METHOD_CODE,
           java.lang.String CUSTOMER_STATUS_CODE,
           java.lang.String CREATE_TIMESTAMP,
           java.lang.String LAST_UPDATE_DATE,
           java.lang.String LAST_UPDATE_USER_ID,
           java.math.BigInteger EXTERNAL_ID,
           java.lang.String PRIMARY_CUSTOMER_SWITCH,
           java.math.BigInteger PRIMARY_CUSTOMER_ID,
           java.math.BigInteger APPEND_ATTEMPT_COUNT,
           java.util.Date LAST_APPEND_DATE,
           com.heb.xmlns.ei.CUSTOMER.CUSTOMERCUSTOMER_ADDRESS[] CUSTOMER_ADDRESS,
           com.heb.xmlns.ei.CUSTOMER.CUSTOMERCUSTOMER_PHONE[] CUSTOMER_PHONE,
           com.heb.xmlns.ei.CUSTOMER.CUSTOMERCUSTOMER_EMAIL_ADDRESS CUSTOMER_EMAIL_ADDRESS) {
           this.CUSTOMER_ID = CUSTOMER_ID;
           this.MODELED_UNIT_ID = MODELED_UNIT_ID;
           this.TITLE_NAME = TITLE_NAME;
           this.FIRST_NAME = FIRST_NAME;
           this.LAST_NAME = LAST_NAME;
           this.SUFFIX_NAME = SUFFIX_NAME;
           this.BIRTH_DATE = BIRTH_DATE;
           this.MIDDLE_NAME = MIDDLE_NAME;
           this.CUSTOMER_TYPE_CODE = CUSTOMER_TYPE_CODE;
           this.PREFERENCE_LANGUAGE_TYPE_CODE = PREFERENCE_LANGUAGE_TYPE_CODE;
           this.FIRST_PURCHASE_DATE = FIRST_PURCHASE_DATE;
           this.LAST_PURCHASE_DATE = LAST_PURCHASE_DATE;
           this.NO_CALL_SWITCH = NO_CALL_SWITCH;
           this.NO_MAIL_SWITCH = NO_MAIL_SWITCH;
           this.OFFER_EMAIL_OPTION_IN_SWITCH = OFFER_EMAIL_OPTION_IN_SWITCH;
           this.REWARD_EMAIL_OPTION_IN_SWITCH = REWARD_EMAIL_OPTION_IN_SWITCH;
           this.SEND_ALL_MAIL_SWITCH = SEND_ALL_MAIL_SWITCH;
           this.WEB_REWARDS_OPTION_IN_SWITCH = WEB_REWARDS_OPTION_IN_SWITCH;
           this.WEB_REWARDS_OPTION_IN_DATE = WEB_REWARDS_OPTION_IN_DATE;
           this.CUSTOMER_SOURCE_CODE = CUSTOMER_SOURCE_CODE;
           this.CUSTOMER_ACCOUNT_ALPHA_ID = CUSTOMER_ACCOUNT_ALPHA_ID;
           this.CUSTOMER_ACCOUNT_NUMERIC_ID = CUSTOMER_ACCOUNT_NUMERIC_ID;
           this.CUSTOMER_ACCOUNT_TYPE = CUSTOMER_ACCOUNT_TYPE;
           this.COMMUNICATION_METHOD_CODE = COMMUNICATION_METHOD_CODE;
           this.CUSTOMER_STATUS_CODE = CUSTOMER_STATUS_CODE;
           this.CREATE_TIMESTAMP = CREATE_TIMESTAMP;
           this.LAST_UPDATE_DATE = LAST_UPDATE_DATE;
           this.LAST_UPDATE_USER_ID = LAST_UPDATE_USER_ID;
           this.EXTERNAL_ID = EXTERNAL_ID;
           this.PRIMARY_CUSTOMER_SWITCH = PRIMARY_CUSTOMER_SWITCH;
           this.PRIMARY_CUSTOMER_ID = PRIMARY_CUSTOMER_ID;
           this.APPEND_ATTEMPT_COUNT = APPEND_ATTEMPT_COUNT;
           this.LAST_APPEND_DATE = LAST_APPEND_DATE;
           this.CUSTOMER_ADDRESS = CUSTOMER_ADDRESS;
           this.CUSTOMER_PHONE = CUSTOMER_PHONE;
           this.CUSTOMER_EMAIL_ADDRESS = CUSTOMER_EMAIL_ADDRESS;
    }


    /**
     * Gets the CUSTOMER_ID value for this CUSTOMER.
     * 
     * @return CUSTOMER_ID
     */
    public java.math.BigInteger getCUSTOMER_ID() {
        return CUSTOMER_ID;
    }


    /**
     * Sets the CUSTOMER_ID value for this CUSTOMER.
     * 
     * @param CUSTOMER_ID
     */
    public void setCUSTOMER_ID(java.math.BigInteger CUSTOMER_ID) {
        this.CUSTOMER_ID = CUSTOMER_ID;
    }


    /**
     * Gets the MODELED_UNIT_ID value for this CUSTOMER.
     * 
     * @return MODELED_UNIT_ID
     */
    public java.math.BigInteger getMODELED_UNIT_ID() {
        return MODELED_UNIT_ID;
    }


    /**
     * Sets the MODELED_UNIT_ID value for this CUSTOMER.
     * 
     * @param MODELED_UNIT_ID
     */
    public void setMODELED_UNIT_ID(java.math.BigInteger MODELED_UNIT_ID) {
        this.MODELED_UNIT_ID = MODELED_UNIT_ID;
    }


    /**
     * Gets the TITLE_NAME value for this CUSTOMER.
     * 
     * @return TITLE_NAME
     */
    public java.lang.String getTITLE_NAME() {
        return TITLE_NAME;
    }


    /**
     * Sets the TITLE_NAME value for this CUSTOMER.
     * 
     * @param TITLE_NAME
     */
    public void setTITLE_NAME(java.lang.String TITLE_NAME) {
        this.TITLE_NAME = TITLE_NAME;
    }


    /**
     * Gets the FIRST_NAME value for this CUSTOMER.
     * 
     * @return FIRST_NAME
     */
    public java.lang.String getFIRST_NAME() {
        return FIRST_NAME;
    }


    /**
     * Sets the FIRST_NAME value for this CUSTOMER.
     * 
     * @param FIRST_NAME
     */
    public void setFIRST_NAME(java.lang.String FIRST_NAME) {
        this.FIRST_NAME = FIRST_NAME;
    }


    /**
     * Gets the LAST_NAME value for this CUSTOMER.
     * 
     * @return LAST_NAME
     */
    public java.lang.String getLAST_NAME() {
        return LAST_NAME;
    }


    /**
     * Sets the LAST_NAME value for this CUSTOMER.
     * 
     * @param LAST_NAME
     */
    public void setLAST_NAME(java.lang.String LAST_NAME) {
        this.LAST_NAME = LAST_NAME;
    }


    /**
     * Gets the SUFFIX_NAME value for this CUSTOMER.
     * 
     * @return SUFFIX_NAME
     */
    public java.lang.String getSUFFIX_NAME() {
        return SUFFIX_NAME;
    }


    /**
     * Sets the SUFFIX_NAME value for this CUSTOMER.
     * 
     * @param SUFFIX_NAME
     */
    public void setSUFFIX_NAME(java.lang.String SUFFIX_NAME) {
        this.SUFFIX_NAME = SUFFIX_NAME;
    }


    /**
     * Gets the BIRTH_DATE value for this CUSTOMER.
     * 
     * @return BIRTH_DATE
     */
    public java.lang.String getBIRTH_DATE() {
        return BIRTH_DATE;
    }


    /**
     * Sets the BIRTH_DATE value for this CUSTOMER.
     * 
     * @param BIRTH_DATE
     */
    public void setBIRTH_DATE(java.lang.String BIRTH_DATE) {
        this.BIRTH_DATE = BIRTH_DATE;
    }


    /**
     * Gets the MIDDLE_NAME value for this CUSTOMER.
     * 
     * @return MIDDLE_NAME
     */
    public java.lang.String getMIDDLE_NAME() {
        return MIDDLE_NAME;
    }


    /**
     * Sets the MIDDLE_NAME value for this CUSTOMER.
     * 
     * @param MIDDLE_NAME
     */
    public void setMIDDLE_NAME(java.lang.String MIDDLE_NAME) {
        this.MIDDLE_NAME = MIDDLE_NAME;
    }


    /**
     * Gets the CUSTOMER_TYPE_CODE value for this CUSTOMER.
     * 
     * @return CUSTOMER_TYPE_CODE
     */
    public java.lang.String getCUSTOMER_TYPE_CODE() {
        return CUSTOMER_TYPE_CODE;
    }


    /**
     * Sets the CUSTOMER_TYPE_CODE value for this CUSTOMER.
     * 
     * @param CUSTOMER_TYPE_CODE
     */
    public void setCUSTOMER_TYPE_CODE(java.lang.String CUSTOMER_TYPE_CODE) {
        this.CUSTOMER_TYPE_CODE = CUSTOMER_TYPE_CODE;
    }


    /**
     * Gets the PREFERENCE_LANGUAGE_TYPE_CODE value for this CUSTOMER.
     * 
     * @return PREFERENCE_LANGUAGE_TYPE_CODE
     */
    public java.lang.String getPREFERENCE_LANGUAGE_TYPE_CODE() {
        return PREFERENCE_LANGUAGE_TYPE_CODE;
    }


    /**
     * Sets the PREFERENCE_LANGUAGE_TYPE_CODE value for this CUSTOMER.
     * 
     * @param PREFERENCE_LANGUAGE_TYPE_CODE
     */
    public void setPREFERENCE_LANGUAGE_TYPE_CODE(java.lang.String PREFERENCE_LANGUAGE_TYPE_CODE) {
        this.PREFERENCE_LANGUAGE_TYPE_CODE = PREFERENCE_LANGUAGE_TYPE_CODE;
    }


    /**
     * Gets the FIRST_PURCHASE_DATE value for this CUSTOMER.
     * 
     * @return FIRST_PURCHASE_DATE
     */
    public java.lang.String getFIRST_PURCHASE_DATE() {
        return FIRST_PURCHASE_DATE;
    }


    /**
     * Sets the FIRST_PURCHASE_DATE value for this CUSTOMER.
     * 
     * @param FIRST_PURCHASE_DATE
     */
    public void setFIRST_PURCHASE_DATE(java.lang.String FIRST_PURCHASE_DATE) {
        this.FIRST_PURCHASE_DATE = FIRST_PURCHASE_DATE;
    }


    /**
     * Gets the LAST_PURCHASE_DATE value for this CUSTOMER.
     * 
     * @return LAST_PURCHASE_DATE
     */
    public java.lang.String getLAST_PURCHASE_DATE() {
        return LAST_PURCHASE_DATE;
    }


    /**
     * Sets the LAST_PURCHASE_DATE value for this CUSTOMER.
     * 
     * @param LAST_PURCHASE_DATE
     */
    public void setLAST_PURCHASE_DATE(java.lang.String LAST_PURCHASE_DATE) {
        this.LAST_PURCHASE_DATE = LAST_PURCHASE_DATE;
    }


    /**
     * Gets the NO_CALL_SWITCH value for this CUSTOMER.
     * 
     * @return NO_CALL_SWITCH
     */
    public java.lang.String getNO_CALL_SWITCH() {
        return NO_CALL_SWITCH;
    }


    /**
     * Sets the NO_CALL_SWITCH value for this CUSTOMER.
     * 
     * @param NO_CALL_SWITCH
     */
    public void setNO_CALL_SWITCH(java.lang.String NO_CALL_SWITCH) {
        this.NO_CALL_SWITCH = NO_CALL_SWITCH;
    }


    /**
     * Gets the NO_MAIL_SWITCH value for this CUSTOMER.
     * 
     * @return NO_MAIL_SWITCH
     */
    public java.lang.String getNO_MAIL_SWITCH() {
        return NO_MAIL_SWITCH;
    }


    /**
     * Sets the NO_MAIL_SWITCH value for this CUSTOMER.
     * 
     * @param NO_MAIL_SWITCH
     */
    public void setNO_MAIL_SWITCH(java.lang.String NO_MAIL_SWITCH) {
        this.NO_MAIL_SWITCH = NO_MAIL_SWITCH;
    }


    /**
     * Gets the OFFER_EMAIL_OPTION_IN_SWITCH value for this CUSTOMER.
     * 
     * @return OFFER_EMAIL_OPTION_IN_SWITCH
     */
    public java.lang.String getOFFER_EMAIL_OPTION_IN_SWITCH() {
        return OFFER_EMAIL_OPTION_IN_SWITCH;
    }


    /**
     * Sets the OFFER_EMAIL_OPTION_IN_SWITCH value for this CUSTOMER.
     * 
     * @param OFFER_EMAIL_OPTION_IN_SWITCH
     */
    public void setOFFER_EMAIL_OPTION_IN_SWITCH(java.lang.String OFFER_EMAIL_OPTION_IN_SWITCH) {
        this.OFFER_EMAIL_OPTION_IN_SWITCH = OFFER_EMAIL_OPTION_IN_SWITCH;
    }


    /**
     * Gets the REWARD_EMAIL_OPTION_IN_SWITCH value for this CUSTOMER.
     * 
     * @return REWARD_EMAIL_OPTION_IN_SWITCH
     */
    public java.lang.String getREWARD_EMAIL_OPTION_IN_SWITCH() {
        return REWARD_EMAIL_OPTION_IN_SWITCH;
    }


    /**
     * Sets the REWARD_EMAIL_OPTION_IN_SWITCH value for this CUSTOMER.
     * 
     * @param REWARD_EMAIL_OPTION_IN_SWITCH
     */
    public void setREWARD_EMAIL_OPTION_IN_SWITCH(java.lang.String REWARD_EMAIL_OPTION_IN_SWITCH) {
        this.REWARD_EMAIL_OPTION_IN_SWITCH = REWARD_EMAIL_OPTION_IN_SWITCH;
    }


    /**
     * Gets the SEND_ALL_MAIL_SWITCH value for this CUSTOMER.
     * 
     * @return SEND_ALL_MAIL_SWITCH
     */
    public java.lang.String getSEND_ALL_MAIL_SWITCH() {
        return SEND_ALL_MAIL_SWITCH;
    }


    /**
     * Sets the SEND_ALL_MAIL_SWITCH value for this CUSTOMER.
     * 
     * @param SEND_ALL_MAIL_SWITCH
     */
    public void setSEND_ALL_MAIL_SWITCH(java.lang.String SEND_ALL_MAIL_SWITCH) {
        this.SEND_ALL_MAIL_SWITCH = SEND_ALL_MAIL_SWITCH;
    }


    /**
     * Gets the WEB_REWARDS_OPTION_IN_SWITCH value for this CUSTOMER.
     * 
     * @return WEB_REWARDS_OPTION_IN_SWITCH
     */
    public java.lang.String getWEB_REWARDS_OPTION_IN_SWITCH() {
        return WEB_REWARDS_OPTION_IN_SWITCH;
    }


    /**
     * Sets the WEB_REWARDS_OPTION_IN_SWITCH value for this CUSTOMER.
     * 
     * @param WEB_REWARDS_OPTION_IN_SWITCH
     */
    public void setWEB_REWARDS_OPTION_IN_SWITCH(java.lang.String WEB_REWARDS_OPTION_IN_SWITCH) {
        this.WEB_REWARDS_OPTION_IN_SWITCH = WEB_REWARDS_OPTION_IN_SWITCH;
    }


    /**
     * Gets the WEB_REWARDS_OPTION_IN_DATE value for this CUSTOMER.
     * 
     * @return WEB_REWARDS_OPTION_IN_DATE
     */
    public java.lang.String getWEB_REWARDS_OPTION_IN_DATE() {
        return WEB_REWARDS_OPTION_IN_DATE;
    }


    /**
     * Sets the WEB_REWARDS_OPTION_IN_DATE value for this CUSTOMER.
     * 
     * @param WEB_REWARDS_OPTION_IN_DATE
     */
    public void setWEB_REWARDS_OPTION_IN_DATE(java.lang.String WEB_REWARDS_OPTION_IN_DATE) {
        this.WEB_REWARDS_OPTION_IN_DATE = WEB_REWARDS_OPTION_IN_DATE;
    }


    /**
     * Gets the CUSTOMER_SOURCE_CODE value for this CUSTOMER.
     * 
     * @return CUSTOMER_SOURCE_CODE
     */
    public java.lang.String getCUSTOMER_SOURCE_CODE() {
        return CUSTOMER_SOURCE_CODE;
    }


    /**
     * Sets the CUSTOMER_SOURCE_CODE value for this CUSTOMER.
     * 
     * @param CUSTOMER_SOURCE_CODE
     */
    public void setCUSTOMER_SOURCE_CODE(java.lang.String CUSTOMER_SOURCE_CODE) {
        this.CUSTOMER_SOURCE_CODE = CUSTOMER_SOURCE_CODE;
    }


    /**
     * Gets the CUSTOMER_ACCOUNT_ALPHA_ID value for this CUSTOMER.
     * 
     * @return CUSTOMER_ACCOUNT_ALPHA_ID
     */
    public java.lang.String getCUSTOMER_ACCOUNT_ALPHA_ID() {
        return CUSTOMER_ACCOUNT_ALPHA_ID;
    }


    /**
     * Sets the CUSTOMER_ACCOUNT_ALPHA_ID value for this CUSTOMER.
     * 
     * @param CUSTOMER_ACCOUNT_ALPHA_ID
     */
    public void setCUSTOMER_ACCOUNT_ALPHA_ID(java.lang.String CUSTOMER_ACCOUNT_ALPHA_ID) {
        this.CUSTOMER_ACCOUNT_ALPHA_ID = CUSTOMER_ACCOUNT_ALPHA_ID;
    }


    /**
     * Gets the CUSTOMER_ACCOUNT_NUMERIC_ID value for this CUSTOMER.
     * 
     * @return CUSTOMER_ACCOUNT_NUMERIC_ID
     */
    public java.math.BigInteger getCUSTOMER_ACCOUNT_NUMERIC_ID() {
        return CUSTOMER_ACCOUNT_NUMERIC_ID;
    }


    /**
     * Sets the CUSTOMER_ACCOUNT_NUMERIC_ID value for this CUSTOMER.
     * 
     * @param CUSTOMER_ACCOUNT_NUMERIC_ID
     */
    public void setCUSTOMER_ACCOUNT_NUMERIC_ID(java.math.BigInteger CUSTOMER_ACCOUNT_NUMERIC_ID) {
        this.CUSTOMER_ACCOUNT_NUMERIC_ID = CUSTOMER_ACCOUNT_NUMERIC_ID;
    }


    /**
     * Gets the CUSTOMER_ACCOUNT_TYPE value for this CUSTOMER.
     * 
     * @return CUSTOMER_ACCOUNT_TYPE
     */
    public java.lang.String getCUSTOMER_ACCOUNT_TYPE() {
        return CUSTOMER_ACCOUNT_TYPE;
    }


    /**
     * Sets the CUSTOMER_ACCOUNT_TYPE value for this CUSTOMER.
     * 
     * @param CUSTOMER_ACCOUNT_TYPE
     */
    public void setCUSTOMER_ACCOUNT_TYPE(java.lang.String CUSTOMER_ACCOUNT_TYPE) {
        this.CUSTOMER_ACCOUNT_TYPE = CUSTOMER_ACCOUNT_TYPE;
    }


    /**
     * Gets the COMMUNICATION_METHOD_CODE value for this CUSTOMER.
     * 
     * @return COMMUNICATION_METHOD_CODE
     */
    public java.lang.String getCOMMUNICATION_METHOD_CODE() {
        return COMMUNICATION_METHOD_CODE;
    }


    /**
     * Sets the COMMUNICATION_METHOD_CODE value for this CUSTOMER.
     * 
     * @param COMMUNICATION_METHOD_CODE
     */
    public void setCOMMUNICATION_METHOD_CODE(java.lang.String COMMUNICATION_METHOD_CODE) {
        this.COMMUNICATION_METHOD_CODE = COMMUNICATION_METHOD_CODE;
    }


    /**
     * Gets the CUSTOMER_STATUS_CODE value for this CUSTOMER.
     * 
     * @return CUSTOMER_STATUS_CODE
     */
    public java.lang.String getCUSTOMER_STATUS_CODE() {
        return CUSTOMER_STATUS_CODE;
    }


    /**
     * Sets the CUSTOMER_STATUS_CODE value for this CUSTOMER.
     * 
     * @param CUSTOMER_STATUS_CODE
     */
    public void setCUSTOMER_STATUS_CODE(java.lang.String CUSTOMER_STATUS_CODE) {
        this.CUSTOMER_STATUS_CODE = CUSTOMER_STATUS_CODE;
    }


    /**
     * Gets the CREATE_TIMESTAMP value for this CUSTOMER.
     * 
     * @return CREATE_TIMESTAMP
     */
    public java.lang.String getCREATE_TIMESTAMP() {
        return CREATE_TIMESTAMP;
    }


    /**
     * Sets the CREATE_TIMESTAMP value for this CUSTOMER.
     * 
     * @param CREATE_TIMESTAMP
     */
    public void setCREATE_TIMESTAMP(java.lang.String CREATE_TIMESTAMP) {
        this.CREATE_TIMESTAMP = CREATE_TIMESTAMP;
    }


    /**
     * Gets the LAST_UPDATE_DATE value for this CUSTOMER.
     * 
     * @return LAST_UPDATE_DATE
     */
    public java.lang.String getLAST_UPDATE_DATE() {
        return LAST_UPDATE_DATE;
    }


    /**
     * Sets the LAST_UPDATE_DATE value for this CUSTOMER.
     * 
     * @param LAST_UPDATE_DATE
     */
    public void setLAST_UPDATE_DATE(java.lang.String LAST_UPDATE_DATE) {
        this.LAST_UPDATE_DATE = LAST_UPDATE_DATE;
    }


    /**
     * Gets the LAST_UPDATE_USER_ID value for this CUSTOMER.
     * 
     * @return LAST_UPDATE_USER_ID
     */
    public java.lang.String getLAST_UPDATE_USER_ID() {
        return LAST_UPDATE_USER_ID;
    }


    /**
     * Sets the LAST_UPDATE_USER_ID value for this CUSTOMER.
     * 
     * @param LAST_UPDATE_USER_ID
     */
    public void setLAST_UPDATE_USER_ID(java.lang.String LAST_UPDATE_USER_ID) {
        this.LAST_UPDATE_USER_ID = LAST_UPDATE_USER_ID;
    }


    /**
     * Gets the EXTERNAL_ID value for this CUSTOMER.
     * 
     * @return EXTERNAL_ID
     */
    public java.math.BigInteger getEXTERNAL_ID() {
        return EXTERNAL_ID;
    }


    /**
     * Sets the EXTERNAL_ID value for this CUSTOMER.
     * 
     * @param EXTERNAL_ID
     */
    public void setEXTERNAL_ID(java.math.BigInteger EXTERNAL_ID) {
        this.EXTERNAL_ID = EXTERNAL_ID;
    }


    /**
     * Gets the PRIMARY_CUSTOMER_SWITCH value for this CUSTOMER.
     * 
     * @return PRIMARY_CUSTOMER_SWITCH
     */
    public java.lang.String getPRIMARY_CUSTOMER_SWITCH() {
        return PRIMARY_CUSTOMER_SWITCH;
    }


    /**
     * Sets the PRIMARY_CUSTOMER_SWITCH value for this CUSTOMER.
     * 
     * @param PRIMARY_CUSTOMER_SWITCH
     */
    public void setPRIMARY_CUSTOMER_SWITCH(java.lang.String PRIMARY_CUSTOMER_SWITCH) {
        this.PRIMARY_CUSTOMER_SWITCH = PRIMARY_CUSTOMER_SWITCH;
    }


    /**
     * Gets the PRIMARY_CUSTOMER_ID value for this CUSTOMER.
     * 
     * @return PRIMARY_CUSTOMER_ID
     */
    public java.math.BigInteger getPRIMARY_CUSTOMER_ID() {
        return PRIMARY_CUSTOMER_ID;
    }


    /**
     * Sets the PRIMARY_CUSTOMER_ID value for this CUSTOMER.
     * 
     * @param PRIMARY_CUSTOMER_ID
     */
    public void setPRIMARY_CUSTOMER_ID(java.math.BigInteger PRIMARY_CUSTOMER_ID) {
        this.PRIMARY_CUSTOMER_ID = PRIMARY_CUSTOMER_ID;
    }


    /**
     * Gets the APPEND_ATTEMPT_COUNT value for this CUSTOMER.
     * 
     * @return APPEND_ATTEMPT_COUNT
     */
    public java.math.BigInteger getAPPEND_ATTEMPT_COUNT() {
        return APPEND_ATTEMPT_COUNT;
    }


    /**
     * Sets the APPEND_ATTEMPT_COUNT value for this CUSTOMER.
     * 
     * @param APPEND_ATTEMPT_COUNT
     */
    public void setAPPEND_ATTEMPT_COUNT(java.math.BigInteger APPEND_ATTEMPT_COUNT) {
        this.APPEND_ATTEMPT_COUNT = APPEND_ATTEMPT_COUNT;
    }


    /**
     * Gets the LAST_APPEND_DATE value for this CUSTOMER.
     * 
     * @return LAST_APPEND_DATE
     */
    public java.util.Date getLAST_APPEND_DATE() {
        return LAST_APPEND_DATE;
    }


    /**
     * Sets the LAST_APPEND_DATE value for this CUSTOMER.
     * 
     * @param LAST_APPEND_DATE
     */
    public void setLAST_APPEND_DATE(java.util.Date LAST_APPEND_DATE) {
        this.LAST_APPEND_DATE = LAST_APPEND_DATE;
    }


    /**
     * Gets the CUSTOMER_ADDRESS value for this CUSTOMER.
     * 
     * @return CUSTOMER_ADDRESS
     */
    public com.heb.xmlns.ei.CUSTOMER.CUSTOMERCUSTOMER_ADDRESS[] getCUSTOMER_ADDRESS() {
        return CUSTOMER_ADDRESS;
    }


    /**
     * Sets the CUSTOMER_ADDRESS value for this CUSTOMER.
     * 
     * @param CUSTOMER_ADDRESS
     */
    public void setCUSTOMER_ADDRESS(com.heb.xmlns.ei.CUSTOMER.CUSTOMERCUSTOMER_ADDRESS[] CUSTOMER_ADDRESS) {
        this.CUSTOMER_ADDRESS = CUSTOMER_ADDRESS;
    }

    public com.heb.xmlns.ei.CUSTOMER.CUSTOMERCUSTOMER_ADDRESS getCUSTOMER_ADDRESS(int i) {
        return this.CUSTOMER_ADDRESS[i];
    }

    public void setCUSTOMER_ADDRESS(int i, com.heb.xmlns.ei.CUSTOMER.CUSTOMERCUSTOMER_ADDRESS _value) {
        this.CUSTOMER_ADDRESS[i] = _value;
    }


    /**
     * Gets the CUSTOMER_PHONE value for this CUSTOMER.
     * 
     * @return CUSTOMER_PHONE
     */
    public com.heb.xmlns.ei.CUSTOMER.CUSTOMERCUSTOMER_PHONE[] getCUSTOMER_PHONE() {
        return CUSTOMER_PHONE;
    }


    /**
     * Sets the CUSTOMER_PHONE value for this CUSTOMER.
     * 
     * @param CUSTOMER_PHONE
     */
    public void setCUSTOMER_PHONE(com.heb.xmlns.ei.CUSTOMER.CUSTOMERCUSTOMER_PHONE[] CUSTOMER_PHONE) {
        this.CUSTOMER_PHONE = CUSTOMER_PHONE;
    }

    public com.heb.xmlns.ei.CUSTOMER.CUSTOMERCUSTOMER_PHONE getCUSTOMER_PHONE(int i) {
        return this.CUSTOMER_PHONE[i];
    }

    public void setCUSTOMER_PHONE(int i, com.heb.xmlns.ei.CUSTOMER.CUSTOMERCUSTOMER_PHONE _value) {
        this.CUSTOMER_PHONE[i] = _value;
    }


    /**
     * Gets the CUSTOMER_EMAIL_ADDRESS value for this CUSTOMER.
     * 
     * @return CUSTOMER_EMAIL_ADDRESS
     */
    public com.heb.xmlns.ei.CUSTOMER.CUSTOMERCUSTOMER_EMAIL_ADDRESS getCUSTOMER_EMAIL_ADDRESS() {
        return CUSTOMER_EMAIL_ADDRESS;
    }


    /**
     * Sets the CUSTOMER_EMAIL_ADDRESS value for this CUSTOMER.
     * 
     * @param CUSTOMER_EMAIL_ADDRESS
     */
    public void setCUSTOMER_EMAIL_ADDRESS(com.heb.xmlns.ei.CUSTOMER.CUSTOMERCUSTOMER_EMAIL_ADDRESS CUSTOMER_EMAIL_ADDRESS) {
        this.CUSTOMER_EMAIL_ADDRESS = CUSTOMER_EMAIL_ADDRESS;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CUSTOMER)) return false;
        CUSTOMER other = (CUSTOMER) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.CUSTOMER_ID==null && other.getCUSTOMER_ID()==null) || 
             (this.CUSTOMER_ID!=null &&
              this.CUSTOMER_ID.equals(other.getCUSTOMER_ID()))) &&
            ((this.MODELED_UNIT_ID==null && other.getMODELED_UNIT_ID()==null) || 
             (this.MODELED_UNIT_ID!=null &&
              this.MODELED_UNIT_ID.equals(other.getMODELED_UNIT_ID()))) &&
            ((this.TITLE_NAME==null && other.getTITLE_NAME()==null) || 
             (this.TITLE_NAME!=null &&
              this.TITLE_NAME.equals(other.getTITLE_NAME()))) &&
            ((this.FIRST_NAME==null && other.getFIRST_NAME()==null) || 
             (this.FIRST_NAME!=null &&
              this.FIRST_NAME.equals(other.getFIRST_NAME()))) &&
            ((this.LAST_NAME==null && other.getLAST_NAME()==null) || 
             (this.LAST_NAME!=null &&
              this.LAST_NAME.equals(other.getLAST_NAME()))) &&
            ((this.SUFFIX_NAME==null && other.getSUFFIX_NAME()==null) || 
             (this.SUFFIX_NAME!=null &&
              this.SUFFIX_NAME.equals(other.getSUFFIX_NAME()))) &&
            ((this.BIRTH_DATE==null && other.getBIRTH_DATE()==null) || 
             (this.BIRTH_DATE!=null &&
              this.BIRTH_DATE.equals(other.getBIRTH_DATE()))) &&
            ((this.MIDDLE_NAME==null && other.getMIDDLE_NAME()==null) || 
             (this.MIDDLE_NAME!=null &&
              this.MIDDLE_NAME.equals(other.getMIDDLE_NAME()))) &&
            ((this.CUSTOMER_TYPE_CODE==null && other.getCUSTOMER_TYPE_CODE()==null) || 
             (this.CUSTOMER_TYPE_CODE!=null &&
              this.CUSTOMER_TYPE_CODE.equals(other.getCUSTOMER_TYPE_CODE()))) &&
            ((this.PREFERENCE_LANGUAGE_TYPE_CODE==null && other.getPREFERENCE_LANGUAGE_TYPE_CODE()==null) || 
             (this.PREFERENCE_LANGUAGE_TYPE_CODE!=null &&
              this.PREFERENCE_LANGUAGE_TYPE_CODE.equals(other.getPREFERENCE_LANGUAGE_TYPE_CODE()))) &&
            ((this.FIRST_PURCHASE_DATE==null && other.getFIRST_PURCHASE_DATE()==null) || 
             (this.FIRST_PURCHASE_DATE!=null &&
              this.FIRST_PURCHASE_DATE.equals(other.getFIRST_PURCHASE_DATE()))) &&
            ((this.LAST_PURCHASE_DATE==null && other.getLAST_PURCHASE_DATE()==null) || 
             (this.LAST_PURCHASE_DATE!=null &&
              this.LAST_PURCHASE_DATE.equals(other.getLAST_PURCHASE_DATE()))) &&
            ((this.NO_CALL_SWITCH==null && other.getNO_CALL_SWITCH()==null) || 
             (this.NO_CALL_SWITCH!=null &&
              this.NO_CALL_SWITCH.equals(other.getNO_CALL_SWITCH()))) &&
            ((this.NO_MAIL_SWITCH==null && other.getNO_MAIL_SWITCH()==null) || 
             (this.NO_MAIL_SWITCH!=null &&
              this.NO_MAIL_SWITCH.equals(other.getNO_MAIL_SWITCH()))) &&
            ((this.OFFER_EMAIL_OPTION_IN_SWITCH==null && other.getOFFER_EMAIL_OPTION_IN_SWITCH()==null) || 
             (this.OFFER_EMAIL_OPTION_IN_SWITCH!=null &&
              this.OFFER_EMAIL_OPTION_IN_SWITCH.equals(other.getOFFER_EMAIL_OPTION_IN_SWITCH()))) &&
            ((this.REWARD_EMAIL_OPTION_IN_SWITCH==null && other.getREWARD_EMAIL_OPTION_IN_SWITCH()==null) || 
             (this.REWARD_EMAIL_OPTION_IN_SWITCH!=null &&
              this.REWARD_EMAIL_OPTION_IN_SWITCH.equals(other.getREWARD_EMAIL_OPTION_IN_SWITCH()))) &&
            ((this.SEND_ALL_MAIL_SWITCH==null && other.getSEND_ALL_MAIL_SWITCH()==null) || 
             (this.SEND_ALL_MAIL_SWITCH!=null &&
              this.SEND_ALL_MAIL_SWITCH.equals(other.getSEND_ALL_MAIL_SWITCH()))) &&
            ((this.WEB_REWARDS_OPTION_IN_SWITCH==null && other.getWEB_REWARDS_OPTION_IN_SWITCH()==null) || 
             (this.WEB_REWARDS_OPTION_IN_SWITCH!=null &&
              this.WEB_REWARDS_OPTION_IN_SWITCH.equals(other.getWEB_REWARDS_OPTION_IN_SWITCH()))) &&
            ((this.WEB_REWARDS_OPTION_IN_DATE==null && other.getWEB_REWARDS_OPTION_IN_DATE()==null) || 
             (this.WEB_REWARDS_OPTION_IN_DATE!=null &&
              this.WEB_REWARDS_OPTION_IN_DATE.equals(other.getWEB_REWARDS_OPTION_IN_DATE()))) &&
            ((this.CUSTOMER_SOURCE_CODE==null && other.getCUSTOMER_SOURCE_CODE()==null) || 
             (this.CUSTOMER_SOURCE_CODE!=null &&
              this.CUSTOMER_SOURCE_CODE.equals(other.getCUSTOMER_SOURCE_CODE()))) &&
            ((this.CUSTOMER_ACCOUNT_ALPHA_ID==null && other.getCUSTOMER_ACCOUNT_ALPHA_ID()==null) || 
             (this.CUSTOMER_ACCOUNT_ALPHA_ID!=null &&
              this.CUSTOMER_ACCOUNT_ALPHA_ID.equals(other.getCUSTOMER_ACCOUNT_ALPHA_ID()))) &&
            ((this.CUSTOMER_ACCOUNT_NUMERIC_ID==null && other.getCUSTOMER_ACCOUNT_NUMERIC_ID()==null) || 
             (this.CUSTOMER_ACCOUNT_NUMERIC_ID!=null &&
              this.CUSTOMER_ACCOUNT_NUMERIC_ID.equals(other.getCUSTOMER_ACCOUNT_NUMERIC_ID()))) &&
            ((this.CUSTOMER_ACCOUNT_TYPE==null && other.getCUSTOMER_ACCOUNT_TYPE()==null) || 
             (this.CUSTOMER_ACCOUNT_TYPE!=null &&
              this.CUSTOMER_ACCOUNT_TYPE.equals(other.getCUSTOMER_ACCOUNT_TYPE()))) &&
            ((this.COMMUNICATION_METHOD_CODE==null && other.getCOMMUNICATION_METHOD_CODE()==null) || 
             (this.COMMUNICATION_METHOD_CODE!=null &&
              this.COMMUNICATION_METHOD_CODE.equals(other.getCOMMUNICATION_METHOD_CODE()))) &&
            ((this.CUSTOMER_STATUS_CODE==null && other.getCUSTOMER_STATUS_CODE()==null) || 
             (this.CUSTOMER_STATUS_CODE!=null &&
              this.CUSTOMER_STATUS_CODE.equals(other.getCUSTOMER_STATUS_CODE()))) &&
            ((this.CREATE_TIMESTAMP==null && other.getCREATE_TIMESTAMP()==null) || 
             (this.CREATE_TIMESTAMP!=null &&
              this.CREATE_TIMESTAMP.equals(other.getCREATE_TIMESTAMP()))) &&
            ((this.LAST_UPDATE_DATE==null && other.getLAST_UPDATE_DATE()==null) || 
             (this.LAST_UPDATE_DATE!=null &&
              this.LAST_UPDATE_DATE.equals(other.getLAST_UPDATE_DATE()))) &&
            ((this.LAST_UPDATE_USER_ID==null && other.getLAST_UPDATE_USER_ID()==null) || 
             (this.LAST_UPDATE_USER_ID!=null &&
              this.LAST_UPDATE_USER_ID.equals(other.getLAST_UPDATE_USER_ID()))) &&
            ((this.EXTERNAL_ID==null && other.getEXTERNAL_ID()==null) || 
             (this.EXTERNAL_ID!=null &&
              this.EXTERNAL_ID.equals(other.getEXTERNAL_ID()))) &&
            ((this.PRIMARY_CUSTOMER_SWITCH==null && other.getPRIMARY_CUSTOMER_SWITCH()==null) || 
             (this.PRIMARY_CUSTOMER_SWITCH!=null &&
              this.PRIMARY_CUSTOMER_SWITCH.equals(other.getPRIMARY_CUSTOMER_SWITCH()))) &&
            ((this.PRIMARY_CUSTOMER_ID==null && other.getPRIMARY_CUSTOMER_ID()==null) || 
             (this.PRIMARY_CUSTOMER_ID!=null &&
              this.PRIMARY_CUSTOMER_ID.equals(other.getPRIMARY_CUSTOMER_ID()))) &&
            ((this.APPEND_ATTEMPT_COUNT==null && other.getAPPEND_ATTEMPT_COUNT()==null) || 
             (this.APPEND_ATTEMPT_COUNT!=null &&
              this.APPEND_ATTEMPT_COUNT.equals(other.getAPPEND_ATTEMPT_COUNT()))) &&
            ((this.LAST_APPEND_DATE==null && other.getLAST_APPEND_DATE()==null) || 
             (this.LAST_APPEND_DATE!=null &&
              this.LAST_APPEND_DATE.equals(other.getLAST_APPEND_DATE()))) &&
            ((this.CUSTOMER_ADDRESS==null && other.getCUSTOMER_ADDRESS()==null) || 
             (this.CUSTOMER_ADDRESS!=null &&
              java.util.Arrays.equals(this.CUSTOMER_ADDRESS, other.getCUSTOMER_ADDRESS()))) &&
            ((this.CUSTOMER_PHONE==null && other.getCUSTOMER_PHONE()==null) || 
             (this.CUSTOMER_PHONE!=null &&
              java.util.Arrays.equals(this.CUSTOMER_PHONE, other.getCUSTOMER_PHONE()))) &&
            ((this.CUSTOMER_EMAIL_ADDRESS==null && other.getCUSTOMER_EMAIL_ADDRESS()==null) || 
             (this.CUSTOMER_EMAIL_ADDRESS!=null &&
              this.CUSTOMER_EMAIL_ADDRESS.equals(other.getCUSTOMER_EMAIL_ADDRESS())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCUSTOMER_ID() != null) {
            _hashCode += getCUSTOMER_ID().hashCode();
        }
        if (getMODELED_UNIT_ID() != null) {
            _hashCode += getMODELED_UNIT_ID().hashCode();
        }
        if (getTITLE_NAME() != null) {
            _hashCode += getTITLE_NAME().hashCode();
        }
        if (getFIRST_NAME() != null) {
            _hashCode += getFIRST_NAME().hashCode();
        }
        if (getLAST_NAME() != null) {
            _hashCode += getLAST_NAME().hashCode();
        }
        if (getSUFFIX_NAME() != null) {
            _hashCode += getSUFFIX_NAME().hashCode();
        }
        if (getBIRTH_DATE() != null) {
            _hashCode += getBIRTH_DATE().hashCode();
        }
        if (getMIDDLE_NAME() != null) {
            _hashCode += getMIDDLE_NAME().hashCode();
        }
        if (getCUSTOMER_TYPE_CODE() != null) {
            _hashCode += getCUSTOMER_TYPE_CODE().hashCode();
        }
        if (getPREFERENCE_LANGUAGE_TYPE_CODE() != null) {
            _hashCode += getPREFERENCE_LANGUAGE_TYPE_CODE().hashCode();
        }
        if (getFIRST_PURCHASE_DATE() != null) {
            _hashCode += getFIRST_PURCHASE_DATE().hashCode();
        }
        if (getLAST_PURCHASE_DATE() != null) {
            _hashCode += getLAST_PURCHASE_DATE().hashCode();
        }
        if (getNO_CALL_SWITCH() != null) {
            _hashCode += getNO_CALL_SWITCH().hashCode();
        }
        if (getNO_MAIL_SWITCH() != null) {
            _hashCode += getNO_MAIL_SWITCH().hashCode();
        }
        if (getOFFER_EMAIL_OPTION_IN_SWITCH() != null) {
            _hashCode += getOFFER_EMAIL_OPTION_IN_SWITCH().hashCode();
        }
        if (getREWARD_EMAIL_OPTION_IN_SWITCH() != null) {
            _hashCode += getREWARD_EMAIL_OPTION_IN_SWITCH().hashCode();
        }
        if (getSEND_ALL_MAIL_SWITCH() != null) {
            _hashCode += getSEND_ALL_MAIL_SWITCH().hashCode();
        }
        if (getWEB_REWARDS_OPTION_IN_SWITCH() != null) {
            _hashCode += getWEB_REWARDS_OPTION_IN_SWITCH().hashCode();
        }
        if (getWEB_REWARDS_OPTION_IN_DATE() != null) {
            _hashCode += getWEB_REWARDS_OPTION_IN_DATE().hashCode();
        }
        if (getCUSTOMER_SOURCE_CODE() != null) {
            _hashCode += getCUSTOMER_SOURCE_CODE().hashCode();
        }
        if (getCUSTOMER_ACCOUNT_ALPHA_ID() != null) {
            _hashCode += getCUSTOMER_ACCOUNT_ALPHA_ID().hashCode();
        }
        if (getCUSTOMER_ACCOUNT_NUMERIC_ID() != null) {
            _hashCode += getCUSTOMER_ACCOUNT_NUMERIC_ID().hashCode();
        }
        if (getCUSTOMER_ACCOUNT_TYPE() != null) {
            _hashCode += getCUSTOMER_ACCOUNT_TYPE().hashCode();
        }
        if (getCOMMUNICATION_METHOD_CODE() != null) {
            _hashCode += getCOMMUNICATION_METHOD_CODE().hashCode();
        }
        if (getCUSTOMER_STATUS_CODE() != null) {
            _hashCode += getCUSTOMER_STATUS_CODE().hashCode();
        }
        if (getCREATE_TIMESTAMP() != null) {
            _hashCode += getCREATE_TIMESTAMP().hashCode();
        }
        if (getLAST_UPDATE_DATE() != null) {
            _hashCode += getLAST_UPDATE_DATE().hashCode();
        }
        if (getLAST_UPDATE_USER_ID() != null) {
            _hashCode += getLAST_UPDATE_USER_ID().hashCode();
        }
        if (getEXTERNAL_ID() != null) {
            _hashCode += getEXTERNAL_ID().hashCode();
        }
        if (getPRIMARY_CUSTOMER_SWITCH() != null) {
            _hashCode += getPRIMARY_CUSTOMER_SWITCH().hashCode();
        }
        if (getPRIMARY_CUSTOMER_ID() != null) {
            _hashCode += getPRIMARY_CUSTOMER_ID().hashCode();
        }
        if (getAPPEND_ATTEMPT_COUNT() != null) {
            _hashCode += getAPPEND_ATTEMPT_COUNT().hashCode();
        }
        if (getLAST_APPEND_DATE() != null) {
            _hashCode += getLAST_APPEND_DATE().hashCode();
        }
        if (getCUSTOMER_ADDRESS() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCUSTOMER_ADDRESS());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCUSTOMER_ADDRESS(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCUSTOMER_PHONE() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCUSTOMER_PHONE());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCUSTOMER_PHONE(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCUSTOMER_EMAIL_ADDRESS() != null) {
            _hashCode += getCUSTOMER_EMAIL_ADDRESS().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CUSTOMER.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", ">CUSTOMER"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CUSTOMER_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "CUSTOMER_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MODELED_UNIT_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "MODELED_UNIT_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TITLE_NAME");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "TITLE_NAME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("FIRST_NAME");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "FIRST_NAME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LAST_NAME");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "LAST_NAME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SUFFIX_NAME");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "SUFFIX_NAME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("BIRTH_DATE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "BIRTH_DATE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MIDDLE_NAME");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "MIDDLE_NAME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CUSTOMER_TYPE_CODE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "CUSTOMER_TYPE_CODE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PREFERENCE_LANGUAGE_TYPE_CODE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "PREFERENCE_LANGUAGE_TYPE_CODE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("FIRST_PURCHASE_DATE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "FIRST_PURCHASE_DATE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LAST_PURCHASE_DATE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "LAST_PURCHASE_DATE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NO_CALL_SWITCH");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "NO_CALL_SWITCH"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NO_MAIL_SWITCH");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "NO_MAIL_SWITCH"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("OFFER_EMAIL_OPTION_IN_SWITCH");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "OFFER_EMAIL_OPTION_IN_SWITCH"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("REWARD_EMAIL_OPTION_IN_SWITCH");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "REWARD_EMAIL_OPTION_IN_SWITCH"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SEND_ALL_MAIL_SWITCH");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "SEND_ALL_MAIL_SWITCH"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("WEB_REWARDS_OPTION_IN_SWITCH");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "WEB_REWARDS_OPTION_IN_SWITCH"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("WEB_REWARDS_OPTION_IN_DATE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "WEB_REWARDS_OPTION_IN_DATE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CUSTOMER_SOURCE_CODE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "CUSTOMER_SOURCE_CODE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CUSTOMER_ACCOUNT_ALPHA_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "CUSTOMER_ACCOUNT_ALPHA_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CUSTOMER_ACCOUNT_NUMERIC_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "CUSTOMER_ACCOUNT_NUMERIC_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CUSTOMER_ACCOUNT_TYPE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "CUSTOMER_ACCOUNT_TYPE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("COMMUNICATION_METHOD_CODE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "COMMUNICATION_METHOD_CODE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CUSTOMER_STATUS_CODE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "CUSTOMER_STATUS_CODE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CREATE_TIMESTAMP");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "CREATE_TIMESTAMP"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LAST_UPDATE_DATE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "LAST_UPDATE_DATE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LAST_UPDATE_USER_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "LAST_UPDATE_USER_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("EXTERNAL_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "EXTERNAL_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PRIMARY_CUSTOMER_SWITCH");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "PRIMARY_CUSTOMER_SWITCH"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PRIMARY_CUSTOMER_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "PRIMARY_CUSTOMER_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("APPEND_ATTEMPT_COUNT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "APPEND_ATTEMPT_COUNT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LAST_APPEND_DATE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "LAST_APPEND_DATE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CUSTOMER_ADDRESS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "CUSTOMER_ADDRESS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", ">>CUSTOMER>CUSTOMER_ADDRESS"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CUSTOMER_PHONE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "CUSTOMER_PHONE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", ">>CUSTOMER>CUSTOMER_PHONE"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CUSTOMER_EMAIL_ADDRESS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "CUSTOMER_EMAIL_ADDRESS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", ">>CUSTOMER>CUSTOMER_EMAIL_ADDRESS"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
