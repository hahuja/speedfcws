/**
 * REASON_CODES.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.xmlns.ei.REASON_CODES;

public class REASON_CODES  implements java.io.Serializable {
    private java.lang.String REASON_ID;

    private java.lang.String REASON_DESCRIPTION;

    private java.lang.String DEACTIVATION_NOW;

    public REASON_CODES() {
    }

    public REASON_CODES(
           java.lang.String REASON_ID,
           java.lang.String REASON_DESCRIPTION,
           java.lang.String DEACTIVATION_NOW) {
           this.REASON_ID = REASON_ID;
           this.REASON_DESCRIPTION = REASON_DESCRIPTION;
           this.DEACTIVATION_NOW = DEACTIVATION_NOW;
    }


    /**
     * Gets the REASON_ID value for this REASON_CODES.
     * 
     * @return REASON_ID
     */
    public java.lang.String getREASON_ID() {
        return REASON_ID;
    }


    /**
     * Sets the REASON_ID value for this REASON_CODES.
     * 
     * @param REASON_ID
     */
    public void setREASON_ID(java.lang.String REASON_ID) {
        this.REASON_ID = REASON_ID;
    }


    /**
     * Gets the REASON_DESCRIPTION value for this REASON_CODES.
     * 
     * @return REASON_DESCRIPTION
     */
    public java.lang.String getREASON_DESCRIPTION() {
        return REASON_DESCRIPTION;
    }


    /**
     * Sets the REASON_DESCRIPTION value for this REASON_CODES.
     * 
     * @param REASON_DESCRIPTION
     */
    public void setREASON_DESCRIPTION(java.lang.String REASON_DESCRIPTION) {
        this.REASON_DESCRIPTION = REASON_DESCRIPTION;
    }


    /**
     * Gets the DEACTIVATION_NOW value for this REASON_CODES.
     * 
     * @return DEACTIVATION_NOW
     */
    public java.lang.String getDEACTIVATION_NOW() {
        return DEACTIVATION_NOW;
    }


    /**
     * Sets the DEACTIVATION_NOW value for this REASON_CODES.
     * 
     * @param DEACTIVATION_NOW
     */
    public void setDEACTIVATION_NOW(java.lang.String DEACTIVATION_NOW) {
        this.DEACTIVATION_NOW = DEACTIVATION_NOW;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof REASON_CODES)) return false;
        REASON_CODES other = (REASON_CODES) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.REASON_ID==null && other.getREASON_ID()==null) || 
             (this.REASON_ID!=null &&
              this.REASON_ID.equals(other.getREASON_ID()))) &&
            ((this.REASON_DESCRIPTION==null && other.getREASON_DESCRIPTION()==null) || 
             (this.REASON_DESCRIPTION!=null &&
              this.REASON_DESCRIPTION.equals(other.getREASON_DESCRIPTION()))) &&
            ((this.DEACTIVATION_NOW==null && other.getDEACTIVATION_NOW()==null) || 
             (this.DEACTIVATION_NOW!=null &&
              this.DEACTIVATION_NOW.equals(other.getDEACTIVATION_NOW())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getREASON_ID() != null) {
            _hashCode += getREASON_ID().hashCode();
        }
        if (getREASON_DESCRIPTION() != null) {
            _hashCode += getREASON_DESCRIPTION().hashCode();
        }
        if (getDEACTIVATION_NOW() != null) {
            _hashCode += getDEACTIVATION_NOW().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(REASON_CODES.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/REASON_CODES", ">REASON_CODES"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("REASON_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/REASON_CODES", "REASON_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("REASON_DESCRIPTION");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/REASON_CODES", "REASON_DESCRIPTION"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DEACTIVATION_NOW");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/REASON_CODES", "DEACTIVATION_NOW"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
