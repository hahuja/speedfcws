/**
 * GetReceiptDetails_Request.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.xmlns.ei.GetReceiptDetails_Request;

public class GetReceiptDetails_Request  implements java.io.Serializable {
    private java.util.Date DATE;

    private java.math.BigInteger STORE;

    private java.math.BigInteger ORDER_NUMBER;

    private java.math.BigInteger HOUSEHOLD_ID;

    private java.lang.String RECIEPT_ID;

    private com.heb.xmlns.ei.Authentication.Authentication authentication;

    public GetReceiptDetails_Request() {
    }

    public GetReceiptDetails_Request(
           java.util.Date DATE,
           java.math.BigInteger STORE,
           java.math.BigInteger ORDER_NUMBER,
           java.math.BigInteger HOUSEHOLD_ID,
           java.lang.String RECIEPT_ID,
           com.heb.xmlns.ei.Authentication.Authentication authentication) {
           this.DATE = DATE;
           this.STORE = STORE;
           this.ORDER_NUMBER = ORDER_NUMBER;
           this.HOUSEHOLD_ID = HOUSEHOLD_ID;
           this.RECIEPT_ID = RECIEPT_ID;
           this.authentication = authentication;
    }


    /**
     * Gets the DATE value for this GetReceiptDetails_Request.
     * 
     * @return DATE
     */
    public java.util.Date getDATE() {
        return DATE;
    }


    /**
     * Sets the DATE value for this GetReceiptDetails_Request.
     * 
     * @param DATE
     */
    public void setDATE(java.util.Date DATE) {
        this.DATE = DATE;
    }


    /**
     * Gets the STORE value for this GetReceiptDetails_Request.
     * 
     * @return STORE
     */
    public java.math.BigInteger getSTORE() {
        return STORE;
    }


    /**
     * Sets the STORE value for this GetReceiptDetails_Request.
     * 
     * @param STORE
     */
    public void setSTORE(java.math.BigInteger STORE) {
        this.STORE = STORE;
    }


    /**
     * Gets the ORDER_NUMBER value for this GetReceiptDetails_Request.
     * 
     * @return ORDER_NUMBER
     */
    public java.math.BigInteger getORDER_NUMBER() {
        return ORDER_NUMBER;
    }


    /**
     * Sets the ORDER_NUMBER value for this GetReceiptDetails_Request.
     * 
     * @param ORDER_NUMBER
     */
    public void setORDER_NUMBER(java.math.BigInteger ORDER_NUMBER) {
        this.ORDER_NUMBER = ORDER_NUMBER;
    }


    /**
     * Gets the HOUSEHOLD_ID value for this GetReceiptDetails_Request.
     * 
     * @return HOUSEHOLD_ID
     */
    public java.math.BigInteger getHOUSEHOLD_ID() {
        return HOUSEHOLD_ID;
    }


    /**
     * Sets the HOUSEHOLD_ID value for this GetReceiptDetails_Request.
     * 
     * @param HOUSEHOLD_ID
     */
    public void setHOUSEHOLD_ID(java.math.BigInteger HOUSEHOLD_ID) {
        this.HOUSEHOLD_ID = HOUSEHOLD_ID;
    }


    /**
     * Gets the RECIEPT_ID value for this GetReceiptDetails_Request.
     * 
     * @return RECIEPT_ID
     */
    public java.lang.String getRECIEPT_ID() {
        return RECIEPT_ID;
    }


    /**
     * Sets the RECIEPT_ID value for this GetReceiptDetails_Request.
     * 
     * @param RECIEPT_ID
     */
    public void setRECIEPT_ID(java.lang.String RECIEPT_ID) {
        this.RECIEPT_ID = RECIEPT_ID;
    }


    /**
     * Gets the authentication value for this GetReceiptDetails_Request.
     * 
     * @return authentication
     */
    public com.heb.xmlns.ei.Authentication.Authentication getAuthentication() {
        return authentication;
    }


    /**
     * Sets the authentication value for this GetReceiptDetails_Request.
     * 
     * @param authentication
     */
    public void setAuthentication(com.heb.xmlns.ei.Authentication.Authentication authentication) {
        this.authentication = authentication;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetReceiptDetails_Request)) return false;
        GetReceiptDetails_Request other = (GetReceiptDetails_Request) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.DATE==null && other.getDATE()==null) || 
             (this.DATE!=null &&
              this.DATE.equals(other.getDATE()))) &&
            ((this.STORE==null && other.getSTORE()==null) || 
             (this.STORE!=null &&
              this.STORE.equals(other.getSTORE()))) &&
            ((this.ORDER_NUMBER==null && other.getORDER_NUMBER()==null) || 
             (this.ORDER_NUMBER!=null &&
              this.ORDER_NUMBER.equals(other.getORDER_NUMBER()))) &&
            ((this.HOUSEHOLD_ID==null && other.getHOUSEHOLD_ID()==null) || 
             (this.HOUSEHOLD_ID!=null &&
              this.HOUSEHOLD_ID.equals(other.getHOUSEHOLD_ID()))) &&
            ((this.RECIEPT_ID==null && other.getRECIEPT_ID()==null) || 
             (this.RECIEPT_ID!=null &&
              this.RECIEPT_ID.equals(other.getRECIEPT_ID()))) &&
            ((this.authentication==null && other.getAuthentication()==null) || 
             (this.authentication!=null &&
              this.authentication.equals(other.getAuthentication())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDATE() != null) {
            _hashCode += getDATE().hashCode();
        }
        if (getSTORE() != null) {
            _hashCode += getSTORE().hashCode();
        }
        if (getORDER_NUMBER() != null) {
            _hashCode += getORDER_NUMBER().hashCode();
        }
        if (getHOUSEHOLD_ID() != null) {
            _hashCode += getHOUSEHOLD_ID().hashCode();
        }
        if (getRECIEPT_ID() != null) {
            _hashCode += getRECIEPT_ID().hashCode();
        }
        if (getAuthentication() != null) {
            _hashCode += getAuthentication().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetReceiptDetails_Request.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetReceiptDetails_Request", ">GetReceiptDetails_Request"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetReceiptDetails_Request", "DATE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("STORE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetReceiptDetails_Request", "STORE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ORDER_NUMBER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetReceiptDetails_Request", "ORDER_NUMBER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("HOUSEHOLD_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetReceiptDetails_Request", "HOUSEHOLD_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("RECIEPT_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetReceiptDetails_Request", "RECIEPT_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authentication");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Authentication", "Authentication"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Authentication", ">Authentication"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
