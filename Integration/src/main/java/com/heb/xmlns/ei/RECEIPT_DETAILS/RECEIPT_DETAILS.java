/**
 * RECEIPT_DETAILS.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.xmlns.ei.RECEIPT_DETAILS;

public class RECEIPT_DETAILS  implements java.io.Serializable {
    private java.lang.String UPC;

    private java.math.BigDecimal QUANTITY;

    private java.lang.String DESCRIPTION;

    private java.math.BigDecimal DETAIL_AMOUNT;

    public RECEIPT_DETAILS() {
    }

    public RECEIPT_DETAILS(
           java.lang.String UPC,
           java.math.BigDecimal QUANTITY,
           java.lang.String DESCRIPTION,
           java.math.BigDecimal DETAIL_AMOUNT) {
           this.UPC = UPC;
           this.QUANTITY = QUANTITY;
           this.DESCRIPTION = DESCRIPTION;
           this.DETAIL_AMOUNT = DETAIL_AMOUNT;
    }


    /**
     * Gets the UPC value for this RECEIPT_DETAILS.
     * 
     * @return UPC
     */
    public java.lang.String getUPC() {
        return UPC;
    }


    /**
     * Sets the UPC value for this RECEIPT_DETAILS.
     * 
     * @param UPC
     */
    public void setUPC(java.lang.String UPC) {
        this.UPC = UPC;
    }


    /**
     * Gets the QUANTITY value for this RECEIPT_DETAILS.
     * 
     * @return QUANTITY
     */
    public java.math.BigDecimal getQUANTITY() {
        return QUANTITY;
    }


    /**
     * Sets the QUANTITY value for this RECEIPT_DETAILS.
     * 
     * @param QUANTITY
     */
    public void setQUANTITY(java.math.BigDecimal QUANTITY) {
        this.QUANTITY = QUANTITY;
    }


    /**
     * Gets the DESCRIPTION value for this RECEIPT_DETAILS.
     * 
     * @return DESCRIPTION
     */
    public java.lang.String getDESCRIPTION() {
        return DESCRIPTION;
    }


    /**
     * Sets the DESCRIPTION value for this RECEIPT_DETAILS.
     * 
     * @param DESCRIPTION
     */
    public void setDESCRIPTION(java.lang.String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }


    /**
     * Gets the DETAIL_AMOUNT value for this RECEIPT_DETAILS.
     * 
     * @return DETAIL_AMOUNT
     */
    public java.math.BigDecimal getDETAIL_AMOUNT() {
        return DETAIL_AMOUNT;
    }


    /**
     * Sets the DETAIL_AMOUNT value for this RECEIPT_DETAILS.
     * 
     * @param DETAIL_AMOUNT
     */
    public void setDETAIL_AMOUNT(java.math.BigDecimal DETAIL_AMOUNT) {
        this.DETAIL_AMOUNT = DETAIL_AMOUNT;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RECEIPT_DETAILS)) return false;
        RECEIPT_DETAILS other = (RECEIPT_DETAILS) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.UPC==null && other.getUPC()==null) || 
             (this.UPC!=null &&
              this.UPC.equals(other.getUPC()))) &&
            ((this.QUANTITY==null && other.getQUANTITY()==null) || 
             (this.QUANTITY!=null &&
              this.QUANTITY.equals(other.getQUANTITY()))) &&
            ((this.DESCRIPTION==null && other.getDESCRIPTION()==null) || 
             (this.DESCRIPTION!=null &&
              this.DESCRIPTION.equals(other.getDESCRIPTION()))) &&
            ((this.DETAIL_AMOUNT==null && other.getDETAIL_AMOUNT()==null) || 
             (this.DETAIL_AMOUNT!=null &&
              this.DETAIL_AMOUNT.equals(other.getDETAIL_AMOUNT())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getUPC() != null) {
            _hashCode += getUPC().hashCode();
        }
        if (getQUANTITY() != null) {
            _hashCode += getQUANTITY().hashCode();
        }
        if (getDESCRIPTION() != null) {
            _hashCode += getDESCRIPTION().hashCode();
        }
        if (getDETAIL_AMOUNT() != null) {
            _hashCode += getDETAIL_AMOUNT().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RECEIPT_DETAILS.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/RECEIPT_DETAILS", ">RECEIPT_DETAILS"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("UPC");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/RECEIPT_DETAILS", "UPC"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("QUANTITY");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/RECEIPT_DETAILS", "QUANTITY"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DESCRIPTION");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/RECEIPT_DETAILS", "DESCRIPTION"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DETAIL_AMOUNT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/RECEIPT_DETAILS", "DETAIL_AMOUNT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
