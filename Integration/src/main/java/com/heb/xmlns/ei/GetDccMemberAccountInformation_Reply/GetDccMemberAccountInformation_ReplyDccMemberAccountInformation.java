/**
 * GetDccMemberAccountInformation_ReplyDccMemberAccountInformation.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.xmlns.ei.GetDccMemberAccountInformation_Reply;

public class GetDccMemberAccountInformation_ReplyDccMemberAccountInformation  implements java.io.Serializable {
    private com.heb.xmlns.ei.GetDccMemberAccountInformation_Reply.GetDccMemberAccountInformation_ReplyDccMemberAccountInformationCustomers customers;

    private com.heb.xmlns.ei.GetDccMemberAccountInformation_Reply.GetDccMemberAccountInformation_ReplyDccMemberAccountInformationOffers offers;

    public GetDccMemberAccountInformation_ReplyDccMemberAccountInformation() {
    }

    public GetDccMemberAccountInformation_ReplyDccMemberAccountInformation(
           com.heb.xmlns.ei.GetDccMemberAccountInformation_Reply.GetDccMemberAccountInformation_ReplyDccMemberAccountInformationCustomers customers,
           com.heb.xmlns.ei.GetDccMemberAccountInformation_Reply.GetDccMemberAccountInformation_ReplyDccMemberAccountInformationOffers offers) {
           this.customers = customers;
           this.offers = offers;
    }


    /**
     * Gets the customers value for this GetDccMemberAccountInformation_ReplyDccMemberAccountInformation.
     * 
     * @return customers
     */
    public com.heb.xmlns.ei.GetDccMemberAccountInformation_Reply.GetDccMemberAccountInformation_ReplyDccMemberAccountInformationCustomers getCustomers() {
        return customers;
    }


    /**
     * Sets the customers value for this GetDccMemberAccountInformation_ReplyDccMemberAccountInformation.
     * 
     * @param customers
     */
    public void setCustomers(com.heb.xmlns.ei.GetDccMemberAccountInformation_Reply.GetDccMemberAccountInformation_ReplyDccMemberAccountInformationCustomers customers) {
        this.customers = customers;
    }


    /**
     * Gets the offers value for this GetDccMemberAccountInformation_ReplyDccMemberAccountInformation.
     * 
     * @return offers
     */
    public com.heb.xmlns.ei.GetDccMemberAccountInformation_Reply.GetDccMemberAccountInformation_ReplyDccMemberAccountInformationOffers getOffers() {
        return offers;
    }


    /**
     * Sets the offers value for this GetDccMemberAccountInformation_ReplyDccMemberAccountInformation.
     * 
     * @param offers
     */
    public void setOffers(com.heb.xmlns.ei.GetDccMemberAccountInformation_Reply.GetDccMemberAccountInformation_ReplyDccMemberAccountInformationOffers offers) {
        this.offers = offers;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetDccMemberAccountInformation_ReplyDccMemberAccountInformation)) return false;
        GetDccMemberAccountInformation_ReplyDccMemberAccountInformation other = (GetDccMemberAccountInformation_ReplyDccMemberAccountInformation) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.customers==null && other.getCustomers()==null) || 
             (this.customers!=null &&
              this.customers.equals(other.getCustomers()))) &&
            ((this.offers==null && other.getOffers()==null) || 
             (this.offers!=null &&
              this.offers.equals(other.getOffers())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCustomers() != null) {
            _hashCode += getCustomers().hashCode();
        }
        if (getOffers() != null) {
            _hashCode += getOffers().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetDccMemberAccountInformation_ReplyDccMemberAccountInformation.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetDccMemberAccountInformation_Reply", ">>GetDccMemberAccountInformation_Reply>DccMemberAccountInformation"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customers");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetDccMemberAccountInformation_Reply", "Customers"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetDccMemberAccountInformation_Reply", ">>>GetDccMemberAccountInformation_Reply>DccMemberAccountInformation>Customers"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("offers");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetDccMemberAccountInformation_Reply", "Offers"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetDccMemberAccountInformation_Reply", ">>>GetDccMemberAccountInformation_Reply>DccMemberAccountInformation>Offers"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
