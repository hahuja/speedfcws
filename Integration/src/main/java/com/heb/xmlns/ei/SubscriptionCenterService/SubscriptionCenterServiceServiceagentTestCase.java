/**
 * SubscriptionCenterServiceServiceagentTestCase.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.xmlns.ei.SubscriptionCenterService;

public class SubscriptionCenterServiceServiceagentTestCase extends junit.framework.TestCase {
    public SubscriptionCenterServiceServiceagentTestCase(java.lang.String name) {
        super(name);
    }

    public void testSubscriptionCenterServiceWSDL() throws Exception {
        javax.xml.rpc.ServiceFactory serviceFactory = javax.xml.rpc.ServiceFactory.newInstance();
        java.net.URL url = new java.net.URL(new com.heb.xmlns.ei.SubscriptionCenterService.SubscriptionCenterServiceServiceagentLocator().getSubscriptionCenterServiceAddress() + "?WSDL");
        javax.xml.rpc.Service service = serviceFactory.createService(url, new com.heb.xmlns.ei.SubscriptionCenterService.SubscriptionCenterServiceServiceagentLocator().getServiceName());
        assertTrue(service != null);
    }

    public void test1SubscriptionCenterServiceGetSubscriptionStatus() throws Exception {
        com.heb.xmlns.ei.SubscriptionCenterService.SubscriptionCenterServiceBindingStub binding;
        try {
            binding = (com.heb.xmlns.ei.SubscriptionCenterService.SubscriptionCenterServiceBindingStub)
                          new com.heb.xmlns.ei.SubscriptionCenterService.SubscriptionCenterServiceServiceagentLocator().getSubscriptionCenterService();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.heb.xmlns.ei.SubscriptionCenterService.GetSubscriptionStatus_Reply.GetSubscriptionStatus_Reply value = null;
            value = binding.getSubscriptionStatus(new com.heb.xmlns.ei.SubscriptionCenterService.GetSubscriptionStatus_Request.GetSubscriptionStatus_Request());
        }
        catch (com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault e1) {
            throw new junit.framework.AssertionFailedError("fault Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test2SubscriptionCenterServiceSubscribe() throws Exception {
        com.heb.xmlns.ei.SubscriptionCenterService.SubscriptionCenterServiceBindingStub binding;
        try {
            binding = (com.heb.xmlns.ei.SubscriptionCenterService.SubscriptionCenterServiceBindingStub)
                          new com.heb.xmlns.ei.SubscriptionCenterService.SubscriptionCenterServiceServiceagentLocator().getSubscriptionCenterService();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.heb.xmlns.ei.SubscriptionCenterService.Subscribe_Reply.Subscribe_Reply value = null;
            value = binding.subscribe(new com.heb.xmlns.ei.SubscriptionCenterService.Subscribe_Request.Subscribe_Request());
        }
        catch (com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault e1) {
            throw new junit.framework.AssertionFailedError("fault Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test3SubscriptionCenterServiceListSubscriptions() throws Exception {
        com.heb.xmlns.ei.SubscriptionCenterService.SubscriptionCenterServiceBindingStub binding;
        try {
            binding = (com.heb.xmlns.ei.SubscriptionCenterService.SubscriptionCenterServiceBindingStub)
                          new com.heb.xmlns.ei.SubscriptionCenterService.SubscriptionCenterServiceServiceagentLocator().getSubscriptionCenterService();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.heb.xmlns.ei.SubscriptionCenterService.ListSubscriptions_Reply.ListSubscriptions_Reply value = null;
            value = binding.listSubscriptions(new com.heb.xmlns.ei.SubscriptionCenterService.ListSubscriptions_Request.ListSubscriptions_Request());
        }
        catch (com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault e1) {
            throw new junit.framework.AssertionFailedError("fault Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test4SubscriptionCenterServiceVerifyMobileDirectoryNumber() throws Exception {
        com.heb.xmlns.ei.SubscriptionCenterService.SubscriptionCenterServiceBindingStub binding;
        try {
            binding = (com.heb.xmlns.ei.SubscriptionCenterService.SubscriptionCenterServiceBindingStub)
                          new com.heb.xmlns.ei.SubscriptionCenterService.SubscriptionCenterServiceServiceagentLocator().getSubscriptionCenterService();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.heb.xmlns.ei.SubscriptionCenterService.VerifyMobileDirectoryNumber_Reply.VerifyMobileDirectoryNumber_Reply value = null;
            value = binding.verifyMobileDirectoryNumber(new com.heb.xmlns.ei.SubscriptionCenterService.VerifyMobileDirectoryNumber_Request.VerifyMobileDirectoryNumber_Request());
        }
        catch (com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault e1) {
            throw new junit.framework.AssertionFailedError("fault Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test5SubscriptionCenterServiceModifySubscriptions() throws Exception {
        com.heb.xmlns.ei.SubscriptionCenterService.SubscriptionCenterServiceBindingStub binding;
        try {
            binding = (com.heb.xmlns.ei.SubscriptionCenterService.SubscriptionCenterServiceBindingStub)
                          new com.heb.xmlns.ei.SubscriptionCenterService.SubscriptionCenterServiceServiceagentLocator().getSubscriptionCenterService();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.heb.xmlns.ei.SubscriptionCenterService.ModifySubscriptions_Reply.ModifySubscriptions_Reply value = null;
            value = binding.modifySubscriptions(new com.heb.xmlns.ei.SubscriptionCenterService.ModifySubscriptions_Request.ModifySubscriptions_Request());
        }
        catch (com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault e1) {
            throw new junit.framework.AssertionFailedError("fault Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test6SubscriptionCenterServiceUnsubscribe() throws Exception {
        com.heb.xmlns.ei.SubscriptionCenterService.SubscriptionCenterServiceBindingStub binding;
        try {
            binding = (com.heb.xmlns.ei.SubscriptionCenterService.SubscriptionCenterServiceBindingStub)
                          new com.heb.xmlns.ei.SubscriptionCenterService.SubscriptionCenterServiceServiceagentLocator().getSubscriptionCenterService();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.heb.xmlns.ei.SubscriptionCenterService.Unsubscribe_Reply.Unsubscribe_Reply value = null;
            value = binding.unsubscribe(new com.heb.xmlns.ei.SubscriptionCenterService.Unsubscribe_Request.Unsubscribe_Request());
        }
        catch (com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault e1) {
            throw new junit.framework.AssertionFailedError("fault Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test7SubscriptionCenterServiceSendAlert() throws Exception {
        com.heb.xmlns.ei.SubscriptionCenterService.SubscriptionCenterServiceBindingStub binding;
        try {
            binding = (com.heb.xmlns.ei.SubscriptionCenterService.SubscriptionCenterServiceBindingStub)
                          new com.heb.xmlns.ei.SubscriptionCenterService.SubscriptionCenterServiceServiceagentLocator().getSubscriptionCenterService();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.heb.xmlns.ei.Salt3.SendMessageResponse value = null;
            value = binding.sendAlert(new com.heb.xmlns.ei.Salt3.SendMessageRequest());
        }
        catch (com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault e1) {
            throw new junit.framework.AssertionFailedError("fault1 Exception caught: " + e1);
        }
            // TBD - validate results
    }

}
