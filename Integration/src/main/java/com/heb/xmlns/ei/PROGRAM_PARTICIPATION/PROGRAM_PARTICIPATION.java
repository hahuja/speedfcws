/**
 * PROGRAM_PARTICIPATION.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.xmlns.ei.PROGRAM_PARTICIPATION;

public class PROGRAM_PARTICIPATION  implements java.io.Serializable {
    private java.math.BigInteger PARTIC_ID;

    private java.math.BigInteger PART_TYPE_CD;

    private java.lang.String STATUS;

    private java.lang.String START_DATE;

    private java.lang.String END_DATE;

    private com.heb.xmlns.ei.PROGRAM_PARTICIPATION.PROGRAM_PARTICIPATIONPARTICIPATION_TYPE[] PARTICIPATION_TYPE;

    public PROGRAM_PARTICIPATION() {
    }

    public PROGRAM_PARTICIPATION(
           java.math.BigInteger PARTIC_ID,
           java.math.BigInteger PART_TYPE_CD,
           java.lang.String STATUS,
           java.lang.String START_DATE,
           java.lang.String END_DATE,
           com.heb.xmlns.ei.PROGRAM_PARTICIPATION.PROGRAM_PARTICIPATIONPARTICIPATION_TYPE[] PARTICIPATION_TYPE) {
           this.PARTIC_ID = PARTIC_ID;
           this.PART_TYPE_CD = PART_TYPE_CD;
           this.STATUS = STATUS;
           this.START_DATE = START_DATE;
           this.END_DATE = END_DATE;
           this.PARTICIPATION_TYPE = PARTICIPATION_TYPE;
    }


    /**
     * Gets the PARTIC_ID value for this PROGRAM_PARTICIPATION.
     * 
     * @return PARTIC_ID
     */
    public java.math.BigInteger getPARTIC_ID() {
        return PARTIC_ID;
    }


    /**
     * Sets the PARTIC_ID value for this PROGRAM_PARTICIPATION.
     * 
     * @param PARTIC_ID
     */
    public void setPARTIC_ID(java.math.BigInteger PARTIC_ID) {
        this.PARTIC_ID = PARTIC_ID;
    }


    /**
     * Gets the PART_TYPE_CD value for this PROGRAM_PARTICIPATION.
     * 
     * @return PART_TYPE_CD
     */
    public java.math.BigInteger getPART_TYPE_CD() {
        return PART_TYPE_CD;
    }


    /**
     * Sets the PART_TYPE_CD value for this PROGRAM_PARTICIPATION.
     * 
     * @param PART_TYPE_CD
     */
    public void setPART_TYPE_CD(java.math.BigInteger PART_TYPE_CD) {
        this.PART_TYPE_CD = PART_TYPE_CD;
    }


    /**
     * Gets the STATUS value for this PROGRAM_PARTICIPATION.
     * 
     * @return STATUS
     */
    public java.lang.String getSTATUS() {
        return STATUS;
    }


    /**
     * Sets the STATUS value for this PROGRAM_PARTICIPATION.
     * 
     * @param STATUS
     */
    public void setSTATUS(java.lang.String STATUS) {
        this.STATUS = STATUS;
    }


    /**
     * Gets the START_DATE value for this PROGRAM_PARTICIPATION.
     * 
     * @return START_DATE
     */
    public java.lang.String getSTART_DATE() {
        return START_DATE;
    }


    /**
     * Sets the START_DATE value for this PROGRAM_PARTICIPATION.
     * 
     * @param START_DATE
     */
    public void setSTART_DATE(java.lang.String START_DATE) {
        this.START_DATE = START_DATE;
    }


    /**
     * Gets the END_DATE value for this PROGRAM_PARTICIPATION.
     * 
     * @return END_DATE
     */
    public java.lang.String getEND_DATE() {
        return END_DATE;
    }


    /**
     * Sets the END_DATE value for this PROGRAM_PARTICIPATION.
     * 
     * @param END_DATE
     */
    public void setEND_DATE(java.lang.String END_DATE) {
        this.END_DATE = END_DATE;
    }


    /**
     * Gets the PARTICIPATION_TYPE value for this PROGRAM_PARTICIPATION.
     * 
     * @return PARTICIPATION_TYPE
     */
    public com.heb.xmlns.ei.PROGRAM_PARTICIPATION.PROGRAM_PARTICIPATIONPARTICIPATION_TYPE[] getPARTICIPATION_TYPE() {
        return PARTICIPATION_TYPE;
    }


    /**
     * Sets the PARTICIPATION_TYPE value for this PROGRAM_PARTICIPATION.
     * 
     * @param PARTICIPATION_TYPE
     */
    public void setPARTICIPATION_TYPE(com.heb.xmlns.ei.PROGRAM_PARTICIPATION.PROGRAM_PARTICIPATIONPARTICIPATION_TYPE[] PARTICIPATION_TYPE) {
        this.PARTICIPATION_TYPE = PARTICIPATION_TYPE;
    }

    public com.heb.xmlns.ei.PROGRAM_PARTICIPATION.PROGRAM_PARTICIPATIONPARTICIPATION_TYPE getPARTICIPATION_TYPE(int i) {
        return this.PARTICIPATION_TYPE[i];
    }

    public void setPARTICIPATION_TYPE(int i, com.heb.xmlns.ei.PROGRAM_PARTICIPATION.PROGRAM_PARTICIPATIONPARTICIPATION_TYPE _value) {
        this.PARTICIPATION_TYPE[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PROGRAM_PARTICIPATION)) return false;
        PROGRAM_PARTICIPATION other = (PROGRAM_PARTICIPATION) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.PARTIC_ID==null && other.getPARTIC_ID()==null) || 
             (this.PARTIC_ID!=null &&
              this.PARTIC_ID.equals(other.getPARTIC_ID()))) &&
            ((this.PART_TYPE_CD==null && other.getPART_TYPE_CD()==null) || 
             (this.PART_TYPE_CD!=null &&
              this.PART_TYPE_CD.equals(other.getPART_TYPE_CD()))) &&
            ((this.STATUS==null && other.getSTATUS()==null) || 
             (this.STATUS!=null &&
              this.STATUS.equals(other.getSTATUS()))) &&
            ((this.START_DATE==null && other.getSTART_DATE()==null) || 
             (this.START_DATE!=null &&
              this.START_DATE.equals(other.getSTART_DATE()))) &&
            ((this.END_DATE==null && other.getEND_DATE()==null) || 
             (this.END_DATE!=null &&
              this.END_DATE.equals(other.getEND_DATE()))) &&
            ((this.PARTICIPATION_TYPE==null && other.getPARTICIPATION_TYPE()==null) || 
             (this.PARTICIPATION_TYPE!=null &&
              java.util.Arrays.equals(this.PARTICIPATION_TYPE, other.getPARTICIPATION_TYPE())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPARTIC_ID() != null) {
            _hashCode += getPARTIC_ID().hashCode();
        }
        if (getPART_TYPE_CD() != null) {
            _hashCode += getPART_TYPE_CD().hashCode();
        }
        if (getSTATUS() != null) {
            _hashCode += getSTATUS().hashCode();
        }
        if (getSTART_DATE() != null) {
            _hashCode += getSTART_DATE().hashCode();
        }
        if (getEND_DATE() != null) {
            _hashCode += getEND_DATE().hashCode();
        }
        if (getPARTICIPATION_TYPE() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPARTICIPATION_TYPE());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPARTICIPATION_TYPE(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PROGRAM_PARTICIPATION.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PROGRAM_PARTICIPATION", ">PROGRAM_PARTICIPATION"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PARTIC_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PROGRAM_PARTICIPATION", "PARTIC_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PART_TYPE_CD");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PROGRAM_PARTICIPATION", "PART_TYPE_CD"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("STATUS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PROGRAM_PARTICIPATION", "STATUS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("START_DATE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PROGRAM_PARTICIPATION", "START_DATE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("END_DATE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PROGRAM_PARTICIPATION", "END_DATE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PARTICIPATION_TYPE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PROGRAM_PARTICIPATION", "PARTICIPATION_TYPE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PROGRAM_PARTICIPATION", ">>PROGRAM_PARTICIPATION>PARTICIPATION_TYPE"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
