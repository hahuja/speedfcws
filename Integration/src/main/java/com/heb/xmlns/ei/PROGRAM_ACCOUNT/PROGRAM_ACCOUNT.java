/**
 * PROGRAM_ACCOUNT.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.xmlns.ei.PROGRAM_ACCOUNT;

public class PROGRAM_ACCOUNT  implements java.io.Serializable {
    private java.lang.String PROGRAM_ACCOUNT_ID;

    private java.lang.String ACCOUNT_TYPE_CD;

    private java.math.BigInteger CUSTOMER_ID;

    private java.math.BigInteger PRIMARY_PROGRAM_ACCOUNT_ID;

    private java.lang.String PROGRAM_ACCOUNT_ALPHA_ID;

    private java.lang.String ENROLLMENT_DATE;

    private java.lang.String PROGRAM_ACCOUNT_STATUS_CD;

    private java.lang.String PROGRAM_ACCOUNT_STATUS_REASON_CD;

    private java.lang.String EMPLOYEE_ID;

    private java.lang.String CREATE_TIMESTAMP;

    private java.lang.String LAST_UPDATE_DATE;

    private java.lang.String LAST_UPDATE_USER_ID;

    private java.math.BigInteger EXTERNAL_ID;

    private java.lang.String AMS_HOUSEHOLD_ID;

    private java.lang.String AMS_CUSTOMER_ID;

    private java.lang.String AMS_ALTERNATE_ID;

    private java.lang.String AMS_ALTERNATE_ID_VERFICATION_TEXT;

    private java.math.BigInteger AMS_BANNER_CD;

    private java.math.BigInteger HEBDOTCOM_ID;

    private com.heb.xmlns.ei.CARD_PRINT_STAGING.CARD_PRINT_STAGING CARD_PRINT_STAGING;

    public PROGRAM_ACCOUNT() {
    }

    public PROGRAM_ACCOUNT(
           java.lang.String PROGRAM_ACCOUNT_ID,
           java.lang.String ACCOUNT_TYPE_CD,
           java.math.BigInteger CUSTOMER_ID,
           java.math.BigInteger PRIMARY_PROGRAM_ACCOUNT_ID,
           java.lang.String PROGRAM_ACCOUNT_ALPHA_ID,
           java.lang.String ENROLLMENT_DATE,
           java.lang.String PROGRAM_ACCOUNT_STATUS_CD,
           java.lang.String PROGRAM_ACCOUNT_STATUS_REASON_CD,
           java.lang.String EMPLOYEE_ID,
           java.lang.String CREATE_TIMESTAMP,
           java.lang.String LAST_UPDATE_DATE,
           java.lang.String LAST_UPDATE_USER_ID,
           java.math.BigInteger EXTERNAL_ID,
           java.lang.String AMS_HOUSEHOLD_ID,
           java.lang.String AMS_CUSTOMER_ID,
           java.lang.String AMS_ALTERNATE_ID,
           java.lang.String AMS_ALTERNATE_ID_VERFICATION_TEXT,
           java.math.BigInteger AMS_BANNER_CD,
           java.math.BigInteger HEBDOTCOM_ID,
           com.heb.xmlns.ei.CARD_PRINT_STAGING.CARD_PRINT_STAGING CARD_PRINT_STAGING) {
           this.PROGRAM_ACCOUNT_ID = PROGRAM_ACCOUNT_ID;
           this.ACCOUNT_TYPE_CD = ACCOUNT_TYPE_CD;
           this.CUSTOMER_ID = CUSTOMER_ID;
           this.PRIMARY_PROGRAM_ACCOUNT_ID = PRIMARY_PROGRAM_ACCOUNT_ID;
           this.PROGRAM_ACCOUNT_ALPHA_ID = PROGRAM_ACCOUNT_ALPHA_ID;
           this.ENROLLMENT_DATE = ENROLLMENT_DATE;
           this.PROGRAM_ACCOUNT_STATUS_CD = PROGRAM_ACCOUNT_STATUS_CD;
           this.PROGRAM_ACCOUNT_STATUS_REASON_CD = PROGRAM_ACCOUNT_STATUS_REASON_CD;
           this.EMPLOYEE_ID = EMPLOYEE_ID;
           this.CREATE_TIMESTAMP = CREATE_TIMESTAMP;
           this.LAST_UPDATE_DATE = LAST_UPDATE_DATE;
           this.LAST_UPDATE_USER_ID = LAST_UPDATE_USER_ID;
           this.EXTERNAL_ID = EXTERNAL_ID;
           this.AMS_HOUSEHOLD_ID = AMS_HOUSEHOLD_ID;
           this.AMS_CUSTOMER_ID = AMS_CUSTOMER_ID;
           this.AMS_ALTERNATE_ID = AMS_ALTERNATE_ID;
           this.AMS_ALTERNATE_ID_VERFICATION_TEXT = AMS_ALTERNATE_ID_VERFICATION_TEXT;
           this.AMS_BANNER_CD = AMS_BANNER_CD;
           this.HEBDOTCOM_ID = HEBDOTCOM_ID;
           this.CARD_PRINT_STAGING = CARD_PRINT_STAGING;
    }


    /**
     * Gets the PROGRAM_ACCOUNT_ID value for this PROGRAM_ACCOUNT.
     * 
     * @return PROGRAM_ACCOUNT_ID
     */
    public java.lang.String getPROGRAM_ACCOUNT_ID() {
        return PROGRAM_ACCOUNT_ID;
    }


    /**
     * Sets the PROGRAM_ACCOUNT_ID value for this PROGRAM_ACCOUNT.
     * 
     * @param PROGRAM_ACCOUNT_ID
     */
    public void setPROGRAM_ACCOUNT_ID(java.lang.String PROGRAM_ACCOUNT_ID) {
        this.PROGRAM_ACCOUNT_ID = PROGRAM_ACCOUNT_ID;
    }


    /**
     * Gets the ACCOUNT_TYPE_CD value for this PROGRAM_ACCOUNT.
     * 
     * @return ACCOUNT_TYPE_CD
     */
    public java.lang.String getACCOUNT_TYPE_CD() {
        return ACCOUNT_TYPE_CD;
    }


    /**
     * Sets the ACCOUNT_TYPE_CD value for this PROGRAM_ACCOUNT.
     * 
     * @param ACCOUNT_TYPE_CD
     */
    public void setACCOUNT_TYPE_CD(java.lang.String ACCOUNT_TYPE_CD) {
        this.ACCOUNT_TYPE_CD = ACCOUNT_TYPE_CD;
    }


    /**
     * Gets the CUSTOMER_ID value for this PROGRAM_ACCOUNT.
     * 
     * @return CUSTOMER_ID
     */
    public java.math.BigInteger getCUSTOMER_ID() {
        return CUSTOMER_ID;
    }


    /**
     * Sets the CUSTOMER_ID value for this PROGRAM_ACCOUNT.
     * 
     * @param CUSTOMER_ID
     */
    public void setCUSTOMER_ID(java.math.BigInteger CUSTOMER_ID) {
        this.CUSTOMER_ID = CUSTOMER_ID;
    }


    /**
     * Gets the PRIMARY_PROGRAM_ACCOUNT_ID value for this PROGRAM_ACCOUNT.
     * 
     * @return PRIMARY_PROGRAM_ACCOUNT_ID
     */
    public java.math.BigInteger getPRIMARY_PROGRAM_ACCOUNT_ID() {
        return PRIMARY_PROGRAM_ACCOUNT_ID;
    }


    /**
     * Sets the PRIMARY_PROGRAM_ACCOUNT_ID value for this PROGRAM_ACCOUNT.
     * 
     * @param PRIMARY_PROGRAM_ACCOUNT_ID
     */
    public void setPRIMARY_PROGRAM_ACCOUNT_ID(java.math.BigInteger PRIMARY_PROGRAM_ACCOUNT_ID) {
        this.PRIMARY_PROGRAM_ACCOUNT_ID = PRIMARY_PROGRAM_ACCOUNT_ID;
    }


    /**
     * Gets the PROGRAM_ACCOUNT_ALPHA_ID value for this PROGRAM_ACCOUNT.
     * 
     * @return PROGRAM_ACCOUNT_ALPHA_ID
     */
    public java.lang.String getPROGRAM_ACCOUNT_ALPHA_ID() {
        return PROGRAM_ACCOUNT_ALPHA_ID;
    }


    /**
     * Sets the PROGRAM_ACCOUNT_ALPHA_ID value for this PROGRAM_ACCOUNT.
     * 
     * @param PROGRAM_ACCOUNT_ALPHA_ID
     */
    public void setPROGRAM_ACCOUNT_ALPHA_ID(java.lang.String PROGRAM_ACCOUNT_ALPHA_ID) {
        this.PROGRAM_ACCOUNT_ALPHA_ID = PROGRAM_ACCOUNT_ALPHA_ID;
    }


    /**
     * Gets the ENROLLMENT_DATE value for this PROGRAM_ACCOUNT.
     * 
     * @return ENROLLMENT_DATE
     */
    public java.lang.String getENROLLMENT_DATE() {
        return ENROLLMENT_DATE;
    }


    /**
     * Sets the ENROLLMENT_DATE value for this PROGRAM_ACCOUNT.
     * 
     * @param ENROLLMENT_DATE
     */
    public void setENROLLMENT_DATE(java.lang.String ENROLLMENT_DATE) {
        this.ENROLLMENT_DATE = ENROLLMENT_DATE;
    }


    /**
     * Gets the PROGRAM_ACCOUNT_STATUS_CD value for this PROGRAM_ACCOUNT.
     * 
     * @return PROGRAM_ACCOUNT_STATUS_CD
     */
    public java.lang.String getPROGRAM_ACCOUNT_STATUS_CD() {
        return PROGRAM_ACCOUNT_STATUS_CD;
    }


    /**
     * Sets the PROGRAM_ACCOUNT_STATUS_CD value for this PROGRAM_ACCOUNT.
     * 
     * @param PROGRAM_ACCOUNT_STATUS_CD
     */
    public void setPROGRAM_ACCOUNT_STATUS_CD(java.lang.String PROGRAM_ACCOUNT_STATUS_CD) {
        this.PROGRAM_ACCOUNT_STATUS_CD = PROGRAM_ACCOUNT_STATUS_CD;
    }


    /**
     * Gets the PROGRAM_ACCOUNT_STATUS_REASON_CD value for this PROGRAM_ACCOUNT.
     * 
     * @return PROGRAM_ACCOUNT_STATUS_REASON_CD
     */
    public java.lang.String getPROGRAM_ACCOUNT_STATUS_REASON_CD() {
        return PROGRAM_ACCOUNT_STATUS_REASON_CD;
    }


    /**
     * Sets the PROGRAM_ACCOUNT_STATUS_REASON_CD value for this PROGRAM_ACCOUNT.
     * 
     * @param PROGRAM_ACCOUNT_STATUS_REASON_CD
     */
    public void setPROGRAM_ACCOUNT_STATUS_REASON_CD(java.lang.String PROGRAM_ACCOUNT_STATUS_REASON_CD) {
        this.PROGRAM_ACCOUNT_STATUS_REASON_CD = PROGRAM_ACCOUNT_STATUS_REASON_CD;
    }


    /**
     * Gets the EMPLOYEE_ID value for this PROGRAM_ACCOUNT.
     * 
     * @return EMPLOYEE_ID
     */
    public java.lang.String getEMPLOYEE_ID() {
        return EMPLOYEE_ID;
    }


    /**
     * Sets the EMPLOYEE_ID value for this PROGRAM_ACCOUNT.
     * 
     * @param EMPLOYEE_ID
     */
    public void setEMPLOYEE_ID(java.lang.String EMPLOYEE_ID) {
        this.EMPLOYEE_ID = EMPLOYEE_ID;
    }


    /**
     * Gets the CREATE_TIMESTAMP value for this PROGRAM_ACCOUNT.
     * 
     * @return CREATE_TIMESTAMP
     */
    public java.lang.String getCREATE_TIMESTAMP() {
        return CREATE_TIMESTAMP;
    }


    /**
     * Sets the CREATE_TIMESTAMP value for this PROGRAM_ACCOUNT.
     * 
     * @param CREATE_TIMESTAMP
     */
    public void setCREATE_TIMESTAMP(java.lang.String CREATE_TIMESTAMP) {
        this.CREATE_TIMESTAMP = CREATE_TIMESTAMP;
    }


    /**
     * Gets the LAST_UPDATE_DATE value for this PROGRAM_ACCOUNT.
     * 
     * @return LAST_UPDATE_DATE
     */
    public java.lang.String getLAST_UPDATE_DATE() {
        return LAST_UPDATE_DATE;
    }


    /**
     * Sets the LAST_UPDATE_DATE value for this PROGRAM_ACCOUNT.
     * 
     * @param LAST_UPDATE_DATE
     */
    public void setLAST_UPDATE_DATE(java.lang.String LAST_UPDATE_DATE) {
        this.LAST_UPDATE_DATE = LAST_UPDATE_DATE;
    }


    /**
     * Gets the LAST_UPDATE_USER_ID value for this PROGRAM_ACCOUNT.
     * 
     * @return LAST_UPDATE_USER_ID
     */
    public java.lang.String getLAST_UPDATE_USER_ID() {
        return LAST_UPDATE_USER_ID;
    }


    /**
     * Sets the LAST_UPDATE_USER_ID value for this PROGRAM_ACCOUNT.
     * 
     * @param LAST_UPDATE_USER_ID
     */
    public void setLAST_UPDATE_USER_ID(java.lang.String LAST_UPDATE_USER_ID) {
        this.LAST_UPDATE_USER_ID = LAST_UPDATE_USER_ID;
    }


    /**
     * Gets the EXTERNAL_ID value for this PROGRAM_ACCOUNT.
     * 
     * @return EXTERNAL_ID
     */
    public java.math.BigInteger getEXTERNAL_ID() {
        return EXTERNAL_ID;
    }


    /**
     * Sets the EXTERNAL_ID value for this PROGRAM_ACCOUNT.
     * 
     * @param EXTERNAL_ID
     */
    public void setEXTERNAL_ID(java.math.BigInteger EXTERNAL_ID) {
        this.EXTERNAL_ID = EXTERNAL_ID;
    }


    /**
     * Gets the AMS_HOUSEHOLD_ID value for this PROGRAM_ACCOUNT.
     * 
     * @return AMS_HOUSEHOLD_ID
     */
    public java.lang.String getAMS_HOUSEHOLD_ID() {
        return AMS_HOUSEHOLD_ID;
    }


    /**
     * Sets the AMS_HOUSEHOLD_ID value for this PROGRAM_ACCOUNT.
     * 
     * @param AMS_HOUSEHOLD_ID
     */
    public void setAMS_HOUSEHOLD_ID(java.lang.String AMS_HOUSEHOLD_ID) {
        this.AMS_HOUSEHOLD_ID = AMS_HOUSEHOLD_ID;
    }


    /**
     * Gets the AMS_CUSTOMER_ID value for this PROGRAM_ACCOUNT.
     * 
     * @return AMS_CUSTOMER_ID
     */
    public java.lang.String getAMS_CUSTOMER_ID() {
        return AMS_CUSTOMER_ID;
    }


    /**
     * Sets the AMS_CUSTOMER_ID value for this PROGRAM_ACCOUNT.
     * 
     * @param AMS_CUSTOMER_ID
     */
    public void setAMS_CUSTOMER_ID(java.lang.String AMS_CUSTOMER_ID) {
        this.AMS_CUSTOMER_ID = AMS_CUSTOMER_ID;
    }


    /**
     * Gets the AMS_ALTERNATE_ID value for this PROGRAM_ACCOUNT.
     * 
     * @return AMS_ALTERNATE_ID
     */
    public java.lang.String getAMS_ALTERNATE_ID() {
        return AMS_ALTERNATE_ID;
    }


    /**
     * Sets the AMS_ALTERNATE_ID value for this PROGRAM_ACCOUNT.
     * 
     * @param AMS_ALTERNATE_ID
     */
    public void setAMS_ALTERNATE_ID(java.lang.String AMS_ALTERNATE_ID) {
        this.AMS_ALTERNATE_ID = AMS_ALTERNATE_ID;
    }


    /**
     * Gets the AMS_ALTERNATE_ID_VERFICATION_TEXT value for this PROGRAM_ACCOUNT.
     * 
     * @return AMS_ALTERNATE_ID_VERFICATION_TEXT
     */
    public java.lang.String getAMS_ALTERNATE_ID_VERFICATION_TEXT() {
        return AMS_ALTERNATE_ID_VERFICATION_TEXT;
    }


    /**
     * Sets the AMS_ALTERNATE_ID_VERFICATION_TEXT value for this PROGRAM_ACCOUNT.
     * 
     * @param AMS_ALTERNATE_ID_VERFICATION_TEXT
     */
    public void setAMS_ALTERNATE_ID_VERFICATION_TEXT(java.lang.String AMS_ALTERNATE_ID_VERFICATION_TEXT) {
        this.AMS_ALTERNATE_ID_VERFICATION_TEXT = AMS_ALTERNATE_ID_VERFICATION_TEXT;
    }


    /**
     * Gets the AMS_BANNER_CD value for this PROGRAM_ACCOUNT.
     * 
     * @return AMS_BANNER_CD
     */
    public java.math.BigInteger getAMS_BANNER_CD() {
        return AMS_BANNER_CD;
    }


    /**
     * Sets the AMS_BANNER_CD value for this PROGRAM_ACCOUNT.
     * 
     * @param AMS_BANNER_CD
     */
    public void setAMS_BANNER_CD(java.math.BigInteger AMS_BANNER_CD) {
        this.AMS_BANNER_CD = AMS_BANNER_CD;
    }


    /**
     * Gets the HEBDOTCOM_ID value for this PROGRAM_ACCOUNT.
     * 
     * @return HEBDOTCOM_ID
     */
    public java.math.BigInteger getHEBDOTCOM_ID() {
        return HEBDOTCOM_ID;
    }


    /**
     * Sets the HEBDOTCOM_ID value for this PROGRAM_ACCOUNT.
     * 
     * @param HEBDOTCOM_ID
     */
    public void setHEBDOTCOM_ID(java.math.BigInteger HEBDOTCOM_ID) {
        this.HEBDOTCOM_ID = HEBDOTCOM_ID;
    }


    /**
     * Gets the CARD_PRINT_STAGING value for this PROGRAM_ACCOUNT.
     * 
     * @return CARD_PRINT_STAGING
     */
    public com.heb.xmlns.ei.CARD_PRINT_STAGING.CARD_PRINT_STAGING getCARD_PRINT_STAGING() {
        return CARD_PRINT_STAGING;
    }


    /**
     * Sets the CARD_PRINT_STAGING value for this PROGRAM_ACCOUNT.
     * 
     * @param CARD_PRINT_STAGING
     */
    public void setCARD_PRINT_STAGING(com.heb.xmlns.ei.CARD_PRINT_STAGING.CARD_PRINT_STAGING CARD_PRINT_STAGING) {
        this.CARD_PRINT_STAGING = CARD_PRINT_STAGING;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PROGRAM_ACCOUNT)) return false;
        PROGRAM_ACCOUNT other = (PROGRAM_ACCOUNT) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.PROGRAM_ACCOUNT_ID==null && other.getPROGRAM_ACCOUNT_ID()==null) || 
             (this.PROGRAM_ACCOUNT_ID!=null &&
              this.PROGRAM_ACCOUNT_ID.equals(other.getPROGRAM_ACCOUNT_ID()))) &&
            ((this.ACCOUNT_TYPE_CD==null && other.getACCOUNT_TYPE_CD()==null) || 
             (this.ACCOUNT_TYPE_CD!=null &&
              this.ACCOUNT_TYPE_CD.equals(other.getACCOUNT_TYPE_CD()))) &&
            ((this.CUSTOMER_ID==null && other.getCUSTOMER_ID()==null) || 
             (this.CUSTOMER_ID!=null &&
              this.CUSTOMER_ID.equals(other.getCUSTOMER_ID()))) &&
            ((this.PRIMARY_PROGRAM_ACCOUNT_ID==null && other.getPRIMARY_PROGRAM_ACCOUNT_ID()==null) || 
             (this.PRIMARY_PROGRAM_ACCOUNT_ID!=null &&
              this.PRIMARY_PROGRAM_ACCOUNT_ID.equals(other.getPRIMARY_PROGRAM_ACCOUNT_ID()))) &&
            ((this.PROGRAM_ACCOUNT_ALPHA_ID==null && other.getPROGRAM_ACCOUNT_ALPHA_ID()==null) || 
             (this.PROGRAM_ACCOUNT_ALPHA_ID!=null &&
              this.PROGRAM_ACCOUNT_ALPHA_ID.equals(other.getPROGRAM_ACCOUNT_ALPHA_ID()))) &&
            ((this.ENROLLMENT_DATE==null && other.getENROLLMENT_DATE()==null) || 
             (this.ENROLLMENT_DATE!=null &&
              this.ENROLLMENT_DATE.equals(other.getENROLLMENT_DATE()))) &&
            ((this.PROGRAM_ACCOUNT_STATUS_CD==null && other.getPROGRAM_ACCOUNT_STATUS_CD()==null) || 
             (this.PROGRAM_ACCOUNT_STATUS_CD!=null &&
              this.PROGRAM_ACCOUNT_STATUS_CD.equals(other.getPROGRAM_ACCOUNT_STATUS_CD()))) &&
            ((this.PROGRAM_ACCOUNT_STATUS_REASON_CD==null && other.getPROGRAM_ACCOUNT_STATUS_REASON_CD()==null) || 
             (this.PROGRAM_ACCOUNT_STATUS_REASON_CD!=null &&
              this.PROGRAM_ACCOUNT_STATUS_REASON_CD.equals(other.getPROGRAM_ACCOUNT_STATUS_REASON_CD()))) &&
            ((this.EMPLOYEE_ID==null && other.getEMPLOYEE_ID()==null) || 
             (this.EMPLOYEE_ID!=null &&
              this.EMPLOYEE_ID.equals(other.getEMPLOYEE_ID()))) &&
            ((this.CREATE_TIMESTAMP==null && other.getCREATE_TIMESTAMP()==null) || 
             (this.CREATE_TIMESTAMP!=null &&
              this.CREATE_TIMESTAMP.equals(other.getCREATE_TIMESTAMP()))) &&
            ((this.LAST_UPDATE_DATE==null && other.getLAST_UPDATE_DATE()==null) || 
             (this.LAST_UPDATE_DATE!=null &&
              this.LAST_UPDATE_DATE.equals(other.getLAST_UPDATE_DATE()))) &&
            ((this.LAST_UPDATE_USER_ID==null && other.getLAST_UPDATE_USER_ID()==null) || 
             (this.LAST_UPDATE_USER_ID!=null &&
              this.LAST_UPDATE_USER_ID.equals(other.getLAST_UPDATE_USER_ID()))) &&
            ((this.EXTERNAL_ID==null && other.getEXTERNAL_ID()==null) || 
             (this.EXTERNAL_ID!=null &&
              this.EXTERNAL_ID.equals(other.getEXTERNAL_ID()))) &&
            ((this.AMS_HOUSEHOLD_ID==null && other.getAMS_HOUSEHOLD_ID()==null) || 
             (this.AMS_HOUSEHOLD_ID!=null &&
              this.AMS_HOUSEHOLD_ID.equals(other.getAMS_HOUSEHOLD_ID()))) &&
            ((this.AMS_CUSTOMER_ID==null && other.getAMS_CUSTOMER_ID()==null) || 
             (this.AMS_CUSTOMER_ID!=null &&
              this.AMS_CUSTOMER_ID.equals(other.getAMS_CUSTOMER_ID()))) &&
            ((this.AMS_ALTERNATE_ID==null && other.getAMS_ALTERNATE_ID()==null) || 
             (this.AMS_ALTERNATE_ID!=null &&
              this.AMS_ALTERNATE_ID.equals(other.getAMS_ALTERNATE_ID()))) &&
            ((this.AMS_ALTERNATE_ID_VERFICATION_TEXT==null && other.getAMS_ALTERNATE_ID_VERFICATION_TEXT()==null) || 
             (this.AMS_ALTERNATE_ID_VERFICATION_TEXT!=null &&
              this.AMS_ALTERNATE_ID_VERFICATION_TEXT.equals(other.getAMS_ALTERNATE_ID_VERFICATION_TEXT()))) &&
            ((this.AMS_BANNER_CD==null && other.getAMS_BANNER_CD()==null) || 
             (this.AMS_BANNER_CD!=null &&
              this.AMS_BANNER_CD.equals(other.getAMS_BANNER_CD()))) &&
            ((this.HEBDOTCOM_ID==null && other.getHEBDOTCOM_ID()==null) || 
             (this.HEBDOTCOM_ID!=null &&
              this.HEBDOTCOM_ID.equals(other.getHEBDOTCOM_ID()))) &&
            ((this.CARD_PRINT_STAGING==null && other.getCARD_PRINT_STAGING()==null) || 
             (this.CARD_PRINT_STAGING!=null &&
              this.CARD_PRINT_STAGING.equals(other.getCARD_PRINT_STAGING())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPROGRAM_ACCOUNT_ID() != null) {
            _hashCode += getPROGRAM_ACCOUNT_ID().hashCode();
        }
        if (getACCOUNT_TYPE_CD() != null) {
            _hashCode += getACCOUNT_TYPE_CD().hashCode();
        }
        if (getCUSTOMER_ID() != null) {
            _hashCode += getCUSTOMER_ID().hashCode();
        }
        if (getPRIMARY_PROGRAM_ACCOUNT_ID() != null) {
            _hashCode += getPRIMARY_PROGRAM_ACCOUNT_ID().hashCode();
        }
        if (getPROGRAM_ACCOUNT_ALPHA_ID() != null) {
            _hashCode += getPROGRAM_ACCOUNT_ALPHA_ID().hashCode();
        }
        if (getENROLLMENT_DATE() != null) {
            _hashCode += getENROLLMENT_DATE().hashCode();
        }
        if (getPROGRAM_ACCOUNT_STATUS_CD() != null) {
            _hashCode += getPROGRAM_ACCOUNT_STATUS_CD().hashCode();
        }
        if (getPROGRAM_ACCOUNT_STATUS_REASON_CD() != null) {
            _hashCode += getPROGRAM_ACCOUNT_STATUS_REASON_CD().hashCode();
        }
        if (getEMPLOYEE_ID() != null) {
            _hashCode += getEMPLOYEE_ID().hashCode();
        }
        if (getCREATE_TIMESTAMP() != null) {
            _hashCode += getCREATE_TIMESTAMP().hashCode();
        }
        if (getLAST_UPDATE_DATE() != null) {
            _hashCode += getLAST_UPDATE_DATE().hashCode();
        }
        if (getLAST_UPDATE_USER_ID() != null) {
            _hashCode += getLAST_UPDATE_USER_ID().hashCode();
        }
        if (getEXTERNAL_ID() != null) {
            _hashCode += getEXTERNAL_ID().hashCode();
        }
        if (getAMS_HOUSEHOLD_ID() != null) {
            _hashCode += getAMS_HOUSEHOLD_ID().hashCode();
        }
        if (getAMS_CUSTOMER_ID() != null) {
            _hashCode += getAMS_CUSTOMER_ID().hashCode();
        }
        if (getAMS_ALTERNATE_ID() != null) {
            _hashCode += getAMS_ALTERNATE_ID().hashCode();
        }
        if (getAMS_ALTERNATE_ID_VERFICATION_TEXT() != null) {
            _hashCode += getAMS_ALTERNATE_ID_VERFICATION_TEXT().hashCode();
        }
        if (getAMS_BANNER_CD() != null) {
            _hashCode += getAMS_BANNER_CD().hashCode();
        }
        if (getHEBDOTCOM_ID() != null) {
            _hashCode += getHEBDOTCOM_ID().hashCode();
        }
        if (getCARD_PRINT_STAGING() != null) {
            _hashCode += getCARD_PRINT_STAGING().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PROGRAM_ACCOUNT.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PROGRAM_ACCOUNT", ">PROGRAM_ACCOUNT"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PROGRAM_ACCOUNT_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PROGRAM_ACCOUNT", "PROGRAM_ACCOUNT_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ACCOUNT_TYPE_CD");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PROGRAM_ACCOUNT", "ACCOUNT_TYPE_CD"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CUSTOMER_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PROGRAM_ACCOUNT", "CUSTOMER_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PRIMARY_PROGRAM_ACCOUNT_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PROGRAM_ACCOUNT", "PRIMARY_PROGRAM_ACCOUNT_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PROGRAM_ACCOUNT_ALPHA_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PROGRAM_ACCOUNT", "PROGRAM_ACCOUNT_ALPHA_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ENROLLMENT_DATE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PROGRAM_ACCOUNT", "ENROLLMENT_DATE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PROGRAM_ACCOUNT_STATUS_CD");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PROGRAM_ACCOUNT", "PROGRAM_ACCOUNT_STATUS_CD"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PROGRAM_ACCOUNT_STATUS_REASON_CD");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PROGRAM_ACCOUNT", "PROGRAM_ACCOUNT_STATUS_REASON_CD"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("EMPLOYEE_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PROGRAM_ACCOUNT", "EMPLOYEE_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CREATE_TIMESTAMP");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PROGRAM_ACCOUNT", "CREATE_TIMESTAMP"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LAST_UPDATE_DATE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PROGRAM_ACCOUNT", "LAST_UPDATE_DATE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LAST_UPDATE_USER_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PROGRAM_ACCOUNT", "LAST_UPDATE_USER_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("EXTERNAL_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PROGRAM_ACCOUNT", "EXTERNAL_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("AMS_HOUSEHOLD_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PROGRAM_ACCOUNT", "AMS_HOUSEHOLD_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("AMS_CUSTOMER_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PROGRAM_ACCOUNT", "AMS_CUSTOMER_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("AMS_ALTERNATE_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PROGRAM_ACCOUNT", "AMS_ALTERNATE_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("AMS_ALTERNATE_ID_VERFICATION_TEXT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PROGRAM_ACCOUNT", "AMS_ALTERNATE_ID_VERFICATION_TEXT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("AMS_BANNER_CD");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PROGRAM_ACCOUNT", "AMS_BANNER_CD"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("HEBDOTCOM_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PROGRAM_ACCOUNT", "HEBDOTCOM_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CARD_PRINT_STAGING");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CARD_PRINT_STAGING", "CARD_PRINT_STAGING"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CARD_PRINT_STAGING", ">CARD_PRINT_STAGING"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
