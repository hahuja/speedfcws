/**
 * ListSubscriptions_ReplyKeywordsKeyword.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.xmlns.ei.SubscriptionCenterService.ListSubscriptions_Reply;

public class ListSubscriptions_ReplyKeywordsKeyword  implements java.io.Serializable {
    private java.lang.String ID;

    private java.lang.String DESCRIPTION;

    private com.heb.xmlns.ei.SubscriptionCenterService.ListSubscriptions_Reply.AudienceCode AUDIENCE_CODE;

    private com.heb.xmlns.ei.SubscriptionCenterService.ListSubscriptions_Reply.SubscriptionStatusCode SUBSCRIPTION_STATUS_CODE;

    public ListSubscriptions_ReplyKeywordsKeyword() {
    }

    public ListSubscriptions_ReplyKeywordsKeyword(
           java.lang.String ID,
           java.lang.String DESCRIPTION,
           com.heb.xmlns.ei.SubscriptionCenterService.ListSubscriptions_Reply.AudienceCode AUDIENCE_CODE,
           com.heb.xmlns.ei.SubscriptionCenterService.ListSubscriptions_Reply.SubscriptionStatusCode SUBSCRIPTION_STATUS_CODE) {
           this.ID = ID;
           this.DESCRIPTION = DESCRIPTION;
           this.AUDIENCE_CODE = AUDIENCE_CODE;
           this.SUBSCRIPTION_STATUS_CODE = SUBSCRIPTION_STATUS_CODE;
    }


    /**
     * Gets the ID value for this ListSubscriptions_ReplyKeywordsKeyword.
     * 
     * @return ID
     */
    public java.lang.String getID() {
        return ID;
    }


    /**
     * Sets the ID value for this ListSubscriptions_ReplyKeywordsKeyword.
     * 
     * @param ID
     */
    public void setID(java.lang.String ID) {
        this.ID = ID;
    }


    /**
     * Gets the DESCRIPTION value for this ListSubscriptions_ReplyKeywordsKeyword.
     * 
     * @return DESCRIPTION
     */
    public java.lang.String getDESCRIPTION() {
        return DESCRIPTION;
    }


    /**
     * Sets the DESCRIPTION value for this ListSubscriptions_ReplyKeywordsKeyword.
     * 
     * @param DESCRIPTION
     */
    public void setDESCRIPTION(java.lang.String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }


    /**
     * Gets the AUDIENCE_CODE value for this ListSubscriptions_ReplyKeywordsKeyword.
     * 
     * @return AUDIENCE_CODE
     */
    public com.heb.xmlns.ei.SubscriptionCenterService.ListSubscriptions_Reply.AudienceCode getAUDIENCE_CODE() {
        return AUDIENCE_CODE;
    }


    /**
     * Sets the AUDIENCE_CODE value for this ListSubscriptions_ReplyKeywordsKeyword.
     * 
     * @param AUDIENCE_CODE
     */
    public void setAUDIENCE_CODE(com.heb.xmlns.ei.SubscriptionCenterService.ListSubscriptions_Reply.AudienceCode AUDIENCE_CODE) {
        this.AUDIENCE_CODE = AUDIENCE_CODE;
    }


    /**
     * Gets the SUBSCRIPTION_STATUS_CODE value for this ListSubscriptions_ReplyKeywordsKeyword.
     * 
     * @return SUBSCRIPTION_STATUS_CODE
     */
    public com.heb.xmlns.ei.SubscriptionCenterService.ListSubscriptions_Reply.SubscriptionStatusCode getSUBSCRIPTION_STATUS_CODE() {
        return SUBSCRIPTION_STATUS_CODE;
    }


    /**
     * Sets the SUBSCRIPTION_STATUS_CODE value for this ListSubscriptions_ReplyKeywordsKeyword.
     * 
     * @param SUBSCRIPTION_STATUS_CODE
     */
    public void setSUBSCRIPTION_STATUS_CODE(com.heb.xmlns.ei.SubscriptionCenterService.ListSubscriptions_Reply.SubscriptionStatusCode SUBSCRIPTION_STATUS_CODE) {
        this.SUBSCRIPTION_STATUS_CODE = SUBSCRIPTION_STATUS_CODE;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ListSubscriptions_ReplyKeywordsKeyword)) return false;
        ListSubscriptions_ReplyKeywordsKeyword other = (ListSubscriptions_ReplyKeywordsKeyword) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.ID==null && other.getID()==null) || 
             (this.ID!=null &&
              this.ID.equals(other.getID()))) &&
            ((this.DESCRIPTION==null && other.getDESCRIPTION()==null) || 
             (this.DESCRIPTION!=null &&
              this.DESCRIPTION.equals(other.getDESCRIPTION()))) &&
            ((this.AUDIENCE_CODE==null && other.getAUDIENCE_CODE()==null) || 
             (this.AUDIENCE_CODE!=null &&
              this.AUDIENCE_CODE.equals(other.getAUDIENCE_CODE()))) &&
            ((this.SUBSCRIPTION_STATUS_CODE==null && other.getSUBSCRIPTION_STATUS_CODE()==null) || 
             (this.SUBSCRIPTION_STATUS_CODE!=null &&
              this.SUBSCRIPTION_STATUS_CODE.equals(other.getSUBSCRIPTION_STATUS_CODE())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getID() != null) {
            _hashCode += getID().hashCode();
        }
        if (getDESCRIPTION() != null) {
            _hashCode += getDESCRIPTION().hashCode();
        }
        if (getAUDIENCE_CODE() != null) {
            _hashCode += getAUDIENCE_CODE().hashCode();
        }
        if (getSUBSCRIPTION_STATUS_CODE() != null) {
            _hashCode += getSUBSCRIPTION_STATUS_CODE().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ListSubscriptions_ReplyKeywordsKeyword.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/ListSubscriptions_Reply", ">>>ListSubscriptions_Reply>Keywords>Keyword"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/ListSubscriptions_Reply", "ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DESCRIPTION");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/ListSubscriptions_Reply", "DESCRIPTION"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("AUDIENCE_CODE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/ListSubscriptions_Reply", "AUDIENCE_CODE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/ListSubscriptions_Reply", "AudienceCode"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SUBSCRIPTION_STATUS_CODE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/ListSubscriptions_Reply", "SUBSCRIPTION_STATUS_CODE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/ListSubscriptions_Reply", "SubscriptionStatusCode"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
