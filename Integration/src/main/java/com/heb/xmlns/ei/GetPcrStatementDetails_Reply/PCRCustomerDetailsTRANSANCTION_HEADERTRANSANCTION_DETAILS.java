/**
 * PCRCustomerDetailsTRANSANCTION_HEADERTRANSANCTION_DETAILS.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.xmlns.ei.GetPcrStatementDetails_Reply;

public class PCRCustomerDetailsTRANSANCTION_HEADERTRANSANCTION_DETAILS  implements java.io.Serializable {
    private java.math.BigDecimal TRANSANCTION_ID;

    private java.math.BigInteger OFFER_ID;

    private java.lang.String INCENTIVE_NAME;

    private java.math.BigInteger POINTS;

    public PCRCustomerDetailsTRANSANCTION_HEADERTRANSANCTION_DETAILS() {
    }

    public PCRCustomerDetailsTRANSANCTION_HEADERTRANSANCTION_DETAILS(
           java.math.BigDecimal TRANSANCTION_ID,
           java.math.BigInteger OFFER_ID,
           java.lang.String INCENTIVE_NAME,
           java.math.BigInteger POINTS) {
           this.TRANSANCTION_ID = TRANSANCTION_ID;
           this.OFFER_ID = OFFER_ID;
           this.INCENTIVE_NAME = INCENTIVE_NAME;
           this.POINTS = POINTS;
    }


    /**
     * Gets the TRANSANCTION_ID value for this PCRCustomerDetailsTRANSANCTION_HEADERTRANSANCTION_DETAILS.
     * 
     * @return TRANSANCTION_ID
     */
    public java.math.BigDecimal getTRANSANCTION_ID() {
        return TRANSANCTION_ID;
    }


    /**
     * Sets the TRANSANCTION_ID value for this PCRCustomerDetailsTRANSANCTION_HEADERTRANSANCTION_DETAILS.
     * 
     * @param TRANSANCTION_ID
     */
    public void setTRANSANCTION_ID(java.math.BigDecimal TRANSANCTION_ID) {
        this.TRANSANCTION_ID = TRANSANCTION_ID;
    }


    /**
     * Gets the OFFER_ID value for this PCRCustomerDetailsTRANSANCTION_HEADERTRANSANCTION_DETAILS.
     * 
     * @return OFFER_ID
     */
    public java.math.BigInteger getOFFER_ID() {
        return OFFER_ID;
    }


    /**
     * Sets the OFFER_ID value for this PCRCustomerDetailsTRANSANCTION_HEADERTRANSANCTION_DETAILS.
     * 
     * @param OFFER_ID
     */
    public void setOFFER_ID(java.math.BigInteger OFFER_ID) {
        this.OFFER_ID = OFFER_ID;
    }


    /**
     * Gets the INCENTIVE_NAME value for this PCRCustomerDetailsTRANSANCTION_HEADERTRANSANCTION_DETAILS.
     * 
     * @return INCENTIVE_NAME
     */
    public java.lang.String getINCENTIVE_NAME() {
        return INCENTIVE_NAME;
    }


    /**
     * Sets the INCENTIVE_NAME value for this PCRCustomerDetailsTRANSANCTION_HEADERTRANSANCTION_DETAILS.
     * 
     * @param INCENTIVE_NAME
     */
    public void setINCENTIVE_NAME(java.lang.String INCENTIVE_NAME) {
        this.INCENTIVE_NAME = INCENTIVE_NAME;
    }


    /**
     * Gets the POINTS value for this PCRCustomerDetailsTRANSANCTION_HEADERTRANSANCTION_DETAILS.
     * 
     * @return POINTS
     */
    public java.math.BigInteger getPOINTS() {
        return POINTS;
    }


    /**
     * Sets the POINTS value for this PCRCustomerDetailsTRANSANCTION_HEADERTRANSANCTION_DETAILS.
     * 
     * @param POINTS
     */
    public void setPOINTS(java.math.BigInteger POINTS) {
        this.POINTS = POINTS;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PCRCustomerDetailsTRANSANCTION_HEADERTRANSANCTION_DETAILS)) return false;
        PCRCustomerDetailsTRANSANCTION_HEADERTRANSANCTION_DETAILS other = (PCRCustomerDetailsTRANSANCTION_HEADERTRANSANCTION_DETAILS) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.TRANSANCTION_ID==null && other.getTRANSANCTION_ID()==null) || 
             (this.TRANSANCTION_ID!=null &&
              this.TRANSANCTION_ID.equals(other.getTRANSANCTION_ID()))) &&
            ((this.OFFER_ID==null && other.getOFFER_ID()==null) || 
             (this.OFFER_ID!=null &&
              this.OFFER_ID.equals(other.getOFFER_ID()))) &&
            ((this.INCENTIVE_NAME==null && other.getINCENTIVE_NAME()==null) || 
             (this.INCENTIVE_NAME!=null &&
              this.INCENTIVE_NAME.equals(other.getINCENTIVE_NAME()))) &&
            ((this.POINTS==null && other.getPOINTS()==null) || 
             (this.POINTS!=null &&
              this.POINTS.equals(other.getPOINTS())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTRANSANCTION_ID() != null) {
            _hashCode += getTRANSANCTION_ID().hashCode();
        }
        if (getOFFER_ID() != null) {
            _hashCode += getOFFER_ID().hashCode();
        }
        if (getINCENTIVE_NAME() != null) {
            _hashCode += getINCENTIVE_NAME().hashCode();
        }
        if (getPOINTS() != null) {
            _hashCode += getPOINTS().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PCRCustomerDetailsTRANSANCTION_HEADERTRANSANCTION_DETAILS.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPcrStatementDetails_Reply", ">>>PCRCustomerDetails>TRANSANCTION_HEADER>TRANSANCTION_DETAILS"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TRANSANCTION_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPcrStatementDetails_Reply", "TRANSANCTION_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("OFFER_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPcrStatementDetails_Reply", "OFFER_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("INCENTIVE_NAME");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPcrStatementDetails_Reply", "INCENTIVE_NAME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("POINTS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPcrStatementDetails_Reply", "POINTS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
