/**
 * GetDccMemberAccountInformation_Request.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.xmlns.ei.GetDccMemberAccountInformation_Request;

public class GetDccMemberAccountInformation_Request  implements java.io.Serializable {
    private java.lang.String dccAlternateId;

    private com.heb.xmlns.ei.Authentication.Authentication authentication;

    public GetDccMemberAccountInformation_Request() {
    }

    public GetDccMemberAccountInformation_Request(
           java.lang.String dccAlternateId,
           com.heb.xmlns.ei.Authentication.Authentication authentication) {
           this.dccAlternateId = dccAlternateId;
           this.authentication = authentication;
    }


    /**
     * Gets the dccAlternateId value for this GetDccMemberAccountInformation_Request.
     * 
     * @return dccAlternateId
     */
    public java.lang.String getDccAlternateId() {
        return dccAlternateId;
    }


    /**
     * Sets the dccAlternateId value for this GetDccMemberAccountInformation_Request.
     * 
     * @param dccAlternateId
     */
    public void setDccAlternateId(java.lang.String dccAlternateId) {
        this.dccAlternateId = dccAlternateId;
    }


    /**
     * Gets the authentication value for this GetDccMemberAccountInformation_Request.
     * 
     * @return authentication
     */
    public com.heb.xmlns.ei.Authentication.Authentication getAuthentication() {
        return authentication;
    }


    /**
     * Sets the authentication value for this GetDccMemberAccountInformation_Request.
     * 
     * @param authentication
     */
    public void setAuthentication(com.heb.xmlns.ei.Authentication.Authentication authentication) {
        this.authentication = authentication;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetDccMemberAccountInformation_Request)) return false;
        GetDccMemberAccountInformation_Request other = (GetDccMemberAccountInformation_Request) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.dccAlternateId==null && other.getDccAlternateId()==null) || 
             (this.dccAlternateId!=null &&
              this.dccAlternateId.equals(other.getDccAlternateId()))) &&
            ((this.authentication==null && other.getAuthentication()==null) || 
             (this.authentication!=null &&
              this.authentication.equals(other.getAuthentication())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDccAlternateId() != null) {
            _hashCode += getDccAlternateId().hashCode();
        }
        if (getAuthentication() != null) {
            _hashCode += getAuthentication().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetDccMemberAccountInformation_Request.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetDccMemberAccountInformation_Request", ">GetDccMemberAccountInformation_Request"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dccAlternateId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetDccMemberAccountInformation_Request", "DccAlternateId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authentication");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Authentication", "Authentication"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Authentication", ">Authentication"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
