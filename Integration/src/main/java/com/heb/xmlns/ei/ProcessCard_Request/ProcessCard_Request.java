/**
 * ProcessCard_Request.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.xmlns.ei.ProcessCard_Request;

public class ProcessCard_Request  implements java.io.Serializable {
    private com.heb.xmlns.ei.PROGRAM_ACCOUNT.PROGRAM_ACCOUNT PROGRAM_ACCOUNT;

    private com.heb.xmlns.ei.Authentication.Authentication authentication;

    public ProcessCard_Request() {
    }

    public ProcessCard_Request(
           com.heb.xmlns.ei.PROGRAM_ACCOUNT.PROGRAM_ACCOUNT PROGRAM_ACCOUNT,
           com.heb.xmlns.ei.Authentication.Authentication authentication) {
           this.PROGRAM_ACCOUNT = PROGRAM_ACCOUNT;
           this.authentication = authentication;
    }


    /**
     * Gets the PROGRAM_ACCOUNT value for this ProcessCard_Request.
     * 
     * @return PROGRAM_ACCOUNT
     */
    public com.heb.xmlns.ei.PROGRAM_ACCOUNT.PROGRAM_ACCOUNT getPROGRAM_ACCOUNT() {
        return PROGRAM_ACCOUNT;
    }


    /**
     * Sets the PROGRAM_ACCOUNT value for this ProcessCard_Request.
     * 
     * @param PROGRAM_ACCOUNT
     */
    public void setPROGRAM_ACCOUNT(com.heb.xmlns.ei.PROGRAM_ACCOUNT.PROGRAM_ACCOUNT PROGRAM_ACCOUNT) {
        this.PROGRAM_ACCOUNT = PROGRAM_ACCOUNT;
    }


    /**
     * Gets the authentication value for this ProcessCard_Request.
     * 
     * @return authentication
     */
    public com.heb.xmlns.ei.Authentication.Authentication getAuthentication() {
        return authentication;
    }


    /**
     * Sets the authentication value for this ProcessCard_Request.
     * 
     * @param authentication
     */
    public void setAuthentication(com.heb.xmlns.ei.Authentication.Authentication authentication) {
        this.authentication = authentication;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ProcessCard_Request)) return false;
        ProcessCard_Request other = (ProcessCard_Request) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.PROGRAM_ACCOUNT==null && other.getPROGRAM_ACCOUNT()==null) || 
             (this.PROGRAM_ACCOUNT!=null &&
              this.PROGRAM_ACCOUNT.equals(other.getPROGRAM_ACCOUNT()))) &&
            ((this.authentication==null && other.getAuthentication()==null) || 
             (this.authentication!=null &&
              this.authentication.equals(other.getAuthentication())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPROGRAM_ACCOUNT() != null) {
            _hashCode += getPROGRAM_ACCOUNT().hashCode();
        }
        if (getAuthentication() != null) {
            _hashCode += getAuthentication().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ProcessCard_Request.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProcessCard_Request", ">ProcessCard_Request"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PROGRAM_ACCOUNT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PROGRAM_ACCOUNT", "PROGRAM_ACCOUNT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PROGRAM_ACCOUNT", ">PROGRAM_ACCOUNT"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authentication");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Authentication", "Authentication"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Authentication", ">Authentication"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
