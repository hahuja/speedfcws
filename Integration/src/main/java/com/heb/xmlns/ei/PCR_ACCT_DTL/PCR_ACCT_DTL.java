/**
 * PCR_ACCT_DTL.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.xmlns.ei.PCR_ACCT_DTL;

public class PCR_ACCT_DTL  implements java.io.Serializable {
    private java.math.BigInteger PGM_ACCT_ID;

    private java.math.BigInteger CURR_BAL_NBR;

    private java.math.BigDecimal CURR_RWRD_STRD_VAL_NBR;

    private java.lang.String ERN_PRD_PNT_END_DT;

    private java.lang.String RWRD_STRD_VAL_END_DT;

    public PCR_ACCT_DTL() {
    }

    public PCR_ACCT_DTL(
           java.math.BigInteger PGM_ACCT_ID,
           java.math.BigInteger CURR_BAL_NBR,
           java.math.BigDecimal CURR_RWRD_STRD_VAL_NBR,
           java.lang.String ERN_PRD_PNT_END_DT,
           java.lang.String RWRD_STRD_VAL_END_DT) {
           this.PGM_ACCT_ID = PGM_ACCT_ID;
           this.CURR_BAL_NBR = CURR_BAL_NBR;
           this.CURR_RWRD_STRD_VAL_NBR = CURR_RWRD_STRD_VAL_NBR;
           this.ERN_PRD_PNT_END_DT = ERN_PRD_PNT_END_DT;
           this.RWRD_STRD_VAL_END_DT = RWRD_STRD_VAL_END_DT;
    }


    /**
     * Gets the PGM_ACCT_ID value for this PCR_ACCT_DTL.
     * 
     * @return PGM_ACCT_ID
     */
    public java.math.BigInteger getPGM_ACCT_ID() {
        return PGM_ACCT_ID;
    }


    /**
     * Sets the PGM_ACCT_ID value for this PCR_ACCT_DTL.
     * 
     * @param PGM_ACCT_ID
     */
    public void setPGM_ACCT_ID(java.math.BigInteger PGM_ACCT_ID) {
        this.PGM_ACCT_ID = PGM_ACCT_ID;
    }


    /**
     * Gets the CURR_BAL_NBR value for this PCR_ACCT_DTL.
     * 
     * @return CURR_BAL_NBR
     */
    public java.math.BigInteger getCURR_BAL_NBR() {
        return CURR_BAL_NBR;
    }


    /**
     * Sets the CURR_BAL_NBR value for this PCR_ACCT_DTL.
     * 
     * @param CURR_BAL_NBR
     */
    public void setCURR_BAL_NBR(java.math.BigInteger CURR_BAL_NBR) {
        this.CURR_BAL_NBR = CURR_BAL_NBR;
    }


    /**
     * Gets the CURR_RWRD_STRD_VAL_NBR value for this PCR_ACCT_DTL.
     * 
     * @return CURR_RWRD_STRD_VAL_NBR
     */
    public java.math.BigDecimal getCURR_RWRD_STRD_VAL_NBR() {
        return CURR_RWRD_STRD_VAL_NBR;
    }


    /**
     * Sets the CURR_RWRD_STRD_VAL_NBR value for this PCR_ACCT_DTL.
     * 
     * @param CURR_RWRD_STRD_VAL_NBR
     */
    public void setCURR_RWRD_STRD_VAL_NBR(java.math.BigDecimal CURR_RWRD_STRD_VAL_NBR) {
        this.CURR_RWRD_STRD_VAL_NBR = CURR_RWRD_STRD_VAL_NBR;
    }


    /**
     * Gets the ERN_PRD_PNT_END_DT value for this PCR_ACCT_DTL.
     * 
     * @return ERN_PRD_PNT_END_DT
     */
    public java.lang.String getERN_PRD_PNT_END_DT() {
        return ERN_PRD_PNT_END_DT;
    }


    /**
     * Sets the ERN_PRD_PNT_END_DT value for this PCR_ACCT_DTL.
     * 
     * @param ERN_PRD_PNT_END_DT
     */
    public void setERN_PRD_PNT_END_DT(java.lang.String ERN_PRD_PNT_END_DT) {
        this.ERN_PRD_PNT_END_DT = ERN_PRD_PNT_END_DT;
    }


    /**
     * Gets the RWRD_STRD_VAL_END_DT value for this PCR_ACCT_DTL.
     * 
     * @return RWRD_STRD_VAL_END_DT
     */
    public java.lang.String getRWRD_STRD_VAL_END_DT() {
        return RWRD_STRD_VAL_END_DT;
    }


    /**
     * Sets the RWRD_STRD_VAL_END_DT value for this PCR_ACCT_DTL.
     * 
     * @param RWRD_STRD_VAL_END_DT
     */
    public void setRWRD_STRD_VAL_END_DT(java.lang.String RWRD_STRD_VAL_END_DT) {
        this.RWRD_STRD_VAL_END_DT = RWRD_STRD_VAL_END_DT;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PCR_ACCT_DTL)) return false;
        PCR_ACCT_DTL other = (PCR_ACCT_DTL) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.PGM_ACCT_ID==null && other.getPGM_ACCT_ID()==null) || 
             (this.PGM_ACCT_ID!=null &&
              this.PGM_ACCT_ID.equals(other.getPGM_ACCT_ID()))) &&
            ((this.CURR_BAL_NBR==null && other.getCURR_BAL_NBR()==null) || 
             (this.CURR_BAL_NBR!=null &&
              this.CURR_BAL_NBR.equals(other.getCURR_BAL_NBR()))) &&
            ((this.CURR_RWRD_STRD_VAL_NBR==null && other.getCURR_RWRD_STRD_VAL_NBR()==null) || 
             (this.CURR_RWRD_STRD_VAL_NBR!=null &&
              this.CURR_RWRD_STRD_VAL_NBR.equals(other.getCURR_RWRD_STRD_VAL_NBR()))) &&
            ((this.ERN_PRD_PNT_END_DT==null && other.getERN_PRD_PNT_END_DT()==null) || 
             (this.ERN_PRD_PNT_END_DT!=null &&
              this.ERN_PRD_PNT_END_DT.equals(other.getERN_PRD_PNT_END_DT()))) &&
            ((this.RWRD_STRD_VAL_END_DT==null && other.getRWRD_STRD_VAL_END_DT()==null) || 
             (this.RWRD_STRD_VAL_END_DT!=null &&
              this.RWRD_STRD_VAL_END_DT.equals(other.getRWRD_STRD_VAL_END_DT())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPGM_ACCT_ID() != null) {
            _hashCode += getPGM_ACCT_ID().hashCode();
        }
        if (getCURR_BAL_NBR() != null) {
            _hashCode += getCURR_BAL_NBR().hashCode();
        }
        if (getCURR_RWRD_STRD_VAL_NBR() != null) {
            _hashCode += getCURR_RWRD_STRD_VAL_NBR().hashCode();
        }
        if (getERN_PRD_PNT_END_DT() != null) {
            _hashCode += getERN_PRD_PNT_END_DT().hashCode();
        }
        if (getRWRD_STRD_VAL_END_DT() != null) {
            _hashCode += getRWRD_STRD_VAL_END_DT().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PCR_ACCT_DTL.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PCR_ACCT_DTL", ">PCR_ACCT_DTL"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PGM_ACCT_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PCR_ACCT_DTL", "PGM_ACCT_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CURR_BAL_NBR");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PCR_ACCT_DTL", "CURR_BAL_NBR"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CURR_RWRD_STRD_VAL_NBR");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PCR_ACCT_DTL", "CURR_RWRD_STRD_VAL_NBR"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ERN_PRD_PNT_END_DT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PCR_ACCT_DTL", "ERN_PRD_PNT_END_DT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("RWRD_STRD_VAL_END_DT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PCR_ACCT_DTL", "RWRD_STRD_VAL_END_DT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
