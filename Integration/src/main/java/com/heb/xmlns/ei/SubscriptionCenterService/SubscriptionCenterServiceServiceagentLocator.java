/**
 * SubscriptionCenterServiceServiceagentLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.xmlns.ei.SubscriptionCenterService;

public class SubscriptionCenterServiceServiceagentLocator extends org.apache.axis.client.Service implements com.heb.xmlns.ei.SubscriptionCenterService.SubscriptionCenterServiceServiceagent {

    public SubscriptionCenterServiceServiceagentLocator() {
    }


    public SubscriptionCenterServiceServiceagentLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public SubscriptionCenterServiceServiceagentLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for SubscriptionCenterService
    private java.lang.String SubscriptionCenterService_address = "http://arpapl0016723.heb.com:9601/SubscriptionCenterService";

    public java.lang.String getSubscriptionCenterServiceAddress() {
        return SubscriptionCenterService_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String SubscriptionCenterServiceWSDDServiceName = "SubscriptionCenterService";

    public java.lang.String getSubscriptionCenterServiceWSDDServiceName() {
        return SubscriptionCenterServiceWSDDServiceName;
    }

    public void setSubscriptionCenterServiceWSDDServiceName(java.lang.String name) {
        SubscriptionCenterServiceWSDDServiceName = name;
    }

    public com.heb.xmlns.ei.SubscriptionCenterService.SubscriptionCenterService_PortType getSubscriptionCenterService() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(SubscriptionCenterService_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getSubscriptionCenterService(endpoint);
    }

    public com.heb.xmlns.ei.SubscriptionCenterService.SubscriptionCenterService_PortType getSubscriptionCenterService(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.heb.xmlns.ei.SubscriptionCenterService.SubscriptionCenterServiceBindingStub _stub = new com.heb.xmlns.ei.SubscriptionCenterService.SubscriptionCenterServiceBindingStub(portAddress, this);
            _stub.setPortName(getSubscriptionCenterServiceWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setSubscriptionCenterServiceEndpointAddress(java.lang.String address) {
        SubscriptionCenterService_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.heb.xmlns.ei.SubscriptionCenterService.SubscriptionCenterService_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.heb.xmlns.ei.SubscriptionCenterService.SubscriptionCenterServiceBindingStub _stub = new com.heb.xmlns.ei.SubscriptionCenterService.SubscriptionCenterServiceBindingStub(new java.net.URL(SubscriptionCenterService_address), this);
                _stub.setPortName(getSubscriptionCenterServiceWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("SubscriptionCenterService".equals(inputPortName)) {
            return getSubscriptionCenterService();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService", "SubscriptionCenterService.serviceagent");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService", "SubscriptionCenterService"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("SubscriptionCenterService".equals(portName)) {
            setSubscriptionCenterServiceEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
