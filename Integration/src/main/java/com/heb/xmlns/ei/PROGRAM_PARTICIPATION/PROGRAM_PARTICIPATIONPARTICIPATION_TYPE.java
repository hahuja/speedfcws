/**
 * PROGRAM_PARTICIPATIONPARTICIPATION_TYPE.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.xmlns.ei.PROGRAM_PARTICIPATION;

public class PROGRAM_PARTICIPATIONPARTICIPATION_TYPE  implements java.io.Serializable {
    private java.lang.String PARTICIPATION_TYPE_DESC;

    private java.lang.String PART_TYP_CD;

    private java.lang.String STATUS;

    private java.lang.String START_DATE;

    private java.lang.String END_DATE;

    public PROGRAM_PARTICIPATIONPARTICIPATION_TYPE() {
    }

    public PROGRAM_PARTICIPATIONPARTICIPATION_TYPE(
           java.lang.String PARTICIPATION_TYPE_DESC,
           java.lang.String PART_TYP_CD,
           java.lang.String STATUS,
           java.lang.String START_DATE,
           java.lang.String END_DATE) {
           this.PARTICIPATION_TYPE_DESC = PARTICIPATION_TYPE_DESC;
           this.PART_TYP_CD = PART_TYP_CD;
           this.STATUS = STATUS;
           this.START_DATE = START_DATE;
           this.END_DATE = END_DATE;
    }


    /**
     * Gets the PARTICIPATION_TYPE_DESC value for this PROGRAM_PARTICIPATIONPARTICIPATION_TYPE.
     * 
     * @return PARTICIPATION_TYPE_DESC
     */
    public java.lang.String getPARTICIPATION_TYPE_DESC() {
        return PARTICIPATION_TYPE_DESC;
    }


    /**
     * Sets the PARTICIPATION_TYPE_DESC value for this PROGRAM_PARTICIPATIONPARTICIPATION_TYPE.
     * 
     * @param PARTICIPATION_TYPE_DESC
     */
    public void setPARTICIPATION_TYPE_DESC(java.lang.String PARTICIPATION_TYPE_DESC) {
        this.PARTICIPATION_TYPE_DESC = PARTICIPATION_TYPE_DESC;
    }


    /**
     * Gets the PART_TYP_CD value for this PROGRAM_PARTICIPATIONPARTICIPATION_TYPE.
     * 
     * @return PART_TYP_CD
     */
    public java.lang.String getPART_TYP_CD() {
        return PART_TYP_CD;
    }


    /**
     * Sets the PART_TYP_CD value for this PROGRAM_PARTICIPATIONPARTICIPATION_TYPE.
     * 
     * @param PART_TYP_CD
     */
    public void setPART_TYP_CD(java.lang.String PART_TYP_CD) {
        this.PART_TYP_CD = PART_TYP_CD;
    }


    /**
     * Gets the STATUS value for this PROGRAM_PARTICIPATIONPARTICIPATION_TYPE.
     * 
     * @return STATUS
     */
    public java.lang.String getSTATUS() {
        return STATUS;
    }


    /**
     * Sets the STATUS value for this PROGRAM_PARTICIPATIONPARTICIPATION_TYPE.
     * 
     * @param STATUS
     */
    public void setSTATUS(java.lang.String STATUS) {
        this.STATUS = STATUS;
    }


    /**
     * Gets the START_DATE value for this PROGRAM_PARTICIPATIONPARTICIPATION_TYPE.
     * 
     * @return START_DATE
     */
    public java.lang.String getSTART_DATE() {
        return START_DATE;
    }


    /**
     * Sets the START_DATE value for this PROGRAM_PARTICIPATIONPARTICIPATION_TYPE.
     * 
     * @param START_DATE
     */
    public void setSTART_DATE(java.lang.String START_DATE) {
        this.START_DATE = START_DATE;
    }


    /**
     * Gets the END_DATE value for this PROGRAM_PARTICIPATIONPARTICIPATION_TYPE.
     * 
     * @return END_DATE
     */
    public java.lang.String getEND_DATE() {
        return END_DATE;
    }


    /**
     * Sets the END_DATE value for this PROGRAM_PARTICIPATIONPARTICIPATION_TYPE.
     * 
     * @param END_DATE
     */
    public void setEND_DATE(java.lang.String END_DATE) {
        this.END_DATE = END_DATE;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PROGRAM_PARTICIPATIONPARTICIPATION_TYPE)) return false;
        PROGRAM_PARTICIPATIONPARTICIPATION_TYPE other = (PROGRAM_PARTICIPATIONPARTICIPATION_TYPE) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.PARTICIPATION_TYPE_DESC==null && other.getPARTICIPATION_TYPE_DESC()==null) || 
             (this.PARTICIPATION_TYPE_DESC!=null &&
              this.PARTICIPATION_TYPE_DESC.equals(other.getPARTICIPATION_TYPE_DESC()))) &&
            ((this.PART_TYP_CD==null && other.getPART_TYP_CD()==null) || 
             (this.PART_TYP_CD!=null &&
              this.PART_TYP_CD.equals(other.getPART_TYP_CD()))) &&
            ((this.STATUS==null && other.getSTATUS()==null) || 
             (this.STATUS!=null &&
              this.STATUS.equals(other.getSTATUS()))) &&
            ((this.START_DATE==null && other.getSTART_DATE()==null) || 
             (this.START_DATE!=null &&
              this.START_DATE.equals(other.getSTART_DATE()))) &&
            ((this.END_DATE==null && other.getEND_DATE()==null) || 
             (this.END_DATE!=null &&
              this.END_DATE.equals(other.getEND_DATE())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPARTICIPATION_TYPE_DESC() != null) {
            _hashCode += getPARTICIPATION_TYPE_DESC().hashCode();
        }
        if (getPART_TYP_CD() != null) {
            _hashCode += getPART_TYP_CD().hashCode();
        }
        if (getSTATUS() != null) {
            _hashCode += getSTATUS().hashCode();
        }
        if (getSTART_DATE() != null) {
            _hashCode += getSTART_DATE().hashCode();
        }
        if (getEND_DATE() != null) {
            _hashCode += getEND_DATE().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PROGRAM_PARTICIPATIONPARTICIPATION_TYPE.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PROGRAM_PARTICIPATION", ">>PROGRAM_PARTICIPATION>PARTICIPATION_TYPE"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PARTICIPATION_TYPE_DESC");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PROGRAM_PARTICIPATION", "PARTICIPATION_TYPE_DESC"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PART_TYP_CD");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PROGRAM_PARTICIPATION", "PART_TYP_CD"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("STATUS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PROGRAM_PARTICIPATION", "STATUS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("START_DATE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PROGRAM_PARTICIPATION", "START_DATE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("END_DATE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PROGRAM_PARTICIPATION", "END_DATE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
