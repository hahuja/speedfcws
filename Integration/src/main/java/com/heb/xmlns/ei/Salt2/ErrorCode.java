/**
 * ErrorCode.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.xmlns.ei.Salt2;

public class ErrorCode implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected ErrorCode(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _SYSTEM_ERROR = "SYSTEM_ERROR";
    public static final java.lang.String _INVALID_MDN = "INVALID_MDN";
    public static final java.lang.String _BLACKLISTED_MDN = "BLACKLISTED_MDN";
    public static final java.lang.String _INVALID_SOURCE_CODE = "INVALID_SOURCE_CODE";
    public static final java.lang.String _INVALID_KEYWORD = "INVALID_KEYWORD";
    public static final java.lang.String _INVALID_PARTNER_ID = "INVALID_PARTNER_ID";
    public static final ErrorCode SYSTEM_ERROR = new ErrorCode(_SYSTEM_ERROR);
    public static final ErrorCode INVALID_MDN = new ErrorCode(_INVALID_MDN);
    public static final ErrorCode BLACKLISTED_MDN = new ErrorCode(_BLACKLISTED_MDN);
    public static final ErrorCode INVALID_SOURCE_CODE = new ErrorCode(_INVALID_SOURCE_CODE);
    public static final ErrorCode INVALID_KEYWORD = new ErrorCode(_INVALID_KEYWORD);
    public static final ErrorCode INVALID_PARTNER_ID = new ErrorCode(_INVALID_PARTNER_ID);
    public java.lang.String getValue() { return _value_;}
    public static ErrorCode fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        ErrorCode enumeration = (ErrorCode)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static ErrorCode fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ErrorCode.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Salt2", "ErrorCode"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
