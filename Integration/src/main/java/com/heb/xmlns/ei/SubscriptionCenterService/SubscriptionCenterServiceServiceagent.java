/**
 * SubscriptionCenterServiceServiceagent.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.xmlns.ei.SubscriptionCenterService;

public interface SubscriptionCenterServiceServiceagent extends javax.xml.rpc.Service {
    public java.lang.String getSubscriptionCenterServiceAddress();

    public com.heb.xmlns.ei.SubscriptionCenterService.SubscriptionCenterService_PortType getSubscriptionCenterService() throws javax.xml.rpc.ServiceException;

    public com.heb.xmlns.ei.SubscriptionCenterService.SubscriptionCenterService_PortType getSubscriptionCenterService(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
