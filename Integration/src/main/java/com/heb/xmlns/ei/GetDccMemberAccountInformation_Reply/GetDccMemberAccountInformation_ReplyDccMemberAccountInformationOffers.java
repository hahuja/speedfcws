/**
 * GetDccMemberAccountInformation_ReplyDccMemberAccountInformationOffers.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.xmlns.ei.GetDccMemberAccountInformation_Reply;

public class GetDccMemberAccountInformation_ReplyDccMemberAccountInformationOffers  implements java.io.Serializable {
    private java.math.BigInteger incentiveId;

    private java.lang.String incentiveName;

    public GetDccMemberAccountInformation_ReplyDccMemberAccountInformationOffers() {
    }

    public GetDccMemberAccountInformation_ReplyDccMemberAccountInformationOffers(
           java.math.BigInteger incentiveId,
           java.lang.String incentiveName) {
           this.incentiveId = incentiveId;
           this.incentiveName = incentiveName;
    }


    /**
     * Gets the incentiveId value for this GetDccMemberAccountInformation_ReplyDccMemberAccountInformationOffers.
     * 
     * @return incentiveId
     */
    public java.math.BigInteger getIncentiveId() {
        return incentiveId;
    }


    /**
     * Sets the incentiveId value for this GetDccMemberAccountInformation_ReplyDccMemberAccountInformationOffers.
     * 
     * @param incentiveId
     */
    public void setIncentiveId(java.math.BigInteger incentiveId) {
        this.incentiveId = incentiveId;
    }


    /**
     * Gets the incentiveName value for this GetDccMemberAccountInformation_ReplyDccMemberAccountInformationOffers.
     * 
     * @return incentiveName
     */
    public java.lang.String getIncentiveName() {
        return incentiveName;
    }


    /**
     * Sets the incentiveName value for this GetDccMemberAccountInformation_ReplyDccMemberAccountInformationOffers.
     * 
     * @param incentiveName
     */
    public void setIncentiveName(java.lang.String incentiveName) {
        this.incentiveName = incentiveName;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetDccMemberAccountInformation_ReplyDccMemberAccountInformationOffers)) return false;
        GetDccMemberAccountInformation_ReplyDccMemberAccountInformationOffers other = (GetDccMemberAccountInformation_ReplyDccMemberAccountInformationOffers) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.incentiveId==null && other.getIncentiveId()==null) || 
             (this.incentiveId!=null &&
              this.incentiveId.equals(other.getIncentiveId()))) &&
            ((this.incentiveName==null && other.getIncentiveName()==null) || 
             (this.incentiveName!=null &&
              this.incentiveName.equals(other.getIncentiveName())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getIncentiveId() != null) {
            _hashCode += getIncentiveId().hashCode();
        }
        if (getIncentiveName() != null) {
            _hashCode += getIncentiveName().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetDccMemberAccountInformation_ReplyDccMemberAccountInformationOffers.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetDccMemberAccountInformation_Reply", ">>>GetDccMemberAccountInformation_Reply>DccMemberAccountInformation>Offers"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("incentiveId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetDccMemberAccountInformation_Reply", "IncentiveId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("incentiveName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetDccMemberAccountInformation_Reply", "IncentiveName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
