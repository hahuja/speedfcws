/**
 * PCRCustomerDetailsTRANSANCTION_HEADER.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.xmlns.ei.GetPcrStatementDetails_Reply;

public class PCRCustomerDetailsTRANSANCTION_HEADER  implements java.io.Serializable {
    private java.math.BigDecimal TRANSANCTION_ID;

    private java.math.BigInteger ORDER_NUMBER;

    private java.lang.String DATE;

    private java.math.BigInteger STORE_NUMBER;

    private java.lang.String STORE_LOCATION;

    private java.math.BigDecimal AMOUNT;

    private java.math.BigInteger POINTS;

    private java.math.BigDecimal CARD_NUMBER;

    private java.math.BigDecimal AWARDS_DOLLARS_REDEEMED;

    private com.heb.xmlns.ei.GetPcrStatementDetails_Reply.PCRCustomerDetailsTRANSANCTION_HEADERTRANSANCTION_DETAILS[] TRANSANCTION_DETAILS;

    public PCRCustomerDetailsTRANSANCTION_HEADER() {
    }

    public PCRCustomerDetailsTRANSANCTION_HEADER(
           java.math.BigDecimal TRANSANCTION_ID,
           java.math.BigInteger ORDER_NUMBER,
           java.lang.String DATE,
           java.math.BigInteger STORE_NUMBER,
           java.lang.String STORE_LOCATION,
           java.math.BigDecimal AMOUNT,
           java.math.BigInteger POINTS,
           java.math.BigDecimal CARD_NUMBER,
           java.math.BigDecimal AWARDS_DOLLARS_REDEEMED,
           com.heb.xmlns.ei.GetPcrStatementDetails_Reply.PCRCustomerDetailsTRANSANCTION_HEADERTRANSANCTION_DETAILS[] TRANSANCTION_DETAILS) {
           this.TRANSANCTION_ID = TRANSANCTION_ID;
           this.ORDER_NUMBER = ORDER_NUMBER;
           this.DATE = DATE;
           this.STORE_NUMBER = STORE_NUMBER;
           this.STORE_LOCATION = STORE_LOCATION;
           this.AMOUNT = AMOUNT;
           this.POINTS = POINTS;
           this.CARD_NUMBER = CARD_NUMBER;
           this.AWARDS_DOLLARS_REDEEMED = AWARDS_DOLLARS_REDEEMED;
           this.TRANSANCTION_DETAILS = TRANSANCTION_DETAILS;
    }


    /**
     * Gets the TRANSANCTION_ID value for this PCRCustomerDetailsTRANSANCTION_HEADER.
     * 
     * @return TRANSANCTION_ID
     */
    public java.math.BigDecimal getTRANSANCTION_ID() {
        return TRANSANCTION_ID;
    }


    /**
     * Sets the TRANSANCTION_ID value for this PCRCustomerDetailsTRANSANCTION_HEADER.
     * 
     * @param TRANSANCTION_ID
     */
    public void setTRANSANCTION_ID(java.math.BigDecimal TRANSANCTION_ID) {
        this.TRANSANCTION_ID = TRANSANCTION_ID;
    }


    /**
     * Gets the ORDER_NUMBER value for this PCRCustomerDetailsTRANSANCTION_HEADER.
     * 
     * @return ORDER_NUMBER
     */
    public java.math.BigInteger getORDER_NUMBER() {
        return ORDER_NUMBER;
    }


    /**
     * Sets the ORDER_NUMBER value for this PCRCustomerDetailsTRANSANCTION_HEADER.
     * 
     * @param ORDER_NUMBER
     */
    public void setORDER_NUMBER(java.math.BigInteger ORDER_NUMBER) {
        this.ORDER_NUMBER = ORDER_NUMBER;
    }


    /**
     * Gets the DATE value for this PCRCustomerDetailsTRANSANCTION_HEADER.
     * 
     * @return DATE
     */
    public java.lang.String getDATE() {
        return DATE;
    }


    /**
     * Sets the DATE value for this PCRCustomerDetailsTRANSANCTION_HEADER.
     * 
     * @param DATE
     */
    public void setDATE(java.lang.String DATE) {
        this.DATE = DATE;
    }


    /**
     * Gets the STORE_NUMBER value for this PCRCustomerDetailsTRANSANCTION_HEADER.
     * 
     * @return STORE_NUMBER
     */
    public java.math.BigInteger getSTORE_NUMBER() {
        return STORE_NUMBER;
    }


    /**
     * Sets the STORE_NUMBER value for this PCRCustomerDetailsTRANSANCTION_HEADER.
     * 
     * @param STORE_NUMBER
     */
    public void setSTORE_NUMBER(java.math.BigInteger STORE_NUMBER) {
        this.STORE_NUMBER = STORE_NUMBER;
    }


    /**
     * Gets the STORE_LOCATION value for this PCRCustomerDetailsTRANSANCTION_HEADER.
     * 
     * @return STORE_LOCATION
     */
    public java.lang.String getSTORE_LOCATION() {
        return STORE_LOCATION;
    }


    /**
     * Sets the STORE_LOCATION value for this PCRCustomerDetailsTRANSANCTION_HEADER.
     * 
     * @param STORE_LOCATION
     */
    public void setSTORE_LOCATION(java.lang.String STORE_LOCATION) {
        this.STORE_LOCATION = STORE_LOCATION;
    }


    /**
     * Gets the AMOUNT value for this PCRCustomerDetailsTRANSANCTION_HEADER.
     * 
     * @return AMOUNT
     */
    public java.math.BigDecimal getAMOUNT() {
        return AMOUNT;
    }


    /**
     * Sets the AMOUNT value for this PCRCustomerDetailsTRANSANCTION_HEADER.
     * 
     * @param AMOUNT
     */
    public void setAMOUNT(java.math.BigDecimal AMOUNT) {
        this.AMOUNT = AMOUNT;
    }


    /**
     * Gets the POINTS value for this PCRCustomerDetailsTRANSANCTION_HEADER.
     * 
     * @return POINTS
     */
    public java.math.BigInteger getPOINTS() {
        return POINTS;
    }


    /**
     * Sets the POINTS value for this PCRCustomerDetailsTRANSANCTION_HEADER.
     * 
     * @param POINTS
     */
    public void setPOINTS(java.math.BigInteger POINTS) {
        this.POINTS = POINTS;
    }


    /**
     * Gets the CARD_NUMBER value for this PCRCustomerDetailsTRANSANCTION_HEADER.
     * 
     * @return CARD_NUMBER
     */
    public java.math.BigDecimal getCARD_NUMBER() {
        return CARD_NUMBER;
    }


    /**
     * Sets the CARD_NUMBER value for this PCRCustomerDetailsTRANSANCTION_HEADER.
     * 
     * @param CARD_NUMBER
     */
    public void setCARD_NUMBER(java.math.BigDecimal CARD_NUMBER) {
        this.CARD_NUMBER = CARD_NUMBER;
    }


    /**
     * Gets the AWARDS_DOLLARS_REDEEMED value for this PCRCustomerDetailsTRANSANCTION_HEADER.
     * 
     * @return AWARDS_DOLLARS_REDEEMED
     */
    public java.math.BigDecimal getAWARDS_DOLLARS_REDEEMED() {
        return AWARDS_DOLLARS_REDEEMED;
    }


    /**
     * Sets the AWARDS_DOLLARS_REDEEMED value for this PCRCustomerDetailsTRANSANCTION_HEADER.
     * 
     * @param AWARDS_DOLLARS_REDEEMED
     */
    public void setAWARDS_DOLLARS_REDEEMED(java.math.BigDecimal AWARDS_DOLLARS_REDEEMED) {
        this.AWARDS_DOLLARS_REDEEMED = AWARDS_DOLLARS_REDEEMED;
    }


    /**
     * Gets the TRANSANCTION_DETAILS value for this PCRCustomerDetailsTRANSANCTION_HEADER.
     * 
     * @return TRANSANCTION_DETAILS
     */
    public com.heb.xmlns.ei.GetPcrStatementDetails_Reply.PCRCustomerDetailsTRANSANCTION_HEADERTRANSANCTION_DETAILS[] getTRANSANCTION_DETAILS() {
        return TRANSANCTION_DETAILS;
    }


    /**
     * Sets the TRANSANCTION_DETAILS value for this PCRCustomerDetailsTRANSANCTION_HEADER.
     * 
     * @param TRANSANCTION_DETAILS
     */
    public void setTRANSANCTION_DETAILS(com.heb.xmlns.ei.GetPcrStatementDetails_Reply.PCRCustomerDetailsTRANSANCTION_HEADERTRANSANCTION_DETAILS[] TRANSANCTION_DETAILS) {
        this.TRANSANCTION_DETAILS = TRANSANCTION_DETAILS;
    }

    public com.heb.xmlns.ei.GetPcrStatementDetails_Reply.PCRCustomerDetailsTRANSANCTION_HEADERTRANSANCTION_DETAILS getTRANSANCTION_DETAILS(int i) {
        return this.TRANSANCTION_DETAILS[i];
    }

    public void setTRANSANCTION_DETAILS(int i, com.heb.xmlns.ei.GetPcrStatementDetails_Reply.PCRCustomerDetailsTRANSANCTION_HEADERTRANSANCTION_DETAILS _value) {
        this.TRANSANCTION_DETAILS[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PCRCustomerDetailsTRANSANCTION_HEADER)) return false;
        PCRCustomerDetailsTRANSANCTION_HEADER other = (PCRCustomerDetailsTRANSANCTION_HEADER) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.TRANSANCTION_ID==null && other.getTRANSANCTION_ID()==null) || 
             (this.TRANSANCTION_ID!=null &&
              this.TRANSANCTION_ID.equals(other.getTRANSANCTION_ID()))) &&
            ((this.ORDER_NUMBER==null && other.getORDER_NUMBER()==null) || 
             (this.ORDER_NUMBER!=null &&
              this.ORDER_NUMBER.equals(other.getORDER_NUMBER()))) &&
            ((this.DATE==null && other.getDATE()==null) || 
             (this.DATE!=null &&
              this.DATE.equals(other.getDATE()))) &&
            ((this.STORE_NUMBER==null && other.getSTORE_NUMBER()==null) || 
             (this.STORE_NUMBER!=null &&
              this.STORE_NUMBER.equals(other.getSTORE_NUMBER()))) &&
            ((this.STORE_LOCATION==null && other.getSTORE_LOCATION()==null) || 
             (this.STORE_LOCATION!=null &&
              this.STORE_LOCATION.equals(other.getSTORE_LOCATION()))) &&
            ((this.AMOUNT==null && other.getAMOUNT()==null) || 
             (this.AMOUNT!=null &&
              this.AMOUNT.equals(other.getAMOUNT()))) &&
            ((this.POINTS==null && other.getPOINTS()==null) || 
             (this.POINTS!=null &&
              this.POINTS.equals(other.getPOINTS()))) &&
            ((this.CARD_NUMBER==null && other.getCARD_NUMBER()==null) || 
             (this.CARD_NUMBER!=null &&
              this.CARD_NUMBER.equals(other.getCARD_NUMBER()))) &&
            ((this.AWARDS_DOLLARS_REDEEMED==null && other.getAWARDS_DOLLARS_REDEEMED()==null) || 
             (this.AWARDS_DOLLARS_REDEEMED!=null &&
              this.AWARDS_DOLLARS_REDEEMED.equals(other.getAWARDS_DOLLARS_REDEEMED()))) &&
            ((this.TRANSANCTION_DETAILS==null && other.getTRANSANCTION_DETAILS()==null) || 
             (this.TRANSANCTION_DETAILS!=null &&
              java.util.Arrays.equals(this.TRANSANCTION_DETAILS, other.getTRANSANCTION_DETAILS())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTRANSANCTION_ID() != null) {
            _hashCode += getTRANSANCTION_ID().hashCode();
        }
        if (getORDER_NUMBER() != null) {
            _hashCode += getORDER_NUMBER().hashCode();
        }
        if (getDATE() != null) {
            _hashCode += getDATE().hashCode();
        }
        if (getSTORE_NUMBER() != null) {
            _hashCode += getSTORE_NUMBER().hashCode();
        }
        if (getSTORE_LOCATION() != null) {
            _hashCode += getSTORE_LOCATION().hashCode();
        }
        if (getAMOUNT() != null) {
            _hashCode += getAMOUNT().hashCode();
        }
        if (getPOINTS() != null) {
            _hashCode += getPOINTS().hashCode();
        }
        if (getCARD_NUMBER() != null) {
            _hashCode += getCARD_NUMBER().hashCode();
        }
        if (getAWARDS_DOLLARS_REDEEMED() != null) {
            _hashCode += getAWARDS_DOLLARS_REDEEMED().hashCode();
        }
        if (getTRANSANCTION_DETAILS() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTRANSANCTION_DETAILS());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTRANSANCTION_DETAILS(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PCRCustomerDetailsTRANSANCTION_HEADER.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPcrStatementDetails_Reply", ">>PCRCustomerDetails>TRANSANCTION_HEADER"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TRANSANCTION_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPcrStatementDetails_Reply", "TRANSANCTION_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ORDER_NUMBER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPcrStatementDetails_Reply", "ORDER_NUMBER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPcrStatementDetails_Reply", "DATE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("STORE_NUMBER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPcrStatementDetails_Reply", "STORE_NUMBER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("STORE_LOCATION");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPcrStatementDetails_Reply", "STORE_LOCATION"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("AMOUNT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPcrStatementDetails_Reply", "AMOUNT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("POINTS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPcrStatementDetails_Reply", "POINTS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CARD_NUMBER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPcrStatementDetails_Reply", "CARD_NUMBER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("AWARDS_DOLLARS_REDEEMED");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPcrStatementDetails_Reply", "AWARDS_DOLLARS_REDEEMED"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TRANSANCTION_DETAILS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPcrStatementDetails_Reply", "TRANSANCTION_DETAILS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPcrStatementDetails_Reply", ">>>PCRCustomerDetails>TRANSANCTION_HEADER>TRANSANCTION_DETAILS"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
