/**
 * ListSubscriptions_Request.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.xmlns.ei.SubscriptionCenterService.ListSubscriptions_Request;

public class ListSubscriptions_Request  implements java.io.Serializable {
    private java.lang.String MOBILE_DIRECTORY_NUMBER;

    private java.math.BigInteger PARTNER_ID;

    private com.heb.xmlns.ei.Authentication.Authentication authentication;

    public ListSubscriptions_Request() {
    }

    public ListSubscriptions_Request(
           java.lang.String MOBILE_DIRECTORY_NUMBER,
           java.math.BigInteger PARTNER_ID,
           com.heb.xmlns.ei.Authentication.Authentication authentication) {
           this.MOBILE_DIRECTORY_NUMBER = MOBILE_DIRECTORY_NUMBER;
           this.PARTNER_ID = PARTNER_ID;
           this.authentication = authentication;
    }


    /**
     * Gets the MOBILE_DIRECTORY_NUMBER value for this ListSubscriptions_Request.
     * 
     * @return MOBILE_DIRECTORY_NUMBER
     */
    public java.lang.String getMOBILE_DIRECTORY_NUMBER() {
        return MOBILE_DIRECTORY_NUMBER;
    }


    /**
     * Sets the MOBILE_DIRECTORY_NUMBER value for this ListSubscriptions_Request.
     * 
     * @param MOBILE_DIRECTORY_NUMBER
     */
    public void setMOBILE_DIRECTORY_NUMBER(java.lang.String MOBILE_DIRECTORY_NUMBER) {
        this.MOBILE_DIRECTORY_NUMBER = MOBILE_DIRECTORY_NUMBER;
    }


    /**
     * Gets the PARTNER_ID value for this ListSubscriptions_Request.
     * 
     * @return PARTNER_ID
     */
    public java.math.BigInteger getPARTNER_ID() {
        return PARTNER_ID;
    }


    /**
     * Sets the PARTNER_ID value for this ListSubscriptions_Request.
     * 
     * @param PARTNER_ID
     */
    public void setPARTNER_ID(java.math.BigInteger PARTNER_ID) {
        this.PARTNER_ID = PARTNER_ID;
    }


    /**
     * Gets the authentication value for this ListSubscriptions_Request.
     * 
     * @return authentication
     */
    public com.heb.xmlns.ei.Authentication.Authentication getAuthentication() {
        return authentication;
    }


    /**
     * Sets the authentication value for this ListSubscriptions_Request.
     * 
     * @param authentication
     */
    public void setAuthentication(com.heb.xmlns.ei.Authentication.Authentication authentication) {
        this.authentication = authentication;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ListSubscriptions_Request)) return false;
        ListSubscriptions_Request other = (ListSubscriptions_Request) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.MOBILE_DIRECTORY_NUMBER==null && other.getMOBILE_DIRECTORY_NUMBER()==null) || 
             (this.MOBILE_DIRECTORY_NUMBER!=null &&
              this.MOBILE_DIRECTORY_NUMBER.equals(other.getMOBILE_DIRECTORY_NUMBER()))) &&
            ((this.PARTNER_ID==null && other.getPARTNER_ID()==null) || 
             (this.PARTNER_ID!=null &&
              this.PARTNER_ID.equals(other.getPARTNER_ID()))) &&
            ((this.authentication==null && other.getAuthentication()==null) || 
             (this.authentication!=null &&
              this.authentication.equals(other.getAuthentication())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMOBILE_DIRECTORY_NUMBER() != null) {
            _hashCode += getMOBILE_DIRECTORY_NUMBER().hashCode();
        }
        if (getPARTNER_ID() != null) {
            _hashCode += getPARTNER_ID().hashCode();
        }
        if (getAuthentication() != null) {
            _hashCode += getAuthentication().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ListSubscriptions_Request.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/ListSubscriptions_Request", ">ListSubscriptions_Request"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MOBILE_DIRECTORY_NUMBER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/ListSubscriptions_Request", "MOBILE_DIRECTORY_NUMBER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PARTNER_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/ListSubscriptions_Request", "PARTNER_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authentication");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Authentication", "Authentication"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Authentication", ">Authentication"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
