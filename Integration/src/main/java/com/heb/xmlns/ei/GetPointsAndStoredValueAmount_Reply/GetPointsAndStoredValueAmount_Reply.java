/**
 * GetPointsAndStoredValueAmount_Reply.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.xmlns.ei.GetPointsAndStoredValueAmount_Reply;

public class GetPointsAndStoredValueAmount_Reply  implements java.io.Serializable {
    private java.lang.String pointsAmount;

    private java.lang.String[] storedValueAmount;

    public GetPointsAndStoredValueAmount_Reply() {
    }

    public GetPointsAndStoredValueAmount_Reply(
           java.lang.String pointsAmount,
           java.lang.String[] storedValueAmount) {
           this.pointsAmount = pointsAmount;
           this.storedValueAmount = storedValueAmount;
    }


    /**
     * Gets the pointsAmount value for this GetPointsAndStoredValueAmount_Reply.
     * 
     * @return pointsAmount
     */
    public java.lang.String getPointsAmount() {
        return pointsAmount;
    }


    /**
     * Sets the pointsAmount value for this GetPointsAndStoredValueAmount_Reply.
     * 
     * @param pointsAmount
     */
    public void setPointsAmount(java.lang.String pointsAmount) {
        this.pointsAmount = pointsAmount;
    }


    /**
     * Gets the storedValueAmount value for this GetPointsAndStoredValueAmount_Reply.
     * 
     * @return storedValueAmount
     */
    public java.lang.String[] getStoredValueAmount() {
        return storedValueAmount;
    }


    /**
     * Sets the storedValueAmount value for this GetPointsAndStoredValueAmount_Reply.
     * 
     * @param storedValueAmount
     */
    public void setStoredValueAmount(java.lang.String[] storedValueAmount) {
        this.storedValueAmount = storedValueAmount;
    }

    public java.lang.String getStoredValueAmount(int i) {
        return this.storedValueAmount[i];
    }

    public void setStoredValueAmount(int i, java.lang.String _value) {
        this.storedValueAmount[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetPointsAndStoredValueAmount_Reply)) return false;
        GetPointsAndStoredValueAmount_Reply other = (GetPointsAndStoredValueAmount_Reply) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.pointsAmount==null && other.getPointsAmount()==null) || 
             (this.pointsAmount!=null &&
              this.pointsAmount.equals(other.getPointsAmount()))) &&
            ((this.storedValueAmount==null && other.getStoredValueAmount()==null) || 
             (this.storedValueAmount!=null &&
              java.util.Arrays.equals(this.storedValueAmount, other.getStoredValueAmount())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPointsAmount() != null) {
            _hashCode += getPointsAmount().hashCode();
        }
        if (getStoredValueAmount() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getStoredValueAmount());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getStoredValueAmount(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetPointsAndStoredValueAmount_Reply.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPointsAndStoredValueAmount_Reply", ">GetPointsAndStoredValueAmount_Reply"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pointsAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPointsAndStoredValueAmount_Reply", "PointsAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("storedValueAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPointsAndStoredValueAmount_Reply", "StoredValueAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPointsAndStoredValueAmount_Reply", "StoredValueAmount"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
