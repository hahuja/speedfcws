/**
 * CUSTOMERCUSTOMER_PHONE.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.xmlns.ei.CUSTOMER;

public class CUSTOMERCUSTOMER_PHONE  implements java.io.Serializable {
    private java.lang.String CUSTOMER_PHONE_ID;

    private java.math.BigInteger CUSTOMER_ID;

    private java.lang.String PHONE_TYPE_CODE;

    private java.lang.String PHONE_NUMBER;

    private java.lang.String CUSTOMER_SOURCE_CD;

    public CUSTOMERCUSTOMER_PHONE() {
    }

    public CUSTOMERCUSTOMER_PHONE(
           java.lang.String CUSTOMER_PHONE_ID,
           java.math.BigInteger CUSTOMER_ID,
           java.lang.String PHONE_TYPE_CODE,
           java.lang.String PHONE_NUMBER,
           java.lang.String CUSTOMER_SOURCE_CD) {
           this.CUSTOMER_PHONE_ID = CUSTOMER_PHONE_ID;
           this.CUSTOMER_ID = CUSTOMER_ID;
           this.PHONE_TYPE_CODE = PHONE_TYPE_CODE;
           this.PHONE_NUMBER = PHONE_NUMBER;
           this.CUSTOMER_SOURCE_CD = CUSTOMER_SOURCE_CD;
    }


    /**
     * Gets the CUSTOMER_PHONE_ID value for this CUSTOMERCUSTOMER_PHONE.
     * 
     * @return CUSTOMER_PHONE_ID
     */
    public java.lang.String getCUSTOMER_PHONE_ID() {
        return CUSTOMER_PHONE_ID;
    }


    /**
     * Sets the CUSTOMER_PHONE_ID value for this CUSTOMERCUSTOMER_PHONE.
     * 
     * @param CUSTOMER_PHONE_ID
     */
    public void setCUSTOMER_PHONE_ID(java.lang.String CUSTOMER_PHONE_ID) {
        this.CUSTOMER_PHONE_ID = CUSTOMER_PHONE_ID;
    }


    /**
     * Gets the CUSTOMER_ID value for this CUSTOMERCUSTOMER_PHONE.
     * 
     * @return CUSTOMER_ID
     */
    public java.math.BigInteger getCUSTOMER_ID() {
        return CUSTOMER_ID;
    }


    /**
     * Sets the CUSTOMER_ID value for this CUSTOMERCUSTOMER_PHONE.
     * 
     * @param CUSTOMER_ID
     */
    public void setCUSTOMER_ID(java.math.BigInteger CUSTOMER_ID) {
        this.CUSTOMER_ID = CUSTOMER_ID;
    }


    /**
     * Gets the PHONE_TYPE_CODE value for this CUSTOMERCUSTOMER_PHONE.
     * 
     * @return PHONE_TYPE_CODE
     */
    public java.lang.String getPHONE_TYPE_CODE() {
        return PHONE_TYPE_CODE;
    }


    /**
     * Sets the PHONE_TYPE_CODE value for this CUSTOMERCUSTOMER_PHONE.
     * 
     * @param PHONE_TYPE_CODE
     */
    public void setPHONE_TYPE_CODE(java.lang.String PHONE_TYPE_CODE) {
        this.PHONE_TYPE_CODE = PHONE_TYPE_CODE;
    }


    /**
     * Gets the PHONE_NUMBER value for this CUSTOMERCUSTOMER_PHONE.
     * 
     * @return PHONE_NUMBER
     */
    public java.lang.String getPHONE_NUMBER() {
        return PHONE_NUMBER;
    }


    /**
     * Sets the PHONE_NUMBER value for this CUSTOMERCUSTOMER_PHONE.
     * 
     * @param PHONE_NUMBER
     */
    public void setPHONE_NUMBER(java.lang.String PHONE_NUMBER) {
        this.PHONE_NUMBER = PHONE_NUMBER;
    }


    /**
     * Gets the CUSTOMER_SOURCE_CD value for this CUSTOMERCUSTOMER_PHONE.
     * 
     * @return CUSTOMER_SOURCE_CD
     */
    public java.lang.String getCUSTOMER_SOURCE_CD() {
        return CUSTOMER_SOURCE_CD;
    }


    /**
     * Sets the CUSTOMER_SOURCE_CD value for this CUSTOMERCUSTOMER_PHONE.
     * 
     * @param CUSTOMER_SOURCE_CD
     */
    public void setCUSTOMER_SOURCE_CD(java.lang.String CUSTOMER_SOURCE_CD) {
        this.CUSTOMER_SOURCE_CD = CUSTOMER_SOURCE_CD;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CUSTOMERCUSTOMER_PHONE)) return false;
        CUSTOMERCUSTOMER_PHONE other = (CUSTOMERCUSTOMER_PHONE) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.CUSTOMER_PHONE_ID==null && other.getCUSTOMER_PHONE_ID()==null) || 
             (this.CUSTOMER_PHONE_ID!=null &&
              this.CUSTOMER_PHONE_ID.equals(other.getCUSTOMER_PHONE_ID()))) &&
            ((this.CUSTOMER_ID==null && other.getCUSTOMER_ID()==null) || 
             (this.CUSTOMER_ID!=null &&
              this.CUSTOMER_ID.equals(other.getCUSTOMER_ID()))) &&
            ((this.PHONE_TYPE_CODE==null && other.getPHONE_TYPE_CODE()==null) || 
             (this.PHONE_TYPE_CODE!=null &&
              this.PHONE_TYPE_CODE.equals(other.getPHONE_TYPE_CODE()))) &&
            ((this.PHONE_NUMBER==null && other.getPHONE_NUMBER()==null) || 
             (this.PHONE_NUMBER!=null &&
              this.PHONE_NUMBER.equals(other.getPHONE_NUMBER()))) &&
            ((this.CUSTOMER_SOURCE_CD==null && other.getCUSTOMER_SOURCE_CD()==null) || 
             (this.CUSTOMER_SOURCE_CD!=null &&
              this.CUSTOMER_SOURCE_CD.equals(other.getCUSTOMER_SOURCE_CD())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCUSTOMER_PHONE_ID() != null) {
            _hashCode += getCUSTOMER_PHONE_ID().hashCode();
        }
        if (getCUSTOMER_ID() != null) {
            _hashCode += getCUSTOMER_ID().hashCode();
        }
        if (getPHONE_TYPE_CODE() != null) {
            _hashCode += getPHONE_TYPE_CODE().hashCode();
        }
        if (getPHONE_NUMBER() != null) {
            _hashCode += getPHONE_NUMBER().hashCode();
        }
        if (getCUSTOMER_SOURCE_CD() != null) {
            _hashCode += getCUSTOMER_SOURCE_CD().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CUSTOMERCUSTOMER_PHONE.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", ">>CUSTOMER>CUSTOMER_PHONE"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CUSTOMER_PHONE_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "CUSTOMER_PHONE_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CUSTOMER_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "CUSTOMER_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PHONE_TYPE_CODE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "PHONE_TYPE_CODE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PHONE_NUMBER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "PHONE_NUMBER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CUSTOMER_SOURCE_CD");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "CUSTOMER_SOURCE_CD"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
