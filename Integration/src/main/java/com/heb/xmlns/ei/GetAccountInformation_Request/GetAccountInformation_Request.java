/**
 * GetAccountInformation_Request.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.xmlns.ei.GetAccountInformation_Request;

public class GetAccountInformation_Request  implements java.io.Serializable {
    private java.lang.String HEBDOTCOM_ID;

    private java.lang.String LOYALTY_ACCOUNT_TYPE;

    private java.lang.String PCR_CARD_NUMBER;

    private com.heb.xmlns.ei.Authentication.Authentication authentication;

    public GetAccountInformation_Request() {
    }

    public GetAccountInformation_Request(
           java.lang.String HEBDOTCOM_ID,
           java.lang.String LOYALTY_ACCOUNT_TYPE,
           java.lang.String PCR_CARD_NUMBER,
           com.heb.xmlns.ei.Authentication.Authentication authentication) {
           this.HEBDOTCOM_ID = HEBDOTCOM_ID;
           this.LOYALTY_ACCOUNT_TYPE = LOYALTY_ACCOUNT_TYPE;
           this.PCR_CARD_NUMBER = PCR_CARD_NUMBER;
           this.authentication = authentication;
    }


    /**
     * Gets the HEBDOTCOM_ID value for this GetAccountInformation_Request.
     * 
     * @return HEBDOTCOM_ID
     */
    public java.lang.String getHEBDOTCOM_ID() {
        return HEBDOTCOM_ID;
    }


    /**
     * Sets the HEBDOTCOM_ID value for this GetAccountInformation_Request.
     * 
     * @param HEBDOTCOM_ID
     */
    public void setHEBDOTCOM_ID(java.lang.String HEBDOTCOM_ID) {
        this.HEBDOTCOM_ID = HEBDOTCOM_ID;
    }


    /**
     * Gets the LOYALTY_ACCOUNT_TYPE value for this GetAccountInformation_Request.
     * 
     * @return LOYALTY_ACCOUNT_TYPE
     */
    public java.lang.String getLOYALTY_ACCOUNT_TYPE() {
        return LOYALTY_ACCOUNT_TYPE;
    }


    /**
     * Sets the LOYALTY_ACCOUNT_TYPE value for this GetAccountInformation_Request.
     * 
     * @param LOYALTY_ACCOUNT_TYPE
     */
    public void setLOYALTY_ACCOUNT_TYPE(java.lang.String LOYALTY_ACCOUNT_TYPE) {
        this.LOYALTY_ACCOUNT_TYPE = LOYALTY_ACCOUNT_TYPE;
    }


    /**
     * Gets the PCR_CARD_NUMBER value for this GetAccountInformation_Request.
     * 
     * @return PCR_CARD_NUMBER
     */
    public java.lang.String getPCR_CARD_NUMBER() {
        return PCR_CARD_NUMBER;
    }


    /**
     * Sets the PCR_CARD_NUMBER value for this GetAccountInformation_Request.
     * 
     * @param PCR_CARD_NUMBER
     */
    public void setPCR_CARD_NUMBER(java.lang.String PCR_CARD_NUMBER) {
        this.PCR_CARD_NUMBER = PCR_CARD_NUMBER;
    }


    /**
     * Gets the authentication value for this GetAccountInformation_Request.
     * 
     * @return authentication
     */
    public com.heb.xmlns.ei.Authentication.Authentication getAuthentication() {
        return authentication;
    }


    /**
     * Sets the authentication value for this GetAccountInformation_Request.
     * 
     * @param authentication
     */
    public void setAuthentication(com.heb.xmlns.ei.Authentication.Authentication authentication) {
        this.authentication = authentication;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetAccountInformation_Request)) return false;
        GetAccountInformation_Request other = (GetAccountInformation_Request) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.HEBDOTCOM_ID==null && other.getHEBDOTCOM_ID()==null) || 
             (this.HEBDOTCOM_ID!=null &&
              this.HEBDOTCOM_ID.equals(other.getHEBDOTCOM_ID()))) &&
            ((this.LOYALTY_ACCOUNT_TYPE==null && other.getLOYALTY_ACCOUNT_TYPE()==null) || 
             (this.LOYALTY_ACCOUNT_TYPE!=null &&
              this.LOYALTY_ACCOUNT_TYPE.equals(other.getLOYALTY_ACCOUNT_TYPE()))) &&
            ((this.PCR_CARD_NUMBER==null && other.getPCR_CARD_NUMBER()==null) || 
             (this.PCR_CARD_NUMBER!=null &&
              this.PCR_CARD_NUMBER.equals(other.getPCR_CARD_NUMBER()))) &&
            ((this.authentication==null && other.getAuthentication()==null) || 
             (this.authentication!=null &&
              this.authentication.equals(other.getAuthentication())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getHEBDOTCOM_ID() != null) {
            _hashCode += getHEBDOTCOM_ID().hashCode();
        }
        if (getLOYALTY_ACCOUNT_TYPE() != null) {
            _hashCode += getLOYALTY_ACCOUNT_TYPE().hashCode();
        }
        if (getPCR_CARD_NUMBER() != null) {
            _hashCode += getPCR_CARD_NUMBER().hashCode();
        }
        if (getAuthentication() != null) {
            _hashCode += getAuthentication().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetAccountInformation_Request.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetAccountInformation_Request", ">GetAccountInformation_Request"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("HEBDOTCOM_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetAccountInformation_Request", "HEBDOTCOM_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LOYALTY_ACCOUNT_TYPE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetAccountInformation_Request", "LOYALTY_ACCOUNT_TYPE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PCR_CARD_NUMBER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetAccountInformation_Request", "PCR_CARD_NUMBER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authentication");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Authentication", "Authentication"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Authentication", ">Authentication"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
