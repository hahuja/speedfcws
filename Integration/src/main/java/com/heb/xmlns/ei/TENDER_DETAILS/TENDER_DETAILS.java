/**
 * TENDER_DETAILS.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.xmlns.ei.TENDER_DETAILS;

public class TENDER_DETAILS  implements java.io.Serializable {
    private java.math.BigDecimal TENDERED_AMOUNT;

    private java.lang.String PAYMENT_TYPE;

    public TENDER_DETAILS() {
    }

    public TENDER_DETAILS(
           java.math.BigDecimal TENDERED_AMOUNT,
           java.lang.String PAYMENT_TYPE) {
           this.TENDERED_AMOUNT = TENDERED_AMOUNT;
           this.PAYMENT_TYPE = PAYMENT_TYPE;
    }


    /**
     * Gets the TENDERED_AMOUNT value for this TENDER_DETAILS.
     * 
     * @return TENDERED_AMOUNT
     */
    public java.math.BigDecimal getTENDERED_AMOUNT() {
        return TENDERED_AMOUNT;
    }


    /**
     * Sets the TENDERED_AMOUNT value for this TENDER_DETAILS.
     * 
     * @param TENDERED_AMOUNT
     */
    public void setTENDERED_AMOUNT(java.math.BigDecimal TENDERED_AMOUNT) {
        this.TENDERED_AMOUNT = TENDERED_AMOUNT;
    }


    /**
     * Gets the PAYMENT_TYPE value for this TENDER_DETAILS.
     * 
     * @return PAYMENT_TYPE
     */
    public java.lang.String getPAYMENT_TYPE() {
        return PAYMENT_TYPE;
    }


    /**
     * Sets the PAYMENT_TYPE value for this TENDER_DETAILS.
     * 
     * @param PAYMENT_TYPE
     */
    public void setPAYMENT_TYPE(java.lang.String PAYMENT_TYPE) {
        this.PAYMENT_TYPE = PAYMENT_TYPE;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TENDER_DETAILS)) return false;
        TENDER_DETAILS other = (TENDER_DETAILS) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.TENDERED_AMOUNT==null && other.getTENDERED_AMOUNT()==null) || 
             (this.TENDERED_AMOUNT!=null &&
              this.TENDERED_AMOUNT.equals(other.getTENDERED_AMOUNT()))) &&
            ((this.PAYMENT_TYPE==null && other.getPAYMENT_TYPE()==null) || 
             (this.PAYMENT_TYPE!=null &&
              this.PAYMENT_TYPE.equals(other.getPAYMENT_TYPE())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTENDERED_AMOUNT() != null) {
            _hashCode += getTENDERED_AMOUNT().hashCode();
        }
        if (getPAYMENT_TYPE() != null) {
            _hashCode += getPAYMENT_TYPE().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TENDER_DETAILS.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/TENDER_DETAILS", ">TENDER_DETAILS"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TENDERED_AMOUNT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/TENDER_DETAILS", "TENDERED_AMOUNT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PAYMENT_TYPE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/TENDER_DETAILS", "PAYMENT_TYPE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
