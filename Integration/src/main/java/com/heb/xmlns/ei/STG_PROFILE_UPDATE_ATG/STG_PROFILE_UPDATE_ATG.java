/**
 * STG_PROFILE_UPDATE_ATG.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.xmlns.ei.STG_PROFILE_UPDATE_ATG;

public class STG_PROFILE_UPDATE_ATG  implements java.io.Serializable {
    private java.lang.String OPT_IN_SW_DCC;

    private java.lang.String OPT_IN_SW_TXT;

    public STG_PROFILE_UPDATE_ATG() {
    }

    public STG_PROFILE_UPDATE_ATG(
           java.lang.String OPT_IN_SW_DCC,
           java.lang.String OPT_IN_SW_TXT) {
           this.OPT_IN_SW_DCC = OPT_IN_SW_DCC;
           this.OPT_IN_SW_TXT = OPT_IN_SW_TXT;
    }


    /**
     * Gets the OPT_IN_SW_DCC value for this STG_PROFILE_UPDATE_ATG.
     * 
     * @return OPT_IN_SW_DCC
     */
    public java.lang.String getOPT_IN_SW_DCC() {
        return OPT_IN_SW_DCC;
    }


    /**
     * Sets the OPT_IN_SW_DCC value for this STG_PROFILE_UPDATE_ATG.
     * 
     * @param OPT_IN_SW_DCC
     */
    public void setOPT_IN_SW_DCC(java.lang.String OPT_IN_SW_DCC) {
        this.OPT_IN_SW_DCC = OPT_IN_SW_DCC;
    }


    /**
     * Gets the OPT_IN_SW_TXT value for this STG_PROFILE_UPDATE_ATG.
     * 
     * @return OPT_IN_SW_TXT
     */
    public java.lang.String getOPT_IN_SW_TXT() {
        return OPT_IN_SW_TXT;
    }


    /**
     * Sets the OPT_IN_SW_TXT value for this STG_PROFILE_UPDATE_ATG.
     * 
     * @param OPT_IN_SW_TXT
     */
    public void setOPT_IN_SW_TXT(java.lang.String OPT_IN_SW_TXT) {
        this.OPT_IN_SW_TXT = OPT_IN_SW_TXT;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof STG_PROFILE_UPDATE_ATG)) return false;
        STG_PROFILE_UPDATE_ATG other = (STG_PROFILE_UPDATE_ATG) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.OPT_IN_SW_DCC==null && other.getOPT_IN_SW_DCC()==null) || 
             (this.OPT_IN_SW_DCC!=null &&
              this.OPT_IN_SW_DCC.equals(other.getOPT_IN_SW_DCC()))) &&
            ((this.OPT_IN_SW_TXT==null && other.getOPT_IN_SW_TXT()==null) || 
             (this.OPT_IN_SW_TXT!=null &&
              this.OPT_IN_SW_TXT.equals(other.getOPT_IN_SW_TXT())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getOPT_IN_SW_DCC() != null) {
            _hashCode += getOPT_IN_SW_DCC().hashCode();
        }
        if (getOPT_IN_SW_TXT() != null) {
            _hashCode += getOPT_IN_SW_TXT().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(STG_PROFILE_UPDATE_ATG.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/STG_PROFILE_UPDATE_ATG", ">STG_PROFILE_UPDATE_ATG"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("OPT_IN_SW_DCC");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/STG_PROFILE_UPDATE_ATG", "OPT_IN_SW_DCC"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("OPT_IN_SW_TXT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/STG_PROFILE_UPDATE_ATG", "OPT_IN_SW_TXT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
