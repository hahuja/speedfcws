/**
 * CustomerAccountServiceBindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.xmlns.ei.CustomerAccountService;

public class CustomerAccountServiceBindingStub extends org.apache.axis.client.Stub implements com.heb.xmlns.ei.CustomerAccountService.CustomerAccountService_PortType {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[11];
        _initOperationDesc1();
        _initOperationDesc2();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getAccountInformation");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetAccountInformation_Request", "GetAccountInformation_Request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetAccountInformation_Request", ">GetAccountInformation_Request"), com.heb.xmlns.ei.GetAccountInformation_Request.GetAccountInformation_Request.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetAccountInformation_Reply", ">GetAccountInformation_Reply"));
        oper.setReturnClass(com.heb.xmlns.ei.GetAccountInformation_Reply.GetAccountInformation_Reply.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetAccountInformation_Reply", "GetAccountInformation_Reply"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProviderFaultSchema", "ProviderSOAPFault"),
                      "com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault",
                      new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProviderFaultSchema", ">ProviderSOAPFault"), 
                      true
                     ));
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getReceiptDetails");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetReceiptDetails_Request", "GetReceiptDetails_Request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetReceiptDetails_Request", ">GetReceiptDetails_Request"), com.heb.xmlns.ei.GetReceiptDetails_Request.GetReceiptDetails_Request.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetReceiptDetails_Reply", ">GetReceiptDetails_Reply"));
        oper.setReturnClass(com.heb.xmlns.ei.GetReceiptDetails_Reply.GetReceiptDetails_Reply.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetReceiptDetails_Reply", "GetReceiptDetails_Reply"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProviderFaultSchema", "ProviderSOAPFault"),
                      "com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault",
                      new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProviderFaultSchema", ">ProviderSOAPFault"), 
                      true
                     ));
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("associateAccount");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/AssociateAccount_Request", "AssociateAccount_Request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://xmlns.heb.com/ei/AssociateAccount_Request", ">AssociateAccount_Request"), com.heb.xmlns.ei.AssociateAccount_Request.AssociateAccount_Request.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/AssociateAccount_Reply", ">AssociateAccount_Reply"));
        oper.setReturnClass(com.heb.xmlns.ei.AssociateAccount_Reply.AssociateAccount_Reply.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/AssociateAccount_Reply", "AssociateAccount_Reply"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProviderFaultSchema", "ProviderSOAPFault"),
                      "com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault",
                      new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProviderFaultSchema", ">ProviderSOAPFault"), 
                      true
                     ));
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("processRetroRequest");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProcessRetroRequest_Request", "ProcessRetroRequest_Request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProcessRetroRequest_Request", ">ProcessRetroRequest_Request"), com.heb.xmlns.ei.ProcessRetroRequest_Request.ProcessRetroRequest_Request.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProcessRetroRequest_Reply", ">ProcessRetroRequest_Reply"));
        oper.setReturnClass(com.heb.xmlns.ei.ProcessRetroRequest_Reply.ProcessRetroRequest_Reply.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProcessRetroRequest_Reply", "ProcessRetroRequest_Reply"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProviderFaultSchema", "ProviderSOAPFault"),
                      "com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault",
                      new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProviderFaultSchema", ">ProviderSOAPFault"), 
                      true
                     ));
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("processCard");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProcessCard_Request", "ProcessCard_Request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProcessCard_Request", ">ProcessCard_Request"), com.heb.xmlns.ei.ProcessCard_Request.ProcessCard_Request.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProcessCard_Reply", ">ProcessCard_Reply"));
        oper.setReturnClass(com.heb.xmlns.ei.ProcessCard_Reply.ProcessCard_Reply.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProcessCard_Reply", "ProcessCard_Reply"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProviderFaultSchema", "ProviderSOAPFault"),
                      "com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault",
                      new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProviderFaultSchema", ">ProviderSOAPFault"), 
                      true
                     ));
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getReasonCodes");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetReasonCodes_Request", "GetReasonCodes_Request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetReasonCodes_Request", ">GetReasonCodes_Request"), com.heb.xmlns.ei.GetReasonCodes_Request.GetReasonCodes_Request.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetReasonCodes_Reply", ">GetReasonCodes_Reply"));
        oper.setReturnClass(com.heb.xmlns.ei.REASON_CODES.REASON_CODES[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetReasonCodes_Reply", "GetReasonCodes_Reply"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProviderFaultSchema", "ProviderSOAPFault"),
                      "com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault",
                      new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProviderFaultSchema", ">ProviderSOAPFault"), 
                      true
                     ));
        _operations[5] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("updateProfile");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/UpdateProfile_Request", "UpdateProfile_Request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://xmlns.heb.com/ei/UpdateProfile_Request", ">UpdateProfile_Request"), com.heb.xmlns.ei.UpdateProfile_Request.UpdateProfile_Request.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/UpdateProfile_Reply", ">UpdateProfile_Reply"));
        oper.setReturnClass(com.heb.xmlns.ei.UpdateProfile_Reply.UpdateProfile_Reply.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/UpdateProfile_Reply", "UpdateProfile_Reply"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProviderFaultSchema", "ProviderSOAPFault"),
                      "com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault",
                      new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProviderFaultSchema", ">ProviderSOAPFault"), 
                      true
                     ));
        _operations[6] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getPin");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPin_Request", "GetPin_Request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPin_Request", ">GetPin_Request"), com.heb.xmlns.ei.GetPin_Request.GetPin_Request.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPin_Reply", ">GetPin_Reply"));
        oper.setReturnClass(com.heb.xmlns.ei.GetPin_Reply.GetPin_Reply.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPin_Reply", "GetPin_Reply"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProviderFaultSchema", "ProviderSOAPFault"),
                      "com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault",
                      new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProviderFaultSchema", ">ProviderSOAPFault"), 
                      true
                     ));
        _operations[7] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getDccMemberAccountInformation");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetDccMemberAccountInformation_Request", "GetDccMemberAccountInformation_Request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetDccMemberAccountInformation_Request", ">GetDccMemberAccountInformation_Request"), com.heb.xmlns.ei.GetDccMemberAccountInformation_Request.GetDccMemberAccountInformation_Request.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetDccMemberAccountInformation_Reply", ">GetDccMemberAccountInformation_Reply"));
        oper.setReturnClass(com.heb.xmlns.ei.GetDccMemberAccountInformation_Reply.GetDccMemberAccountInformation_ReplyDccMemberAccountInformation[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetDccMemberAccountInformation_Reply", "GetDccMemberAccountInformation_Reply"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetDccMemberAccountInformation_Reply", "DccMemberAccountInformation"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProviderFaultSchema", "ProviderSOAPFault"),
                      "com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault",
                      new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProviderFaultSchema", ">ProviderSOAPFault"), 
                      true
                     ));
        _operations[8] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getPointsAndStoredValueAmount");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPointsAndStoredValueAmount_Request", "GetPointsAndStoredValueAmount_Request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPointsAndStoredValueAmount_Request", ">GetPointsAndStoredValueAmount_Request"), com.heb.xmlns.ei.GetPointsAndStoredValueAmount_Request.GetPointsAndStoredValueAmount_Request.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPointsAndStoredValueAmount_Reply", ">GetPointsAndStoredValueAmount_Reply"));
        oper.setReturnClass(com.heb.xmlns.ei.GetPointsAndStoredValueAmount_Reply.GetPointsAndStoredValueAmount_Reply.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPointsAndStoredValueAmount_Reply", "GetPointsAndStoredValueAmount_Reply"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProviderFaultSchema", "ProviderSOAPFault"),
                      "com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault",
                      new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProviderFaultSchema", ">ProviderSOAPFault"), 
                      true
                     ));
        _operations[9] = oper;

    }

    private static void _initOperationDesc2(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getPcrStatementDetails");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPcrStatementDetails_Request", "GetPcrStatementDetails_Request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPcrStatementDetails_Request", ">GetPcrStatementDetails_Request"), com.heb.xmlns.ei.GetPcrStatementDetails_Request.GetPcrStatementDetails_Request.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPcrStatementDetails_Reply", ">PCRCustomerDetails"));
        oper.setReturnClass(com.heb.xmlns.ei.GetPcrStatementDetails_Reply.PCRCustomerDetails.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPcrStatementDetails_Reply", "PCRCustomerDetails"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProviderFaultSchema", "ProviderSOAPFault"),
                      "com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault",
                      new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProviderFaultSchema", ">ProviderSOAPFault"), 
                      true
                     ));
        _operations[10] = oper;

    }

    public CustomerAccountServiceBindingStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public CustomerAccountServiceBindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public CustomerAccountServiceBindingStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/AssociateAccount_Reply", ">AssociateAccount_Reply");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.AssociateAccount_Reply.AssociateAccount_Reply.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/AssociateAccount_Request", ">AssociateAccount_Request");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.AssociateAccount_Request.AssociateAccount_Request.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Authentication", ">Authentication");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.Authentication.Authentication.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CARD_PRINT_STAGING", ">CARD_PRINT_STAGING");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.CARD_PRINT_STAGING.CARD_PRINT_STAGING.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", ">>CUSTOMER>CUSTOMER_ADDRESS");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.CUSTOMER.CUSTOMERCUSTOMER_ADDRESS.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", ">>CUSTOMER>CUSTOMER_EMAIL_ADDRESS");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.CUSTOMER.CUSTOMERCUSTOMER_EMAIL_ADDRESS.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", ">>CUSTOMER>CUSTOMER_PHONE");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.CUSTOMER.CUSTOMERCUSTOMER_PHONE.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", ">CUSTOMER");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.CUSTOMER.CUSTOMER.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetAccountInformation_Reply", ">GetAccountInformation_Reply");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.GetAccountInformation_Reply.GetAccountInformation_Reply.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetAccountInformation_Request", ">GetAccountInformation_Request");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.GetAccountInformation_Request.GetAccountInformation_Request.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetDccMemberAccountInformation_Reply", ">>>GetDccMemberAccountInformation_Reply>DccMemberAccountInformation>Customers");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.GetDccMemberAccountInformation_Reply.GetDccMemberAccountInformation_ReplyDccMemberAccountInformationCustomers.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetDccMemberAccountInformation_Reply", ">>>GetDccMemberAccountInformation_Reply>DccMemberAccountInformation>Offers");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.GetDccMemberAccountInformation_Reply.GetDccMemberAccountInformation_ReplyDccMemberAccountInformationOffers.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetDccMemberAccountInformation_Reply", ">>GetDccMemberAccountInformation_Reply>DccMemberAccountInformation");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.GetDccMemberAccountInformation_Reply.GetDccMemberAccountInformation_ReplyDccMemberAccountInformation.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetDccMemberAccountInformation_Reply", ">GetDccMemberAccountInformation_Reply");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.GetDccMemberAccountInformation_Reply.GetDccMemberAccountInformation_ReplyDccMemberAccountInformation[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetDccMemberAccountInformation_Reply", ">>GetDccMemberAccountInformation_Reply>DccMemberAccountInformation");
            qName2 = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetDccMemberAccountInformation_Reply", "DccMemberAccountInformation");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetDccMemberAccountInformation_Request", ">GetDccMemberAccountInformation_Request");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.GetDccMemberAccountInformation_Request.GetDccMemberAccountInformation_Request.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPcrStatementDetails_Reply", ">>>PCRCustomerDetails>TRANSANCTION_HEADER>TRANSANCTION_DETAILS");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.GetPcrStatementDetails_Reply.PCRCustomerDetailsTRANSANCTION_HEADERTRANSANCTION_DETAILS.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPcrStatementDetails_Reply", ">>PCRCustomerDetails>PCR_CARD_DETAILS");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.GetPcrStatementDetails_Reply.PCRCustomerDetailsPCR_CARD_DETAILS.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPcrStatementDetails_Reply", ">>PCRCustomerDetails>TRANSANCTION_HEADER");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.GetPcrStatementDetails_Reply.PCRCustomerDetailsTRANSANCTION_HEADER.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPcrStatementDetails_Reply", ">PCRCustomerDetails");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.GetPcrStatementDetails_Reply.PCRCustomerDetails.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPcrStatementDetails_Request", ">GetPcrStatementDetails_Request");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.GetPcrStatementDetails_Request.GetPcrStatementDetails_Request.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPin_Reply", ">GetPin_Reply");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.GetPin_Reply.GetPin_Reply.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPin_Request", ">GetPin_Request");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.GetPin_Request.GetPin_Request.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPointsAndStoredValueAmount_Reply", ">GetPointsAndStoredValueAmount_Reply");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.GetPointsAndStoredValueAmount_Reply.GetPointsAndStoredValueAmount_Reply.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPointsAndStoredValueAmount_Request", ">GetPointsAndStoredValueAmount_Request");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.GetPointsAndStoredValueAmount_Request.GetPointsAndStoredValueAmount_Request.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetReasonCodes_Reply", ">GetReasonCodes_Reply");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.REASON_CODES.REASON_CODES[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/REASON_CODES", "REASON_CODES");
            qName2 = null;
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetReasonCodes_Request", ">GetReasonCodes_Request");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.GetReasonCodes_Request.GetReasonCodes_Request.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetReceiptDetails_Reply", ">GetReceiptDetails_Reply");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.GetReceiptDetails_Reply.GetReceiptDetails_Reply.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetReceiptDetails_Request", ">GetReceiptDetails_Request");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.GetReceiptDetails_Request.GetReceiptDetails_Request.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PCR_ACCT_DTL", ">PCR_ACCT_DTL");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.PCR_ACCT_DTL.PCR_ACCT_DTL.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProcessCard_Reply", ">ProcessCard_Reply");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.ProcessCard_Reply.ProcessCard_Reply.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProcessCard_Request", ">ProcessCard_Request");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.ProcessCard_Request.ProcessCard_Request.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProcessRetroRequest_Reply", ">ProcessRetroRequest_Reply");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.ProcessRetroRequest_Reply.ProcessRetroRequest_Reply.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProcessRetroRequest_Request", ">ProcessRetroRequest_Request");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.ProcessRetroRequest_Request.ProcessRetroRequest_Request.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PROGRAM_ACCOUNT", ">PROGRAM_ACCOUNT");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.PROGRAM_ACCOUNT.PROGRAM_ACCOUNT.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PROGRAM_PARTICIPATION", ">>PROGRAM_PARTICIPATION>PARTICIPATION_TYPE");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.PROGRAM_PARTICIPATION.PROGRAM_PARTICIPATIONPARTICIPATION_TYPE.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PROGRAM_PARTICIPATION", ">PROGRAM_PARTICIPATION");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.PROGRAM_PARTICIPATION.PROGRAM_PARTICIPATION.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProviderFaultSchema", ">ProviderSOAPErrorMsg");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPErrorMsg.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProviderFaultSchema", ">ProviderSOAPFault");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/REASON_CODES", ">REASON_CODES");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.REASON_CODES.REASON_CODES.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/RECEIPT_DETAILS", ">RECEIPT_DETAILS");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.RECEIPT_DETAILS.RECEIPT_DETAILS.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/STG_PROFILE_UPDATE_ATG", ">STG_PROFILE_UPDATE_ATG");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.STG_PROFILE_UPDATE_ATG.STG_PROFILE_UPDATE_ATG.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/TENDER_DETAILS", ">TENDER_DETAILS");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.TENDER_DETAILS.TENDER_DETAILS.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/UpdateProfile_Reply", ">UpdateProfile_Reply");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.UpdateProfile_Reply.UpdateProfile_Reply.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/UpdateProfile_Request", ">UpdateProfile_Request");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.UpdateProfile_Request.UpdateProfile_Request.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public com.heb.xmlns.ei.GetAccountInformation_Reply.GetAccountInformation_Reply getAccountInformation(com.heb.xmlns.ei.GetAccountInformation_Request.GetAccountInformation_Request getAccountInformation_Request) throws java.rmi.RemoteException, com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("/CustomerAccountService/getAccountInformation");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getAccountInformation"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getAccountInformation_Request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.heb.xmlns.ei.GetAccountInformation_Reply.GetAccountInformation_Reply) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.heb.xmlns.ei.GetAccountInformation_Reply.GetAccountInformation_Reply) org.apache.axis.utils.JavaUtils.convert(_resp, com.heb.xmlns.ei.GetAccountInformation_Reply.GetAccountInformation_Reply.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault) {
              throw (com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.heb.xmlns.ei.GetReceiptDetails_Reply.GetReceiptDetails_Reply getReceiptDetails(com.heb.xmlns.ei.GetReceiptDetails_Request.GetReceiptDetails_Request getReceiptDetails_Request) throws java.rmi.RemoteException, com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("/CustomerAccountService%20Processes/Starter%20Process/CustomerAccountService.serviceagent//getReceiptDetails");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getReceiptDetails"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getReceiptDetails_Request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.heb.xmlns.ei.GetReceiptDetails_Reply.GetReceiptDetails_Reply) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.heb.xmlns.ei.GetReceiptDetails_Reply.GetReceiptDetails_Reply) org.apache.axis.utils.JavaUtils.convert(_resp, com.heb.xmlns.ei.GetReceiptDetails_Reply.GetReceiptDetails_Reply.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault) {
              throw (com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.heb.xmlns.ei.AssociateAccount_Reply.AssociateAccount_Reply associateAccount(com.heb.xmlns.ei.AssociateAccount_Request.AssociateAccount_Request associateAccount_Request) throws java.rmi.RemoteException, com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("/CustomerAccountService/associateAccount");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "associateAccount"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {associateAccount_Request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.heb.xmlns.ei.AssociateAccount_Reply.AssociateAccount_Reply) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.heb.xmlns.ei.AssociateAccount_Reply.AssociateAccount_Reply) org.apache.axis.utils.JavaUtils.convert(_resp, com.heb.xmlns.ei.AssociateAccount_Reply.AssociateAccount_Reply.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault) {
              throw (com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.heb.xmlns.ei.ProcessRetroRequest_Reply.ProcessRetroRequest_Reply processRetroRequest(com.heb.xmlns.ei.ProcessRetroRequest_Request.ProcessRetroRequest_Request processRetroRequest_Request) throws java.rmi.RemoteException, com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("/CustomerAccountService%20Processes/Starter%20Process/CustomerAccountService.serviceagent//processRetroRequest");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "processRetroRequest"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {processRetroRequest_Request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.heb.xmlns.ei.ProcessRetroRequest_Reply.ProcessRetroRequest_Reply) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.heb.xmlns.ei.ProcessRetroRequest_Reply.ProcessRetroRequest_Reply) org.apache.axis.utils.JavaUtils.convert(_resp, com.heb.xmlns.ei.ProcessRetroRequest_Reply.ProcessRetroRequest_Reply.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault) {
              throw (com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.heb.xmlns.ei.ProcessCard_Reply.ProcessCard_Reply processCard(com.heb.xmlns.ei.ProcessCard_Request.ProcessCard_Request processCard_Request) throws java.rmi.RemoteException, com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("/CustomerAccountService/processCard");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "processCard"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {processCard_Request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.heb.xmlns.ei.ProcessCard_Reply.ProcessCard_Reply) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.heb.xmlns.ei.ProcessCard_Reply.ProcessCard_Reply) org.apache.axis.utils.JavaUtils.convert(_resp, com.heb.xmlns.ei.ProcessCard_Reply.ProcessCard_Reply.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault) {
              throw (com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.heb.xmlns.ei.REASON_CODES.REASON_CODES[] getReasonCodes(com.heb.xmlns.ei.GetReasonCodes_Request.GetReasonCodes_Request getReasonCodes_Request) throws java.rmi.RemoteException, com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("/CustomerAccountService/getReasonCodess");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getReasonCodes"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getReasonCodes_Request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.heb.xmlns.ei.REASON_CODES.REASON_CODES[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.heb.xmlns.ei.REASON_CODES.REASON_CODES[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.heb.xmlns.ei.REASON_CODES.REASON_CODES[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault) {
              throw (com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.heb.xmlns.ei.UpdateProfile_Reply.UpdateProfile_Reply updateProfile(com.heb.xmlns.ei.UpdateProfile_Request.UpdateProfile_Request updateProfile_Request) throws java.rmi.RemoteException, com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[6]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("/CustomerAccountService/updateProfile");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "updateProfile"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {updateProfile_Request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.heb.xmlns.ei.UpdateProfile_Reply.UpdateProfile_Reply) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.heb.xmlns.ei.UpdateProfile_Reply.UpdateProfile_Reply) org.apache.axis.utils.JavaUtils.convert(_resp, com.heb.xmlns.ei.UpdateProfile_Reply.UpdateProfile_Reply.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault) {
              throw (com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.heb.xmlns.ei.GetPin_Reply.GetPin_Reply getPin(com.heb.xmlns.ei.GetPin_Request.GetPin_Request getPin_Request) throws java.rmi.RemoteException, com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[7]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("/CustomerAccountService/getPin");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getPin"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getPin_Request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.heb.xmlns.ei.GetPin_Reply.GetPin_Reply) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.heb.xmlns.ei.GetPin_Reply.GetPin_Reply) org.apache.axis.utils.JavaUtils.convert(_resp, com.heb.xmlns.ei.GetPin_Reply.GetPin_Reply.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault) {
              throw (com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.heb.xmlns.ei.GetDccMemberAccountInformation_Reply.GetDccMemberAccountInformation_ReplyDccMemberAccountInformation[] getDccMemberAccountInformation(com.heb.xmlns.ei.GetDccMemberAccountInformation_Request.GetDccMemberAccountInformation_Request getDccMemberAccountInformation_Request) throws java.rmi.RemoteException, com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[8]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("/CustomerAccountService/getDccMemberAccountInformation");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getDccMemberAccountInformation"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getDccMemberAccountInformation_Request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.heb.xmlns.ei.GetDccMemberAccountInformation_Reply.GetDccMemberAccountInformation_ReplyDccMemberAccountInformation[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.heb.xmlns.ei.GetDccMemberAccountInformation_Reply.GetDccMemberAccountInformation_ReplyDccMemberAccountInformation[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.heb.xmlns.ei.GetDccMemberAccountInformation_Reply.GetDccMemberAccountInformation_ReplyDccMemberAccountInformation[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault) {
              throw (com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.heb.xmlns.ei.GetPointsAndStoredValueAmount_Reply.GetPointsAndStoredValueAmount_Reply getPointsAndStoredValueAmount(com.heb.xmlns.ei.GetPointsAndStoredValueAmount_Request.GetPointsAndStoredValueAmount_Request getPointsAndStoredValueAmount_Request) throws java.rmi.RemoteException, com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[9]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("/CustomerAccountService/getPointsAndStoredValueAmount");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getPointsAndStoredValueAmount"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getPointsAndStoredValueAmount_Request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.heb.xmlns.ei.GetPointsAndStoredValueAmount_Reply.GetPointsAndStoredValueAmount_Reply) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.heb.xmlns.ei.GetPointsAndStoredValueAmount_Reply.GetPointsAndStoredValueAmount_Reply) org.apache.axis.utils.JavaUtils.convert(_resp, com.heb.xmlns.ei.GetPointsAndStoredValueAmount_Reply.GetPointsAndStoredValueAmount_Reply.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault) {
              throw (com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.heb.xmlns.ei.GetPcrStatementDetails_Reply.PCRCustomerDetails getPcrStatementDetails(com.heb.xmlns.ei.GetPcrStatementDetails_Request.GetPcrStatementDetails_Request getPcrStatementDetails_Request) throws java.rmi.RemoteException, com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[10]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("/CustomerAccountService/getPcrStatementDetails");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getPcrStatementDetails"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getPcrStatementDetails_Request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.heb.xmlns.ei.GetPcrStatementDetails_Reply.PCRCustomerDetails) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.heb.xmlns.ei.GetPcrStatementDetails_Reply.PCRCustomerDetails) org.apache.axis.utils.JavaUtils.convert(_resp, com.heb.xmlns.ei.GetPcrStatementDetails_Reply.PCRCustomerDetails.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault) {
              throw (com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

}
