/**
 * ListSubscriptions_Reply.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.xmlns.ei.SubscriptionCenterService.ListSubscriptions_Reply;

public class ListSubscriptions_Reply  implements java.io.Serializable {
    private boolean success;

    private com.heb.xmlns.ei.SubscriptionCenterService.ListSubscriptions_Reply.VerificationStatusCode verification_Status_Code;

    private com.heb.xmlns.ei.SubscriptionCenterService.ListSubscriptions_Reply.ListSubscriptions_ReplyKeywordsKeyword[] keywords;

    public ListSubscriptions_Reply() {
    }

    public ListSubscriptions_Reply(
           boolean success,
           com.heb.xmlns.ei.SubscriptionCenterService.ListSubscriptions_Reply.VerificationStatusCode verification_Status_Code,
           com.heb.xmlns.ei.SubscriptionCenterService.ListSubscriptions_Reply.ListSubscriptions_ReplyKeywordsKeyword[] keywords) {
           this.success = success;
           this.verification_Status_Code = verification_Status_Code;
           this.keywords = keywords;
    }


    /**
     * Gets the success value for this ListSubscriptions_Reply.
     * 
     * @return success
     */
    public boolean isSuccess() {
        return success;
    }


    /**
     * Sets the success value for this ListSubscriptions_Reply.
     * 
     * @param success
     */
    public void setSuccess(boolean success) {
        this.success = success;
    }


    /**
     * Gets the verification_Status_Code value for this ListSubscriptions_Reply.
     * 
     * @return verification_Status_Code
     */
    public com.heb.xmlns.ei.SubscriptionCenterService.ListSubscriptions_Reply.VerificationStatusCode getVerification_Status_Code() {
        return verification_Status_Code;
    }


    /**
     * Sets the verification_Status_Code value for this ListSubscriptions_Reply.
     * 
     * @param verification_Status_Code
     */
    public void setVerification_Status_Code(com.heb.xmlns.ei.SubscriptionCenterService.ListSubscriptions_Reply.VerificationStatusCode verification_Status_Code) {
        this.verification_Status_Code = verification_Status_Code;
    }


    /**
     * Gets the keywords value for this ListSubscriptions_Reply.
     * 
     * @return keywords
     */
    public com.heb.xmlns.ei.SubscriptionCenterService.ListSubscriptions_Reply.ListSubscriptions_ReplyKeywordsKeyword[] getKeywords() {
        return keywords;
    }


    /**
     * Sets the keywords value for this ListSubscriptions_Reply.
     * 
     * @param keywords
     */
    public void setKeywords(com.heb.xmlns.ei.SubscriptionCenterService.ListSubscriptions_Reply.ListSubscriptions_ReplyKeywordsKeyword[] keywords) {
        this.keywords = keywords;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ListSubscriptions_Reply)) return false;
        ListSubscriptions_Reply other = (ListSubscriptions_Reply) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.success == other.isSuccess() &&
            ((this.verification_Status_Code==null && other.getVerification_Status_Code()==null) || 
             (this.verification_Status_Code!=null &&
              this.verification_Status_Code.equals(other.getVerification_Status_Code()))) &&
            ((this.keywords==null && other.getKeywords()==null) || 
             (this.keywords!=null &&
              java.util.Arrays.equals(this.keywords, other.getKeywords())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += (isSuccess() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getVerification_Status_Code() != null) {
            _hashCode += getVerification_Status_Code().hashCode();
        }
        if (getKeywords() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getKeywords());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getKeywords(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ListSubscriptions_Reply.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/ListSubscriptions_Reply", ">ListSubscriptions_Reply"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("success");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/ListSubscriptions_Reply", "Success"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("verification_Status_Code");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/ListSubscriptions_Reply", "Verification_Status_Code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/ListSubscriptions_Reply", "VerificationStatusCode"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("keywords");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/ListSubscriptions_Reply", "Keywords"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/ListSubscriptions_Reply", ">>>ListSubscriptions_Reply>Keywords>Keyword"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/ListSubscriptions_Reply", "Keyword"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
