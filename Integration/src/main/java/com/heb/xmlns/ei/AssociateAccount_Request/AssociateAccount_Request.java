/**
 * AssociateAccount_Request.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.xmlns.ei.AssociateAccount_Request;

public class AssociateAccount_Request  implements java.io.Serializable {
    private java.lang.String HEBDOTCOM_ID;

    private java.lang.String ACCOUNT_PIN;

    private java.math.BigInteger CARD_NUMBER;

    private java.lang.String ACCOUNT_TYPE;

    private java.lang.String EMAIL_ADDRESS;

    private com.heb.xmlns.ei.Authentication.Authentication authentication;

    public AssociateAccount_Request() {
    }

    public AssociateAccount_Request(
           java.lang.String HEBDOTCOM_ID,
           java.lang.String ACCOUNT_PIN,
           java.math.BigInteger CARD_NUMBER,
           java.lang.String ACCOUNT_TYPE,
           java.lang.String EMAIL_ADDRESS,
           com.heb.xmlns.ei.Authentication.Authentication authentication) {
           this.HEBDOTCOM_ID = HEBDOTCOM_ID;
           this.ACCOUNT_PIN = ACCOUNT_PIN;
           this.CARD_NUMBER = CARD_NUMBER;
           this.ACCOUNT_TYPE = ACCOUNT_TYPE;
           this.EMAIL_ADDRESS = EMAIL_ADDRESS;
           this.authentication = authentication;
    }


    /**
     * Gets the HEBDOTCOM_ID value for this AssociateAccount_Request.
     * 
     * @return HEBDOTCOM_ID
     */
    public java.lang.String getHEBDOTCOM_ID() {
        return HEBDOTCOM_ID;
    }


    /**
     * Sets the HEBDOTCOM_ID value for this AssociateAccount_Request.
     * 
     * @param HEBDOTCOM_ID
     */
    public void setHEBDOTCOM_ID(java.lang.String HEBDOTCOM_ID) {
        this.HEBDOTCOM_ID = HEBDOTCOM_ID;
    }


    /**
     * Gets the ACCOUNT_PIN value for this AssociateAccount_Request.
     * 
     * @return ACCOUNT_PIN
     */
    public java.lang.String getACCOUNT_PIN() {
        return ACCOUNT_PIN;
    }


    /**
     * Sets the ACCOUNT_PIN value for this AssociateAccount_Request.
     * 
     * @param ACCOUNT_PIN
     */
    public void setACCOUNT_PIN(java.lang.String ACCOUNT_PIN) {
        this.ACCOUNT_PIN = ACCOUNT_PIN;
    }


    /**
     * Gets the CARD_NUMBER value for this AssociateAccount_Request.
     * 
     * @return CARD_NUMBER
     */
    public java.math.BigInteger getCARD_NUMBER() {
        return CARD_NUMBER;
    }


    /**
     * Sets the CARD_NUMBER value for this AssociateAccount_Request.
     * 
     * @param CARD_NUMBER
     */
    public void setCARD_NUMBER(java.math.BigInteger CARD_NUMBER) {
        this.CARD_NUMBER = CARD_NUMBER;
    }


    /**
     * Gets the ACCOUNT_TYPE value for this AssociateAccount_Request.
     * 
     * @return ACCOUNT_TYPE
     */
    public java.lang.String getACCOUNT_TYPE() {
        return ACCOUNT_TYPE;
    }


    /**
     * Sets the ACCOUNT_TYPE value for this AssociateAccount_Request.
     * 
     * @param ACCOUNT_TYPE
     */
    public void setACCOUNT_TYPE(java.lang.String ACCOUNT_TYPE) {
        this.ACCOUNT_TYPE = ACCOUNT_TYPE;
    }


    /**
     * Gets the EMAIL_ADDRESS value for this AssociateAccount_Request.
     * 
     * @return EMAIL_ADDRESS
     */
    public java.lang.String getEMAIL_ADDRESS() {
        return EMAIL_ADDRESS;
    }


    /**
     * Sets the EMAIL_ADDRESS value for this AssociateAccount_Request.
     * 
     * @param EMAIL_ADDRESS
     */
    public void setEMAIL_ADDRESS(java.lang.String EMAIL_ADDRESS) {
        this.EMAIL_ADDRESS = EMAIL_ADDRESS;
    }


    /**
     * Gets the authentication value for this AssociateAccount_Request.
     * 
     * @return authentication
     */
    public com.heb.xmlns.ei.Authentication.Authentication getAuthentication() {
        return authentication;
    }


    /**
     * Sets the authentication value for this AssociateAccount_Request.
     * 
     * @param authentication
     */
    public void setAuthentication(com.heb.xmlns.ei.Authentication.Authentication authentication) {
        this.authentication = authentication;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AssociateAccount_Request)) return false;
        AssociateAccount_Request other = (AssociateAccount_Request) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.HEBDOTCOM_ID==null && other.getHEBDOTCOM_ID()==null) || 
             (this.HEBDOTCOM_ID!=null &&
              this.HEBDOTCOM_ID.equals(other.getHEBDOTCOM_ID()))) &&
            ((this.ACCOUNT_PIN==null && other.getACCOUNT_PIN()==null) || 
             (this.ACCOUNT_PIN!=null &&
              this.ACCOUNT_PIN.equals(other.getACCOUNT_PIN()))) &&
            ((this.CARD_NUMBER==null && other.getCARD_NUMBER()==null) || 
             (this.CARD_NUMBER!=null &&
              this.CARD_NUMBER.equals(other.getCARD_NUMBER()))) &&
            ((this.ACCOUNT_TYPE==null && other.getACCOUNT_TYPE()==null) || 
             (this.ACCOUNT_TYPE!=null &&
              this.ACCOUNT_TYPE.equals(other.getACCOUNT_TYPE()))) &&
            ((this.EMAIL_ADDRESS==null && other.getEMAIL_ADDRESS()==null) || 
             (this.EMAIL_ADDRESS!=null &&
              this.EMAIL_ADDRESS.equals(other.getEMAIL_ADDRESS()))) &&
            ((this.authentication==null && other.getAuthentication()==null) || 
             (this.authentication!=null &&
              this.authentication.equals(other.getAuthentication())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getHEBDOTCOM_ID() != null) {
            _hashCode += getHEBDOTCOM_ID().hashCode();
        }
        if (getACCOUNT_PIN() != null) {
            _hashCode += getACCOUNT_PIN().hashCode();
        }
        if (getCARD_NUMBER() != null) {
            _hashCode += getCARD_NUMBER().hashCode();
        }
        if (getACCOUNT_TYPE() != null) {
            _hashCode += getACCOUNT_TYPE().hashCode();
        }
        if (getEMAIL_ADDRESS() != null) {
            _hashCode += getEMAIL_ADDRESS().hashCode();
        }
        if (getAuthentication() != null) {
            _hashCode += getAuthentication().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AssociateAccount_Request.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/AssociateAccount_Request", ">AssociateAccount_Request"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("HEBDOTCOM_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/AssociateAccount_Request", "HEBDOTCOM_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ACCOUNT_PIN");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/AssociateAccount_Request", "ACCOUNT_PIN"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CARD_NUMBER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/AssociateAccount_Request", "CARD_NUMBER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ACCOUNT_TYPE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/AssociateAccount_Request", "ACCOUNT_TYPE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("EMAIL_ADDRESS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/AssociateAccount_Request", "EMAIL_ADDRESS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authentication");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Authentication", "Authentication"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Authentication", ">Authentication"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
