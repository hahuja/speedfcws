/**
 * UpdateProfile_Reply.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.xmlns.ei.UpdateProfile_Reply;

public class UpdateProfile_Reply  implements java.io.Serializable {
    private java.lang.Boolean RESPONSE_STATUS;

    public UpdateProfile_Reply() {
    }

    public UpdateProfile_Reply(
           java.lang.Boolean RESPONSE_STATUS) {
           this.RESPONSE_STATUS = RESPONSE_STATUS;
    }


    /**
     * Gets the RESPONSE_STATUS value for this UpdateProfile_Reply.
     * 
     * @return RESPONSE_STATUS
     */
    public java.lang.Boolean getRESPONSE_STATUS() {
        return RESPONSE_STATUS;
    }


    /**
     * Sets the RESPONSE_STATUS value for this UpdateProfile_Reply.
     * 
     * @param RESPONSE_STATUS
     */
    public void setRESPONSE_STATUS(java.lang.Boolean RESPONSE_STATUS) {
        this.RESPONSE_STATUS = RESPONSE_STATUS;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UpdateProfile_Reply)) return false;
        UpdateProfile_Reply other = (UpdateProfile_Reply) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.RESPONSE_STATUS==null && other.getRESPONSE_STATUS()==null) || 
             (this.RESPONSE_STATUS!=null &&
              this.RESPONSE_STATUS.equals(other.getRESPONSE_STATUS())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getRESPONSE_STATUS() != null) {
            _hashCode += getRESPONSE_STATUS().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(UpdateProfile_Reply.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/UpdateProfile_Reply", ">UpdateProfile_Reply"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("RESPONSE_STATUS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/UpdateProfile_Reply", "RESPONSE_STATUS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
