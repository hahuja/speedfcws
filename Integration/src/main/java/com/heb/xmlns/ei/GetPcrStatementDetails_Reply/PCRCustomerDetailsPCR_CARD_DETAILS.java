/**
 * PCRCustomerDetailsPCR_CARD_DETAILS.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.xmlns.ei.GetPcrStatementDetails_Reply;

public class PCRCustomerDetailsPCR_CARD_DETAILS  implements java.io.Serializable {
    private java.lang.String LAST_UPDATE;

    private java.lang.String PROGRAM_NAME;

    private java.math.BigDecimal CARD_NUMBER;

    private java.math.BigInteger HOUSEHOLD_ID;

    private java.math.BigDecimal ADJUSTMENTS_POINTS;

    public PCRCustomerDetailsPCR_CARD_DETAILS() {
    }

    public PCRCustomerDetailsPCR_CARD_DETAILS(
           java.lang.String LAST_UPDATE,
           java.lang.String PROGRAM_NAME,
           java.math.BigDecimal CARD_NUMBER,
           java.math.BigInteger HOUSEHOLD_ID,
           java.math.BigDecimal ADJUSTMENTS_POINTS) {
           this.LAST_UPDATE = LAST_UPDATE;
           this.PROGRAM_NAME = PROGRAM_NAME;
           this.CARD_NUMBER = CARD_NUMBER;
           this.HOUSEHOLD_ID = HOUSEHOLD_ID;
           this.ADJUSTMENTS_POINTS = ADJUSTMENTS_POINTS;
    }


    /**
     * Gets the LAST_UPDATE value for this PCRCustomerDetailsPCR_CARD_DETAILS.
     * 
     * @return LAST_UPDATE
     */
    public java.lang.String getLAST_UPDATE() {
        return LAST_UPDATE;
    }


    /**
     * Sets the LAST_UPDATE value for this PCRCustomerDetailsPCR_CARD_DETAILS.
     * 
     * @param LAST_UPDATE
     */
    public void setLAST_UPDATE(java.lang.String LAST_UPDATE) {
        this.LAST_UPDATE = LAST_UPDATE;
    }


    /**
     * Gets the PROGRAM_NAME value for this PCRCustomerDetailsPCR_CARD_DETAILS.
     * 
     * @return PROGRAM_NAME
     */
    public java.lang.String getPROGRAM_NAME() {
        return PROGRAM_NAME;
    }


    /**
     * Sets the PROGRAM_NAME value for this PCRCustomerDetailsPCR_CARD_DETAILS.
     * 
     * @param PROGRAM_NAME
     */
    public void setPROGRAM_NAME(java.lang.String PROGRAM_NAME) {
        this.PROGRAM_NAME = PROGRAM_NAME;
    }


    /**
     * Gets the CARD_NUMBER value for this PCRCustomerDetailsPCR_CARD_DETAILS.
     * 
     * @return CARD_NUMBER
     */
    public java.math.BigDecimal getCARD_NUMBER() {
        return CARD_NUMBER;
    }


    /**
     * Sets the CARD_NUMBER value for this PCRCustomerDetailsPCR_CARD_DETAILS.
     * 
     * @param CARD_NUMBER
     */
    public void setCARD_NUMBER(java.math.BigDecimal CARD_NUMBER) {
        this.CARD_NUMBER = CARD_NUMBER;
    }


    /**
     * Gets the HOUSEHOLD_ID value for this PCRCustomerDetailsPCR_CARD_DETAILS.
     * 
     * @return HOUSEHOLD_ID
     */
    public java.math.BigInteger getHOUSEHOLD_ID() {
        return HOUSEHOLD_ID;
    }


    /**
     * Sets the HOUSEHOLD_ID value for this PCRCustomerDetailsPCR_CARD_DETAILS.
     * 
     * @param HOUSEHOLD_ID
     */
    public void setHOUSEHOLD_ID(java.math.BigInteger HOUSEHOLD_ID) {
        this.HOUSEHOLD_ID = HOUSEHOLD_ID;
    }


    /**
     * Gets the ADJUSTMENTS_POINTS value for this PCRCustomerDetailsPCR_CARD_DETAILS.
     * 
     * @return ADJUSTMENTS_POINTS
     */
    public java.math.BigDecimal getADJUSTMENTS_POINTS() {
        return ADJUSTMENTS_POINTS;
    }


    /**
     * Sets the ADJUSTMENTS_POINTS value for this PCRCustomerDetailsPCR_CARD_DETAILS.
     * 
     * @param ADJUSTMENTS_POINTS
     */
    public void setADJUSTMENTS_POINTS(java.math.BigDecimal ADJUSTMENTS_POINTS) {
        this.ADJUSTMENTS_POINTS = ADJUSTMENTS_POINTS;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PCRCustomerDetailsPCR_CARD_DETAILS)) return false;
        PCRCustomerDetailsPCR_CARD_DETAILS other = (PCRCustomerDetailsPCR_CARD_DETAILS) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.LAST_UPDATE==null && other.getLAST_UPDATE()==null) || 
             (this.LAST_UPDATE!=null &&
              this.LAST_UPDATE.equals(other.getLAST_UPDATE()))) &&
            ((this.PROGRAM_NAME==null && other.getPROGRAM_NAME()==null) || 
             (this.PROGRAM_NAME!=null &&
              this.PROGRAM_NAME.equals(other.getPROGRAM_NAME()))) &&
            ((this.CARD_NUMBER==null && other.getCARD_NUMBER()==null) || 
             (this.CARD_NUMBER!=null &&
              this.CARD_NUMBER.equals(other.getCARD_NUMBER()))) &&
            ((this.HOUSEHOLD_ID==null && other.getHOUSEHOLD_ID()==null) || 
             (this.HOUSEHOLD_ID!=null &&
              this.HOUSEHOLD_ID.equals(other.getHOUSEHOLD_ID()))) &&
            ((this.ADJUSTMENTS_POINTS==null && other.getADJUSTMENTS_POINTS()==null) || 
             (this.ADJUSTMENTS_POINTS!=null &&
              this.ADJUSTMENTS_POINTS.equals(other.getADJUSTMENTS_POINTS())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getLAST_UPDATE() != null) {
            _hashCode += getLAST_UPDATE().hashCode();
        }
        if (getPROGRAM_NAME() != null) {
            _hashCode += getPROGRAM_NAME().hashCode();
        }
        if (getCARD_NUMBER() != null) {
            _hashCode += getCARD_NUMBER().hashCode();
        }
        if (getHOUSEHOLD_ID() != null) {
            _hashCode += getHOUSEHOLD_ID().hashCode();
        }
        if (getADJUSTMENTS_POINTS() != null) {
            _hashCode += getADJUSTMENTS_POINTS().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PCRCustomerDetailsPCR_CARD_DETAILS.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPcrStatementDetails_Reply", ">>PCRCustomerDetails>PCR_CARD_DETAILS"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LAST_UPDATE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPcrStatementDetails_Reply", "LAST_UPDATE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PROGRAM_NAME");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPcrStatementDetails_Reply", "PROGRAM_NAME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CARD_NUMBER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPcrStatementDetails_Reply", "CARD_NUMBER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("HOUSEHOLD_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPcrStatementDetails_Reply", "HOUSEHOLD_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ADJUSTMENTS_POINTS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPcrStatementDetails_Reply", "ADJUSTMENTS_POINTS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
