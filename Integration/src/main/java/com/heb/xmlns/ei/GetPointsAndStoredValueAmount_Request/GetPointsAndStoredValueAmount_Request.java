/**
 * GetPointsAndStoredValueAmount_Request.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.xmlns.ei.GetPointsAndStoredValueAmount_Request;

public class GetPointsAndStoredValueAmount_Request  implements java.io.Serializable {
    private java.lang.String altId;

    private java.lang.String txtMsg;

    private com.heb.xmlns.ei.Authentication.Authentication authentication;

    public GetPointsAndStoredValueAmount_Request() {
    }

    public GetPointsAndStoredValueAmount_Request(
           java.lang.String altId,
           java.lang.String txtMsg,
           com.heb.xmlns.ei.Authentication.Authentication authentication) {
           this.altId = altId;
           this.txtMsg = txtMsg;
           this.authentication = authentication;
    }


    /**
     * Gets the altId value for this GetPointsAndStoredValueAmount_Request.
     * 
     * @return altId
     */
    public java.lang.String getAltId() {
        return altId;
    }


    /**
     * Sets the altId value for this GetPointsAndStoredValueAmount_Request.
     * 
     * @param altId
     */
    public void setAltId(java.lang.String altId) {
        this.altId = altId;
    }


    /**
     * Gets the txtMsg value for this GetPointsAndStoredValueAmount_Request.
     * 
     * @return txtMsg
     */
    public java.lang.String getTxtMsg() {
        return txtMsg;
    }


    /**
     * Sets the txtMsg value for this GetPointsAndStoredValueAmount_Request.
     * 
     * @param txtMsg
     */
    public void setTxtMsg(java.lang.String txtMsg) {
        this.txtMsg = txtMsg;
    }


    /**
     * Gets the authentication value for this GetPointsAndStoredValueAmount_Request.
     * 
     * @return authentication
     */
    public com.heb.xmlns.ei.Authentication.Authentication getAuthentication() {
        return authentication;
    }


    /**
     * Sets the authentication value for this GetPointsAndStoredValueAmount_Request.
     * 
     * @param authentication
     */
    public void setAuthentication(com.heb.xmlns.ei.Authentication.Authentication authentication) {
        this.authentication = authentication;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetPointsAndStoredValueAmount_Request)) return false;
        GetPointsAndStoredValueAmount_Request other = (GetPointsAndStoredValueAmount_Request) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.altId==null && other.getAltId()==null) || 
             (this.altId!=null &&
              this.altId.equals(other.getAltId()))) &&
            ((this.txtMsg==null && other.getTxtMsg()==null) || 
             (this.txtMsg!=null &&
              this.txtMsg.equals(other.getTxtMsg()))) &&
            ((this.authentication==null && other.getAuthentication()==null) || 
             (this.authentication!=null &&
              this.authentication.equals(other.getAuthentication())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAltId() != null) {
            _hashCode += getAltId().hashCode();
        }
        if (getTxtMsg() != null) {
            _hashCode += getTxtMsg().hashCode();
        }
        if (getAuthentication() != null) {
            _hashCode += getAuthentication().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetPointsAndStoredValueAmount_Request.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPointsAndStoredValueAmount_Request", ">GetPointsAndStoredValueAmount_Request"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("altId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPointsAndStoredValueAmount_Request", "AltId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("txtMsg");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPointsAndStoredValueAmount_Request", "TxtMsg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authentication");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Authentication", "Authentication"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Authentication", ">Authentication"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
