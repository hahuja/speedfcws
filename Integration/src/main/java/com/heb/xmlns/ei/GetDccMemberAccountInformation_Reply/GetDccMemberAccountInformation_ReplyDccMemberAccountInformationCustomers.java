/**
 * GetDccMemberAccountInformation_ReplyDccMemberAccountInformationCustomers.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.xmlns.ei.GetDccMemberAccountInformation_Reply;

public class GetDccMemberAccountInformation_ReplyDccMemberAccountInformationCustomers  implements java.io.Serializable {
    private java.lang.String houseHoldId;

    public GetDccMemberAccountInformation_ReplyDccMemberAccountInformationCustomers() {
    }

    public GetDccMemberAccountInformation_ReplyDccMemberAccountInformationCustomers(
           java.lang.String houseHoldId) {
           this.houseHoldId = houseHoldId;
    }


    /**
     * Gets the houseHoldId value for this GetDccMemberAccountInformation_ReplyDccMemberAccountInformationCustomers.
     * 
     * @return houseHoldId
     */
    public java.lang.String getHouseHoldId() {
        return houseHoldId;
    }


    /**
     * Sets the houseHoldId value for this GetDccMemberAccountInformation_ReplyDccMemberAccountInformationCustomers.
     * 
     * @param houseHoldId
     */
    public void setHouseHoldId(java.lang.String houseHoldId) {
        this.houseHoldId = houseHoldId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetDccMemberAccountInformation_ReplyDccMemberAccountInformationCustomers)) return false;
        GetDccMemberAccountInformation_ReplyDccMemberAccountInformationCustomers other = (GetDccMemberAccountInformation_ReplyDccMemberAccountInformationCustomers) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.houseHoldId==null && other.getHouseHoldId()==null) || 
             (this.houseHoldId!=null &&
              this.houseHoldId.equals(other.getHouseHoldId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getHouseHoldId() != null) {
            _hashCode += getHouseHoldId().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetDccMemberAccountInformation_ReplyDccMemberAccountInformationCustomers.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetDccMemberAccountInformation_Reply", ">>>GetDccMemberAccountInformation_Reply>DccMemberAccountInformation>Customers"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("houseHoldId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetDccMemberAccountInformation_Reply", "HouseHoldId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
