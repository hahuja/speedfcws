/**
 * SubscriptionCenterService_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.xmlns.ei.SubscriptionCenterService;

public interface SubscriptionCenterService_PortType extends java.rmi.Remote {
    public com.heb.xmlns.ei.SubscriptionCenterService.GetSubscriptionStatus_Reply.GetSubscriptionStatus_Reply getSubscriptionStatus(com.heb.xmlns.ei.SubscriptionCenterService.GetSubscriptionStatus_Request.GetSubscriptionStatus_Request getSubscriptionStatus_Request) throws java.rmi.RemoteException, com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault;
    public com.heb.xmlns.ei.SubscriptionCenterService.Subscribe_Reply.Subscribe_Reply subscribe(com.heb.xmlns.ei.SubscriptionCenterService.Subscribe_Request.Subscribe_Request subscribe_Request) throws java.rmi.RemoteException, com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault;
    public com.heb.xmlns.ei.SubscriptionCenterService.ListSubscriptions_Reply.ListSubscriptions_Reply listSubscriptions(com.heb.xmlns.ei.SubscriptionCenterService.ListSubscriptions_Request.ListSubscriptions_Request listSubscriptions_Request) throws java.rmi.RemoteException, com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault;
    public com.heb.xmlns.ei.SubscriptionCenterService.VerifyMobileDirectoryNumber_Reply.VerifyMobileDirectoryNumber_Reply verifyMobileDirectoryNumber(com.heb.xmlns.ei.SubscriptionCenterService.VerifyMobileDirectoryNumber_Request.VerifyMobileDirectoryNumber_Request verifyMobileDirectoryNumber_Request) throws java.rmi.RemoteException, com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault;
    public com.heb.xmlns.ei.SubscriptionCenterService.ModifySubscriptions_Reply.ModifySubscriptions_Reply modifySubscriptions(com.heb.xmlns.ei.SubscriptionCenterService.ModifySubscriptions_Request.ModifySubscriptions_Request modifySubscriptions_Request) throws java.rmi.RemoteException, com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault;
    public com.heb.xmlns.ei.SubscriptionCenterService.Unsubscribe_Reply.Unsubscribe_Reply unsubscribe(com.heb.xmlns.ei.SubscriptionCenterService.Unsubscribe_Request.Unsubscribe_Request unsubscribe_Request) throws java.rmi.RemoteException, com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault;
    public com.heb.xmlns.ei.Salt3.SendMessageResponse sendAlert(com.heb.xmlns.ei.Salt3.SendMessageRequest sendAlert_Request) throws java.rmi.RemoteException, com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault;
}
