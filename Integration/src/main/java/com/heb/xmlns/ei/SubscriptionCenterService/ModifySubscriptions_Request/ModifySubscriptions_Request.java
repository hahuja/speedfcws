/**
 * ModifySubscriptions_Request.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.xmlns.ei.SubscriptionCenterService.ModifySubscriptions_Request;

public class ModifySubscriptions_Request  implements java.io.Serializable {
    private java.lang.String MOBILE_DIRECTORY_NUMBER;

    private java.lang.Integer PARTNER_ID;

    private java.lang.String SOURCE_CODE;

    private java.lang.String[] sub_Keywords;

    private java.lang.String[] unsub_Keywords;

    private com.heb.xmlns.ei.Authentication.Authentication authentication;

    public ModifySubscriptions_Request() {
    }

    public ModifySubscriptions_Request(
           java.lang.String MOBILE_DIRECTORY_NUMBER,
           java.lang.Integer PARTNER_ID,
           java.lang.String SOURCE_CODE,
           java.lang.String[] sub_Keywords,
           java.lang.String[] unsub_Keywords,
           com.heb.xmlns.ei.Authentication.Authentication authentication) {
           this.MOBILE_DIRECTORY_NUMBER = MOBILE_DIRECTORY_NUMBER;
           this.PARTNER_ID = PARTNER_ID;
           this.SOURCE_CODE = SOURCE_CODE;
           this.sub_Keywords = sub_Keywords;
           this.unsub_Keywords = unsub_Keywords;
           this.authentication = authentication;
    }


    /**
     * Gets the MOBILE_DIRECTORY_NUMBER value for this ModifySubscriptions_Request.
     * 
     * @return MOBILE_DIRECTORY_NUMBER
     */
    public java.lang.String getMOBILE_DIRECTORY_NUMBER() {
        return MOBILE_DIRECTORY_NUMBER;
    }


    /**
     * Sets the MOBILE_DIRECTORY_NUMBER value for this ModifySubscriptions_Request.
     * 
     * @param MOBILE_DIRECTORY_NUMBER
     */
    public void setMOBILE_DIRECTORY_NUMBER(java.lang.String MOBILE_DIRECTORY_NUMBER) {
        this.MOBILE_DIRECTORY_NUMBER = MOBILE_DIRECTORY_NUMBER;
    }


    /**
     * Gets the PARTNER_ID value for this ModifySubscriptions_Request.
     * 
     * @return PARTNER_ID
     */
    public java.lang.Integer getPARTNER_ID() {
        return PARTNER_ID;
    }


    /**
     * Sets the PARTNER_ID value for this ModifySubscriptions_Request.
     * 
     * @param PARTNER_ID
     */
    public void setPARTNER_ID(java.lang.Integer PARTNER_ID) {
        this.PARTNER_ID = PARTNER_ID;
    }


    /**
     * Gets the SOURCE_CODE value for this ModifySubscriptions_Request.
     * 
     * @return SOURCE_CODE
     */
    public java.lang.String getSOURCE_CODE() {
        return SOURCE_CODE;
    }


    /**
     * Sets the SOURCE_CODE value for this ModifySubscriptions_Request.
     * 
     * @param SOURCE_CODE
     */
    public void setSOURCE_CODE(java.lang.String SOURCE_CODE) {
        this.SOURCE_CODE = SOURCE_CODE;
    }


    /**
     * Gets the sub_Keywords value for this ModifySubscriptions_Request.
     * 
     * @return sub_Keywords
     */
    public java.lang.String[] getSub_Keywords() {
        return sub_Keywords;
    }


    /**
     * Sets the sub_Keywords value for this ModifySubscriptions_Request.
     * 
     * @param sub_Keywords
     */
    public void setSub_Keywords(java.lang.String[] sub_Keywords) {
        this.sub_Keywords = sub_Keywords;
    }


    /**
     * Gets the unsub_Keywords value for this ModifySubscriptions_Request.
     * 
     * @return unsub_Keywords
     */
    public java.lang.String[] getUnsub_Keywords() {
        return unsub_Keywords;
    }


    /**
     * Sets the unsub_Keywords value for this ModifySubscriptions_Request.
     * 
     * @param unsub_Keywords
     */
    public void setUnsub_Keywords(java.lang.String[] unsub_Keywords) {
        this.unsub_Keywords = unsub_Keywords;
    }


    /**
     * Gets the authentication value for this ModifySubscriptions_Request.
     * 
     * @return authentication
     */
    public com.heb.xmlns.ei.Authentication.Authentication getAuthentication() {
        return authentication;
    }


    /**
     * Sets the authentication value for this ModifySubscriptions_Request.
     * 
     * @param authentication
     */
    public void setAuthentication(com.heb.xmlns.ei.Authentication.Authentication authentication) {
        this.authentication = authentication;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ModifySubscriptions_Request)) return false;
        ModifySubscriptions_Request other = (ModifySubscriptions_Request) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.MOBILE_DIRECTORY_NUMBER==null && other.getMOBILE_DIRECTORY_NUMBER()==null) || 
             (this.MOBILE_DIRECTORY_NUMBER!=null &&
              this.MOBILE_DIRECTORY_NUMBER.equals(other.getMOBILE_DIRECTORY_NUMBER()))) &&
            ((this.PARTNER_ID==null && other.getPARTNER_ID()==null) || 
             (this.PARTNER_ID!=null &&
              this.PARTNER_ID.equals(other.getPARTNER_ID()))) &&
            ((this.SOURCE_CODE==null && other.getSOURCE_CODE()==null) || 
             (this.SOURCE_CODE!=null &&
              this.SOURCE_CODE.equals(other.getSOURCE_CODE()))) &&
            ((this.sub_Keywords==null && other.getSub_Keywords()==null) || 
             (this.sub_Keywords!=null &&
              java.util.Arrays.equals(this.sub_Keywords, other.getSub_Keywords()))) &&
            ((this.unsub_Keywords==null && other.getUnsub_Keywords()==null) || 
             (this.unsub_Keywords!=null &&
              java.util.Arrays.equals(this.unsub_Keywords, other.getUnsub_Keywords()))) &&
            ((this.authentication==null && other.getAuthentication()==null) || 
             (this.authentication!=null &&
              this.authentication.equals(other.getAuthentication())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMOBILE_DIRECTORY_NUMBER() != null) {
            _hashCode += getMOBILE_DIRECTORY_NUMBER().hashCode();
        }
        if (getPARTNER_ID() != null) {
            _hashCode += getPARTNER_ID().hashCode();
        }
        if (getSOURCE_CODE() != null) {
            _hashCode += getSOURCE_CODE().hashCode();
        }
        if (getSub_Keywords() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSub_Keywords());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSub_Keywords(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getUnsub_Keywords() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getUnsub_Keywords());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getUnsub_Keywords(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getAuthentication() != null) {
            _hashCode += getAuthentication().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ModifySubscriptions_Request.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/ModifySubscriptions_Request", ">ModifySubscriptions_Request"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MOBILE_DIRECTORY_NUMBER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/ModifySubscriptions_Request", "MOBILE_DIRECTORY_NUMBER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PARTNER_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/ModifySubscriptions_Request", "PARTNER_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SOURCE_CODE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/ModifySubscriptions_Request", "SOURCE_CODE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sub_Keywords");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/ModifySubscriptions_Request", "Sub_Keywords"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/ModifySubscriptions_Request", "Keyword"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("unsub_Keywords");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/ModifySubscriptions_Request", "Unsub_Keywords"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/ModifySubscriptions_Request", "Keyword"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authentication");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Authentication", "Authentication"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Authentication", ">Authentication"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
