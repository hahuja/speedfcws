/**
 * PCRCustomerDetails.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.xmlns.ei.GetPcrStatementDetails_Reply;

public class PCRCustomerDetails  implements java.io.Serializable {
    private com.heb.xmlns.ei.GetPcrStatementDetails_Reply.PCRCustomerDetailsTRANSANCTION_HEADER[] TRANSANCTION_HEADER;

    private com.heb.xmlns.ei.GetPcrStatementDetails_Reply.PCRCustomerDetailsPCR_CARD_DETAILS[] PCR_CARD_DETAILS;

    public PCRCustomerDetails() {
    }

    public PCRCustomerDetails(
           com.heb.xmlns.ei.GetPcrStatementDetails_Reply.PCRCustomerDetailsTRANSANCTION_HEADER[] TRANSANCTION_HEADER,
           com.heb.xmlns.ei.GetPcrStatementDetails_Reply.PCRCustomerDetailsPCR_CARD_DETAILS[] PCR_CARD_DETAILS) {
           this.TRANSANCTION_HEADER = TRANSANCTION_HEADER;
           this.PCR_CARD_DETAILS = PCR_CARD_DETAILS;
    }


    /**
     * Gets the TRANSANCTION_HEADER value for this PCRCustomerDetails.
     * 
     * @return TRANSANCTION_HEADER
     */
    public com.heb.xmlns.ei.GetPcrStatementDetails_Reply.PCRCustomerDetailsTRANSANCTION_HEADER[] getTRANSANCTION_HEADER() {
        return TRANSANCTION_HEADER;
    }


    /**
     * Sets the TRANSANCTION_HEADER value for this PCRCustomerDetails.
     * 
     * @param TRANSANCTION_HEADER
     */
    public void setTRANSANCTION_HEADER(com.heb.xmlns.ei.GetPcrStatementDetails_Reply.PCRCustomerDetailsTRANSANCTION_HEADER[] TRANSANCTION_HEADER) {
        this.TRANSANCTION_HEADER = TRANSANCTION_HEADER;
    }

    public com.heb.xmlns.ei.GetPcrStatementDetails_Reply.PCRCustomerDetailsTRANSANCTION_HEADER getTRANSANCTION_HEADER(int i) {
        return this.TRANSANCTION_HEADER[i];
    }

    public void setTRANSANCTION_HEADER(int i, com.heb.xmlns.ei.GetPcrStatementDetails_Reply.PCRCustomerDetailsTRANSANCTION_HEADER _value) {
        this.TRANSANCTION_HEADER[i] = _value;
    }


    /**
     * Gets the PCR_CARD_DETAILS value for this PCRCustomerDetails.
     * 
     * @return PCR_CARD_DETAILS
     */
    public com.heb.xmlns.ei.GetPcrStatementDetails_Reply.PCRCustomerDetailsPCR_CARD_DETAILS[] getPCR_CARD_DETAILS() {
        return PCR_CARD_DETAILS;
    }


    /**
     * Sets the PCR_CARD_DETAILS value for this PCRCustomerDetails.
     * 
     * @param PCR_CARD_DETAILS
     */
    public void setPCR_CARD_DETAILS(com.heb.xmlns.ei.GetPcrStatementDetails_Reply.PCRCustomerDetailsPCR_CARD_DETAILS[] PCR_CARD_DETAILS) {
        this.PCR_CARD_DETAILS = PCR_CARD_DETAILS;
    }

    public com.heb.xmlns.ei.GetPcrStatementDetails_Reply.PCRCustomerDetailsPCR_CARD_DETAILS getPCR_CARD_DETAILS(int i) {
        return this.PCR_CARD_DETAILS[i];
    }

    public void setPCR_CARD_DETAILS(int i, com.heb.xmlns.ei.GetPcrStatementDetails_Reply.PCRCustomerDetailsPCR_CARD_DETAILS _value) {
        this.PCR_CARD_DETAILS[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PCRCustomerDetails)) return false;
        PCRCustomerDetails other = (PCRCustomerDetails) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.TRANSANCTION_HEADER==null && other.getTRANSANCTION_HEADER()==null) || 
             (this.TRANSANCTION_HEADER!=null &&
              java.util.Arrays.equals(this.TRANSANCTION_HEADER, other.getTRANSANCTION_HEADER()))) &&
            ((this.PCR_CARD_DETAILS==null && other.getPCR_CARD_DETAILS()==null) || 
             (this.PCR_CARD_DETAILS!=null &&
              java.util.Arrays.equals(this.PCR_CARD_DETAILS, other.getPCR_CARD_DETAILS())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTRANSANCTION_HEADER() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTRANSANCTION_HEADER());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTRANSANCTION_HEADER(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPCR_CARD_DETAILS() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPCR_CARD_DETAILS());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPCR_CARD_DETAILS(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PCRCustomerDetails.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPcrStatementDetails_Reply", ">PCRCustomerDetails"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TRANSANCTION_HEADER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPcrStatementDetails_Reply", "TRANSANCTION_HEADER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPcrStatementDetails_Reply", ">>PCRCustomerDetails>TRANSANCTION_HEADER"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PCR_CARD_DETAILS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPcrStatementDetails_Reply", "PCR_CARD_DETAILS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPcrStatementDetails_Reply", ">>PCRCustomerDetails>PCR_CARD_DETAILS"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
