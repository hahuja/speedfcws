/**
 * GetAccountInformation_Reply.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.xmlns.ei.GetAccountInformation_Reply;

public class GetAccountInformation_Reply  implements java.io.Serializable {
    private com.heb.xmlns.ei.PROGRAM_ACCOUNT.PROGRAM_ACCOUNT[] PROGRAM_ACCOUNT;

    private com.heb.xmlns.ei.CUSTOMER.CUSTOMER[] CUSTOMER;

    private com.heb.xmlns.ei.PCR_ACCT_DTL.PCR_ACCT_DTL[] PCR_ACCT_DTL;

    private com.heb.xmlns.ei.PROGRAM_PARTICIPATION.PROGRAM_PARTICIPATION[] PROGRAM_PARTICIPATION;

    public GetAccountInformation_Reply() {
    }

    public GetAccountInformation_Reply(
           com.heb.xmlns.ei.PROGRAM_ACCOUNT.PROGRAM_ACCOUNT[] PROGRAM_ACCOUNT,
           com.heb.xmlns.ei.CUSTOMER.CUSTOMER[] CUSTOMER,
           com.heb.xmlns.ei.PCR_ACCT_DTL.PCR_ACCT_DTL[] PCR_ACCT_DTL,
           com.heb.xmlns.ei.PROGRAM_PARTICIPATION.PROGRAM_PARTICIPATION[] PROGRAM_PARTICIPATION) {
           this.PROGRAM_ACCOUNT = PROGRAM_ACCOUNT;
           this.CUSTOMER = CUSTOMER;
           this.PCR_ACCT_DTL = PCR_ACCT_DTL;
           this.PROGRAM_PARTICIPATION = PROGRAM_PARTICIPATION;
    }


    /**
     * Gets the PROGRAM_ACCOUNT value for this GetAccountInformation_Reply.
     * 
     * @return PROGRAM_ACCOUNT
     */
    public com.heb.xmlns.ei.PROGRAM_ACCOUNT.PROGRAM_ACCOUNT[] getPROGRAM_ACCOUNT() {
        return PROGRAM_ACCOUNT;
    }


    /**
     * Sets the PROGRAM_ACCOUNT value for this GetAccountInformation_Reply.
     * 
     * @param PROGRAM_ACCOUNT
     */
    public void setPROGRAM_ACCOUNT(com.heb.xmlns.ei.PROGRAM_ACCOUNT.PROGRAM_ACCOUNT[] PROGRAM_ACCOUNT) {
        this.PROGRAM_ACCOUNT = PROGRAM_ACCOUNT;
    }

    public com.heb.xmlns.ei.PROGRAM_ACCOUNT.PROGRAM_ACCOUNT getPROGRAM_ACCOUNT(int i) {
        return this.PROGRAM_ACCOUNT[i];
    }

    public void setPROGRAM_ACCOUNT(int i, com.heb.xmlns.ei.PROGRAM_ACCOUNT.PROGRAM_ACCOUNT _value) {
        this.PROGRAM_ACCOUNT[i] = _value;
    }


    /**
     * Gets the CUSTOMER value for this GetAccountInformation_Reply.
     * 
     * @return CUSTOMER
     */
    public com.heb.xmlns.ei.CUSTOMER.CUSTOMER[] getCUSTOMER() {
        return CUSTOMER;
    }


    /**
     * Sets the CUSTOMER value for this GetAccountInformation_Reply.
     * 
     * @param CUSTOMER
     */
    public void setCUSTOMER(com.heb.xmlns.ei.CUSTOMER.CUSTOMER[] CUSTOMER) {
        this.CUSTOMER = CUSTOMER;
    }

    public com.heb.xmlns.ei.CUSTOMER.CUSTOMER getCUSTOMER(int i) {
        return this.CUSTOMER[i];
    }

    public void setCUSTOMER(int i, com.heb.xmlns.ei.CUSTOMER.CUSTOMER _value) {
        this.CUSTOMER[i] = _value;
    }


    /**
     * Gets the PCR_ACCT_DTL value for this GetAccountInformation_Reply.
     * 
     * @return PCR_ACCT_DTL
     */
    public com.heb.xmlns.ei.PCR_ACCT_DTL.PCR_ACCT_DTL[] getPCR_ACCT_DTL() {
        return PCR_ACCT_DTL;
    }


    /**
     * Sets the PCR_ACCT_DTL value for this GetAccountInformation_Reply.
     * 
     * @param PCR_ACCT_DTL
     */
    public void setPCR_ACCT_DTL(com.heb.xmlns.ei.PCR_ACCT_DTL.PCR_ACCT_DTL[] PCR_ACCT_DTL) {
        this.PCR_ACCT_DTL = PCR_ACCT_DTL;
    }

    public com.heb.xmlns.ei.PCR_ACCT_DTL.PCR_ACCT_DTL getPCR_ACCT_DTL(int i) {
        return this.PCR_ACCT_DTL[i];
    }

    public void setPCR_ACCT_DTL(int i, com.heb.xmlns.ei.PCR_ACCT_DTL.PCR_ACCT_DTL _value) {
        this.PCR_ACCT_DTL[i] = _value;
    }


    /**
     * Gets the PROGRAM_PARTICIPATION value for this GetAccountInformation_Reply.
     * 
     * @return PROGRAM_PARTICIPATION
     */
    public com.heb.xmlns.ei.PROGRAM_PARTICIPATION.PROGRAM_PARTICIPATION[] getPROGRAM_PARTICIPATION() {
        return PROGRAM_PARTICIPATION;
    }


    /**
     * Sets the PROGRAM_PARTICIPATION value for this GetAccountInformation_Reply.
     * 
     * @param PROGRAM_PARTICIPATION
     */
    public void setPROGRAM_PARTICIPATION(com.heb.xmlns.ei.PROGRAM_PARTICIPATION.PROGRAM_PARTICIPATION[] PROGRAM_PARTICIPATION) {
        this.PROGRAM_PARTICIPATION = PROGRAM_PARTICIPATION;
    }

    public com.heb.xmlns.ei.PROGRAM_PARTICIPATION.PROGRAM_PARTICIPATION getPROGRAM_PARTICIPATION(int i) {
        return this.PROGRAM_PARTICIPATION[i];
    }

    public void setPROGRAM_PARTICIPATION(int i, com.heb.xmlns.ei.PROGRAM_PARTICIPATION.PROGRAM_PARTICIPATION _value) {
        this.PROGRAM_PARTICIPATION[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetAccountInformation_Reply)) return false;
        GetAccountInformation_Reply other = (GetAccountInformation_Reply) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.PROGRAM_ACCOUNT==null && other.getPROGRAM_ACCOUNT()==null) || 
             (this.PROGRAM_ACCOUNT!=null &&
              java.util.Arrays.equals(this.PROGRAM_ACCOUNT, other.getPROGRAM_ACCOUNT()))) &&
            ((this.CUSTOMER==null && other.getCUSTOMER()==null) || 
             (this.CUSTOMER!=null &&
              java.util.Arrays.equals(this.CUSTOMER, other.getCUSTOMER()))) &&
            ((this.PCR_ACCT_DTL==null && other.getPCR_ACCT_DTL()==null) || 
             (this.PCR_ACCT_DTL!=null &&
              java.util.Arrays.equals(this.PCR_ACCT_DTL, other.getPCR_ACCT_DTL()))) &&
            ((this.PROGRAM_PARTICIPATION==null && other.getPROGRAM_PARTICIPATION()==null) || 
             (this.PROGRAM_PARTICIPATION!=null &&
              java.util.Arrays.equals(this.PROGRAM_PARTICIPATION, other.getPROGRAM_PARTICIPATION())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPROGRAM_ACCOUNT() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPROGRAM_ACCOUNT());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPROGRAM_ACCOUNT(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCUSTOMER() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCUSTOMER());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCUSTOMER(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPCR_ACCT_DTL() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPCR_ACCT_DTL());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPCR_ACCT_DTL(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPROGRAM_PARTICIPATION() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPROGRAM_PARTICIPATION());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPROGRAM_PARTICIPATION(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetAccountInformation_Reply.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetAccountInformation_Reply", ">GetAccountInformation_Reply"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PROGRAM_ACCOUNT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PROGRAM_ACCOUNT", "PROGRAM_ACCOUNT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PROGRAM_ACCOUNT", "PROGRAM_ACCOUNT"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CUSTOMER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "CUSTOMER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CUSTOMER", "CUSTOMER"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PCR_ACCT_DTL");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PCR_ACCT_DTL", "PCR_ACCT_DTL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PCR_ACCT_DTL", "PCR_ACCT_DTL"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PROGRAM_PARTICIPATION");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PROGRAM_PARTICIPATION", "PROGRAM_PARTICIPATION"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/PROGRAM_PARTICIPATION", "PROGRAM_PARTICIPATION"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
