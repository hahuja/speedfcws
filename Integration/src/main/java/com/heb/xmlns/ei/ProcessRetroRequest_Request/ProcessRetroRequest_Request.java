/**
 * ProcessRetroRequest_Request.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.xmlns.ei.ProcessRetroRequest_Request;

public class ProcessRetroRequest_Request  implements java.io.Serializable {
    private java.math.BigInteger PCR_CARD_NUMBER;

    private java.math.BigInteger STORE;

    private java.util.Date DATE;

    private java.math.BigInteger TIME;

    private java.math.BigDecimal TOTAL_DOLLAR_AMT;

    private java.math.BigInteger ORDER_NUMBER;

    private java.math.BigDecimal GALLONS;

    private com.heb.xmlns.ei.Authentication.Authentication authentication;

    public ProcessRetroRequest_Request() {
    }

    public ProcessRetroRequest_Request(
           java.math.BigInteger PCR_CARD_NUMBER,
           java.math.BigInteger STORE,
           java.util.Date DATE,
           java.math.BigInteger TIME,
           java.math.BigDecimal TOTAL_DOLLAR_AMT,
           java.math.BigInteger ORDER_NUMBER,
           java.math.BigDecimal GALLONS,
           com.heb.xmlns.ei.Authentication.Authentication authentication) {
           this.PCR_CARD_NUMBER = PCR_CARD_NUMBER;
           this.STORE = STORE;
           this.DATE = DATE;
           this.TIME = TIME;
           this.TOTAL_DOLLAR_AMT = TOTAL_DOLLAR_AMT;
           this.ORDER_NUMBER = ORDER_NUMBER;
           this.GALLONS = GALLONS;
           this.authentication = authentication;
    }


    /**
     * Gets the PCR_CARD_NUMBER value for this ProcessRetroRequest_Request.
     * 
     * @return PCR_CARD_NUMBER
     */
    public java.math.BigInteger getPCR_CARD_NUMBER() {
        return PCR_CARD_NUMBER;
    }


    /**
     * Sets the PCR_CARD_NUMBER value for this ProcessRetroRequest_Request.
     * 
     * @param PCR_CARD_NUMBER
     */
    public void setPCR_CARD_NUMBER(java.math.BigInteger PCR_CARD_NUMBER) {
        this.PCR_CARD_NUMBER = PCR_CARD_NUMBER;
    }


    /**
     * Gets the STORE value for this ProcessRetroRequest_Request.
     * 
     * @return STORE
     */
    public java.math.BigInteger getSTORE() {
        return STORE;
    }


    /**
     * Sets the STORE value for this ProcessRetroRequest_Request.
     * 
     * @param STORE
     */
    public void setSTORE(java.math.BigInteger STORE) {
        this.STORE = STORE;
    }


    /**
     * Gets the DATE value for this ProcessRetroRequest_Request.
     * 
     * @return DATE
     */
    public java.util.Date getDATE() {
        return DATE;
    }


    /**
     * Sets the DATE value for this ProcessRetroRequest_Request.
     * 
     * @param DATE
     */
    public void setDATE(java.util.Date DATE) {
        this.DATE = DATE;
    }


    /**
     * Gets the TIME value for this ProcessRetroRequest_Request.
     * 
     * @return TIME
     */
    public java.math.BigInteger getTIME() {
        return TIME;
    }


    /**
     * Sets the TIME value for this ProcessRetroRequest_Request.
     * 
     * @param TIME
     */
    public void setTIME(java.math.BigInteger TIME) {
        this.TIME = TIME;
    }


    /**
     * Gets the TOTAL_DOLLAR_AMT value for this ProcessRetroRequest_Request.
     * 
     * @return TOTAL_DOLLAR_AMT
     */
    public java.math.BigDecimal getTOTAL_DOLLAR_AMT() {
        return TOTAL_DOLLAR_AMT;
    }


    /**
     * Sets the TOTAL_DOLLAR_AMT value for this ProcessRetroRequest_Request.
     * 
     * @param TOTAL_DOLLAR_AMT
     */
    public void setTOTAL_DOLLAR_AMT(java.math.BigDecimal TOTAL_DOLLAR_AMT) {
        this.TOTAL_DOLLAR_AMT = TOTAL_DOLLAR_AMT;
    }


    /**
     * Gets the ORDER_NUMBER value for this ProcessRetroRequest_Request.
     * 
     * @return ORDER_NUMBER
     */
    public java.math.BigInteger getORDER_NUMBER() {
        return ORDER_NUMBER;
    }


    /**
     * Sets the ORDER_NUMBER value for this ProcessRetroRequest_Request.
     * 
     * @param ORDER_NUMBER
     */
    public void setORDER_NUMBER(java.math.BigInteger ORDER_NUMBER) {
        this.ORDER_NUMBER = ORDER_NUMBER;
    }


    /**
     * Gets the GALLONS value for this ProcessRetroRequest_Request.
     * 
     * @return GALLONS
     */
    public java.math.BigDecimal getGALLONS() {
        return GALLONS;
    }


    /**
     * Sets the GALLONS value for this ProcessRetroRequest_Request.
     * 
     * @param GALLONS
     */
    public void setGALLONS(java.math.BigDecimal GALLONS) {
        this.GALLONS = GALLONS;
    }


    /**
     * Gets the authentication value for this ProcessRetroRequest_Request.
     * 
     * @return authentication
     */
    public com.heb.xmlns.ei.Authentication.Authentication getAuthentication() {
        return authentication;
    }


    /**
     * Sets the authentication value for this ProcessRetroRequest_Request.
     * 
     * @param authentication
     */
    public void setAuthentication(com.heb.xmlns.ei.Authentication.Authentication authentication) {
        this.authentication = authentication;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ProcessRetroRequest_Request)) return false;
        ProcessRetroRequest_Request other = (ProcessRetroRequest_Request) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.PCR_CARD_NUMBER==null && other.getPCR_CARD_NUMBER()==null) || 
             (this.PCR_CARD_NUMBER!=null &&
              this.PCR_CARD_NUMBER.equals(other.getPCR_CARD_NUMBER()))) &&
            ((this.STORE==null && other.getSTORE()==null) || 
             (this.STORE!=null &&
              this.STORE.equals(other.getSTORE()))) &&
            ((this.DATE==null && other.getDATE()==null) || 
             (this.DATE!=null &&
              this.DATE.equals(other.getDATE()))) &&
            ((this.TIME==null && other.getTIME()==null) || 
             (this.TIME!=null &&
              this.TIME.equals(other.getTIME()))) &&
            ((this.TOTAL_DOLLAR_AMT==null && other.getTOTAL_DOLLAR_AMT()==null) || 
             (this.TOTAL_DOLLAR_AMT!=null &&
              this.TOTAL_DOLLAR_AMT.equals(other.getTOTAL_DOLLAR_AMT()))) &&
            ((this.ORDER_NUMBER==null && other.getORDER_NUMBER()==null) || 
             (this.ORDER_NUMBER!=null &&
              this.ORDER_NUMBER.equals(other.getORDER_NUMBER()))) &&
            ((this.GALLONS==null && other.getGALLONS()==null) || 
             (this.GALLONS!=null &&
              this.GALLONS.equals(other.getGALLONS()))) &&
            ((this.authentication==null && other.getAuthentication()==null) || 
             (this.authentication!=null &&
              this.authentication.equals(other.getAuthentication())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPCR_CARD_NUMBER() != null) {
            _hashCode += getPCR_CARD_NUMBER().hashCode();
        }
        if (getSTORE() != null) {
            _hashCode += getSTORE().hashCode();
        }
        if (getDATE() != null) {
            _hashCode += getDATE().hashCode();
        }
        if (getTIME() != null) {
            _hashCode += getTIME().hashCode();
        }
        if (getTOTAL_DOLLAR_AMT() != null) {
            _hashCode += getTOTAL_DOLLAR_AMT().hashCode();
        }
        if (getORDER_NUMBER() != null) {
            _hashCode += getORDER_NUMBER().hashCode();
        }
        if (getGALLONS() != null) {
            _hashCode += getGALLONS().hashCode();
        }
        if (getAuthentication() != null) {
            _hashCode += getAuthentication().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ProcessRetroRequest_Request.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProcessRetroRequest_Request", ">ProcessRetroRequest_Request"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PCR_CARD_NUMBER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProcessRetroRequest_Request", "PCR_CARD_NUMBER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("STORE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProcessRetroRequest_Request", "STORE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProcessRetroRequest_Request", "DATE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TIME");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProcessRetroRequest_Request", "TIME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TOTAL_DOLLAR_AMT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProcessRetroRequest_Request", "TOTAL_DOLLAR_AMT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ORDER_NUMBER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProcessRetroRequest_Request", "ORDER_NUMBER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("GALLONS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProcessRetroRequest_Request", "GALLONS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authentication");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Authentication", "Authentication"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Authentication", ">Authentication"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
