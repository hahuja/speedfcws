/**
 * GetPcrStatementDetails_Request.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.xmlns.ei.GetPcrStatementDetails_Request;

public class GetPcrStatementDetails_Request  implements java.io.Serializable {
    private java.math.BigInteger AMS_HOUSEHOLD_ID;

    private com.heb.xmlns.ei.Authentication.Authentication authentication;

    public GetPcrStatementDetails_Request() {
    }

    public GetPcrStatementDetails_Request(
           java.math.BigInteger AMS_HOUSEHOLD_ID,
           com.heb.xmlns.ei.Authentication.Authentication authentication) {
           this.AMS_HOUSEHOLD_ID = AMS_HOUSEHOLD_ID;
           this.authentication = authentication;
    }


    /**
     * Gets the AMS_HOUSEHOLD_ID value for this GetPcrStatementDetails_Request.
     * 
     * @return AMS_HOUSEHOLD_ID
     */
    public java.math.BigInteger getAMS_HOUSEHOLD_ID() {
        return AMS_HOUSEHOLD_ID;
    }


    /**
     * Sets the AMS_HOUSEHOLD_ID value for this GetPcrStatementDetails_Request.
     * 
     * @param AMS_HOUSEHOLD_ID
     */
    public void setAMS_HOUSEHOLD_ID(java.math.BigInteger AMS_HOUSEHOLD_ID) {
        this.AMS_HOUSEHOLD_ID = AMS_HOUSEHOLD_ID;
    }


    /**
     * Gets the authentication value for this GetPcrStatementDetails_Request.
     * 
     * @return authentication
     */
    public com.heb.xmlns.ei.Authentication.Authentication getAuthentication() {
        return authentication;
    }


    /**
     * Sets the authentication value for this GetPcrStatementDetails_Request.
     * 
     * @param authentication
     */
    public void setAuthentication(com.heb.xmlns.ei.Authentication.Authentication authentication) {
        this.authentication = authentication;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetPcrStatementDetails_Request)) return false;
        GetPcrStatementDetails_Request other = (GetPcrStatementDetails_Request) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.AMS_HOUSEHOLD_ID==null && other.getAMS_HOUSEHOLD_ID()==null) || 
             (this.AMS_HOUSEHOLD_ID!=null &&
              this.AMS_HOUSEHOLD_ID.equals(other.getAMS_HOUSEHOLD_ID()))) &&
            ((this.authentication==null && other.getAuthentication()==null) || 
             (this.authentication!=null &&
              this.authentication.equals(other.getAuthentication())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAMS_HOUSEHOLD_ID() != null) {
            _hashCode += getAMS_HOUSEHOLD_ID().hashCode();
        }
        if (getAuthentication() != null) {
            _hashCode += getAuthentication().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetPcrStatementDetails_Request.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPcrStatementDetails_Request", ">GetPcrStatementDetails_Request"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("AMS_HOUSEHOLD_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetPcrStatementDetails_Request", "AMS_HOUSEHOLD_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authentication");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Authentication", "Authentication"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Authentication", ">Authentication"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
