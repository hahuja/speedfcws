/**
 * CustomerAccountServiceServiceagentTestCase.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.xmlns.ei.CustomerAccountService;

import org.apache.axis.client.Call;

public class CustomerAccountServiceServiceagentTestCase extends junit.framework.TestCase {
    public CustomerAccountServiceServiceagentTestCase(java.lang.String name) {
        super(name);
    }

    public void testCustomerAccountServiceWSDL() throws Exception {
        javax.xml.rpc.ServiceFactory serviceFactory = javax.xml.rpc.ServiceFactory.newInstance();
        java.net.URL url = new java.net.URL(new com.heb.xmlns.ei.CustomerAccountService.CustomerAccountServiceServiceagentLocator().getCustomerAccountServiceAddress() + "?WSDL");
        javax.xml.rpc.Service service = serviceFactory.createService(url, new com.heb.xmlns.ei.CustomerAccountService.CustomerAccountServiceServiceagentLocator().getServiceName());
        assertTrue(service != null);
    }

    public void test1CustomerAccountServiceGetAccountInformation() throws Exception {
        com.heb.xmlns.ei.CustomerAccountService.CustomerAccountServiceBindingStub binding;
        try {
            binding = (com.heb.xmlns.ei.CustomerAccountService.CustomerAccountServiceBindingStub)
                          new com.heb.xmlns.ei.CustomerAccountService.CustomerAccountServiceServiceagentLocator().getCustomerAccountService();           
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.heb.xmlns.ei.GetAccountInformation_Reply.GetAccountInformation_Reply value = null;
            value = binding.getAccountInformation(new com.heb.xmlns.ei.GetAccountInformation_Request.GetAccountInformation_Request());
        }
        catch (com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault e1) {
            throw new junit.framework.AssertionFailedError("fault Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test2CustomerAccountServiceGetReceiptDetails() throws Exception {
        com.heb.xmlns.ei.CustomerAccountService.CustomerAccountServiceBindingStub binding;
        try {
            binding = (com.heb.xmlns.ei.CustomerAccountService.CustomerAccountServiceBindingStub)
                          new com.heb.xmlns.ei.CustomerAccountService.CustomerAccountServiceServiceagentLocator().getCustomerAccountService();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.heb.xmlns.ei.GetReceiptDetails_Reply.GetReceiptDetails_Reply value = null;
            value = binding.getReceiptDetails(new com.heb.xmlns.ei.GetReceiptDetails_Request.GetReceiptDetails_Request());
        }
        catch (com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault e1) {
            throw new junit.framework.AssertionFailedError("fault Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test3CustomerAccountServiceAssociateAccount() throws Exception {
        com.heb.xmlns.ei.CustomerAccountService.CustomerAccountServiceBindingStub binding;
        try {
            binding = (com.heb.xmlns.ei.CustomerAccountService.CustomerAccountServiceBindingStub)
                          new com.heb.xmlns.ei.CustomerAccountService.CustomerAccountServiceServiceagentLocator().getCustomerAccountService();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.heb.xmlns.ei.AssociateAccount_Reply.AssociateAccount_Reply value = null;
            value = binding.associateAccount(new com.heb.xmlns.ei.AssociateAccount_Request.AssociateAccount_Request());
        }
        catch (com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault e1) {
            throw new junit.framework.AssertionFailedError("fault Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test4CustomerAccountServiceProcessRetroRequest() throws Exception {
        com.heb.xmlns.ei.CustomerAccountService.CustomerAccountServiceBindingStub binding;
        try {
            binding = (com.heb.xmlns.ei.CustomerAccountService.CustomerAccountServiceBindingStub)
                          new com.heb.xmlns.ei.CustomerAccountService.CustomerAccountServiceServiceagentLocator().getCustomerAccountService();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.heb.xmlns.ei.ProcessRetroRequest_Reply.ProcessRetroRequest_Reply value = null;
            value = binding.processRetroRequest(new com.heb.xmlns.ei.ProcessRetroRequest_Request.ProcessRetroRequest_Request());
        }
        catch (com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault e1) {
            throw new junit.framework.AssertionFailedError("fault Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test5CustomerAccountServiceProcessCard() throws Exception {
        com.heb.xmlns.ei.CustomerAccountService.CustomerAccountServiceBindingStub binding;
        try {
            binding = (com.heb.xmlns.ei.CustomerAccountService.CustomerAccountServiceBindingStub)
                          new com.heb.xmlns.ei.CustomerAccountService.CustomerAccountServiceServiceagentLocator().getCustomerAccountService();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.heb.xmlns.ei.ProcessCard_Reply.ProcessCard_Reply value = null;
            value = binding.processCard(new com.heb.xmlns.ei.ProcessCard_Request.ProcessCard_Request());
        }
        catch (com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault e1) {
            throw new junit.framework.AssertionFailedError("fault Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test6CustomerAccountServiceGetReasonCodes() throws Exception {
        com.heb.xmlns.ei.CustomerAccountService.CustomerAccountServiceBindingStub binding;
        try {
            binding = (com.heb.xmlns.ei.CustomerAccountService.CustomerAccountServiceBindingStub)
                          new com.heb.xmlns.ei.CustomerAccountService.CustomerAccountServiceServiceagentLocator().getCustomerAccountService();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.heb.xmlns.ei.REASON_CODES.REASON_CODES[] value = null;
            value = binding.getReasonCodes(new com.heb.xmlns.ei.GetReasonCodes_Request.GetReasonCodes_Request());
        }
        catch (com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault e1) {
            throw new junit.framework.AssertionFailedError("fault Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test7CustomerAccountServiceUpdateProfile() throws Exception {
        com.heb.xmlns.ei.CustomerAccountService.CustomerAccountServiceBindingStub binding;
        try {
            binding = (com.heb.xmlns.ei.CustomerAccountService.CustomerAccountServiceBindingStub)
                          new com.heb.xmlns.ei.CustomerAccountService.CustomerAccountServiceServiceagentLocator().getCustomerAccountService();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.heb.xmlns.ei.UpdateProfile_Reply.UpdateProfile_Reply value = null;
            value = binding.updateProfile(new com.heb.xmlns.ei.UpdateProfile_Request.UpdateProfile_Request());
        }
        catch (com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault e1) {
            throw new junit.framework.AssertionFailedError("fault Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test8CustomerAccountServiceGetPin() throws Exception {
        com.heb.xmlns.ei.CustomerAccountService.CustomerAccountServiceBindingStub binding;
        try {
            binding = (com.heb.xmlns.ei.CustomerAccountService.CustomerAccountServiceBindingStub)
                          new com.heb.xmlns.ei.CustomerAccountService.CustomerAccountServiceServiceagentLocator().getCustomerAccountService();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.heb.xmlns.ei.GetPin_Reply.GetPin_Reply value = null;
            value = binding.getPin(new com.heb.xmlns.ei.GetPin_Request.GetPin_Request());
        }
        catch (com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault e1) {
            throw new junit.framework.AssertionFailedError("fault1 Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test9CustomerAccountServiceGetDccMemberAccountInformation() throws Exception {
        com.heb.xmlns.ei.CustomerAccountService.CustomerAccountServiceBindingStub binding;
        try {
            binding = (com.heb.xmlns.ei.CustomerAccountService.CustomerAccountServiceBindingStub)
                          new com.heb.xmlns.ei.CustomerAccountService.CustomerAccountServiceServiceagentLocator().getCustomerAccountService();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.heb.xmlns.ei.GetDccMemberAccountInformation_Reply.GetDccMemberAccountInformation_ReplyDccMemberAccountInformation[] value = null;
            value = binding.getDccMemberAccountInformation(new com.heb.xmlns.ei.GetDccMemberAccountInformation_Request.GetDccMemberAccountInformation_Request());
        }
        catch (com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault e1) {
            throw new junit.framework.AssertionFailedError("fault1 Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test10CustomerAccountServiceGetPointsAndStoredValueAmount() throws Exception {
        com.heb.xmlns.ei.CustomerAccountService.CustomerAccountServiceBindingStub binding;
        try {
            binding = (com.heb.xmlns.ei.CustomerAccountService.CustomerAccountServiceBindingStub)
                          new com.heb.xmlns.ei.CustomerAccountService.CustomerAccountServiceServiceagentLocator().getCustomerAccountService();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.heb.xmlns.ei.GetPointsAndStoredValueAmount_Reply.GetPointsAndStoredValueAmount_Reply value = null;
            value = binding.getPointsAndStoredValueAmount(new com.heb.xmlns.ei.GetPointsAndStoredValueAmount_Request.GetPointsAndStoredValueAmount_Request());
        }
        catch (com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault e1) {
            throw new junit.framework.AssertionFailedError("fault Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test11CustomerAccountServiceGetPcrStatementDetails() throws Exception {
        com.heb.xmlns.ei.CustomerAccountService.CustomerAccountServiceBindingStub binding;
        try {
            binding = (com.heb.xmlns.ei.CustomerAccountService.CustomerAccountServiceBindingStub)
                          new com.heb.xmlns.ei.CustomerAccountService.CustomerAccountServiceServiceagentLocator().getCustomerAccountService();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.heb.xmlns.ei.GetPcrStatementDetails_Reply.PCRCustomerDetails value = null;
            value = binding.getPcrStatementDetails(new com.heb.xmlns.ei.GetPcrStatementDetails_Request.GetPcrStatementDetails_Request());
        }
        catch (com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault e1) {
            throw new junit.framework.AssertionFailedError("fault Exception caught: " + e1);
        }
            // TBD - validate results
    }

}
