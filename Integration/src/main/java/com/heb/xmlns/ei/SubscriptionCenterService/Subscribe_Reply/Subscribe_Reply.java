/**
 * Subscribe_Reply.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.xmlns.ei.SubscriptionCenterService.Subscribe_Reply;

public class Subscribe_Reply  implements java.io.Serializable {
    private boolean success;

    private com.heb.xmlns.ei.Salt2.ErrorCode[] errorCodes;

    private com.heb.xmlns.ei.Salt2.SubscriptionStatusCode subscriptionStatusCode;

    public Subscribe_Reply() {
    }

    public Subscribe_Reply(
           boolean success,
           com.heb.xmlns.ei.Salt2.ErrorCode[] errorCodes,
           com.heb.xmlns.ei.Salt2.SubscriptionStatusCode subscriptionStatusCode) {
           this.success = success;
           this.errorCodes = errorCodes;
           this.subscriptionStatusCode = subscriptionStatusCode;
    }


    /**
     * Gets the success value for this Subscribe_Reply.
     * 
     * @return success
     */
    public boolean isSuccess() {
        return success;
    }


    /**
     * Sets the success value for this Subscribe_Reply.
     * 
     * @param success
     */
    public void setSuccess(boolean success) {
        this.success = success;
    }


    /**
     * Gets the errorCodes value for this Subscribe_Reply.
     * 
     * @return errorCodes
     */
    public com.heb.xmlns.ei.Salt2.ErrorCode[] getErrorCodes() {
        return errorCodes;
    }


    /**
     * Sets the errorCodes value for this Subscribe_Reply.
     * 
     * @param errorCodes
     */
    public void setErrorCodes(com.heb.xmlns.ei.Salt2.ErrorCode[] errorCodes) {
        this.errorCodes = errorCodes;
    }


    /**
     * Gets the subscriptionStatusCode value for this Subscribe_Reply.
     * 
     * @return subscriptionStatusCode
     */
    public com.heb.xmlns.ei.Salt2.SubscriptionStatusCode getSubscriptionStatusCode() {
        return subscriptionStatusCode;
    }


    /**
     * Sets the subscriptionStatusCode value for this Subscribe_Reply.
     * 
     * @param subscriptionStatusCode
     */
    public void setSubscriptionStatusCode(com.heb.xmlns.ei.Salt2.SubscriptionStatusCode subscriptionStatusCode) {
        this.subscriptionStatusCode = subscriptionStatusCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Subscribe_Reply)) return false;
        Subscribe_Reply other = (Subscribe_Reply) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.success == other.isSuccess() &&
            ((this.errorCodes==null && other.getErrorCodes()==null) || 
             (this.errorCodes!=null &&
              java.util.Arrays.equals(this.errorCodes, other.getErrorCodes()))) &&
            ((this.subscriptionStatusCode==null && other.getSubscriptionStatusCode()==null) || 
             (this.subscriptionStatusCode!=null &&
              this.subscriptionStatusCode.equals(other.getSubscriptionStatusCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += (isSuccess() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getErrorCodes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getErrorCodes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getErrorCodes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getSubscriptionStatusCode() != null) {
            _hashCode += getSubscriptionStatusCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Subscribe_Reply.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/Subscribe_Reply", ">Subscribe_Reply"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("success");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/Subscribe_Reply", "success"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorCodes");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/Subscribe_Reply", "ErrorCodes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Salt2", "ErrorCode"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Salt2", "error-code"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subscriptionStatusCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/Subscribe_Reply", "SubscriptionStatusCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Salt2", "SubscriptionStatusCode"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
