/**
 * GetReceiptDetails_Reply.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.xmlns.ei.GetReceiptDetails_Reply;

public class GetReceiptDetails_Reply  implements java.io.Serializable {
    private com.heb.xmlns.ei.RECEIPT_DETAILS.RECEIPT_DETAILS[] RECEIPT_DETAILS;

    private com.heb.xmlns.ei.TENDER_DETAILS.TENDER_DETAILS[] TENDER_DETAILS;

    public GetReceiptDetails_Reply() {
    }

    public GetReceiptDetails_Reply(
           com.heb.xmlns.ei.RECEIPT_DETAILS.RECEIPT_DETAILS[] RECEIPT_DETAILS,
           com.heb.xmlns.ei.TENDER_DETAILS.TENDER_DETAILS[] TENDER_DETAILS) {
           this.RECEIPT_DETAILS = RECEIPT_DETAILS;
           this.TENDER_DETAILS = TENDER_DETAILS;
    }


    /**
     * Gets the RECEIPT_DETAILS value for this GetReceiptDetails_Reply.
     * 
     * @return RECEIPT_DETAILS
     */
    public com.heb.xmlns.ei.RECEIPT_DETAILS.RECEIPT_DETAILS[] getRECEIPT_DETAILS() {
        return RECEIPT_DETAILS;
    }


    /**
     * Sets the RECEIPT_DETAILS value for this GetReceiptDetails_Reply.
     * 
     * @param RECEIPT_DETAILS
     */
    public void setRECEIPT_DETAILS(com.heb.xmlns.ei.RECEIPT_DETAILS.RECEIPT_DETAILS[] RECEIPT_DETAILS) {
        this.RECEIPT_DETAILS = RECEIPT_DETAILS;
    }

    public com.heb.xmlns.ei.RECEIPT_DETAILS.RECEIPT_DETAILS getRECEIPT_DETAILS(int i) {
        return this.RECEIPT_DETAILS[i];
    }

    public void setRECEIPT_DETAILS(int i, com.heb.xmlns.ei.RECEIPT_DETAILS.RECEIPT_DETAILS _value) {
        this.RECEIPT_DETAILS[i] = _value;
    }


    /**
     * Gets the TENDER_DETAILS value for this GetReceiptDetails_Reply.
     * 
     * @return TENDER_DETAILS
     */
    public com.heb.xmlns.ei.TENDER_DETAILS.TENDER_DETAILS[] getTENDER_DETAILS() {
        return TENDER_DETAILS;
    }


    /**
     * Sets the TENDER_DETAILS value for this GetReceiptDetails_Reply.
     * 
     * @param TENDER_DETAILS
     */
    public void setTENDER_DETAILS(com.heb.xmlns.ei.TENDER_DETAILS.TENDER_DETAILS[] TENDER_DETAILS) {
        this.TENDER_DETAILS = TENDER_DETAILS;
    }

    public com.heb.xmlns.ei.TENDER_DETAILS.TENDER_DETAILS getTENDER_DETAILS(int i) {
        return this.TENDER_DETAILS[i];
    }

    public void setTENDER_DETAILS(int i, com.heb.xmlns.ei.TENDER_DETAILS.TENDER_DETAILS _value) {
        this.TENDER_DETAILS[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetReceiptDetails_Reply)) return false;
        GetReceiptDetails_Reply other = (GetReceiptDetails_Reply) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.RECEIPT_DETAILS==null && other.getRECEIPT_DETAILS()==null) || 
             (this.RECEIPT_DETAILS!=null &&
              java.util.Arrays.equals(this.RECEIPT_DETAILS, other.getRECEIPT_DETAILS()))) &&
            ((this.TENDER_DETAILS==null && other.getTENDER_DETAILS()==null) || 
             (this.TENDER_DETAILS!=null &&
              java.util.Arrays.equals(this.TENDER_DETAILS, other.getTENDER_DETAILS())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getRECEIPT_DETAILS() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getRECEIPT_DETAILS());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getRECEIPT_DETAILS(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTENDER_DETAILS() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTENDER_DETAILS());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTENDER_DETAILS(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetReceiptDetails_Reply.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/GetReceiptDetails_Reply", ">GetReceiptDetails_Reply"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("RECEIPT_DETAILS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/RECEIPT_DETAILS", "RECEIPT_DETAILS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/RECEIPT_DETAILS", "RECEIPT_DETAILS"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TENDER_DETAILS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/TENDER_DETAILS", "TENDER_DETAILS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/TENDER_DETAILS", "TENDER_DETAILS"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
