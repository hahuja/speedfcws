/**
 * SendMessageResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.xmlns.ei.Salt3;

public class SendMessageResponse  implements java.io.Serializable {
    private boolean success;

    private com.heb.xmlns.ei.Salt3.ErrorCode[] errorCode;

    private java.lang.String messageId;

    public SendMessageResponse() {
    }

    public SendMessageResponse(
           boolean success,
           com.heb.xmlns.ei.Salt3.ErrorCode[] errorCode,
           java.lang.String messageId) {
           this.success = success;
           this.errorCode = errorCode;
           this.messageId = messageId;
    }


    /**
     * Gets the success value for this SendMessageResponse.
     * 
     * @return success
     */
    public boolean isSuccess() {
        return success;
    }


    /**
     * Sets the success value for this SendMessageResponse.
     * 
     * @param success
     */
    public void setSuccess(boolean success) {
        this.success = success;
    }


    /**
     * Gets the errorCode value for this SendMessageResponse.
     * 
     * @return errorCode
     */
    public com.heb.xmlns.ei.Salt3.ErrorCode[] getErrorCode() {
        return errorCode;
    }


    /**
     * Sets the errorCode value for this SendMessageResponse.
     * 
     * @param errorCode
     */
    public void setErrorCode(com.heb.xmlns.ei.Salt3.ErrorCode[] errorCode) {
        this.errorCode = errorCode;
    }

    public com.heb.xmlns.ei.Salt3.ErrorCode getErrorCode(int i) {
        return this.errorCode[i];
    }

    public void setErrorCode(int i, com.heb.xmlns.ei.Salt3.ErrorCode _value) {
        this.errorCode[i] = _value;
    }


    /**
     * Gets the messageId value for this SendMessageResponse.
     * 
     * @return messageId
     */
    public java.lang.String getMessageId() {
        return messageId;
    }


    /**
     * Sets the messageId value for this SendMessageResponse.
     * 
     * @param messageId
     */
    public void setMessageId(java.lang.String messageId) {
        this.messageId = messageId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SendMessageResponse)) return false;
        SendMessageResponse other = (SendMessageResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.success == other.isSuccess() &&
            ((this.errorCode==null && other.getErrorCode()==null) || 
             (this.errorCode!=null &&
              java.util.Arrays.equals(this.errorCode, other.getErrorCode()))) &&
            ((this.messageId==null && other.getMessageId()==null) || 
             (this.messageId!=null &&
              this.messageId.equals(other.getMessageId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += (isSuccess() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getErrorCode() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getErrorCode());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getErrorCode(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getMessageId() != null) {
            _hashCode += getMessageId().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SendMessageResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Salt3", ">send-message-response"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("success");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Salt3", "success"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Salt3", "error-code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Salt3", "ErrorCode"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("messageId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Salt3", "message-id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
