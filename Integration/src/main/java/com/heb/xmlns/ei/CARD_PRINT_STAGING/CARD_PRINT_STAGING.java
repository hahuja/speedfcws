/**
 * CARD_PRINT_STAGING.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.xmlns.ei.CARD_PRINT_STAGING;

public class CARD_PRINT_STAGING  implements java.io.Serializable {
    private java.math.BigDecimal PS_EMPLOYEE_ID;

    private java.lang.String PRIMARY_ACTIVATION_CARD_TIMESTAMP;

    private java.lang.String PRIMARY_REPLACE_CARD_TIMESTAMP;

    private java.lang.String SECONDARY_CREATE_TIMESTAMP;

    private java.lang.String SECONDARY_REPLACE_CARD_TIMESTAMP;

    private java.lang.String SECONDARY_DEACTIVATION_CARD_TIMESTAMP;

    private java.lang.String PRIMARY_CARD_PRINT_REASON_ID;

    private java.lang.String SECONDARY_CARD_PRINT_REASON_ID;

    private java.lang.String PS_SECONDARY_LAST_NAME;

    private java.lang.String PS_SECONDARY_FIRST_NAME;

    public CARD_PRINT_STAGING() {
    }

    public CARD_PRINT_STAGING(
           java.math.BigDecimal PS_EMPLOYEE_ID,
           java.lang.String PRIMARY_ACTIVATION_CARD_TIMESTAMP,
           java.lang.String PRIMARY_REPLACE_CARD_TIMESTAMP,
           java.lang.String SECONDARY_CREATE_TIMESTAMP,
           java.lang.String SECONDARY_REPLACE_CARD_TIMESTAMP,
           java.lang.String SECONDARY_DEACTIVATION_CARD_TIMESTAMP,
           java.lang.String PRIMARY_CARD_PRINT_REASON_ID,
           java.lang.String SECONDARY_CARD_PRINT_REASON_ID,
           java.lang.String PS_SECONDARY_LAST_NAME,
           java.lang.String PS_SECONDARY_FIRST_NAME) {
           this.PS_EMPLOYEE_ID = PS_EMPLOYEE_ID;
           this.PRIMARY_ACTIVATION_CARD_TIMESTAMP = PRIMARY_ACTIVATION_CARD_TIMESTAMP;
           this.PRIMARY_REPLACE_CARD_TIMESTAMP = PRIMARY_REPLACE_CARD_TIMESTAMP;
           this.SECONDARY_CREATE_TIMESTAMP = SECONDARY_CREATE_TIMESTAMP;
           this.SECONDARY_REPLACE_CARD_TIMESTAMP = SECONDARY_REPLACE_CARD_TIMESTAMP;
           this.SECONDARY_DEACTIVATION_CARD_TIMESTAMP = SECONDARY_DEACTIVATION_CARD_TIMESTAMP;
           this.PRIMARY_CARD_PRINT_REASON_ID = PRIMARY_CARD_PRINT_REASON_ID;
           this.SECONDARY_CARD_PRINT_REASON_ID = SECONDARY_CARD_PRINT_REASON_ID;
           this.PS_SECONDARY_LAST_NAME = PS_SECONDARY_LAST_NAME;
           this.PS_SECONDARY_FIRST_NAME = PS_SECONDARY_FIRST_NAME;
    }


    /**
     * Gets the PS_EMPLOYEE_ID value for this CARD_PRINT_STAGING.
     * 
     * @return PS_EMPLOYEE_ID
     */
    public java.math.BigDecimal getPS_EMPLOYEE_ID() {
        return PS_EMPLOYEE_ID;
    }


    /**
     * Sets the PS_EMPLOYEE_ID value for this CARD_PRINT_STAGING.
     * 
     * @param PS_EMPLOYEE_ID
     */
    public void setPS_EMPLOYEE_ID(java.math.BigDecimal PS_EMPLOYEE_ID) {
        this.PS_EMPLOYEE_ID = PS_EMPLOYEE_ID;
    }


    /**
     * Gets the PRIMARY_ACTIVATION_CARD_TIMESTAMP value for this CARD_PRINT_STAGING.
     * 
     * @return PRIMARY_ACTIVATION_CARD_TIMESTAMP
     */
    public java.lang.String getPRIMARY_ACTIVATION_CARD_TIMESTAMP() {
        return PRIMARY_ACTIVATION_CARD_TIMESTAMP;
    }


    /**
     * Sets the PRIMARY_ACTIVATION_CARD_TIMESTAMP value for this CARD_PRINT_STAGING.
     * 
     * @param PRIMARY_ACTIVATION_CARD_TIMESTAMP
     */
    public void setPRIMARY_ACTIVATION_CARD_TIMESTAMP(java.lang.String PRIMARY_ACTIVATION_CARD_TIMESTAMP) {
        this.PRIMARY_ACTIVATION_CARD_TIMESTAMP = PRIMARY_ACTIVATION_CARD_TIMESTAMP;
    }


    /**
     * Gets the PRIMARY_REPLACE_CARD_TIMESTAMP value for this CARD_PRINT_STAGING.
     * 
     * @return PRIMARY_REPLACE_CARD_TIMESTAMP
     */
    public java.lang.String getPRIMARY_REPLACE_CARD_TIMESTAMP() {
        return PRIMARY_REPLACE_CARD_TIMESTAMP;
    }


    /**
     * Sets the PRIMARY_REPLACE_CARD_TIMESTAMP value for this CARD_PRINT_STAGING.
     * 
     * @param PRIMARY_REPLACE_CARD_TIMESTAMP
     */
    public void setPRIMARY_REPLACE_CARD_TIMESTAMP(java.lang.String PRIMARY_REPLACE_CARD_TIMESTAMP) {
        this.PRIMARY_REPLACE_CARD_TIMESTAMP = PRIMARY_REPLACE_CARD_TIMESTAMP;
    }


    /**
     * Gets the SECONDARY_CREATE_TIMESTAMP value for this CARD_PRINT_STAGING.
     * 
     * @return SECONDARY_CREATE_TIMESTAMP
     */
    public java.lang.String getSECONDARY_CREATE_TIMESTAMP() {
        return SECONDARY_CREATE_TIMESTAMP;
    }


    /**
     * Sets the SECONDARY_CREATE_TIMESTAMP value for this CARD_PRINT_STAGING.
     * 
     * @param SECONDARY_CREATE_TIMESTAMP
     */
    public void setSECONDARY_CREATE_TIMESTAMP(java.lang.String SECONDARY_CREATE_TIMESTAMP) {
        this.SECONDARY_CREATE_TIMESTAMP = SECONDARY_CREATE_TIMESTAMP;
    }


    /**
     * Gets the SECONDARY_REPLACE_CARD_TIMESTAMP value for this CARD_PRINT_STAGING.
     * 
     * @return SECONDARY_REPLACE_CARD_TIMESTAMP
     */
    public java.lang.String getSECONDARY_REPLACE_CARD_TIMESTAMP() {
        return SECONDARY_REPLACE_CARD_TIMESTAMP;
    }


    /**
     * Sets the SECONDARY_REPLACE_CARD_TIMESTAMP value for this CARD_PRINT_STAGING.
     * 
     * @param SECONDARY_REPLACE_CARD_TIMESTAMP
     */
    public void setSECONDARY_REPLACE_CARD_TIMESTAMP(java.lang.String SECONDARY_REPLACE_CARD_TIMESTAMP) {
        this.SECONDARY_REPLACE_CARD_TIMESTAMP = SECONDARY_REPLACE_CARD_TIMESTAMP;
    }


    /**
     * Gets the SECONDARY_DEACTIVATION_CARD_TIMESTAMP value for this CARD_PRINT_STAGING.
     * 
     * @return SECONDARY_DEACTIVATION_CARD_TIMESTAMP
     */
    public java.lang.String getSECONDARY_DEACTIVATION_CARD_TIMESTAMP() {
        return SECONDARY_DEACTIVATION_CARD_TIMESTAMP;
    }


    /**
     * Sets the SECONDARY_DEACTIVATION_CARD_TIMESTAMP value for this CARD_PRINT_STAGING.
     * 
     * @param SECONDARY_DEACTIVATION_CARD_TIMESTAMP
     */
    public void setSECONDARY_DEACTIVATION_CARD_TIMESTAMP(java.lang.String SECONDARY_DEACTIVATION_CARD_TIMESTAMP) {
        this.SECONDARY_DEACTIVATION_CARD_TIMESTAMP = SECONDARY_DEACTIVATION_CARD_TIMESTAMP;
    }


    /**
     * Gets the PRIMARY_CARD_PRINT_REASON_ID value for this CARD_PRINT_STAGING.
     * 
     * @return PRIMARY_CARD_PRINT_REASON_ID
     */
    public java.lang.String getPRIMARY_CARD_PRINT_REASON_ID() {
        return PRIMARY_CARD_PRINT_REASON_ID;
    }


    /**
     * Sets the PRIMARY_CARD_PRINT_REASON_ID value for this CARD_PRINT_STAGING.
     * 
     * @param PRIMARY_CARD_PRINT_REASON_ID
     */
    public void setPRIMARY_CARD_PRINT_REASON_ID(java.lang.String PRIMARY_CARD_PRINT_REASON_ID) {
        this.PRIMARY_CARD_PRINT_REASON_ID = PRIMARY_CARD_PRINT_REASON_ID;
    }


    /**
     * Gets the SECONDARY_CARD_PRINT_REASON_ID value for this CARD_PRINT_STAGING.
     * 
     * @return SECONDARY_CARD_PRINT_REASON_ID
     */
    public java.lang.String getSECONDARY_CARD_PRINT_REASON_ID() {
        return SECONDARY_CARD_PRINT_REASON_ID;
    }


    /**
     * Sets the SECONDARY_CARD_PRINT_REASON_ID value for this CARD_PRINT_STAGING.
     * 
     * @param SECONDARY_CARD_PRINT_REASON_ID
     */
    public void setSECONDARY_CARD_PRINT_REASON_ID(java.lang.String SECONDARY_CARD_PRINT_REASON_ID) {
        this.SECONDARY_CARD_PRINT_REASON_ID = SECONDARY_CARD_PRINT_REASON_ID;
    }


    /**
     * Gets the PS_SECONDARY_LAST_NAME value for this CARD_PRINT_STAGING.
     * 
     * @return PS_SECONDARY_LAST_NAME
     */
    public java.lang.String getPS_SECONDARY_LAST_NAME() {
        return PS_SECONDARY_LAST_NAME;
    }


    /**
     * Sets the PS_SECONDARY_LAST_NAME value for this CARD_PRINT_STAGING.
     * 
     * @param PS_SECONDARY_LAST_NAME
     */
    public void setPS_SECONDARY_LAST_NAME(java.lang.String PS_SECONDARY_LAST_NAME) {
        this.PS_SECONDARY_LAST_NAME = PS_SECONDARY_LAST_NAME;
    }


    /**
     * Gets the PS_SECONDARY_FIRST_NAME value for this CARD_PRINT_STAGING.
     * 
     * @return PS_SECONDARY_FIRST_NAME
     */
    public java.lang.String getPS_SECONDARY_FIRST_NAME() {
        return PS_SECONDARY_FIRST_NAME;
    }


    /**
     * Sets the PS_SECONDARY_FIRST_NAME value for this CARD_PRINT_STAGING.
     * 
     * @param PS_SECONDARY_FIRST_NAME
     */
    public void setPS_SECONDARY_FIRST_NAME(java.lang.String PS_SECONDARY_FIRST_NAME) {
        this.PS_SECONDARY_FIRST_NAME = PS_SECONDARY_FIRST_NAME;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CARD_PRINT_STAGING)) return false;
        CARD_PRINT_STAGING other = (CARD_PRINT_STAGING) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.PS_EMPLOYEE_ID==null && other.getPS_EMPLOYEE_ID()==null) || 
             (this.PS_EMPLOYEE_ID!=null &&
              this.PS_EMPLOYEE_ID.equals(other.getPS_EMPLOYEE_ID()))) &&
            ((this.PRIMARY_ACTIVATION_CARD_TIMESTAMP==null && other.getPRIMARY_ACTIVATION_CARD_TIMESTAMP()==null) || 
             (this.PRIMARY_ACTIVATION_CARD_TIMESTAMP!=null &&
              this.PRIMARY_ACTIVATION_CARD_TIMESTAMP.equals(other.getPRIMARY_ACTIVATION_CARD_TIMESTAMP()))) &&
            ((this.PRIMARY_REPLACE_CARD_TIMESTAMP==null && other.getPRIMARY_REPLACE_CARD_TIMESTAMP()==null) || 
             (this.PRIMARY_REPLACE_CARD_TIMESTAMP!=null &&
              this.PRIMARY_REPLACE_CARD_TIMESTAMP.equals(other.getPRIMARY_REPLACE_CARD_TIMESTAMP()))) &&
            ((this.SECONDARY_CREATE_TIMESTAMP==null && other.getSECONDARY_CREATE_TIMESTAMP()==null) || 
             (this.SECONDARY_CREATE_TIMESTAMP!=null &&
              this.SECONDARY_CREATE_TIMESTAMP.equals(other.getSECONDARY_CREATE_TIMESTAMP()))) &&
            ((this.SECONDARY_REPLACE_CARD_TIMESTAMP==null && other.getSECONDARY_REPLACE_CARD_TIMESTAMP()==null) || 
             (this.SECONDARY_REPLACE_CARD_TIMESTAMP!=null &&
              this.SECONDARY_REPLACE_CARD_TIMESTAMP.equals(other.getSECONDARY_REPLACE_CARD_TIMESTAMP()))) &&
            ((this.SECONDARY_DEACTIVATION_CARD_TIMESTAMP==null && other.getSECONDARY_DEACTIVATION_CARD_TIMESTAMP()==null) || 
             (this.SECONDARY_DEACTIVATION_CARD_TIMESTAMP!=null &&
              this.SECONDARY_DEACTIVATION_CARD_TIMESTAMP.equals(other.getSECONDARY_DEACTIVATION_CARD_TIMESTAMP()))) &&
            ((this.PRIMARY_CARD_PRINT_REASON_ID==null && other.getPRIMARY_CARD_PRINT_REASON_ID()==null) || 
             (this.PRIMARY_CARD_PRINT_REASON_ID!=null &&
              this.PRIMARY_CARD_PRINT_REASON_ID.equals(other.getPRIMARY_CARD_PRINT_REASON_ID()))) &&
            ((this.SECONDARY_CARD_PRINT_REASON_ID==null && other.getSECONDARY_CARD_PRINT_REASON_ID()==null) || 
             (this.SECONDARY_CARD_PRINT_REASON_ID!=null &&
              this.SECONDARY_CARD_PRINT_REASON_ID.equals(other.getSECONDARY_CARD_PRINT_REASON_ID()))) &&
            ((this.PS_SECONDARY_LAST_NAME==null && other.getPS_SECONDARY_LAST_NAME()==null) || 
             (this.PS_SECONDARY_LAST_NAME!=null &&
              this.PS_SECONDARY_LAST_NAME.equals(other.getPS_SECONDARY_LAST_NAME()))) &&
            ((this.PS_SECONDARY_FIRST_NAME==null && other.getPS_SECONDARY_FIRST_NAME()==null) || 
             (this.PS_SECONDARY_FIRST_NAME!=null &&
              this.PS_SECONDARY_FIRST_NAME.equals(other.getPS_SECONDARY_FIRST_NAME())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPS_EMPLOYEE_ID() != null) {
            _hashCode += getPS_EMPLOYEE_ID().hashCode();
        }
        if (getPRIMARY_ACTIVATION_CARD_TIMESTAMP() != null) {
            _hashCode += getPRIMARY_ACTIVATION_CARD_TIMESTAMP().hashCode();
        }
        if (getPRIMARY_REPLACE_CARD_TIMESTAMP() != null) {
            _hashCode += getPRIMARY_REPLACE_CARD_TIMESTAMP().hashCode();
        }
        if (getSECONDARY_CREATE_TIMESTAMP() != null) {
            _hashCode += getSECONDARY_CREATE_TIMESTAMP().hashCode();
        }
        if (getSECONDARY_REPLACE_CARD_TIMESTAMP() != null) {
            _hashCode += getSECONDARY_REPLACE_CARD_TIMESTAMP().hashCode();
        }
        if (getSECONDARY_DEACTIVATION_CARD_TIMESTAMP() != null) {
            _hashCode += getSECONDARY_DEACTIVATION_CARD_TIMESTAMP().hashCode();
        }
        if (getPRIMARY_CARD_PRINT_REASON_ID() != null) {
            _hashCode += getPRIMARY_CARD_PRINT_REASON_ID().hashCode();
        }
        if (getSECONDARY_CARD_PRINT_REASON_ID() != null) {
            _hashCode += getSECONDARY_CARD_PRINT_REASON_ID().hashCode();
        }
        if (getPS_SECONDARY_LAST_NAME() != null) {
            _hashCode += getPS_SECONDARY_LAST_NAME().hashCode();
        }
        if (getPS_SECONDARY_FIRST_NAME() != null) {
            _hashCode += getPS_SECONDARY_FIRST_NAME().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CARD_PRINT_STAGING.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CARD_PRINT_STAGING", ">CARD_PRINT_STAGING"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PS_EMPLOYEE_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CARD_PRINT_STAGING", "PS_EMPLOYEE_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PRIMARY_ACTIVATION_CARD_TIMESTAMP");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CARD_PRINT_STAGING", "PRIMARY_ACTIVATION_CARD_TIMESTAMP"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PRIMARY_REPLACE_CARD_TIMESTAMP");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CARD_PRINT_STAGING", "PRIMARY_REPLACE_CARD_TIMESTAMP"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SECONDARY_CREATE_TIMESTAMP");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CARD_PRINT_STAGING", "SECONDARY_CREATE_TIMESTAMP"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SECONDARY_REPLACE_CARD_TIMESTAMP");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CARD_PRINT_STAGING", "SECONDARY_REPLACE_CARD_TIMESTAMP"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SECONDARY_DEACTIVATION_CARD_TIMESTAMP");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CARD_PRINT_STAGING", "SECONDARY_DEACTIVATION_CARD_TIMESTAMP"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PRIMARY_CARD_PRINT_REASON_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CARD_PRINT_STAGING", "PRIMARY_CARD_PRINT_REASON_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SECONDARY_CARD_PRINT_REASON_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CARD_PRINT_STAGING", "SECONDARY_CARD_PRINT_REASON_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PS_SECONDARY_LAST_NAME");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CARD_PRINT_STAGING", "PS_SECONDARY_LAST_NAME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PS_SECONDARY_FIRST_NAME");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/CARD_PRINT_STAGING", "PS_SECONDARY_FIRST_NAME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
