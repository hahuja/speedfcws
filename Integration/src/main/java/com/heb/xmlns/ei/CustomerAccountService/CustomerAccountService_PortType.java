/**
 * CustomerAccountService_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.xmlns.ei.CustomerAccountService;

public interface CustomerAccountService_PortType extends java.rmi.Remote {
    public com.heb.xmlns.ei.GetAccountInformation_Reply.GetAccountInformation_Reply getAccountInformation(com.heb.xmlns.ei.GetAccountInformation_Request.GetAccountInformation_Request getAccountInformation_Request) throws java.rmi.RemoteException, com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault;

    /**
     * getReceiptDetails
     */
    public com.heb.xmlns.ei.GetReceiptDetails_Reply.GetReceiptDetails_Reply getReceiptDetails(com.heb.xmlns.ei.GetReceiptDetails_Request.GetReceiptDetails_Request getReceiptDetails_Request) throws java.rmi.RemoteException, com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault;
    public com.heb.xmlns.ei.AssociateAccount_Reply.AssociateAccount_Reply associateAccount(com.heb.xmlns.ei.AssociateAccount_Request.AssociateAccount_Request associateAccount_Request) throws java.rmi.RemoteException, com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault;
    public com.heb.xmlns.ei.ProcessRetroRequest_Reply.ProcessRetroRequest_Reply processRetroRequest(com.heb.xmlns.ei.ProcessRetroRequest_Request.ProcessRetroRequest_Request processRetroRequest_Request) throws java.rmi.RemoteException, com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault;
    public com.heb.xmlns.ei.ProcessCard_Reply.ProcessCard_Reply processCard(com.heb.xmlns.ei.ProcessCard_Request.ProcessCard_Request processCard_Request) throws java.rmi.RemoteException, com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault;
    public com.heb.xmlns.ei.REASON_CODES.REASON_CODES[] getReasonCodes(com.heb.xmlns.ei.GetReasonCodes_Request.GetReasonCodes_Request getReasonCodes_Request) throws java.rmi.RemoteException, com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault;
    public com.heb.xmlns.ei.UpdateProfile_Reply.UpdateProfile_Reply updateProfile(com.heb.xmlns.ei.UpdateProfile_Request.UpdateProfile_Request updateProfile_Request) throws java.rmi.RemoteException, com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault;
    public com.heb.xmlns.ei.GetPin_Reply.GetPin_Reply getPin(com.heb.xmlns.ei.GetPin_Request.GetPin_Request getPin_Request) throws java.rmi.RemoteException, com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault;
    public com.heb.xmlns.ei.GetDccMemberAccountInformation_Reply.GetDccMemberAccountInformation_ReplyDccMemberAccountInformation[] getDccMemberAccountInformation(com.heb.xmlns.ei.GetDccMemberAccountInformation_Request.GetDccMemberAccountInformation_Request getDccMemberAccountInformation_Request) throws java.rmi.RemoteException, com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault;
    public com.heb.xmlns.ei.GetPointsAndStoredValueAmount_Reply.GetPointsAndStoredValueAmount_Reply getPointsAndStoredValueAmount(com.heb.xmlns.ei.GetPointsAndStoredValueAmount_Request.GetPointsAndStoredValueAmount_Request getPointsAndStoredValueAmount_Request) throws java.rmi.RemoteException, com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault;
    public com.heb.xmlns.ei.GetPcrStatementDetails_Reply.PCRCustomerDetails getPcrStatementDetails(com.heb.xmlns.ei.GetPcrStatementDetails_Request.GetPcrStatementDetails_Request getPcrStatementDetails_Request) throws java.rmi.RemoteException, com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault;
}
