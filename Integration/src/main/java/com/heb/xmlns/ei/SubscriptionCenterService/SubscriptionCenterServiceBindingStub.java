/**
 * SubscriptionCenterServiceBindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.xmlns.ei.SubscriptionCenterService;

public class SubscriptionCenterServiceBindingStub extends org.apache.axis.client.Stub implements com.heb.xmlns.ei.SubscriptionCenterService.SubscriptionCenterService_PortType {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[7];
        _initOperationDesc1();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getSubscriptionStatus");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/GetSubscriptionStatus_Request", "GetSubscriptionStatus_Request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/GetSubscriptionStatus_Request", ">GetSubscriptionStatus_Request"), com.heb.xmlns.ei.SubscriptionCenterService.GetSubscriptionStatus_Request.GetSubscriptionStatus_Request.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/GetSubscriptionStatus_Reply", ">GetSubscriptionStatus_Reply"));
        oper.setReturnClass(com.heb.xmlns.ei.SubscriptionCenterService.GetSubscriptionStatus_Reply.GetSubscriptionStatus_Reply.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/GetSubscriptionStatus_Reply", "GetSubscriptionStatus_Reply"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProviderFaultSchema", "ProviderSOAPFault"),
                      "com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault",
                      new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProviderFaultSchema", ">ProviderSOAPFault"), 
                      true
                     ));
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("subscribe");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/Subscribe_Request", "Subscribe_Request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/Subscribe_Request", ">Subscribe_Request"), com.heb.xmlns.ei.SubscriptionCenterService.Subscribe_Request.Subscribe_Request.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/Subscribe_Reply", ">Subscribe_Reply"));
        oper.setReturnClass(com.heb.xmlns.ei.SubscriptionCenterService.Subscribe_Reply.Subscribe_Reply.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/Subscribe_Reply", "Subscribe_Reply"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProviderFaultSchema", "ProviderSOAPFault"),
                      "com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault",
                      new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProviderFaultSchema", ">ProviderSOAPFault"), 
                      true
                     ));
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("listSubscriptions");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/ListSubscriptions_Request", "ListSubscriptions_Request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/ListSubscriptions_Request", ">ListSubscriptions_Request"), com.heb.xmlns.ei.SubscriptionCenterService.ListSubscriptions_Request.ListSubscriptions_Request.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/ListSubscriptions_Reply", ">ListSubscriptions_Reply"));
        oper.setReturnClass(com.heb.xmlns.ei.SubscriptionCenterService.ListSubscriptions_Reply.ListSubscriptions_Reply.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/ListSubscriptions_Reply", "ListSubscriptions_Reply"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProviderFaultSchema", "ProviderSOAPFault"),
                      "com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault",
                      new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProviderFaultSchema", ">ProviderSOAPFault"), 
                      true
                     ));
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("verifyMobileDirectoryNumber");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/VerifyMobileDirectoryNumber_Request", "VerifyMobileDirectoryNumber_Request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/VerifyMobileDirectoryNumber_Request", ">VerifyMobileDirectoryNumber_Request"), com.heb.xmlns.ei.SubscriptionCenterService.VerifyMobileDirectoryNumber_Request.VerifyMobileDirectoryNumber_Request.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/VerifyMobileDirectoryNumber_Reply", ">VerifyMobileDirectoryNumber_Reply"));
        oper.setReturnClass(com.heb.xmlns.ei.SubscriptionCenterService.VerifyMobileDirectoryNumber_Reply.VerifyMobileDirectoryNumber_Reply.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/VerifyMobileDirectoryNumber_Reply", "VerifyMobileDirectoryNumber_Reply"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProviderFaultSchema", "ProviderSOAPFault"),
                      "com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault",
                      new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProviderFaultSchema", ">ProviderSOAPFault"), 
                      true
                     ));
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("modifySubscriptions");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/ModifySubscriptions_Request", "ModifySubscriptions_Request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/ModifySubscriptions_Request", ">ModifySubscriptions_Request"), com.heb.xmlns.ei.SubscriptionCenterService.ModifySubscriptions_Request.ModifySubscriptions_Request.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/ModifySubscriptions_Reply", ">ModifySubscriptions_Reply"));
        oper.setReturnClass(com.heb.xmlns.ei.SubscriptionCenterService.ModifySubscriptions_Reply.ModifySubscriptions_Reply.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/ModifySubscriptions_Reply", "ModifySubscriptions_Reply"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProviderFaultSchema", "ProviderSOAPFault"),
                      "com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault",
                      new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProviderFaultSchema", ">ProviderSOAPFault"), 
                      true
                     ));
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("unsubscribe");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/Unsubscribe_Request", "Unsubscribe_Request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/Unsubscribe_Request", ">Unsubscribe_Request"), com.heb.xmlns.ei.SubscriptionCenterService.Unsubscribe_Request.Unsubscribe_Request.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/Unsubscribe_Reply", ">Unsubscribe_Reply"));
        oper.setReturnClass(com.heb.xmlns.ei.SubscriptionCenterService.Unsubscribe_Reply.Unsubscribe_Reply.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/Unsubscribe_Reply", "Unsubscribe_Reply"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProviderFaultSchema", "ProviderSOAPFault"),
                      "com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault",
                      new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProviderFaultSchema", ">ProviderSOAPFault"), 
                      true
                     ));
        _operations[5] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("sendAlert");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Salt3", "send-message-request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Salt3", ">send-message-request"), com.heb.xmlns.ei.Salt3.SendMessageRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Salt3", ">send-message-response"));
        oper.setReturnClass(com.heb.xmlns.ei.Salt3.SendMessageResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Salt3", "send-message-response"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProviderFaultSchema", "ProviderSOAPFault"),
                      "com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault",
                      new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProviderFaultSchema", ">ProviderSOAPFault"), 
                      true
                     ));
        _operations[6] = oper;

    }

    public SubscriptionCenterServiceBindingStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public SubscriptionCenterServiceBindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public SubscriptionCenterServiceBindingStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Authentication", ">Authentication");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.Authentication.Authentication.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProviderFaultSchema", ">ProviderSOAPErrorMsg");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPErrorMsg.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/ProviderFaultSchema", ">ProviderSOAPFault");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Salt2", "ErrorCode");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.Salt2.ErrorCode.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Salt2", "ErrorCodes");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.Salt2.ErrorCode[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Salt2", "ErrorCode");
            qName2 = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Salt2", "error-code");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Salt2", "SubscriptionStatusCode");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.Salt2.SubscriptionStatusCode.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Salt3", ">>>send-message-request>template>arguments");
            cachedSerQNames.add(qName);
            cls = java.lang.String[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string");
            qName2 = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Salt3", "argument");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Salt3", ">>send-message-request>template");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.Salt3.SendMessageRequestTemplate.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Salt3", ">send-message-request");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.Salt3.SendMessageRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Salt3", ">send-message-response");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.Salt3.SendMessageResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/Salt3", "ErrorCode");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.Salt3.ErrorCode.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/GetSubscriptionStatus_Reply", ">GetSubscriptionStatus_Reply");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.SubscriptionCenterService.GetSubscriptionStatus_Reply.GetSubscriptionStatus_Reply.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/GetSubscriptionStatus_Request", ">GetSubscriptionStatus_Request");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.SubscriptionCenterService.GetSubscriptionStatus_Request.GetSubscriptionStatus_Request.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/ListSubscriptions_Reply", ">>>ListSubscriptions_Reply>Keywords>Keyword");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.SubscriptionCenterService.ListSubscriptions_Reply.ListSubscriptions_ReplyKeywordsKeyword.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/ListSubscriptions_Reply", ">>ListSubscriptions_Reply>Keywords");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.SubscriptionCenterService.ListSubscriptions_Reply.ListSubscriptions_ReplyKeywordsKeyword[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/ListSubscriptions_Reply", ">>>ListSubscriptions_Reply>Keywords>Keyword");
            qName2 = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/ListSubscriptions_Reply", "Keyword");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/ListSubscriptions_Reply", ">ListSubscriptions_Reply");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.SubscriptionCenterService.ListSubscriptions_Reply.ListSubscriptions_Reply.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/ListSubscriptions_Reply", "AudienceCode");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.SubscriptionCenterService.ListSubscriptions_Reply.AudienceCode.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/ListSubscriptions_Reply", "SubscriptionStatusCode");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.SubscriptionCenterService.ListSubscriptions_Reply.SubscriptionStatusCode.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/ListSubscriptions_Reply", "VerificationStatusCode");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.SubscriptionCenterService.ListSubscriptions_Reply.VerificationStatusCode.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/ListSubscriptions_Request", ">ListSubscriptions_Request");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.SubscriptionCenterService.ListSubscriptions_Request.ListSubscriptions_Request.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/ModifySubscriptions_Reply", ">ModifySubscriptions_Reply");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.SubscriptionCenterService.ModifySubscriptions_Reply.ModifySubscriptions_Reply.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/ModifySubscriptions_Reply", "VerificationStatusCode");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.SubscriptionCenterService.ModifySubscriptions_Reply.VerificationStatusCode.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/ModifySubscriptions_Request", ">>ModifySubscriptions_Request>Sub_Keywords");
            cachedSerQNames.add(qName);
            cls = java.lang.String[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string");
            qName2 = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/ModifySubscriptions_Request", "Keyword");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/ModifySubscriptions_Request", ">>ModifySubscriptions_Request>Unsub_Keywords");
            cachedSerQNames.add(qName);
            cls = java.lang.String[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string");
            qName2 = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/ModifySubscriptions_Request", "Keyword");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/ModifySubscriptions_Request", ">ModifySubscriptions_Request");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.SubscriptionCenterService.ModifySubscriptions_Request.ModifySubscriptions_Request.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/Subscribe_Reply", ">Subscribe_Reply");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.SubscriptionCenterService.Subscribe_Reply.Subscribe_Reply.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/Subscribe_Request", ">Subscribe_Request");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.SubscriptionCenterService.Subscribe_Request.Subscribe_Request.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/Unsubscribe_Reply", ">Unsubscribe_Reply");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.SubscriptionCenterService.Unsubscribe_Reply.Unsubscribe_Reply.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/Unsubscribe_Request", ">Unsubscribe_Request");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.SubscriptionCenterService.Unsubscribe_Request.Unsubscribe_Request.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/VerifyMobileDirectoryNumber_Reply", ">VerifyMobileDirectoryNumber_Reply");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.SubscriptionCenterService.VerifyMobileDirectoryNumber_Reply.VerifyMobileDirectoryNumber_Reply.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/VerifyMobileDirectoryNumber_Reply", "VerificationStatusCode");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.SubscriptionCenterService.VerifyMobileDirectoryNumber_Reply.VerificationStatusCode.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://xmlns.heb.com/ei/SubscriptionCenterService/VerifyMobileDirectoryNumber_Request", ">VerifyMobileDirectoryNumber_Request");
            cachedSerQNames.add(qName);
            cls = com.heb.xmlns.ei.SubscriptionCenterService.VerifyMobileDirectoryNumber_Request.VerifyMobileDirectoryNumber_Request.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public com.heb.xmlns.ei.SubscriptionCenterService.GetSubscriptionStatus_Reply.GetSubscriptionStatus_Reply getSubscriptionStatus(com.heb.xmlns.ei.SubscriptionCenterService.GetSubscriptionStatus_Request.GetSubscriptionStatus_Request getSubscriptionStatus_Request) throws java.rmi.RemoteException, com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("/SubscriptionCenterService/getSubscriptionStatus");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getSubscriptionStatus"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getSubscriptionStatus_Request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.heb.xmlns.ei.SubscriptionCenterService.GetSubscriptionStatus_Reply.GetSubscriptionStatus_Reply) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.heb.xmlns.ei.SubscriptionCenterService.GetSubscriptionStatus_Reply.GetSubscriptionStatus_Reply) org.apache.axis.utils.JavaUtils.convert(_resp, com.heb.xmlns.ei.SubscriptionCenterService.GetSubscriptionStatus_Reply.GetSubscriptionStatus_Reply.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault) {
              throw (com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.heb.xmlns.ei.SubscriptionCenterService.Subscribe_Reply.Subscribe_Reply subscribe(com.heb.xmlns.ei.SubscriptionCenterService.Subscribe_Request.Subscribe_Request subscribe_Request) throws java.rmi.RemoteException, com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("/SubscriptionCenterService/subscribe");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "subscribe"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {subscribe_Request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.heb.xmlns.ei.SubscriptionCenterService.Subscribe_Reply.Subscribe_Reply) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.heb.xmlns.ei.SubscriptionCenterService.Subscribe_Reply.Subscribe_Reply) org.apache.axis.utils.JavaUtils.convert(_resp, com.heb.xmlns.ei.SubscriptionCenterService.Subscribe_Reply.Subscribe_Reply.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault) {
              throw (com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.heb.xmlns.ei.SubscriptionCenterService.ListSubscriptions_Reply.ListSubscriptions_Reply listSubscriptions(com.heb.xmlns.ei.SubscriptionCenterService.ListSubscriptions_Request.ListSubscriptions_Request listSubscriptions_Request) throws java.rmi.RemoteException, com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("/SubscriptionCenterService/listSubscriptions");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "listSubscriptions"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {listSubscriptions_Request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.heb.xmlns.ei.SubscriptionCenterService.ListSubscriptions_Reply.ListSubscriptions_Reply) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.heb.xmlns.ei.SubscriptionCenterService.ListSubscriptions_Reply.ListSubscriptions_Reply) org.apache.axis.utils.JavaUtils.convert(_resp, com.heb.xmlns.ei.SubscriptionCenterService.ListSubscriptions_Reply.ListSubscriptions_Reply.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault) {
              throw (com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.heb.xmlns.ei.SubscriptionCenterService.VerifyMobileDirectoryNumber_Reply.VerifyMobileDirectoryNumber_Reply verifyMobileDirectoryNumber(com.heb.xmlns.ei.SubscriptionCenterService.VerifyMobileDirectoryNumber_Request.VerifyMobileDirectoryNumber_Request verifyMobileDirectoryNumber_Request) throws java.rmi.RemoteException, com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("/SubscriptionCenterService/verifyMobileDirectoryNumber");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "verifyMobileDirectoryNumber"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {verifyMobileDirectoryNumber_Request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.heb.xmlns.ei.SubscriptionCenterService.VerifyMobileDirectoryNumber_Reply.VerifyMobileDirectoryNumber_Reply) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.heb.xmlns.ei.SubscriptionCenterService.VerifyMobileDirectoryNumber_Reply.VerifyMobileDirectoryNumber_Reply) org.apache.axis.utils.JavaUtils.convert(_resp, com.heb.xmlns.ei.SubscriptionCenterService.VerifyMobileDirectoryNumber_Reply.VerifyMobileDirectoryNumber_Reply.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault) {
              throw (com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.heb.xmlns.ei.SubscriptionCenterService.ModifySubscriptions_Reply.ModifySubscriptions_Reply modifySubscriptions(com.heb.xmlns.ei.SubscriptionCenterService.ModifySubscriptions_Request.ModifySubscriptions_Request modifySubscriptions_Request) throws java.rmi.RemoteException, com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("/SubscriptionCenterService/modifySubscriptions");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "modifySubscriptions"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {modifySubscriptions_Request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.heb.xmlns.ei.SubscriptionCenterService.ModifySubscriptions_Reply.ModifySubscriptions_Reply) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.heb.xmlns.ei.SubscriptionCenterService.ModifySubscriptions_Reply.ModifySubscriptions_Reply) org.apache.axis.utils.JavaUtils.convert(_resp, com.heb.xmlns.ei.SubscriptionCenterService.ModifySubscriptions_Reply.ModifySubscriptions_Reply.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault) {
              throw (com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.heb.xmlns.ei.SubscriptionCenterService.Unsubscribe_Reply.Unsubscribe_Reply unsubscribe(com.heb.xmlns.ei.SubscriptionCenterService.Unsubscribe_Request.Unsubscribe_Request unsubscribe_Request) throws java.rmi.RemoteException, com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("/SubscriptionCenterService/unsubscribe");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "unsubscribe"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {unsubscribe_Request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.heb.xmlns.ei.SubscriptionCenterService.Unsubscribe_Reply.Unsubscribe_Reply) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.heb.xmlns.ei.SubscriptionCenterService.Unsubscribe_Reply.Unsubscribe_Reply) org.apache.axis.utils.JavaUtils.convert(_resp, com.heb.xmlns.ei.SubscriptionCenterService.Unsubscribe_Reply.Unsubscribe_Reply.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault) {
              throw (com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.heb.xmlns.ei.Salt3.SendMessageResponse sendAlert(com.heb.xmlns.ei.Salt3.SendMessageRequest sendAlert_Request) throws java.rmi.RemoteException, com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[6]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("/SubscriptionCenterService/sendAlert");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "sendAlert"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {sendAlert_Request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.heb.xmlns.ei.Salt3.SendMessageResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.heb.xmlns.ei.Salt3.SendMessageResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.heb.xmlns.ei.Salt3.SendMessageResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault) {
              throw (com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

}
