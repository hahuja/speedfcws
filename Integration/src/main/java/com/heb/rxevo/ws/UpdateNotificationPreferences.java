/**
 * UpdateNotificationPreferences.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.rxevo.ws;

public class UpdateNotificationPreferences  implements java.io.Serializable {
    private com.heb.rxevo.ws.Token token;

    private java.lang.String patientId;

    private boolean byEmail;

    private boolean byPhone;

    private boolean byText;

    public UpdateNotificationPreferences() {
    }

    public UpdateNotificationPreferences(
           com.heb.rxevo.ws.Token token,
           java.lang.String patientId,
           boolean byEmail,
           boolean byPhone,
           boolean byText) {
           this.token = token;
           this.patientId = patientId;
           this.byEmail = byEmail;
           this.byPhone = byPhone;
           this.byText = byText;
    }


    /**
     * Gets the token value for this UpdateNotificationPreferences.
     * 
     * @return token
     */
    public com.heb.rxevo.ws.Token getToken() {
        return token;
    }


    /**
     * Sets the token value for this UpdateNotificationPreferences.
     * 
     * @param token
     */
    public void setToken(com.heb.rxevo.ws.Token token) {
        this.token = token;
    }


    /**
     * Gets the patientId value for this UpdateNotificationPreferences.
     * 
     * @return patientId
     */
    public java.lang.String getPatientId() {
        return patientId;
    }


    /**
     * Sets the patientId value for this UpdateNotificationPreferences.
     * 
     * @param patientId
     */
    public void setPatientId(java.lang.String patientId) {
        this.patientId = patientId;
    }


    /**
     * Gets the byEmail value for this UpdateNotificationPreferences.
     * 
     * @return byEmail
     */
    public boolean isByEmail() {
        return byEmail;
    }


    /**
     * Sets the byEmail value for this UpdateNotificationPreferences.
     * 
     * @param byEmail
     */
    public void setByEmail(boolean byEmail) {
        this.byEmail = byEmail;
    }


    /**
     * Gets the byPhone value for this UpdateNotificationPreferences.
     * 
     * @return byPhone
     */
    public boolean isByPhone() {
        return byPhone;
    }


    /**
     * Sets the byPhone value for this UpdateNotificationPreferences.
     * 
     * @param byPhone
     */
    public void setByPhone(boolean byPhone) {
        this.byPhone = byPhone;
    }


    /**
     * Gets the byText value for this UpdateNotificationPreferences.
     * 
     * @return byText
     */
    public boolean isByText() {
        return byText;
    }


    /**
     * Sets the byText value for this UpdateNotificationPreferences.
     * 
     * @param byText
     */
    public void setByText(boolean byText) {
        this.byText = byText;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UpdateNotificationPreferences)) return false;
        UpdateNotificationPreferences other = (UpdateNotificationPreferences) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.token==null && other.getToken()==null) || 
             (this.token!=null &&
              this.token.equals(other.getToken()))) &&
            ((this.patientId==null && other.getPatientId()==null) || 
             (this.patientId!=null &&
              this.patientId.equals(other.getPatientId()))) &&
            this.byEmail == other.isByEmail() &&
            this.byPhone == other.isByPhone() &&
            this.byText == other.isByText();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getToken() != null) {
            _hashCode += getToken().hashCode();
        }
        if (getPatientId() != null) {
            _hashCode += getPatientId().hashCode();
        }
        _hashCode += (isByEmail() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        _hashCode += (isByPhone() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        _hashCode += (isByText() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(UpdateNotificationPreferences.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "updateNotificationPreferences"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("token");
        elemField.setXmlName(new javax.xml.namespace.QName("", "token"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "token"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("patientId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "patientId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("byEmail");
        elemField.setXmlName(new javax.xml.namespace.QName("", "byEmail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("byPhone");
        elemField.setXmlName(new javax.xml.namespace.QName("", "byPhone"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("byText");
        elemField.setXmlName(new javax.xml.namespace.QName("", "byText"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
