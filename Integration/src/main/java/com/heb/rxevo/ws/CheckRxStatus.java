/**
 * CheckRxStatus.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.rxevo.ws;

public class CheckRxStatus  implements java.io.Serializable {
    private com.heb.rxevo.ws.Token token;

    private java.lang.String rxId;

    public CheckRxStatus() {
    }

    public CheckRxStatus(
           com.heb.rxevo.ws.Token token,
           java.lang.String rxId) {
           this.token = token;
           this.rxId = rxId;
    }


    /**
     * Gets the token value for this CheckRxStatus.
     * 
     * @return token
     */
    public com.heb.rxevo.ws.Token getToken() {
        return token;
    }


    /**
     * Sets the token value for this CheckRxStatus.
     * 
     * @param token
     */
    public void setToken(com.heb.rxevo.ws.Token token) {
        this.token = token;
    }


    /**
     * Gets the rxId value for this CheckRxStatus.
     * 
     * @return rxId
     */
    public java.lang.String getRxId() {
        return rxId;
    }


    /**
     * Sets the rxId value for this CheckRxStatus.
     * 
     * @param rxId
     */
    public void setRxId(java.lang.String rxId) {
        this.rxId = rxId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CheckRxStatus)) return false;
        CheckRxStatus other = (CheckRxStatus) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.token==null && other.getToken()==null) || 
             (this.token!=null &&
              this.token.equals(other.getToken()))) &&
            ((this.rxId==null && other.getRxId()==null) || 
             (this.rxId!=null &&
              this.rxId.equals(other.getRxId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getToken() != null) {
            _hashCode += getToken().hashCode();
        }
        if (getRxId() != null) {
            _hashCode += getRxId().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CheckRxStatus.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "checkRxStatus"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("token");
        elemField.setXmlName(new javax.xml.namespace.QName("", "token"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "token"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rxId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "rxId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
