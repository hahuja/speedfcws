/**
 * RxEvoServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.rxevo.ws;

public class RxEvoServiceLocator extends org.apache.axis.client.Service implements com.heb.rxevo.ws.RxEvoService {

    public RxEvoServiceLocator() {
    }


    public RxEvoServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public RxEvoServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for HebRx_HebRxServiceSoapBinding_HTTPPort
    private java.lang.String HebRx_HebRxServiceSoapBinding_HTTPPort_address = "http://lctibbc01.heb.com:8181/dmz/SOAP?host=HEB&amp;tpname=ECOM&amp;opid=HebRx%2FHebRxServiceSoapBinding";

    public java.lang.String getHebRx_HebRxServiceSoapBinding_HTTPPortAddress() {
        return HebRx_HebRxServiceSoapBinding_HTTPPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String HebRx_HebRxServiceSoapBinding_HTTPPortWSDDServiceName = "HebRx_HebRxServiceSoapBinding_HTTPPort";

    public java.lang.String getHebRx_HebRxServiceSoapBinding_HTTPPortWSDDServiceName() {
        return HebRx_HebRxServiceSoapBinding_HTTPPortWSDDServiceName;
    }

    public void setHebRx_HebRxServiceSoapBinding_HTTPPortWSDDServiceName(java.lang.String name) {
        HebRx_HebRxServiceSoapBinding_HTTPPortWSDDServiceName = name;
    }

    public com.heb.rxevo.ws.HebRx getHebRx_HebRxServiceSoapBinding_HTTPPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(HebRx_HebRxServiceSoapBinding_HTTPPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getHebRx_HebRxServiceSoapBinding_HTTPPort(endpoint);
    }

    public com.heb.rxevo.ws.HebRx getHebRx_HebRxServiceSoapBinding_HTTPPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub _stub = new com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub(portAddress, this);
            _stub.setPortName(getHebRx_HebRxServiceSoapBinding_HTTPPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setHebRx_HebRxServiceSoapBinding_HTTPPortEndpointAddress(java.lang.String address) {
        HebRx_HebRxServiceSoapBinding_HTTPPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.heb.rxevo.ws.HebRx.class.isAssignableFrom(serviceEndpointInterface)) {
                com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub _stub = new com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub(new java.net.URL(HebRx_HebRxServiceSoapBinding_HTTPPort_address), this);
                _stub.setPortName(getHebRx_HebRxServiceSoapBinding_HTTPPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("HebRx_HebRxServiceSoapBinding_HTTPPort".equals(inputPortName)) {
            return getHebRx_HebRxServiceSoapBinding_HTTPPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://ws.rxevo.heb.com/", "RxEvoService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://ws.rxevo.heb.com/", "HebRx_HebRxServiceSoapBinding_HTTPPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("HebRx_HebRxServiceSoapBinding_HTTPPort".equals(portName)) {
            setHebRx_HebRxServiceSoapBinding_HTTPPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
