/**
 * OrderItem.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.rxevo.ws;

public class OrderItem  implements java.io.Serializable {
    private java.lang.String rxId;

    private java.lang.String action;

    private java.lang.String pickupCorpNumber;

    private java.util.Calendar pickupTime;

    public OrderItem() {
    }

    public OrderItem(
           java.lang.String rxId,
           java.lang.String action,
           java.lang.String pickupCorpNumber,
           java.util.Calendar pickupTime) {
           this.rxId = rxId;
           this.action = action;
           this.pickupCorpNumber = pickupCorpNumber;
           this.pickupTime = pickupTime;
    }


    /**
     * Gets the rxId value for this OrderItem.
     * 
     * @return rxId
     */
    public java.lang.String getRxId() {
        return rxId;
    }


    /**
     * Sets the rxId value for this OrderItem.
     * 
     * @param rxId
     */
    public void setRxId(java.lang.String rxId) {
        this.rxId = rxId;
    }


    /**
     * Gets the action value for this OrderItem.
     * 
     * @return action
     */
    public java.lang.String getAction() {
        return action;
    }


    /**
     * Sets the action value for this OrderItem.
     * 
     * @param action
     */
    public void setAction(java.lang.String action) {
        this.action = action;
    }


    /**
     * Gets the pickupCorpNumber value for this OrderItem.
     * 
     * @return pickupCorpNumber
     */
    public java.lang.String getPickupCorpNumber() {
        return pickupCorpNumber;
    }


    /**
     * Sets the pickupCorpNumber value for this OrderItem.
     * 
     * @param pickupCorpNumber
     */
    public void setPickupCorpNumber(java.lang.String pickupCorpNumber) {
        this.pickupCorpNumber = pickupCorpNumber;
    }


    /**
     * Gets the pickupTime value for this OrderItem.
     * 
     * @return pickupTime
     */
    public java.util.Calendar getPickupTime() {
        return pickupTime;
    }


    /**
     * Sets the pickupTime value for this OrderItem.
     * 
     * @param pickupTime
     */
    public void setPickupTime(java.util.Calendar pickupTime) {
        this.pickupTime = pickupTime;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OrderItem)) return false;
        OrderItem other = (OrderItem) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.rxId==null && other.getRxId()==null) || 
             (this.rxId!=null &&
              this.rxId.equals(other.getRxId()))) &&
            ((this.action==null && other.getAction()==null) || 
             (this.action!=null &&
              this.action.equals(other.getAction()))) &&
            ((this.pickupCorpNumber==null && other.getPickupCorpNumber()==null) || 
             (this.pickupCorpNumber!=null &&
              this.pickupCorpNumber.equals(other.getPickupCorpNumber()))) &&
            ((this.pickupTime==null && other.getPickupTime()==null) || 
             (this.pickupTime!=null &&
              this.pickupTime.equals(other.getPickupTime())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getRxId() != null) {
            _hashCode += getRxId().hashCode();
        }
        if (getAction() != null) {
            _hashCode += getAction().hashCode();
        }
        if (getPickupCorpNumber() != null) {
            _hashCode += getPickupCorpNumber().hashCode();
        }
        if (getPickupTime() != null) {
            _hashCode += getPickupTime().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OrderItem.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "orderItem"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rxId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "rxId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("action");
        elemField.setXmlName(new javax.xml.namespace.QName("", "action"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pickupCorpNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pickupCorpNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pickupTime");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pickupTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
