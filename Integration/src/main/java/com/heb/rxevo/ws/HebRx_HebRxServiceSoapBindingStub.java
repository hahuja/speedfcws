/**
 * HebRx_HebRxServiceSoapBindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.rxevo.ws;

public class HebRx_HebRxServiceSoapBindingStub extends org.apache.axis.client.Stub implements com.heb.rxevo.ws.HebRx {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[32];
        _initOperationDesc1();
        _initOperationDesc2();
        _initOperationDesc3();
        _initOperationDesc4();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getInsurances");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "getInsurances"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "getInsurances"), com.heb.rxevo.ws.GetInsurances.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", "PartyInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", ">PartyInfo"), com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo.class, true, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "getInsurancesResponse"));
        oper.setReturnClass(com.heb.rxevo.ws.GetInsurancesResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "getInsurancesResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("addAllergies");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "addAllergies"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "addAllergies"), com.heb.rxevo.ws.AddAllergies.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", "PartyInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", ">PartyInfo"), com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo.class, true, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "addAllergiesResponse"));
        oper.setReturnClass(com.heb.rxevo.ws.AddAllergiesResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "addAllergiesResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getPrescribers");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "getPrescribers"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "getPrescribers"), com.heb.rxevo.ws.GetPrescribers.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", "PartyInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", ">PartyInfo"), com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo.class, true, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "getPrescribersResponse"));
        oper.setReturnClass(com.heb.rxevo.ws.GetPrescribersResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "getPrescribersResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getPrescriptionHistory");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "getPrescriptionHistory"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "getPrescriptionHistory"), com.heb.rxevo.ws.GetPrescriptionHistory.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", "PartyInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", ">PartyInfo"), com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo.class, true, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "getPrescriptionHistoryResponse"));
        oper.setReturnClass(com.heb.rxevo.ws.GetPrescriptionHistoryResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "getPrescriptionHistoryResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("updatePatientPhoneNumber");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "updatePatientPhoneNumber"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "updatePatientPhoneNumber"), com.heb.rxevo.ws.UpdatePatientPhoneNumber.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", "PartyInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", ">PartyInfo"), com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo.class, true, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "updatePatientPhoneNumberResponse"));
        oper.setReturnClass(com.heb.rxevo.ws.UpdatePatientPhoneNumberResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "updatePatientPhoneNumberResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getAllergies");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "getAllergies"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "getAllergies"), com.heb.rxevo.ws.GetAllergies.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", "PartyInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", ">PartyInfo"), com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo.class, true, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "getAllergiesResponse"));
        oper.setReturnClass(com.heb.rxevo.ws.GetAllergiesResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "getAllergiesResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[5] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("addInsurance");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "addInsurance"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "addInsurance"), com.heb.rxevo.ws.AddInsurance.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", "PartyInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", ">PartyInfo"), com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo.class, true, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "addInsuranceResponse"));
        oper.setReturnClass(com.heb.rxevo.ws.AddInsuranceResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "addInsuranceResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[6] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("updatePatientMobileNumber");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "updatePatientMobileNumber"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "updatePatientMobileNumber"), com.heb.rxevo.ws.UpdatePatientMobileNumber.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", "PartyInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", ">PartyInfo"), com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo.class, true, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "updatePatientMobileNumberResponse"));
        oper.setReturnClass(com.heb.rxevo.ws.UpdatePatientMobileNumberResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "updatePatientMobileNumberResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[7] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("submitNewPrescription");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "submitNewPrescription"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "submitNewPrescription"), com.heb.rxevo.ws.SubmitNewPrescription.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", "PartyInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", ">PartyInfo"), com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo.class, true, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "submitNewPrescriptionResponse"));
        oper.setReturnClass(com.heb.rxevo.ws.SubmitNewPrescriptionResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "submitNewPrescriptionResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[8] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("submitTransferRequest");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "submitTransferRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "submitTransferRequest"), com.heb.rxevo.ws.SubmitTransferRequest.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", "PartyInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", ">PartyInfo"), com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo.class, true, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "submitTransferRequestResponse"));
        oper.setReturnClass(com.heb.rxevo.ws.SubmitTransferRequestResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "submitTransferRequestResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[9] = oper;

    }

    private static void _initOperationDesc2(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("updateRefillExpressForRx");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "updateRefillExpressForRx"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "updateRefillExpressForRx"), com.heb.rxevo.ws.UpdateRefillExpressForRx.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", "PartyInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", ">PartyInfo"), com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo.class, true, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "updateRefillExpressForRxResponse"));
        oper.setReturnClass(com.heb.rxevo.ws.UpdateRefillExpressForRxResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "updateRefillExpressForRxResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[10] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("enableRefillExpressForPatient");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "enableRefillExpressForPatient"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "enableRefillExpressForPatient"), com.heb.rxevo.ws.EnableRefillExpressForPatient.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", "PartyInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", ">PartyInfo"), com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo.class, true, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "enableRefillExpressForPatientResponse"));
        oper.setReturnClass(com.heb.rxevo.ws.EnableRefillExpressForPatientResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "enableRefillExpressForPatientResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[11] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("checkRxStatus");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "checkRxStatus"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "checkRxStatus"), com.heb.rxevo.ws.CheckRxStatus.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", "PartyInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", ">PartyInfo"), com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo.class, true, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "checkRxStatusResponse"));
        oper.setReturnClass(com.heb.rxevo.ws.CheckRxStatusResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "checkRxStatusResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[12] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("updateNotificationPreferences");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "updateNotificationPreferences"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "updateNotificationPreferences"), com.heb.rxevo.ws.UpdateNotificationPreferences.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", "PartyInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", ">PartyInfo"), com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo.class, true, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "updateNotificationPreferencesResponse"));
        oper.setReturnClass(com.heb.rxevo.ws.UpdateNotificationPreferencesResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "updateNotificationPreferencesResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[13] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("updateRefillExpressForPatient");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "updateRefillExpressForPatient"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "updateRefillExpressForPatient"), com.heb.rxevo.ws.UpdateRefillExpressForPatient.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", "PartyInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", ">PartyInfo"), com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo.class, true, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "updateRefillExpressForPatientResponse"));
        oper.setReturnClass(com.heb.rxevo.ws.UpdateRefillExpressForPatientResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "updateRefillExpressForPatientResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[14] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getRevoPatient");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "getRevoPatient"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "getRevoPatient"), com.heb.rxevo.ws.GetRevoPatient.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", "PartyInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", ">PartyInfo"), com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo.class, true, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "getRevoPatientResponse"));
        oper.setReturnClass(com.heb.rxevo.ws.GetRevoPatientResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "getRevoPatientResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[15] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("validatePatient");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "validatePatient"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "validatePatient"), com.heb.rxevo.ws.ValidatePatient.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", "PartyInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", ">PartyInfo"), com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo.class, true, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "validatePatientResponse"));
        oper.setReturnClass(com.heb.rxevo.ws.ValidatePatientResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "validatePatientResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[16] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getPatientSummary");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "getPatientSummary"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "getPatientSummary"), com.heb.rxevo.ws.GetPatientSummary.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", "PartyInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", ">PartyInfo"), com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo.class, true, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "getPatientSummaryResponse"));
        oper.setReturnClass(com.heb.rxevo.ws.GetPatientSummaryResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "getPatientSummaryResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[17] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("removeAllergies");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "removeAllergies"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "removeAllergies"), com.heb.rxevo.ws.RemoveAllergies.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", "PartyInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", ">PartyInfo"), com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo.class, true, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "removeAllergiesResponse"));
        oper.setReturnClass(com.heb.rxevo.ws.RemoveAllergiesResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "removeAllergiesResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[18] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getRevoPharmacy");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "getRevoPharmacy"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "getRevoPharmacy"), com.heb.rxevo.ws.GetRevoPharmacy.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", "PartyInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", ">PartyInfo"), com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo.class, true, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "getRevoPharmacyResponse"));
        oper.setReturnClass(com.heb.rxevo.ws.GetRevoPharmacyResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "getRevoPharmacyResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[19] = oper;

    }

    private static void _initOperationDesc3(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("confirmProfile");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "confirmProfile"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "confirmProfile"), com.heb.rxevo.ws.ConfirmProfile.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", "PartyInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", ">PartyInfo"), com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo.class, true, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "confirmProfileResponse"));
        oper.setReturnClass(com.heb.rxevo.ws.ConfirmProfileResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "confirmProfileResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[20] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("removeInsurance");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "removeInsurance"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "removeInsurance"), com.heb.rxevo.ws.RemoveInsurance.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", "PartyInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", ">PartyInfo"), com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo.class, true, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "removeInsuranceResponse"));
        oper.setReturnClass(com.heb.rxevo.ws.RemoveInsuranceResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "removeInsuranceResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[21] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("updatePatientEmailAddress");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "updatePatientEmailAddress"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "updatePatientEmailAddress"), com.heb.rxevo.ws.UpdatePatientEmailAddress.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", "PartyInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", ">PartyInfo"), com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo.class, true, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "updatePatientEmailAddressResponse"));
        oper.setReturnClass(com.heb.rxevo.ws.UpdatePatientEmailAddressResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "updatePatientEmailAddressResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[22] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("enableRefillExpressForRx");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "enableRefillExpressForRx"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "enableRefillExpressForRx"), com.heb.rxevo.ws.EnableRefillExpressForRx.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", "PartyInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", ">PartyInfo"), com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo.class, true, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "enableRefillExpressForRxResponse"));
        oper.setReturnClass(com.heb.rxevo.ws.EnableRefillExpressForRxResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "enableRefillExpressForRxResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[23] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("linkProfile");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "linkProfile"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "linkProfile"), com.heb.rxevo.ws.LinkProfile.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", "PartyInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", ">PartyInfo"), com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo.class, true, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "linkProfileResponse"));
        oper.setReturnClass(com.heb.rxevo.ws.LinkProfileResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "linkProfileResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[24] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("submitOrder");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "submitOrder"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "submitOrder"), com.heb.rxevo.ws.SubmitOrder.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", "PartyInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", ">PartyInfo"), com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo.class, true, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "submitOrderResponse"));
        oper.setReturnClass(com.heb.rxevo.ws.SubmitOrderResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "submitOrderResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[25] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("updatePatientRelationship");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "updatePatientRelationship"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "updatePatientRelationship"), com.heb.rxevo.ws.UpdatePatientRelationship.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", "PartyInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", ">PartyInfo"), com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo.class, true, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "updatePatientRelationshipResponse"));
        oper.setReturnClass(com.heb.rxevo.ws.UpdatePatientRelationshipResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "updatePatientRelationshipResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[26] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("validatePatientByGlobalFillId");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "validatePatientByGlobalFillId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "validatePatientByGlobalFillId"), com.heb.rxevo.ws.ValidatePatientByGlobalFillId.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", "PartyInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", ">PartyInfo"), com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo.class, true, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "validatePatientByGlobalFillIdResponse"));
        oper.setReturnClass(com.heb.rxevo.ws.ValidatePatientByGlobalFillIdResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "validatePatientByGlobalFillIdResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[27] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("updatePatientRecord");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "updatePatientRecord"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "updatePatientRecord"), com.heb.rxevo.ws.UpdatePatientRecord.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", "PartyInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", ">PartyInfo"), com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo.class, true, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "updatePatientRecordResponse"));
        oper.setReturnClass(com.heb.rxevo.ws.UpdatePatientRecordResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "updatePatientRecordResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[28] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getPatientSummaries");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "getPatientSummaries"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "getPatientSummaries"), com.heb.rxevo.ws.GetPatientSummaries.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", "PartyInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", ">PartyInfo"), com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo.class, true, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "getPatientSummariesResponse"));
        oper.setReturnClass(com.heb.rxevo.ws.GetPatientSummariesResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "getPatientSummariesResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[29] = oper;

    }

    private static void _initOperationDesc4(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("contactDr");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "contactDr"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "contactDr"), com.heb.rxevo.ws.ContactDr.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", "PartyInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", ">PartyInfo"), com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo.class, true, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "contactDrResponse"));
        oper.setReturnClass(com.heb.rxevo.ws.ContactDrResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "contactDrResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[30] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("refillRx");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "refillRx"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "refillRx"), com.heb.rxevo.ws.RefillRx.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", "PartyInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", ">PartyInfo"), com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo.class, true, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "refillRxResponse"));
        oper.setReturnClass(com.heb.rxevo.ws.RefillRxResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "refillRxResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[31] = oper;

    }

    public HebRx_HebRxServiceSoapBindingStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public HebRx_HebRxServiceSoapBindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public HebRx_HebRxServiceSoapBindingStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "addAllergies");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.AddAllergies.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "addAllergiesResponse");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.AddAllergiesResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "addInsurance");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.AddInsurance.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "addInsuranceResponse");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.AddInsuranceResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "allergy");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.Allergy.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "allergyArray");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.Allergy[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "allergy");
            qName2 = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "allergys");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "checkRxStatus");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.CheckRxStatus.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "checkRxStatusResponse");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.CheckRxStatusResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "confirmProfile");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.ConfirmProfile.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "confirmProfileResponse");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.ConfirmProfileResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "contactDr");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.ContactDr.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "contactDrResponse");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.ContactDrResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "enableRefillExpressForPatient");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.EnableRefillExpressForPatient.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "enableRefillExpressForPatientResponse");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.EnableRefillExpressForPatientResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "enableRefillExpressForRx");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.EnableRefillExpressForRx.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "enableRefillExpressForRxResponse");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.EnableRefillExpressForRxResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "fill");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.Fill.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "fillArray");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.Fill[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "fill");
            qName2 = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "fills");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "getAllergies");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.GetAllergies.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "getAllergiesResponse");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.GetAllergiesResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "getInsurances");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.GetInsurances.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "getInsurancesResponse");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.GetInsurancesResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "getPatientSummaries");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.GetPatientSummaries.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "getPatientSummariesResponse");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.GetPatientSummariesResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "getPatientSummary");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.GetPatientSummary.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "getPatientSummaryResponse");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.GetPatientSummaryResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "getPrescribers");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.GetPrescribers.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "getPrescribersResponse");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.GetPrescribersResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "getPrescriptionHistory");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.GetPrescriptionHistory.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "getPrescriptionHistoryResponse");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.GetPrescriptionHistoryResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "getRevoPatient");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.GetRevoPatient.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "getRevoPatientResponse");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.GetRevoPatientResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "getRevoPharmacy");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.GetRevoPharmacy.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "getRevoPharmacyResponse");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.GetRevoPharmacyResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "insurance");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.Insurance.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "insuranceArray");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.Insurance[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "insurance");
            qName2 = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "insurances");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "linkProfile");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.LinkProfile.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "linkProfileResponse");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.LinkProfileResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "order");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.Order.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "orderItem");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.OrderItem.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "orderItemArray");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.OrderItem[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "orderItem");
            qName2 = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "orderItems");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "patient");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.Patient.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "patientSummary");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.PatientSummary.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "patientSummaryArray");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.PatientSummary[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "patientSummary");
            qName2 = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "patientSummarys");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "pharmacy");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.Pharmacy.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "prescriber");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.Prescriber.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "prescriberArray");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.Prescriber[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "prescriber");
            qName2 = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "prescribers");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "prescription");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.Prescription.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "prescriptionArray");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.Prescription[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "prescription");
            qName2 = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "prescriptions");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "refillRx");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.RefillRx.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "refillRxResponse");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.RefillRxResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "relationship");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.Relationship.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "relationshipArray");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.Relationship[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "relationship");
            qName2 = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "relationships");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "removeAllergies");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.RemoveAllergies.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "removeAllergiesResponse");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.RemoveAllergiesResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "removeInsurance");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.RemoveInsurance.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "removeInsuranceResponse");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.RemoveInsuranceResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "storeHours");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.StoreHours.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "storeHoursArray");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.StoreHours[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "storeHours");
            qName2 = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "storeHours");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "submitNewPrescription");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.SubmitNewPrescription.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "submitNewPrescriptionResponse");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.SubmitNewPrescriptionResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "submitOrder");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.SubmitOrder.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "submitOrderResponse");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.SubmitOrderResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "submitTransferRequest");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.SubmitTransferRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "submitTransferRequestResponse");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.SubmitTransferRequestResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "token");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.Token.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "updateNotificationPreferences");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.UpdateNotificationPreferences.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "updateNotificationPreferencesResponse");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.UpdateNotificationPreferencesResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "updatePatientEmailAddress");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.UpdatePatientEmailAddress.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "updatePatientEmailAddressResponse");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.UpdatePatientEmailAddressResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "updatePatientMobileNumber");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.UpdatePatientMobileNumber.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "updatePatientMobileNumberResponse");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.UpdatePatientMobileNumberResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "updatePatientPhoneNumber");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.UpdatePatientPhoneNumber.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "updatePatientPhoneNumberResponse");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.UpdatePatientPhoneNumberResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "updatePatientRecord");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.UpdatePatientRecord.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "updatePatientRecordResponse");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.UpdatePatientRecordResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "updatePatientRelationship");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.UpdatePatientRelationship.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "updatePatientRelationshipResponse");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.UpdatePatientRelationshipResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "updateRefillExpressForPatient");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.UpdateRefillExpressForPatient.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "updateRefillExpressForPatientResponse");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.UpdateRefillExpressForPatientResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "updateRefillExpressForRx");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.UpdateRefillExpressForRx.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "updateRefillExpressForRxResponse");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.UpdateRefillExpressForRxResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "validatePatient");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.ValidatePatient.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "validatePatientByGlobalFillId");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.ValidatePatientByGlobalFillId.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "validatePatientByGlobalFillIdResponse");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.ValidatePatientByGlobalFillIdResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "validatePatientResponse");
            cachedSerQNames.add(qName);
            cls = com.heb.rxevo.ws.ValidatePatientResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", ">PartyInfo");
            cachedSerQNames.add(qName);
            cls = com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.tibco.com/namespaces/bc/2002/04/partyinfo.xsd", "party");
            cachedSerQNames.add(qName);
            cls = com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.Party.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public com.heb.rxevo.ws.GetInsurancesResponse getInsurances(com.heb.rxevo.ws.GetInsurances body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getInsurances"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {body__HebRxServiceSoapBinding, partyInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.heb.rxevo.ws.GetInsurancesResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.heb.rxevo.ws.GetInsurancesResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.heb.rxevo.ws.GetInsurancesResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.heb.rxevo.ws.AddAllergiesResponse addAllergies(com.heb.rxevo.ws.AddAllergies body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "addAllergies"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {body__HebRxServiceSoapBinding, partyInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.heb.rxevo.ws.AddAllergiesResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.heb.rxevo.ws.AddAllergiesResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.heb.rxevo.ws.AddAllergiesResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.heb.rxevo.ws.GetPrescribersResponse getPrescribers(com.heb.rxevo.ws.GetPrescribers body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getPrescribers"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {body__HebRxServiceSoapBinding, partyInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.heb.rxevo.ws.GetPrescribersResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.heb.rxevo.ws.GetPrescribersResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.heb.rxevo.ws.GetPrescribersResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.heb.rxevo.ws.GetPrescriptionHistoryResponse getPrescriptionHistory(com.heb.rxevo.ws.GetPrescriptionHistory body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getPrescriptionHistory"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {body__HebRxServiceSoapBinding, partyInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.heb.rxevo.ws.GetPrescriptionHistoryResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.heb.rxevo.ws.GetPrescriptionHistoryResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.heb.rxevo.ws.GetPrescriptionHistoryResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.heb.rxevo.ws.UpdatePatientPhoneNumberResponse updatePatientPhoneNumber(com.heb.rxevo.ws.UpdatePatientPhoneNumber body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "updatePatientPhoneNumber"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {body__HebRxServiceSoapBinding, partyInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.heb.rxevo.ws.UpdatePatientPhoneNumberResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.heb.rxevo.ws.UpdatePatientPhoneNumberResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.heb.rxevo.ws.UpdatePatientPhoneNumberResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.heb.rxevo.ws.GetAllergiesResponse getAllergies(com.heb.rxevo.ws.GetAllergies body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getAllergies"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {body__HebRxServiceSoapBinding, partyInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.heb.rxevo.ws.GetAllergiesResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.heb.rxevo.ws.GetAllergiesResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.heb.rxevo.ws.GetAllergiesResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.heb.rxevo.ws.AddInsuranceResponse addInsurance(com.heb.rxevo.ws.AddInsurance body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[6]);
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "addInsurance"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {body__HebRxServiceSoapBinding, partyInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.heb.rxevo.ws.AddInsuranceResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.heb.rxevo.ws.AddInsuranceResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.heb.rxevo.ws.AddInsuranceResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.heb.rxevo.ws.UpdatePatientMobileNumberResponse updatePatientMobileNumber(com.heb.rxevo.ws.UpdatePatientMobileNumber body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[7]);
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "updatePatientMobileNumber"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {body__HebRxServiceSoapBinding, partyInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.heb.rxevo.ws.UpdatePatientMobileNumberResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.heb.rxevo.ws.UpdatePatientMobileNumberResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.heb.rxevo.ws.UpdatePatientMobileNumberResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.heb.rxevo.ws.SubmitNewPrescriptionResponse submitNewPrescription(com.heb.rxevo.ws.SubmitNewPrescription body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[8]);
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "submitNewPrescription"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {body__HebRxServiceSoapBinding, partyInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.heb.rxevo.ws.SubmitNewPrescriptionResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.heb.rxevo.ws.SubmitNewPrescriptionResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.heb.rxevo.ws.SubmitNewPrescriptionResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.heb.rxevo.ws.SubmitTransferRequestResponse submitTransferRequest(com.heb.rxevo.ws.SubmitTransferRequest body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[9]);
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "submitTransferRequest"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {body__HebRxServiceSoapBinding, partyInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.heb.rxevo.ws.SubmitTransferRequestResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.heb.rxevo.ws.SubmitTransferRequestResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.heb.rxevo.ws.SubmitTransferRequestResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.heb.rxevo.ws.UpdateRefillExpressForRxResponse updateRefillExpressForRx(com.heb.rxevo.ws.UpdateRefillExpressForRx body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[10]);
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "updateRefillExpressForRx"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {body__HebRxServiceSoapBinding, partyInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.heb.rxevo.ws.UpdateRefillExpressForRxResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.heb.rxevo.ws.UpdateRefillExpressForRxResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.heb.rxevo.ws.UpdateRefillExpressForRxResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.heb.rxevo.ws.EnableRefillExpressForPatientResponse enableRefillExpressForPatient(com.heb.rxevo.ws.EnableRefillExpressForPatient body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[11]);
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "enableRefillExpressForPatient"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {body__HebRxServiceSoapBinding, partyInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.heb.rxevo.ws.EnableRefillExpressForPatientResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.heb.rxevo.ws.EnableRefillExpressForPatientResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.heb.rxevo.ws.EnableRefillExpressForPatientResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.heb.rxevo.ws.CheckRxStatusResponse checkRxStatus(com.heb.rxevo.ws.CheckRxStatus body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[12]);
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "checkRxStatus"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {body__HebRxServiceSoapBinding, partyInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.heb.rxevo.ws.CheckRxStatusResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.heb.rxevo.ws.CheckRxStatusResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.heb.rxevo.ws.CheckRxStatusResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.heb.rxevo.ws.UpdateNotificationPreferencesResponse updateNotificationPreferences(com.heb.rxevo.ws.UpdateNotificationPreferences body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[13]);
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "updateNotificationPreferences"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {body__HebRxServiceSoapBinding, partyInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.heb.rxevo.ws.UpdateNotificationPreferencesResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.heb.rxevo.ws.UpdateNotificationPreferencesResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.heb.rxevo.ws.UpdateNotificationPreferencesResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.heb.rxevo.ws.UpdateRefillExpressForPatientResponse updateRefillExpressForPatient(com.heb.rxevo.ws.UpdateRefillExpressForPatient body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[14]);
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "updateRefillExpressForPatient"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {body__HebRxServiceSoapBinding, partyInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.heb.rxevo.ws.UpdateRefillExpressForPatientResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.heb.rxevo.ws.UpdateRefillExpressForPatientResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.heb.rxevo.ws.UpdateRefillExpressForPatientResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.heb.rxevo.ws.GetRevoPatientResponse getRevoPatient(com.heb.rxevo.ws.GetRevoPatient body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[15]);
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getRevoPatient"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {body__HebRxServiceSoapBinding, partyInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.heb.rxevo.ws.GetRevoPatientResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.heb.rxevo.ws.GetRevoPatientResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.heb.rxevo.ws.GetRevoPatientResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.heb.rxevo.ws.ValidatePatientResponse validatePatient(com.heb.rxevo.ws.ValidatePatient body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[16]);
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "validatePatient"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {body__HebRxServiceSoapBinding, partyInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.heb.rxevo.ws.ValidatePatientResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.heb.rxevo.ws.ValidatePatientResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.heb.rxevo.ws.ValidatePatientResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.heb.rxevo.ws.GetPatientSummaryResponse getPatientSummary(com.heb.rxevo.ws.GetPatientSummary body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[17]);
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getPatientSummary"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {body__HebRxServiceSoapBinding, partyInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.heb.rxevo.ws.GetPatientSummaryResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.heb.rxevo.ws.GetPatientSummaryResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.heb.rxevo.ws.GetPatientSummaryResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.heb.rxevo.ws.RemoveAllergiesResponse removeAllergies(com.heb.rxevo.ws.RemoveAllergies body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[18]);
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "removeAllergies"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {body__HebRxServiceSoapBinding, partyInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.heb.rxevo.ws.RemoveAllergiesResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.heb.rxevo.ws.RemoveAllergiesResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.heb.rxevo.ws.RemoveAllergiesResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.heb.rxevo.ws.GetRevoPharmacyResponse getRevoPharmacy(com.heb.rxevo.ws.GetRevoPharmacy body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[19]);
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getRevoPharmacy"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {body__HebRxServiceSoapBinding, partyInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.heb.rxevo.ws.GetRevoPharmacyResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.heb.rxevo.ws.GetRevoPharmacyResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.heb.rxevo.ws.GetRevoPharmacyResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.heb.rxevo.ws.ConfirmProfileResponse confirmProfile(com.heb.rxevo.ws.ConfirmProfile body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[20]);
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "confirmProfile"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {body__HebRxServiceSoapBinding, partyInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.heb.rxevo.ws.ConfirmProfileResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.heb.rxevo.ws.ConfirmProfileResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.heb.rxevo.ws.ConfirmProfileResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.heb.rxevo.ws.RemoveInsuranceResponse removeInsurance(com.heb.rxevo.ws.RemoveInsurance body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[21]);
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "removeInsurance"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {body__HebRxServiceSoapBinding, partyInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.heb.rxevo.ws.RemoveInsuranceResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.heb.rxevo.ws.RemoveInsuranceResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.heb.rxevo.ws.RemoveInsuranceResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.heb.rxevo.ws.UpdatePatientEmailAddressResponse updatePatientEmailAddress(com.heb.rxevo.ws.UpdatePatientEmailAddress body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[22]);
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "updatePatientEmailAddress"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {body__HebRxServiceSoapBinding, partyInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.heb.rxevo.ws.UpdatePatientEmailAddressResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.heb.rxevo.ws.UpdatePatientEmailAddressResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.heb.rxevo.ws.UpdatePatientEmailAddressResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.heb.rxevo.ws.EnableRefillExpressForRxResponse enableRefillExpressForRx(com.heb.rxevo.ws.EnableRefillExpressForRx body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[23]);
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "enableRefillExpressForRx"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {body__HebRxServiceSoapBinding, partyInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.heb.rxevo.ws.EnableRefillExpressForRxResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.heb.rxevo.ws.EnableRefillExpressForRxResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.heb.rxevo.ws.EnableRefillExpressForRxResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.heb.rxevo.ws.LinkProfileResponse linkProfile(com.heb.rxevo.ws.LinkProfile body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[24]);
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "linkProfile"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {body__HebRxServiceSoapBinding, partyInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.heb.rxevo.ws.LinkProfileResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.heb.rxevo.ws.LinkProfileResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.heb.rxevo.ws.LinkProfileResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.heb.rxevo.ws.SubmitOrderResponse submitOrder(com.heb.rxevo.ws.SubmitOrder body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[25]);
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "submitOrder"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {body__HebRxServiceSoapBinding, partyInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.heb.rxevo.ws.SubmitOrderResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.heb.rxevo.ws.SubmitOrderResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.heb.rxevo.ws.SubmitOrderResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.heb.rxevo.ws.UpdatePatientRelationshipResponse updatePatientRelationship(com.heb.rxevo.ws.UpdatePatientRelationship body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[26]);
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "updatePatientRelationship"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {body__HebRxServiceSoapBinding, partyInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.heb.rxevo.ws.UpdatePatientRelationshipResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.heb.rxevo.ws.UpdatePatientRelationshipResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.heb.rxevo.ws.UpdatePatientRelationshipResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.heb.rxevo.ws.ValidatePatientByGlobalFillIdResponse validatePatientByGlobalFillId(com.heb.rxevo.ws.ValidatePatientByGlobalFillId body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[27]);
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "validatePatientByGlobalFillId"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {body__HebRxServiceSoapBinding, partyInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.heb.rxevo.ws.ValidatePatientByGlobalFillIdResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.heb.rxevo.ws.ValidatePatientByGlobalFillIdResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.heb.rxevo.ws.ValidatePatientByGlobalFillIdResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.heb.rxevo.ws.UpdatePatientRecordResponse updatePatientRecord(com.heb.rxevo.ws.UpdatePatientRecord body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[28]);
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "updatePatientRecord"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {body__HebRxServiceSoapBinding, partyInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.heb.rxevo.ws.UpdatePatientRecordResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.heb.rxevo.ws.UpdatePatientRecordResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.heb.rxevo.ws.UpdatePatientRecordResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.heb.rxevo.ws.GetPatientSummariesResponse getPatientSummaries(com.heb.rxevo.ws.GetPatientSummaries body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[29]);
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getPatientSummaries"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {body__HebRxServiceSoapBinding, partyInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.heb.rxevo.ws.GetPatientSummariesResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.heb.rxevo.ws.GetPatientSummariesResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.heb.rxevo.ws.GetPatientSummariesResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.heb.rxevo.ws.ContactDrResponse contactDr(com.heb.rxevo.ws.ContactDr body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[30]);
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "contactDr"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {body__HebRxServiceSoapBinding, partyInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.heb.rxevo.ws.ContactDrResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.heb.rxevo.ws.ContactDrResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.heb.rxevo.ws.ContactDrResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.heb.rxevo.ws.RefillRxResponse refillRx(com.heb.rxevo.ws.RefillRx body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[31]);
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "refillRx"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {body__HebRxServiceSoapBinding, partyInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.heb.rxevo.ws.RefillRxResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.heb.rxevo.ws.RefillRxResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.heb.rxevo.ws.RefillRxResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}
