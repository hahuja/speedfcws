/**
 * SubmitNewPrescription.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.rxevo.ws;

public class SubmitNewPrescription  implements java.io.Serializable {
    private java.lang.String token;

    private com.heb.rxevo.ws.Prescription prescription;

    private java.lang.String notes;

    private java.lang.String pickupCorpNumber;

    private java.util.Calendar pickupTime;

    public SubmitNewPrescription() {
    }

    public SubmitNewPrescription(
           java.lang.String token,
           com.heb.rxevo.ws.Prescription prescription,
           java.lang.String notes,
           java.lang.String pickupCorpNumber,
           java.util.Calendar pickupTime) {
           this.token = token;
           this.prescription = prescription;
           this.notes = notes;
           this.pickupCorpNumber = pickupCorpNumber;
           this.pickupTime = pickupTime;
    }


    /**
     * Gets the token value for this SubmitNewPrescription.
     * 
     * @return token
     */
    public java.lang.String getToken() {
        return token;
    }


    /**
     * Sets the token value for this SubmitNewPrescription.
     * 
     * @param token
     */
    public void setToken(java.lang.String token) {
        this.token = token;
    }


    /**
     * Gets the prescription value for this SubmitNewPrescription.
     * 
     * @return prescription
     */
    public com.heb.rxevo.ws.Prescription getPrescription() {
        return prescription;
    }


    /**
     * Sets the prescription value for this SubmitNewPrescription.
     * 
     * @param prescription
     */
    public void setPrescription(com.heb.rxevo.ws.Prescription prescription) {
        this.prescription = prescription;
    }


    /**
     * Gets the notes value for this SubmitNewPrescription.
     * 
     * @return notes
     */
    public java.lang.String getNotes() {
        return notes;
    }


    /**
     * Sets the notes value for this SubmitNewPrescription.
     * 
     * @param notes
     */
    public void setNotes(java.lang.String notes) {
        this.notes = notes;
    }


    /**
     * Gets the pickupCorpNumber value for this SubmitNewPrescription.
     * 
     * @return pickupCorpNumber
     */
    public java.lang.String getPickupCorpNumber() {
        return pickupCorpNumber;
    }


    /**
     * Sets the pickupCorpNumber value for this SubmitNewPrescription.
     * 
     * @param pickupCorpNumber
     */
    public void setPickupCorpNumber(java.lang.String pickupCorpNumber) {
        this.pickupCorpNumber = pickupCorpNumber;
    }


    /**
     * Gets the pickupTime value for this SubmitNewPrescription.
     * 
     * @return pickupTime
     */
    public java.util.Calendar getPickupTime() {
        return pickupTime;
    }


    /**
     * Sets the pickupTime value for this SubmitNewPrescription.
     * 
     * @param pickupTime
     */
    public void setPickupTime(java.util.Calendar pickupTime) {
        this.pickupTime = pickupTime;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SubmitNewPrescription)) return false;
        SubmitNewPrescription other = (SubmitNewPrescription) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.token==null && other.getToken()==null) || 
             (this.token!=null &&
              this.token.equals(other.getToken()))) &&
            ((this.prescription==null && other.getPrescription()==null) || 
             (this.prescription!=null &&
              this.prescription.equals(other.getPrescription()))) &&
            ((this.notes==null && other.getNotes()==null) || 
             (this.notes!=null &&
              this.notes.equals(other.getNotes()))) &&
            ((this.pickupCorpNumber==null && other.getPickupCorpNumber()==null) || 
             (this.pickupCorpNumber!=null &&
              this.pickupCorpNumber.equals(other.getPickupCorpNumber()))) &&
            ((this.pickupTime==null && other.getPickupTime()==null) || 
             (this.pickupTime!=null &&
              this.pickupTime.equals(other.getPickupTime())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getToken() != null) {
            _hashCode += getToken().hashCode();
        }
        if (getPrescription() != null) {
            _hashCode += getPrescription().hashCode();
        }
        if (getNotes() != null) {
            _hashCode += getNotes().hashCode();
        }
        if (getPickupCorpNumber() != null) {
            _hashCode += getPickupCorpNumber().hashCode();
        }
        if (getPickupTime() != null) {
            _hashCode += getPickupTime().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SubmitNewPrescription.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "submitNewPrescription"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("token");
        elemField.setXmlName(new javax.xml.namespace.QName("", "token"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prescription");
        elemField.setXmlName(new javax.xml.namespace.QName("", "prescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "prescription"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("notes");
        elemField.setXmlName(new javax.xml.namespace.QName("", "notes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pickupCorpNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pickupCorpNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pickupTime");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pickupTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
