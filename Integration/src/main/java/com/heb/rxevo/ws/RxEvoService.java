/**
 * RxEvoService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.rxevo.ws;

public interface RxEvoService extends javax.xml.rpc.Service {
    public java.lang.String getHebRx_HebRxServiceSoapBinding_HTTPPortAddress();

    public com.heb.rxevo.ws.HebRx getHebRx_HebRxServiceSoapBinding_HTTPPort() throws javax.xml.rpc.ServiceException;

    public com.heb.rxevo.ws.HebRx getHebRx_HebRxServiceSoapBinding_HTTPPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
