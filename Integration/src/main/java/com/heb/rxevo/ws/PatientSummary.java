/**
 * PatientSummary.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.rxevo.ws;

public class PatientSummary  implements java.io.Serializable {
    private com.heb.rxevo.ws.Patient patient;

    private com.heb.rxevo.ws.Prescription[] prescriptionArray;

    private com.heb.rxevo.ws.Relationship[] relationshipArray;

    private java.lang.String patientStatus;

    public PatientSummary() {
    }

    public PatientSummary(
           com.heb.rxevo.ws.Patient patient,
           com.heb.rxevo.ws.Prescription[] prescriptionArray,
           com.heb.rxevo.ws.Relationship[] relationshipArray,
           java.lang.String patientStatus) {
           this.patient = patient;
           this.prescriptionArray = prescriptionArray;
           this.relationshipArray = relationshipArray;
           this.patientStatus = patientStatus;
    }


    /**
     * Gets the patient value for this PatientSummary.
     * 
     * @return patient
     */
    public com.heb.rxevo.ws.Patient getPatient() {
        return patient;
    }


    /**
     * Sets the patient value for this PatientSummary.
     * 
     * @param patient
     */
    public void setPatient(com.heb.rxevo.ws.Patient patient) {
        this.patient = patient;
    }


    /**
     * Gets the prescriptionArray value for this PatientSummary.
     * 
     * @return prescriptionArray
     */
    public com.heb.rxevo.ws.Prescription[] getPrescriptionArray() {
        return prescriptionArray;
    }


    /**
     * Sets the prescriptionArray value for this PatientSummary.
     * 
     * @param prescriptionArray
     */
    public void setPrescriptionArray(com.heb.rxevo.ws.Prescription[] prescriptionArray) {
        this.prescriptionArray = prescriptionArray;
    }


    /**
     * Gets the relationshipArray value for this PatientSummary.
     * 
     * @return relationshipArray
     */
    public com.heb.rxevo.ws.Relationship[] getRelationshipArray() {
        return relationshipArray;
    }


    /**
     * Sets the relationshipArray value for this PatientSummary.
     * 
     * @param relationshipArray
     */
    public void setRelationshipArray(com.heb.rxevo.ws.Relationship[] relationshipArray) {
        this.relationshipArray = relationshipArray;
    }


    /**
     * Gets the patientStatus value for this PatientSummary.
     * 
     * @return patientStatus
     */
    public java.lang.String getPatientStatus() {
        return patientStatus;
    }


    /**
     * Sets the patientStatus value for this PatientSummary.
     * 
     * @param patientStatus
     */
    public void setPatientStatus(java.lang.String patientStatus) {
        this.patientStatus = patientStatus;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PatientSummary)) return false;
        PatientSummary other = (PatientSummary) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.patient==null && other.getPatient()==null) || 
             (this.patient!=null &&
              this.patient.equals(other.getPatient()))) &&
            ((this.prescriptionArray==null && other.getPrescriptionArray()==null) || 
             (this.prescriptionArray!=null &&
              java.util.Arrays.equals(this.prescriptionArray, other.getPrescriptionArray()))) &&
            ((this.relationshipArray==null && other.getRelationshipArray()==null) || 
             (this.relationshipArray!=null &&
              java.util.Arrays.equals(this.relationshipArray, other.getRelationshipArray()))) &&
            ((this.patientStatus==null && other.getPatientStatus()==null) || 
             (this.patientStatus!=null &&
              this.patientStatus.equals(other.getPatientStatus())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPatient() != null) {
            _hashCode += getPatient().hashCode();
        }
        if (getPrescriptionArray() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPrescriptionArray());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPrescriptionArray(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getRelationshipArray() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getRelationshipArray());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getRelationshipArray(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getPatientStatus() != null) {
            _hashCode += getPatientStatus().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PatientSummary.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "patientSummary"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("patient");
        elemField.setXmlName(new javax.xml.namespace.QName("", "patient"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "patient"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prescriptionArray");
        elemField.setXmlName(new javax.xml.namespace.QName("", "prescriptionArray"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "prescription"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "prescriptions"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("relationshipArray");
        elemField.setXmlName(new javax.xml.namespace.QName("", "relationshipArray"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "relationship"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "relationships"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("patientStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("", "patientStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
