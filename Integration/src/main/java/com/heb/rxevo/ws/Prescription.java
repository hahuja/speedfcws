/**
 * Prescription.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.rxevo.ws;

public class Prescription  implements java.io.Serializable {
    private java.lang.String rxId;

    private java.lang.String patientId;

    private com.heb.rxevo.ws.Prescriber prescriber;

    private int corporateNumber;

    private double refillsRemaining;

    private boolean isRefillExpress;

    private java.lang.String prescribedProduct;

    private java.lang.String productNdc;

    private java.lang.String sig;

    private java.util.Calendar lastFillDate;

    private java.util.Calendar nextFillDate;

    private java.util.Calendar pickupTime;

    private java.util.Calendar writtenDate;

    private java.util.Calendar expirationDate;

    private java.lang.String reassignedRxNumber;

    private java.lang.String status;

    private boolean isImmunization;

    public Prescription() {
    }

    public Prescription(
           java.lang.String rxId,
           java.lang.String patientId,
           com.heb.rxevo.ws.Prescriber prescriber,
           int corporateNumber,
           double refillsRemaining,
           boolean isRefillExpress,
           java.lang.String prescribedProduct,
           java.lang.String productNdc,
           java.lang.String sig,
           java.util.Calendar lastFillDate,
           java.util.Calendar nextFillDate,
           java.util.Calendar pickupTime,
           java.util.Calendar writtenDate,
           java.util.Calendar expirationDate,
           java.lang.String reassignedRxNumber,
           java.lang.String status,
           boolean isImmunization) {
           this.rxId = rxId;
           this.patientId = patientId;
           this.prescriber = prescriber;
           this.corporateNumber = corporateNumber;
           this.refillsRemaining = refillsRemaining;
           this.isRefillExpress = isRefillExpress;
           this.prescribedProduct = prescribedProduct;
           this.productNdc = productNdc;
           this.sig = sig;
           this.lastFillDate = lastFillDate;
           this.nextFillDate = nextFillDate;
           this.pickupTime = pickupTime;
           this.writtenDate = writtenDate;
           this.expirationDate = expirationDate;
           this.reassignedRxNumber = reassignedRxNumber;
           this.status = status;
           this.isImmunization = isImmunization;
    }


    /**
     * Gets the rxId value for this Prescription.
     * 
     * @return rxId
     */
    public java.lang.String getRxId() {
        return rxId;
    }


    /**
     * Sets the rxId value for this Prescription.
     * 
     * @param rxId
     */
    public void setRxId(java.lang.String rxId) {
        this.rxId = rxId;
    }


    /**
     * Gets the patientId value for this Prescription.
     * 
     * @return patientId
     */
    public java.lang.String getPatientId() {
        return patientId;
    }


    /**
     * Sets the patientId value for this Prescription.
     * 
     * @param patientId
     */
    public void setPatientId(java.lang.String patientId) {
        this.patientId = patientId;
    }


    /**
     * Gets the prescriber value for this Prescription.
     * 
     * @return prescriber
     */
    public com.heb.rxevo.ws.Prescriber getPrescriber() {
        return prescriber;
    }


    /**
     * Sets the prescriber value for this Prescription.
     * 
     * @param prescriber
     */
    public void setPrescriber(com.heb.rxevo.ws.Prescriber prescriber) {
        this.prescriber = prescriber;
    }


    /**
     * Gets the corporateNumber value for this Prescription.
     * 
     * @return corporateNumber
     */
    public int getCorporateNumber() {
        return corporateNumber;
    }


    /**
     * Sets the corporateNumber value for this Prescription.
     * 
     * @param corporateNumber
     */
    public void setCorporateNumber(int corporateNumber) {
        this.corporateNumber = corporateNumber;
    }


    /**
     * Gets the refillsRemaining value for this Prescription.
     * 
     * @return refillsRemaining
     */
    public double getRefillsRemaining() {
        return refillsRemaining;
    }


    /**
     * Sets the refillsRemaining value for this Prescription.
     * 
     * @param refillsRemaining
     */
    public void setRefillsRemaining(double refillsRemaining) {
        this.refillsRemaining = refillsRemaining;
    }


    /**
     * Gets the isRefillExpress value for this Prescription.
     * 
     * @return isRefillExpress
     */
    public boolean isIsRefillExpress() {
        return isRefillExpress;
    }


    /**
     * Sets the isRefillExpress value for this Prescription.
     * 
     * @param isRefillExpress
     */
    public void setIsRefillExpress(boolean isRefillExpress) {
        this.isRefillExpress = isRefillExpress;
    }


    /**
     * Gets the prescribedProduct value for this Prescription.
     * 
     * @return prescribedProduct
     */
    public java.lang.String getPrescribedProduct() {
        return prescribedProduct;
    }


    /**
     * Sets the prescribedProduct value for this Prescription.
     * 
     * @param prescribedProduct
     */
    public void setPrescribedProduct(java.lang.String prescribedProduct) {
        this.prescribedProduct = prescribedProduct;
    }


    /**
     * Gets the productNdc value for this Prescription.
     * 
     * @return productNdc
     */
    public java.lang.String getProductNdc() {
        return productNdc;
    }


    /**
     * Sets the productNdc value for this Prescription.
     * 
     * @param productNdc
     */
    public void setProductNdc(java.lang.String productNdc) {
        this.productNdc = productNdc;
    }


    /**
     * Gets the sig value for this Prescription.
     * 
     * @return sig
     */
    public java.lang.String getSig() {
        return sig;
    }


    /**
     * Sets the sig value for this Prescription.
     * 
     * @param sig
     */
    public void setSig(java.lang.String sig) {
        this.sig = sig;
    }


    /**
     * Gets the lastFillDate value for this Prescription.
     * 
     * @return lastFillDate
     */
    public java.util.Calendar getLastFillDate() {
        return lastFillDate;
    }


    /**
     * Sets the lastFillDate value for this Prescription.
     * 
     * @param lastFillDate
     */
    public void setLastFillDate(java.util.Calendar lastFillDate) {
        this.lastFillDate = lastFillDate;
    }


    /**
     * Gets the nextFillDate value for this Prescription.
     * 
     * @return nextFillDate
     */
    public java.util.Calendar getNextFillDate() {
        return nextFillDate;
    }


    /**
     * Sets the nextFillDate value for this Prescription.
     * 
     * @param nextFillDate
     */
    public void setNextFillDate(java.util.Calendar nextFillDate) {
        this.nextFillDate = nextFillDate;
    }


    /**
     * Gets the pickupTime value for this Prescription.
     * 
     * @return pickupTime
     */
    public java.util.Calendar getPickupTime() {
        return pickupTime;
    }


    /**
     * Sets the pickupTime value for this Prescription.
     * 
     * @param pickupTime
     */
    public void setPickupTime(java.util.Calendar pickupTime) {
        this.pickupTime = pickupTime;
    }


    /**
     * Gets the writtenDate value for this Prescription.
     * 
     * @return writtenDate
     */
    public java.util.Calendar getWrittenDate() {
        return writtenDate;
    }


    /**
     * Sets the writtenDate value for this Prescription.
     * 
     * @param writtenDate
     */
    public void setWrittenDate(java.util.Calendar writtenDate) {
        this.writtenDate = writtenDate;
    }


    /**
     * Gets the expirationDate value for this Prescription.
     * 
     * @return expirationDate
     */
    public java.util.Calendar getExpirationDate() {
        return expirationDate;
    }


    /**
     * Sets the expirationDate value for this Prescription.
     * 
     * @param expirationDate
     */
    public void setExpirationDate(java.util.Calendar expirationDate) {
        this.expirationDate = expirationDate;
    }


    /**
     * Gets the reassignedRxNumber value for this Prescription.
     * 
     * @return reassignedRxNumber
     */
    public java.lang.String getReassignedRxNumber() {
        return reassignedRxNumber;
    }


    /**
     * Sets the reassignedRxNumber value for this Prescription.
     * 
     * @param reassignedRxNumber
     */
    public void setReassignedRxNumber(java.lang.String reassignedRxNumber) {
        this.reassignedRxNumber = reassignedRxNumber;
    }


    /**
     * Gets the status value for this Prescription.
     * 
     * @return status
     */
    public java.lang.String getStatus() {
        return status;
    }


    /**
     * Sets the status value for this Prescription.
     * 
     * @param status
     */
    public void setStatus(java.lang.String status) {
        this.status = status;
    }


    /**
     * Gets the isImmunization value for this Prescription.
     * 
     * @return isImmunization
     */
    public boolean isIsImmunization() {
        return isImmunization;
    }


    /**
     * Sets the isImmunization value for this Prescription.
     * 
     * @param isImmunization
     */
    public void setIsImmunization(boolean isImmunization) {
        this.isImmunization = isImmunization;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Prescription)) return false;
        Prescription other = (Prescription) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.rxId==null && other.getRxId()==null) || 
             (this.rxId!=null &&
              this.rxId.equals(other.getRxId()))) &&
            ((this.patientId==null && other.getPatientId()==null) || 
             (this.patientId!=null &&
              this.patientId.equals(other.getPatientId()))) &&
            ((this.prescriber==null && other.getPrescriber()==null) || 
             (this.prescriber!=null &&
              this.prescriber.equals(other.getPrescriber()))) &&
            this.corporateNumber == other.getCorporateNumber() &&
            this.refillsRemaining == other.getRefillsRemaining() &&
            this.isRefillExpress == other.isIsRefillExpress() &&
            ((this.prescribedProduct==null && other.getPrescribedProduct()==null) || 
             (this.prescribedProduct!=null &&
              this.prescribedProduct.equals(other.getPrescribedProduct()))) &&
            ((this.productNdc==null && other.getProductNdc()==null) || 
             (this.productNdc!=null &&
              this.productNdc.equals(other.getProductNdc()))) &&
            ((this.sig==null && other.getSig()==null) || 
             (this.sig!=null &&
              this.sig.equals(other.getSig()))) &&
            ((this.lastFillDate==null && other.getLastFillDate()==null) || 
             (this.lastFillDate!=null &&
              this.lastFillDate.equals(other.getLastFillDate()))) &&
            ((this.nextFillDate==null && other.getNextFillDate()==null) || 
             (this.nextFillDate!=null &&
              this.nextFillDate.equals(other.getNextFillDate()))) &&
            ((this.pickupTime==null && other.getPickupTime()==null) || 
             (this.pickupTime!=null &&
              this.pickupTime.equals(other.getPickupTime()))) &&
            ((this.writtenDate==null && other.getWrittenDate()==null) || 
             (this.writtenDate!=null &&
              this.writtenDate.equals(other.getWrittenDate()))) &&
            ((this.expirationDate==null && other.getExpirationDate()==null) || 
             (this.expirationDate!=null &&
              this.expirationDate.equals(other.getExpirationDate()))) &&
            ((this.reassignedRxNumber==null && other.getReassignedRxNumber()==null) || 
             (this.reassignedRxNumber!=null &&
              this.reassignedRxNumber.equals(other.getReassignedRxNumber()))) &&
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            this.isImmunization == other.isIsImmunization();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getRxId() != null) {
            _hashCode += getRxId().hashCode();
        }
        if (getPatientId() != null) {
            _hashCode += getPatientId().hashCode();
        }
        if (getPrescriber() != null) {
            _hashCode += getPrescriber().hashCode();
        }
        _hashCode += getCorporateNumber();
        _hashCode += new Double(getRefillsRemaining()).hashCode();
        _hashCode += (isIsRefillExpress() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getPrescribedProduct() != null) {
            _hashCode += getPrescribedProduct().hashCode();
        }
        if (getProductNdc() != null) {
            _hashCode += getProductNdc().hashCode();
        }
        if (getSig() != null) {
            _hashCode += getSig().hashCode();
        }
        if (getLastFillDate() != null) {
            _hashCode += getLastFillDate().hashCode();
        }
        if (getNextFillDate() != null) {
            _hashCode += getNextFillDate().hashCode();
        }
        if (getPickupTime() != null) {
            _hashCode += getPickupTime().hashCode();
        }
        if (getWrittenDate() != null) {
            _hashCode += getWrittenDate().hashCode();
        }
        if (getExpirationDate() != null) {
            _hashCode += getExpirationDate().hashCode();
        }
        if (getReassignedRxNumber() != null) {
            _hashCode += getReassignedRxNumber().hashCode();
        }
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        _hashCode += (isIsImmunization() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Prescription.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "prescription"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rxId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "rxId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("patientId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "patientId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prescriber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "prescriber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "prescriber"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("corporateNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "corporateNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("refillsRemaining");
        elemField.setXmlName(new javax.xml.namespace.QName("", "refillsRemaining"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isRefillExpress");
        elemField.setXmlName(new javax.xml.namespace.QName("", "isRefillExpress"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prescribedProduct");
        elemField.setXmlName(new javax.xml.namespace.QName("", "prescribedProduct"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productNdc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "productNdc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sig");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sig"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastFillDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "lastFillDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nextFillDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nextFillDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pickupTime");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pickupTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("writtenDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "writtenDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("expirationDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "expirationDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("reassignedRxNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "reassignedRxNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("", "status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isImmunization");
        elemField.setXmlName(new javax.xml.namespace.QName("", "isImmunization"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
