/**
 * Pharmacy.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.rxevo.ws;

public class Pharmacy  implements java.io.Serializable {
    private int corporateNumber;

    private com.heb.rxevo.ws.StoreHours[] operationHours;

    private java.lang.String legalDescription;

    private java.lang.String streetAddress;

    private java.lang.String city;

    private java.lang.String stateCode;

    private java.lang.String zipCode;

    private java.lang.String phoneNumber1;

    private java.lang.String phoneNumber2;

    private java.lang.String faxNumber;

    private java.lang.String pharmacyManager;

    public Pharmacy() {
    }

    public Pharmacy(
           int corporateNumber,
           com.heb.rxevo.ws.StoreHours[] operationHours,
           java.lang.String legalDescription,
           java.lang.String streetAddress,
           java.lang.String city,
           java.lang.String stateCode,
           java.lang.String zipCode,
           java.lang.String phoneNumber1,
           java.lang.String phoneNumber2,
           java.lang.String faxNumber,
           java.lang.String pharmacyManager) {
           this.corporateNumber = corporateNumber;
           this.operationHours = operationHours;
           this.legalDescription = legalDescription;
           this.streetAddress = streetAddress;
           this.city = city;
           this.stateCode = stateCode;
           this.zipCode = zipCode;
           this.phoneNumber1 = phoneNumber1;
           this.phoneNumber2 = phoneNumber2;
           this.faxNumber = faxNumber;
           this.pharmacyManager = pharmacyManager;
    }


    /**
     * Gets the corporateNumber value for this Pharmacy.
     * 
     * @return corporateNumber
     */
    public int getCorporateNumber() {
        return corporateNumber;
    }


    /**
     * Sets the corporateNumber value for this Pharmacy.
     * 
     * @param corporateNumber
     */
    public void setCorporateNumber(int corporateNumber) {
        this.corporateNumber = corporateNumber;
    }


    /**
     * Gets the operationHours value for this Pharmacy.
     * 
     * @return operationHours
     */
    public com.heb.rxevo.ws.StoreHours[] getOperationHours() {
        return operationHours;
    }


    /**
     * Sets the operationHours value for this Pharmacy.
     * 
     * @param operationHours
     */
    public void setOperationHours(com.heb.rxevo.ws.StoreHours[] operationHours) {
        this.operationHours = operationHours;
    }


    /**
     * Gets the legalDescription value for this Pharmacy.
     * 
     * @return legalDescription
     */
    public java.lang.String getLegalDescription() {
        return legalDescription;
    }


    /**
     * Sets the legalDescription value for this Pharmacy.
     * 
     * @param legalDescription
     */
    public void setLegalDescription(java.lang.String legalDescription) {
        this.legalDescription = legalDescription;
    }


    /**
     * Gets the streetAddress value for this Pharmacy.
     * 
     * @return streetAddress
     */
    public java.lang.String getStreetAddress() {
        return streetAddress;
    }


    /**
     * Sets the streetAddress value for this Pharmacy.
     * 
     * @param streetAddress
     */
    public void setStreetAddress(java.lang.String streetAddress) {
        this.streetAddress = streetAddress;
    }


    /**
     * Gets the city value for this Pharmacy.
     * 
     * @return city
     */
    public java.lang.String getCity() {
        return city;
    }


    /**
     * Sets the city value for this Pharmacy.
     * 
     * @param city
     */
    public void setCity(java.lang.String city) {
        this.city = city;
    }


    /**
     * Gets the stateCode value for this Pharmacy.
     * 
     * @return stateCode
     */
    public java.lang.String getStateCode() {
        return stateCode;
    }


    /**
     * Sets the stateCode value for this Pharmacy.
     * 
     * @param stateCode
     */
    public void setStateCode(java.lang.String stateCode) {
        this.stateCode = stateCode;
    }


    /**
     * Gets the zipCode value for this Pharmacy.
     * 
     * @return zipCode
     */
    public java.lang.String getZipCode() {
        return zipCode;
    }


    /**
     * Sets the zipCode value for this Pharmacy.
     * 
     * @param zipCode
     */
    public void setZipCode(java.lang.String zipCode) {
        this.zipCode = zipCode;
    }


    /**
     * Gets the phoneNumber1 value for this Pharmacy.
     * 
     * @return phoneNumber1
     */
    public java.lang.String getPhoneNumber1() {
        return phoneNumber1;
    }


    /**
     * Sets the phoneNumber1 value for this Pharmacy.
     * 
     * @param phoneNumber1
     */
    public void setPhoneNumber1(java.lang.String phoneNumber1) {
        this.phoneNumber1 = phoneNumber1;
    }


    /**
     * Gets the phoneNumber2 value for this Pharmacy.
     * 
     * @return phoneNumber2
     */
    public java.lang.String getPhoneNumber2() {
        return phoneNumber2;
    }


    /**
     * Sets the phoneNumber2 value for this Pharmacy.
     * 
     * @param phoneNumber2
     */
    public void setPhoneNumber2(java.lang.String phoneNumber2) {
        this.phoneNumber2 = phoneNumber2;
    }


    /**
     * Gets the faxNumber value for this Pharmacy.
     * 
     * @return faxNumber
     */
    public java.lang.String getFaxNumber() {
        return faxNumber;
    }


    /**
     * Sets the faxNumber value for this Pharmacy.
     * 
     * @param faxNumber
     */
    public void setFaxNumber(java.lang.String faxNumber) {
        this.faxNumber = faxNumber;
    }


    /**
     * Gets the pharmacyManager value for this Pharmacy.
     * 
     * @return pharmacyManager
     */
    public java.lang.String getPharmacyManager() {
        return pharmacyManager;
    }


    /**
     * Sets the pharmacyManager value for this Pharmacy.
     * 
     * @param pharmacyManager
     */
    public void setPharmacyManager(java.lang.String pharmacyManager) {
        this.pharmacyManager = pharmacyManager;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Pharmacy)) return false;
        Pharmacy other = (Pharmacy) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.corporateNumber == other.getCorporateNumber() &&
            ((this.operationHours==null && other.getOperationHours()==null) || 
             (this.operationHours!=null &&
              java.util.Arrays.equals(this.operationHours, other.getOperationHours()))) &&
            ((this.legalDescription==null && other.getLegalDescription()==null) || 
             (this.legalDescription!=null &&
              this.legalDescription.equals(other.getLegalDescription()))) &&
            ((this.streetAddress==null && other.getStreetAddress()==null) || 
             (this.streetAddress!=null &&
              this.streetAddress.equals(other.getStreetAddress()))) &&
            ((this.city==null && other.getCity()==null) || 
             (this.city!=null &&
              this.city.equals(other.getCity()))) &&
            ((this.stateCode==null && other.getStateCode()==null) || 
             (this.stateCode!=null &&
              this.stateCode.equals(other.getStateCode()))) &&
            ((this.zipCode==null && other.getZipCode()==null) || 
             (this.zipCode!=null &&
              this.zipCode.equals(other.getZipCode()))) &&
            ((this.phoneNumber1==null && other.getPhoneNumber1()==null) || 
             (this.phoneNumber1!=null &&
              this.phoneNumber1.equals(other.getPhoneNumber1()))) &&
            ((this.phoneNumber2==null && other.getPhoneNumber2()==null) || 
             (this.phoneNumber2!=null &&
              this.phoneNumber2.equals(other.getPhoneNumber2()))) &&
            ((this.faxNumber==null && other.getFaxNumber()==null) || 
             (this.faxNumber!=null &&
              this.faxNumber.equals(other.getFaxNumber()))) &&
            ((this.pharmacyManager==null && other.getPharmacyManager()==null) || 
             (this.pharmacyManager!=null &&
              this.pharmacyManager.equals(other.getPharmacyManager())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getCorporateNumber();
        if (getOperationHours() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getOperationHours());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getOperationHours(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getLegalDescription() != null) {
            _hashCode += getLegalDescription().hashCode();
        }
        if (getStreetAddress() != null) {
            _hashCode += getStreetAddress().hashCode();
        }
        if (getCity() != null) {
            _hashCode += getCity().hashCode();
        }
        if (getStateCode() != null) {
            _hashCode += getStateCode().hashCode();
        }
        if (getZipCode() != null) {
            _hashCode += getZipCode().hashCode();
        }
        if (getPhoneNumber1() != null) {
            _hashCode += getPhoneNumber1().hashCode();
        }
        if (getPhoneNumber2() != null) {
            _hashCode += getPhoneNumber2().hashCode();
        }
        if (getFaxNumber() != null) {
            _hashCode += getFaxNumber().hashCode();
        }
        if (getPharmacyManager() != null) {
            _hashCode += getPharmacyManager().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Pharmacy.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "pharmacy"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("corporateNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "corporateNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("operationHours");
        elemField.setXmlName(new javax.xml.namespace.QName("", "operationHours"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "storeHours"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "storeHours"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("legalDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("", "legalDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("streetAddress");
        elemField.setXmlName(new javax.xml.namespace.QName("", "streetAddress"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("city");
        elemField.setXmlName(new javax.xml.namespace.QName("", "city"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("stateCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "stateCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("zipCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "zipCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("phoneNumber1");
        elemField.setXmlName(new javax.xml.namespace.QName("", "phoneNumber1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("phoneNumber2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "phoneNumber2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("faxNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "faxNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pharmacyManager");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pharmacyManager"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
