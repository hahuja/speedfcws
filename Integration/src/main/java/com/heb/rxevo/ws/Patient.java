/**
 * Patient.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.rxevo.ws;

public class Patient  implements java.io.Serializable {
    private java.lang.String patientId;

    private java.lang.String lastName;

    private java.lang.String firstName;

    private java.util.Calendar dateOfBirth;

    private boolean isRefillExpress;

    private boolean isReadyNotification;

    private java.lang.String phoneNumber;

    private java.lang.String mobileNumber;

    private java.lang.String emailAddress;

    private java.lang.String street;

    private java.lang.String city;

    private java.lang.String state;

    private java.lang.String zip;

    private boolean isAnimal;

    private boolean isEasyOpen;

    private java.lang.String language;

    private boolean emailNotifications;

    private boolean voiceNotifications;

    private boolean smsNotifications;

    private boolean hippaStatus;

    private java.lang.String eCommId;

    private java.lang.String eCommStatus;

    public Patient() {
    }

    public Patient(
           java.lang.String patientId,
           java.lang.String lastName,
           java.lang.String firstName,
           java.util.Calendar dateOfBirth,
           boolean isRefillExpress,
           boolean isReadyNotification,
           java.lang.String phoneNumber,
           java.lang.String mobileNumber,
           java.lang.String emailAddress,
           java.lang.String street,
           java.lang.String city,
           java.lang.String state,
           java.lang.String zip,
           boolean isAnimal,
           boolean isEasyOpen,
           java.lang.String language,
           boolean emailNotifications,
           boolean voiceNotifications,
           boolean smsNotifications,
           boolean hippaStatus,
           java.lang.String eCommId,
           java.lang.String eCommStatus) {
           this.patientId = patientId;
           this.lastName = lastName;
           this.firstName = firstName;
           this.dateOfBirth = dateOfBirth;
           this.isRefillExpress = isRefillExpress;
           this.isReadyNotification = isReadyNotification;
           this.phoneNumber = phoneNumber;
           this.mobileNumber = mobileNumber;
           this.emailAddress = emailAddress;
           this.street = street;
           this.city = city;
           this.state = state;
           this.zip = zip;
           this.isAnimal = isAnimal;
           this.isEasyOpen = isEasyOpen;
           this.language = language;
           this.emailNotifications = emailNotifications;
           this.voiceNotifications = voiceNotifications;
           this.smsNotifications = smsNotifications;
           this.hippaStatus = hippaStatus;
           this.eCommId = eCommId;
           this.eCommStatus = eCommStatus;
    }


    /**
     * Gets the patientId value for this Patient.
     * 
     * @return patientId
     */
    public java.lang.String getPatientId() {
        return patientId;
    }


    /**
     * Sets the patientId value for this Patient.
     * 
     * @param patientId
     */
    public void setPatientId(java.lang.String patientId) {
        this.patientId = patientId;
    }


    /**
     * Gets the lastName value for this Patient.
     * 
     * @return lastName
     */
    public java.lang.String getLastName() {
        return lastName;
    }


    /**
     * Sets the lastName value for this Patient.
     * 
     * @param lastName
     */
    public void setLastName(java.lang.String lastName) {
        this.lastName = lastName;
    }


    /**
     * Gets the firstName value for this Patient.
     * 
     * @return firstName
     */
    public java.lang.String getFirstName() {
        return firstName;
    }


    /**
     * Sets the firstName value for this Patient.
     * 
     * @param firstName
     */
    public void setFirstName(java.lang.String firstName) {
        this.firstName = firstName;
    }


    /**
     * Gets the dateOfBirth value for this Patient.
     * 
     * @return dateOfBirth
     */
    public java.util.Calendar getDateOfBirth() {
        return dateOfBirth;
    }


    /**
     * Sets the dateOfBirth value for this Patient.
     * 
     * @param dateOfBirth
     */
    public void setDateOfBirth(java.util.Calendar dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }


    /**
     * Gets the isRefillExpress value for this Patient.
     * 
     * @return isRefillExpress
     */
    public boolean isIsRefillExpress() {
        return isRefillExpress;
    }


    /**
     * Sets the isRefillExpress value for this Patient.
     * 
     * @param isRefillExpress
     */
    public void setIsRefillExpress(boolean isRefillExpress) {
        this.isRefillExpress = isRefillExpress;
    }


    /**
     * Gets the isReadyNotification value for this Patient.
     * 
     * @return isReadyNotification
     */
    public boolean isIsReadyNotification() {
        return isReadyNotification;
    }


    /**
     * Sets the isReadyNotification value for this Patient.
     * 
     * @param isReadyNotification
     */
    public void setIsReadyNotification(boolean isReadyNotification) {
        this.isReadyNotification = isReadyNotification;
    }


    /**
     * Gets the phoneNumber value for this Patient.
     * 
     * @return phoneNumber
     */
    public java.lang.String getPhoneNumber() {
        return phoneNumber;
    }


    /**
     * Sets the phoneNumber value for this Patient.
     * 
     * @param phoneNumber
     */
    public void setPhoneNumber(java.lang.String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }


    /**
     * Gets the mobileNumber value for this Patient.
     * 
     * @return mobileNumber
     */
    public java.lang.String getMobileNumber() {
        return mobileNumber;
    }


    /**
     * Sets the mobileNumber value for this Patient.
     * 
     * @param mobileNumber
     */
    public void setMobileNumber(java.lang.String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }


    /**
     * Gets the emailAddress value for this Patient.
     * 
     * @return emailAddress
     */
    public java.lang.String getEmailAddress() {
        return emailAddress;
    }


    /**
     * Sets the emailAddress value for this Patient.
     * 
     * @param emailAddress
     */
    public void setEmailAddress(java.lang.String emailAddress) {
        this.emailAddress = emailAddress;
    }


    /**
     * Gets the street value for this Patient.
     * 
     * @return street
     */
    public java.lang.String getStreet() {
        return street;
    }


    /**
     * Sets the street value for this Patient.
     * 
     * @param street
     */
    public void setStreet(java.lang.String street) {
        this.street = street;
    }


    /**
     * Gets the city value for this Patient.
     * 
     * @return city
     */
    public java.lang.String getCity() {
        return city;
    }


    /**
     * Sets the city value for this Patient.
     * 
     * @param city
     */
    public void setCity(java.lang.String city) {
        this.city = city;
    }


    /**
     * Gets the state value for this Patient.
     * 
     * @return state
     */
    public java.lang.String getState() {
        return state;
    }


    /**
     * Sets the state value for this Patient.
     * 
     * @param state
     */
    public void setState(java.lang.String state) {
        this.state = state;
    }


    /**
     * Gets the zip value for this Patient.
     * 
     * @return zip
     */
    public java.lang.String getZip() {
        return zip;
    }


    /**
     * Sets the zip value for this Patient.
     * 
     * @param zip
     */
    public void setZip(java.lang.String zip) {
        this.zip = zip;
    }


    /**
     * Gets the isAnimal value for this Patient.
     * 
     * @return isAnimal
     */
    public boolean isIsAnimal() {
        return isAnimal;
    }


    /**
     * Sets the isAnimal value for this Patient.
     * 
     * @param isAnimal
     */
    public void setIsAnimal(boolean isAnimal) {
        this.isAnimal = isAnimal;
    }


    /**
     * Gets the isEasyOpen value for this Patient.
     * 
     * @return isEasyOpen
     */
    public boolean isIsEasyOpen() {
        return isEasyOpen;
    }


    /**
     * Sets the isEasyOpen value for this Patient.
     * 
     * @param isEasyOpen
     */
    public void setIsEasyOpen(boolean isEasyOpen) {
        this.isEasyOpen = isEasyOpen;
    }


    /**
     * Gets the language value for this Patient.
     * 
     * @return language
     */
    public java.lang.String getLanguage() {
        return language;
    }


    /**
     * Sets the language value for this Patient.
     * 
     * @param language
     */
    public void setLanguage(java.lang.String language) {
        this.language = language;
    }


    /**
     * Gets the emailNotifications value for this Patient.
     * 
     * @return emailNotifications
     */
    public boolean isEmailNotifications() {
        return emailNotifications;
    }


    /**
     * Sets the emailNotifications value for this Patient.
     * 
     * @param emailNotifications
     */
    public void setEmailNotifications(boolean emailNotifications) {
        this.emailNotifications = emailNotifications;
    }


    /**
     * Gets the voiceNotifications value for this Patient.
     * 
     * @return voiceNotifications
     */
    public boolean isVoiceNotifications() {
        return voiceNotifications;
    }


    /**
     * Sets the voiceNotifications value for this Patient.
     * 
     * @param voiceNotifications
     */
    public void setVoiceNotifications(boolean voiceNotifications) {
        this.voiceNotifications = voiceNotifications;
    }


    /**
     * Gets the smsNotifications value for this Patient.
     * 
     * @return smsNotifications
     */
    public boolean isSmsNotifications() {
        return smsNotifications;
    }


    /**
     * Sets the smsNotifications value for this Patient.
     * 
     * @param smsNotifications
     */
    public void setSmsNotifications(boolean smsNotifications) {
        this.smsNotifications = smsNotifications;
    }


    /**
     * Gets the hippaStatus value for this Patient.
     * 
     * @return hippaStatus
     */
    public boolean isHippaStatus() {
        return hippaStatus;
    }


    /**
     * Sets the hippaStatus value for this Patient.
     * 
     * @param hippaStatus
     */
    public void setHippaStatus(boolean hippaStatus) {
        this.hippaStatus = hippaStatus;
    }


    /**
     * Gets the eCommId value for this Patient.
     * 
     * @return eCommId
     */
    public java.lang.String getECommId() {
        return eCommId;
    }


    /**
     * Sets the eCommId value for this Patient.
     * 
     * @param eCommId
     */
    public void setECommId(java.lang.String eCommId) {
        this.eCommId = eCommId;
    }


    /**
     * Gets the eCommStatus value for this Patient.
     * 
     * @return eCommStatus
     */
    public java.lang.String getECommStatus() {
        return eCommStatus;
    }


    /**
     * Sets the eCommStatus value for this Patient.
     * 
     * @param eCommStatus
     */
    public void setECommStatus(java.lang.String eCommStatus) {
        this.eCommStatus = eCommStatus;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Patient)) return false;
        Patient other = (Patient) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.patientId==null && other.getPatientId()==null) || 
             (this.patientId!=null &&
              this.patientId.equals(other.getPatientId()))) &&
            ((this.lastName==null && other.getLastName()==null) || 
             (this.lastName!=null &&
              this.lastName.equals(other.getLastName()))) &&
            ((this.firstName==null && other.getFirstName()==null) || 
             (this.firstName!=null &&
              this.firstName.equals(other.getFirstName()))) &&
            ((this.dateOfBirth==null && other.getDateOfBirth()==null) || 
             (this.dateOfBirth!=null &&
              this.dateOfBirth.equals(other.getDateOfBirth()))) &&
            this.isRefillExpress == other.isIsRefillExpress() &&
            this.isReadyNotification == other.isIsReadyNotification() &&
            ((this.phoneNumber==null && other.getPhoneNumber()==null) || 
             (this.phoneNumber!=null &&
              this.phoneNumber.equals(other.getPhoneNumber()))) &&
            ((this.mobileNumber==null && other.getMobileNumber()==null) || 
             (this.mobileNumber!=null &&
              this.mobileNumber.equals(other.getMobileNumber()))) &&
            ((this.emailAddress==null && other.getEmailAddress()==null) || 
             (this.emailAddress!=null &&
              this.emailAddress.equals(other.getEmailAddress()))) &&
            ((this.street==null && other.getStreet()==null) || 
             (this.street!=null &&
              this.street.equals(other.getStreet()))) &&
            ((this.city==null && other.getCity()==null) || 
             (this.city!=null &&
              this.city.equals(other.getCity()))) &&
            ((this.state==null && other.getState()==null) || 
             (this.state!=null &&
              this.state.equals(other.getState()))) &&
            ((this.zip==null && other.getZip()==null) || 
             (this.zip!=null &&
              this.zip.equals(other.getZip()))) &&
            this.isAnimal == other.isIsAnimal() &&
            this.isEasyOpen == other.isIsEasyOpen() &&
            ((this.language==null && other.getLanguage()==null) || 
             (this.language!=null &&
              this.language.equals(other.getLanguage()))) &&
            this.emailNotifications == other.isEmailNotifications() &&
            this.voiceNotifications == other.isVoiceNotifications() &&
            this.smsNotifications == other.isSmsNotifications() &&
            this.hippaStatus == other.isHippaStatus() &&
            ((this.eCommId==null && other.getECommId()==null) || 
             (this.eCommId!=null &&
              this.eCommId.equals(other.getECommId()))) &&
            ((this.eCommStatus==null && other.getECommStatus()==null) || 
             (this.eCommStatus!=null &&
              this.eCommStatus.equals(other.getECommStatus())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPatientId() != null) {
            _hashCode += getPatientId().hashCode();
        }
        if (getLastName() != null) {
            _hashCode += getLastName().hashCode();
        }
        if (getFirstName() != null) {
            _hashCode += getFirstName().hashCode();
        }
        if (getDateOfBirth() != null) {
            _hashCode += getDateOfBirth().hashCode();
        }
        _hashCode += (isIsRefillExpress() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        _hashCode += (isIsReadyNotification() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getPhoneNumber() != null) {
            _hashCode += getPhoneNumber().hashCode();
        }
        if (getMobileNumber() != null) {
            _hashCode += getMobileNumber().hashCode();
        }
        if (getEmailAddress() != null) {
            _hashCode += getEmailAddress().hashCode();
        }
        if (getStreet() != null) {
            _hashCode += getStreet().hashCode();
        }
        if (getCity() != null) {
            _hashCode += getCity().hashCode();
        }
        if (getState() != null) {
            _hashCode += getState().hashCode();
        }
        if (getZip() != null) {
            _hashCode += getZip().hashCode();
        }
        _hashCode += (isIsAnimal() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        _hashCode += (isIsEasyOpen() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getLanguage() != null) {
            _hashCode += getLanguage().hashCode();
        }
        _hashCode += (isEmailNotifications() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        _hashCode += (isVoiceNotifications() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        _hashCode += (isSmsNotifications() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        _hashCode += (isHippaStatus() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getECommId() != null) {
            _hashCode += getECommId().hashCode();
        }
        if (getECommStatus() != null) {
            _hashCode += getECommStatus().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Patient.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "patient"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("patientId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "patientId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "lastName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("firstName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "firstName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dateOfBirth");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dateOfBirth"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isRefillExpress");
        elemField.setXmlName(new javax.xml.namespace.QName("", "isRefillExpress"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isReadyNotification");
        elemField.setXmlName(new javax.xml.namespace.QName("", "isReadyNotification"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("phoneNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "phoneNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mobileNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "mobileNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("emailAddress");
        elemField.setXmlName(new javax.xml.namespace.QName("", "emailAddress"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("street");
        elemField.setXmlName(new javax.xml.namespace.QName("", "street"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("city");
        elemField.setXmlName(new javax.xml.namespace.QName("", "city"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("state");
        elemField.setXmlName(new javax.xml.namespace.QName("", "state"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("zip");
        elemField.setXmlName(new javax.xml.namespace.QName("", "zip"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isAnimal");
        elemField.setXmlName(new javax.xml.namespace.QName("", "isAnimal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isEasyOpen");
        elemField.setXmlName(new javax.xml.namespace.QName("", "isEasyOpen"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("language");
        elemField.setXmlName(new javax.xml.namespace.QName("", "language"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("emailNotifications");
        elemField.setXmlName(new javax.xml.namespace.QName("", "emailNotifications"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("voiceNotifications");
        elemField.setXmlName(new javax.xml.namespace.QName("", "voiceNotifications"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("smsNotifications");
        elemField.setXmlName(new javax.xml.namespace.QName("", "smsNotifications"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("hippaStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("", "hippaStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ECommId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "eCommId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ECommStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("", "eCommStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
