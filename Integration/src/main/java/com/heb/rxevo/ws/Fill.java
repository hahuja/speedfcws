/**
 * Fill.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.rxevo.ws;

public class Fill  implements java.io.Serializable {
    private java.lang.String rxId;

    private java.lang.String fillId;

    private double refillsRemaining;

    private java.lang.String dispensedProduct;

    private java.lang.String productNdc;

    private java.util.Calendar fillDate;

    private double quantity;

    private java.lang.String status;

    private int daysSupply;

    private double patientPay;

    public Fill() {
    }

    public Fill(
           java.lang.String rxId,
           java.lang.String fillId,
           double refillsRemaining,
           java.lang.String dispensedProduct,
           java.lang.String productNdc,
           java.util.Calendar fillDate,
           double quantity,
           java.lang.String status,
           int daysSupply,
           double patientPay) {
           this.rxId = rxId;
           this.fillId = fillId;
           this.refillsRemaining = refillsRemaining;
           this.dispensedProduct = dispensedProduct;
           this.productNdc = productNdc;
           this.fillDate = fillDate;
           this.quantity = quantity;
           this.status = status;
           this.daysSupply = daysSupply;
           this.patientPay = patientPay;
    }


    /**
     * Gets the rxId value for this Fill.
     * 
     * @return rxId
     */
    public java.lang.String getRxId() {
        return rxId;
    }


    /**
     * Sets the rxId value for this Fill.
     * 
     * @param rxId
     */
    public void setRxId(java.lang.String rxId) {
        this.rxId = rxId;
    }


    /**
     * Gets the fillId value for this Fill.
     * 
     * @return fillId
     */
    public java.lang.String getFillId() {
        return fillId;
    }


    /**
     * Sets the fillId value for this Fill.
     * 
     * @param fillId
     */
    public void setFillId(java.lang.String fillId) {
        this.fillId = fillId;
    }


    /**
     * Gets the refillsRemaining value for this Fill.
     * 
     * @return refillsRemaining
     */
    public double getRefillsRemaining() {
        return refillsRemaining;
    }


    /**
     * Sets the refillsRemaining value for this Fill.
     * 
     * @param refillsRemaining
     */
    public void setRefillsRemaining(double refillsRemaining) {
        this.refillsRemaining = refillsRemaining;
    }


    /**
     * Gets the dispensedProduct value for this Fill.
     * 
     * @return dispensedProduct
     */
    public java.lang.String getDispensedProduct() {
        return dispensedProduct;
    }


    /**
     * Sets the dispensedProduct value for this Fill.
     * 
     * @param dispensedProduct
     */
    public void setDispensedProduct(java.lang.String dispensedProduct) {
        this.dispensedProduct = dispensedProduct;
    }


    /**
     * Gets the productNdc value for this Fill.
     * 
     * @return productNdc
     */
    public java.lang.String getProductNdc() {
        return productNdc;
    }


    /**
     * Sets the productNdc value for this Fill.
     * 
     * @param productNdc
     */
    public void setProductNdc(java.lang.String productNdc) {
        this.productNdc = productNdc;
    }


    /**
     * Gets the fillDate value for this Fill.
     * 
     * @return fillDate
     */
    public java.util.Calendar getFillDate() {
        return fillDate;
    }


    /**
     * Sets the fillDate value for this Fill.
     * 
     * @param fillDate
     */
    public void setFillDate(java.util.Calendar fillDate) {
        this.fillDate = fillDate;
    }


    /**
     * Gets the quantity value for this Fill.
     * 
     * @return quantity
     */
    public double getQuantity() {
        return quantity;
    }


    /**
     * Sets the quantity value for this Fill.
     * 
     * @param quantity
     */
    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }


    /**
     * Gets the status value for this Fill.
     * 
     * @return status
     */
    public java.lang.String getStatus() {
        return status;
    }


    /**
     * Sets the status value for this Fill.
     * 
     * @param status
     */
    public void setStatus(java.lang.String status) {
        this.status = status;
    }


    /**
     * Gets the daysSupply value for this Fill.
     * 
     * @return daysSupply
     */
    public int getDaysSupply() {
        return daysSupply;
    }


    /**
     * Sets the daysSupply value for this Fill.
     * 
     * @param daysSupply
     */
    public void setDaysSupply(int daysSupply) {
        this.daysSupply = daysSupply;
    }


    /**
     * Gets the patientPay value for this Fill.
     * 
     * @return patientPay
     */
    public double getPatientPay() {
        return patientPay;
    }


    /**
     * Sets the patientPay value for this Fill.
     * 
     * @param patientPay
     */
    public void setPatientPay(double patientPay) {
        this.patientPay = patientPay;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Fill)) return false;
        Fill other = (Fill) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.rxId==null && other.getRxId()==null) || 
             (this.rxId!=null &&
              this.rxId.equals(other.getRxId()))) &&
            ((this.fillId==null && other.getFillId()==null) || 
             (this.fillId!=null &&
              this.fillId.equals(other.getFillId()))) &&
            this.refillsRemaining == other.getRefillsRemaining() &&
            ((this.dispensedProduct==null && other.getDispensedProduct()==null) || 
             (this.dispensedProduct!=null &&
              this.dispensedProduct.equals(other.getDispensedProduct()))) &&
            ((this.productNdc==null && other.getProductNdc()==null) || 
             (this.productNdc!=null &&
              this.productNdc.equals(other.getProductNdc()))) &&
            ((this.fillDate==null && other.getFillDate()==null) || 
             (this.fillDate!=null &&
              this.fillDate.equals(other.getFillDate()))) &&
            this.quantity == other.getQuantity() &&
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            this.daysSupply == other.getDaysSupply() &&
            this.patientPay == other.getPatientPay();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getRxId() != null) {
            _hashCode += getRxId().hashCode();
        }
        if (getFillId() != null) {
            _hashCode += getFillId().hashCode();
        }
        _hashCode += new Double(getRefillsRemaining()).hashCode();
        if (getDispensedProduct() != null) {
            _hashCode += getDispensedProduct().hashCode();
        }
        if (getProductNdc() != null) {
            _hashCode += getProductNdc().hashCode();
        }
        if (getFillDate() != null) {
            _hashCode += getFillDate().hashCode();
        }
        _hashCode += new Double(getQuantity()).hashCode();
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        _hashCode += getDaysSupply();
        _hashCode += new Double(getPatientPay()).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Fill.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "fill"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rxId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "rxId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fillId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fillId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("refillsRemaining");
        elemField.setXmlName(new javax.xml.namespace.QName("", "refillsRemaining"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dispensedProduct");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dispensedProduct"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productNdc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "productNdc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fillDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fillDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("quantity");
        elemField.setXmlName(new javax.xml.namespace.QName("", "quantity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("", "status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("daysSupply");
        elemField.setXmlName(new javax.xml.namespace.QName("", "daysSupply"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("patientPay");
        elemField.setXmlName(new javax.xml.namespace.QName("", "patientPay"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
