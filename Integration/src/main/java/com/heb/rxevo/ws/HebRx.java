/**
 * HebRx.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.rxevo.ws;

public interface HebRx extends java.rmi.Remote {

    /**
     * BCWSDL:getInsurances
     */
    public com.heb.rxevo.ws.GetInsurancesResponse getInsurances(com.heb.rxevo.ws.GetInsurances body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException;

    /**
     * BCWSDL:addAllergies
     */
    public com.heb.rxevo.ws.AddAllergiesResponse addAllergies(com.heb.rxevo.ws.AddAllergies body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException;

    /**
     * BCWSDL:getPrescribers
     */
    public com.heb.rxevo.ws.GetPrescribersResponse getPrescribers(com.heb.rxevo.ws.GetPrescribers body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException;

    /**
     * BCWSDL:getPrescriptionHistory
     */
    public com.heb.rxevo.ws.GetPrescriptionHistoryResponse getPrescriptionHistory(com.heb.rxevo.ws.GetPrescriptionHistory body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException;

    /**
     * BCWSDL:updatePatientPhoneNumber
     */
    public com.heb.rxevo.ws.UpdatePatientPhoneNumberResponse updatePatientPhoneNumber(com.heb.rxevo.ws.UpdatePatientPhoneNumber body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException;

    /**
     * BCWSDL:getAllergies
     */
    public com.heb.rxevo.ws.GetAllergiesResponse getAllergies(com.heb.rxevo.ws.GetAllergies body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException;

    /**
     * BCWSDL:addInsurance
     */
    public com.heb.rxevo.ws.AddInsuranceResponse addInsurance(com.heb.rxevo.ws.AddInsurance body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException;

    /**
     * BCWSDL:updatePatientMobileNumber
     */
    public com.heb.rxevo.ws.UpdatePatientMobileNumberResponse updatePatientMobileNumber(com.heb.rxevo.ws.UpdatePatientMobileNumber body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException;

    /**
     * BCWSDL:submitNewPrescription
     */
    public com.heb.rxevo.ws.SubmitNewPrescriptionResponse submitNewPrescription(com.heb.rxevo.ws.SubmitNewPrescription body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException;

    /**
     * BCWSDL:submitTransferRequest
     */
    public com.heb.rxevo.ws.SubmitTransferRequestResponse submitTransferRequest(com.heb.rxevo.ws.SubmitTransferRequest body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException;

    /**
     * BCWSDL:updateRefillExpressForRx
     */
    public com.heb.rxevo.ws.UpdateRefillExpressForRxResponse updateRefillExpressForRx(com.heb.rxevo.ws.UpdateRefillExpressForRx body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException;

    /**
     * BCWSDL:enableRefillExpressForPatient
     */
    public com.heb.rxevo.ws.EnableRefillExpressForPatientResponse enableRefillExpressForPatient(com.heb.rxevo.ws.EnableRefillExpressForPatient body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException;

    /**
     * BCWSDL:checkRxStatus
     */
    public com.heb.rxevo.ws.CheckRxStatusResponse checkRxStatus(com.heb.rxevo.ws.CheckRxStatus body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException;

    /**
     * BCWSDL:updateNotificationPreferences
     */
    public com.heb.rxevo.ws.UpdateNotificationPreferencesResponse updateNotificationPreferences(com.heb.rxevo.ws.UpdateNotificationPreferences body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException;

    /**
     * BCWSDL:updateRefillExpressForPatient
     */
    public com.heb.rxevo.ws.UpdateRefillExpressForPatientResponse updateRefillExpressForPatient(com.heb.rxevo.ws.UpdateRefillExpressForPatient body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException;

    /**
     * BCWSDL:getRevoPatient
     */
    public com.heb.rxevo.ws.GetRevoPatientResponse getRevoPatient(com.heb.rxevo.ws.GetRevoPatient body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException;

    /**
     * BCWSDL:validatePatient
     */
    public com.heb.rxevo.ws.ValidatePatientResponse validatePatient(com.heb.rxevo.ws.ValidatePatient body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException;

    /**
     * BCWSDL:getPatientSummary
     */
    public com.heb.rxevo.ws.GetPatientSummaryResponse getPatientSummary(com.heb.rxevo.ws.GetPatientSummary body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException;

    /**
     * BCWSDL:removeAllergies
     */
    public com.heb.rxevo.ws.RemoveAllergiesResponse removeAllergies(com.heb.rxevo.ws.RemoveAllergies body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException;

    /**
     * BCWSDL:getRevoPharmacy
     */
    public com.heb.rxevo.ws.GetRevoPharmacyResponse getRevoPharmacy(com.heb.rxevo.ws.GetRevoPharmacy body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException;

    /**
     * BCWSDL:confirmProfile
     */
    public com.heb.rxevo.ws.ConfirmProfileResponse confirmProfile(com.heb.rxevo.ws.ConfirmProfile body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException;

    /**
     * BCWSDL:removeInsurance
     */
    public com.heb.rxevo.ws.RemoveInsuranceResponse removeInsurance(com.heb.rxevo.ws.RemoveInsurance body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException;

    /**
     * BCWSDL:updatePatientEmailAddress
     */
    public com.heb.rxevo.ws.UpdatePatientEmailAddressResponse updatePatientEmailAddress(com.heb.rxevo.ws.UpdatePatientEmailAddress body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException;

    /**
     * BCWSDL:enableRefillExpressForRx
     */
    public com.heb.rxevo.ws.EnableRefillExpressForRxResponse enableRefillExpressForRx(com.heb.rxevo.ws.EnableRefillExpressForRx body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException;

    /**
     * BCWSDL:linkProfile
     */
    public com.heb.rxevo.ws.LinkProfileResponse linkProfile(com.heb.rxevo.ws.LinkProfile body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException;

    /**
     * BCWSDL:submitOrder
     */
    public com.heb.rxevo.ws.SubmitOrderResponse submitOrder(com.heb.rxevo.ws.SubmitOrder body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException;

    /**
     * BCWSDL:updatePatientRelationship
     */
    public com.heb.rxevo.ws.UpdatePatientRelationshipResponse updatePatientRelationship(com.heb.rxevo.ws.UpdatePatientRelationship body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException;

    /**
     * BCWSDL:validatePatientByGlobalFillId
     */
    public com.heb.rxevo.ws.ValidatePatientByGlobalFillIdResponse validatePatientByGlobalFillId(com.heb.rxevo.ws.ValidatePatientByGlobalFillId body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException;

    /**
     * BCWSDL:updatePatientRecord
     */
    public com.heb.rxevo.ws.UpdatePatientRecordResponse updatePatientRecord(com.heb.rxevo.ws.UpdatePatientRecord body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException;

    /**
     * BCWSDL:getPatientSummaries
     */
    public com.heb.rxevo.ws.GetPatientSummariesResponse getPatientSummaries(com.heb.rxevo.ws.GetPatientSummaries body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException;

    /**
     * BCWSDL:contactDr
     */
    public com.heb.rxevo.ws.ContactDrResponse contactDr(com.heb.rxevo.ws.ContactDr body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException;

    /**
     * BCWSDL:refillRx
     */
    public com.heb.rxevo.ws.RefillRxResponse refillRx(com.heb.rxevo.ws.RefillRx body__HebRxServiceSoapBinding, com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo partyInfo) throws java.rmi.RemoteException;
}
