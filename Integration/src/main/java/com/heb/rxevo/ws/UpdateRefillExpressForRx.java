/**
 * UpdateRefillExpressForRx.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.rxevo.ws;

public class UpdateRefillExpressForRx  implements java.io.Serializable {
    private com.heb.rxevo.ws.Token token;

    private java.lang.String rxId;

    private boolean enableRefill;

    public UpdateRefillExpressForRx() {
    }

    public UpdateRefillExpressForRx(
           com.heb.rxevo.ws.Token token,
           java.lang.String rxId,
           boolean enableRefill) {
           this.token = token;
           this.rxId = rxId;
           this.enableRefill = enableRefill;
    }


    /**
     * Gets the token value for this UpdateRefillExpressForRx.
     * 
     * @return token
     */
    public com.heb.rxevo.ws.Token getToken() {
        return token;
    }


    /**
     * Sets the token value for this UpdateRefillExpressForRx.
     * 
     * @param token
     */
    public void setToken(com.heb.rxevo.ws.Token token) {
        this.token = token;
    }


    /**
     * Gets the rxId value for this UpdateRefillExpressForRx.
     * 
     * @return rxId
     */
    public java.lang.String getRxId() {
        return rxId;
    }


    /**
     * Sets the rxId value for this UpdateRefillExpressForRx.
     * 
     * @param rxId
     */
    public void setRxId(java.lang.String rxId) {
        this.rxId = rxId;
    }


    /**
     * Gets the enableRefill value for this UpdateRefillExpressForRx.
     * 
     * @return enableRefill
     */
    public boolean isEnableRefill() {
        return enableRefill;
    }


    /**
     * Sets the enableRefill value for this UpdateRefillExpressForRx.
     * 
     * @param enableRefill
     */
    public void setEnableRefill(boolean enableRefill) {
        this.enableRefill = enableRefill;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UpdateRefillExpressForRx)) return false;
        UpdateRefillExpressForRx other = (UpdateRefillExpressForRx) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.token==null && other.getToken()==null) || 
             (this.token!=null &&
              this.token.equals(other.getToken()))) &&
            ((this.rxId==null && other.getRxId()==null) || 
             (this.rxId!=null &&
              this.rxId.equals(other.getRxId()))) &&
            this.enableRefill == other.isEnableRefill();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getToken() != null) {
            _hashCode += getToken().hashCode();
        }
        if (getRxId() != null) {
            _hashCode += getRxId().hashCode();
        }
        _hashCode += (isEnableRefill() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(UpdateRefillExpressForRx.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "updateRefillExpressForRx"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("token");
        elemField.setXmlName(new javax.xml.namespace.QName("", "token"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "token"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rxId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "rxId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("enableRefill");
        elemField.setXmlName(new javax.xml.namespace.QName("", "enableRefill"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
