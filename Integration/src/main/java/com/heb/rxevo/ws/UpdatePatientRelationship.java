/**
 * UpdatePatientRelationship.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.rxevo.ws;

public class UpdatePatientRelationship  implements java.io.Serializable {
    private com.heb.rxevo.ws.Token token;

    private java.lang.String eCommId;

    private com.heb.rxevo.ws.Relationship relationship;

    public UpdatePatientRelationship() {
    }

    public UpdatePatientRelationship(
           com.heb.rxevo.ws.Token token,
           java.lang.String eCommId,
           com.heb.rxevo.ws.Relationship relationship) {
           this.token = token;
           this.eCommId = eCommId;
           this.relationship = relationship;
    }


    /**
     * Gets the token value for this UpdatePatientRelationship.
     * 
     * @return token
     */
    public com.heb.rxevo.ws.Token getToken() {
        return token;
    }


    /**
     * Sets the token value for this UpdatePatientRelationship.
     * 
     * @param token
     */
    public void setToken(com.heb.rxevo.ws.Token token) {
        this.token = token;
    }


    /**
     * Gets the eCommId value for this UpdatePatientRelationship.
     * 
     * @return eCommId
     */
    public java.lang.String getECommId() {
        return eCommId;
    }


    /**
     * Sets the eCommId value for this UpdatePatientRelationship.
     * 
     * @param eCommId
     */
    public void setECommId(java.lang.String eCommId) {
        this.eCommId = eCommId;
    }


    /**
     * Gets the relationship value for this UpdatePatientRelationship.
     * 
     * @return relationship
     */
    public com.heb.rxevo.ws.Relationship getRelationship() {
        return relationship;
    }


    /**
     * Sets the relationship value for this UpdatePatientRelationship.
     * 
     * @param relationship
     */
    public void setRelationship(com.heb.rxevo.ws.Relationship relationship) {
        this.relationship = relationship;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UpdatePatientRelationship)) return false;
        UpdatePatientRelationship other = (UpdatePatientRelationship) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.token==null && other.getToken()==null) || 
             (this.token!=null &&
              this.token.equals(other.getToken()))) &&
            ((this.eCommId==null && other.getECommId()==null) || 
             (this.eCommId!=null &&
              this.eCommId.equals(other.getECommId()))) &&
            ((this.relationship==null && other.getRelationship()==null) || 
             (this.relationship!=null &&
              this.relationship.equals(other.getRelationship())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getToken() != null) {
            _hashCode += getToken().hashCode();
        }
        if (getECommId() != null) {
            _hashCode += getECommId().hashCode();
        }
        if (getRelationship() != null) {
            _hashCode += getRelationship().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(UpdatePatientRelationship.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "updatePatientRelationship"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("token");
        elemField.setXmlName(new javax.xml.namespace.QName("", "token"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "token"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ECommId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "eCommId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("relationship");
        elemField.setXmlName(new javax.xml.namespace.QName("", "relationship"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "relationship"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
