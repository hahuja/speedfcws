/**
 * SubmitTransferRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.rxevo.ws;

public class SubmitTransferRequest  implements java.io.Serializable {
    private com.heb.rxevo.ws.Token token;

    private com.heb.rxevo.ws.Patient patient;

    private com.heb.rxevo.ws.Prescription[] prescriptions;

    private com.heb.rxevo.ws.Pharmacy externalPharmacy;

    private java.lang.String pickupCorpNumber;

    private java.util.Calendar pickupTime;

    public SubmitTransferRequest() {
    }

    public SubmitTransferRequest(
           com.heb.rxevo.ws.Token token,
           com.heb.rxevo.ws.Patient patient,
           com.heb.rxevo.ws.Prescription[] prescriptions,
           com.heb.rxevo.ws.Pharmacy externalPharmacy,
           java.lang.String pickupCorpNumber,
           java.util.Calendar pickupTime) {
           this.token = token;
           this.patient = patient;
           this.prescriptions = prescriptions;
           this.externalPharmacy = externalPharmacy;
           this.pickupCorpNumber = pickupCorpNumber;
           this.pickupTime = pickupTime;
    }


    /**
     * Gets the token value for this SubmitTransferRequest.
     * 
     * @return token
     */
    public com.heb.rxevo.ws.Token getToken() {
        return token;
    }


    /**
     * Sets the token value for this SubmitTransferRequest.
     * 
     * @param token
     */
    public void setToken(com.heb.rxevo.ws.Token token) {
        this.token = token;
    }


    /**
     * Gets the patient value for this SubmitTransferRequest.
     * 
     * @return patient
     */
    public com.heb.rxevo.ws.Patient getPatient() {
        return patient;
    }


    /**
     * Sets the patient value for this SubmitTransferRequest.
     * 
     * @param patient
     */
    public void setPatient(com.heb.rxevo.ws.Patient patient) {
        this.patient = patient;
    }


    /**
     * Gets the prescriptions value for this SubmitTransferRequest.
     * 
     * @return prescriptions
     */
    public com.heb.rxevo.ws.Prescription[] getPrescriptions() {
        return prescriptions;
    }


    /**
     * Sets the prescriptions value for this SubmitTransferRequest.
     * 
     * @param prescriptions
     */
    public void setPrescriptions(com.heb.rxevo.ws.Prescription[] prescriptions) {
        this.prescriptions = prescriptions;
    }


    /**
     * Gets the externalPharmacy value for this SubmitTransferRequest.
     * 
     * @return externalPharmacy
     */
    public com.heb.rxevo.ws.Pharmacy getExternalPharmacy() {
        return externalPharmacy;
    }


    /**
     * Sets the externalPharmacy value for this SubmitTransferRequest.
     * 
     * @param externalPharmacy
     */
    public void setExternalPharmacy(com.heb.rxevo.ws.Pharmacy externalPharmacy) {
        this.externalPharmacy = externalPharmacy;
    }


    /**
     * Gets the pickupCorpNumber value for this SubmitTransferRequest.
     * 
     * @return pickupCorpNumber
     */
    public java.lang.String getPickupCorpNumber() {
        return pickupCorpNumber;
    }


    /**
     * Sets the pickupCorpNumber value for this SubmitTransferRequest.
     * 
     * @param pickupCorpNumber
     */
    public void setPickupCorpNumber(java.lang.String pickupCorpNumber) {
        this.pickupCorpNumber = pickupCorpNumber;
    }


    /**
     * Gets the pickupTime value for this SubmitTransferRequest.
     * 
     * @return pickupTime
     */
    public java.util.Calendar getPickupTime() {
        return pickupTime;
    }


    /**
     * Sets the pickupTime value for this SubmitTransferRequest.
     * 
     * @param pickupTime
     */
    public void setPickupTime(java.util.Calendar pickupTime) {
        this.pickupTime = pickupTime;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SubmitTransferRequest)) return false;
        SubmitTransferRequest other = (SubmitTransferRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.token==null && other.getToken()==null) || 
             (this.token!=null &&
              this.token.equals(other.getToken()))) &&
            ((this.patient==null && other.getPatient()==null) || 
             (this.patient!=null &&
              this.patient.equals(other.getPatient()))) &&
            ((this.prescriptions==null && other.getPrescriptions()==null) || 
             (this.prescriptions!=null &&
              java.util.Arrays.equals(this.prescriptions, other.getPrescriptions()))) &&
            ((this.externalPharmacy==null && other.getExternalPharmacy()==null) || 
             (this.externalPharmacy!=null &&
              this.externalPharmacy.equals(other.getExternalPharmacy()))) &&
            ((this.pickupCorpNumber==null && other.getPickupCorpNumber()==null) || 
             (this.pickupCorpNumber!=null &&
              this.pickupCorpNumber.equals(other.getPickupCorpNumber()))) &&
            ((this.pickupTime==null && other.getPickupTime()==null) || 
             (this.pickupTime!=null &&
              this.pickupTime.equals(other.getPickupTime())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getToken() != null) {
            _hashCode += getToken().hashCode();
        }
        if (getPatient() != null) {
            _hashCode += getPatient().hashCode();
        }
        if (getPrescriptions() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPrescriptions());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPrescriptions(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getExternalPharmacy() != null) {
            _hashCode += getExternalPharmacy().hashCode();
        }
        if (getPickupCorpNumber() != null) {
            _hashCode += getPickupCorpNumber().hashCode();
        }
        if (getPickupTime() != null) {
            _hashCode += getPickupTime().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SubmitTransferRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "submitTransferRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("token");
        elemField.setXmlName(new javax.xml.namespace.QName("", "token"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "token"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("patient");
        elemField.setXmlName(new javax.xml.namespace.QName("", "patient"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "patient"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prescriptions");
        elemField.setXmlName(new javax.xml.namespace.QName("", "prescriptions"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "prescription"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "prescriptions"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("externalPharmacy");
        elemField.setXmlName(new javax.xml.namespace.QName("", "externalPharmacy"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://ws.rxevo.heb.com", "pharmacy"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pickupCorpNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pickupCorpNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pickupTime");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pickupTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
