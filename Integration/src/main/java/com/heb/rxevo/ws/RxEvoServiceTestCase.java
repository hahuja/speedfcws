/**
 * RxEvoServiceTestCase.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.heb.rxevo.ws;

public class RxEvoServiceTestCase extends junit.framework.TestCase {
    public RxEvoServiceTestCase(java.lang.String name) {
        super(name);
    }

    public void testHebRx_HebRxServiceSoapBinding_HTTPPortWSDL() throws Exception {
        javax.xml.rpc.ServiceFactory serviceFactory = javax.xml.rpc.ServiceFactory.newInstance();
        java.net.URL url = new java.net.URL(new com.heb.rxevo.ws.RxEvoServiceLocator().getHebRx_HebRxServiceSoapBinding_HTTPPortAddress() + "?WSDL");
        javax.xml.rpc.Service service = serviceFactory.createService(url, new com.heb.rxevo.ws.RxEvoServiceLocator().getServiceName());
        assertTrue(service != null);
    }

    public void test1HebRx_HebRxServiceSoapBinding_HTTPPortGetInsurances() throws Exception {
        com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub binding;
        try {
            binding = (com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub)
                          new com.heb.rxevo.ws.RxEvoServiceLocator().getHebRx_HebRxServiceSoapBinding_HTTPPort();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.heb.rxevo.ws.GetInsurancesResponse value = null;
        value = binding.getInsurances(new com.heb.rxevo.ws.GetInsurances(), new com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo());
        // TBD - validate results
    }

    public void test2HebRx_HebRxServiceSoapBinding_HTTPPortAddAllergies() throws Exception {
        com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub binding;
        try {
            binding = (com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub)
                          new com.heb.rxevo.ws.RxEvoServiceLocator().getHebRx_HebRxServiceSoapBinding_HTTPPort();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.heb.rxevo.ws.AddAllergiesResponse value = null;
        value = binding.addAllergies(new com.heb.rxevo.ws.AddAllergies(), new com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo());
        // TBD - validate results
    }

    public void test3HebRx_HebRxServiceSoapBinding_HTTPPortGetPrescribers() throws Exception {
        com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub binding;
        try {
            binding = (com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub)
                          new com.heb.rxevo.ws.RxEvoServiceLocator().getHebRx_HebRxServiceSoapBinding_HTTPPort();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.heb.rxevo.ws.GetPrescribersResponse value = null;
        value = binding.getPrescribers(new com.heb.rxevo.ws.GetPrescribers(), new com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo());
        // TBD - validate results
    }

    public void test4HebRx_HebRxServiceSoapBinding_HTTPPortGetPrescriptionHistory() throws Exception {
        com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub binding;
        try {
            binding = (com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub)
                          new com.heb.rxevo.ws.RxEvoServiceLocator().getHebRx_HebRxServiceSoapBinding_HTTPPort();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.heb.rxevo.ws.GetPrescriptionHistoryResponse value = null;
        value = binding.getPrescriptionHistory(new com.heb.rxevo.ws.GetPrescriptionHistory(), new com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo());
        // TBD - validate results
    }

    public void test5HebRx_HebRxServiceSoapBinding_HTTPPortUpdatePatientPhoneNumber() throws Exception {
        com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub binding;
        try {
            binding = (com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub)
                          new com.heb.rxevo.ws.RxEvoServiceLocator().getHebRx_HebRxServiceSoapBinding_HTTPPort();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.heb.rxevo.ws.UpdatePatientPhoneNumberResponse value = null;
        value = binding.updatePatientPhoneNumber(new com.heb.rxevo.ws.UpdatePatientPhoneNumber(), new com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo());
        // TBD - validate results
    }

    public void test6HebRx_HebRxServiceSoapBinding_HTTPPortGetAllergies() throws Exception {
        com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub binding;
        try {
            binding = (com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub)
                          new com.heb.rxevo.ws.RxEvoServiceLocator().getHebRx_HebRxServiceSoapBinding_HTTPPort();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.heb.rxevo.ws.GetAllergiesResponse value = null;
        value = binding.getAllergies(new com.heb.rxevo.ws.GetAllergies(), new com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo());
        // TBD - validate results
    }

    public void test7HebRx_HebRxServiceSoapBinding_HTTPPortAddInsurance() throws Exception {
        com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub binding;
        try {
            binding = (com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub)
                          new com.heb.rxevo.ws.RxEvoServiceLocator().getHebRx_HebRxServiceSoapBinding_HTTPPort();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.heb.rxevo.ws.AddInsuranceResponse value = null;
        value = binding.addInsurance(new com.heb.rxevo.ws.AddInsurance(), new com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo());
        // TBD - validate results
    }

    public void test8HebRx_HebRxServiceSoapBinding_HTTPPortUpdatePatientMobileNumber() throws Exception {
        com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub binding;
        try {
            binding = (com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub)
                          new com.heb.rxevo.ws.RxEvoServiceLocator().getHebRx_HebRxServiceSoapBinding_HTTPPort();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.heb.rxevo.ws.UpdatePatientMobileNumberResponse value = null;
        value = binding.updatePatientMobileNumber(new com.heb.rxevo.ws.UpdatePatientMobileNumber(), new com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo());
        // TBD - validate results
    }

    public void test9HebRx_HebRxServiceSoapBinding_HTTPPortSubmitNewPrescription() throws Exception {
        com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub binding;
        try {
            binding = (com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub)
                          new com.heb.rxevo.ws.RxEvoServiceLocator().getHebRx_HebRxServiceSoapBinding_HTTPPort();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.heb.rxevo.ws.SubmitNewPrescriptionResponse value = null;
        value = binding.submitNewPrescription(new com.heb.rxevo.ws.SubmitNewPrescription(), new com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo());
        // TBD - validate results
    }

    public void test10HebRx_HebRxServiceSoapBinding_HTTPPortSubmitTransferRequest() throws Exception {
        com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub binding;
        try {
            binding = (com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub)
                          new com.heb.rxevo.ws.RxEvoServiceLocator().getHebRx_HebRxServiceSoapBinding_HTTPPort();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.heb.rxevo.ws.SubmitTransferRequestResponse value = null;
        value = binding.submitTransferRequest(new com.heb.rxevo.ws.SubmitTransferRequest(), new com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo());
        // TBD - validate results
    }

    public void test11HebRx_HebRxServiceSoapBinding_HTTPPortUpdateRefillExpressForRx() throws Exception {
        com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub binding;
        try {
            binding = (com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub)
                          new com.heb.rxevo.ws.RxEvoServiceLocator().getHebRx_HebRxServiceSoapBinding_HTTPPort();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.heb.rxevo.ws.UpdateRefillExpressForRxResponse value = null;
        value = binding.updateRefillExpressForRx(new com.heb.rxevo.ws.UpdateRefillExpressForRx(), new com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo());
        // TBD - validate results
    }

    public void test12HebRx_HebRxServiceSoapBinding_HTTPPortEnableRefillExpressForPatient() throws Exception {
        com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub binding;
        try {
            binding = (com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub)
                          new com.heb.rxevo.ws.RxEvoServiceLocator().getHebRx_HebRxServiceSoapBinding_HTTPPort();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.heb.rxevo.ws.EnableRefillExpressForPatientResponse value = null;
        value = binding.enableRefillExpressForPatient(new com.heb.rxevo.ws.EnableRefillExpressForPatient(), new com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo());
        // TBD - validate results
    }

    public void test13HebRx_HebRxServiceSoapBinding_HTTPPortCheckRxStatus() throws Exception {
        com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub binding;
        try {
            binding = (com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub)
                          new com.heb.rxevo.ws.RxEvoServiceLocator().getHebRx_HebRxServiceSoapBinding_HTTPPort();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.heb.rxevo.ws.CheckRxStatusResponse value = null;
        value = binding.checkRxStatus(new com.heb.rxevo.ws.CheckRxStatus(), new com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo());
        // TBD - validate results
    }

    public void test14HebRx_HebRxServiceSoapBinding_HTTPPortUpdateNotificationPreferences() throws Exception {
        com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub binding;
        try {
            binding = (com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub)
                          new com.heb.rxevo.ws.RxEvoServiceLocator().getHebRx_HebRxServiceSoapBinding_HTTPPort();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.heb.rxevo.ws.UpdateNotificationPreferencesResponse value = null;
        value = binding.updateNotificationPreferences(new com.heb.rxevo.ws.UpdateNotificationPreferences(), new com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo());
        // TBD - validate results
    }

    public void test15HebRx_HebRxServiceSoapBinding_HTTPPortUpdateRefillExpressForPatient() throws Exception {
        com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub binding;
        try {
            binding = (com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub)
                          new com.heb.rxevo.ws.RxEvoServiceLocator().getHebRx_HebRxServiceSoapBinding_HTTPPort();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.heb.rxevo.ws.UpdateRefillExpressForPatientResponse value = null;
        value = binding.updateRefillExpressForPatient(new com.heb.rxevo.ws.UpdateRefillExpressForPatient(), new com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo());
        // TBD - validate results
    }

    public void test16HebRx_HebRxServiceSoapBinding_HTTPPortGetRevoPatient() throws Exception {
        com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub binding;
        try {
            binding = (com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub)
                          new com.heb.rxevo.ws.RxEvoServiceLocator().getHebRx_HebRxServiceSoapBinding_HTTPPort();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.heb.rxevo.ws.GetRevoPatientResponse value = null;
        value = binding.getRevoPatient(new com.heb.rxevo.ws.GetRevoPatient(), new com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo());
        // TBD - validate results
    }

    public void test17HebRx_HebRxServiceSoapBinding_HTTPPortValidatePatient() throws Exception {
        com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub binding;
        try {
            binding = (com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub)
                          new com.heb.rxevo.ws.RxEvoServiceLocator().getHebRx_HebRxServiceSoapBinding_HTTPPort();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.heb.rxevo.ws.ValidatePatientResponse value = null;
        value = binding.validatePatient(new com.heb.rxevo.ws.ValidatePatient(), new com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo());
        // TBD - validate results
    }

    public void test18HebRx_HebRxServiceSoapBinding_HTTPPortGetPatientSummary() throws Exception {
        com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub binding;
        try {
            binding = (com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub)
                          new com.heb.rxevo.ws.RxEvoServiceLocator().getHebRx_HebRxServiceSoapBinding_HTTPPort();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.heb.rxevo.ws.GetPatientSummaryResponse value = null;
        value = binding.getPatientSummary(new com.heb.rxevo.ws.GetPatientSummary(), new com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo());
        // TBD - validate results
    }

    public void test19HebRx_HebRxServiceSoapBinding_HTTPPortRemoveAllergies() throws Exception {
        com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub binding;
        try {
            binding = (com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub)
                          new com.heb.rxevo.ws.RxEvoServiceLocator().getHebRx_HebRxServiceSoapBinding_HTTPPort();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.heb.rxevo.ws.RemoveAllergiesResponse value = null;
        value = binding.removeAllergies(new com.heb.rxevo.ws.RemoveAllergies(), new com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo());
        // TBD - validate results
    }

    public void test20HebRx_HebRxServiceSoapBinding_HTTPPortGetRevoPharmacy() throws Exception {
        com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub binding;
        try {
            binding = (com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub)
                          new com.heb.rxevo.ws.RxEvoServiceLocator().getHebRx_HebRxServiceSoapBinding_HTTPPort();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.heb.rxevo.ws.GetRevoPharmacyResponse value = null;
        value = binding.getRevoPharmacy(new com.heb.rxevo.ws.GetRevoPharmacy(), new com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo());
        // TBD - validate results
    }

    public void test21HebRx_HebRxServiceSoapBinding_HTTPPortConfirmProfile() throws Exception {
        com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub binding;
        try {
            binding = (com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub)
                          new com.heb.rxevo.ws.RxEvoServiceLocator().getHebRx_HebRxServiceSoapBinding_HTTPPort();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.heb.rxevo.ws.ConfirmProfileResponse value = null;
        value = binding.confirmProfile(new com.heb.rxevo.ws.ConfirmProfile(), new com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo());
        // TBD - validate results
    }

    public void test22HebRx_HebRxServiceSoapBinding_HTTPPortRemoveInsurance() throws Exception {
        com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub binding;
        try {
            binding = (com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub)
                          new com.heb.rxevo.ws.RxEvoServiceLocator().getHebRx_HebRxServiceSoapBinding_HTTPPort();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.heb.rxevo.ws.RemoveInsuranceResponse value = null;
        value = binding.removeInsurance(new com.heb.rxevo.ws.RemoveInsurance(), new com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo());
        // TBD - validate results
    }

    public void test23HebRx_HebRxServiceSoapBinding_HTTPPortUpdatePatientEmailAddress() throws Exception {
        com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub binding;
        try {
            binding = (com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub)
                          new com.heb.rxevo.ws.RxEvoServiceLocator().getHebRx_HebRxServiceSoapBinding_HTTPPort();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.heb.rxevo.ws.UpdatePatientEmailAddressResponse value = null;
        value = binding.updatePatientEmailAddress(new com.heb.rxevo.ws.UpdatePatientEmailAddress(), new com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo());
        // TBD - validate results
    }

    public void test24HebRx_HebRxServiceSoapBinding_HTTPPortEnableRefillExpressForRx() throws Exception {
        com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub binding;
        try {
            binding = (com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub)
                          new com.heb.rxevo.ws.RxEvoServiceLocator().getHebRx_HebRxServiceSoapBinding_HTTPPort();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.heb.rxevo.ws.EnableRefillExpressForRxResponse value = null;
        value = binding.enableRefillExpressForRx(new com.heb.rxevo.ws.EnableRefillExpressForRx(), new com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo());
        // TBD - validate results
    }

    public void test25HebRx_HebRxServiceSoapBinding_HTTPPortLinkProfile() throws Exception {
        com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub binding;
        try {
            binding = (com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub)
                          new com.heb.rxevo.ws.RxEvoServiceLocator().getHebRx_HebRxServiceSoapBinding_HTTPPort();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.heb.rxevo.ws.LinkProfileResponse value = null;
        value = binding.linkProfile(new com.heb.rxevo.ws.LinkProfile(), new com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo());
        // TBD - validate results
    }

    public void test26HebRx_HebRxServiceSoapBinding_HTTPPortSubmitOrder() throws Exception {
        com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub binding;
        try {
            binding = (com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub)
                          new com.heb.rxevo.ws.RxEvoServiceLocator().getHebRx_HebRxServiceSoapBinding_HTTPPort();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.heb.rxevo.ws.SubmitOrderResponse value = null;
        value = binding.submitOrder(new com.heb.rxevo.ws.SubmitOrder(), new com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo());
        // TBD - validate results
    }

    public void test27HebRx_HebRxServiceSoapBinding_HTTPPortUpdatePatientRelationship() throws Exception {
        com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub binding;
        try {
            binding = (com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub)
                          new com.heb.rxevo.ws.RxEvoServiceLocator().getHebRx_HebRxServiceSoapBinding_HTTPPort();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.heb.rxevo.ws.UpdatePatientRelationshipResponse value = null;
        value = binding.updatePatientRelationship(new com.heb.rxevo.ws.UpdatePatientRelationship(), new com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo());
        // TBD - validate results
    }

    public void test28HebRx_HebRxServiceSoapBinding_HTTPPortValidatePatientByGlobalFillId() throws Exception {
        com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub binding;
        try {
            binding = (com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub)
                          new com.heb.rxevo.ws.RxEvoServiceLocator().getHebRx_HebRxServiceSoapBinding_HTTPPort();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.heb.rxevo.ws.ValidatePatientByGlobalFillIdResponse value = null;
        value = binding.validatePatientByGlobalFillId(new com.heb.rxevo.ws.ValidatePatientByGlobalFillId(), new com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo());
        // TBD - validate results
    }

    public void test29HebRx_HebRxServiceSoapBinding_HTTPPortUpdatePatientRecord() throws Exception {
        com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub binding;
        try {
            binding = (com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub)
                          new com.heb.rxevo.ws.RxEvoServiceLocator().getHebRx_HebRxServiceSoapBinding_HTTPPort();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.heb.rxevo.ws.UpdatePatientRecordResponse value = null;
        value = binding.updatePatientRecord(new com.heb.rxevo.ws.UpdatePatientRecord(), new com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo());
        // TBD - validate results
    }

    public void test30HebRx_HebRxServiceSoapBinding_HTTPPortGetPatientSummaries() throws Exception {
        com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub binding;
        try {
            binding = (com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub)
                          new com.heb.rxevo.ws.RxEvoServiceLocator().getHebRx_HebRxServiceSoapBinding_HTTPPort();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.heb.rxevo.ws.GetPatientSummariesResponse value = null;
        value = binding.getPatientSummaries(new com.heb.rxevo.ws.GetPatientSummaries(), new com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo());
        // TBD - validate results
    }

    public void test31HebRx_HebRxServiceSoapBinding_HTTPPortContactDr() throws Exception {
        com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub binding;
        try {
            binding = (com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub)
                          new com.heb.rxevo.ws.RxEvoServiceLocator().getHebRx_HebRxServiceSoapBinding_HTTPPort();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.heb.rxevo.ws.ContactDrResponse value = null;
        value = binding.contactDr(new com.heb.rxevo.ws.ContactDr(), new com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo());
        // TBD - validate results
    }

    public void test32HebRx_HebRxServiceSoapBinding_HTTPPortRefillRx() throws Exception {
        com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub binding;
        try {
            binding = (com.heb.rxevo.ws.HebRx_HebRxServiceSoapBindingStub)
                          new com.heb.rxevo.ws.RxEvoServiceLocator().getHebRx_HebRxServiceSoapBinding_HTTPPort();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.heb.rxevo.ws.RefillRxResponse value = null;
        value = binding.refillRx(new com.heb.rxevo.ws.RefillRx(), new com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo());
        // TBD - validate results
    }

}
