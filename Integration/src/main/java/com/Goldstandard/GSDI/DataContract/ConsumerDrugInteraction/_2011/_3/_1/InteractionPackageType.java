/**
 * InteractionPackageType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1;

public class InteractionPackageType  implements java.io.Serializable {
    private java.lang.String identifier;

    private com.Goldstandard.GSDI.DataContract._2011._3._1.PackageIdentifierTypeEnum packageIdentifierType;

    private java.lang.String productName;

    private com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.LifestyleInteractionType[] lifestyle;

    private com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.DrugType drug;

    public InteractionPackageType() {
    }

    public InteractionPackageType(
           java.lang.String identifier,
           com.Goldstandard.GSDI.DataContract._2011._3._1.PackageIdentifierTypeEnum packageIdentifierType,
           java.lang.String productName,
           com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.LifestyleInteractionType[] lifestyle,
           com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.DrugType drug) {
           this.identifier = identifier;
           this.packageIdentifierType = packageIdentifierType;
           this.productName = productName;
           this.lifestyle = lifestyle;
           this.drug = drug;
    }


    /**
     * Gets the identifier value for this InteractionPackageType.
     * 
     * @return identifier
     */
    public java.lang.String getIdentifier() {
        return identifier;
    }


    /**
     * Sets the identifier value for this InteractionPackageType.
     * 
     * @param identifier
     */
    public void setIdentifier(java.lang.String identifier) {
        this.identifier = identifier;
    }


    /**
     * Gets the packageIdentifierType value for this InteractionPackageType.
     * 
     * @return packageIdentifierType
     */
    public com.Goldstandard.GSDI.DataContract._2011._3._1.PackageIdentifierTypeEnum getPackageIdentifierType() {
        return packageIdentifierType;
    }


    /**
     * Sets the packageIdentifierType value for this InteractionPackageType.
     * 
     * @param packageIdentifierType
     */
    public void setPackageIdentifierType(com.Goldstandard.GSDI.DataContract._2011._3._1.PackageIdentifierTypeEnum packageIdentifierType) {
        this.packageIdentifierType = packageIdentifierType;
    }


    /**
     * Gets the productName value for this InteractionPackageType.
     * 
     * @return productName
     */
    public java.lang.String getProductName() {
        return productName;
    }


    /**
     * Sets the productName value for this InteractionPackageType.
     * 
     * @param productName
     */
    public void setProductName(java.lang.String productName) {
        this.productName = productName;
    }


    /**
     * Gets the lifestyle value for this InteractionPackageType.
     * 
     * @return lifestyle
     */
    public com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.LifestyleInteractionType[] getLifestyle() {
        return lifestyle;
    }


    /**
     * Sets the lifestyle value for this InteractionPackageType.
     * 
     * @param lifestyle
     */
    public void setLifestyle(com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.LifestyleInteractionType[] lifestyle) {
        this.lifestyle = lifestyle;
    }


    /**
     * Gets the drug value for this InteractionPackageType.
     * 
     * @return drug
     */
    public com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.DrugType getDrug() {
        return drug;
    }


    /**
     * Sets the drug value for this InteractionPackageType.
     * 
     * @param drug
     */
    public void setDrug(com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.DrugType drug) {
        this.drug = drug;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof InteractionPackageType)) return false;
        InteractionPackageType other = (InteractionPackageType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.identifier==null && other.getIdentifier()==null) || 
             (this.identifier!=null &&
              this.identifier.equals(other.getIdentifier()))) &&
            ((this.packageIdentifierType==null && other.getPackageIdentifierType()==null) || 
             (this.packageIdentifierType!=null &&
              this.packageIdentifierType.equals(other.getPackageIdentifierType()))) &&
            ((this.productName==null && other.getProductName()==null) || 
             (this.productName!=null &&
              this.productName.equals(other.getProductName()))) &&
            ((this.lifestyle==null && other.getLifestyle()==null) || 
             (this.lifestyle!=null &&
              java.util.Arrays.equals(this.lifestyle, other.getLifestyle()))) &&
            ((this.drug==null && other.getDrug()==null) || 
             (this.drug!=null &&
              this.drug.equals(other.getDrug())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getIdentifier() != null) {
            _hashCode += getIdentifier().hashCode();
        }
        if (getPackageIdentifierType() != null) {
            _hashCode += getPackageIdentifierType().hashCode();
        }
        if (getProductName() != null) {
            _hashCode += getProductName().hashCode();
        }
        if (getLifestyle() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getLifestyle());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getLifestyle(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getDrug() != null) {
            _hashCode += getDrug().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(InteractionPackageType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "InteractionPackageType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("identifier");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "Identifier"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("packageIdentifierType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "PackageIdentifierType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "PackageIdentifierTypeEnum"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "ProductName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lifestyle");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "Lifestyle"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "LifestyleInteractionType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "LifestyleInteractionType"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("drug");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "Drug"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "DrugType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
