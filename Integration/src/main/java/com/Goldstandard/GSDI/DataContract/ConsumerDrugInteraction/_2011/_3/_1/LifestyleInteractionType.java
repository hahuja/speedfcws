/**
 * LifestyleInteractionType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1;

public class LifestyleInteractionType  implements java.io.Serializable {
    private int lifestyleID;

    private int conceptID;

    private java.lang.String conceptName;

    private com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.LifestyleDrugClassType lifestyleDrugClass;

    private com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.SeverityType severity;

    private java.lang.String consumerNotes;

    public LifestyleInteractionType() {
    }

    public LifestyleInteractionType(
           int lifestyleID,
           int conceptID,
           java.lang.String conceptName,
           com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.LifestyleDrugClassType lifestyleDrugClass,
           com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.SeverityType severity,
           java.lang.String consumerNotes) {
           this.lifestyleID = lifestyleID;
           this.conceptID = conceptID;
           this.conceptName = conceptName;
           this.lifestyleDrugClass = lifestyleDrugClass;
           this.severity = severity;
           this.consumerNotes = consumerNotes;
    }


    /**
     * Gets the lifestyleID value for this LifestyleInteractionType.
     * 
     * @return lifestyleID
     */
    public int getLifestyleID() {
        return lifestyleID;
    }


    /**
     * Sets the lifestyleID value for this LifestyleInteractionType.
     * 
     * @param lifestyleID
     */
    public void setLifestyleID(int lifestyleID) {
        this.lifestyleID = lifestyleID;
    }


    /**
     * Gets the conceptID value for this LifestyleInteractionType.
     * 
     * @return conceptID
     */
    public int getConceptID() {
        return conceptID;
    }


    /**
     * Sets the conceptID value for this LifestyleInteractionType.
     * 
     * @param conceptID
     */
    public void setConceptID(int conceptID) {
        this.conceptID = conceptID;
    }


    /**
     * Gets the conceptName value for this LifestyleInteractionType.
     * 
     * @return conceptName
     */
    public java.lang.String getConceptName() {
        return conceptName;
    }


    /**
     * Sets the conceptName value for this LifestyleInteractionType.
     * 
     * @param conceptName
     */
    public void setConceptName(java.lang.String conceptName) {
        this.conceptName = conceptName;
    }


    /**
     * Gets the lifestyleDrugClass value for this LifestyleInteractionType.
     * 
     * @return lifestyleDrugClass
     */
    public com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.LifestyleDrugClassType getLifestyleDrugClass() {
        return lifestyleDrugClass;
    }


    /**
     * Sets the lifestyleDrugClass value for this LifestyleInteractionType.
     * 
     * @param lifestyleDrugClass
     */
    public void setLifestyleDrugClass(com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.LifestyleDrugClassType lifestyleDrugClass) {
        this.lifestyleDrugClass = lifestyleDrugClass;
    }


    /**
     * Gets the severity value for this LifestyleInteractionType.
     * 
     * @return severity
     */
    public com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.SeverityType getSeverity() {
        return severity;
    }


    /**
     * Sets the severity value for this LifestyleInteractionType.
     * 
     * @param severity
     */
    public void setSeverity(com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.SeverityType severity) {
        this.severity = severity;
    }


    /**
     * Gets the consumerNotes value for this LifestyleInteractionType.
     * 
     * @return consumerNotes
     */
    public java.lang.String getConsumerNotes() {
        return consumerNotes;
    }


    /**
     * Sets the consumerNotes value for this LifestyleInteractionType.
     * 
     * @param consumerNotes
     */
    public void setConsumerNotes(java.lang.String consumerNotes) {
        this.consumerNotes = consumerNotes;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof LifestyleInteractionType)) return false;
        LifestyleInteractionType other = (LifestyleInteractionType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.lifestyleID == other.getLifestyleID() &&
            this.conceptID == other.getConceptID() &&
            ((this.conceptName==null && other.getConceptName()==null) || 
             (this.conceptName!=null &&
              this.conceptName.equals(other.getConceptName()))) &&
            ((this.lifestyleDrugClass==null && other.getLifestyleDrugClass()==null) || 
             (this.lifestyleDrugClass!=null &&
              this.lifestyleDrugClass.equals(other.getLifestyleDrugClass()))) &&
            ((this.severity==null && other.getSeverity()==null) || 
             (this.severity!=null &&
              this.severity.equals(other.getSeverity()))) &&
            ((this.consumerNotes==null && other.getConsumerNotes()==null) || 
             (this.consumerNotes!=null &&
              this.consumerNotes.equals(other.getConsumerNotes())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getLifestyleID();
        _hashCode += getConceptID();
        if (getConceptName() != null) {
            _hashCode += getConceptName().hashCode();
        }
        if (getLifestyleDrugClass() != null) {
            _hashCode += getLifestyleDrugClass().hashCode();
        }
        if (getSeverity() != null) {
            _hashCode += getSeverity().hashCode();
        }
        if (getConsumerNotes() != null) {
            _hashCode += getConsumerNotes().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(LifestyleInteractionType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "LifestyleInteractionType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lifestyleID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "LifestyleID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("conceptID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "ConceptID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("conceptName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "ConceptName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lifestyleDrugClass");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "LifestyleDrugClass"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "LifestyleDrugClassType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("severity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "Severity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "SeverityType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("consumerNotes");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "ConsumerNotes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
