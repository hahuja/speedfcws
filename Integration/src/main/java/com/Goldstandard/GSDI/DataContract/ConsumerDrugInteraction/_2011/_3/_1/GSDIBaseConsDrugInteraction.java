/**
 * GSDIBaseConsDrugInteraction.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1;

public interface GSDIBaseConsDrugInteraction extends javax.xml.rpc.Service {
    public java.lang.String getGSDIBaseConsDrugInteractionSoap12Address();

    public com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.GSDIBaseConsDrugInteractionSoap12 getGSDIBaseConsDrugInteractionSoap12() throws javax.xml.rpc.ServiceException;

    public com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.GSDIBaseConsDrugInteractionSoap12 getGSDIBaseConsDrugInteractionSoap12(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
    public java.lang.String getGSDIBaseConsDrugInteractionSoapAddress();

    public com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.GSDIBaseConsDrugInteractionSoap getGSDIBaseConsDrugInteractionSoap() throws javax.xml.rpc.ServiceException;

    public com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.GSDIBaseConsDrugInteractionSoap getGSDIBaseConsDrugInteractionSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
    public java.lang.String getIGSDIBaseConsDrugInteraction1Address();

    public com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.IGSDIBaseConsDrugInteraction getIGSDIBaseConsDrugInteraction1() throws javax.xml.rpc.ServiceException;

    public com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.IGSDIBaseConsDrugInteraction getIGSDIBaseConsDrugInteraction1(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
    public java.lang.String getIGSDIBaseConsDrugInteractionAddress();

    public com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.IGSDIBaseConsDrugInteraction getIGSDIBaseConsDrugInteraction() throws javax.xml.rpc.ServiceException;

    public com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.IGSDIBaseConsDrugInteraction getIGSDIBaseConsDrugInteraction(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
