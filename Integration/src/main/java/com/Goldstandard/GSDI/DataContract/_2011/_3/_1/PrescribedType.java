/**
 * PrescribedType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.Goldstandard.GSDI.DataContract._2011._3._1;

public class PrescribedType  implements java.io.Serializable {
    private com.Goldstandard.GSDI.DataContract._2011._3._1.PackageIdentifierType[] packages;

    private com.Goldstandard.GSDI.DataContract._2011._3._1.ProductIdentifierType[] products;

    private int[] marketedProductId;

    private int[] representativeMarketedProductId;

    public PrescribedType() {
    }

    public PrescribedType(
           com.Goldstandard.GSDI.DataContract._2011._3._1.PackageIdentifierType[] packages,
           com.Goldstandard.GSDI.DataContract._2011._3._1.ProductIdentifierType[] products,
           int[] marketedProductId,
           int[] representativeMarketedProductId) {
           this.packages = packages;
           this.products = products;
           this.marketedProductId = marketedProductId;
           this.representativeMarketedProductId = representativeMarketedProductId;
    }


    /**
     * Gets the packages value for this PrescribedType.
     * 
     * @return packages
     */
    public com.Goldstandard.GSDI.DataContract._2011._3._1.PackageIdentifierType[] getPackages() {
        return packages;
    }


    /**
     * Sets the packages value for this PrescribedType.
     * 
     * @param packages
     */
    public void setPackages(com.Goldstandard.GSDI.DataContract._2011._3._1.PackageIdentifierType[] packages) {
        this.packages = packages;
    }


    /**
     * Gets the products value for this PrescribedType.
     * 
     * @return products
     */
    public com.Goldstandard.GSDI.DataContract._2011._3._1.ProductIdentifierType[] getProducts() {
        return products;
    }


    /**
     * Sets the products value for this PrescribedType.
     * 
     * @param products
     */
    public void setProducts(com.Goldstandard.GSDI.DataContract._2011._3._1.ProductIdentifierType[] products) {
        this.products = products;
    }


    /**
     * Gets the marketedProductId value for this PrescribedType.
     * 
     * @return marketedProductId
     */
    public int[] getMarketedProductId() {
        return marketedProductId;
    }


    /**
     * Sets the marketedProductId value for this PrescribedType.
     * 
     * @param marketedProductId
     */
    public void setMarketedProductId(int[] marketedProductId) {
        this.marketedProductId = marketedProductId;
    }


    /**
     * Gets the representativeMarketedProductId value for this PrescribedType.
     * 
     * @return representativeMarketedProductId
     */
    public int[] getRepresentativeMarketedProductId() {
        return representativeMarketedProductId;
    }


    /**
     * Sets the representativeMarketedProductId value for this PrescribedType.
     * 
     * @param representativeMarketedProductId
     */
    public void setRepresentativeMarketedProductId(int[] representativeMarketedProductId) {
        this.representativeMarketedProductId = representativeMarketedProductId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PrescribedType)) return false;
        PrescribedType other = (PrescribedType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.packages==null && other.getPackages()==null) || 
             (this.packages!=null &&
              java.util.Arrays.equals(this.packages, other.getPackages()))) &&
            ((this.products==null && other.getProducts()==null) || 
             (this.products!=null &&
              java.util.Arrays.equals(this.products, other.getProducts()))) &&
            ((this.marketedProductId==null && other.getMarketedProductId()==null) || 
             (this.marketedProductId!=null &&
              java.util.Arrays.equals(this.marketedProductId, other.getMarketedProductId()))) &&
            ((this.representativeMarketedProductId==null && other.getRepresentativeMarketedProductId()==null) || 
             (this.representativeMarketedProductId!=null &&
              java.util.Arrays.equals(this.representativeMarketedProductId, other.getRepresentativeMarketedProductId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPackages() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPackages());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPackages(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getProducts() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getProducts());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getProducts(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getMarketedProductId() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getMarketedProductId());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getMarketedProductId(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getRepresentativeMarketedProductId() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getRepresentativeMarketedProductId());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getRepresentativeMarketedProductId(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PrescribedType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "PrescribedType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("packages");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "Packages"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "PackageIdentifierType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "PackageIdentifierType"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("products");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "Products"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "ProductIdentifierType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "ProductIdentifierType"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("marketedProductId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "MarketedProductId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "int"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("representativeMarketedProductId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "RepresentativeMarketedProductId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "int"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
