/**
 * GSDIBasePatientEducationLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1;

public class GSDIBasePatientEducationLocator extends org.apache.axis.client.Service implements com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.GSDIBasePatientEducation {

    public GSDIBasePatientEducationLocator() {
    }


    public GSDIBasePatientEducationLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public GSDIBasePatientEducationLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for IGSDIBasePatientEducation1
    private java.lang.String IGSDIBasePatientEducation1_address = "http://gsdi.goldstandard.com/GSDIPatientEducationWS.asmx";

    public java.lang.String getIGSDIBasePatientEducation1Address() {
        return IGSDIBasePatientEducation1_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String IGSDIBasePatientEducation1WSDDServiceName = "IGSDIBasePatientEducation1";

    public java.lang.String getIGSDIBasePatientEducation1WSDDServiceName() {
        return IGSDIBasePatientEducation1WSDDServiceName;
    }

    public void setIGSDIBasePatientEducation1WSDDServiceName(java.lang.String name) {
        IGSDIBasePatientEducation1WSDDServiceName = name;
    }

    public com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.IGSDIBasePatientEducation getIGSDIBasePatientEducation1() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(IGSDIBasePatientEducation1_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getIGSDIBasePatientEducation1(endpoint);
    }

    public com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.IGSDIBasePatientEducation getIGSDIBasePatientEducation1(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.IGSDIBasePatientEducation1Stub _stub = new com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.IGSDIBasePatientEducation1Stub(portAddress, this);
            _stub.setPortName(getIGSDIBasePatientEducation1WSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setIGSDIBasePatientEducation1EndpointAddress(java.lang.String address) {
        IGSDIBasePatientEducation1_address = address;
    }


    // Use to get a proxy class for GSDIBasePatientEducationSoap
    private java.lang.String GSDIBasePatientEducationSoap_address = "http://gsdi.goldstandard.com/GSDIPatientEducationWS.asmx";

    public java.lang.String getGSDIBasePatientEducationSoapAddress() {
        return GSDIBasePatientEducationSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String GSDIBasePatientEducationSoapWSDDServiceName = "GSDIBasePatientEducationSoap";

    public java.lang.String getGSDIBasePatientEducationSoapWSDDServiceName() {
        return GSDIBasePatientEducationSoapWSDDServiceName;
    }

    public void setGSDIBasePatientEducationSoapWSDDServiceName(java.lang.String name) {
        GSDIBasePatientEducationSoapWSDDServiceName = name;
    }

    public com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.GSDIBasePatientEducationSoap getGSDIBasePatientEducationSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(GSDIBasePatientEducationSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getGSDIBasePatientEducationSoap(endpoint);
    }

    public com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.GSDIBasePatientEducationSoap getGSDIBasePatientEducationSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.GSDIBasePatientEducationSoapStub _stub = new com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.GSDIBasePatientEducationSoapStub(portAddress, this);
            _stub.setPortName(getGSDIBasePatientEducationSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setGSDIBasePatientEducationSoapEndpointAddress(java.lang.String address) {
        GSDIBasePatientEducationSoap_address = address;
    }


    // Use to get a proxy class for IGSDIBasePatientEducation
    private java.lang.String IGSDIBasePatientEducation_address = "http://gsdi.goldstandard.com/GSDIPatientEducationWS.asmx";

    public java.lang.String getIGSDIBasePatientEducationAddress() {
        return IGSDIBasePatientEducation_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String IGSDIBasePatientEducationWSDDServiceName = "IGSDIBasePatientEducation";

    public java.lang.String getIGSDIBasePatientEducationWSDDServiceName() {
        return IGSDIBasePatientEducationWSDDServiceName;
    }

    public void setIGSDIBasePatientEducationWSDDServiceName(java.lang.String name) {
        IGSDIBasePatientEducationWSDDServiceName = name;
    }

    public com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.IGSDIBasePatientEducation getIGSDIBasePatientEducation() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(IGSDIBasePatientEducation_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getIGSDIBasePatientEducation(endpoint);
    }

    public com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.IGSDIBasePatientEducation getIGSDIBasePatientEducation(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.IGSDIBasePatientEducationStub _stub = new com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.IGSDIBasePatientEducationStub(portAddress, this);
            _stub.setPortName(getIGSDIBasePatientEducationWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setIGSDIBasePatientEducationEndpointAddress(java.lang.String address) {
        IGSDIBasePatientEducation_address = address;
    }


    // Use to get a proxy class for GSDIBasePatientEducationSoap12
    private java.lang.String GSDIBasePatientEducationSoap12_address = "http://gsdi.goldstandard.com:8009/GSDIPatientEducationWS.asmx";

    public java.lang.String getGSDIBasePatientEducationSoap12Address() {
        return GSDIBasePatientEducationSoap12_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String GSDIBasePatientEducationSoap12WSDDServiceName = "GSDIBasePatientEducationSoap12";

    public java.lang.String getGSDIBasePatientEducationSoap12WSDDServiceName() {
        return GSDIBasePatientEducationSoap12WSDDServiceName;
    }

    public void setGSDIBasePatientEducationSoap12WSDDServiceName(java.lang.String name) {
        GSDIBasePatientEducationSoap12WSDDServiceName = name;
    }

    public com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.GSDIBasePatientEducationSoap12 getGSDIBasePatientEducationSoap12() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(GSDIBasePatientEducationSoap12_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getGSDIBasePatientEducationSoap12(endpoint);
    }

    public com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.GSDIBasePatientEducationSoap12 getGSDIBasePatientEducationSoap12(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.GSDIBasePatientEducationSoap12Stub _stub = new com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.GSDIBasePatientEducationSoap12Stub(portAddress, this);
            _stub.setPortName(getGSDIBasePatientEducationSoap12WSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setGSDIBasePatientEducationSoap12EndpointAddress(java.lang.String address) {
        GSDIBasePatientEducationSoap12_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     * This service has multiple ports for a given interface;
     * the proxy implementation returned may be indeterminate.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.IGSDIBasePatientEducation.class.isAssignableFrom(serviceEndpointInterface)) {
                com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.IGSDIBasePatientEducation1Stub _stub = new com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.IGSDIBasePatientEducation1Stub(new java.net.URL(IGSDIBasePatientEducation1_address), this);
                _stub.setPortName(getIGSDIBasePatientEducation1WSDDServiceName());
                return _stub;
            }
            if (com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.GSDIBasePatientEducationSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.GSDIBasePatientEducationSoapStub _stub = new com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.GSDIBasePatientEducationSoapStub(new java.net.URL(GSDIBasePatientEducationSoap_address), this);
                _stub.setPortName(getGSDIBasePatientEducationSoapWSDDServiceName());
                return _stub;
            }
            if (com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.IGSDIBasePatientEducation.class.isAssignableFrom(serviceEndpointInterface)) {
                com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.IGSDIBasePatientEducationStub _stub = new com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.IGSDIBasePatientEducationStub(new java.net.URL(IGSDIBasePatientEducation_address), this);
                _stub.setPortName(getIGSDIBasePatientEducationWSDDServiceName());
                return _stub;
            }
            if (com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.GSDIBasePatientEducationSoap12.class.isAssignableFrom(serviceEndpointInterface)) {
                com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.GSDIBasePatientEducationSoap12Stub _stub = new com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.GSDIBasePatientEducationSoap12Stub(new java.net.URL(GSDIBasePatientEducationSoap12_address), this);
                _stub.setPortName(getGSDIBasePatientEducationSoap12WSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("IGSDIBasePatientEducation1".equals(inputPortName)) {
            return getIGSDIBasePatientEducation1();
        }
        else if ("GSDIBasePatientEducationSoap".equals(inputPortName)) {
            return getGSDIBasePatientEducationSoap();
        }
        else if ("IGSDIBasePatientEducation".equals(inputPortName)) {
            return getIGSDIBasePatientEducation();
        }
        else if ("GSDIBasePatientEducationSoap12".equals(inputPortName)) {
            return getGSDIBasePatientEducationSoap12();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "GSDIBasePatientEducation");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "IGSDIBasePatientEducation1"));
            ports.add(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "GSDIBasePatientEducationSoap"));
            ports.add(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "IGSDIBasePatientEducation"));
            ports.add(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "GSDIBasePatientEducationSoap12"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("IGSDIBasePatientEducation1".equals(portName)) {
            setIGSDIBasePatientEducation1EndpointAddress(address);
        }
        else 
if ("GSDIBasePatientEducationSoap".equals(portName)) {
            setGSDIBasePatientEducationSoapEndpointAddress(address);
        }
        else 
if ("IGSDIBasePatientEducation".equals(portName)) {
            setIGSDIBasePatientEducationEndpointAddress(address);
        }
        else 
if ("GSDIBasePatientEducationSoap12".equals(portName)) {
            setGSDIBasePatientEducationSoap12EndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
