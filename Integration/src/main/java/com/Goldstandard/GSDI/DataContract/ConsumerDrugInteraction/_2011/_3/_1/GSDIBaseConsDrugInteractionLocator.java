/**
 * GSDIBaseConsDrugInteractionLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1;

public class GSDIBaseConsDrugInteractionLocator extends org.apache.axis.client.Service implements com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.GSDIBaseConsDrugInteraction {

    public GSDIBaseConsDrugInteractionLocator() {
    }


    public GSDIBaseConsDrugInteractionLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public GSDIBaseConsDrugInteractionLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for GSDIBaseConsDrugInteractionSoap12
    private java.lang.String GSDIBaseConsDrugInteractionSoap12_address = "http://gsdi.goldstandard.com:8009/GSDIConsDrugInteractionWS.asmx";

    public java.lang.String getGSDIBaseConsDrugInteractionSoap12Address() {
        return GSDIBaseConsDrugInteractionSoap12_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String GSDIBaseConsDrugInteractionSoap12WSDDServiceName = "GSDIBaseConsDrugInteractionSoap12";

    public java.lang.String getGSDIBaseConsDrugInteractionSoap12WSDDServiceName() {
        return GSDIBaseConsDrugInteractionSoap12WSDDServiceName;
    }

    public void setGSDIBaseConsDrugInteractionSoap12WSDDServiceName(java.lang.String name) {
        GSDIBaseConsDrugInteractionSoap12WSDDServiceName = name;
    }

    public com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.GSDIBaseConsDrugInteractionSoap12 getGSDIBaseConsDrugInteractionSoap12() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(GSDIBaseConsDrugInteractionSoap12_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getGSDIBaseConsDrugInteractionSoap12(endpoint);
    }

    public com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.GSDIBaseConsDrugInteractionSoap12 getGSDIBaseConsDrugInteractionSoap12(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.GSDIBaseConsDrugInteractionSoap12Stub _stub = new com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.GSDIBaseConsDrugInteractionSoap12Stub(portAddress, this);
            _stub.setPortName(getGSDIBaseConsDrugInteractionSoap12WSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setGSDIBaseConsDrugInteractionSoap12EndpointAddress(java.lang.String address) {
        GSDIBaseConsDrugInteractionSoap12_address = address;
    }


    // Use to get a proxy class for GSDIBaseConsDrugInteractionSoap
    private java.lang.String GSDIBaseConsDrugInteractionSoap_address = "http://gsdi.goldstandard.com/GSDIConsDrugInteractionWS.asmx";

    public java.lang.String getGSDIBaseConsDrugInteractionSoapAddress() {
        return GSDIBaseConsDrugInteractionSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String GSDIBaseConsDrugInteractionSoapWSDDServiceName = "GSDIBaseConsDrugInteractionSoap";

    public java.lang.String getGSDIBaseConsDrugInteractionSoapWSDDServiceName() {
        return GSDIBaseConsDrugInteractionSoapWSDDServiceName;
    }

    public void setGSDIBaseConsDrugInteractionSoapWSDDServiceName(java.lang.String name) {
        GSDIBaseConsDrugInteractionSoapWSDDServiceName = name;
    }

    public com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.GSDIBaseConsDrugInteractionSoap getGSDIBaseConsDrugInteractionSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(GSDIBaseConsDrugInteractionSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getGSDIBaseConsDrugInteractionSoap(endpoint);
    }

    public com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.GSDIBaseConsDrugInteractionSoap getGSDIBaseConsDrugInteractionSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.GSDIBaseConsDrugInteractionSoapStub _stub = new com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.GSDIBaseConsDrugInteractionSoapStub(portAddress, this);
            _stub.setPortName(getGSDIBaseConsDrugInteractionSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setGSDIBaseConsDrugInteractionSoapEndpointAddress(java.lang.String address) {
        GSDIBaseConsDrugInteractionSoap_address = address;
    }


    // Use to get a proxy class for IGSDIBaseConsDrugInteraction1
    private java.lang.String IGSDIBaseConsDrugInteraction1_address = "http://gsdi.goldstandard.com/GSDIConsDrugInteractionWS.asmx";

    public java.lang.String getIGSDIBaseConsDrugInteraction1Address() {
        return IGSDIBaseConsDrugInteraction1_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String IGSDIBaseConsDrugInteraction1WSDDServiceName = "IGSDIBaseConsDrugInteraction1";

    public java.lang.String getIGSDIBaseConsDrugInteraction1WSDDServiceName() {
        return IGSDIBaseConsDrugInteraction1WSDDServiceName;
    }

    public void setIGSDIBaseConsDrugInteraction1WSDDServiceName(java.lang.String name) {
        IGSDIBaseConsDrugInteraction1WSDDServiceName = name;
    }

    public com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.IGSDIBaseConsDrugInteraction getIGSDIBaseConsDrugInteraction1() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(IGSDIBaseConsDrugInteraction1_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getIGSDIBaseConsDrugInteraction1(endpoint);
    }

    public com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.IGSDIBaseConsDrugInteraction getIGSDIBaseConsDrugInteraction1(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.IGSDIBaseConsDrugInteraction1Stub _stub = new com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.IGSDIBaseConsDrugInteraction1Stub(portAddress, this);
            _stub.setPortName(getIGSDIBaseConsDrugInteraction1WSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setIGSDIBaseConsDrugInteraction1EndpointAddress(java.lang.String address) {
        IGSDIBaseConsDrugInteraction1_address = address;
    }


    // Use to get a proxy class for IGSDIBaseConsDrugInteraction
    private java.lang.String IGSDIBaseConsDrugInteraction_address = "http://gsdi.goldstandard.com/GSDIConsDrugInteractionWS.asmx";

    public java.lang.String getIGSDIBaseConsDrugInteractionAddress() {
        return IGSDIBaseConsDrugInteraction_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String IGSDIBaseConsDrugInteractionWSDDServiceName = "IGSDIBaseConsDrugInteraction";

    public java.lang.String getIGSDIBaseConsDrugInteractionWSDDServiceName() {
        return IGSDIBaseConsDrugInteractionWSDDServiceName;
    }

    public void setIGSDIBaseConsDrugInteractionWSDDServiceName(java.lang.String name) {
        IGSDIBaseConsDrugInteractionWSDDServiceName = name;
    }

    public com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.IGSDIBaseConsDrugInteraction getIGSDIBaseConsDrugInteraction() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(IGSDIBaseConsDrugInteraction_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getIGSDIBaseConsDrugInteraction(endpoint);
    }

    public com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.IGSDIBaseConsDrugInteraction getIGSDIBaseConsDrugInteraction(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.IGSDIBaseConsDrugInteractionStub _stub = new com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.IGSDIBaseConsDrugInteractionStub(portAddress, this);
            _stub.setPortName(getIGSDIBaseConsDrugInteractionWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setIGSDIBaseConsDrugInteractionEndpointAddress(java.lang.String address) {
        IGSDIBaseConsDrugInteraction_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     * This service has multiple ports for a given interface;
     * the proxy implementation returned may be indeterminate.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.GSDIBaseConsDrugInteractionSoap12.class.isAssignableFrom(serviceEndpointInterface)) {
                com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.GSDIBaseConsDrugInteractionSoap12Stub _stub = new com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.GSDIBaseConsDrugInteractionSoap12Stub(new java.net.URL(GSDIBaseConsDrugInteractionSoap12_address), this);
                _stub.setPortName(getGSDIBaseConsDrugInteractionSoap12WSDDServiceName());
                return _stub;
            }
            if (com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.GSDIBaseConsDrugInteractionSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.GSDIBaseConsDrugInteractionSoapStub _stub = new com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.GSDIBaseConsDrugInteractionSoapStub(new java.net.URL(GSDIBaseConsDrugInteractionSoap_address), this);
                _stub.setPortName(getGSDIBaseConsDrugInteractionSoapWSDDServiceName());
                return _stub;
            }
            if (com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.IGSDIBaseConsDrugInteraction.class.isAssignableFrom(serviceEndpointInterface)) {
                com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.IGSDIBaseConsDrugInteraction1Stub _stub = new com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.IGSDIBaseConsDrugInteraction1Stub(new java.net.URL(IGSDIBaseConsDrugInteraction1_address), this);
                _stub.setPortName(getIGSDIBaseConsDrugInteraction1WSDDServiceName());
                return _stub;
            }
            if (com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.IGSDIBaseConsDrugInteraction.class.isAssignableFrom(serviceEndpointInterface)) {
                com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.IGSDIBaseConsDrugInteractionStub _stub = new com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.IGSDIBaseConsDrugInteractionStub(new java.net.URL(IGSDIBaseConsDrugInteraction_address), this);
                _stub.setPortName(getIGSDIBaseConsDrugInteractionWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("GSDIBaseConsDrugInteractionSoap12".equals(inputPortName)) {
            return getGSDIBaseConsDrugInteractionSoap12();
        }
        else if ("GSDIBaseConsDrugInteractionSoap".equals(inputPortName)) {
            return getGSDIBaseConsDrugInteractionSoap();
        }
        else if ("IGSDIBaseConsDrugInteraction1".equals(inputPortName)) {
            return getIGSDIBaseConsDrugInteraction1();
        }
        else if ("IGSDIBaseConsDrugInteraction".equals(inputPortName)) {
            return getIGSDIBaseConsDrugInteraction();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "GSDIBaseConsDrugInteraction");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "GSDIBaseConsDrugInteractionSoap12"));
            ports.add(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "GSDIBaseConsDrugInteractionSoap"));
            ports.add(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "IGSDIBaseConsDrugInteraction1"));
            ports.add(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "IGSDIBaseConsDrugInteraction"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("GSDIBaseConsDrugInteractionSoap12".equals(portName)) {
            setGSDIBaseConsDrugInteractionSoap12EndpointAddress(address);
        }
        else 
if ("GSDIBaseConsDrugInteractionSoap".equals(portName)) {
            setGSDIBaseConsDrugInteractionSoapEndpointAddress(address);
        }
        else 
if ("IGSDIBaseConsDrugInteraction1".equals(portName)) {
            setIGSDIBaseConsDrugInteraction1EndpointAddress(address);
        }
        else 
if ("IGSDIBaseConsDrugInteraction".equals(portName)) {
            setIGSDIBaseConsDrugInteractionEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
