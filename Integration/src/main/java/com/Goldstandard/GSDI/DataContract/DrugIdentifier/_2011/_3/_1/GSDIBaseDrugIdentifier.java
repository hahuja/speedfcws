/**
 * GSDIBaseDrugIdentifier.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1;

public interface GSDIBaseDrugIdentifier extends javax.xml.rpc.Service {
    public java.lang.String getIGSDIBaseDrugIdentifierAddress();

    public com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.IGSDIBaseDrugIdentifier getIGSDIBaseDrugIdentifier() throws javax.xml.rpc.ServiceException;

    public com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.IGSDIBaseDrugIdentifier getIGSDIBaseDrugIdentifier(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
    public java.lang.String getIGSDIBaseDrugIdentifier1Address();

    public com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.IGSDIBaseDrugIdentifier getIGSDIBaseDrugIdentifier1() throws javax.xml.rpc.ServiceException;

    public com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.IGSDIBaseDrugIdentifier getIGSDIBaseDrugIdentifier1(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
    public java.lang.String getGSDIBaseDrugIdentifierSoap12Address();

    public com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.GSDIBaseDrugIdentifierSoap12 getGSDIBaseDrugIdentifierSoap12() throws javax.xml.rpc.ServiceException;

    public com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.GSDIBaseDrugIdentifierSoap12 getGSDIBaseDrugIdentifierSoap12(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
    public java.lang.String getGSDIBaseDrugIdentifierSoapAddress();

    public com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.GSDIBaseDrugIdentifierSoap getGSDIBaseDrugIdentifierSoap() throws javax.xml.rpc.ServiceException;

    public com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.GSDIBaseDrugIdentifierSoap getGSDIBaseDrugIdentifierSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
