/**
 * IGSDIBaseConsDrugInteraction.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1;

public interface IGSDIBaseConsDrugInteraction extends java.rmi.Remote {
    public com.Goldstandard.GSDI.DataContract._2011._3._1.DrugNameSearchResult searchForDrugNames(java.lang.String authenticationKey, java.lang.String searchTerm, com.Goldstandard.GSDI.DataContract._2011._3._1.TrademarkGenericEnum trademarkOrGeneric) throws java.rmi.RemoteException;
    public com.Goldstandard.GSDI.DataContract._2011._3._1.ProductSearchResult searchForRepresentativeMarketedProductId(java.lang.String authenticationKey, java.lang.String searchTerm) throws java.rmi.RemoteException;
    public com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.SeverityRankingDescription[] getConsumerSeverityRankingDescriptions(java.lang.String authenticationKey) throws java.rmi.RemoteException;
    public com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.ConsumerInteractionReport getConsumerInteractions(java.lang.String authenticationKey, com.Goldstandard.GSDI.DataContract._2011._3._1.ProductIdentifierType[] products, com.Goldstandard.GSDI.DataContract._2011._3._1.PackageIdentifierType[] packages, int[] marketedProductId, int[] representativeMarketedProductId, com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.SeverityLabelFilter severityLabel, boolean includeCaffeine, boolean includeEnteral, boolean includeEthanol, boolean includeFood, boolean includeGrapefruit, boolean includeTobacco) throws java.rmi.RemoteException;
    public com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.ConsumerInteractionReport getConsumerInteractionsV2(java.lang.String authenticationKey, com.Goldstandard.GSDI.DataContract._2011._3._1.PrescribedType prescribed, com.Goldstandard.GSDI.DataContract._2011._3._1.PrescribedType prescribing, com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.SeverityLabelFilter severityLabel, boolean includeCaffeine, boolean includeEnteral, boolean includeEthanol, boolean includeFood, boolean includeGrapefruit, boolean includeTobacco, boolean ignorePrescribed) throws java.rmi.RemoteException;
}
