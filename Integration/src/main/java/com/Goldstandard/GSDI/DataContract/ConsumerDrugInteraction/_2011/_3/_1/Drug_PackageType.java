/**
 * Drug_PackageType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1;

public class Drug_PackageType  implements java.io.Serializable {
    private com.Goldstandard.GSDI.DataContract._2011._3._1.PackageIdentifierTypeEnum packageIdentifierType;

    private java.lang.String identifier;

    private com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.InteractionType[] interaction;

    public Drug_PackageType() {
    }

    public Drug_PackageType(
           com.Goldstandard.GSDI.DataContract._2011._3._1.PackageIdentifierTypeEnum packageIdentifierType,
           java.lang.String identifier,
           com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.InteractionType[] interaction) {
           this.packageIdentifierType = packageIdentifierType;
           this.identifier = identifier;
           this.interaction = interaction;
    }


    /**
     * Gets the packageIdentifierType value for this Drug_PackageType.
     * 
     * @return packageIdentifierType
     */
    public com.Goldstandard.GSDI.DataContract._2011._3._1.PackageIdentifierTypeEnum getPackageIdentifierType() {
        return packageIdentifierType;
    }


    /**
     * Sets the packageIdentifierType value for this Drug_PackageType.
     * 
     * @param packageIdentifierType
     */
    public void setPackageIdentifierType(com.Goldstandard.GSDI.DataContract._2011._3._1.PackageIdentifierTypeEnum packageIdentifierType) {
        this.packageIdentifierType = packageIdentifierType;
    }


    /**
     * Gets the identifier value for this Drug_PackageType.
     * 
     * @return identifier
     */
    public java.lang.String getIdentifier() {
        return identifier;
    }


    /**
     * Sets the identifier value for this Drug_PackageType.
     * 
     * @param identifier
     */
    public void setIdentifier(java.lang.String identifier) {
        this.identifier = identifier;
    }


    /**
     * Gets the interaction value for this Drug_PackageType.
     * 
     * @return interaction
     */
    public com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.InteractionType[] getInteraction() {
        return interaction;
    }


    /**
     * Sets the interaction value for this Drug_PackageType.
     * 
     * @param interaction
     */
    public void setInteraction(com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.InteractionType[] interaction) {
        this.interaction = interaction;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Drug_PackageType)) return false;
        Drug_PackageType other = (Drug_PackageType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.packageIdentifierType==null && other.getPackageIdentifierType()==null) || 
             (this.packageIdentifierType!=null &&
              this.packageIdentifierType.equals(other.getPackageIdentifierType()))) &&
            ((this.identifier==null && other.getIdentifier()==null) || 
             (this.identifier!=null &&
              this.identifier.equals(other.getIdentifier()))) &&
            ((this.interaction==null && other.getInteraction()==null) || 
             (this.interaction!=null &&
              java.util.Arrays.equals(this.interaction, other.getInteraction())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPackageIdentifierType() != null) {
            _hashCode += getPackageIdentifierType().hashCode();
        }
        if (getIdentifier() != null) {
            _hashCode += getIdentifier().hashCode();
        }
        if (getInteraction() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getInteraction());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getInteraction(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Drug_PackageType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "Drug_PackageType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("packageIdentifierType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "PackageIdentifierType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "PackageIdentifierTypeEnum"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("identifier");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "Identifier"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("interaction");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "Interaction"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "InteractionType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "InteractionType"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
