/**
 * ProductDetail.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.Goldstandard.GSDI.DataContract._2011._3._1;

public class ProductDetail  implements java.io.Serializable {
    private java.lang.String orderableConceptName;

    private java.lang.String dosageForm;

    private int representativeMarketedProductId;

    public ProductDetail() {
    }

    public ProductDetail(
           java.lang.String orderableConceptName,
           java.lang.String dosageForm,
           int representativeMarketedProductId) {
           this.orderableConceptName = orderableConceptName;
           this.dosageForm = dosageForm;
           this.representativeMarketedProductId = representativeMarketedProductId;
    }


    /**
     * Gets the orderableConceptName value for this ProductDetail.
     * 
     * @return orderableConceptName
     */
    public java.lang.String getOrderableConceptName() {
        return orderableConceptName;
    }


    /**
     * Sets the orderableConceptName value for this ProductDetail.
     * 
     * @param orderableConceptName
     */
    public void setOrderableConceptName(java.lang.String orderableConceptName) {
        this.orderableConceptName = orderableConceptName;
    }


    /**
     * Gets the dosageForm value for this ProductDetail.
     * 
     * @return dosageForm
     */
    public java.lang.String getDosageForm() {
        return dosageForm;
    }


    /**
     * Sets the dosageForm value for this ProductDetail.
     * 
     * @param dosageForm
     */
    public void setDosageForm(java.lang.String dosageForm) {
        this.dosageForm = dosageForm;
    }


    /**
     * Gets the representativeMarketedProductId value for this ProductDetail.
     * 
     * @return representativeMarketedProductId
     */
    public int getRepresentativeMarketedProductId() {
        return representativeMarketedProductId;
    }


    /**
     * Sets the representativeMarketedProductId value for this ProductDetail.
     * 
     * @param representativeMarketedProductId
     */
    public void setRepresentativeMarketedProductId(int representativeMarketedProductId) {
        this.representativeMarketedProductId = representativeMarketedProductId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ProductDetail)) return false;
        ProductDetail other = (ProductDetail) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.orderableConceptName==null && other.getOrderableConceptName()==null) || 
             (this.orderableConceptName!=null &&
              this.orderableConceptName.equals(other.getOrderableConceptName()))) &&
            ((this.dosageForm==null && other.getDosageForm()==null) || 
             (this.dosageForm!=null &&
              this.dosageForm.equals(other.getDosageForm()))) &&
            this.representativeMarketedProductId == other.getRepresentativeMarketedProductId();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getOrderableConceptName() != null) {
            _hashCode += getOrderableConceptName().hashCode();
        }
        if (getDosageForm() != null) {
            _hashCode += getDosageForm().hashCode();
        }
        _hashCode += getRepresentativeMarketedProductId();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ProductDetail.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "ProductDetail"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orderableConceptName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "OrderableConceptName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dosageForm");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "DosageForm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("representativeMarketedProductId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "RepresentativeMarketedProductId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
