/**
 * SheetTitle.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1;

public class SheetTitle  implements java.io.Serializable {
    private int sheetTitleId;

    private java.lang.String sheetName;

    private com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.UpdateType updateType;

    public SheetTitle() {
    }

    public SheetTitle(
           int sheetTitleId,
           java.lang.String sheetName,
           com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.UpdateType updateType) {
           this.sheetTitleId = sheetTitleId;
           this.sheetName = sheetName;
           this.updateType = updateType;
    }


    /**
     * Gets the sheetTitleId value for this SheetTitle.
     * 
     * @return sheetTitleId
     */
    public int getSheetTitleId() {
        return sheetTitleId;
    }


    /**
     * Sets the sheetTitleId value for this SheetTitle.
     * 
     * @param sheetTitleId
     */
    public void setSheetTitleId(int sheetTitleId) {
        this.sheetTitleId = sheetTitleId;
    }


    /**
     * Gets the sheetName value for this SheetTitle.
     * 
     * @return sheetName
     */
    public java.lang.String getSheetName() {
        return sheetName;
    }


    /**
     * Sets the sheetName value for this SheetTitle.
     * 
     * @param sheetName
     */
    public void setSheetName(java.lang.String sheetName) {
        this.sheetName = sheetName;
    }


    /**
     * Gets the updateType value for this SheetTitle.
     * 
     * @return updateType
     */
    public com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.UpdateType getUpdateType() {
        return updateType;
    }


    /**
     * Sets the updateType value for this SheetTitle.
     * 
     * @param updateType
     */
    public void setUpdateType(com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.UpdateType updateType) {
        this.updateType = updateType;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SheetTitle)) return false;
        SheetTitle other = (SheetTitle) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.sheetTitleId == other.getSheetTitleId() &&
            ((this.sheetName==null && other.getSheetName()==null) || 
             (this.sheetName!=null &&
              this.sheetName.equals(other.getSheetName()))) &&
            ((this.updateType==null && other.getUpdateType()==null) || 
             (this.updateType!=null &&
              this.updateType.equals(other.getUpdateType())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getSheetTitleId();
        if (getSheetName() != null) {
            _hashCode += getSheetName().hashCode();
        }
        if (getUpdateType() != null) {
            _hashCode += getUpdateType().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SheetTitle.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "SheetTitle"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sheetTitleId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "SheetTitleId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sheetName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "SheetName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("updateType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "UpdateType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "UpdateType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
