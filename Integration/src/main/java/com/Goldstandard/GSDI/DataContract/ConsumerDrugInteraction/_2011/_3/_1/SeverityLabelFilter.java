/**
 * SeverityLabelFilter.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1;

public class SeverityLabelFilter implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected SeverityLabelFilter(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _Severe = "Severe";
    public static final java.lang.String _Major = "Major";
    public static final java.lang.String _Moderate = "Moderate";
    public static final java.lang.String _Minor = "Minor";
    public static final java.lang.String _All = "All";
    public static final SeverityLabelFilter Severe = new SeverityLabelFilter(_Severe);
    public static final SeverityLabelFilter Major = new SeverityLabelFilter(_Major);
    public static final SeverityLabelFilter Moderate = new SeverityLabelFilter(_Moderate);
    public static final SeverityLabelFilter Minor = new SeverityLabelFilter(_Minor);
    public static final SeverityLabelFilter All = new SeverityLabelFilter(_All);
    public java.lang.String getValue() { return _value_;}
    public static SeverityLabelFilter fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        SeverityLabelFilter enumeration = (SeverityLabelFilter)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static SeverityLabelFilter fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SeverityLabelFilter.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "SeverityLabelFilter"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
