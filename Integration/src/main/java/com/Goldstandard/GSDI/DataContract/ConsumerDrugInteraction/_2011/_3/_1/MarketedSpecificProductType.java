/**
 * MarketedSpecificProductType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1;

public class MarketedSpecificProductType  implements java.io.Serializable {
    private int identifier;

    private java.lang.String name;

    private com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.LifestyleInteractionType[] lifestyle;

    private com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.DrugType drug;

    public MarketedSpecificProductType() {
    }

    public MarketedSpecificProductType(
           int identifier,
           java.lang.String name,
           com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.LifestyleInteractionType[] lifestyle,
           com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.DrugType drug) {
           this.identifier = identifier;
           this.name = name;
           this.lifestyle = lifestyle;
           this.drug = drug;
    }


    /**
     * Gets the identifier value for this MarketedSpecificProductType.
     * 
     * @return identifier
     */
    public int getIdentifier() {
        return identifier;
    }


    /**
     * Sets the identifier value for this MarketedSpecificProductType.
     * 
     * @param identifier
     */
    public void setIdentifier(int identifier) {
        this.identifier = identifier;
    }


    /**
     * Gets the name value for this MarketedSpecificProductType.
     * 
     * @return name
     */
    public java.lang.String getName() {
        return name;
    }


    /**
     * Sets the name value for this MarketedSpecificProductType.
     * 
     * @param name
     */
    public void setName(java.lang.String name) {
        this.name = name;
    }


    /**
     * Gets the lifestyle value for this MarketedSpecificProductType.
     * 
     * @return lifestyle
     */
    public com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.LifestyleInteractionType[] getLifestyle() {
        return lifestyle;
    }


    /**
     * Sets the lifestyle value for this MarketedSpecificProductType.
     * 
     * @param lifestyle
     */
    public void setLifestyle(com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.LifestyleInteractionType[] lifestyle) {
        this.lifestyle = lifestyle;
    }


    /**
     * Gets the drug value for this MarketedSpecificProductType.
     * 
     * @return drug
     */
    public com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.DrugType getDrug() {
        return drug;
    }


    /**
     * Sets the drug value for this MarketedSpecificProductType.
     * 
     * @param drug
     */
    public void setDrug(com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.DrugType drug) {
        this.drug = drug;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof MarketedSpecificProductType)) return false;
        MarketedSpecificProductType other = (MarketedSpecificProductType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.identifier == other.getIdentifier() &&
            ((this.name==null && other.getName()==null) || 
             (this.name!=null &&
              this.name.equals(other.getName()))) &&
            ((this.lifestyle==null && other.getLifestyle()==null) || 
             (this.lifestyle!=null &&
              java.util.Arrays.equals(this.lifestyle, other.getLifestyle()))) &&
            ((this.drug==null && other.getDrug()==null) || 
             (this.drug!=null &&
              this.drug.equals(other.getDrug())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getIdentifier();
        if (getName() != null) {
            _hashCode += getName().hashCode();
        }
        if (getLifestyle() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getLifestyle());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getLifestyle(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getDrug() != null) {
            _hashCode += getDrug().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(MarketedSpecificProductType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "MarketedSpecificProductType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("identifier");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "Identifier"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("name");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "Name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lifestyle");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "Lifestyle"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "LifestyleInteractionType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "LifestyleInteractionType"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("drug");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "Drug"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "DrugType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
