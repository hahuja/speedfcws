/**
 * DrugNameSearchResult.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.Goldstandard.GSDI.DataContract._2011._3._1;

public class DrugNameSearchResult  implements java.io.Serializable {
    private java.lang.String searchInputTerm;

    private com.Goldstandard.GSDI.DataContract._2011._3._1.TrademarkGenericEnum searchInputTrademarkGeneric;

    private com.Goldstandard.GSDI.DataContract._2011._3._1.OrderableNameType[] drugNames;

    public DrugNameSearchResult() {
    }

    public DrugNameSearchResult(
           java.lang.String searchInputTerm,
           com.Goldstandard.GSDI.DataContract._2011._3._1.TrademarkGenericEnum searchInputTrademarkGeneric,
           com.Goldstandard.GSDI.DataContract._2011._3._1.OrderableNameType[] drugNames) {
           this.searchInputTerm = searchInputTerm;
           this.searchInputTrademarkGeneric = searchInputTrademarkGeneric;
           this.drugNames = drugNames;
    }


    /**
     * Gets the searchInputTerm value for this DrugNameSearchResult.
     * 
     * @return searchInputTerm
     */
    public java.lang.String getSearchInputTerm() {
        return searchInputTerm;
    }


    /**
     * Sets the searchInputTerm value for this DrugNameSearchResult.
     * 
     * @param searchInputTerm
     */
    public void setSearchInputTerm(java.lang.String searchInputTerm) {
        this.searchInputTerm = searchInputTerm;
    }


    /**
     * Gets the searchInputTrademarkGeneric value for this DrugNameSearchResult.
     * 
     * @return searchInputTrademarkGeneric
     */
    public com.Goldstandard.GSDI.DataContract._2011._3._1.TrademarkGenericEnum getSearchInputTrademarkGeneric() {
        return searchInputTrademarkGeneric;
    }


    /**
     * Sets the searchInputTrademarkGeneric value for this DrugNameSearchResult.
     * 
     * @param searchInputTrademarkGeneric
     */
    public void setSearchInputTrademarkGeneric(com.Goldstandard.GSDI.DataContract._2011._3._1.TrademarkGenericEnum searchInputTrademarkGeneric) {
        this.searchInputTrademarkGeneric = searchInputTrademarkGeneric;
    }


    /**
     * Gets the drugNames value for this DrugNameSearchResult.
     * 
     * @return drugNames
     */
    public com.Goldstandard.GSDI.DataContract._2011._3._1.OrderableNameType[] getDrugNames() {
        return drugNames;
    }


    /**
     * Sets the drugNames value for this DrugNameSearchResult.
     * 
     * @param drugNames
     */
    public void setDrugNames(com.Goldstandard.GSDI.DataContract._2011._3._1.OrderableNameType[] drugNames) {
        this.drugNames = drugNames;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DrugNameSearchResult)) return false;
        DrugNameSearchResult other = (DrugNameSearchResult) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.searchInputTerm==null && other.getSearchInputTerm()==null) || 
             (this.searchInputTerm!=null &&
              this.searchInputTerm.equals(other.getSearchInputTerm()))) &&
            ((this.searchInputTrademarkGeneric==null && other.getSearchInputTrademarkGeneric()==null) || 
             (this.searchInputTrademarkGeneric!=null &&
              this.searchInputTrademarkGeneric.equals(other.getSearchInputTrademarkGeneric()))) &&
            ((this.drugNames==null && other.getDrugNames()==null) || 
             (this.drugNames!=null &&
              java.util.Arrays.equals(this.drugNames, other.getDrugNames())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSearchInputTerm() != null) {
            _hashCode += getSearchInputTerm().hashCode();
        }
        if (getSearchInputTrademarkGeneric() != null) {
            _hashCode += getSearchInputTrademarkGeneric().hashCode();
        }
        if (getDrugNames() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDrugNames());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDrugNames(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DrugNameSearchResult.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "DrugNameSearchResult"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("searchInputTerm");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "SearchInputTerm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("searchInputTrademarkGeneric");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "SearchInputTrademarkGeneric"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "TrademarkGenericEnum"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("drugNames");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "DrugNames"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "OrderableNameType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "OrderableNameType"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
