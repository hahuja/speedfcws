/**
 * IGSDIBasePatientEducation.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1;

public interface IGSDIBasePatientEducation extends java.rmi.Remote {
    public com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.PatientEducationType[] getSheetContent(java.lang.String authenticationKey, int languageId, com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.ProductSearchType productSearchType, java.lang.String searchTerm, com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.SectionName sectionEnum) throws java.rmi.RemoteException;
    public com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.PatientEducationLanguageEntitlement[] listLanguageEntitlements(java.lang.String authenticationKey) throws java.rmi.RemoteException;
    public com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.SheetTitle[] listUpdatedPES(java.lang.String authenticationKey, com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.Language language, java.util.Calendar beginDate, java.util.Calendar endDate) throws java.rmi.RemoteException;
    public com.Goldstandard.GSDI.DataContract._2011._3._1.DrugNameSearchResult searchForDrugNames(java.lang.String authenticationKey, java.lang.String searchTerm, com.Goldstandard.GSDI.DataContract._2011._3._1.TrademarkGenericEnum trademarkOrGeneric) throws java.rmi.RemoteException;
    public com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.ProductSearchResult searchForRepresentativeMarketedProductIdPES(java.lang.String authenticationKey, java.lang.String searchTerm) throws java.rmi.RemoteException;
    public com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.ProductSpecificSearchResult searchForSpecificDrugPES(java.lang.String authenticationKey, com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.ProductType productSearchType, java.lang.String searchTerm) throws java.rmi.RemoteException;
}
