/**
 * GSDIBaseDrugIdentifierLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1;

public class GSDIBaseDrugIdentifierLocator extends org.apache.axis.client.Service implements com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.GSDIBaseDrugIdentifier {

    public GSDIBaseDrugIdentifierLocator() {
    }


    public GSDIBaseDrugIdentifierLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public GSDIBaseDrugIdentifierLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for IGSDIBaseDrugIdentifier
    private java.lang.String IGSDIBaseDrugIdentifier_address = "http://gsdi.goldstandard.com/GSDIDrugIdentifierWS.asmx";

    public java.lang.String getIGSDIBaseDrugIdentifierAddress() {
        return IGSDIBaseDrugIdentifier_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String IGSDIBaseDrugIdentifierWSDDServiceName = "IGSDIBaseDrugIdentifier";

    public java.lang.String getIGSDIBaseDrugIdentifierWSDDServiceName() {
        return IGSDIBaseDrugIdentifierWSDDServiceName;
    }

    public void setIGSDIBaseDrugIdentifierWSDDServiceName(java.lang.String name) {
        IGSDIBaseDrugIdentifierWSDDServiceName = name;
    }

    public com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.IGSDIBaseDrugIdentifier getIGSDIBaseDrugIdentifier() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(IGSDIBaseDrugIdentifier_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getIGSDIBaseDrugIdentifier(endpoint);
    }

    public com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.IGSDIBaseDrugIdentifier getIGSDIBaseDrugIdentifier(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.IGSDIBaseDrugIdentifierStub _stub = new com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.IGSDIBaseDrugIdentifierStub(portAddress, this);
            _stub.setPortName(getIGSDIBaseDrugIdentifierWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setIGSDIBaseDrugIdentifierEndpointAddress(java.lang.String address) {
        IGSDIBaseDrugIdentifier_address = address;
    }


    // Use to get a proxy class for IGSDIBaseDrugIdentifier1
    private java.lang.String IGSDIBaseDrugIdentifier1_address = "http://gsdi.goldstandard.com/GSDIDrugIdentifierWS.asmx";

    public java.lang.String getIGSDIBaseDrugIdentifier1Address() {
        return IGSDIBaseDrugIdentifier1_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String IGSDIBaseDrugIdentifier1WSDDServiceName = "IGSDIBaseDrugIdentifier1";

    public java.lang.String getIGSDIBaseDrugIdentifier1WSDDServiceName() {
        return IGSDIBaseDrugIdentifier1WSDDServiceName;
    }

    public void setIGSDIBaseDrugIdentifier1WSDDServiceName(java.lang.String name) {
        IGSDIBaseDrugIdentifier1WSDDServiceName = name;
    }

    public com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.IGSDIBaseDrugIdentifier getIGSDIBaseDrugIdentifier1() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(IGSDIBaseDrugIdentifier1_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getIGSDIBaseDrugIdentifier1(endpoint);
    }

    public com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.IGSDIBaseDrugIdentifier getIGSDIBaseDrugIdentifier1(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.IGSDIBaseDrugIdentifier1Stub _stub = new com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.IGSDIBaseDrugIdentifier1Stub(portAddress, this);
            _stub.setPortName(getIGSDIBaseDrugIdentifier1WSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setIGSDIBaseDrugIdentifier1EndpointAddress(java.lang.String address) {
        IGSDIBaseDrugIdentifier1_address = address;
    }


    // Use to get a proxy class for GSDIBaseDrugIdentifierSoap12
    private java.lang.String GSDIBaseDrugIdentifierSoap12_address = "http://gsdi.goldstandard.com:8009/GSDIDrugIdentifierWS.asmx";

    public java.lang.String getGSDIBaseDrugIdentifierSoap12Address() {
        return GSDIBaseDrugIdentifierSoap12_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String GSDIBaseDrugIdentifierSoap12WSDDServiceName = "GSDIBaseDrugIdentifierSoap12";

    public java.lang.String getGSDIBaseDrugIdentifierSoap12WSDDServiceName() {
        return GSDIBaseDrugIdentifierSoap12WSDDServiceName;
    }

    public void setGSDIBaseDrugIdentifierSoap12WSDDServiceName(java.lang.String name) {
        GSDIBaseDrugIdentifierSoap12WSDDServiceName = name;
    }

    public com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.GSDIBaseDrugIdentifierSoap12 getGSDIBaseDrugIdentifierSoap12() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(GSDIBaseDrugIdentifierSoap12_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getGSDIBaseDrugIdentifierSoap12(endpoint);
    }

    public com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.GSDIBaseDrugIdentifierSoap12 getGSDIBaseDrugIdentifierSoap12(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.GSDIBaseDrugIdentifierSoap12Stub _stub = new com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.GSDIBaseDrugIdentifierSoap12Stub(portAddress, this);
            _stub.setPortName(getGSDIBaseDrugIdentifierSoap12WSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setGSDIBaseDrugIdentifierSoap12EndpointAddress(java.lang.String address) {
        GSDIBaseDrugIdentifierSoap12_address = address;
    }


    // Use to get a proxy class for GSDIBaseDrugIdentifierSoap
    private java.lang.String GSDIBaseDrugIdentifierSoap_address = "http://gsdi.goldstandard.com/GSDIDrugIdentifierWS.asmx";

    public java.lang.String getGSDIBaseDrugIdentifierSoapAddress() {
        return GSDIBaseDrugIdentifierSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String GSDIBaseDrugIdentifierSoapWSDDServiceName = "GSDIBaseDrugIdentifierSoap";

    public java.lang.String getGSDIBaseDrugIdentifierSoapWSDDServiceName() {
        return GSDIBaseDrugIdentifierSoapWSDDServiceName;
    }

    public void setGSDIBaseDrugIdentifierSoapWSDDServiceName(java.lang.String name) {
        GSDIBaseDrugIdentifierSoapWSDDServiceName = name;
    }

    public com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.GSDIBaseDrugIdentifierSoap getGSDIBaseDrugIdentifierSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(GSDIBaseDrugIdentifierSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getGSDIBaseDrugIdentifierSoap(endpoint);
    }

    public com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.GSDIBaseDrugIdentifierSoap getGSDIBaseDrugIdentifierSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.GSDIBaseDrugIdentifierSoapStub _stub = new com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.GSDIBaseDrugIdentifierSoapStub(portAddress, this);
            _stub.setPortName(getGSDIBaseDrugIdentifierSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setGSDIBaseDrugIdentifierSoapEndpointAddress(java.lang.String address) {
        GSDIBaseDrugIdentifierSoap_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     * This service has multiple ports for a given interface;
     * the proxy implementation returned may be indeterminate.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.IGSDIBaseDrugIdentifier.class.isAssignableFrom(serviceEndpointInterface)) {
                com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.IGSDIBaseDrugIdentifierStub _stub = new com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.IGSDIBaseDrugIdentifierStub(new java.net.URL(IGSDIBaseDrugIdentifier_address), this);
                _stub.setPortName(getIGSDIBaseDrugIdentifierWSDDServiceName());
                return _stub;
            }
            if (com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.IGSDIBaseDrugIdentifier.class.isAssignableFrom(serviceEndpointInterface)) {
                com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.IGSDIBaseDrugIdentifier1Stub _stub = new com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.IGSDIBaseDrugIdentifier1Stub(new java.net.URL(IGSDIBaseDrugIdentifier1_address), this);
                _stub.setPortName(getIGSDIBaseDrugIdentifier1WSDDServiceName());
                return _stub;
            }
            if (com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.GSDIBaseDrugIdentifierSoap12.class.isAssignableFrom(serviceEndpointInterface)) {
                com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.GSDIBaseDrugIdentifierSoap12Stub _stub = new com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.GSDIBaseDrugIdentifierSoap12Stub(new java.net.URL(GSDIBaseDrugIdentifierSoap12_address), this);
                _stub.setPortName(getGSDIBaseDrugIdentifierSoap12WSDDServiceName());
                return _stub;
            }
            if (com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.GSDIBaseDrugIdentifierSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.GSDIBaseDrugIdentifierSoapStub _stub = new com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.GSDIBaseDrugIdentifierSoapStub(new java.net.URL(GSDIBaseDrugIdentifierSoap_address), this);
                _stub.setPortName(getGSDIBaseDrugIdentifierSoapWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("IGSDIBaseDrugIdentifier".equals(inputPortName)) {
            return getIGSDIBaseDrugIdentifier();
        }
        else if ("IGSDIBaseDrugIdentifier1".equals(inputPortName)) {
            return getIGSDIBaseDrugIdentifier1();
        }
        else if ("GSDIBaseDrugIdentifierSoap12".equals(inputPortName)) {
            return getGSDIBaseDrugIdentifierSoap12();
        }
        else if ("GSDIBaseDrugIdentifierSoap".equals(inputPortName)) {
            return getGSDIBaseDrugIdentifierSoap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "GSDIBaseDrugIdentifier");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "IGSDIBaseDrugIdentifier"));
            ports.add(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "IGSDIBaseDrugIdentifier1"));
            ports.add(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "GSDIBaseDrugIdentifierSoap12"));
            ports.add(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "GSDIBaseDrugIdentifierSoap"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("IGSDIBaseDrugIdentifier".equals(portName)) {
            setIGSDIBaseDrugIdentifierEndpointAddress(address);
        }
        else 
if ("IGSDIBaseDrugIdentifier1".equals(portName)) {
            setIGSDIBaseDrugIdentifier1EndpointAddress(address);
        }
        else 
if ("GSDIBaseDrugIdentifierSoap12".equals(portName)) {
            setGSDIBaseDrugIdentifierSoap12EndpointAddress(address);
        }
        else 
if ("GSDIBaseDrugIdentifierSoap".equals(portName)) {
            setGSDIBaseDrugIdentifierSoapEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
