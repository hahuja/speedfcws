/**
 * PatientEducationType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1;

public class PatientEducationType  implements java.io.Serializable {
    private int sheetTitleId;

    private java.lang.String sheetName;

    private java.lang.String[] tradeNames;

    private java.lang.String descriptionHeader;

    private java.lang.String description;

    private java.lang.String descriptionFooter;

    private java.lang.String contraindicationsHeader;

    private java.lang.String contraindications;

    private java.lang.String contraindicationsFooter;

    private java.lang.String administrationHeader;

    private java.lang.String administration;

    private java.lang.String administrationFooter;

    private java.lang.String missedDoseHeader;

    private java.lang.String missedDose;

    private java.lang.String missedDoseFooter;

    private java.lang.String interactionsHeader;

    private java.lang.String interactions;

    private java.lang.String interactionsFooter;

    private java.lang.String monitoringHeader;

    private java.lang.String monitoring;

    private java.lang.String monitoringFooter;

    private java.lang.String sideEffectsHeader;

    private java.lang.String sideEffects;

    private java.lang.String sideEffectsFooter;

    private java.lang.String storageHeader;

    private java.lang.String storage;

    private java.lang.String storageFooter;

    private java.lang.String lastUpdated;

    private java.lang.String copyright;

    public PatientEducationType() {
    }

    public PatientEducationType(
           int sheetTitleId,
           java.lang.String sheetName,
           java.lang.String[] tradeNames,
           java.lang.String descriptionHeader,
           java.lang.String description,
           java.lang.String descriptionFooter,
           java.lang.String contraindicationsHeader,
           java.lang.String contraindications,
           java.lang.String contraindicationsFooter,
           java.lang.String administrationHeader,
           java.lang.String administration,
           java.lang.String administrationFooter,
           java.lang.String missedDoseHeader,
           java.lang.String missedDose,
           java.lang.String missedDoseFooter,
           java.lang.String interactionsHeader,
           java.lang.String interactions,
           java.lang.String interactionsFooter,
           java.lang.String monitoringHeader,
           java.lang.String monitoring,
           java.lang.String monitoringFooter,
           java.lang.String sideEffectsHeader,
           java.lang.String sideEffects,
           java.lang.String sideEffectsFooter,
           java.lang.String storageHeader,
           java.lang.String storage,
           java.lang.String storageFooter,
           java.lang.String lastUpdated,
           java.lang.String copyright) {
           this.sheetTitleId = sheetTitleId;
           this.sheetName = sheetName;
           this.tradeNames = tradeNames;
           this.descriptionHeader = descriptionHeader;
           this.description = description;
           this.descriptionFooter = descriptionFooter;
           this.contraindicationsHeader = contraindicationsHeader;
           this.contraindications = contraindications;
           this.contraindicationsFooter = contraindicationsFooter;
           this.administrationHeader = administrationHeader;
           this.administration = administration;
           this.administrationFooter = administrationFooter;
           this.missedDoseHeader = missedDoseHeader;
           this.missedDose = missedDose;
           this.missedDoseFooter = missedDoseFooter;
           this.interactionsHeader = interactionsHeader;
           this.interactions = interactions;
           this.interactionsFooter = interactionsFooter;
           this.monitoringHeader = monitoringHeader;
           this.monitoring = monitoring;
           this.monitoringFooter = monitoringFooter;
           this.sideEffectsHeader = sideEffectsHeader;
           this.sideEffects = sideEffects;
           this.sideEffectsFooter = sideEffectsFooter;
           this.storageHeader = storageHeader;
           this.storage = storage;
           this.storageFooter = storageFooter;
           this.lastUpdated = lastUpdated;
           this.copyright = copyright;
    }


    /**
     * Gets the sheetTitleId value for this PatientEducationType.
     * 
     * @return sheetTitleId
     */
    public int getSheetTitleId() {
        return sheetTitleId;
    }


    /**
     * Sets the sheetTitleId value for this PatientEducationType.
     * 
     * @param sheetTitleId
     */
    public void setSheetTitleId(int sheetTitleId) {
        this.sheetTitleId = sheetTitleId;
    }


    /**
     * Gets the sheetName value for this PatientEducationType.
     * 
     * @return sheetName
     */
    public java.lang.String getSheetName() {
        return sheetName;
    }


    /**
     * Sets the sheetName value for this PatientEducationType.
     * 
     * @param sheetName
     */
    public void setSheetName(java.lang.String sheetName) {
        this.sheetName = sheetName;
    }


    /**
     * Gets the tradeNames value for this PatientEducationType.
     * 
     * @return tradeNames
     */
    public java.lang.String[] getTradeNames() {
        return tradeNames;
    }


    /**
     * Sets the tradeNames value for this PatientEducationType.
     * 
     * @param tradeNames
     */
    public void setTradeNames(java.lang.String[] tradeNames) {
        this.tradeNames = tradeNames;
    }


    /**
     * Gets the descriptionHeader value for this PatientEducationType.
     * 
     * @return descriptionHeader
     */
    public java.lang.String getDescriptionHeader() {
        return descriptionHeader;
    }


    /**
     * Sets the descriptionHeader value for this PatientEducationType.
     * 
     * @param descriptionHeader
     */
    public void setDescriptionHeader(java.lang.String descriptionHeader) {
        this.descriptionHeader = descriptionHeader;
    }


    /**
     * Gets the description value for this PatientEducationType.
     * 
     * @return description
     */
    public java.lang.String getDescription() {
        return description;
    }


    /**
     * Sets the description value for this PatientEducationType.
     * 
     * @param description
     */
    public void setDescription(java.lang.String description) {
        this.description = description;
    }


    /**
     * Gets the descriptionFooter value for this PatientEducationType.
     * 
     * @return descriptionFooter
     */
    public java.lang.String getDescriptionFooter() {
        return descriptionFooter;
    }


    /**
     * Sets the descriptionFooter value for this PatientEducationType.
     * 
     * @param descriptionFooter
     */
    public void setDescriptionFooter(java.lang.String descriptionFooter) {
        this.descriptionFooter = descriptionFooter;
    }


    /**
     * Gets the contraindicationsHeader value for this PatientEducationType.
     * 
     * @return contraindicationsHeader
     */
    public java.lang.String getContraindicationsHeader() {
        return contraindicationsHeader;
    }


    /**
     * Sets the contraindicationsHeader value for this PatientEducationType.
     * 
     * @param contraindicationsHeader
     */
    public void setContraindicationsHeader(java.lang.String contraindicationsHeader) {
        this.contraindicationsHeader = contraindicationsHeader;
    }


    /**
     * Gets the contraindications value for this PatientEducationType.
     * 
     * @return contraindications
     */
    public java.lang.String getContraindications() {
        return contraindications;
    }


    /**
     * Sets the contraindications value for this PatientEducationType.
     * 
     * @param contraindications
     */
    public void setContraindications(java.lang.String contraindications) {
        this.contraindications = contraindications;
    }


    /**
     * Gets the contraindicationsFooter value for this PatientEducationType.
     * 
     * @return contraindicationsFooter
     */
    public java.lang.String getContraindicationsFooter() {
        return contraindicationsFooter;
    }


    /**
     * Sets the contraindicationsFooter value for this PatientEducationType.
     * 
     * @param contraindicationsFooter
     */
    public void setContraindicationsFooter(java.lang.String contraindicationsFooter) {
        this.contraindicationsFooter = contraindicationsFooter;
    }


    /**
     * Gets the administrationHeader value for this PatientEducationType.
     * 
     * @return administrationHeader
     */
    public java.lang.String getAdministrationHeader() {
        return administrationHeader;
    }


    /**
     * Sets the administrationHeader value for this PatientEducationType.
     * 
     * @param administrationHeader
     */
    public void setAdministrationHeader(java.lang.String administrationHeader) {
        this.administrationHeader = administrationHeader;
    }


    /**
     * Gets the administration value for this PatientEducationType.
     * 
     * @return administration
     */
    public java.lang.String getAdministration() {
        return administration;
    }


    /**
     * Sets the administration value for this PatientEducationType.
     * 
     * @param administration
     */
    public void setAdministration(java.lang.String administration) {
        this.administration = administration;
    }


    /**
     * Gets the administrationFooter value for this PatientEducationType.
     * 
     * @return administrationFooter
     */
    public java.lang.String getAdministrationFooter() {
        return administrationFooter;
    }


    /**
     * Sets the administrationFooter value for this PatientEducationType.
     * 
     * @param administrationFooter
     */
    public void setAdministrationFooter(java.lang.String administrationFooter) {
        this.administrationFooter = administrationFooter;
    }


    /**
     * Gets the missedDoseHeader value for this PatientEducationType.
     * 
     * @return missedDoseHeader
     */
    public java.lang.String getMissedDoseHeader() {
        return missedDoseHeader;
    }


    /**
     * Sets the missedDoseHeader value for this PatientEducationType.
     * 
     * @param missedDoseHeader
     */
    public void setMissedDoseHeader(java.lang.String missedDoseHeader) {
        this.missedDoseHeader = missedDoseHeader;
    }


    /**
     * Gets the missedDose value for this PatientEducationType.
     * 
     * @return missedDose
     */
    public java.lang.String getMissedDose() {
        return missedDose;
    }


    /**
     * Sets the missedDose value for this PatientEducationType.
     * 
     * @param missedDose
     */
    public void setMissedDose(java.lang.String missedDose) {
        this.missedDose = missedDose;
    }


    /**
     * Gets the missedDoseFooter value for this PatientEducationType.
     * 
     * @return missedDoseFooter
     */
    public java.lang.String getMissedDoseFooter() {
        return missedDoseFooter;
    }


    /**
     * Sets the missedDoseFooter value for this PatientEducationType.
     * 
     * @param missedDoseFooter
     */
    public void setMissedDoseFooter(java.lang.String missedDoseFooter) {
        this.missedDoseFooter = missedDoseFooter;
    }


    /**
     * Gets the interactionsHeader value for this PatientEducationType.
     * 
     * @return interactionsHeader
     */
    public java.lang.String getInteractionsHeader() {
        return interactionsHeader;
    }


    /**
     * Sets the interactionsHeader value for this PatientEducationType.
     * 
     * @param interactionsHeader
     */
    public void setInteractionsHeader(java.lang.String interactionsHeader) {
        this.interactionsHeader = interactionsHeader;
    }


    /**
     * Gets the interactions value for this PatientEducationType.
     * 
     * @return interactions
     */
    public java.lang.String getInteractions() {
        return interactions;
    }


    /**
     * Sets the interactions value for this PatientEducationType.
     * 
     * @param interactions
     */
    public void setInteractions(java.lang.String interactions) {
        this.interactions = interactions;
    }


    /**
     * Gets the interactionsFooter value for this PatientEducationType.
     * 
     * @return interactionsFooter
     */
    public java.lang.String getInteractionsFooter() {
        return interactionsFooter;
    }


    /**
     * Sets the interactionsFooter value for this PatientEducationType.
     * 
     * @param interactionsFooter
     */
    public void setInteractionsFooter(java.lang.String interactionsFooter) {
        this.interactionsFooter = interactionsFooter;
    }


    /**
     * Gets the monitoringHeader value for this PatientEducationType.
     * 
     * @return monitoringHeader
     */
    public java.lang.String getMonitoringHeader() {
        return monitoringHeader;
    }


    /**
     * Sets the monitoringHeader value for this PatientEducationType.
     * 
     * @param monitoringHeader
     */
    public void setMonitoringHeader(java.lang.String monitoringHeader) {
        this.monitoringHeader = monitoringHeader;
    }


    /**
     * Gets the monitoring value for this PatientEducationType.
     * 
     * @return monitoring
     */
    public java.lang.String getMonitoring() {
        return monitoring;
    }


    /**
     * Sets the monitoring value for this PatientEducationType.
     * 
     * @param monitoring
     */
    public void setMonitoring(java.lang.String monitoring) {
        this.monitoring = monitoring;
    }


    /**
     * Gets the monitoringFooter value for this PatientEducationType.
     * 
     * @return monitoringFooter
     */
    public java.lang.String getMonitoringFooter() {
        return monitoringFooter;
    }


    /**
     * Sets the monitoringFooter value for this PatientEducationType.
     * 
     * @param monitoringFooter
     */
    public void setMonitoringFooter(java.lang.String monitoringFooter) {
        this.monitoringFooter = monitoringFooter;
    }


    /**
     * Gets the sideEffectsHeader value for this PatientEducationType.
     * 
     * @return sideEffectsHeader
     */
    public java.lang.String getSideEffectsHeader() {
        return sideEffectsHeader;
    }


    /**
     * Sets the sideEffectsHeader value for this PatientEducationType.
     * 
     * @param sideEffectsHeader
     */
    public void setSideEffectsHeader(java.lang.String sideEffectsHeader) {
        this.sideEffectsHeader = sideEffectsHeader;
    }


    /**
     * Gets the sideEffects value for this PatientEducationType.
     * 
     * @return sideEffects
     */
    public java.lang.String getSideEffects() {
        return sideEffects;
    }


    /**
     * Sets the sideEffects value for this PatientEducationType.
     * 
     * @param sideEffects
     */
    public void setSideEffects(java.lang.String sideEffects) {
        this.sideEffects = sideEffects;
    }


    /**
     * Gets the sideEffectsFooter value for this PatientEducationType.
     * 
     * @return sideEffectsFooter
     */
    public java.lang.String getSideEffectsFooter() {
        return sideEffectsFooter;
    }


    /**
     * Sets the sideEffectsFooter value for this PatientEducationType.
     * 
     * @param sideEffectsFooter
     */
    public void setSideEffectsFooter(java.lang.String sideEffectsFooter) {
        this.sideEffectsFooter = sideEffectsFooter;
    }


    /**
     * Gets the storageHeader value for this PatientEducationType.
     * 
     * @return storageHeader
     */
    public java.lang.String getStorageHeader() {
        return storageHeader;
    }


    /**
     * Sets the storageHeader value for this PatientEducationType.
     * 
     * @param storageHeader
     */
    public void setStorageHeader(java.lang.String storageHeader) {
        this.storageHeader = storageHeader;
    }


    /**
     * Gets the storage value for this PatientEducationType.
     * 
     * @return storage
     */
    public java.lang.String getStorage() {
        return storage;
    }


    /**
     * Sets the storage value for this PatientEducationType.
     * 
     * @param storage
     */
    public void setStorage(java.lang.String storage) {
        this.storage = storage;
    }


    /**
     * Gets the storageFooter value for this PatientEducationType.
     * 
     * @return storageFooter
     */
    public java.lang.String getStorageFooter() {
        return storageFooter;
    }


    /**
     * Sets the storageFooter value for this PatientEducationType.
     * 
     * @param storageFooter
     */
    public void setStorageFooter(java.lang.String storageFooter) {
        this.storageFooter = storageFooter;
    }


    /**
     * Gets the lastUpdated value for this PatientEducationType.
     * 
     * @return lastUpdated
     */
    public java.lang.String getLastUpdated() {
        return lastUpdated;
    }


    /**
     * Sets the lastUpdated value for this PatientEducationType.
     * 
     * @param lastUpdated
     */
    public void setLastUpdated(java.lang.String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }


    /**
     * Gets the copyright value for this PatientEducationType.
     * 
     * @return copyright
     */
    public java.lang.String getCopyright() {
        return copyright;
    }


    /**
     * Sets the copyright value for this PatientEducationType.
     * 
     * @param copyright
     */
    public void setCopyright(java.lang.String copyright) {
        this.copyright = copyright;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PatientEducationType)) return false;
        PatientEducationType other = (PatientEducationType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.sheetTitleId == other.getSheetTitleId() &&
            ((this.sheetName==null && other.getSheetName()==null) || 
             (this.sheetName!=null &&
              this.sheetName.equals(other.getSheetName()))) &&
            ((this.tradeNames==null && other.getTradeNames()==null) || 
             (this.tradeNames!=null &&
              java.util.Arrays.equals(this.tradeNames, other.getTradeNames()))) &&
            ((this.descriptionHeader==null && other.getDescriptionHeader()==null) || 
             (this.descriptionHeader!=null &&
              this.descriptionHeader.equals(other.getDescriptionHeader()))) &&
            ((this.description==null && other.getDescription()==null) || 
             (this.description!=null &&
              this.description.equals(other.getDescription()))) &&
            ((this.descriptionFooter==null && other.getDescriptionFooter()==null) || 
             (this.descriptionFooter!=null &&
              this.descriptionFooter.equals(other.getDescriptionFooter()))) &&
            ((this.contraindicationsHeader==null && other.getContraindicationsHeader()==null) || 
             (this.contraindicationsHeader!=null &&
              this.contraindicationsHeader.equals(other.getContraindicationsHeader()))) &&
            ((this.contraindications==null && other.getContraindications()==null) || 
             (this.contraindications!=null &&
              this.contraindications.equals(other.getContraindications()))) &&
            ((this.contraindicationsFooter==null && other.getContraindicationsFooter()==null) || 
             (this.contraindicationsFooter!=null &&
              this.contraindicationsFooter.equals(other.getContraindicationsFooter()))) &&
            ((this.administrationHeader==null && other.getAdministrationHeader()==null) || 
             (this.administrationHeader!=null &&
              this.administrationHeader.equals(other.getAdministrationHeader()))) &&
            ((this.administration==null && other.getAdministration()==null) || 
             (this.administration!=null &&
              this.administration.equals(other.getAdministration()))) &&
            ((this.administrationFooter==null && other.getAdministrationFooter()==null) || 
             (this.administrationFooter!=null &&
              this.administrationFooter.equals(other.getAdministrationFooter()))) &&
            ((this.missedDoseHeader==null && other.getMissedDoseHeader()==null) || 
             (this.missedDoseHeader!=null &&
              this.missedDoseHeader.equals(other.getMissedDoseHeader()))) &&
            ((this.missedDose==null && other.getMissedDose()==null) || 
             (this.missedDose!=null &&
              this.missedDose.equals(other.getMissedDose()))) &&
            ((this.missedDoseFooter==null && other.getMissedDoseFooter()==null) || 
             (this.missedDoseFooter!=null &&
              this.missedDoseFooter.equals(other.getMissedDoseFooter()))) &&
            ((this.interactionsHeader==null && other.getInteractionsHeader()==null) || 
             (this.interactionsHeader!=null &&
              this.interactionsHeader.equals(other.getInteractionsHeader()))) &&
            ((this.interactions==null && other.getInteractions()==null) || 
             (this.interactions!=null &&
              this.interactions.equals(other.getInteractions()))) &&
            ((this.interactionsFooter==null && other.getInteractionsFooter()==null) || 
             (this.interactionsFooter!=null &&
              this.interactionsFooter.equals(other.getInteractionsFooter()))) &&
            ((this.monitoringHeader==null && other.getMonitoringHeader()==null) || 
             (this.monitoringHeader!=null &&
              this.monitoringHeader.equals(other.getMonitoringHeader()))) &&
            ((this.monitoring==null && other.getMonitoring()==null) || 
             (this.monitoring!=null &&
              this.monitoring.equals(other.getMonitoring()))) &&
            ((this.monitoringFooter==null && other.getMonitoringFooter()==null) || 
             (this.monitoringFooter!=null &&
              this.monitoringFooter.equals(other.getMonitoringFooter()))) &&
            ((this.sideEffectsHeader==null && other.getSideEffectsHeader()==null) || 
             (this.sideEffectsHeader!=null &&
              this.sideEffectsHeader.equals(other.getSideEffectsHeader()))) &&
            ((this.sideEffects==null && other.getSideEffects()==null) || 
             (this.sideEffects!=null &&
              this.sideEffects.equals(other.getSideEffects()))) &&
            ((this.sideEffectsFooter==null && other.getSideEffectsFooter()==null) || 
             (this.sideEffectsFooter!=null &&
              this.sideEffectsFooter.equals(other.getSideEffectsFooter()))) &&
            ((this.storageHeader==null && other.getStorageHeader()==null) || 
             (this.storageHeader!=null &&
              this.storageHeader.equals(other.getStorageHeader()))) &&
            ((this.storage==null && other.getStorage()==null) || 
             (this.storage!=null &&
              this.storage.equals(other.getStorage()))) &&
            ((this.storageFooter==null && other.getStorageFooter()==null) || 
             (this.storageFooter!=null &&
              this.storageFooter.equals(other.getStorageFooter()))) &&
            ((this.lastUpdated==null && other.getLastUpdated()==null) || 
             (this.lastUpdated!=null &&
              this.lastUpdated.equals(other.getLastUpdated()))) &&
            ((this.copyright==null && other.getCopyright()==null) || 
             (this.copyright!=null &&
              this.copyright.equals(other.getCopyright())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getSheetTitleId();
        if (getSheetName() != null) {
            _hashCode += getSheetName().hashCode();
        }
        if (getTradeNames() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTradeNames());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTradeNames(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getDescriptionHeader() != null) {
            _hashCode += getDescriptionHeader().hashCode();
        }
        if (getDescription() != null) {
            _hashCode += getDescription().hashCode();
        }
        if (getDescriptionFooter() != null) {
            _hashCode += getDescriptionFooter().hashCode();
        }
        if (getContraindicationsHeader() != null) {
            _hashCode += getContraindicationsHeader().hashCode();
        }
        if (getContraindications() != null) {
            _hashCode += getContraindications().hashCode();
        }
        if (getContraindicationsFooter() != null) {
            _hashCode += getContraindicationsFooter().hashCode();
        }
        if (getAdministrationHeader() != null) {
            _hashCode += getAdministrationHeader().hashCode();
        }
        if (getAdministration() != null) {
            _hashCode += getAdministration().hashCode();
        }
        if (getAdministrationFooter() != null) {
            _hashCode += getAdministrationFooter().hashCode();
        }
        if (getMissedDoseHeader() != null) {
            _hashCode += getMissedDoseHeader().hashCode();
        }
        if (getMissedDose() != null) {
            _hashCode += getMissedDose().hashCode();
        }
        if (getMissedDoseFooter() != null) {
            _hashCode += getMissedDoseFooter().hashCode();
        }
        if (getInteractionsHeader() != null) {
            _hashCode += getInteractionsHeader().hashCode();
        }
        if (getInteractions() != null) {
            _hashCode += getInteractions().hashCode();
        }
        if (getInteractionsFooter() != null) {
            _hashCode += getInteractionsFooter().hashCode();
        }
        if (getMonitoringHeader() != null) {
            _hashCode += getMonitoringHeader().hashCode();
        }
        if (getMonitoring() != null) {
            _hashCode += getMonitoring().hashCode();
        }
        if (getMonitoringFooter() != null) {
            _hashCode += getMonitoringFooter().hashCode();
        }
        if (getSideEffectsHeader() != null) {
            _hashCode += getSideEffectsHeader().hashCode();
        }
        if (getSideEffects() != null) {
            _hashCode += getSideEffects().hashCode();
        }
        if (getSideEffectsFooter() != null) {
            _hashCode += getSideEffectsFooter().hashCode();
        }
        if (getStorageHeader() != null) {
            _hashCode += getStorageHeader().hashCode();
        }
        if (getStorage() != null) {
            _hashCode += getStorage().hashCode();
        }
        if (getStorageFooter() != null) {
            _hashCode += getStorageFooter().hashCode();
        }
        if (getLastUpdated() != null) {
            _hashCode += getLastUpdated().hashCode();
        }
        if (getCopyright() != null) {
            _hashCode += getCopyright().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PatientEducationType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "PatientEducationType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sheetTitleId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "SheetTitleId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sheetName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "SheetName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tradeNames");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "TradeNames"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "string"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descriptionHeader");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "DescriptionHeader"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("description");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "Description"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descriptionFooter");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "DescriptionFooter"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contraindicationsHeader");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "ContraindicationsHeader"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contraindications");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "Contraindications"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contraindicationsFooter");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "ContraindicationsFooter"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("administrationHeader");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "AdministrationHeader"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("administration");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "Administration"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("administrationFooter");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "AdministrationFooter"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("missedDoseHeader");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "MissedDoseHeader"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("missedDose");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "MissedDose"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("missedDoseFooter");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "MissedDoseFooter"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("interactionsHeader");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "InteractionsHeader"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("interactions");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "Interactions"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("interactionsFooter");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "InteractionsFooter"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("monitoringHeader");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "MonitoringHeader"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("monitoring");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "Monitoring"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("monitoringFooter");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "MonitoringFooter"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sideEffectsHeader");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "SideEffectsHeader"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sideEffects");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "SideEffects"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sideEffectsFooter");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "SideEffectsFooter"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("storageHeader");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "StorageHeader"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("storage");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "Storage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("storageFooter");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "StorageFooter"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastUpdated");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "LastUpdated"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("copyright");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "Copyright"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
