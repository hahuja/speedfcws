/**
 * IGSDIBaseConsDrugInteractionStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1;

public class IGSDIBaseConsDrugInteractionStub extends org.apache.axis.client.Stub implements com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.IGSDIBaseConsDrugInteraction {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[5];
        _initOperationDesc1();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("SearchForDrugNames");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "AuthenticationKey"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "SearchTerm"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "TrademarkOrGeneric"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "TrademarkGenericEnum"), com.Goldstandard.GSDI.DataContract._2011._3._1.TrademarkGenericEnum.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "DrugNameSearchResult"));
        oper.setReturnClass(com.Goldstandard.GSDI.DataContract._2011._3._1.DrugNameSearchResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "SearchForDrugNamesResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("SearchForRepresentativeMarketedProductId");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "AuthenticationKey"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "SearchTerm"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "ProductSearchResult"));
        oper.setReturnClass(com.Goldstandard.GSDI.DataContract._2011._3._1.ProductSearchResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "SearchForRepresentativeMarketedProductIdResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetConsumerSeverityRankingDescriptions");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "AuthenticationKey"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "ArrayOfSeverityRankingDescription"));
        oper.setReturnClass(com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.SeverityRankingDescription[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "GetConsumerSeverityRankingDescriptionsResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "SeverityRankingDescription"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetConsumerInteractions");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "AuthenticationKey"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "Products"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "ArrayOfProductIdentifierType"), com.Goldstandard.GSDI.DataContract._2011._3._1.ProductIdentifierType[].class, false, false);
        param.setItemQName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "ProductIdentifierType"));
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "Packages"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "ArrayOfPackageIdentifierType"), com.Goldstandard.GSDI.DataContract._2011._3._1.PackageIdentifierType[].class, false, false);
        param.setItemQName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "PackageIdentifierType"));
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "MarketedProductId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "ArrayOfInt"), int[].class, false, false);
        param.setItemQName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "int"));
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "RepresentativeMarketedProductId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "ArrayOfInt"), int[].class, false, false);
        param.setItemQName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "int"));
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "SeverityLabel"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "SeverityLabelFilter"), com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.SeverityLabelFilter.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "IncludeCaffeine"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"), boolean.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "IncludeEnteral"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"), boolean.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "IncludeEthanol"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"), boolean.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "IncludeFood"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"), boolean.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "IncludeGrapefruit"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"), boolean.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "IncludeTobacco"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"), boolean.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "ConsumerInteractionReport"));
        oper.setReturnClass(com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.ConsumerInteractionReport.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "GetConsumerInteractionsResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetConsumerInteractionsV2");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "AuthenticationKey"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "Prescribed"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "PrescribedType"), com.Goldstandard.GSDI.DataContract._2011._3._1.PrescribedType.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "Prescribing"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "PrescribedType"), com.Goldstandard.GSDI.DataContract._2011._3._1.PrescribedType.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "SeverityLabel"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "SeverityLabelFilter"), com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.SeverityLabelFilter.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "IncludeCaffeine"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"), boolean.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "IncludeEnteral"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"), boolean.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "IncludeEthanol"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"), boolean.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "IncludeFood"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"), boolean.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "IncludeGrapefruit"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"), boolean.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "IncludeTobacco"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"), boolean.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "IgnorePrescribed"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"), boolean.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "ConsumerInteractionReport"));
        oper.setReturnClass(com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.ConsumerInteractionReport.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "GetConsumerInteractionsV2Result"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[4] = oper;

    }

    public IGSDIBaseConsDrugInteractionStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public IGSDIBaseConsDrugInteractionStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public IGSDIBaseConsDrugInteractionStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "ArrayOfInt");
            cachedSerQNames.add(qName);
            cls = int[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int");
            qName2 = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "int");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "ArrayOfOrderableNameType");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract._2011._3._1.OrderableNameType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "OrderableNameType");
            qName2 = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "OrderableNameType");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "ArrayOfPackageIdentifierType");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract._2011._3._1.PackageIdentifierType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "PackageIdentifierType");
            qName2 = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "PackageIdentifierType");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "ArrayOfProductDetail");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract._2011._3._1.ProductDetail[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "ProductDetail");
            qName2 = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "ProductDetail");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "ArrayOfProductIdentifierType");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract._2011._3._1.ProductIdentifierType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "ProductIdentifierType");
            qName2 = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "ProductIdentifierType");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "DrugNameSearchResult");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract._2011._3._1.DrugNameSearchResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "OrderableNameEnum");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract._2011._3._1.OrderableNameEnum.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "OrderableNameType");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract._2011._3._1.OrderableNameType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "PackageIdentifierType");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract._2011._3._1.PackageIdentifierType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "PackageIdentifierTypeEnum");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract._2011._3._1.PackageIdentifierTypeEnum.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "PrescribedType");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract._2011._3._1.PrescribedType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "ProductDetail");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract._2011._3._1.ProductDetail.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "ProductIdentifierType");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract._2011._3._1.ProductIdentifierType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "ProductIdentifierTypeEnum");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract._2011._3._1.ProductIdentifierTypeEnum.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "ProductSearchResult");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract._2011._3._1.ProductSearchResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "TrademarkGenericEnum");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract._2011._3._1.TrademarkGenericEnum.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "ArrayOfDrug_MarketedSpecificProductType");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.Drug_MarketedSpecificProductType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "Drug_MarketedSpecificProductType");
            qName2 = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "Drug_MarketedSpecificProductType");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "ArrayOfDrug_PackageType");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.Drug_PackageType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "Drug_PackageType");
            qName2 = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "Drug_PackageType");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "ArrayOfDrug_ProductType");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.Drug_ProductType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "Drug_ProductType");
            qName2 = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "Drug_ProductType");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "ArrayOfIdentifierNameType");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.IdentifierNameType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "IdentifierNameType");
            qName2 = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "IdentifierNameType");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "ArrayOfInt");
            cachedSerQNames.add(qName);
            cls = int[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int");
            qName2 = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "int");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "ArrayOfInteractionPackageType");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.InteractionPackageType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "InteractionPackageType");
            qName2 = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "InteractionPackageType");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "ArrayOfInteractionProductType");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.InteractionProductType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "InteractionProductType");
            qName2 = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "InteractionProductType");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "ArrayOfInteractionType");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.InteractionType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "InteractionType");
            qName2 = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "InteractionType");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "ArrayOfLifestyleInteractionType");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.LifestyleInteractionType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "LifestyleInteractionType");
            qName2 = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "LifestyleInteractionType");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "ArrayOfMarketedSpecificProductType");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.MarketedSpecificProductType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "MarketedSpecificProductType");
            qName2 = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "MarketedSpecificProductType");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "ArrayOfPackageIdentifierType");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract._2011._3._1.PackageIdentifierType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "PackageIdentifierType");
            qName2 = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "PackageIdentifierType");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "ArrayOfProductIdentifierType");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract._2011._3._1.ProductIdentifierType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "ProductIdentifierType");
            qName2 = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "ProductIdentifierType");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "ArrayOfSeverityRankingDescription");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.SeverityRankingDescription[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "SeverityRankingDescription");
            qName2 = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "SeverityRankingDescription");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "ConsumerInteractionReport");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.ConsumerInteractionReport.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "Drug_MarketedSpecificProductType");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.Drug_MarketedSpecificProductType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "Drug_PackageType");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.Drug_PackageType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "Drug_ProductType");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.Drug_ProductType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "DrugType");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.DrugType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "IdentifierNameType");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.IdentifierNameType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "InteractionClassType");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.InteractionClassType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "InteractionPackageType");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.InteractionPackageType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "InteractionProductType");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.InteractionProductType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "InteractionType");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.InteractionType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "LifestyleDrugClassType");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.LifestyleDrugClassType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "LifestyleInteractionType");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.LifestyleInteractionType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "MarketedSpecificProductType");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.MarketedSpecificProductType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "SeverityLabelFilter");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.SeverityLabelFilter.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "SeverityRankingDescription");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.SeverityRankingDescription.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "SeverityType");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.SeverityType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public com.Goldstandard.GSDI.DataContract._2011._3._1.DrugNameSearchResult searchForDrugNames(java.lang.String authenticationKey, java.lang.String searchTerm, com.Goldstandard.GSDI.DataContract._2011._3._1.TrademarkGenericEnum trademarkOrGeneric) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1/SearchForDrugNames");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "SearchForDrugNames"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {authenticationKey, searchTerm, trademarkOrGeneric});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.Goldstandard.GSDI.DataContract._2011._3._1.DrugNameSearchResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.Goldstandard.GSDI.DataContract._2011._3._1.DrugNameSearchResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.Goldstandard.GSDI.DataContract._2011._3._1.DrugNameSearchResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.Goldstandard.GSDI.DataContract._2011._3._1.ProductSearchResult searchForRepresentativeMarketedProductId(java.lang.String authenticationKey, java.lang.String searchTerm) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1/SearchForRepresentativeMarketedProductId");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "SearchForRepresentativeMarketedProductId"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {authenticationKey, searchTerm});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.Goldstandard.GSDI.DataContract._2011._3._1.ProductSearchResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.Goldstandard.GSDI.DataContract._2011._3._1.ProductSearchResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.Goldstandard.GSDI.DataContract._2011._3._1.ProductSearchResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.SeverityRankingDescription[] getConsumerSeverityRankingDescriptions(java.lang.String authenticationKey) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1/GetConsumerSeverityRankingDescriptions");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "GetConsumerSeverityRankingDescriptions"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {authenticationKey});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.SeverityRankingDescription[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.SeverityRankingDescription[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.SeverityRankingDescription[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.ConsumerInteractionReport getConsumerInteractions(java.lang.String authenticationKey, com.Goldstandard.GSDI.DataContract._2011._3._1.ProductIdentifierType[] products, com.Goldstandard.GSDI.DataContract._2011._3._1.PackageIdentifierType[] packages, int[] marketedProductId, int[] representativeMarketedProductId, com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.SeverityLabelFilter severityLabel, boolean includeCaffeine, boolean includeEnteral, boolean includeEthanol, boolean includeFood, boolean includeGrapefruit, boolean includeTobacco) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1/GetConsumerInteractions");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "GetConsumerInteractions"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {authenticationKey, products, packages, marketedProductId, representativeMarketedProductId, severityLabel, new java.lang.Boolean(includeCaffeine), new java.lang.Boolean(includeEnteral), new java.lang.Boolean(includeEthanol), new java.lang.Boolean(includeFood), new java.lang.Boolean(includeGrapefruit), new java.lang.Boolean(includeTobacco)});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.ConsumerInteractionReport) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.ConsumerInteractionReport) org.apache.axis.utils.JavaUtils.convert(_resp, com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.ConsumerInteractionReport.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.ConsumerInteractionReport getConsumerInteractionsV2(java.lang.String authenticationKey, com.Goldstandard.GSDI.DataContract._2011._3._1.PrescribedType prescribed, com.Goldstandard.GSDI.DataContract._2011._3._1.PrescribedType prescribing, com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.SeverityLabelFilter severityLabel, boolean includeCaffeine, boolean includeEnteral, boolean includeEthanol, boolean includeFood, boolean includeGrapefruit, boolean includeTobacco, boolean ignorePrescribed) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1/GetConsumerInteractionsV2");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "GetConsumerInteractionsV2"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {authenticationKey, prescribed, prescribing, severityLabel, new java.lang.Boolean(includeCaffeine), new java.lang.Boolean(includeEnteral), new java.lang.Boolean(includeEthanol), new java.lang.Boolean(includeFood), new java.lang.Boolean(includeGrapefruit), new java.lang.Boolean(includeTobacco), new java.lang.Boolean(ignorePrescribed)});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.ConsumerInteractionReport) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.ConsumerInteractionReport) org.apache.axis.utils.JavaUtils.convert(_resp, com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.ConsumerInteractionReport.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}
