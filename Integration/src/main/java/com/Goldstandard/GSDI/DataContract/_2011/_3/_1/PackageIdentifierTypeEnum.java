/**
 * PackageIdentifierTypeEnum.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.Goldstandard.GSDI.DataContract._2011._3._1;

public class PackageIdentifierTypeEnum implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected PackageIdentifierTypeEnum(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _NDC10 = "NDC10";
    public static final java.lang.String _NDC11 = "NDC11";
    public static final java.lang.String _UPC = "UPC";
    public static final java.lang.String _GTIN12 = "GTIN12";
    public static final java.lang.String _GTIN14 = "GTIN14";
    public static final PackageIdentifierTypeEnum NDC10 = new PackageIdentifierTypeEnum(_NDC10);
    public static final PackageIdentifierTypeEnum NDC11 = new PackageIdentifierTypeEnum(_NDC11);
    public static final PackageIdentifierTypeEnum UPC = new PackageIdentifierTypeEnum(_UPC);
    public static final PackageIdentifierTypeEnum GTIN12 = new PackageIdentifierTypeEnum(_GTIN12);
    public static final PackageIdentifierTypeEnum GTIN14 = new PackageIdentifierTypeEnum(_GTIN14);
    public java.lang.String getValue() { return _value_;}
    public static PackageIdentifierTypeEnum fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        PackageIdentifierTypeEnum enumeration = (PackageIdentifierTypeEnum)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static PackageIdentifierTypeEnum fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PackageIdentifierTypeEnum.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "PackageIdentifierTypeEnum"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
