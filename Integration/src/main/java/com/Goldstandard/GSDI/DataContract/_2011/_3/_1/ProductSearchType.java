/**
 * ProductSearchType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.Goldstandard.GSDI.DataContract._2011._3._1;

public class ProductSearchType implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected ProductSearchType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _DrugName = "DrugName";
    public static final java.lang.String _CPNUM = "CPNUM";
    public static final java.lang.String _NDC9 = "NDC9";
    public static final java.lang.String _NDC10 = "NDC10";
    public static final java.lang.String _NDC11 = "NDC11";
    public static final java.lang.String _ProductId = "ProductId";
    public static final java.lang.String _UPC = "UPC";
    public static final java.lang.String _MarketedProductId = "MarketedProductId";
    public static final java.lang.String _GTIN12 = "GTIN12";
    public static final java.lang.String _GTIN14 = "GTIN14";
    public static final ProductSearchType DrugName = new ProductSearchType(_DrugName);
    public static final ProductSearchType CPNUM = new ProductSearchType(_CPNUM);
    public static final ProductSearchType NDC9 = new ProductSearchType(_NDC9);
    public static final ProductSearchType NDC10 = new ProductSearchType(_NDC10);
    public static final ProductSearchType NDC11 = new ProductSearchType(_NDC11);
    public static final ProductSearchType ProductId = new ProductSearchType(_ProductId);
    public static final ProductSearchType UPC = new ProductSearchType(_UPC);
    public static final ProductSearchType MarketedProductId = new ProductSearchType(_MarketedProductId);
    public static final ProductSearchType GTIN12 = new ProductSearchType(_GTIN12);
    public static final ProductSearchType GTIN14 = new ProductSearchType(_GTIN14);
    public java.lang.String getValue() { return _value_;}
    public static ProductSearchType fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        ProductSearchType enumeration = (ProductSearchType)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static ProductSearchType fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ProductSearchType.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://GSDI.GoldStandard.com/DataContract/2011/3/1", "ProductSearchType"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
