/**
 * ProductImageResult.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.Goldstandard.GSDI.DataContract._2011._3._1;

public class ProductImageResult  implements java.io.Serializable {
    private ProductSearchType searchInputType;

    private java.lang.String searchInputTerm;

    private com.Goldstandard.GSDI.DataContract._2011._3._1.ProductImageDetail[] productImageDetails;

    public ProductImageResult() {
    }

    public ProductImageResult(
           ProductSearchType searchInputType,
           java.lang.String searchInputTerm,
           com.Goldstandard.GSDI.DataContract._2011._3._1.ProductImageDetail[] productImageDetails) {
           this.searchInputType = searchInputType;
           this.searchInputTerm = searchInputTerm;
           this.productImageDetails = productImageDetails;
    }


    /**
     * Gets the searchInputType value for this ProductImageResult.
     * 
     * @return searchInputType
     */
    public ProductSearchType getSearchInputType() {
        return searchInputType;
    }


    /**
     * Sets the searchInputType value for this ProductImageResult.
     * 
     * @param searchInputType
     */
    public void setSearchInputType(ProductSearchType searchInputType) {
        this.searchInputType = searchInputType;
    }


    /**
     * Gets the searchInputTerm value for this ProductImageResult.
     * 
     * @return searchInputTerm
     */
    public java.lang.String getSearchInputTerm() {
        return searchInputTerm;
    }


    /**
     * Sets the searchInputTerm value for this ProductImageResult.
     * 
     * @param searchInputTerm
     */
    public void setSearchInputTerm(java.lang.String searchInputTerm) {
        this.searchInputTerm = searchInputTerm;
    }


    /**
     * Gets the productImageDetails value for this ProductImageResult.
     * 
     * @return productImageDetails
     */
    public com.Goldstandard.GSDI.DataContract._2011._3._1.ProductImageDetail[] getProductImageDetails() {
        return productImageDetails;
    }


    /**
     * Sets the productImageDetails value for this ProductImageResult.
     * 
     * @param productImageDetails
     */
    public void setProductImageDetails(com.Goldstandard.GSDI.DataContract._2011._3._1.ProductImageDetail[] productImageDetails) {
        this.productImageDetails = productImageDetails;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ProductImageResult)) return false;
        ProductImageResult other = (ProductImageResult) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.searchInputType==null && other.getSearchInputType()==null) || 
             (this.searchInputType!=null &&
              this.searchInputType.equals(other.getSearchInputType()))) &&
            ((this.searchInputTerm==null && other.getSearchInputTerm()==null) || 
             (this.searchInputTerm!=null &&
              this.searchInputTerm.equals(other.getSearchInputTerm()))) &&
            ((this.productImageDetails==null && other.getProductImageDetails()==null) || 
             (this.productImageDetails!=null &&
              java.util.Arrays.equals(this.productImageDetails, other.getProductImageDetails())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSearchInputType() != null) {
            _hashCode += getSearchInputType().hashCode();
        }
        if (getSearchInputTerm() != null) {
            _hashCode += getSearchInputTerm().hashCode();
        }
        if (getProductImageDetails() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getProductImageDetails());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getProductImageDetails(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ProductImageResult.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "ProductImageResult"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("searchInputType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "SearchInputType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://GSDI.GoldStandard.com/DataContract/2011/3/1", "ProductSearchType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("searchInputTerm");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "SearchInputTerm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productImageDetails");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "ProductImageDetails"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "ProductImageDetail"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "ProductImageDetail"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
