/**
 * DrugIdentificationResult.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1;

public class DrugIdentificationResult  implements java.io.Serializable {
    private int marketedProductId;

    private int CPNum;

    private java.lang.String productNameLong;

    private java.lang.String NDC9;

    private java.lang.String[] UPC;

    private boolean onMarket;

    private java.lang.String marketer;

    private com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.IdentifierNameType[] activeIngredients;

    private com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugIdentification[] drugIdentifications;

    public DrugIdentificationResult() {
    }

    public DrugIdentificationResult(
           int marketedProductId,
           int CPNum,
           java.lang.String productNameLong,
           java.lang.String NDC9,
           java.lang.String[] UPC,
           boolean onMarket,
           java.lang.String marketer,
           com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.IdentifierNameType[] activeIngredients,
           com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugIdentification[] drugIdentifications) {
           this.marketedProductId = marketedProductId;
           this.CPNum = CPNum;
           this.productNameLong = productNameLong;
           this.NDC9 = NDC9;
           this.UPC = UPC;
           this.onMarket = onMarket;
           this.marketer = marketer;
           this.activeIngredients = activeIngredients;
           this.drugIdentifications = drugIdentifications;
    }


    /**
     * Gets the marketedProductId value for this DrugIdentificationResult.
     * 
     * @return marketedProductId
     */
    public int getMarketedProductId() {
        return marketedProductId;
    }


    /**
     * Sets the marketedProductId value for this DrugIdentificationResult.
     * 
     * @param marketedProductId
     */
    public void setMarketedProductId(int marketedProductId) {
        this.marketedProductId = marketedProductId;
    }


    /**
     * Gets the CPNum value for this DrugIdentificationResult.
     * 
     * @return CPNum
     */
    public int getCPNum() {
        return CPNum;
    }


    /**
     * Sets the CPNum value for this DrugIdentificationResult.
     * 
     * @param CPNum
     */
    public void setCPNum(int CPNum) {
        this.CPNum = CPNum;
    }


    /**
     * Gets the productNameLong value for this DrugIdentificationResult.
     * 
     * @return productNameLong
     */
    public java.lang.String getProductNameLong() {
        return productNameLong;
    }


    /**
     * Sets the productNameLong value for this DrugIdentificationResult.
     * 
     * @param productNameLong
     */
    public void setProductNameLong(java.lang.String productNameLong) {
        this.productNameLong = productNameLong;
    }


    /**
     * Gets the NDC9 value for this DrugIdentificationResult.
     * 
     * @return NDC9
     */
    public java.lang.String getNDC9() {
        return NDC9;
    }


    /**
     * Sets the NDC9 value for this DrugIdentificationResult.
     * 
     * @param NDC9
     */
    public void setNDC9(java.lang.String NDC9) {
        this.NDC9 = NDC9;
    }


    /**
     * Gets the UPC value for this DrugIdentificationResult.
     * 
     * @return UPC
     */
    public java.lang.String[] getUPC() {
        return UPC;
    }


    /**
     * Sets the UPC value for this DrugIdentificationResult.
     * 
     * @param UPC
     */
    public void setUPC(java.lang.String[] UPC) {
        this.UPC = UPC;
    }


    /**
     * Gets the onMarket value for this DrugIdentificationResult.
     * 
     * @return onMarket
     */
    public boolean isOnMarket() {
        return onMarket;
    }


    /**
     * Sets the onMarket value for this DrugIdentificationResult.
     * 
     * @param onMarket
     */
    public void setOnMarket(boolean onMarket) {
        this.onMarket = onMarket;
    }


    /**
     * Gets the marketer value for this DrugIdentificationResult.
     * 
     * @return marketer
     */
    public java.lang.String getMarketer() {
        return marketer;
    }


    /**
     * Sets the marketer value for this DrugIdentificationResult.
     * 
     * @param marketer
     */
    public void setMarketer(java.lang.String marketer) {
        this.marketer = marketer;
    }


    /**
     * Gets the activeIngredients value for this DrugIdentificationResult.
     * 
     * @return activeIngredients
     */
    public com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.IdentifierNameType[] getActiveIngredients() {
        return activeIngredients;
    }


    /**
     * Sets the activeIngredients value for this DrugIdentificationResult.
     * 
     * @param activeIngredients
     */
    public void setActiveIngredients(com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.IdentifierNameType[] activeIngredients) {
        this.activeIngredients = activeIngredients;
    }


    /**
     * Gets the drugIdentifications value for this DrugIdentificationResult.
     * 
     * @return drugIdentifications
     */
    public com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugIdentification[] getDrugIdentifications() {
        return drugIdentifications;
    }


    /**
     * Sets the drugIdentifications value for this DrugIdentificationResult.
     * 
     * @param drugIdentifications
     */
    public void setDrugIdentifications(com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugIdentification[] drugIdentifications) {
        this.drugIdentifications = drugIdentifications;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DrugIdentificationResult)) return false;
        DrugIdentificationResult other = (DrugIdentificationResult) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.marketedProductId == other.getMarketedProductId() &&
            this.CPNum == other.getCPNum() &&
            ((this.productNameLong==null && other.getProductNameLong()==null) || 
             (this.productNameLong!=null &&
              this.productNameLong.equals(other.getProductNameLong()))) &&
            ((this.NDC9==null && other.getNDC9()==null) || 
             (this.NDC9!=null &&
              this.NDC9.equals(other.getNDC9()))) &&
            ((this.UPC==null && other.getUPC()==null) || 
             (this.UPC!=null &&
              java.util.Arrays.equals(this.UPC, other.getUPC()))) &&
            this.onMarket == other.isOnMarket() &&
            ((this.marketer==null && other.getMarketer()==null) || 
             (this.marketer!=null &&
              this.marketer.equals(other.getMarketer()))) &&
            ((this.activeIngredients==null && other.getActiveIngredients()==null) || 
             (this.activeIngredients!=null &&
              java.util.Arrays.equals(this.activeIngredients, other.getActiveIngredients()))) &&
            ((this.drugIdentifications==null && other.getDrugIdentifications()==null) || 
             (this.drugIdentifications!=null &&
              java.util.Arrays.equals(this.drugIdentifications, other.getDrugIdentifications())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getMarketedProductId();
        _hashCode += getCPNum();
        if (getProductNameLong() != null) {
            _hashCode += getProductNameLong().hashCode();
        }
        if (getNDC9() != null) {
            _hashCode += getNDC9().hashCode();
        }
        if (getUPC() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getUPC());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getUPC(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        _hashCode += (isOnMarket() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getMarketer() != null) {
            _hashCode += getMarketer().hashCode();
        }
        if (getActiveIngredients() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getActiveIngredients());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getActiveIngredients(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getDrugIdentifications() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDrugIdentifications());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDrugIdentifications(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DrugIdentificationResult.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "DrugIdentificationResult"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("marketedProductId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "MarketedProductId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CPNum");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "CPNum"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productNameLong");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "ProductNameLong"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NDC9");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "NDC9"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("UPC");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "UPC"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "string"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("onMarket");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "OnMarket"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("marketer");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "Marketer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("activeIngredients");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "ActiveIngredients"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "IdentifierNameType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "IdentifierNameType"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("drugIdentifications");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "DrugIdentifications"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "DrugIdentification"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "DrugIdentification"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
