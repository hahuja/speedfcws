/**
 * IGSDIBaseDrugIdentifier1Stub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1;

public class IGSDIBaseDrugIdentifier1Stub extends org.apache.axis.client.Stub implements com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.IGSDIBaseDrugIdentifier {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[4];
        _initOperationDesc1();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ListColors");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "AuthenticationKey"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "ArrayOfDrugColor"));
        oper.setReturnClass(com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugColor[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "ListColorsResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "DrugColor"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ListShapes");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "AuthenticationKey"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "ArrayOfDrugShape"));
        oper.setReturnClass(com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugShape[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "ListShapesResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "DrugShape"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ListScorings");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "AuthenticationKey"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "ArrayOfDrugScoring"));
        oper.setReturnClass(com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugScoring[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "ListScoringsResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "DrugScoring"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("SearchDrugIdentifications");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "AuthenticationKey"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "colorId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "shapeId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "markingSide1"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "markingSide2"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "scoringId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "includePrivateLabels"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"), boolean.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "excludeTablets"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"), boolean.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "excludeCapsules"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"), boolean.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "includeRepackaged"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"), boolean.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "ArrayOfDrugIdentificationResult"));
        oper.setReturnClass(com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugIdentificationResult[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "SearchDrugIdentificationsResult"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "DrugIdentificationResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[3] = oper;

    }

    public IGSDIBaseDrugIdentifier1Stub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public IGSDIBaseDrugIdentifier1Stub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public IGSDIBaseDrugIdentifier1Stub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "ProductImage");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract._2011._3._1.ProductImage.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "ProductImageTypeEnum");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract._2011._3._1.ProductImageTypeEnum.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "ArrayOfDrugColor");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugColor[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "DrugColor");
            qName2 = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "DrugColor");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "ArrayOfDrugIdentification");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugIdentification[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "DrugIdentification");
            qName2 = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "DrugIdentification");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "ArrayOfDrugIdentificationResult");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugIdentificationResult[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "DrugIdentificationResult");
            qName2 = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "DrugIdentificationResult");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "ArrayOfDrugScoring");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugScoring[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "DrugScoring");
            qName2 = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "DrugScoring");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "ArrayOfDrugShape");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugShape[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "DrugShape");
            qName2 = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "DrugShape");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "ArrayOfIdentifierNameType");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.IdentifierNameType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "IdentifierNameType");
            qName2 = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "IdentifierNameType");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "ArrayOfProductImage");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract._2011._3._1.ProductImage[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "ProductImage");
            qName2 = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "ProductImage");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "ArrayOfString");
            cachedSerQNames.add(qName);
            cls = java.lang.String[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string");
            qName2 = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "string");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "DrugColor");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugColor.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "DrugIdentification");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugIdentification.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "DrugIdentificationResult");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugIdentificationResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "DrugScoring");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugScoring.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "DrugShape");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugShape.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "IdentifierNameType");
            cachedSerQNames.add(qName);
            cls = com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.IdentifierNameType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugColor[] listColors(java.lang.String authenticationKey) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1/ListColors");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "ListColors"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {authenticationKey});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugColor[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugColor[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugColor[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugShape[] listShapes(java.lang.String authenticationKey) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1/ListShapes");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "ListShapes"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {authenticationKey});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugShape[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugShape[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugShape[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugScoring[] listScorings(java.lang.String authenticationKey) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1/ListScorings");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "ListScorings"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {authenticationKey});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugScoring[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugScoring[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugScoring[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugIdentificationResult[] searchDrugIdentifications(java.lang.String authenticationKey, int colorId, int shapeId, java.lang.String markingSide1, java.lang.String markingSide2, int scoringId, boolean includePrivateLabels, boolean excludeTablets, boolean excludeCapsules, boolean includeRepackaged) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1/SearchDrugIdentifications");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP12_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/DrugIdentifier/2011/3/1", "SearchDrugIdentifications"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {authenticationKey, new java.lang.Integer(colorId), new java.lang.Integer(shapeId), markingSide1, markingSide2, new java.lang.Integer(scoringId), new java.lang.Boolean(includePrivateLabels), new java.lang.Boolean(excludeTablets), new java.lang.Boolean(excludeCapsules), new java.lang.Boolean(includeRepackaged)});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugIdentificationResult[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugIdentificationResult[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugIdentificationResult[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}
