/**
 * InteractionType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1;

public class InteractionType  implements java.io.Serializable {
    private int drugInteractionID;

    private com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.InteractionClassType interactionClass;

    private com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.SeverityType severity;

    private com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.InteractionClassType interactingClass;

    private java.lang.String consumerNotes;

    public InteractionType() {
    }

    public InteractionType(
           int drugInteractionID,
           com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.InteractionClassType interactionClass,
           com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.SeverityType severity,
           com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.InteractionClassType interactingClass,
           java.lang.String consumerNotes) {
           this.drugInteractionID = drugInteractionID;
           this.interactionClass = interactionClass;
           this.severity = severity;
           this.interactingClass = interactingClass;
           this.consumerNotes = consumerNotes;
    }


    /**
     * Gets the drugInteractionID value for this InteractionType.
     * 
     * @return drugInteractionID
     */
    public int getDrugInteractionID() {
        return drugInteractionID;
    }


    /**
     * Sets the drugInteractionID value for this InteractionType.
     * 
     * @param drugInteractionID
     */
    public void setDrugInteractionID(int drugInteractionID) {
        this.drugInteractionID = drugInteractionID;
    }


    /**
     * Gets the interactionClass value for this InteractionType.
     * 
     * @return interactionClass
     */
    public com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.InteractionClassType getInteractionClass() {
        return interactionClass;
    }


    /**
     * Sets the interactionClass value for this InteractionType.
     * 
     * @param interactionClass
     */
    public void setInteractionClass(com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.InteractionClassType interactionClass) {
        this.interactionClass = interactionClass;
    }


    /**
     * Gets the severity value for this InteractionType.
     * 
     * @return severity
     */
    public com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.SeverityType getSeverity() {
        return severity;
    }


    /**
     * Sets the severity value for this InteractionType.
     * 
     * @param severity
     */
    public void setSeverity(com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.SeverityType severity) {
        this.severity = severity;
    }


    /**
     * Gets the interactingClass value for this InteractionType.
     * 
     * @return interactingClass
     */
    public com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.InteractionClassType getInteractingClass() {
        return interactingClass;
    }


    /**
     * Sets the interactingClass value for this InteractionType.
     * 
     * @param interactingClass
     */
    public void setInteractingClass(com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.InteractionClassType interactingClass) {
        this.interactingClass = interactingClass;
    }


    /**
     * Gets the consumerNotes value for this InteractionType.
     * 
     * @return consumerNotes
     */
    public java.lang.String getConsumerNotes() {
        return consumerNotes;
    }


    /**
     * Sets the consumerNotes value for this InteractionType.
     * 
     * @param consumerNotes
     */
    public void setConsumerNotes(java.lang.String consumerNotes) {
        this.consumerNotes = consumerNotes;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof InteractionType)) return false;
        InteractionType other = (InteractionType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.drugInteractionID == other.getDrugInteractionID() &&
            ((this.interactionClass==null && other.getInteractionClass()==null) || 
             (this.interactionClass!=null &&
              this.interactionClass.equals(other.getInteractionClass()))) &&
            ((this.severity==null && other.getSeverity()==null) || 
             (this.severity!=null &&
              this.severity.equals(other.getSeverity()))) &&
            ((this.interactingClass==null && other.getInteractingClass()==null) || 
             (this.interactingClass!=null &&
              this.interactingClass.equals(other.getInteractingClass()))) &&
            ((this.consumerNotes==null && other.getConsumerNotes()==null) || 
             (this.consumerNotes!=null &&
              this.consumerNotes.equals(other.getConsumerNotes())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getDrugInteractionID();
        if (getInteractionClass() != null) {
            _hashCode += getInteractionClass().hashCode();
        }
        if (getSeverity() != null) {
            _hashCode += getSeverity().hashCode();
        }
        if (getInteractingClass() != null) {
            _hashCode += getInteractingClass().hashCode();
        }
        if (getConsumerNotes() != null) {
            _hashCode += getConsumerNotes().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(InteractionType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "InteractionType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("drugInteractionID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "DrugInteractionID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("interactionClass");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "InteractionClass"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "InteractionClassType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("severity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "Severity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "SeverityType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("interactingClass");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "InteractingClass"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "InteractionClassType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("consumerNotes");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "ConsumerNotes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
