/**
 * GSDIBasePatientEducation.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1;

public interface GSDIBasePatientEducation extends javax.xml.rpc.Service {
    public java.lang.String getIGSDIBasePatientEducation1Address();

    public com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.IGSDIBasePatientEducation getIGSDIBasePatientEducation1() throws javax.xml.rpc.ServiceException;

    public com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.IGSDIBasePatientEducation getIGSDIBasePatientEducation1(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
    public java.lang.String getGSDIBasePatientEducationSoapAddress();

    public com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.GSDIBasePatientEducationSoap getGSDIBasePatientEducationSoap() throws javax.xml.rpc.ServiceException;

    public com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.GSDIBasePatientEducationSoap getGSDIBasePatientEducationSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
    public java.lang.String getIGSDIBasePatientEducationAddress();

    public com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.IGSDIBasePatientEducation getIGSDIBasePatientEducation() throws javax.xml.rpc.ServiceException;

    public com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.IGSDIBasePatientEducation getIGSDIBasePatientEducation(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
    public java.lang.String getGSDIBasePatientEducationSoap12Address();

    public com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.GSDIBasePatientEducationSoap12 getGSDIBasePatientEducationSoap12() throws javax.xml.rpc.ServiceException;

    public com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.GSDIBasePatientEducationSoap12 getGSDIBasePatientEducationSoap12(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
