/**
 * ProductType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1;

public class ProductType implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected ProductType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _CPNUM = "CPNUM";
    public static final java.lang.String _NDC9 = "NDC9";
    public static final java.lang.String _NDC10 = "NDC10";
    public static final java.lang.String _NDC11 = "NDC11";
    public static final java.lang.String _ProductId = "ProductId";
    public static final java.lang.String _UPC = "UPC";
    public static final java.lang.String _MarketedProductId = "MarketedProductId";
    public static final java.lang.String _GTIN12 = "GTIN12";
    public static final java.lang.String _GTIN14 = "GTIN14";
    public static final java.lang.String _RepresentativeMarketedProductId = "RepresentativeMarketedProductId";
    public static final ProductType CPNUM = new ProductType(_CPNUM);
    public static final ProductType NDC9 = new ProductType(_NDC9);
    public static final ProductType NDC10 = new ProductType(_NDC10);
    public static final ProductType NDC11 = new ProductType(_NDC11);
    public static final ProductType ProductId = new ProductType(_ProductId);
    public static final ProductType UPC = new ProductType(_UPC);
    public static final ProductType MarketedProductId = new ProductType(_MarketedProductId);
    public static final ProductType GTIN12 = new ProductType(_GTIN12);
    public static final ProductType GTIN14 = new ProductType(_GTIN14);
    public static final ProductType RepresentativeMarketedProductId = new ProductType(_RepresentativeMarketedProductId);
    public java.lang.String getValue() { return _value_;}
    public static ProductType fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        ProductType enumeration = (ProductType)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static ProductType fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ProductType.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "ProductType"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
