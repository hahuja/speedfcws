/**
 * InteractionProductType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1;

public class InteractionProductType  implements java.io.Serializable {
    private java.lang.String identifier;

    private com.Goldstandard.GSDI.DataContract._2011._3._1.ProductIdentifierTypeEnum productIdentifierType;

    private java.lang.String productName;

    private com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.LifestyleInteractionType[] lifestyle;

    private com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.DrugType drug;

    public InteractionProductType() {
    }

    public InteractionProductType(
           java.lang.String identifier,
           com.Goldstandard.GSDI.DataContract._2011._3._1.ProductIdentifierTypeEnum productIdentifierType,
           java.lang.String productName,
           com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.LifestyleInteractionType[] lifestyle,
           com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.DrugType drug) {
           this.identifier = identifier;
           this.productIdentifierType = productIdentifierType;
           this.productName = productName;
           this.lifestyle = lifestyle;
           this.drug = drug;
    }


    /**
     * Gets the identifier value for this InteractionProductType.
     * 
     * @return identifier
     */
    public java.lang.String getIdentifier() {
        return identifier;
    }


    /**
     * Sets the identifier value for this InteractionProductType.
     * 
     * @param identifier
     */
    public void setIdentifier(java.lang.String identifier) {
        this.identifier = identifier;
    }


    /**
     * Gets the productIdentifierType value for this InteractionProductType.
     * 
     * @return productIdentifierType
     */
    public com.Goldstandard.GSDI.DataContract._2011._3._1.ProductIdentifierTypeEnum getProductIdentifierType() {
        return productIdentifierType;
    }


    /**
     * Sets the productIdentifierType value for this InteractionProductType.
     * 
     * @param productIdentifierType
     */
    public void setProductIdentifierType(com.Goldstandard.GSDI.DataContract._2011._3._1.ProductIdentifierTypeEnum productIdentifierType) {
        this.productIdentifierType = productIdentifierType;
    }


    /**
     * Gets the productName value for this InteractionProductType.
     * 
     * @return productName
     */
    public java.lang.String getProductName() {
        return productName;
    }


    /**
     * Sets the productName value for this InteractionProductType.
     * 
     * @param productName
     */
    public void setProductName(java.lang.String productName) {
        this.productName = productName;
    }


    /**
     * Gets the lifestyle value for this InteractionProductType.
     * 
     * @return lifestyle
     */
    public com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.LifestyleInteractionType[] getLifestyle() {
        return lifestyle;
    }


    /**
     * Sets the lifestyle value for this InteractionProductType.
     * 
     * @param lifestyle
     */
    public void setLifestyle(com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.LifestyleInteractionType[] lifestyle) {
        this.lifestyle = lifestyle;
    }


    /**
     * Gets the drug value for this InteractionProductType.
     * 
     * @return drug
     */
    public com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.DrugType getDrug() {
        return drug;
    }


    /**
     * Sets the drug value for this InteractionProductType.
     * 
     * @param drug
     */
    public void setDrug(com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.DrugType drug) {
        this.drug = drug;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof InteractionProductType)) return false;
        InteractionProductType other = (InteractionProductType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.identifier==null && other.getIdentifier()==null) || 
             (this.identifier!=null &&
              this.identifier.equals(other.getIdentifier()))) &&
            ((this.productIdentifierType==null && other.getProductIdentifierType()==null) || 
             (this.productIdentifierType!=null &&
              this.productIdentifierType.equals(other.getProductIdentifierType()))) &&
            ((this.productName==null && other.getProductName()==null) || 
             (this.productName!=null &&
              this.productName.equals(other.getProductName()))) &&
            ((this.lifestyle==null && other.getLifestyle()==null) || 
             (this.lifestyle!=null &&
              java.util.Arrays.equals(this.lifestyle, other.getLifestyle()))) &&
            ((this.drug==null && other.getDrug()==null) || 
             (this.drug!=null &&
              this.drug.equals(other.getDrug())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getIdentifier() != null) {
            _hashCode += getIdentifier().hashCode();
        }
        if (getProductIdentifierType() != null) {
            _hashCode += getProductIdentifierType().hashCode();
        }
        if (getProductName() != null) {
            _hashCode += getProductName().hashCode();
        }
        if (getLifestyle() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getLifestyle());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getLifestyle(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getDrug() != null) {
            _hashCode += getDrug().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(InteractionProductType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "InteractionProductType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("identifier");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "Identifier"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productIdentifierType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "ProductIdentifierType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "ProductIdentifierTypeEnum"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "ProductName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lifestyle");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "Lifestyle"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "LifestyleInteractionType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "LifestyleInteractionType"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("drug");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "Drug"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "DrugType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
