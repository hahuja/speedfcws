/**
 * ProductImage.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.Goldstandard.GSDI.DataContract._2011._3._1;

public class ProductImage  implements java.io.Serializable {
    private com.Goldstandard.GSDI.DataContract._2011._3._1.ProductImageTypeEnum imageType;

    private java.lang.String imageName;

    private int version;

    private java.lang.String thumbnailUrl;

    private java.lang.String fullSizeUrl;

    public ProductImage() {
    }

    public ProductImage(
           com.Goldstandard.GSDI.DataContract._2011._3._1.ProductImageTypeEnum imageType,
           java.lang.String imageName,
           int version,
           java.lang.String thumbnailUrl,
           java.lang.String fullSizeUrl) {
           this.imageType = imageType;
           this.imageName = imageName;
           this.version = version;
           this.thumbnailUrl = thumbnailUrl;
           this.fullSizeUrl = fullSizeUrl;
    }


    /**
     * Gets the imageType value for this ProductImage.
     * 
     * @return imageType
     */
    public com.Goldstandard.GSDI.DataContract._2011._3._1.ProductImageTypeEnum getImageType() {
        return imageType;
    }


    /**
     * Sets the imageType value for this ProductImage.
     * 
     * @param imageType
     */
    public void setImageType(com.Goldstandard.GSDI.DataContract._2011._3._1.ProductImageTypeEnum imageType) {
        this.imageType = imageType;
    }


    /**
     * Gets the imageName value for this ProductImage.
     * 
     * @return imageName
     */
    public java.lang.String getImageName() {
        return imageName;
    }


    /**
     * Sets the imageName value for this ProductImage.
     * 
     * @param imageName
     */
    public void setImageName(java.lang.String imageName) {
        this.imageName = imageName;
    }


    /**
     * Gets the version value for this ProductImage.
     * 
     * @return version
     */
    public int getVersion() {
        return version;
    }


    /**
     * Sets the version value for this ProductImage.
     * 
     * @param version
     */
    public void setVersion(int version) {
        this.version = version;
    }


    /**
     * Gets the thumbnailUrl value for this ProductImage.
     * 
     * @return thumbnailUrl
     */
    public java.lang.String getThumbnailUrl() {
        return thumbnailUrl;
    }


    /**
     * Sets the thumbnailUrl value for this ProductImage.
     * 
     * @param thumbnailUrl
     */
    public void setThumbnailUrl(java.lang.String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }


    /**
     * Gets the fullSizeUrl value for this ProductImage.
     * 
     * @return fullSizeUrl
     */
    public java.lang.String getFullSizeUrl() {
        return fullSizeUrl;
    }


    /**
     * Sets the fullSizeUrl value for this ProductImage.
     * 
     * @param fullSizeUrl
     */
    public void setFullSizeUrl(java.lang.String fullSizeUrl) {
        this.fullSizeUrl = fullSizeUrl;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ProductImage)) return false;
        ProductImage other = (ProductImage) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.imageType==null && other.getImageType()==null) || 
             (this.imageType!=null &&
              this.imageType.equals(other.getImageType()))) &&
            ((this.imageName==null && other.getImageName()==null) || 
             (this.imageName!=null &&
              this.imageName.equals(other.getImageName()))) &&
            this.version == other.getVersion() &&
            ((this.thumbnailUrl==null && other.getThumbnailUrl()==null) || 
             (this.thumbnailUrl!=null &&
              this.thumbnailUrl.equals(other.getThumbnailUrl()))) &&
            ((this.fullSizeUrl==null && other.getFullSizeUrl()==null) || 
             (this.fullSizeUrl!=null &&
              this.fullSizeUrl.equals(other.getFullSizeUrl())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getImageType() != null) {
            _hashCode += getImageType().hashCode();
        }
        if (getImageName() != null) {
            _hashCode += getImageName().hashCode();
        }
        _hashCode += getVersion();
        if (getThumbnailUrl() != null) {
            _hashCode += getThumbnailUrl().hashCode();
        }
        if (getFullSizeUrl() != null) {
            _hashCode += getFullSizeUrl().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ProductImage.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "ProductImage"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("imageType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "ImageType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "ProductImageTypeEnum"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("imageName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "ImageName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("version");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "Version"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("thumbnailUrl");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "ThumbnailUrl"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fullSizeUrl");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "FullSizeUrl"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
