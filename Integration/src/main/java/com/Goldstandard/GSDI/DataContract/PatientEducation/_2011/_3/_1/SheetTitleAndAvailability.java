/**
 * SheetTitleAndAvailability.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1;

public class SheetTitleAndAvailability  implements java.io.Serializable {
    private int sheetTitleId;

    private java.lang.String sheetName;

    private boolean hasSpanishTranslation;

    private boolean hasAdditionalLanguages;

    public SheetTitleAndAvailability() {
    }

    public SheetTitleAndAvailability(
           int sheetTitleId,
           java.lang.String sheetName,
           boolean hasSpanishTranslation,
           boolean hasAdditionalLanguages) {
           this.sheetTitleId = sheetTitleId;
           this.sheetName = sheetName;
           this.hasSpanishTranslation = hasSpanishTranslation;
           this.hasAdditionalLanguages = hasAdditionalLanguages;
    }


    /**
     * Gets the sheetTitleId value for this SheetTitleAndAvailability.
     * 
     * @return sheetTitleId
     */
    public int getSheetTitleId() {
        return sheetTitleId;
    }


    /**
     * Sets the sheetTitleId value for this SheetTitleAndAvailability.
     * 
     * @param sheetTitleId
     */
    public void setSheetTitleId(int sheetTitleId) {
        this.sheetTitleId = sheetTitleId;
    }


    /**
     * Gets the sheetName value for this SheetTitleAndAvailability.
     * 
     * @return sheetName
     */
    public java.lang.String getSheetName() {
        return sheetName;
    }


    /**
     * Sets the sheetName value for this SheetTitleAndAvailability.
     * 
     * @param sheetName
     */
    public void setSheetName(java.lang.String sheetName) {
        this.sheetName = sheetName;
    }


    /**
     * Gets the hasSpanishTranslation value for this SheetTitleAndAvailability.
     * 
     * @return hasSpanishTranslation
     */
    public boolean isHasSpanishTranslation() {
        return hasSpanishTranslation;
    }


    /**
     * Sets the hasSpanishTranslation value for this SheetTitleAndAvailability.
     * 
     * @param hasSpanishTranslation
     */
    public void setHasSpanishTranslation(boolean hasSpanishTranslation) {
        this.hasSpanishTranslation = hasSpanishTranslation;
    }


    /**
     * Gets the hasAdditionalLanguages value for this SheetTitleAndAvailability.
     * 
     * @return hasAdditionalLanguages
     */
    public boolean isHasAdditionalLanguages() {
        return hasAdditionalLanguages;
    }


    /**
     * Sets the hasAdditionalLanguages value for this SheetTitleAndAvailability.
     * 
     * @param hasAdditionalLanguages
     */
    public void setHasAdditionalLanguages(boolean hasAdditionalLanguages) {
        this.hasAdditionalLanguages = hasAdditionalLanguages;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SheetTitleAndAvailability)) return false;
        SheetTitleAndAvailability other = (SheetTitleAndAvailability) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.sheetTitleId == other.getSheetTitleId() &&
            ((this.sheetName==null && other.getSheetName()==null) || 
             (this.sheetName!=null &&
              this.sheetName.equals(other.getSheetName()))) &&
            this.hasSpanishTranslation == other.isHasSpanishTranslation() &&
            this.hasAdditionalLanguages == other.isHasAdditionalLanguages();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getSheetTitleId();
        if (getSheetName() != null) {
            _hashCode += getSheetName().hashCode();
        }
        _hashCode += (isHasSpanishTranslation() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        _hashCode += (isHasAdditionalLanguages() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SheetTitleAndAvailability.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "SheetTitleAndAvailability"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sheetTitleId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "SheetTitleId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sheetName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "SheetName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("hasSpanishTranslation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "HasSpanishTranslation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("hasAdditionalLanguages");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "HasAdditionalLanguages"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
