/**
 * GSDIBaseConsDrugInteractionTestCase.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1;

public class GSDIBaseConsDrugInteractionTestCase extends junit.framework.TestCase {
    public GSDIBaseConsDrugInteractionTestCase(java.lang.String name) {
        super(name);
    }

    public void testGSDIBaseConsDrugInteractionSoap12WSDL() throws Exception {
        javax.xml.rpc.ServiceFactory serviceFactory = javax.xml.rpc.ServiceFactory.newInstance();
        java.net.URL url = new java.net.URL(new com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.GSDIBaseConsDrugInteractionLocator().getGSDIBaseConsDrugInteractionSoap12Address() + "?WSDL");
        javax.xml.rpc.Service service = serviceFactory.createService(url, new com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.GSDIBaseConsDrugInteractionLocator().getServiceName());
        assertTrue(service != null);
    }

    public void testGSDIBaseConsDrugInteractionSoapWSDL() throws Exception {
        javax.xml.rpc.ServiceFactory serviceFactory = javax.xml.rpc.ServiceFactory.newInstance();
        java.net.URL url = new java.net.URL(new com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.GSDIBaseConsDrugInteractionLocator().getGSDIBaseConsDrugInteractionSoapAddress() + "?WSDL");
        javax.xml.rpc.Service service = serviceFactory.createService(url, new com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.GSDIBaseConsDrugInteractionLocator().getServiceName());
        assertTrue(service != null);
    }

    public void testIGSDIBaseConsDrugInteraction1WSDL() throws Exception {
        javax.xml.rpc.ServiceFactory serviceFactory = javax.xml.rpc.ServiceFactory.newInstance();
        java.net.URL url = new java.net.URL(new com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.GSDIBaseConsDrugInteractionLocator().getIGSDIBaseConsDrugInteraction1Address() + "?WSDL");
        javax.xml.rpc.Service service = serviceFactory.createService(url, new com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.GSDIBaseConsDrugInteractionLocator().getServiceName());
        assertTrue(service != null);
    }

    public void test1IGSDIBaseConsDrugInteraction1SearchForDrugNames() throws Exception {
        com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.IGSDIBaseConsDrugInteraction1Stub binding;
        try {
            binding = (com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.IGSDIBaseConsDrugInteraction1Stub)
                          new com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.GSDIBaseConsDrugInteractionLocator().getIGSDIBaseConsDrugInteraction1();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.Goldstandard.GSDI.DataContract._2011._3._1.DrugNameSearchResult value = null;
        value = binding.searchForDrugNames(new java.lang.String(), new java.lang.String(), com.Goldstandard.GSDI.DataContract._2011._3._1.TrademarkGenericEnum.Trademark);
        // TBD - validate results
    }

    public void test2IGSDIBaseConsDrugInteraction1SearchForRepresentativeMarketedProductId() throws Exception {
        com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.IGSDIBaseConsDrugInteraction1Stub binding;
        try {
            binding = (com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.IGSDIBaseConsDrugInteraction1Stub)
                          new com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.GSDIBaseConsDrugInteractionLocator().getIGSDIBaseConsDrugInteraction1();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.Goldstandard.GSDI.DataContract._2011._3._1.ProductSearchResult value = null;
        value = binding.searchForRepresentativeMarketedProductId(new java.lang.String(), new java.lang.String());
        // TBD - validate results
    }

    public void test3IGSDIBaseConsDrugInteraction1GetConsumerSeverityRankingDescriptions() throws Exception {
        com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.IGSDIBaseConsDrugInteraction1Stub binding;
        try {
            binding = (com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.IGSDIBaseConsDrugInteraction1Stub)
                          new com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.GSDIBaseConsDrugInteractionLocator().getIGSDIBaseConsDrugInteraction1();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.SeverityRankingDescription[] value = null;
        value = binding.getConsumerSeverityRankingDescriptions(new java.lang.String());
        // TBD - validate results
    }

    public void test4IGSDIBaseConsDrugInteraction1GetConsumerInteractions() throws Exception {
        com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.IGSDIBaseConsDrugInteraction1Stub binding;
        try {
            binding = (com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.IGSDIBaseConsDrugInteraction1Stub)
                          new com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.GSDIBaseConsDrugInteractionLocator().getIGSDIBaseConsDrugInteraction1();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.ConsumerInteractionReport value = null;
        value = binding.getConsumerInteractions(new java.lang.String(), new com.Goldstandard.GSDI.DataContract._2011._3._1.ProductIdentifierType[0], new com.Goldstandard.GSDI.DataContract._2011._3._1.PackageIdentifierType[0], new int[0], new int[0], com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.SeverityLabelFilter.Severe, true, true, true, true, true, true);
        // TBD - validate results
    }

    public void test5IGSDIBaseConsDrugInteraction1GetConsumerInteractionsV2() throws Exception {
        com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.IGSDIBaseConsDrugInteraction1Stub binding;
        try {
            binding = (com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.IGSDIBaseConsDrugInteraction1Stub)
                          new com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.GSDIBaseConsDrugInteractionLocator().getIGSDIBaseConsDrugInteraction1();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.ConsumerInteractionReport value = null;
        value = binding.getConsumerInteractionsV2(new java.lang.String(), new com.Goldstandard.GSDI.DataContract._2011._3._1.PrescribedType(), new com.Goldstandard.GSDI.DataContract._2011._3._1.PrescribedType(), com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.SeverityLabelFilter.Severe, true, true, true, true, true, true, true);
        // TBD - validate results
    }

    public void testIGSDIBaseConsDrugInteractionWSDL() throws Exception {
        javax.xml.rpc.ServiceFactory serviceFactory = javax.xml.rpc.ServiceFactory.newInstance();
        java.net.URL url = new java.net.URL(new com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.GSDIBaseConsDrugInteractionLocator().getIGSDIBaseConsDrugInteractionAddress() + "?WSDL");
        javax.xml.rpc.Service service = serviceFactory.createService(url, new com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.GSDIBaseConsDrugInteractionLocator().getServiceName());
        assertTrue(service != null);
    }

    public void test6IGSDIBaseConsDrugInteractionSearchForDrugNames() throws Exception {
        com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.IGSDIBaseConsDrugInteractionStub binding;
        try {
            binding = (com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.IGSDIBaseConsDrugInteractionStub)
                          new com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.GSDIBaseConsDrugInteractionLocator().getIGSDIBaseConsDrugInteraction();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.Goldstandard.GSDI.DataContract._2011._3._1.DrugNameSearchResult value = null;
        value = binding.searchForDrugNames(new java.lang.String(), new java.lang.String(), com.Goldstandard.GSDI.DataContract._2011._3._1.TrademarkGenericEnum.Trademark);
        // TBD - validate results
    }

    public void test7IGSDIBaseConsDrugInteractionSearchForRepresentativeMarketedProductId() throws Exception {
        com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.IGSDIBaseConsDrugInteractionStub binding;
        try {
            binding = (com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.IGSDIBaseConsDrugInteractionStub)
                          new com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.GSDIBaseConsDrugInteractionLocator().getIGSDIBaseConsDrugInteraction();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.Goldstandard.GSDI.DataContract._2011._3._1.ProductSearchResult value = null;
        value = binding.searchForRepresentativeMarketedProductId(new java.lang.String(), new java.lang.String());
        // TBD - validate results
    }

    public void test8IGSDIBaseConsDrugInteractionGetConsumerSeverityRankingDescriptions() throws Exception {
        com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.IGSDIBaseConsDrugInteractionStub binding;
        try {
            binding = (com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.IGSDIBaseConsDrugInteractionStub)
                          new com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.GSDIBaseConsDrugInteractionLocator().getIGSDIBaseConsDrugInteraction();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.SeverityRankingDescription[] value = null;
        value = binding.getConsumerSeverityRankingDescriptions(new java.lang.String());
        // TBD - validate results
    }

    public void test9IGSDIBaseConsDrugInteractionGetConsumerInteractions() throws Exception {
        com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.IGSDIBaseConsDrugInteractionStub binding;
        try {
            binding = (com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.IGSDIBaseConsDrugInteractionStub)
                          new com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.GSDIBaseConsDrugInteractionLocator().getIGSDIBaseConsDrugInteraction();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.ConsumerInteractionReport value = null;
        value = binding.getConsumerInteractions(new java.lang.String(), new com.Goldstandard.GSDI.DataContract._2011._3._1.ProductIdentifierType[0], new com.Goldstandard.GSDI.DataContract._2011._3._1.PackageIdentifierType[0], new int[0], new int[0], com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.SeverityLabelFilter.Severe, true, true, true, true, true, true);
        // TBD - validate results
    }

    public void test10IGSDIBaseConsDrugInteractionGetConsumerInteractionsV2() throws Exception {
        com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.IGSDIBaseConsDrugInteractionStub binding;
        try {
            binding = (com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.IGSDIBaseConsDrugInteractionStub)
                          new com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.GSDIBaseConsDrugInteractionLocator().getIGSDIBaseConsDrugInteraction();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.ConsumerInteractionReport value = null;
        value = binding.getConsumerInteractionsV2(new java.lang.String(), new com.Goldstandard.GSDI.DataContract._2011._3._1.PrescribedType(), new com.Goldstandard.GSDI.DataContract._2011._3._1.PrescribedType(), com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.SeverityLabelFilter.Severe, true, true, true, true, true, true, true);
        // TBD - validate results
    }

}
