/**
 * GSDIBaseDrugIdentifierTestCase.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1;

public class GSDIBaseDrugIdentifierTestCase extends junit.framework.TestCase {
    public GSDIBaseDrugIdentifierTestCase(java.lang.String name) {
        super(name);
    }

    public void testIGSDIBaseDrugIdentifierWSDL() throws Exception {
        javax.xml.rpc.ServiceFactory serviceFactory = javax.xml.rpc.ServiceFactory.newInstance();
        java.net.URL url = new java.net.URL(new com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.GSDIBaseDrugIdentifierLocator().getIGSDIBaseDrugIdentifierAddress() + "?WSDL");
        javax.xml.rpc.Service service = serviceFactory.createService(url, new com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.GSDIBaseDrugIdentifierLocator().getServiceName());
        assertTrue(service != null);
    }

    public void test1IGSDIBaseDrugIdentifierListColors() throws Exception {
        com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.IGSDIBaseDrugIdentifierStub binding;
        try {
            binding = (com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.IGSDIBaseDrugIdentifierStub)
                          new com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.GSDIBaseDrugIdentifierLocator().getIGSDIBaseDrugIdentifier();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugColor[] value = null;
        value = binding.listColors(new java.lang.String());
        // TBD - validate results
    }

    public void test2IGSDIBaseDrugIdentifierListShapes() throws Exception {
        com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.IGSDIBaseDrugIdentifierStub binding;
        try {
            binding = (com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.IGSDIBaseDrugIdentifierStub)
                          new com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.GSDIBaseDrugIdentifierLocator().getIGSDIBaseDrugIdentifier();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugShape[] value = null;
        value = binding.listShapes(new java.lang.String());
        // TBD - validate results
    }

    public void test3IGSDIBaseDrugIdentifierListScorings() throws Exception {
        com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.IGSDIBaseDrugIdentifierStub binding;
        try {
            binding = (com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.IGSDIBaseDrugIdentifierStub)
                          new com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.GSDIBaseDrugIdentifierLocator().getIGSDIBaseDrugIdentifier();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugScoring[] value = null;
        value = binding.listScorings(new java.lang.String());
        // TBD - validate results
    }

    public void test4IGSDIBaseDrugIdentifierSearchDrugIdentifications() throws Exception {
        com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.IGSDIBaseDrugIdentifierStub binding;
        try {
            binding = (com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.IGSDIBaseDrugIdentifierStub)
                          new com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.GSDIBaseDrugIdentifierLocator().getIGSDIBaseDrugIdentifier();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugIdentificationResult[] value = null;
        value = binding.searchDrugIdentifications(new java.lang.String(), 0, 0, new java.lang.String(), new java.lang.String(), 0, true, true, true, true);
        // TBD - validate results
    }

    public void testIGSDIBaseDrugIdentifier1WSDL() throws Exception {
        javax.xml.rpc.ServiceFactory serviceFactory = javax.xml.rpc.ServiceFactory.newInstance();
        java.net.URL url = new java.net.URL(new com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.GSDIBaseDrugIdentifierLocator().getIGSDIBaseDrugIdentifier1Address() + "?WSDL");
        javax.xml.rpc.Service service = serviceFactory.createService(url, new com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.GSDIBaseDrugIdentifierLocator().getServiceName());
        assertTrue(service != null);
    }

    public void test5IGSDIBaseDrugIdentifier1ListColors() throws Exception {
        com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.IGSDIBaseDrugIdentifier1Stub binding;
        try {
            binding = (com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.IGSDIBaseDrugIdentifier1Stub)
                          new com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.GSDIBaseDrugIdentifierLocator().getIGSDIBaseDrugIdentifier1();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugColor[] value = null;
        value = binding.listColors(new java.lang.String());
        // TBD - validate results
    }

    public void test6IGSDIBaseDrugIdentifier1ListShapes() throws Exception {
        com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.IGSDIBaseDrugIdentifier1Stub binding;
        try {
            binding = (com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.IGSDIBaseDrugIdentifier1Stub)
                          new com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.GSDIBaseDrugIdentifierLocator().getIGSDIBaseDrugIdentifier1();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugShape[] value = null;
        value = binding.listShapes(new java.lang.String());
        // TBD - validate results
    }

    public void test7IGSDIBaseDrugIdentifier1ListScorings() throws Exception {
        com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.IGSDIBaseDrugIdentifier1Stub binding;
        try {
            binding = (com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.IGSDIBaseDrugIdentifier1Stub)
                          new com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.GSDIBaseDrugIdentifierLocator().getIGSDIBaseDrugIdentifier1();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugScoring[] value = null;
        value = binding.listScorings(new java.lang.String());
        // TBD - validate results
    }

    public void test8IGSDIBaseDrugIdentifier1SearchDrugIdentifications() throws Exception {
        com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.IGSDIBaseDrugIdentifier1Stub binding;
        try {
            binding = (com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.IGSDIBaseDrugIdentifier1Stub)
                          new com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.GSDIBaseDrugIdentifierLocator().getIGSDIBaseDrugIdentifier1();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugIdentificationResult[] value = null;
        value = binding.searchDrugIdentifications(new java.lang.String(), 0, 0, new java.lang.String(), new java.lang.String(), 0, true, true, true, true);
        // TBD - validate results
    }

    public void testGSDIBaseDrugIdentifierSoap12WSDL() throws Exception {
        javax.xml.rpc.ServiceFactory serviceFactory = javax.xml.rpc.ServiceFactory.newInstance();
        java.net.URL url = new java.net.URL(new com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.GSDIBaseDrugIdentifierLocator().getGSDIBaseDrugIdentifierSoap12Address() + "?WSDL");
        javax.xml.rpc.Service service = serviceFactory.createService(url, new com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.GSDIBaseDrugIdentifierLocator().getServiceName());
        assertTrue(service != null);
    }

    public void testGSDIBaseDrugIdentifierSoapWSDL() throws Exception {
        javax.xml.rpc.ServiceFactory serviceFactory = javax.xml.rpc.ServiceFactory.newInstance();
        java.net.URL url = new java.net.URL(new com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.GSDIBaseDrugIdentifierLocator().getGSDIBaseDrugIdentifierSoapAddress() + "?WSDL");
        javax.xml.rpc.Service service = serviceFactory.createService(url, new com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.GSDIBaseDrugIdentifierLocator().getServiceName());
        assertTrue(service != null);
    }

}
