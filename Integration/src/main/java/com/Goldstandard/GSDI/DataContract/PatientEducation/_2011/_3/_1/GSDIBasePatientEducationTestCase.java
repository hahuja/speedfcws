/**
 * GSDIBasePatientEducationTestCase.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1;

public class GSDIBasePatientEducationTestCase extends junit.framework.TestCase {
    public GSDIBasePatientEducationTestCase(java.lang.String name) {
        super(name);
    }

    public void testIGSDIBasePatientEducation1WSDL() throws Exception {
        javax.xml.rpc.ServiceFactory serviceFactory = javax.xml.rpc.ServiceFactory.newInstance();
        java.net.URL url = new java.net.URL(new com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.GSDIBasePatientEducationLocator().getIGSDIBasePatientEducation1Address() + "?WSDL");
        javax.xml.rpc.Service service = serviceFactory.createService(url, new com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.GSDIBasePatientEducationLocator().getServiceName());
        assertTrue(service != null);
    }

    public void test1IGSDIBasePatientEducation1GetSheetContent() throws Exception {
        com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.IGSDIBasePatientEducation1Stub binding;
        try {
            binding = (com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.IGSDIBasePatientEducation1Stub)
                          new com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.GSDIBasePatientEducationLocator().getIGSDIBasePatientEducation1();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.PatientEducationType[] value = null;
        value = binding.getSheetContent(new java.lang.String(), 0, com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.ProductSearchType.CPNUM, new java.lang.String(), com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.SectionName.Description);
        // TBD - validate results
    }

    public void test2IGSDIBasePatientEducation1ListLanguageEntitlements() throws Exception {
        com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.IGSDIBasePatientEducation1Stub binding;
        try {
            binding = (com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.IGSDIBasePatientEducation1Stub)
                          new com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.GSDIBasePatientEducationLocator().getIGSDIBasePatientEducation1();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.PatientEducationLanguageEntitlement[] value = null;
        value = binding.listLanguageEntitlements(new java.lang.String());
        // TBD - validate results
    }

    public void test3IGSDIBasePatientEducation1ListUpdatedPES() throws Exception {
        com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.IGSDIBasePatientEducation1Stub binding;
        try {
            binding = (com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.IGSDIBasePatientEducation1Stub)
                          new com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.GSDIBasePatientEducationLocator().getIGSDIBasePatientEducation1();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.SheetTitle[] value = null;
        value = binding.listUpdatedPES(new java.lang.String(), com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.Language.English, java.util.Calendar.getInstance(), java.util.Calendar.getInstance());
        // TBD - validate results
    }

    public void test4IGSDIBasePatientEducation1SearchForDrugNames() throws Exception {
        com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.IGSDIBasePatientEducation1Stub binding;
        try {
            binding = (com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.IGSDIBasePatientEducation1Stub)
                          new com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.GSDIBasePatientEducationLocator().getIGSDIBasePatientEducation1();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.Goldstandard.GSDI.DataContract._2011._3._1.DrugNameSearchResult value = null;
        value = binding.searchForDrugNames(new java.lang.String(), new java.lang.String(), com.Goldstandard.GSDI.DataContract._2011._3._1.TrademarkGenericEnum.Trademark);
        // TBD - validate results
    }

    public void test5IGSDIBasePatientEducation1SearchForRepresentativeMarketedProductIdPES() throws Exception {
        com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.IGSDIBasePatientEducation1Stub binding;
        try {
            binding = (com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.IGSDIBasePatientEducation1Stub)
                          new com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.GSDIBasePatientEducationLocator().getIGSDIBasePatientEducation1();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.ProductSearchResult value = null;
        value = binding.searchForRepresentativeMarketedProductIdPES(new java.lang.String(), new java.lang.String());
        // TBD - validate results
    }

    public void test6IGSDIBasePatientEducation1SearchForSpecificDrugPES() throws Exception {
        com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.IGSDIBasePatientEducation1Stub binding;
        try {
            binding = (com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.IGSDIBasePatientEducation1Stub)
                          new com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.GSDIBasePatientEducationLocator().getIGSDIBasePatientEducation1();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.ProductSpecificSearchResult value = null;
        value = binding.searchForSpecificDrugPES(new java.lang.String(), com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.ProductType.CPNUM, new java.lang.String());
        // TBD - validate results
    }

    public void testGSDIBasePatientEducationSoapWSDL() throws Exception {
        javax.xml.rpc.ServiceFactory serviceFactory = javax.xml.rpc.ServiceFactory.newInstance();
        java.net.URL url = new java.net.URL(new com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.GSDIBasePatientEducationLocator().getGSDIBasePatientEducationSoapAddress() + "?WSDL");
        javax.xml.rpc.Service service = serviceFactory.createService(url, new com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.GSDIBasePatientEducationLocator().getServiceName());
        assertTrue(service != null);
    }

    public void testIGSDIBasePatientEducationWSDL() throws Exception {
        javax.xml.rpc.ServiceFactory serviceFactory = javax.xml.rpc.ServiceFactory.newInstance();
        java.net.URL url = new java.net.URL(new com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.GSDIBasePatientEducationLocator().getIGSDIBasePatientEducationAddress() + "?WSDL");
        javax.xml.rpc.Service service = serviceFactory.createService(url, new com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.GSDIBasePatientEducationLocator().getServiceName());
        assertTrue(service != null);
    }

    public void test7IGSDIBasePatientEducationGetSheetContent() throws Exception {
        com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.IGSDIBasePatientEducationStub binding;
        try {
            binding = (com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.IGSDIBasePatientEducationStub)
                          new com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.GSDIBasePatientEducationLocator().getIGSDIBasePatientEducation();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.PatientEducationType[] value = null;
        value = binding.getSheetContent(new java.lang.String(), 0, com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.ProductSearchType.CPNUM, new java.lang.String(), com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.SectionName.Description);
        // TBD - validate results
    }

    public void test8IGSDIBasePatientEducationListLanguageEntitlements() throws Exception {
        com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.IGSDIBasePatientEducationStub binding;
        try {
            binding = (com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.IGSDIBasePatientEducationStub)
                          new com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.GSDIBasePatientEducationLocator().getIGSDIBasePatientEducation();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.PatientEducationLanguageEntitlement[] value = null;
        value = binding.listLanguageEntitlements(new java.lang.String());
        // TBD - validate results
    }

    public void test9IGSDIBasePatientEducationListUpdatedPES() throws Exception {
        com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.IGSDIBasePatientEducationStub binding;
        try {
            binding = (com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.IGSDIBasePatientEducationStub)
                          new com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.GSDIBasePatientEducationLocator().getIGSDIBasePatientEducation();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.SheetTitle[] value = null;
        value = binding.listUpdatedPES(new java.lang.String(), com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.Language.English, java.util.Calendar.getInstance(), java.util.Calendar.getInstance());
        // TBD - validate results
    }

    public void test10IGSDIBasePatientEducationSearchForDrugNames() throws Exception {
        com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.IGSDIBasePatientEducationStub binding;
        try {
            binding = (com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.IGSDIBasePatientEducationStub)
                          new com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.GSDIBasePatientEducationLocator().getIGSDIBasePatientEducation();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.Goldstandard.GSDI.DataContract._2011._3._1.DrugNameSearchResult value = null;
        value = binding.searchForDrugNames(new java.lang.String(), new java.lang.String(), com.Goldstandard.GSDI.DataContract._2011._3._1.TrademarkGenericEnum.Trademark);
        // TBD - validate results
    }

    public void test11IGSDIBasePatientEducationSearchForRepresentativeMarketedProductIdPES() throws Exception {
        com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.IGSDIBasePatientEducationStub binding;
        try {
            binding = (com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.IGSDIBasePatientEducationStub)
                          new com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.GSDIBasePatientEducationLocator().getIGSDIBasePatientEducation();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.ProductSearchResult value = null;
        value = binding.searchForRepresentativeMarketedProductIdPES(new java.lang.String(), new java.lang.String());
        // TBD - validate results
    }

    public void test12IGSDIBasePatientEducationSearchForSpecificDrugPES() throws Exception {
        com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.IGSDIBasePatientEducationStub binding;
        try {
            binding = (com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.IGSDIBasePatientEducationStub)
                          new com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.GSDIBasePatientEducationLocator().getIGSDIBasePatientEducation();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.ProductSpecificSearchResult value = null;
        value = binding.searchForSpecificDrugPES(new java.lang.String(), com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.ProductType.CPNUM, new java.lang.String());
        // TBD - validate results
    }

    public void testGSDIBasePatientEducationSoap12WSDL() throws Exception {
        javax.xml.rpc.ServiceFactory serviceFactory = javax.xml.rpc.ServiceFactory.newInstance();
        java.net.URL url = new java.net.URL(new com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.GSDIBasePatientEducationLocator().getGSDIBasePatientEducationSoap12Address() + "?WSDL");
        javax.xml.rpc.Service service = serviceFactory.createService(url, new com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.GSDIBasePatientEducationLocator().getServiceName());
        assertTrue(service != null);
    }

}
