/**
 * ProductSpecificSearchResult.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1;

public class ProductSpecificSearchResult  implements java.io.Serializable {
    private java.lang.String searchInputTerm;

    private com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.ProductType searchInputType;

    private com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.SheetTitleAndAvailability[] sheetTitles;

    public ProductSpecificSearchResult() {
    }

    public ProductSpecificSearchResult(
           java.lang.String searchInputTerm,
           com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.ProductType searchInputType,
           com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.SheetTitleAndAvailability[] sheetTitles) {
           this.searchInputTerm = searchInputTerm;
           this.searchInputType = searchInputType;
           this.sheetTitles = sheetTitles;
    }


    /**
     * Gets the searchInputTerm value for this ProductSpecificSearchResult.
     * 
     * @return searchInputTerm
     */
    public java.lang.String getSearchInputTerm() {
        return searchInputTerm;
    }


    /**
     * Sets the searchInputTerm value for this ProductSpecificSearchResult.
     * 
     * @param searchInputTerm
     */
    public void setSearchInputTerm(java.lang.String searchInputTerm) {
        this.searchInputTerm = searchInputTerm;
    }


    /**
     * Gets the searchInputType value for this ProductSpecificSearchResult.
     * 
     * @return searchInputType
     */
    public com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.ProductType getSearchInputType() {
        return searchInputType;
    }


    /**
     * Sets the searchInputType value for this ProductSpecificSearchResult.
     * 
     * @param searchInputType
     */
    public void setSearchInputType(com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.ProductType searchInputType) {
        this.searchInputType = searchInputType;
    }


    /**
     * Gets the sheetTitles value for this ProductSpecificSearchResult.
     * 
     * @return sheetTitles
     */
    public com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.SheetTitleAndAvailability[] getSheetTitles() {
        return sheetTitles;
    }


    /**
     * Sets the sheetTitles value for this ProductSpecificSearchResult.
     * 
     * @param sheetTitles
     */
    public void setSheetTitles(com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.SheetTitleAndAvailability[] sheetTitles) {
        this.sheetTitles = sheetTitles;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ProductSpecificSearchResult)) return false;
        ProductSpecificSearchResult other = (ProductSpecificSearchResult) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.searchInputTerm==null && other.getSearchInputTerm()==null) || 
             (this.searchInputTerm!=null &&
              this.searchInputTerm.equals(other.getSearchInputTerm()))) &&
            ((this.searchInputType==null && other.getSearchInputType()==null) || 
             (this.searchInputType!=null &&
              this.searchInputType.equals(other.getSearchInputType()))) &&
            ((this.sheetTitles==null && other.getSheetTitles()==null) || 
             (this.sheetTitles!=null &&
              java.util.Arrays.equals(this.sheetTitles, other.getSheetTitles())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSearchInputTerm() != null) {
            _hashCode += getSearchInputTerm().hashCode();
        }
        if (getSearchInputType() != null) {
            _hashCode += getSearchInputType().hashCode();
        }
        if (getSheetTitles() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSheetTitles());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSheetTitles(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ProductSpecificSearchResult.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "ProductSpecificSearchResult"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("searchInputTerm");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "SearchInputTerm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("searchInputType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "SearchInputType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "ProductType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sheetTitles");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "SheetTitles"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "SheetTitleAndAvailability"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "SheetTitleAndAvailability"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
