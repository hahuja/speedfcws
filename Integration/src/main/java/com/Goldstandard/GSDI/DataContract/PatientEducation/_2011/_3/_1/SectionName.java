/**
 * SectionName.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1;

public class SectionName implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected SectionName(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _Description = "Description";
    public static final java.lang.String _Contraindications = "Contraindications";
    public static final java.lang.String _Administration = "Administration";
    public static final java.lang.String _Missed = "Missed";
    public static final java.lang.String _Interactions = "Interactions";
    public static final java.lang.String _Monitoring = "Monitoring";
    public static final java.lang.String _SideEffects = "SideEffects";
    public static final java.lang.String _Storage = "Storage";
    public static final java.lang.String _All = "All";
    public static final SectionName Description = new SectionName(_Description);
    public static final SectionName Contraindications = new SectionName(_Contraindications);
    public static final SectionName Administration = new SectionName(_Administration);
    public static final SectionName Missed = new SectionName(_Missed);
    public static final SectionName Interactions = new SectionName(_Interactions);
    public static final SectionName Monitoring = new SectionName(_Monitoring);
    public static final SectionName SideEffects = new SectionName(_SideEffects);
    public static final SectionName Storage = new SectionName(_Storage);
    public static final SectionName All = new SectionName(_All);
    public java.lang.String getValue() { return _value_;}
    public static SectionName fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        SectionName enumeration = (SectionName)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static SectionName fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SectionName.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/PatientEducation/2011/3/1", "SectionName"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
