/**
 * ProductImageDetail.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.Goldstandard.GSDI.DataContract._2011._3._1;

public class ProductImageDetail  implements java.io.Serializable {
    private int productId;

    private java.lang.String productNameLong;

    private java.lang.String marketer;

    private java.lang.String offMarketDate;

    private com.Goldstandard.GSDI.DataContract._2011._3._1.IdentifierNameType[] activeIngredients;

    private com.Goldstandard.GSDI.DataContract._2011._3._1.ProductImage[] productImages;

    public ProductImageDetail() {
    }

    public ProductImageDetail(
           int productId,
           java.lang.String productNameLong,
           java.lang.String marketer,
           java.lang.String offMarketDate,
           com.Goldstandard.GSDI.DataContract._2011._3._1.IdentifierNameType[] activeIngredients,
           com.Goldstandard.GSDI.DataContract._2011._3._1.ProductImage[] productImages) {
           this.productId = productId;
           this.productNameLong = productNameLong;
           this.marketer = marketer;
           this.offMarketDate = offMarketDate;
           this.activeIngredients = activeIngredients;
           this.productImages = productImages;
    }


    /**
     * Gets the productId value for this ProductImageDetail.
     * 
     * @return productId
     */
    public int getProductId() {
        return productId;
    }


    /**
     * Sets the productId value for this ProductImageDetail.
     * 
     * @param productId
     */
    public void setProductId(int productId) {
        this.productId = productId;
    }


    /**
     * Gets the productNameLong value for this ProductImageDetail.
     * 
     * @return productNameLong
     */
    public java.lang.String getProductNameLong() {
        return productNameLong;
    }


    /**
     * Sets the productNameLong value for this ProductImageDetail.
     * 
     * @param productNameLong
     */
    public void setProductNameLong(java.lang.String productNameLong) {
        this.productNameLong = productNameLong;
    }


    /**
     * Gets the marketer value for this ProductImageDetail.
     * 
     * @return marketer
     */
    public java.lang.String getMarketer() {
        return marketer;
    }


    /**
     * Sets the marketer value for this ProductImageDetail.
     * 
     * @param marketer
     */
    public void setMarketer(java.lang.String marketer) {
        this.marketer = marketer;
    }


    /**
     * Gets the offMarketDate value for this ProductImageDetail.
     * 
     * @return offMarketDate
     */
    public java.lang.String getOffMarketDate() {
        return offMarketDate;
    }


    /**
     * Sets the offMarketDate value for this ProductImageDetail.
     * 
     * @param offMarketDate
     */
    public void setOffMarketDate(java.lang.String offMarketDate) {
        this.offMarketDate = offMarketDate;
    }


    /**
     * Gets the activeIngredients value for this ProductImageDetail.
     * 
     * @return activeIngredients
     */
    public com.Goldstandard.GSDI.DataContract._2011._3._1.IdentifierNameType[] getActiveIngredients() {
        return activeIngredients;
    }


    /**
     * Sets the activeIngredients value for this ProductImageDetail.
     * 
     * @param activeIngredients
     */
    public void setActiveIngredients(com.Goldstandard.GSDI.DataContract._2011._3._1.IdentifierNameType[] activeIngredients) {
        this.activeIngredients = activeIngredients;
    }


    /**
     * Gets the productImages value for this ProductImageDetail.
     * 
     * @return productImages
     */
    public com.Goldstandard.GSDI.DataContract._2011._3._1.ProductImage[] getProductImages() {
        return productImages;
    }


    /**
     * Sets the productImages value for this ProductImageDetail.
     * 
     * @param productImages
     */
    public void setProductImages(com.Goldstandard.GSDI.DataContract._2011._3._1.ProductImage[] productImages) {
        this.productImages = productImages;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ProductImageDetail)) return false;
        ProductImageDetail other = (ProductImageDetail) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.productId == other.getProductId() &&
            ((this.productNameLong==null && other.getProductNameLong()==null) || 
             (this.productNameLong!=null &&
              this.productNameLong.equals(other.getProductNameLong()))) &&
            ((this.marketer==null && other.getMarketer()==null) || 
             (this.marketer!=null &&
              this.marketer.equals(other.getMarketer()))) &&
            ((this.offMarketDate==null && other.getOffMarketDate()==null) || 
             (this.offMarketDate!=null &&
              this.offMarketDate.equals(other.getOffMarketDate()))) &&
            ((this.activeIngredients==null && other.getActiveIngredients()==null) || 
             (this.activeIngredients!=null &&
              java.util.Arrays.equals(this.activeIngredients, other.getActiveIngredients()))) &&
            ((this.productImages==null && other.getProductImages()==null) || 
             (this.productImages!=null &&
              java.util.Arrays.equals(this.productImages, other.getProductImages())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getProductId();
        if (getProductNameLong() != null) {
            _hashCode += getProductNameLong().hashCode();
        }
        if (getMarketer() != null) {
            _hashCode += getMarketer().hashCode();
        }
        if (getOffMarketDate() != null) {
            _hashCode += getOffMarketDate().hashCode();
        }
        if (getActiveIngredients() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getActiveIngredients());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getActiveIngredients(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getProductImages() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getProductImages());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getProductImages(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ProductImageDetail.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "ProductImageDetail"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "ProductId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productNameLong");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "ProductNameLong"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("marketer");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "Marketer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("offMarketDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "OffMarketDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("activeIngredients");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "ActiveIngredients"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "IdentifierNameType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "IdentifierNameType"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productImages");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "ProductImages"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "ProductImage"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/2011/3/1", "ProductImage"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
