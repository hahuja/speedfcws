/**
 * ConsumerInteractionReport.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1;

public class ConsumerInteractionReport  implements java.io.Serializable {
    private com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.InteractionProductType[] product;

    private com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.InteractionPackageType[] _package;

    private com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.MarketedSpecificProductType[] marketedProduct;

    private com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.MarketedSpecificProductType[] representativeMarketedProduct;

    public ConsumerInteractionReport() {
    }

    public ConsumerInteractionReport(
           com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.InteractionProductType[] product,
           com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.InteractionPackageType[] _package,
           com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.MarketedSpecificProductType[] marketedProduct,
           com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.MarketedSpecificProductType[] representativeMarketedProduct) {
           this.product = product;
           this._package = _package;
           this.marketedProduct = marketedProduct;
           this.representativeMarketedProduct = representativeMarketedProduct;
    }


    /**
     * Gets the product value for this ConsumerInteractionReport.
     * 
     * @return product
     */
    public com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.InteractionProductType[] getProduct() {
        return product;
    }


    /**
     * Sets the product value for this ConsumerInteractionReport.
     * 
     * @param product
     */
    public void setProduct(com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.InteractionProductType[] product) {
        this.product = product;
    }


    /**
     * Gets the _package value for this ConsumerInteractionReport.
     * 
     * @return _package
     */
    public com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.InteractionPackageType[] get_package() {
        return _package;
    }


    /**
     * Sets the _package value for this ConsumerInteractionReport.
     * 
     * @param _package
     */
    public void set_package(com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.InteractionPackageType[] _package) {
        this._package = _package;
    }


    /**
     * Gets the marketedProduct value for this ConsumerInteractionReport.
     * 
     * @return marketedProduct
     */
    public com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.MarketedSpecificProductType[] getMarketedProduct() {
        return marketedProduct;
    }


    /**
     * Sets the marketedProduct value for this ConsumerInteractionReport.
     * 
     * @param marketedProduct
     */
    public void setMarketedProduct(com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.MarketedSpecificProductType[] marketedProduct) {
        this.marketedProduct = marketedProduct;
    }


    /**
     * Gets the representativeMarketedProduct value for this ConsumerInteractionReport.
     * 
     * @return representativeMarketedProduct
     */
    public com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.MarketedSpecificProductType[] getRepresentativeMarketedProduct() {
        return representativeMarketedProduct;
    }


    /**
     * Sets the representativeMarketedProduct value for this ConsumerInteractionReport.
     * 
     * @param representativeMarketedProduct
     */
    public void setRepresentativeMarketedProduct(com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.MarketedSpecificProductType[] representativeMarketedProduct) {
        this.representativeMarketedProduct = representativeMarketedProduct;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ConsumerInteractionReport)) return false;
        ConsumerInteractionReport other = (ConsumerInteractionReport) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.product==null && other.getProduct()==null) || 
             (this.product!=null &&
              java.util.Arrays.equals(this.product, other.getProduct()))) &&
            ((this._package==null && other.get_package()==null) || 
             (this._package!=null &&
              java.util.Arrays.equals(this._package, other.get_package()))) &&
            ((this.marketedProduct==null && other.getMarketedProduct()==null) || 
             (this.marketedProduct!=null &&
              java.util.Arrays.equals(this.marketedProduct, other.getMarketedProduct()))) &&
            ((this.representativeMarketedProduct==null && other.getRepresentativeMarketedProduct()==null) || 
             (this.representativeMarketedProduct!=null &&
              java.util.Arrays.equals(this.representativeMarketedProduct, other.getRepresentativeMarketedProduct())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getProduct() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getProduct());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getProduct(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (get_package() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(get_package());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(get_package(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getMarketedProduct() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getMarketedProduct());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getMarketedProduct(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getRepresentativeMarketedProduct() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getRepresentativeMarketedProduct());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getRepresentativeMarketedProduct(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ConsumerInteractionReport.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "ConsumerInteractionReport"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "Product"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "InteractionProductType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "InteractionProductType"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("_package");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "Package"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "InteractionPackageType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "InteractionPackageType"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("marketedProduct");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "MarketedProduct"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "MarketedSpecificProductType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "MarketedSpecificProductType"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("representativeMarketedProduct");
        elemField.setXmlName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "RepresentativeMarketedProduct"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "MarketedSpecificProductType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/DataContract/ConsumerDrugInteraction/2011/3/1", "MarketedSpecificProductType"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
