/**
 * IGSDIBaseDrugIdentifier.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1;

public interface IGSDIBaseDrugIdentifier extends java.rmi.Remote {
    public com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugColor[] listColors(java.lang.String authenticationKey) throws java.rmi.RemoteException;
    public com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugShape[] listShapes(java.lang.String authenticationKey) throws java.rmi.RemoteException;
    public com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugScoring[] listScorings(java.lang.String authenticationKey) throws java.rmi.RemoteException;
    public com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugIdentificationResult[] searchDrugIdentifications(java.lang.String authenticationKey, int colorId, int shapeId, java.lang.String markingSide1, java.lang.String markingSide2, int scoringId, boolean includePrivateLabels, boolean excludeTablets, boolean excludeCapsules, boolean includeRepackaged) throws java.rmi.RemoteException;
}
