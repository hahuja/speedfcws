/**
 * IGSDIBasePhoto.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.Goldstandard.GSDI.Photo._2011._3._1;

import com.Goldstandard.GSDI.DataContract._2011._3._1.ProductSearchType;

public interface IGSDIBasePhoto extends java.rmi.Remote {
    public com.Goldstandard.GSDI.DataContract._2011._3._1.ProductImageResult searchForDrugProductPhotos(java.lang.String authenticationKey, ProductSearchType productSearchType, java.lang.String searchTerm) throws java.rmi.RemoteException;
}
