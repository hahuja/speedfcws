/**
 * GSDIBasePhotoLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.Goldstandard.GSDI.Photo._2011._3._1;

public class GSDIBasePhotoLocator extends org.apache.axis.client.Service implements com.Goldstandard.GSDI.Photo._2011._3._1.GSDIBasePhoto {

    public GSDIBasePhotoLocator() {
    }


    public GSDIBasePhotoLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public GSDIBasePhotoLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for GSDIBasePhotoSoap12
    private java.lang.String GSDIBasePhotoSoap12_address = "http://gsdi.goldstandard.com:8009/GSDIPhotoWS.asmx";

    public java.lang.String getGSDIBasePhotoSoap12Address() {
        return GSDIBasePhotoSoap12_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String GSDIBasePhotoSoap12WSDDServiceName = "GSDIBasePhotoSoap12";

    public java.lang.String getGSDIBasePhotoSoap12WSDDServiceName() {
        return GSDIBasePhotoSoap12WSDDServiceName;
    }

    public void setGSDIBasePhotoSoap12WSDDServiceName(java.lang.String name) {
        GSDIBasePhotoSoap12WSDDServiceName = name;
    }

    public com.Goldstandard.GSDI.Photo._2011._3._1.GSDIBasePhotoSoap12 getGSDIBasePhotoSoap12() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(GSDIBasePhotoSoap12_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getGSDIBasePhotoSoap12(endpoint);
    }

    public com.Goldstandard.GSDI.Photo._2011._3._1.GSDIBasePhotoSoap12 getGSDIBasePhotoSoap12(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.Goldstandard.GSDI.Photo._2011._3._1.GSDIBasePhotoSoap12Stub _stub = new com.Goldstandard.GSDI.Photo._2011._3._1.GSDIBasePhotoSoap12Stub(portAddress, this);
            _stub.setPortName(getGSDIBasePhotoSoap12WSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setGSDIBasePhotoSoap12EndpointAddress(java.lang.String address) {
        GSDIBasePhotoSoap12_address = address;
    }


    // Use to get a proxy class for IGSDIBasePhoto
    private java.lang.String IGSDIBasePhoto_address = "http://gsdi.goldstandard.com/GSDIPhotoWS.asmx";

    public java.lang.String getIGSDIBasePhotoAddress() {
        return IGSDIBasePhoto_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String IGSDIBasePhotoWSDDServiceName = "IGSDIBasePhoto";

    public java.lang.String getIGSDIBasePhotoWSDDServiceName() {
        return IGSDIBasePhotoWSDDServiceName;
    }

    public void setIGSDIBasePhotoWSDDServiceName(java.lang.String name) {
        IGSDIBasePhotoWSDDServiceName = name;
    }

    public com.Goldstandard.GSDI.Photo._2011._3._1.IGSDIBasePhoto getIGSDIBasePhoto() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(IGSDIBasePhoto_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getIGSDIBasePhoto(endpoint);
    }

    public com.Goldstandard.GSDI.Photo._2011._3._1.IGSDIBasePhoto getIGSDIBasePhoto(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.Goldstandard.GSDI.Photo._2011._3._1.IGSDIBasePhotoStub _stub = new com.Goldstandard.GSDI.Photo._2011._3._1.IGSDIBasePhotoStub(portAddress, this);
            _stub.setPortName(getIGSDIBasePhotoWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setIGSDIBasePhotoEndpointAddress(java.lang.String address) {
        IGSDIBasePhoto_address = address;
    }


    // Use to get a proxy class for GSDIBasePhotoSoap
    private java.lang.String GSDIBasePhotoSoap_address = "http://gsdi.goldstandard.com/GSDIPhotoWS.asmx";

    public java.lang.String getGSDIBasePhotoSoapAddress() {
        return GSDIBasePhotoSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String GSDIBasePhotoSoapWSDDServiceName = "GSDIBasePhotoSoap";

    public java.lang.String getGSDIBasePhotoSoapWSDDServiceName() {
        return GSDIBasePhotoSoapWSDDServiceName;
    }

    public void setGSDIBasePhotoSoapWSDDServiceName(java.lang.String name) {
        GSDIBasePhotoSoapWSDDServiceName = name;
    }

    public com.Goldstandard.GSDI.Photo._2011._3._1.GSDIBasePhotoSoap getGSDIBasePhotoSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(GSDIBasePhotoSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getGSDIBasePhotoSoap(endpoint);
    }

    public com.Goldstandard.GSDI.Photo._2011._3._1.GSDIBasePhotoSoap getGSDIBasePhotoSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.Goldstandard.GSDI.Photo._2011._3._1.GSDIBasePhotoSoapStub _stub = new com.Goldstandard.GSDI.Photo._2011._3._1.GSDIBasePhotoSoapStub(portAddress, this);
            _stub.setPortName(getGSDIBasePhotoSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setGSDIBasePhotoSoapEndpointAddress(java.lang.String address) {
        GSDIBasePhotoSoap_address = address;
    }


    // Use to get a proxy class for IGSDIBasePhoto1
    private java.lang.String IGSDIBasePhoto1_address = "http://gsdi.goldstandard.com/GSDIPhotoWS.asmx";

    public java.lang.String getIGSDIBasePhoto1Address() {
        return IGSDIBasePhoto1_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String IGSDIBasePhoto1WSDDServiceName = "IGSDIBasePhoto1";

    public java.lang.String getIGSDIBasePhoto1WSDDServiceName() {
        return IGSDIBasePhoto1WSDDServiceName;
    }

    public void setIGSDIBasePhoto1WSDDServiceName(java.lang.String name) {
        IGSDIBasePhoto1WSDDServiceName = name;
    }

    public com.Goldstandard.GSDI.Photo._2011._3._1.IGSDIBasePhoto getIGSDIBasePhoto1() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(IGSDIBasePhoto1_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getIGSDIBasePhoto1(endpoint);
    }

    public com.Goldstandard.GSDI.Photo._2011._3._1.IGSDIBasePhoto getIGSDIBasePhoto1(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.Goldstandard.GSDI.Photo._2011._3._1.IGSDIBasePhoto1Stub _stub = new com.Goldstandard.GSDI.Photo._2011._3._1.IGSDIBasePhoto1Stub(portAddress, this);
            _stub.setPortName(getIGSDIBasePhoto1WSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setIGSDIBasePhoto1EndpointAddress(java.lang.String address) {
        IGSDIBasePhoto1_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     * This service has multiple ports for a given interface;
     * the proxy implementation returned may be indeterminate.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.Goldstandard.GSDI.Photo._2011._3._1.GSDIBasePhotoSoap12.class.isAssignableFrom(serviceEndpointInterface)) {
                com.Goldstandard.GSDI.Photo._2011._3._1.GSDIBasePhotoSoap12Stub _stub = new com.Goldstandard.GSDI.Photo._2011._3._1.GSDIBasePhotoSoap12Stub(new java.net.URL(GSDIBasePhotoSoap12_address), this);
                _stub.setPortName(getGSDIBasePhotoSoap12WSDDServiceName());
                return _stub;
            }
            if (com.Goldstandard.GSDI.Photo._2011._3._1.IGSDIBasePhoto.class.isAssignableFrom(serviceEndpointInterface)) {
                com.Goldstandard.GSDI.Photo._2011._3._1.IGSDIBasePhotoStub _stub = new com.Goldstandard.GSDI.Photo._2011._3._1.IGSDIBasePhotoStub(new java.net.URL(IGSDIBasePhoto_address), this);
                _stub.setPortName(getIGSDIBasePhotoWSDDServiceName());
                return _stub;
            }
            if (com.Goldstandard.GSDI.Photo._2011._3._1.GSDIBasePhotoSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                com.Goldstandard.GSDI.Photo._2011._3._1.GSDIBasePhotoSoapStub _stub = new com.Goldstandard.GSDI.Photo._2011._3._1.GSDIBasePhotoSoapStub(new java.net.URL(GSDIBasePhotoSoap_address), this);
                _stub.setPortName(getGSDIBasePhotoSoapWSDDServiceName());
                return _stub;
            }
            if (com.Goldstandard.GSDI.Photo._2011._3._1.IGSDIBasePhoto.class.isAssignableFrom(serviceEndpointInterface)) {
                com.Goldstandard.GSDI.Photo._2011._3._1.IGSDIBasePhoto1Stub _stub = new com.Goldstandard.GSDI.Photo._2011._3._1.IGSDIBasePhoto1Stub(new java.net.URL(IGSDIBasePhoto1_address), this);
                _stub.setPortName(getIGSDIBasePhoto1WSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("GSDIBasePhotoSoap12".equals(inputPortName)) {
            return getGSDIBasePhotoSoap12();
        }
        else if ("IGSDIBasePhoto".equals(inputPortName)) {
            return getIGSDIBasePhoto();
        }
        else if ("GSDIBasePhotoSoap".equals(inputPortName)) {
            return getGSDIBasePhotoSoap();
        }
        else if ("IGSDIBasePhoto1".equals(inputPortName)) {
            return getIGSDIBasePhoto1();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/Photo/2011/3/1", "GSDIBasePhoto");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/Photo/2011/3/1", "GSDIBasePhotoSoap12"));
            ports.add(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/Photo/2011/3/1", "IGSDIBasePhoto"));
            ports.add(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/Photo/2011/3/1", "GSDIBasePhotoSoap"));
            ports.add(new javax.xml.namespace.QName("http://GSDI.Goldstandard.com/Photo/2011/3/1", "IGSDIBasePhoto1"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("GSDIBasePhotoSoap12".equals(portName)) {
            setGSDIBasePhotoSoap12EndpointAddress(address);
        }
        else 
if ("IGSDIBasePhoto".equals(portName)) {
            setIGSDIBasePhotoEndpointAddress(address);
        }
        else 
if ("GSDIBasePhotoSoap".equals(portName)) {
            setGSDIBasePhotoSoapEndpointAddress(address);
        }
        else 
if ("IGSDIBasePhoto1".equals(portName)) {
            setIGSDIBasePhoto1EndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
