/**
 * GSDIBasePhoto.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.Goldstandard.GSDI.Photo._2011._3._1;

public interface GSDIBasePhoto extends javax.xml.rpc.Service {
    public java.lang.String getGSDIBasePhotoSoap12Address();

    public com.Goldstandard.GSDI.Photo._2011._3._1.GSDIBasePhotoSoap12 getGSDIBasePhotoSoap12() throws javax.xml.rpc.ServiceException;

    public com.Goldstandard.GSDI.Photo._2011._3._1.GSDIBasePhotoSoap12 getGSDIBasePhotoSoap12(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
    public java.lang.String getIGSDIBasePhotoAddress();

    public com.Goldstandard.GSDI.Photo._2011._3._1.IGSDIBasePhoto getIGSDIBasePhoto() throws javax.xml.rpc.ServiceException;

    public com.Goldstandard.GSDI.Photo._2011._3._1.IGSDIBasePhoto getIGSDIBasePhoto(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
    public java.lang.String getGSDIBasePhotoSoapAddress();

    public com.Goldstandard.GSDI.Photo._2011._3._1.GSDIBasePhotoSoap getGSDIBasePhotoSoap() throws javax.xml.rpc.ServiceException;

    public com.Goldstandard.GSDI.Photo._2011._3._1.GSDIBasePhotoSoap getGSDIBasePhotoSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
    public java.lang.String getIGSDIBasePhoto1Address();

    public com.Goldstandard.GSDI.Photo._2011._3._1.IGSDIBasePhoto getIGSDIBasePhoto1() throws javax.xml.rpc.ServiceException;

    public com.Goldstandard.GSDI.Photo._2011._3._1.IGSDIBasePhoto getIGSDIBasePhoto1(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
