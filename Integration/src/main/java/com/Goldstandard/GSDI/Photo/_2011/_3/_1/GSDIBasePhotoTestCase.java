/**
 * GSDIBasePhotoTestCase.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.Goldstandard.GSDI.Photo._2011._3._1;

import com.Goldstandard.GSDI.DataContract._2011._3._1.ProductSearchType;

public class GSDIBasePhotoTestCase extends junit.framework.TestCase {
    public GSDIBasePhotoTestCase(java.lang.String name) {
        super(name);
    }

    public void testGSDIBasePhotoSoap12WSDL() throws Exception {
        javax.xml.rpc.ServiceFactory serviceFactory = javax.xml.rpc.ServiceFactory.newInstance();
        java.net.URL url = new java.net.URL(new com.Goldstandard.GSDI.Photo._2011._3._1.GSDIBasePhotoLocator().getGSDIBasePhotoSoap12Address() + "?WSDL");
        javax.xml.rpc.Service service = serviceFactory.createService(url, new com.Goldstandard.GSDI.Photo._2011._3._1.GSDIBasePhotoLocator().getServiceName());
        assertTrue(service != null);
    }

    public void testIGSDIBasePhotoWSDL() throws Exception {
        javax.xml.rpc.ServiceFactory serviceFactory = javax.xml.rpc.ServiceFactory.newInstance();
        java.net.URL url = new java.net.URL(new com.Goldstandard.GSDI.Photo._2011._3._1.GSDIBasePhotoLocator().getIGSDIBasePhotoAddress() + "?WSDL");
        javax.xml.rpc.Service service = serviceFactory.createService(url, new com.Goldstandard.GSDI.Photo._2011._3._1.GSDIBasePhotoLocator().getServiceName());
        assertTrue(service != null);
    }

    public void test1IGSDIBasePhotoSearchForDrugProductPhotos() throws Exception {
        com.Goldstandard.GSDI.Photo._2011._3._1.IGSDIBasePhotoStub binding;
        try {
            binding = (com.Goldstandard.GSDI.Photo._2011._3._1.IGSDIBasePhotoStub)
                          new com.Goldstandard.GSDI.Photo._2011._3._1.GSDIBasePhotoLocator().getIGSDIBasePhoto();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.Goldstandard.GSDI.DataContract._2011._3._1.ProductImageResult value = null;
        value = binding.searchForDrugProductPhotos(new java.lang.String(), ProductSearchType.DrugName, new java.lang.String());
        // TBD - validate results
    }

    public void testGSDIBasePhotoSoapWSDL() throws Exception {
        javax.xml.rpc.ServiceFactory serviceFactory = javax.xml.rpc.ServiceFactory.newInstance();
        java.net.URL url = new java.net.URL(new com.Goldstandard.GSDI.Photo._2011._3._1.GSDIBasePhotoLocator().getGSDIBasePhotoSoapAddress() + "?WSDL");
        javax.xml.rpc.Service service = serviceFactory.createService(url, new com.Goldstandard.GSDI.Photo._2011._3._1.GSDIBasePhotoLocator().getServiceName());
        assertTrue(service != null);
    }

    public void testIGSDIBasePhoto1WSDL() throws Exception {
        javax.xml.rpc.ServiceFactory serviceFactory = javax.xml.rpc.ServiceFactory.newInstance();
        java.net.URL url = new java.net.URL(new com.Goldstandard.GSDI.Photo._2011._3._1.GSDIBasePhotoLocator().getIGSDIBasePhoto1Address() + "?WSDL");
        javax.xml.rpc.Service service = serviceFactory.createService(url, new com.Goldstandard.GSDI.Photo._2011._3._1.GSDIBasePhotoLocator().getServiceName());
        assertTrue(service != null);
    }

    public void test2IGSDIBasePhoto1SearchForDrugProductPhotos() throws Exception {
        com.Goldstandard.GSDI.Photo._2011._3._1.IGSDIBasePhoto1Stub binding;
        try {
            binding = (com.Goldstandard.GSDI.Photo._2011._3._1.IGSDIBasePhoto1Stub)
                          new com.Goldstandard.GSDI.Photo._2011._3._1.GSDIBasePhotoLocator().getIGSDIBasePhoto1();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.Goldstandard.GSDI.DataContract._2011._3._1.ProductImageResult value = null;
        value = binding.searchForDrugProductPhotos(new java.lang.String(), ProductSearchType.DrugName, new java.lang.String());
        // TBD - validate results
    }

}
