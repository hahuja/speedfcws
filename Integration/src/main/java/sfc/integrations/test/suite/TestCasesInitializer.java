package sfc.integrations.test.suite;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

import junitx.ddtunit.DDTTestCase;

import org.apache.log4j.Logger;

import sfc.integrations.webservices.common.exception.HebBusinessException;
import sfc.integrations.webservices.common.services.GenericWebService;
import sfc.integrations.webservices.soa.manager.PharmacyServiceManager;
import sfc.integrations.webservices.soa.manager.PharmacyServiceManagerImpl;

import com.sfc.ws.test.suite.config.TestConfigLoader;
import com.sfc.ws.test.suite.config.TestConfiguration;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;


/**
 * This class used to initialize and run all the test cases related to single service id, so if system has more then one service ids than
 * same number of instance will be created of this class
 * @author Himkar
 *
 */
public class TestCasesInitializer extends DDTTestCase {
 
	private Logger logger = null;
	
	private String testConfigurationXMLPath = null;
	private String testCaseServiceId = null;
	
	private PharmacyServiceManager pharmacyServiceManager = null;
	private TestConfiguration testConfiguration = null;
	
	/**
	 * Initialize test case with null configuration
	 */
	public TestCasesInitializer(){
		this(null, null);
	}
	/**
	 * Initialize test case with testCaseServiceId and testConfigurationXMLPath
	 */
	public TestCasesInitializer(String testCaseServiceId, String testConfigurationXMLPath){
		super("testServiceWithNucleus");
		this.setTestCaseServiceId(testCaseServiceId);
		this.setTestConfigurationXMLPath(testConfigurationXMLPath);
		
	}
	/**
	 * Initialize test case with testMethodName, testCaseServiceId and testConfigurationXMLPath
	 */
	public TestCasesInitializer(String testMethodName, String testCaseServiceId, String testConfigurationXMLPath){
		super(testMethodName);
		this.setTestCaseServiceId(testCaseServiceId);
		this.setTestConfigurationXMLPath(testConfigurationXMLPath);
	}
	/**
	 * This method is used to initialize DDTunit testing framework to work. It parse xml file and extract all the test cases and run test method accordingly
	 */
	@Override
	protected void initContext() {
		initTestData(this.getTestConfigurationXMLPath(), this.getTestCaseServiceId());		
	}
	/**
	 * This method is used to perform test case. DDTunit framework call this method for each test case has been defined in configuration 
	 */
	public void testServiceWithNucleus() {
		this.setTestConfiguration((TestConfiguration)TestConfigLoader.resolveComponent("/com/sfc/ws/test/suite/config/TestConfiguration"));
		if(this.getTestConfiguration().isLoggingDebug()){
			this.getTestConfiguration().logDebug("TestCasesInitializer : testServiceWithNucleus : Start for - "+this.getTestCaseServiceId());
		}
		try {
			/**
			 *  Retrieve Operation name from this test case
			 */			
			String operationName = (String) getObject("methodName");				
			String requestCount = (String) getObject("requestCount");
			/**
			 *  Create request xml path for all the request required for this operation
			 */
			String[] totalRequestFiles = this.getTestConfiguration().createRequestXMLPath(this.getTestCaseServiceId(), operationName, requestCount);
			
			/**
			 * Retrieve object types for all the required request objects
			 */
			String requestClass = (String) getObject("class"); 
			String[] requestClasses= requestClass.split("-");			
			
			Object[] requestObjectList = new Object[totalRequestFiles.length];
			for(int i=0 ; i<totalRequestFiles.length;i++){					
				requestObjectList[i] = createTestCaseRequest(totalRequestFiles[i], requestClasses[i]);
			}				
			if(this.getTestConfiguration().isLoggingDebug()){
				this.getTestConfiguration().logDebug("TestCasesInitializer : testServiceWithNucleus : Request has been created : "+requestObjectList);
			}
			Object serviceResponse  = null;
			GenericWebService service = (GenericWebService)TestConfigLoader.resolveComponent("/sfc/integrations/webservices/common/services/"+this.getTestCaseServiceId());
			if(service != null){
				serviceResponse  = service.callService(this.getTestCaseServiceId(), operationName, requestObjectList);
			}else{
				if(this.getTestConfiguration().isLoggingDebug()){
					this.getTestConfiguration().logDebug("TestCasesInitializer : testServiceWithNucleus : Service object is null");
				}
			}
			
			if(this.getTestConfiguration().isLoggingDebug()){
				this.getTestConfiguration().logDebug("TestCasesInitializer : testServiceWithNucleus : Got service response: "+serviceResponse);
			}
			addObjectToAssert("responseClass", serviceResponse.getClass().toString());			
		} catch (HebBusinessException hebBusinessException) {
			if(this.getTestConfiguration().isLoggingDebug()){
				this.getTestConfiguration().logDebug("TestCasesInitializer : testServiceWithNucleus : something went wrong ");
				this.getTestConfiguration().logDebug("Fault Code :  "+hebBusinessException.getFaultCode());
				this.getTestConfiguration().logDebug("Fault Message :  "+hebBusinessException.getMessage());
				if(hebBusinessException.getTarget() != null){
					this.getTestConfiguration().logDebug("Target Exception Detail :  "+hebBusinessException.getTarget().toString());
				}
			}			
		}catch (Exception generalException) {
			if(this.getTestConfiguration().isLoggingDebug()){
				this.getTestConfiguration().logDebug("TestCasesInitializer : testServiceWithNucleus : something went wrong ");
				this.getTestConfiguration().logDebug("Fault Message :  "+generalException.getMessage());
			}
		}finally{
			TestConfigLoader.removeTestService(this.getTestCaseServiceId());
			TestConfigLoader.shutdownNucleus();
		}
		if(this.getTestConfiguration().isLoggingDebug()){
			this.getTestConfiguration().logDebug("TestCasesInitializer : testServiceWithNucleus : End for - "+this.getTestCaseServiceId());
		}
	}
	/**
	 * This method is used to create java request object from given xml path
	 * 
	 * @param requestFilePath - xml file path which need to be mapped with request class object
	 * @param requestClass - fully qualified class name
	 * @return  Object - request object
	 * @throws SpeedFCServiceBusinessException
	 */
	public Object createTestCaseRequest(String requestFilePath, String requestClass)throws HebBusinessException {
		if(this.getTestConfiguration().isLoggingDebug()){
			this.getTestConfiguration().logDebug("TestCasesInitializer : createTestCaseRequest : Start for - "+this.getTestCaseServiceId());
		}
		/**
		 * Create parser to convert xml file to java object
		 */
		XStream xmlToJavaObjectConverter = new XStream(new DomDriver());
		BufferedReader filePathReader = null;
		Object request = null;
	
		try {
			/**
			 * Retrieve Class object from given qualified class name
			 */
			Class requestClassObject = Class.forName(requestClass);
			String requestClassName = requestClass;
			if(requestClass.lastIndexOf(".") > -1){
				requestClassName = requestClass.subSequence(requestClass.lastIndexOf(".")+1, requestClass.length()).toString();
			}	
			/**
			 * set aliases to map xml attribute with java objects
			 */
			xmlToJavaObjectConverter.alias(requestClassName, requestClassObject);			
			/**
			 * read request xml file
			 */
			filePathReader = new BufferedReader(new FileReader(requestFilePath));
			if(this.getTestConfiguration().isLoggingDebug()){
				this.getTestConfiguration().logDebug("TestCasesInitializer : createTestCaseRequest : request xml file loaded into reader object");
			}
			/**
			 * convert xml file to respective java object
			 */
			request = (Object) xmlToJavaObjectConverter.fromXML(filePathReader);
			if(this.getTestConfiguration().isLoggingDebug()){
				this.getTestConfiguration().logDebug("TestCasesInitializer : createTestCaseRequest : request object created - "+request);
			}
		} catch (FileNotFoundException fileNotFoundException) {
			if(this.getTestConfiguration().isLoggingDebug()){
				this.getTestConfiguration().logDebug("TestCasesInitializer : createTestCaseRequest : File can't be located at : "+requestFilePath);
			}
			throw new HebBusinessException("TestCasesInitializer : createTestCaseRequest : File can't be located at : "+requestFilePath, "10003", fileNotFoundException );
		}catch (ClassNotFoundException classNotFoundException) {
			if(this.getTestConfiguration().isLoggingDebug()){
				this.getTestConfiguration().logDebug("TestCasesInitializer : createTestCaseRequest : specified class can't be found : "+requestClass);
			}
			throw new HebBusinessException("TestCasesInitializer : createTestCaseRequest : specified class can't be found : "+requestClass, "10004", classNotFoundException);
		}finally{
			try{
				/**
				 * close file reader object to clean memory
				 */
				filePathReader.close();
			}catch(Exception exception){
				if(this.getTestConfiguration().isLoggingDebug()){
					this.getTestConfiguration().logDebug("TestCasesInitializer : createTestCaseRequest : Error while closing reader ");
				}
			}
		}
		if(this.getTestConfiguration().isLoggingDebug()){
			this.getTestConfiguration().logDebug("TestCasesInitializer : createTestCaseRequest : End for - "+this.getTestCaseServiceId());
		}
		return request;
	}
	/**
	 * @return the logger
	 */
	public Logger getLogger() {
		if(logger == null){
			logger = Logger.getLogger(this.getClass());
		}
		return logger;
	}

	/**
	 * @param logger the logger to set
	 */
	public void setLogger(Logger logger) {
		this.logger = logger;
	}
	
	/**
	 * @return the testConfigurationXMLPath
	 */
	public String getTestConfigurationXMLPath() {
		return testConfigurationXMLPath;
	}
	/**
	 * @param testConfigurationXMLPath the testConfigurationXMLPath to set
	 */
	public void setTestConfigurationXMLPath(String testConfigurationXMLPath) {
		this.testConfigurationXMLPath = testConfigurationXMLPath;
	}
	/**
	 * @return the testCaseServiceId
	 */
	public String getTestCaseServiceId() {
		return testCaseServiceId;
	}
	/**
	 * @param testCaseServiceId the testCaseServiceId to set
	 */
	public void setTestCaseServiceId(String testCaseServiceId) {
		this.testCaseServiceId = testCaseServiceId;
	}
	/**
	 * @return the pharmacyServiceManager
	 */
	public PharmacyServiceManager getPharmacyServiceManager() {
		if(pharmacyServiceManager == null){
			this.setPharmacyServiceManager((PharmacyServiceManagerImpl) TestConfigLoader.resolveComponent("/sfc/integrations/webservices/soa/manager/PharmacyServiceManager"));
		}
		return pharmacyServiceManager;
	}
	/**
	 * @param pharmacyServiceManager the pharmacyServiceManager to set
	 */
	public void setPharmacyServiceManager(
			PharmacyServiceManager pharmacyServiceManager) {
		this.pharmacyServiceManager = pharmacyServiceManager;
	}
	/**
	 * @return the testConfiguration
	 */
	public TestConfiguration getTestConfiguration() {
		return testConfiguration;
	}
	/**
	 * @param testConfiguration the testConfiguration to set
	 */
	public void setTestConfiguration(TestConfiguration testConfiguration) {
		this.testConfiguration = testConfiguration;
	}
		
}