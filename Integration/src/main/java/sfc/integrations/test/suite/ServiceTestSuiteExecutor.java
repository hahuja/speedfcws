package sfc.integrations.test.suite;

import java.util.List;
import java.util.Properties;

import junit.framework.Test;
import junit.framework.TestSuite;
import atg.core.util.StringUtils;

import com.sfc.ws.test.suite.config.TestConfigLoader;
import com.sfc.ws.test.suite.config.TestConfiguration;


/**
 * This class is used to initiate all test cases. for each configured service id, this class creates instance of TestCasesInitializer and put it into the pool of test cases suite
 * which finally execute all the test cases one by one.
 * @author Himkar
 *
 */
public class ServiceTestSuiteExecutor{
	
	/**
	 * This method is responsible to perform test case creation, and can be called directly from ant script, so you can run all the test cases either from ant or from eclipse 
	 * @return Test -  collection of test cases objects
	 */
	public static Test suite(){
		/**
		 * Test case holder, which hold all the references of TestCasesInitializer object to run one by one
		 */
		TestSuite suite = new TestSuite();	
		Properties properties = System.getProperties();
		String configPath = properties.getProperty("configPath");
		/**
		 * verify if all the configuration properties are in place to run ATG Dust framework
		 */
		if(StringUtils.isBlank(configPath)){
			/**
			 * set all the configuration properties to run ATG Dust framework
			 */
			TestConfigLoader.setDefaultSystemProperties();
		}		 
		try{
			/**
			 * Retrieve TestConfiguration component from running nucleus
			 */
			TestConfiguration testConfiguration = (TestConfiguration)TestConfigLoader.resolveComponent("/com/sfc/ws/test/suite/config/TestConfiguration");
			if(testConfiguration != null){
				if(testConfiguration.isLoggingDebug()){
					testConfiguration.logDebug("ServiceTestSuiteExecutor : suite : Start");
				}
				TestCasesInitializer testCasesInitializer = null;
				/**
				 * Retrieve list of service ids to create same number of test cases
				 */
				List<String> appliedServiceIds = testConfiguration.getAppliedServiceIds();
				String testConfigurationXMLPath = null;			
				for(String testCaseServiceId: appliedServiceIds)
				{
					/**
					 * Create test cases configuration file path, which is used to specify all the properties related to that test cases, like request, operation etc.
					 */
					testConfigurationXMLPath = testConfiguration.createResourceXMLPath(testCaseServiceId);
					if(testConfiguration.isLoggingDebug()){
						testConfiguration.logDebug("ServiceTestSuiteExecutor : suite : Initializing Test Cases for Service - "+testCaseServiceId+" XML Path - "+testConfigurationXMLPath);
					}
					/**
					 * Check if test case configuration is valid or not
					 */
					if(!StringUtils.isBlank(testCaseServiceId) && !StringUtils.isBlank(testConfigurationXMLPath)){
						/**
						 * Initialize test case with service id and xml file path
						 */
						testCasesInitializer = new TestCasesInitializer(testCaseServiceId, testConfigurationXMLPath);
						/**
						 * Add test case to the suite for batch execution
						 */
						suite.addTest(testCasesInitializer);
						/**
						 * Notify it to the nucleus for test case registered
						 */
						TestConfigLoader.addTestService(testCaseServiceId);
						if(testConfiguration.isLoggingDebug()){
							testConfiguration.logDebug("ServiceTestSuiteExecutor : suite : Initialized Test Cases for Service - "+testCaseServiceId+" XML Path - "+testConfigurationXMLPath);
						}
					}						
				}	
				if(testConfiguration.isLoggingDebug()){
					testConfiguration.logDebug("ServiceTestSuiteExecutor : suite : End");
				}				
			}	
		}finally{
			/**
			 * Notify Nucleus to shutdown if none test case are remain for execution
			 */
			TestConfigLoader.shutdownNucleus();
		}
			
		return suite;
	}	
}
