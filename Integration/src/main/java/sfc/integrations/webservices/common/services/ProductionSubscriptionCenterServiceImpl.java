package sfc.integrations.webservices.common.services;

import sfc.integrations.webservices.common.exception.HebBusinessException;

import com.heb.xmlns.ei.Salt3.SendMessageRequest;
import com.heb.xmlns.ei.Salt3.SendMessageResponse;
import com.heb.xmlns.ei.SubscriptionCenterService.GetSubscriptionStatus_Reply.GetSubscriptionStatus_Reply;
import com.heb.xmlns.ei.SubscriptionCenterService.GetSubscriptionStatus_Request.GetSubscriptionStatus_Request;
import com.heb.xmlns.ei.SubscriptionCenterService.ListSubscriptions_Reply.ListSubscriptions_Reply;
import com.heb.xmlns.ei.SubscriptionCenterService.ListSubscriptions_Request.ListSubscriptions_Request;
import com.heb.xmlns.ei.SubscriptionCenterService.ModifySubscriptions_Reply.ModifySubscriptions_Reply;
import com.heb.xmlns.ei.SubscriptionCenterService.ModifySubscriptions_Request.ModifySubscriptions_Request;
import com.heb.xmlns.ei.SubscriptionCenterService.Subscribe_Reply.Subscribe_Reply;
import com.heb.xmlns.ei.SubscriptionCenterService.Subscribe_Request.Subscribe_Request;
import com.heb.xmlns.ei.SubscriptionCenterService.Unsubscribe_Reply.Unsubscribe_Reply;
import com.heb.xmlns.ei.SubscriptionCenterService.Unsubscribe_Request.Unsubscribe_Request;
import com.heb.xmlns.ei.SubscriptionCenterService.VerifyMobileDirectoryNumber_Reply.VerifyMobileDirectoryNumber_Reply;
import com.heb.xmlns.ei.SubscriptionCenterService.VerifyMobileDirectoryNumber_Request.VerifyMobileDirectoryNumber_Request;

public class ProductionSubscriptionCenterServiceImpl extends GenericWebServiceImpl implements
		ProductionSubscriptionCenterService {

	@Override
	public Object callService(Object serviceName, Object operationName,
			Object[] requestParams) throws HebBusinessException {
		return super.callService(serviceName, operationName, requestParams);
	}

	@Override
	public GetSubscriptionStatus_Reply getSubscriptionStatus(
			GetSubscriptionStatus_Request getSubscriptionStatus_Request)
			throws HebBusinessException {
		String operationName = "getSubscriptionStatus";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{getSubscriptionStatus_Request});
		if(reply instanceof GetSubscriptionStatus_Reply){
			return (GetSubscriptionStatus_Reply) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+GetSubscriptionStatus_Reply.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public Subscribe_Reply subscribe(Subscribe_Request subscribe_Request)
			throws HebBusinessException {
		String operationName = "subscribe";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{subscribe_Request});
		if(reply instanceof Subscribe_Reply){
			return (Subscribe_Reply) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+Subscribe_Reply.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public ListSubscriptions_Reply listSubscriptions(
			ListSubscriptions_Request listSubscriptions_Request)
			throws HebBusinessException {
		String operationName = "listSubscriptions";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{listSubscriptions_Request});
		if(reply instanceof ListSubscriptions_Reply){
			return (ListSubscriptions_Reply) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+ListSubscriptions_Reply.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public VerifyMobileDirectoryNumber_Reply verifyMobileDirectoryNumber(
			VerifyMobileDirectoryNumber_Request verifyMobileDirectoryNumber_Request)
			throws HebBusinessException {
		String operationName = "verifyMobileDirectoryNumber";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{verifyMobileDirectoryNumber_Request});
		if(reply instanceof VerifyMobileDirectoryNumber_Reply){
			return (VerifyMobileDirectoryNumber_Reply) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+VerifyMobileDirectoryNumber_Reply.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public ModifySubscriptions_Reply modifySubscriptions(
			ModifySubscriptions_Request modifySubscriptions_Request)
			throws HebBusinessException {
		String operationName = "modifySubscriptions";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{modifySubscriptions_Request});
		if(reply instanceof ModifySubscriptions_Reply){
			return (ModifySubscriptions_Reply) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+ModifySubscriptions_Reply.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public Unsubscribe_Reply unsubscribe(Unsubscribe_Request unsubscribe_Request)
			throws HebBusinessException {
		String operationName = "unsubscribe";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{unsubscribe_Request});
		if(reply instanceof Unsubscribe_Reply){
			return (Unsubscribe_Reply) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+Unsubscribe_Reply.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public SendMessageResponse sendAlert(SendMessageRequest sendAlert_Request)
			throws HebBusinessException {
		String operationName = "sendAlert";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{sendAlert_Request});
		if(reply instanceof SendMessageResponse){
			return (SendMessageResponse) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+SendMessageResponse.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

}
