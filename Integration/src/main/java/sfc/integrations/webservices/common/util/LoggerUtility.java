package sfc.integrations.webservices.common.util;

import sfc.integrations.webservices.common.conf.BaseWebServiceConfiguration;


public interface LoggerUtility {
	
	public void setWebServiceConfiguration(BaseWebServiceConfiguration webServiceConfiguration);
	
	public void logMessage(Object logMessage);
	public void logMessage(Object logMessage, boolean flag);	
	public void logMessage(String messagePrefix, Object logMessage);
	public void logMessage(String messagePrefix, Object logMessage, boolean flag);
	public void logMessage(String messagePrefix, Object logMessage, Throwable exception);
	public void logMessage(String messagePrefix, Object logMessage, Throwable exception, boolean flag);	
	
	public void logError(Object logMessage);
	public void logError(Object logMessage, boolean flag);	
	public void logError(String messagePrefix, Object logMessage);
	public void logError(String messagePrefix, Object logMessage, boolean flag);
	public void logError(String messagePrefix, Object logMessage, Throwable exception);
	public void logError(String messagePrefix, Object logMessage, Throwable exception, boolean flag);	
	
	public void logRequestResponse(Object message);
	public void logRequestResponse(String message, Object request);
}
