package sfc.integrations.webservices.common.services;

import sfc.integrations.webservices.common.exception.HebBusinessException;

import com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugColor;
import com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugIdentificationResult;
import com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugScoring;
import com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugShape;

public interface GSDIDrugIdentifierService extends GenericWebService {

	public static String SERVICE_NAME="GSDIDrugIdentifierService";
	public DrugColor[] listColors(String authenticationKey) throws HebBusinessException;
    public DrugShape[] listShapes(String authenticationKey) throws HebBusinessException;
    public DrugScoring[] listScorings(String authenticationKey) throws HebBusinessException;
    public DrugIdentificationResult[] searchDrugIdentifications(String authenticationKey, int colorId, int shapeId, String markingSide1, String markingSide2, int scoringId, boolean includePrivateLabels, boolean excludeTablets, boolean excludeCapsules, boolean includeRepackaged) throws HebBusinessException;
}
