package sfc.integrations.webservices.common.services;

import sfc.integrations.webservices.common.exception.HebBusinessException;

import com.heb.xmlns.ei.AssociateAccount_Reply.AssociateAccount_Reply;
import com.heb.xmlns.ei.AssociateAccount_Request.AssociateAccount_Request;
import com.heb.xmlns.ei.GetAccountInformation_Reply.GetAccountInformation_Reply;
import com.heb.xmlns.ei.GetAccountInformation_Request.GetAccountInformation_Request;
import com.heb.xmlns.ei.GetDccMemberAccountInformation_Reply.GetDccMemberAccountInformation_ReplyDccMemberAccountInformation;
import com.heb.xmlns.ei.GetDccMemberAccountInformation_Request.GetDccMemberAccountInformation_Request;
import com.heb.xmlns.ei.GetPcrStatementDetails_Reply.PCRCustomerDetails;
import com.heb.xmlns.ei.GetPcrStatementDetails_Request.GetPcrStatementDetails_Request;
import com.heb.xmlns.ei.GetPin_Reply.GetPin_Reply;
import com.heb.xmlns.ei.GetPin_Request.GetPin_Request;
import com.heb.xmlns.ei.GetPointsAndStoredValueAmount_Reply.GetPointsAndStoredValueAmount_Reply;
import com.heb.xmlns.ei.GetPointsAndStoredValueAmount_Request.GetPointsAndStoredValueAmount_Request;
import com.heb.xmlns.ei.GetReasonCodes_Request.GetReasonCodes_Request;
import com.heb.xmlns.ei.GetReceiptDetails_Reply.GetReceiptDetails_Reply;
import com.heb.xmlns.ei.GetReceiptDetails_Request.GetReceiptDetails_Request;
import com.heb.xmlns.ei.ProcessCard_Reply.ProcessCard_Reply;
import com.heb.xmlns.ei.ProcessCard_Request.ProcessCard_Request;
import com.heb.xmlns.ei.ProcessRetroRequest_Reply.ProcessRetroRequest_Reply;
import com.heb.xmlns.ei.ProcessRetroRequest_Request.ProcessRetroRequest_Request;
import com.heb.xmlns.ei.REASON_CODES.REASON_CODES;
import com.heb.xmlns.ei.UpdateProfile_Reply.UpdateProfile_Reply;
import com.heb.xmlns.ei.UpdateProfile_Request.UpdateProfile_Request;


public interface CustomerAccountService extends GenericWebService {

	public static String SERVICE_NAME="CustomerAccountService";
	public GetAccountInformation_Reply getAccountInformation(GetAccountInformation_Request getAccountInformation_Request) throws HebBusinessException;
    public GetReceiptDetails_Reply getReceiptDetails(GetReceiptDetails_Request getReceiptDetails_Request) throws HebBusinessException;
    public AssociateAccount_Reply associateAccount(AssociateAccount_Request associateAccount_Request) throws HebBusinessException;
    public ProcessRetroRequest_Reply processRetroRequest(ProcessRetroRequest_Request processRetroRequest_Request) throws HebBusinessException;
    public ProcessCard_Reply processCard(ProcessCard_Request processCard_Request) throws HebBusinessException;
    public REASON_CODES[] getReasonCodes(GetReasonCodes_Request getReasonCodes_Request) throws HebBusinessException;
    public UpdateProfile_Reply updateProfile(UpdateProfile_Request updateProfile_Request) throws HebBusinessException;
    public GetPin_Reply getPin(GetPin_Request getPin_Request) throws HebBusinessException;
    public GetDccMemberAccountInformation_ReplyDccMemberAccountInformation[] getDccMemberAccountInformation(GetDccMemberAccountInformation_Request getDccMemberAccountInformation_Request) throws HebBusinessException;
    public GetPointsAndStoredValueAmount_Reply getPointsAndStoredValueAmount(GetPointsAndStoredValueAmount_Request getPointsAndStoredValueAmount_Request) throws HebBusinessException;
    public PCRCustomerDetails getPcrStatementDetails(GetPcrStatementDetails_Request getPcrStatementDetails_Request) throws HebBusinessException;
	
}
