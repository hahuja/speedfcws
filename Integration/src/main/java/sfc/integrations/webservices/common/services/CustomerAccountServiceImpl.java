package sfc.integrations.webservices.common.services;

import sfc.integrations.webservices.common.exception.HebBusinessException;

import com.heb.xmlns.ei.AssociateAccount_Reply.AssociateAccount_Reply;
import com.heb.xmlns.ei.AssociateAccount_Request.AssociateAccount_Request;
import com.heb.xmlns.ei.GetAccountInformation_Reply.GetAccountInformation_Reply;
import com.heb.xmlns.ei.GetAccountInformation_Request.GetAccountInformation_Request;
import com.heb.xmlns.ei.GetDccMemberAccountInformation_Reply.GetDccMemberAccountInformation_ReplyDccMemberAccountInformation;
import com.heb.xmlns.ei.GetDccMemberAccountInformation_Request.GetDccMemberAccountInformation_Request;
import com.heb.xmlns.ei.GetPcrStatementDetails_Reply.PCRCustomerDetails;
import com.heb.xmlns.ei.GetPcrStatementDetails_Request.GetPcrStatementDetails_Request;
import com.heb.xmlns.ei.GetPin_Reply.GetPin_Reply;
import com.heb.xmlns.ei.GetPin_Request.GetPin_Request;
import com.heb.xmlns.ei.GetPointsAndStoredValueAmount_Reply.GetPointsAndStoredValueAmount_Reply;
import com.heb.xmlns.ei.GetPointsAndStoredValueAmount_Request.GetPointsAndStoredValueAmount_Request;
import com.heb.xmlns.ei.GetReasonCodes_Request.GetReasonCodes_Request;
import com.heb.xmlns.ei.GetReceiptDetails_Reply.GetReceiptDetails_Reply;
import com.heb.xmlns.ei.GetReceiptDetails_Request.GetReceiptDetails_Request;
import com.heb.xmlns.ei.ProcessCard_Reply.ProcessCard_Reply;
import com.heb.xmlns.ei.ProcessCard_Request.ProcessCard_Request;
import com.heb.xmlns.ei.ProcessRetroRequest_Reply.ProcessRetroRequest_Reply;
import com.heb.xmlns.ei.ProcessRetroRequest_Request.ProcessRetroRequest_Request;
import com.heb.xmlns.ei.REASON_CODES.REASON_CODES;
import com.heb.xmlns.ei.UpdateProfile_Reply.UpdateProfile_Reply;
import com.heb.xmlns.ei.UpdateProfile_Request.UpdateProfile_Request;

public class CustomerAccountServiceImpl extends GenericWebServiceImpl implements CustomerAccountService {

	@Override
	public Object callService(Object serviceName, Object operationName,
			Object[] requestParams) throws HebBusinessException {
		return  super.callService(serviceName, operationName, requestParams);
	}

	@Override
	public GetAccountInformation_Reply getAccountInformation(
			GetAccountInformation_Request getAccountInformation_Request)
			throws HebBusinessException {		
		String operationName = "getAccountInformation";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{getAccountInformation_Request});
		if(reply instanceof GetAccountInformation_Reply){
			return (GetAccountInformation_Reply) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+GetAccountInformation_Reply.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public GetReceiptDetails_Reply getReceiptDetails(
			GetReceiptDetails_Request getReceiptDetails_Request)
			throws HebBusinessException {
		String operationName = "getReceiptDetails";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{getReceiptDetails_Request});
		if(reply instanceof GetReceiptDetails_Reply){
			return (GetReceiptDetails_Reply) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+GetReceiptDetails_Reply.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public AssociateAccount_Reply associateAccount(
			AssociateAccount_Request associateAccount_Request)
			throws HebBusinessException {
		String operationName = "associateAccount";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{associateAccount_Request});
		if(reply instanceof AssociateAccount_Reply){
			return (AssociateAccount_Reply) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+AssociateAccount_Reply.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public ProcessRetroRequest_Reply processRetroRequest(
			ProcessRetroRequest_Request processRetroRequest_Request)
			throws HebBusinessException {
		String operationName = "processRetroRequest";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{processRetroRequest_Request});
		if(reply instanceof ProcessRetroRequest_Reply){
			return (ProcessRetroRequest_Reply) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+ProcessRetroRequest_Reply.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public ProcessCard_Reply processCard(ProcessCard_Request processCard_Request)
			throws HebBusinessException {
		String operationName = "processCard";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{processCard_Request});
		if(reply instanceof ProcessCard_Reply){
			return (ProcessCard_Reply) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+ProcessCard_Reply.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public REASON_CODES[] getReasonCodes(
			GetReasonCodes_Request getReasonCodes_Request)
			throws HebBusinessException {
		String operationName = "getReasonCodes";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{getReasonCodes_Request});
		if(reply instanceof REASON_CODES[]){
			return (REASON_CODES[]) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+REASON_CODES[].class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public UpdateProfile_Reply updateProfile(
			UpdateProfile_Request updateProfile_Request)
			throws HebBusinessException {
		String operationName = "updateProfile";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{updateProfile_Request});
		if(reply instanceof UpdateProfile_Reply){
			return (UpdateProfile_Reply) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+UpdateProfile_Reply.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public GetPin_Reply getPin(GetPin_Request getPin_Request)
			throws HebBusinessException {
		String operationName = "getPin";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{getPin_Request});
		if(reply instanceof GetPin_Reply){
			return (GetPin_Reply) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+GetPin_Reply.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public GetDccMemberAccountInformation_ReplyDccMemberAccountInformation[] getDccMemberAccountInformation(
			GetDccMemberAccountInformation_Request getDccMemberAccountInformation_Request)
			throws HebBusinessException {
		String operationName = "getDccMemberAccountInformation";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{getDccMemberAccountInformation_Request});
		if(reply instanceof GetDccMemberAccountInformation_ReplyDccMemberAccountInformation[]){
			return (GetDccMemberAccountInformation_ReplyDccMemberAccountInformation[]) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+GetDccMemberAccountInformation_ReplyDccMemberAccountInformation[].class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public GetPointsAndStoredValueAmount_Reply getPointsAndStoredValueAmount(
			GetPointsAndStoredValueAmount_Request getPointsAndStoredValueAmount_Request)
			throws HebBusinessException {
		String operationName = "getPointsAndStoredValueAmount";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{getPointsAndStoredValueAmount_Request});
		if(reply instanceof GetPointsAndStoredValueAmount_Reply){
			return (GetPointsAndStoredValueAmount_Reply) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+GetPointsAndStoredValueAmount_Reply.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public PCRCustomerDetails getPcrStatementDetails(
			GetPcrStatementDetails_Request getPcrStatementDetails_Request)
			throws HebBusinessException {
		String operationName = "getPcrStatementDetails";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{getPcrStatementDetails_Request});
		if(reply instanceof PCRCustomerDetails){
			return (PCRCustomerDetails) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+PCRCustomerDetails.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

}
