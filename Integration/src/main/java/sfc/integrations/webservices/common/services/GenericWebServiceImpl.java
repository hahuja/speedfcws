package sfc.integrations.webservices.common.services;

import sfc.integrations.webservices.common.exception.HebBusinessException;
import sfc.integrations.webservices.common.util.LoggerUtility;
import sfc.integrations.webservices.common.util.RandomStringGenerator;
import sfc.integrations.webservices.soa.communicator.GenericWebServiceCommunicator;
import atg.nucleus.GenericService;
/**
 * The class is responsible to call the actual web service after generating the request. Following are the steps it does for an operation:
a.	Generate the request
b.	Load the configuration from WebServiceConfiguration for a particular service, e.g. timeouts, endPointURL etc.
c.	Call web service utility to call the end point.
d.	Process the response to send it back to the client
e.	Logs the time taken with the request and response and a random number will be appended to both the request and response for a unique identification.

 * @return
 */
public class GenericWebServiceImpl extends GenericService implements GenericWebService {

	/**
	 * This variable is used to hold serviceCommunicator
	 */
	private GenericWebServiceCommunicator serviceCommunicator;
	
	/**
	 * This variable is used to hold loggerUtility
	 */
	private LoggerUtility loggerUtility;
	
	/**
	 * This method implementation is to call web service utility and process the response to send it back to the client
	 * @param serviceName - service name to be called
	 * @param operationName - operation name to be called 
	 * @param requestParams - request parameters
	 * @return - service response object
	 * @throws HebBusinessException - if there are any exception
	 */
	@Override
	public Object callService(Object serviceName, Object operationName, Object[] requestParams)
			throws HebBusinessException {
		
		Object serviceResponse = null;
		final String perfMonitorOpName = serviceName+"-"+operationName+"- callService";
		final String key = RandomStringGenerator.generateString();
		String logMessagePrelude = perfMonitorOpName + " " + key;
		
		this.getLoggerUtility().logMessage(logMessagePrelude, ": Start", this.isLoggingDebug());
		
		if(this.getServiceCommunicator() != null){
			serviceResponse = this.getServiceCommunicator().callServiceCommunicator(serviceName, operationName, requestParams);
			this.getLoggerUtility().logMessage(logMessagePrelude,": Got response from communicator "+serviceResponse, this.isLoggingDebug());
		}else{
			this.getLoggerUtility().logError(logMessagePrelude, ": No Communicator is registered with this service: ", this.isLoggingDebug());
			throw new HebBusinessException("No Communicator is registered with this service", "10022");
		}
		this.getLoggerUtility().logMessage(logMessagePrelude, ": End "+serviceResponse, this.isLoggingDebug());
		return serviceResponse;
	}

	/**
	 * @return the serviceCommunicator
	 */
	public GenericWebServiceCommunicator getServiceCommunicator() {
		return serviceCommunicator;
	}

	/**
	 * @param serviceCommunicator the serviceCommunicator to set
	 */
	public void setServiceCommunicator(
			GenericWebServiceCommunicator serviceCommunicator) {
		this.serviceCommunicator = serviceCommunicator;
	}

	public LoggerUtility getLoggerUtility() {
		return loggerUtility;
	}

	public void setLoggerUtility(LoggerUtility loggerUtility) {
		this.loggerUtility = loggerUtility;
	}

}
