package sfc.integrations.webservices.common.exception;

public class HebBusinessException extends Exception {

	private String faultCode;
	private Throwable target;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	public HebBusinessException(){
		this(null);
	}
	public HebBusinessException(String message){
		this(message, null, null);
	}
	public HebBusinessException(String message, Throwable target){
		this(message, null, target);
	}
	public HebBusinessException(String message, String faultCode){
		this(message, faultCode, null);
	}
	public HebBusinessException(String message, String faultCode, Throwable target){
		super(message);
		this.setFaultCode(faultCode);
		this.setTarget(target);
		
	}
	public String getFaultCode() {
		return faultCode;
	}
	public void setFaultCode(String faultCode) {
		this.faultCode = faultCode;
	}
	/**
	 * @return the target
	 */
	public Throwable getTarget() {
		return target;
	}
	/**
	 * @param target the target to set
	 */
	public void setTarget(Throwable target) {
		this.target = target;
	}
}
