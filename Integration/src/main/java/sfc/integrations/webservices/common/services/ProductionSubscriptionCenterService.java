package sfc.integrations.webservices.common.services;

import sfc.integrations.webservices.common.exception.HebBusinessException;

import com.heb.xmlns.ei.Salt3.SendMessageRequest;
import com.heb.xmlns.ei.Salt3.SendMessageResponse;
import com.heb.xmlns.ei.SubscriptionCenterService.GetSubscriptionStatus_Reply.GetSubscriptionStatus_Reply;
import com.heb.xmlns.ei.SubscriptionCenterService.GetSubscriptionStatus_Request.GetSubscriptionStatus_Request;
import com.heb.xmlns.ei.SubscriptionCenterService.ListSubscriptions_Reply.ListSubscriptions_Reply;
import com.heb.xmlns.ei.SubscriptionCenterService.ListSubscriptions_Request.ListSubscriptions_Request;
import com.heb.xmlns.ei.SubscriptionCenterService.ModifySubscriptions_Reply.ModifySubscriptions_Reply;
import com.heb.xmlns.ei.SubscriptionCenterService.ModifySubscriptions_Request.ModifySubscriptions_Request;
import com.heb.xmlns.ei.SubscriptionCenterService.Subscribe_Reply.Subscribe_Reply;
import com.heb.xmlns.ei.SubscriptionCenterService.Subscribe_Request.Subscribe_Request;
import com.heb.xmlns.ei.SubscriptionCenterService.Unsubscribe_Reply.Unsubscribe_Reply;
import com.heb.xmlns.ei.SubscriptionCenterService.Unsubscribe_Request.Unsubscribe_Request;
import com.heb.xmlns.ei.SubscriptionCenterService.VerifyMobileDirectoryNumber_Reply.VerifyMobileDirectoryNumber_Reply;
import com.heb.xmlns.ei.SubscriptionCenterService.VerifyMobileDirectoryNumber_Request.VerifyMobileDirectoryNumber_Request;

public interface ProductionSubscriptionCenterService extends GenericWebService {

	public static String SERVICE_NAME="ProductionSubscriptionCenterService";
	public GetSubscriptionStatus_Reply getSubscriptionStatus(GetSubscriptionStatus_Request getSubscriptionStatus_Request) throws HebBusinessException;
    public Subscribe_Reply subscribe(Subscribe_Request subscribe_Request) throws HebBusinessException;
    public ListSubscriptions_Reply listSubscriptions(ListSubscriptions_Request listSubscriptions_Request) throws HebBusinessException;
    public VerifyMobileDirectoryNumber_Reply verifyMobileDirectoryNumber(VerifyMobileDirectoryNumber_Request verifyMobileDirectoryNumber_Request) throws HebBusinessException;
    public ModifySubscriptions_Reply modifySubscriptions(ModifySubscriptions_Request modifySubscriptions_Request) throws HebBusinessException;
    public Unsubscribe_Reply unsubscribe(Unsubscribe_Request unsubscribe_Request) throws HebBusinessException;
    public SendMessageResponse sendAlert(SendMessageRequest sendAlert_Request) throws HebBusinessException;
}
