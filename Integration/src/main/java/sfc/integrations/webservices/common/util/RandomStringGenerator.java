package sfc.integrations.webservices.common.util;

import java.util.Random;

/**
 * This class is responsible for creating unique ids 
 * @author Himkar
 *
 */
public final class RandomStringGenerator {
	
	public final static String generateString() {
		int keyLengh = 31;
	    char[] key = new char[keyLengh];
	    Random random = new Random();
	    String symbols = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	    
	    for (int i = 0; i < keyLengh; i++) {
	    	key[i] = symbols.charAt(random.nextInt(symbols.length()));
	    }
	    return new String(key);
	}

}