package sfc.integrations.webservices.common.services;

import sfc.integrations.webservices.common.exception.HebBusinessException;

import com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.ConsumerInteractionReport;
import com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.SeverityLabelFilter;
import com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.SeverityRankingDescription;
import com.Goldstandard.GSDI.DataContract._2011._3._1.DrugNameSearchResult;
import com.Goldstandard.GSDI.DataContract._2011._3._1.PackageIdentifierType;
import com.Goldstandard.GSDI.DataContract._2011._3._1.PrescribedType;
import com.Goldstandard.GSDI.DataContract._2011._3._1.ProductIdentifierType;
import com.Goldstandard.GSDI.DataContract._2011._3._1.ProductSearchResult;
import com.Goldstandard.GSDI.DataContract._2011._3._1.TrademarkGenericEnum;

public interface GSDIConsDrugInteractionService extends GenericWebService {

	public static String SERVICE_NAME="GSDIConsDrugInteractionService";
	public DrugNameSearchResult searchForDrugNames(String authenticationKey, String searchTerm, TrademarkGenericEnum trademarkOrGeneric) throws HebBusinessException;
    public ProductSearchResult searchForRepresentativeMarketedProductId(String authenticationKey, String searchTerm) throws HebBusinessException;
    public SeverityRankingDescription[] getConsumerSeverityRankingDescriptions(String authenticationKey) throws HebBusinessException;
    public ConsumerInteractionReport getConsumerInteractions(String authenticationKey, ProductIdentifierType[] products, PackageIdentifierType[] packages, int[] marketedProductId, int[] representativeMarketedProductId, SeverityLabelFilter severityLabel, boolean includeCaffeine, boolean includeEnteral, boolean includeEthanol, boolean includeFood, boolean includeGrapefruit, boolean includeTobacco) throws HebBusinessException;
    public ConsumerInteractionReport getConsumerInteractionsV2(String authenticationKey, PrescribedType prescribed, PrescribedType prescribing, SeverityLabelFilter severityLabel, boolean includeCaffeine, boolean includeEnteral, boolean includeEthanol, boolean includeFood, boolean includeGrapefruit, boolean includeTobacco, boolean ignorePrescribed) throws HebBusinessException;
}
