package sfc.integrations.webservices.common.services;

import sfc.integrations.webservices.common.exception.HebBusinessException;


public interface GenericWebService {

	/**
	 * This method implementation is to call web service utility and process the response to send it back to the client
	 * @param serviceName - service name to be called
	 * @param operationName - operation name to be called 
	 * @param requestParams - request parameters
	 * @return - service response object
	 * @throws HebBusinessException - if there are any exception
	 */
	public Object callService(Object serviceName, Object operationName, Object[] requestParams)throws HebBusinessException;
}
