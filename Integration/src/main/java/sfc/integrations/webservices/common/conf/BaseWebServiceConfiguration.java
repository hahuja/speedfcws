package sfc.integrations.webservices.common.conf;

import java.util.Map;

import sfc.integrations.procesor.ServiceRequestProcessor;
import sfc.integrations.webservices.common.exception.HebBusinessException;
import sfc.integrations.webservices.common.util.LoggerUtility;
import sfc.integrations.webservices.common.util.RandomStringGenerator;
import atg.core.util.StringUtils;
import atg.nucleus.GenericService;

public class BaseWebServiceConfiguration extends GenericService {

	private int maxConnections;
	private int serviceTimeOut;	
	private boolean securedService;
	private String requestType;
	private String serviceType;
	
	private Map<String, String> serviceLocator;
	private Map<String, String> serviceStub;
	private Map<String, String> endPointURL;
	private Map<String, String> servicePORTMap;
	
	private Map<String, String> userNameMap;
	private Map<String, String> passwordMap;
	
	private Map<String, String> serviceProxyMap;
	private Map<String, String> serviceProxyPortMap;
	
	private ServiceRequestProcessor prePostRequestProcessor;
	
	private LoggerUtility loggerUtility;
	
	/**
	 * @return the serviceTimeOut
	 */
	public int getServiceTimeOut() {
		return serviceTimeOut;
	}
	/**
	 * @param serviceTimeOut the serviceTimeOut to set
	 */
	public void setServiceTimeOut(int serviceTimeOut) {
		this.serviceTimeOut = serviceTimeOut;
	}	
	
	/**
	 * @return the requestType
	 */
	public String getRequestType() {
		return requestType;
	}
	/**
	 * @param requestType the requestType to set
	 */
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	/**
	 * @return the serviceType
	 */
	public String getServiceType() {
		return serviceType;
	}
	/**
	 * @param serviceType the serviceType to set
	 */
	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}
	/**
	 * @return the serviceStub
	 */
	public Map<String, String> getServiceStub() {
		return serviceStub;
	}
	/**
	 * @param serviceStub the serviceStub to set
	 */
	public void setServiceStub(Map<String, String> serviceStub) {
		this.serviceStub = serviceStub;
	}
	/**
	 * @return the endPointURL
	 */
	public Map<String, String> getEndPointURL() {
		return endPointURL;
	}
	/**
	 * @param endPointURL the endPointURL to set
	 */
	public void setEndPointURL(Map<String, String> endPointURL) {
		this.endPointURL = endPointURL;
	}
	public String getServiceLocatorClassName(String serviceType)throws HebBusinessException{
		final String perfMonitorOpName = "BaseWebServiceConfiguration - getServiceLocatorClassName ";
		final String key = RandomStringGenerator.generateString();
		String logMessagePrelude = perfMonitorOpName + " " + key;
		
		String webService = null;
		this.getLoggerUtility().logMessage(logMessagePrelude, ": Start", this.isLoggingDebug());
		
		if(StringUtils.isBlank(serviceType)){
			this.getLoggerUtility().logMessage(logMessagePrelude, ": Service type is null", this.isLoggingDebug());
			throw new HebBusinessException("Service type can't be null or blank", "10013");
		}
		if(this.getServiceLocator() != null){
			this.getLoggerUtility().logMessage(logMessagePrelude, ": Service Locator collection found", this.isLoggingDebug());
			if(this.getServiceLocator().containsKey(serviceType)){
				webService = this.getServiceLocator().get(serviceType);
				this.getLoggerUtility().logMessage(logMessagePrelude, ": Service name : "+webService, this.isLoggingDebug());
			}
		}else{
			this.getLoggerUtility().logMessage(logMessagePrelude, ": None serviceType are registered in serviceType pool", this.isLoggingDebug());
			throw new HebBusinessException("None serviceType are registered in serviceType pool", "10014");
		}
		if(webService == null){
			this.getLoggerUtility().logMessage(logMessagePrelude, ": serviceType name : "+serviceType+" is not a valid serviceType name", this.isLoggingDebug());
			throw new HebBusinessException("serviceType name : "+serviceType+" is not a valid serviceType name", "10015");
		}
		this.getLoggerUtility().logMessage(logMessagePrelude, ": End", this.isLoggingDebug());
		return webService;
	}
	
	public String getServiceStubClassName(String serviceType)throws HebBusinessException{
		final String perfMonitorOpName = "BaseWebServiceConfiguration - getServiceStubClassName ";
		final String key = RandomStringGenerator.generateString();
		String logMessagePrelude = perfMonitorOpName + " " + key;
		
		String webService = null;
		this.getLoggerUtility().logMessage(logMessagePrelude, ": Start", this.isLoggingDebug());
		
		if(StringUtils.isBlank(serviceType)){
			this.getLoggerUtility().logMessage(logMessagePrelude, ": Service type is null", this.isLoggingDebug());
			throw new HebBusinessException("Service type can't be null or blank", "10013");
		}
		if(this.getServiceStub() != null){
			this.getLoggerUtility().logMessage(logMessagePrelude, ": Service Stub collection found", this.isLoggingDebug());
			if(this.getServiceStub().containsKey(serviceType)){
				webService = this.getServiceStub().get(serviceType);
				this.getLoggerUtility().logMessage(logMessagePrelude, ": Service name : "+webService, this.isLoggingDebug());
			}
		}else{
			this.getLoggerUtility().logMessage(logMessagePrelude, ": None serviceType are registered in serviceType pool", this.isLoggingDebug());
			throw new HebBusinessException("None serviceType are registered in serviceType pool", "10014");
		}
		if(webService == null){
			this.getLoggerUtility().logMessage(logMessagePrelude, ": serviceType name : "+serviceType+" is not a valid serviceType name", this.isLoggingDebug());
			throw new HebBusinessException("serviceType name : "+serviceType+" is not a valid serviceType name", "10015");
		}
		this.getLoggerUtility().logMessage(logMessagePrelude, ": End", this.isLoggingDebug());
		return webService;
	}
	
	public String getServiceEndPontURL(String serviceType)throws HebBusinessException{
		final String perfMonitorOpName = "BaseWebServiceConfiguration - getServiceEndPontURL ";
		final String key = RandomStringGenerator.generateString();
		String logMessagePrelude = perfMonitorOpName + " " + key;
		
		String webServiceEndPont = null;
		this.getLoggerUtility().logMessage(logMessagePrelude, ": Start", this.isLoggingDebug());
		if(StringUtils.isBlank(serviceType)){
			this.getLoggerUtility().logMessage(logMessagePrelude, ": Service type is null", this.isLoggingDebug());
			throw new HebBusinessException("Service type can't be null or blank", "10013");
		}
		if(this.getEndPointURL() != null){
			this.getLoggerUtility().logMessage(logMessagePrelude, ": Service end point collection found", this.isLoggingDebug());
			if(this.getEndPointURL().containsKey(serviceType)){
				webServiceEndPont = this.getEndPointURL().get(serviceType);	
				this.getLoggerUtility().logMessage(logMessagePrelude, ": Service end point : "+webServiceEndPont, this.isLoggingDebug());
			}
		}else{
			this.getLoggerUtility().logMessage(logMessagePrelude, ": None serviceType are registered in serviceType pool", this.isLoggingDebug());
			throw new HebBusinessException("None serviceType are registered in serviceType pool", "10014");
		}
		if(webServiceEndPont == null){
			this.getLoggerUtility().logMessage(logMessagePrelude, ": serviceType name : "+serviceType+" is not a valid serviceType name", this.isLoggingDebug());
			throw new HebBusinessException("serviceType name : "+serviceType+" is not a valid serviceType name", "10015");
		}
		this.getLoggerUtility().logMessage(logMessagePrelude, ": End ", this.isLoggingDebug());
		return webServiceEndPont;
	}
	public String getServicePORT(String serviceType)throws HebBusinessException{
		final String perfMonitorOpName = "BaseWebServiceConfiguration - getServicePORT ";
		final String key = RandomStringGenerator.generateString();
		String logMessagePrelude = perfMonitorOpName + " " + key;
		
		String webServicePort = null;
		this.getLoggerUtility().logMessage(logMessagePrelude, ": Start", this.isLoggingDebug());
		if(StringUtils.isBlank(serviceType)){
			this.getLoggerUtility().logMessage(logMessagePrelude, ": Service type is null", this.isLoggingDebug());
			throw new HebBusinessException("Service type can't be null or blank", "10013");
		}
		if(this.getServicePORTMap() != null){
			this.getLoggerUtility().logMessage(logMessagePrelude, ": Service Port collection found", this.isLoggingDebug());
			if(this.getServicePORTMap().containsKey(serviceType)){
				webServicePort = this.getServicePORTMap().get(serviceType);	
				this.getLoggerUtility().logMessage(logMessagePrelude, ": Service end point : "+webServicePort, this.isLoggingDebug());
			}
		}else{
			this.getLoggerUtility().logMessage(logMessagePrelude, ": None prot are registered in serviceType pool", this.isLoggingDebug());
			throw new HebBusinessException("None serviceType are registered in serviceType pool", "10014");
		}
		if(webServicePort == null){
			this.getLoggerUtility().logMessage(logMessagePrelude, ": serviceType name : "+serviceType+" is not a valid serviceType name", this.isLoggingDebug());
			throw new HebBusinessException("serviceType name : "+serviceType+" is not a valid serviceType name", "10015");
		}
		this.getLoggerUtility().logMessage(logMessagePrelude, ": End", this.isLoggingDebug());
		return webServicePort;
	}
	public String getServiceUserName(String serviceType)throws HebBusinessException{
		final String perfMonitorOpName = "BaseWebServiceConfiguration - getServiceUserName ";
		final String key = RandomStringGenerator.generateString();
		String logMessagePrelude = perfMonitorOpName + " " + key;
		
		String webServiceUserName = null;
		this.getLoggerUtility().logMessage(logMessagePrelude, ": Start", this.isLoggingDebug());
		if(StringUtils.isBlank(serviceType)){
			this.getLoggerUtility().logMessage(logMessagePrelude, ": Service type is null", this.isLoggingDebug());
			throw new HebBusinessException("Service type can't be null or blank", "10013");
		}
		if(this.getUserNameMap() != null){
			this.getLoggerUtility().logMessage(logMessagePrelude, ": Service User Name collection found", this.isLoggingDebug());
			if(this.getUserNameMap().containsKey(serviceType)){
				webServiceUserName = this.getUserNameMap().get(serviceType);
				this.getLoggerUtility().logMessage(logMessagePrelude, ": Service suer name : "+webServiceUserName, this.isLoggingDebug());
			}
		}else{
			this.getLoggerUtility().logMessage(logMessagePrelude, ": None userName are registered in serviceType pool", this.isLoggingDebug());
			throw new HebBusinessException("None serviceType are registered in serviceType pool", "10014");
		}
		if(webServiceUserName == null){
			this.getLoggerUtility().logMessage(logMessagePrelude, ": serviceType name : "+serviceType+" is not a valid serviceType name", this.isLoggingDebug());
			throw new HebBusinessException("serviceType name : "+serviceType+" is not a valid serviceType name", "10015");
		}
		this.getLoggerUtility().logMessage(logMessagePrelude, ": End", this.isLoggingDebug());
		return webServiceUserName;
	}
	public String getServicePassword(String serviceType)throws HebBusinessException{
		final String perfMonitorOpName = "BaseWebServiceConfiguration - getServicePassword ";
		final String key = RandomStringGenerator.generateString();
		String logMessagePrelude = perfMonitorOpName + " " + key;
		
		String webServicePassword = null;
		this.getLoggerUtility().logMessage(logMessagePrelude, ": Start", this.isLoggingDebug());
		if(StringUtils.isBlank(serviceType)){
			this.getLoggerUtility().logMessage(logMessagePrelude, ": Service Password collection found", this.isLoggingDebug());
			this.getLoggerUtility().logMessage(logMessagePrelude, ": Service type is null", this.isLoggingDebug());
			throw new HebBusinessException("Service type can't be null or blank", "10013");
		}
		if(this.getPasswordMap() != null){
			if(this.getPasswordMap().containsKey(serviceType)){
				webServicePassword = this.getPasswordMap().get(serviceType);
				this.getLoggerUtility().logMessage(logMessagePrelude, ": Service Password : "+webServicePassword, this.isLoggingDebug());
			}
		}else{
			this.getLoggerUtility().logMessage(logMessagePrelude, ": None password are registered in serviceType pool", this.isLoggingDebug());
			throw new HebBusinessException("None serviceType are registered in serviceType pool", "10014");
		}
		if(webServicePassword == null){
			this.getLoggerUtility().logMessage(logMessagePrelude, ": serviceType name : "+serviceType+" is not a valid serviceType name", this.isLoggingDebug());
			throw new HebBusinessException("serviceType name : "+serviceType+" is not a valid serviceType name", "10015");
		}
		this.getLoggerUtility().logMessage(logMessagePrelude, ": End", this.isLoggingDebug());
		return webServicePassword;
	}
	public String getServiceProxy(String serviceType)throws HebBusinessException{
		final String perfMonitorOpName = "BaseWebServiceConfiguration - getServiceProxy ";
		final String key = RandomStringGenerator.generateString();
		String logMessagePrelude = perfMonitorOpName + " " + key;
		
		String webServiceProxy = null;
		this.getLoggerUtility().logMessage(logMessagePrelude, ": Start", this.isLoggingDebug());
		if(StringUtils.isBlank(serviceType)){			
			this.getLoggerUtility().logMessage(logMessagePrelude, ": Service type is null", this.isLoggingDebug());
			throw new HebBusinessException("Service type can't be null or blank", "10013");
		}
		if(this.getServiceProxyMap() != null){
			this.getLoggerUtility().logMessage(logMessagePrelude, ": Service Proxy collection found", this.isLoggingDebug());
			if(this.getServiceProxyMap().containsKey(serviceType)){
				webServiceProxy = this.getServiceProxyMap().get(serviceType);	
				this.getLoggerUtility().logMessage(logMessagePrelude, ": Service end point : "+webServiceProxy, this.isLoggingDebug());
			}
		}else{
			this.getLoggerUtility().logMessage(logMessagePrelude, ": None proxy are registered in serviceType pool", this.isLoggingDebug());
			throw new HebBusinessException("None serviceType are registered in serviceType pool", "10014");
		}
		if(webServiceProxy == null){
			this.getLoggerUtility().logMessage(logMessagePrelude, ": serviceType name : "+serviceType+" is not a valid serviceType name", this.isLoggingDebug());
			throw new HebBusinessException("serviceType name : "+serviceType+" is not a valid serviceType name", "10015");
		}
		this.getLoggerUtility().logMessage(logMessagePrelude, ": End", this.isLoggingDebug());
		return webServiceProxy;
	}
	public String getServiceProxyPORT(String serviceType)throws HebBusinessException{
		final String perfMonitorOpName = "BaseWebServiceConfiguration - getServiceProxyPORT ";
		final String key = RandomStringGenerator.generateString();
		String logMessagePrelude = perfMonitorOpName + " " + key;
		
		String webServicePort = null;
		this.getLoggerUtility().logMessage(logMessagePrelude, ": Start", this.isLoggingDebug());
		if(StringUtils.isBlank(serviceType)){
			this.getLoggerUtility().logMessage(logMessagePrelude, ": Service type is null", this.isLoggingDebug());
			throw new HebBusinessException("Service type can't be null or blank", "10013");
		}
		if(this.getServiceProxyPortMap() != null){
			this.getLoggerUtility().logMessage(logMessagePrelude, ": Service Port collection found", this.isLoggingDebug());
			if(this.getServiceProxyPortMap().containsKey(serviceType)){
				webServicePort = this.getServiceProxyPortMap().get(serviceType);	
				this.getLoggerUtility().logMessage(logMessagePrelude, ": Service Proxy Port : "+webServicePort, this.isLoggingDebug());
			}
		}else{
			this.getLoggerUtility().logMessage(logMessagePrelude, ": None prot are registered in serviceType pool", this.isLoggingDebug());
			throw new HebBusinessException("None serviceType are registered in serviceType pool", "10014");
		}
		if(webServicePort == null){
			this.getLoggerUtility().logMessage(logMessagePrelude, ": serviceType name : "+serviceType+" is not a valid serviceType name", this.isLoggingDebug());
			throw new HebBusinessException("serviceType name : "+serviceType+" is not a valid serviceType name", "10015");
		}
		this.getLoggerUtility().logMessage(logMessagePrelude, ": End", this.isLoggingDebug());
		return webServicePort;
	}
	public String convertPassword(String password)throws Exception{
		return password;
	}
	/**
	 * @return the prePostRequestProcessor
	 */
	public ServiceRequestProcessor getPrePostRequestProcessor() {
		return prePostRequestProcessor;
	}
	/**
	 * @param prePostRequestProcessor the prePostRequestProcessor to set
	 */
	public void setPrePostRequestProcessor(
			ServiceRequestProcessor prePostRequestProcessor) {
		this.prePostRequestProcessor = prePostRequestProcessor;
	}
	
	/**
	 * @return the userNameMap
	 */
	public Map<String, String> getUserNameMap() {
		return userNameMap;
	}
	/**
	 * @param userNameMap the userNameMap to set
	 */
	public void setUserNameMap(Map<String, String> userNameMap) {
		this.userNameMap = userNameMap;
	}
	/**
	 * @return the passwordMap
	 */
	public Map<String, String> getPasswordMap() {
		return passwordMap;
	}
	/**
	 * @param passwordMap the passwordMap to set
	 */
	public void setPasswordMap(Map<String, String> passwordMap) {
		this.passwordMap = passwordMap;
	}
	/**
	 * @return the serviceProxyMap
	 */
	public Map<String, String> getServiceProxyMap() {
		return serviceProxyMap;
	}
	/**
	 * @param serviceProxyMap the serviceProxyMap to set
	 */
	public void setServiceProxyMap(Map<String, String> serviceProxyMap) {
		this.serviceProxyMap = serviceProxyMap;
	}
	/**
	 * @return the servicePORTMap
	 */
	public Map<String, String> getServicePORTMap() {
		return servicePORTMap;
	}
	/**
	 * @param servicePORTMap the servicePORTMap to set
	 */
	public void setServicePORTMap(Map<String, String> servicePORTMap) {
		this.servicePORTMap = servicePORTMap;
	}
	/**
	 * @return the securedService
	 */
	public boolean isSecuredService() {
		return securedService;
	}
	/**
	 * @param securedService the securedService to set
	 */
	public void setSecuredService(boolean securedService) {
		this.securedService = securedService;
	}
	/**
	 * @return the maxConnections
	 */
	public int getMaxConnections() {
		return maxConnections;
	}
	/**
	 * @param maxConnections the maxConnections to set
	 */
	public void setMaxConnections(int maxConnections) {
		this.maxConnections = maxConnections;
	}
	/**
	 * @return the loggerUtility
	 */
	public LoggerUtility getLoggerUtility() {
		return loggerUtility;
	}
	/**
	 * @param loggerUtility the loggerUtility to set
	 */
	public void setLoggerUtility(LoggerUtility loggerUtility) {
		this.loggerUtility = loggerUtility;
	}
	/**
	 * @return the serviceLocator
	 */
	public Map<String, String> getServiceLocator() {
		return serviceLocator;
	}
	/**
	 * @param serviceLocator the serviceLocator to set
	 */
	public void setServiceLocator(Map<String, String> serviceLocator) {
		this.serviceLocator = serviceLocator;
	}
	/**
	 * @return the serviceProxyPortMap
	 */
	public Map<String, String> getServiceProxyPortMap() {
		return serviceProxyPortMap;
	}
	/**
	 * @param serviceProxyPortMap the serviceProxyPortMap to set
	 */
	public void setServiceProxyPortMap(Map<String, String> serviceProxyPortMap) {
		this.serviceProxyPortMap = serviceProxyPortMap;
	}
	
}
