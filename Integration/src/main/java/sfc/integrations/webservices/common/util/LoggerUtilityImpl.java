package sfc.integrations.webservices.common.util;

import sfc.integrations.webservices.common.conf.BaseWebServiceConfiguration;
import atg.nucleus.GenericService;

import com.thoughtworks.xstream.XStream;

public class LoggerUtilityImpl extends GenericService implements LoggerUtility {

	private BaseWebServiceConfiguration webServiceConfiguration;
	private XStream xstream;
	// logMessage method with only Message as parameter
	@Override
	public void logMessage(Object logMessage) {
		this.logMessage(null, logMessage, this.getWebServiceConfiguration().isLoggingDebug());
	}
	
	@Override
	public void logMessage(Object logMessage, boolean flag) {
		this.logMessage(null, logMessage, flag);
	}
	
	// logMessage method with ServiceName and OperationName as parameter
	@Override
	public void logMessage(String messagePrefix, Object logMessage) {
		this.logMessage(messagePrefix, logMessage, null);
	}
	
	@Override
	public void logMessage(String messagePrefix, Object logMessage, boolean flag) {
		this.logMessage(messagePrefix, logMessage, null, flag);
	}
	
	// logMessage method with ServiceName and OperationName as parameter
	@Override
	public void logMessage(String messagePrefix, Object logMessage, Throwable exception) {
		this.logMessage(messagePrefix, logMessage, exception, this.getWebServiceConfiguration().isLoggingDebug());
	}
	@Override
	public void logMessage(String messagePrefix, Object logMessage, Throwable exception, boolean flag) {
		String message = null;
		if(exception != null ){
			message = messagePrefix + " " + logMessage +" "+exception.getMessage();	
		}else{
			message = messagePrefix + " " + logMessage;	
		}
			
		if (flag) {			
			logDebug(message);
		}
	}

		// logError method with only Message as parameter
		@Override
		public void logError(Object logMessage) {
			this.logMessage(null, logMessage, this.getWebServiceConfiguration().isLoggingDebug());
		}
		
		@Override
		public void logError(Object logMessage, boolean flag) {
			this.logMessage(null, logMessage, flag);
		}
		
		// logMessage method with ServiceName and OperationName as parameter
		@Override
		public void logError(String messagePrefix, Object logMessage) {
			this.logMessage(messagePrefix, logMessage, null);
		}
		
		@Override
		public void logError(String messagePrefix, Object logMessage, boolean flag) {
			this.logMessage(messagePrefix, logMessage, null, flag);
		}
		
		// logMessage method with ServiceName and OperationName as parameter
		@Override
		public void logError(String messagePrefix, Object logMessage, Throwable exception) {
			this.logMessage(messagePrefix, logMessage, exception, this.getWebServiceConfiguration().isLoggingError());
		}
		@Override
		public void logError(String messagePrefix, Object logMessage, Throwable exception, boolean flag) {
			String message = null;
			if(exception != null ){
				message = messagePrefix + " " + logMessage +" "+exception.getMessage();	
			}else{
				message = messagePrefix + " " + logMessage;	
			}
				
			if (flag) {			
				logError(message);
			}
		}

	/**
	 * @return the webServiceConfiguration
	 */
	public BaseWebServiceConfiguration getWebServiceConfiguration() {
		return webServiceConfiguration;
	}

	/**
	 * @param webServiceConfiguration
	 *            the webServiceConfiguration to set
	 */
	public void setWebServiceConfiguration(
			BaseWebServiceConfiguration webServiceConfiguration) {
		this.webServiceConfiguration = webServiceConfiguration;
	}

	/**
	 * @return the xstream
	 */
	public XStream getXstream() {
		if (xstream == null) {
			xstream = new XStream();
		}
		return xstream;
	}

	/**
	 * @param xstream
	 *            the xstream to set
	 */
	public void setXstream(XStream xstream) {
		this.xstream = xstream;
	}

	@Override
	public void logRequestResponse(Object request) {
		this.logRequestResponse(null, request);

	}

	@Override
	public void logRequestResponse(String message, Object request) {
		if (this.getWebServiceConfiguration().isLoggingDebug()) {
			String xmlMessage = this.getXstream().toXML(request);
			logDebug(message+" : " + xmlMessage);
		}

	}
}
