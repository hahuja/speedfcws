package sfc.integrations.webservices.common.services;

import sfc.integrations.webservices.common.exception.HebBusinessException;

import com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.Language;
import com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.PatientEducationLanguageEntitlement;
import com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.PatientEducationType;
import com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.ProductSearchResult;
import com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.ProductSearchType;
import com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.ProductSpecificSearchResult;
import com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.ProductType;
import com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.SectionName;
import com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.SheetTitle;
import com.Goldstandard.GSDI.DataContract._2011._3._1.DrugNameSearchResult;
import com.Goldstandard.GSDI.DataContract._2011._3._1.TrademarkGenericEnum;

public interface GSDIPatientEducationService extends GenericWebService {

	public static String SERVICE_NAME="GSDIPatientEducationService";
	public PatientEducationType[] getSheetContent(String authenticationKey, int languageId, ProductSearchType productSearchType, String searchTerm, SectionName sectionEnum) throws HebBusinessException;
    public PatientEducationLanguageEntitlement[] listLanguageEntitlements(String authenticationKey) throws HebBusinessException;
    public SheetTitle[] listUpdatedPES(String authenticationKey, Language language, java.util.Calendar beginDate, java.util.Calendar endDate) throws HebBusinessException;
    public DrugNameSearchResult searchForDrugNames(String authenticationKey, String searchTerm, TrademarkGenericEnum trademarkOrGeneric) throws HebBusinessException;
    public ProductSearchResult searchForRepresentativeMarketedProductIdPES(String authenticationKey, String searchTerm) throws HebBusinessException;
    public ProductSpecificSearchResult searchForSpecificDrugPES(String authenticationKey, ProductType productSearchType, String searchTerm) throws HebBusinessException;
}
