package sfc.integrations.webservices.common.services;

import sfc.integrations.webservices.common.exception.HebBusinessException;

import com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.ConsumerInteractionReport;
import com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.SeverityLabelFilter;
import com.Goldstandard.GSDI.DataContract.ConsumerDrugInteraction._2011._3._1.SeverityRankingDescription;
import com.Goldstandard.GSDI.DataContract._2011._3._1.DrugNameSearchResult;
import com.Goldstandard.GSDI.DataContract._2011._3._1.PackageIdentifierType;
import com.Goldstandard.GSDI.DataContract._2011._3._1.PrescribedType;
import com.Goldstandard.GSDI.DataContract._2011._3._1.ProductIdentifierType;
import com.Goldstandard.GSDI.DataContract._2011._3._1.ProductSearchResult;
import com.Goldstandard.GSDI.DataContract._2011._3._1.TrademarkGenericEnum;

public class GSDIConsDrugInteractionServiceImpl extends GenericWebServiceImpl implements
		GSDIConsDrugInteractionService {

	@Override
	public Object callService(Object serviceName, Object operationName,
			Object[] requestParams) throws HebBusinessException {
		return super.callService(serviceName, operationName, requestParams);
	}

	@Override
	public DrugNameSearchResult searchForDrugNames(String authenticationKey,
			String searchTerm, TrademarkGenericEnum trademarkOrGeneric)
			throws HebBusinessException {
		String operationName = "searchForDrugNames";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{authenticationKey, searchTerm, trademarkOrGeneric });
		if(reply instanceof DrugNameSearchResult){
			return (DrugNameSearchResult) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+DrugNameSearchResult.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public ProductSearchResult searchForRepresentativeMarketedProductId(
			String authenticationKey, String searchTerm) throws HebBusinessException {
		String operationName = "searchForRepresentativeMarketedProductId";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{authenticationKey, searchTerm});
		if(reply instanceof ProductSearchResult){
			return (ProductSearchResult) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+ProductSearchResult.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public SeverityRankingDescription[] getConsumerSeverityRankingDescriptions(
			String authenticationKey) throws HebBusinessException {
		String operationName = "getConsumerSeverityRankingDescriptions";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{authenticationKey});
		if(reply instanceof SeverityRankingDescription[]){
			return (SeverityRankingDescription[]) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+SeverityRankingDescription[].class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public ConsumerInteractionReport getConsumerInteractions(
			String authenticationKey, ProductIdentifierType[] products,
			PackageIdentifierType[] packages, int[] marketedProductId,
			int[] representativeMarketedProductId,
			SeverityLabelFilter severityLabel, boolean includeCaffeine,
			boolean includeEnteral, boolean includeEthanol,
			boolean includeFood, boolean includeGrapefruit,
			boolean includeTobacco) throws HebBusinessException {
		String operationName = "getConsumerInteractions";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{authenticationKey, 
				products, packages, marketedProductId, 
				representativeMarketedProductId, severityLabel, 
				includeCaffeine, includeEnteral, includeEthanol, includeFood, includeGrapefruit, includeTobacco});
		if(reply instanceof ConsumerInteractionReport){
			return (ConsumerInteractionReport) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+ConsumerInteractionReport.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public ConsumerInteractionReport getConsumerInteractionsV2(
			String authenticationKey, PrescribedType prescribed,
			PrescribedType prescribing, SeverityLabelFilter severityLabel,
			boolean includeCaffeine, boolean includeEnteral,
			boolean includeEthanol, boolean includeFood,
			boolean includeGrapefruit, boolean includeTobacco,
			boolean ignorePrescribed) throws HebBusinessException {
		String operationName = "getConsumerInteractions";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{authenticationKey, 
				prescribed, prescribing, severityLabel, severityLabel, 
				includeCaffeine, includeEnteral, includeEthanol, includeFood, includeGrapefruit, includeTobacco, ignorePrescribed});
		if(reply instanceof ConsumerInteractionReport){
			return (ConsumerInteractionReport) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+ConsumerInteractionReport.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

}
