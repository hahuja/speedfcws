package sfc.integrations.webservices.common.services;

import sfc.integrations.webservices.common.exception.HebBusinessException;

import com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugColor;
import com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugIdentificationResult;
import com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugScoring;
import com.Goldstandard.GSDI.DataContract.DrugIdentifier._2011._3._1.DrugShape;
import com.Goldstandard.GSDI.DataContract._2011._3._1.DrugNameSearchResult;

public class GSDIDrugIdentifierServiceImpl extends GenericWebServiceImpl implements GSDIDrugIdentifierService {

	@Override
	public Object callService(Object serviceName, Object operationName,
			Object[] requestParams) throws HebBusinessException {
		return super.callService(serviceName, operationName, requestParams);
	}

	@Override
	public DrugColor[] listColors(String authenticationKey)
			throws HebBusinessException {
		String operationName = "listColors";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{authenticationKey});
		if(reply instanceof DrugColor[]){
			return (DrugColor[]) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+DrugColor[].class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public DrugShape[] listShapes(String authenticationKey)
			throws HebBusinessException {
		String operationName = "listShapes";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{authenticationKey});
		if(reply instanceof DrugShape[]){
			return (DrugShape[]) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+DrugShape[].class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public DrugScoring[] listScorings(String authenticationKey)
			throws HebBusinessException {
		String operationName = "listScorings";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{authenticationKey});
		if(reply instanceof DrugScoring[]){
			return (DrugScoring[]) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+DrugShape[].class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public DrugIdentificationResult[] searchDrugIdentifications(
			String authenticationKey, int colorId, int shapeId,
			String markingSide1, String markingSide2, int scoringId,
			boolean includePrivateLabels, boolean excludeTablets,
			boolean excludeCapsules, boolean includeRepackaged)
			throws HebBusinessException {
		String operationName = "searchDrugIdentifications";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{authenticationKey, colorId, shapeId, markingSide1,
				markingSide2, scoringId, includePrivateLabels, excludeTablets, excludeCapsules, includeRepackaged});
		if(reply instanceof DrugIdentificationResult[]){
			return (DrugIdentificationResult[]) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+DrugIdentificationResult[].class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

}
