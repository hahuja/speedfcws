package sfc.integrations.webservices.common.util;

import com.sfc.ws.test.suite.config.TestConfigLoader;

import atg.nucleus.GenericService;
import atg.nucleus.Nucleus;

public class HebServiceConstants extends GenericService {

	public Object getComponentInstance(String componentPath){
		Object componentObject = null;
		
		if(Nucleus.getGlobalNucleus() != null){
			componentObject = Nucleus.getGlobalNucleus().resolveName(componentPath);
		}else{
			componentObject = TestConfigLoader.resolveComponent(componentPath);
		}		
		return componentObject;
	}
}
