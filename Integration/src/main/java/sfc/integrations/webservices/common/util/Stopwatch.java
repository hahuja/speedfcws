package sfc.integrations.webservices.common.util;


/**
 * This class is used to monitor stopwatch functionality. to monitor a particular operation we can use this 
 * @author Himkar
 *
 */
public class Stopwatch {
	private long _start = 0, _end = 0;
	/**
	 * Start stopwatch
	 */
	public void start() {
		_start = System.currentTimeMillis();
	}
	/**
	 * Stop stopwatch
	 */
	public void stop() {
		_end = System.currentTimeMillis();
	}
	/**
	 * Retrieve elapsed time in millisecond
	 * @return long value
	 */
	public long getElapsedTimeMillis() {
		return _end - _start;
	}
}