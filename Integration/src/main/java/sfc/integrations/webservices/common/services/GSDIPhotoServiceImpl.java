package sfc.integrations.webservices.common.services;

import sfc.integrations.webservices.common.exception.HebBusinessException;

import com.Goldstandard.GSDI.DataContract._2011._3._1.ProductImageResult;
import com.Goldstandard.GSDI.DataContract._2011._3._1.ProductSearchType;

public class GSDIPhotoServiceImpl extends GenericWebServiceImpl implements GSDIPhotoService {

	@Override
	public Object callService(Object serviceName, Object operationName,
			Object[] requestParams) throws HebBusinessException {
		return super.callService(serviceName, operationName, requestParams);
	}

	@Override
	public ProductImageResult searchForDrugProductPhotos(
			String authenticationKey, ProductSearchType productSearchType,
			String searchTerm) throws HebBusinessException {
		String operationName = "searchForDrugProductPhotos";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{authenticationKey, productSearchType, searchTerm});
		if(reply instanceof ProductImageResult){
			return (ProductImageResult) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+ProductImageResult.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

}
