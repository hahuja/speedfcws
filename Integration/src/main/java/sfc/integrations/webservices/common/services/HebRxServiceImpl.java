package sfc.integrations.webservices.common.services;

import sfc.integrations.webservices.common.exception.HebBusinessException;

import com.heb.rxevo.ws.AddAllergies;
import com.heb.rxevo.ws.AddAllergiesResponse;
import com.heb.rxevo.ws.AddInsurance;
import com.heb.rxevo.ws.AddInsuranceResponse;
import com.heb.rxevo.ws.CheckRxStatus;
import com.heb.rxevo.ws.CheckRxStatusResponse;
import com.heb.rxevo.ws.ConfirmProfile;
import com.heb.rxevo.ws.ConfirmProfileResponse;
import com.heb.rxevo.ws.ContactDr;
import com.heb.rxevo.ws.ContactDrResponse;
import com.heb.rxevo.ws.EnableRefillExpressForPatient;
import com.heb.rxevo.ws.EnableRefillExpressForPatientResponse;
import com.heb.rxevo.ws.EnableRefillExpressForRx;
import com.heb.rxevo.ws.EnableRefillExpressForRxResponse;
import com.heb.rxevo.ws.GetAllergies;
import com.heb.rxevo.ws.GetAllergiesResponse;
import com.heb.rxevo.ws.GetInsurances;
import com.heb.rxevo.ws.GetInsurancesResponse;
import com.heb.rxevo.ws.GetPatientSummaries;
import com.heb.rxevo.ws.GetPatientSummariesResponse;
import com.heb.rxevo.ws.GetPatientSummary;
import com.heb.rxevo.ws.GetPatientSummaryResponse;
import com.heb.rxevo.ws.GetPrescribers;
import com.heb.rxevo.ws.GetPrescribersResponse;
import com.heb.rxevo.ws.GetPrescriptionHistory;
import com.heb.rxevo.ws.GetPrescriptionHistoryResponse;
import com.heb.rxevo.ws.GetRevoPatient;
import com.heb.rxevo.ws.GetRevoPatientResponse;
import com.heb.rxevo.ws.GetRevoPharmacy;
import com.heb.rxevo.ws.GetRevoPharmacyResponse;
import com.heb.rxevo.ws.LinkProfile;
import com.heb.rxevo.ws.LinkProfileResponse;
import com.heb.rxevo.ws.RefillRx;
import com.heb.rxevo.ws.RefillRxResponse;
import com.heb.rxevo.ws.RemoveAllergies;
import com.heb.rxevo.ws.RemoveAllergiesResponse;
import com.heb.rxevo.ws.RemoveInsurance;
import com.heb.rxevo.ws.RemoveInsuranceResponse;
import com.heb.rxevo.ws.SubmitNewPrescription;
import com.heb.rxevo.ws.SubmitNewPrescriptionResponse;
import com.heb.rxevo.ws.SubmitOrder;
import com.heb.rxevo.ws.SubmitOrderResponse;
import com.heb.rxevo.ws.SubmitTransferRequest;
import com.heb.rxevo.ws.SubmitTransferRequestResponse;
import com.heb.rxevo.ws.UpdateNotificationPreferences;
import com.heb.rxevo.ws.UpdateNotificationPreferencesResponse;
import com.heb.rxevo.ws.UpdatePatientEmailAddress;
import com.heb.rxevo.ws.UpdatePatientEmailAddressResponse;
import com.heb.rxevo.ws.UpdatePatientMobileNumber;
import com.heb.rxevo.ws.UpdatePatientMobileNumberResponse;
import com.heb.rxevo.ws.UpdatePatientPhoneNumber;
import com.heb.rxevo.ws.UpdatePatientPhoneNumberResponse;
import com.heb.rxevo.ws.UpdatePatientRecord;
import com.heb.rxevo.ws.UpdatePatientRecordResponse;
import com.heb.rxevo.ws.UpdatePatientRelationship;
import com.heb.rxevo.ws.UpdatePatientRelationshipResponse;
import com.heb.rxevo.ws.UpdateRefillExpressForPatient;
import com.heb.rxevo.ws.UpdateRefillExpressForPatientResponse;
import com.heb.rxevo.ws.UpdateRefillExpressForRx;
import com.heb.rxevo.ws.UpdateRefillExpressForRxResponse;
import com.heb.rxevo.ws.ValidatePatient;
import com.heb.rxevo.ws.ValidatePatientByGlobalFillId;
import com.heb.rxevo.ws.ValidatePatientByGlobalFillIdResponse;
import com.heb.rxevo.ws.ValidatePatientResponse;
import com.tibco.www.namespaces.bc._2002._04.partyinfo_xsd.PartyInfo;

public class HebRxServiceImpl extends GenericWebServiceImpl implements HebRxService {

	@Override
	public Object callService(Object serviceName, Object operationName,
			Object[] requestParams) throws HebBusinessException {
		return super.callService(serviceName, operationName, requestParams);
	}

	@Override
	public GetInsurancesResponse getInsurances(
			GetInsurances body__HebRxServiceSoapBinding, PartyInfo partyInfo)
			throws HebBusinessException {
		String operationName = "getInsurances";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{body__HebRxServiceSoapBinding, partyInfo});
		if(reply instanceof GetInsurancesResponse){
			return (GetInsurancesResponse) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+GetInsurancesResponse.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public AddAllergiesResponse addAllergies(
			AddAllergies body__HebRxServiceSoapBinding, PartyInfo partyInfo)
			throws HebBusinessException {
		String operationName = "addAllergies";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{body__HebRxServiceSoapBinding, partyInfo});
		if(reply instanceof AddAllergiesResponse){
			return (AddAllergiesResponse) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+AddAllergiesResponse.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public GetPrescribersResponse getPrescribers(
			GetPrescribers body__HebRxServiceSoapBinding, PartyInfo partyInfo)
			throws HebBusinessException {
		String operationName = "getPrescribers";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{body__HebRxServiceSoapBinding, partyInfo});
		if(reply instanceof GetPrescribersResponse){
			return (GetPrescribersResponse) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+GetPrescribersResponse.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public GetPrescriptionHistoryResponse getPrescriptionHistory(
			GetPrescriptionHistory body__HebRxServiceSoapBinding,
			PartyInfo partyInfo) throws HebBusinessException {
		String operationName = "getPrescriptionHistory";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{body__HebRxServiceSoapBinding, partyInfo});
		if(reply instanceof GetPrescriptionHistoryResponse){
			return (GetPrescriptionHistoryResponse) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+GetPrescriptionHistoryResponse.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public UpdatePatientPhoneNumberResponse updatePatientPhoneNumber(
			UpdatePatientPhoneNumber body__HebRxServiceSoapBinding,
			PartyInfo partyInfo) throws HebBusinessException {
		String operationName = "updatePatientPhoneNumber";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{body__HebRxServiceSoapBinding, partyInfo});
		if(reply instanceof UpdatePatientPhoneNumberResponse){
			return (UpdatePatientPhoneNumberResponse) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+UpdatePatientPhoneNumberResponse.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public GetAllergiesResponse getAllergies(
			GetAllergies body__HebRxServiceSoapBinding, PartyInfo partyInfo)
			throws HebBusinessException {
		String operationName = "getAllergies";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{body__HebRxServiceSoapBinding, partyInfo});
		if(reply instanceof GetAllergiesResponse){
			return (GetAllergiesResponse) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+GetAllergiesResponse.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public AddInsuranceResponse addInsurance(
			AddInsurance body__HebRxServiceSoapBinding, PartyInfo partyInfo)
			throws HebBusinessException {
		String operationName = "addInsurance";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{body__HebRxServiceSoapBinding, partyInfo});
		if(reply instanceof AddInsuranceResponse){
			return (AddInsuranceResponse) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+AddInsuranceResponse.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public UpdatePatientMobileNumberResponse updatePatientMobileNumber(
			UpdatePatientMobileNumber body__HebRxServiceSoapBinding,
			PartyInfo partyInfo) throws HebBusinessException {
		String operationName = "updatePatientMobileNumber";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{body__HebRxServiceSoapBinding, partyInfo});
		if(reply instanceof UpdatePatientMobileNumberResponse){
			return (UpdatePatientMobileNumberResponse) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+UpdatePatientMobileNumberResponse.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public SubmitNewPrescriptionResponse submitNewPrescription(
			SubmitNewPrescription body__HebRxServiceSoapBinding,
			PartyInfo partyInfo) throws HebBusinessException {
		String operationName = "submitNewPrescription";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{body__HebRxServiceSoapBinding, partyInfo});
		if(reply instanceof SubmitNewPrescriptionResponse){
			return (SubmitNewPrescriptionResponse) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+SubmitNewPrescriptionResponse.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public SubmitTransferRequestResponse submitTransferRequest(
			SubmitTransferRequest body__HebRxServiceSoapBinding,
			PartyInfo partyInfo) throws HebBusinessException {
		String operationName = "submitTransferRequest";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{body__HebRxServiceSoapBinding, partyInfo});
		if(reply instanceof SubmitTransferRequestResponse){
			return (SubmitTransferRequestResponse) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+SubmitTransferRequestResponse.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public UpdateRefillExpressForRxResponse updateRefillExpressForRx(
			UpdateRefillExpressForRx body__HebRxServiceSoapBinding,
			PartyInfo partyInfo) throws HebBusinessException {
		String operationName = "updateRefillExpressForRx";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{body__HebRxServiceSoapBinding, partyInfo});
		if(reply instanceof UpdateRefillExpressForRxResponse){
			return (UpdateRefillExpressForRxResponse) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+UpdateRefillExpressForRxResponse.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public EnableRefillExpressForPatientResponse enableRefillExpressForPatient(
			EnableRefillExpressForPatient body__HebRxServiceSoapBinding,
			PartyInfo partyInfo) throws HebBusinessException {
		String operationName = "enableRefillExpressForPatient";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{body__HebRxServiceSoapBinding, partyInfo});
		if(reply instanceof EnableRefillExpressForPatientResponse){
			return (EnableRefillExpressForPatientResponse) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+EnableRefillExpressForPatientResponse.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public CheckRxStatusResponse checkRxStatus(
			CheckRxStatus body__HebRxServiceSoapBinding, PartyInfo partyInfo)
			throws HebBusinessException {
		String operationName = "checkRxStatus";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{body__HebRxServiceSoapBinding, partyInfo});
		if(reply instanceof CheckRxStatusResponse){
			return (CheckRxStatusResponse) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+CheckRxStatusResponse.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public UpdateNotificationPreferencesResponse updateNotificationPreferences(
			UpdateNotificationPreferences body__HebRxServiceSoapBinding,
			PartyInfo partyInfo) throws HebBusinessException {
		String operationName = "updateNotificationPreferences";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{body__HebRxServiceSoapBinding, partyInfo});
		if(reply instanceof UpdateNotificationPreferencesResponse){
			return (UpdateNotificationPreferencesResponse) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+UpdateNotificationPreferencesResponse.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public UpdateRefillExpressForPatientResponse updateRefillExpressForPatient(
			UpdateRefillExpressForPatient body__HebRxServiceSoapBinding,
			PartyInfo partyInfo) throws HebBusinessException {
		String operationName = "updateRefillExpressForPatient";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{body__HebRxServiceSoapBinding, partyInfo});
		if(reply instanceof UpdateRefillExpressForPatientResponse){
			return (UpdateRefillExpressForPatientResponse) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+UpdateRefillExpressForPatientResponse.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public GetRevoPatientResponse getRevoPatient(
			GetRevoPatient body__HebRxServiceSoapBinding, PartyInfo partyInfo)
			throws HebBusinessException {
		String operationName = "getRevoPatient";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{body__HebRxServiceSoapBinding, partyInfo});
		if(reply instanceof GetRevoPatientResponse){
			return (GetRevoPatientResponse) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+GetRevoPatientResponse.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public ValidatePatientResponse validatePatient(
			ValidatePatient body__HebRxServiceSoapBinding, PartyInfo partyInfo)
			throws HebBusinessException {
		String operationName = "validatePatient";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{body__HebRxServiceSoapBinding, partyInfo});
		if(reply instanceof ValidatePatientResponse){
			return (ValidatePatientResponse) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+ValidatePatientResponse.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public GetPatientSummaryResponse getPatientSummary(
			GetPatientSummary body__HebRxServiceSoapBinding, PartyInfo partyInfo)
			throws HebBusinessException {
		String operationName = "getPatientSummary";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{body__HebRxServiceSoapBinding, partyInfo});
		if(reply instanceof GetPatientSummaryResponse){
			return (GetPatientSummaryResponse) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+GetPatientSummaryResponse.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public RemoveAllergiesResponse removeAllergies(
			RemoveAllergies body__HebRxServiceSoapBinding, PartyInfo partyInfo)
			throws HebBusinessException {
		String operationName = "removeAllergies";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{body__HebRxServiceSoapBinding, partyInfo});
		if(reply instanceof RemoveAllergiesResponse){
			return (RemoveAllergiesResponse) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+RemoveAllergiesResponse.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public GetRevoPharmacyResponse getRevoPharmacy(
			GetRevoPharmacy body__HebRxServiceSoapBinding, PartyInfo partyInfo)
			throws HebBusinessException {
		String operationName = "getRevoPharmacy";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{body__HebRxServiceSoapBinding, partyInfo});
		if(reply instanceof GetRevoPharmacyResponse){
			return (GetRevoPharmacyResponse) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+GetRevoPharmacyResponse.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public ConfirmProfileResponse confirmProfile(
			ConfirmProfile body__HebRxServiceSoapBinding, PartyInfo partyInfo)
			throws HebBusinessException {
		String operationName = "confirmProfile";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{body__HebRxServiceSoapBinding, partyInfo});
		if(reply instanceof ConfirmProfileResponse){
			return (ConfirmProfileResponse) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+ConfirmProfileResponse.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public RemoveInsuranceResponse removeInsurance(
			RemoveInsurance body__HebRxServiceSoapBinding, PartyInfo partyInfo)
			throws HebBusinessException {
		String operationName = "removeInsurance";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{body__HebRxServiceSoapBinding, partyInfo});
		if(reply instanceof RemoveInsuranceResponse){
			return (RemoveInsuranceResponse) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+RemoveInsuranceResponse.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public UpdatePatientEmailAddressResponse updatePatientEmailAddress(
			UpdatePatientEmailAddress body__HebRxServiceSoapBinding,
			PartyInfo partyInfo) throws HebBusinessException {
		String operationName = "updatePatientEmailAddress";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{body__HebRxServiceSoapBinding, partyInfo});
		if(reply instanceof UpdatePatientEmailAddressResponse){
			return (UpdatePatientEmailAddressResponse) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+UpdatePatientEmailAddressResponse.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public EnableRefillExpressForRxResponse enableRefillExpressForRx(
			EnableRefillExpressForRx body__HebRxServiceSoapBinding,
			PartyInfo partyInfo) throws HebBusinessException {
		String operationName = "enableRefillExpressForRx";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{body__HebRxServiceSoapBinding, partyInfo});
		if(reply instanceof EnableRefillExpressForRxResponse){
			return (EnableRefillExpressForRxResponse) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+EnableRefillExpressForRxResponse.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public LinkProfileResponse linkProfile(
			LinkProfile body__HebRxServiceSoapBinding, PartyInfo partyInfo)
			throws HebBusinessException {
		String operationName = "linkProfile";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{body__HebRxServiceSoapBinding, partyInfo});
		if(reply instanceof LinkProfileResponse){
			return (LinkProfileResponse) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+LinkProfileResponse.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public SubmitOrderResponse submitOrder(
			SubmitOrder body__HebRxServiceSoapBinding, PartyInfo partyInfo)
			throws HebBusinessException {
		String operationName = "submitOrder";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{body__HebRxServiceSoapBinding, partyInfo});
		if(reply instanceof SubmitOrderResponse){
			return (SubmitOrderResponse) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+SubmitOrderResponse.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public UpdatePatientRelationshipResponse updatePatientRelationship(
			UpdatePatientRelationship body__HebRxServiceSoapBinding,
			PartyInfo partyInfo) throws HebBusinessException {
		String operationName = "updatePatientRelationship";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{body__HebRxServiceSoapBinding, partyInfo});
		if(reply instanceof UpdatePatientRelationshipResponse){
			return (UpdatePatientRelationshipResponse) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+UpdatePatientRelationshipResponse.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public ValidatePatientByGlobalFillIdResponse validatePatientByGlobalFillId(
			ValidatePatientByGlobalFillId body__HebRxServiceSoapBinding,
			PartyInfo partyInfo) throws HebBusinessException {
		String operationName = "validatePatientByGlobalFillId";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{body__HebRxServiceSoapBinding, partyInfo});
		if(reply instanceof ValidatePatientByGlobalFillIdResponse){
			return (ValidatePatientByGlobalFillIdResponse) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+ValidatePatientByGlobalFillIdResponse.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public UpdatePatientRecordResponse updatePatientRecord(
			UpdatePatientRecord body__HebRxServiceSoapBinding,
			PartyInfo partyInfo) throws HebBusinessException {
		String operationName = "updatePatientRecord";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{body__HebRxServiceSoapBinding, partyInfo});
		if(reply instanceof UpdatePatientRecordResponse){
			return (UpdatePatientRecordResponse) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+UpdatePatientRecordResponse.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public GetPatientSummariesResponse getPatientSummaries(
			GetPatientSummaries body__HebRxServiceSoapBinding,
			PartyInfo partyInfo) throws HebBusinessException {
		String operationName = "getPatientSummaries";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{body__HebRxServiceSoapBinding, partyInfo});
		if(reply instanceof GetPatientSummariesResponse){
			return (GetPatientSummariesResponse) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+GetPatientSummariesResponse.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public ContactDrResponse contactDr(ContactDr body__HebRxServiceSoapBinding,
			PartyInfo partyInfo) throws HebBusinessException {
		String operationName = "contactDr";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{body__HebRxServiceSoapBinding, partyInfo});
		if(reply instanceof ContactDrResponse){
			return (ContactDrResponse) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+ContactDrResponse.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public RefillRxResponse refillRx(RefillRx body__HebRxServiceSoapBinding,
			PartyInfo partyInfo) throws HebBusinessException {
		String operationName = "refillRx";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{body__HebRxServiceSoapBinding, partyInfo});
		if(reply instanceof RefillRxResponse){
			return (RefillRxResponse) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+RefillRxResponse.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

}
