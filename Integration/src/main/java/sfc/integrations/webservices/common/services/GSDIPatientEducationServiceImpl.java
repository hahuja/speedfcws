package sfc.integrations.webservices.common.services;

import java.util.Calendar;

import sfc.integrations.webservices.common.exception.HebBusinessException;

import com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.Language;
import com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.PatientEducationLanguageEntitlement;
import com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.PatientEducationType;
import com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.ProductSearchResult;
import com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.ProductSearchType;
import com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.ProductSpecificSearchResult;
import com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.ProductType;
import com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.SectionName;
import com.Goldstandard.GSDI.DataContract.PatientEducation._2011._3._1.SheetTitle;
import com.Goldstandard.GSDI.DataContract._2011._3._1.DrugNameSearchResult;
import com.Goldstandard.GSDI.DataContract._2011._3._1.TrademarkGenericEnum;

public class GSDIPatientEducationServiceImpl extends GenericWebServiceImpl implements
		GSDIPatientEducationService {

	@Override
	public Object callService(Object serviceName, Object operationName,
			Object[] requestParams) throws HebBusinessException {
		return super.callService(serviceName, operationName, requestParams);
	}

	@Override
	public PatientEducationType[] getSheetContent(String authenticationKey,
			int languageId, ProductSearchType productSearchType,
			String searchTerm, SectionName sectionEnum) throws HebBusinessException {
		String operationName = "getSheetContent";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{authenticationKey, languageId, productSearchType, searchTerm, sectionEnum});
		if(reply instanceof PatientEducationType[]){
			return (PatientEducationType[]) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+PatientEducationType[].class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public PatientEducationLanguageEntitlement[] listLanguageEntitlements(
			String authenticationKey) throws HebBusinessException {
		String operationName = "listLanguageEntitlements";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{authenticationKey});
		if(reply instanceof PatientEducationLanguageEntitlement[]){
			return (PatientEducationLanguageEntitlement[]) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+PatientEducationLanguageEntitlement[].class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public SheetTitle[] listUpdatedPES(String authenticationKey,
			Language language, Calendar beginDate, Calendar endDate)
			throws HebBusinessException {
		String operationName = "listUpdatedPES";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{authenticationKey, language, beginDate, endDate});
		if(reply instanceof SheetTitle[]){
			return (SheetTitle[]) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+SheetTitle[].class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public DrugNameSearchResult searchForDrugNames(String authenticationKey,
			String searchTerm, TrademarkGenericEnum trademarkOrGeneric)
			throws HebBusinessException {
		String operationName = "searchForDrugNames";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{authenticationKey, searchTerm, trademarkOrGeneric});
		if(reply instanceof DrugNameSearchResult){
			return (DrugNameSearchResult) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+DrugNameSearchResult.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public ProductSearchResult searchForRepresentativeMarketedProductIdPES(
			String authenticationKey, String searchTerm) throws HebBusinessException {
		String operationName = "searchForDrugNames";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{authenticationKey, searchTerm});
		if(reply instanceof ProductSearchResult){
			return (ProductSearchResult) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+ProductSearchResult.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

	@Override
	public ProductSpecificSearchResult searchForSpecificDrugPES(
			String authenticationKey, ProductType productSearchType,
			String searchTerm) throws HebBusinessException {
		String operationName = "searchForSpecificDrugPES";
		Object reply = this.callService(SERVICE_NAME, operationName, new Object[]{authenticationKey, productSearchType, searchTerm});
		if(reply instanceof ProductSpecificSearchResult){
			return (ProductSpecificSearchResult) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "+ProductSpecificSearchResult.class.getName()+" Found "+reply.getClass().getName(),"10023");
	}

}
