package sfc.integrations.webservices.common.services;

import org.tempuri.ScriptSaveServices.Processor.GetClosestPharmaciesResponseGetClosestPharmaciesResult;
import org.tempuri.ScriptSaveServices.Processor.GetDrugLabelNamesResponseGetDrugLabelNamesResult;
import org.tempuri.ScriptSaveServices.Processor.GetPharmacyResponseGetPharmacyResult;

import sfc.integrations.webservices.common.exception.HebBusinessException;

public class ScriptSaveProcessorServiceImpl extends GenericWebServiceImpl
		implements ScriptSaveProcessorService {

	@Override
	public Object callService(Object serviceName, Object operationName,
			Object[] requestParams) throws HebBusinessException {
		return super.callService(serviceName, operationName, requestParams);
	}

	@Override
	public GetClosestPharmaciesResponseGetClosestPharmaciesResult getClosestPharmacies(
			String groupId, String address, String city, String state,
			String zip, double latitude, double longitude, int numberToReturn)
			throws HebBusinessException {
		String operationName = "getClosestPharmacies";
		Object reply = this.callService(SERVICE_NAME, operationName,
				new Object[] { groupId, address, city, state, zip, latitude,
						longitude, numberToReturn });
		if (reply instanceof GetClosestPharmaciesResponseGetClosestPharmaciesResult) {
			return (GetClosestPharmaciesResponseGetClosestPharmaciesResult) reply;
		}
		throw new HebBusinessException(
				"Invalid Response object. axpected : "
						+ GetClosestPharmaciesResponseGetClosestPharmaciesResult.class.getName()
						+ " Found " + reply.getClass().getName(), "10023");
	}

	@Override
	public GetPharmacyResponseGetPharmacyResult getPharmacy(String groupId,
			String NCPDP_NPI) throws HebBusinessException {
		String operationName = "getPharmacy";
		Object reply = this.callService(SERVICE_NAME, operationName,
				new Object[] { groupId, NCPDP_NPI });
		if (reply instanceof GetPharmacyResponseGetPharmacyResult) {
			return (GetPharmacyResponseGetPharmacyResult) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "
				+ GetPharmacyResponseGetPharmacyResult.class.getName()
				+ " Found " + reply.getClass().getName(), "10023");
	}

	@Override
	public String[] getDrugBasicNames(String groupId, String search)
			throws HebBusinessException {
		String operationName = "getDrugBasicNames";
		Object reply = this.callService(SERVICE_NAME, operationName,
				new Object[] { groupId, search });
		if (reply instanceof String[]) {
			return (String[]) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "
				+ String[].class.getName() + " Found "
				+ reply.getClass().getName(), "10023");
	}

	@Override
	public GetDrugLabelNamesResponseGetDrugLabelNamesResult getDrugLabelNames(
			String groupId, String basicName) throws HebBusinessException {
		String operationName = "getDrugLabelNames";
		Object reply = this.callService(SERVICE_NAME, operationName,
				new Object[] { groupId, basicName });
		if (reply instanceof GetDrugLabelNamesResponseGetDrugLabelNamesResult) {
			return (GetDrugLabelNamesResponseGetDrugLabelNamesResult) reply;
		}
		throw new HebBusinessException(
				"Invalid Response object. axpected : "
						+ GetDrugLabelNamesResponseGetDrugLabelNamesResult.class.getName()
						+ " Found " + reply.getClass().getName(), "10023");
	}

	@Override
	public String[] getDrugPrice(String groupId, String NCPDP_NPI,
			String labelName, String NDC11, double quantity, double unC,
			double daysSupply) throws HebBusinessException {
		String operationName = "getDrugPrice";
		Object reply = this.callService(SERVICE_NAME, operationName,
				new Object[] { groupId, labelName, NDC11, quantity, unC,
						daysSupply });
		if (reply instanceof String[]) {
			return (String[]) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "
				+ String[].class.getName() + " Found "
				+ reply.getClass().getName(), "10023");
	}

	@Override
	public String[] enrollMember(String groupId, String cardholderId,
			String firstName, String middleInit, String lastName, String DOB,
			String relationshipCode, String gender, String effectiveDate,
			String terminationDate, boolean signature1, boolean signature2,
			String storeNumber, String address1, String address2, String city,
			String state, String zip, String phone, String email,
			String externalId) throws HebBusinessException {
		String operationName = "enrollMember";
		Object reply = this.callService(SERVICE_NAME, operationName,
				new Object[] { groupId, cardholderId, firstName, middleInit,
						lastName, DOB, relationshipCode, gender, effectiveDate,
						terminationDate, signature1, signature2, storeNumber,
						address1, address2, city, state, zip, phone, email,
						externalId });
		if (reply instanceof String[]) {
			return (String[]) reply;
		}
		throw new HebBusinessException("Invalid Response object. axpected : "
				+ String[].class.getName() + " Found "
				+ reply.getClass().getName(), "10023");
	}

}
