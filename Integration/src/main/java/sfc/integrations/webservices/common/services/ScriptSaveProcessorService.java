package sfc.integrations.webservices.common.services;

import org.tempuri.ScriptSaveServices.Processor.GetClosestPharmaciesResponseGetClosestPharmaciesResult;
import org.tempuri.ScriptSaveServices.Processor.GetDrugLabelNamesResponseGetDrugLabelNamesResult;
import org.tempuri.ScriptSaveServices.Processor.GetPharmacyResponseGetPharmacyResult;

import sfc.integrations.webservices.common.exception.HebBusinessException;


public interface ScriptSaveProcessorService extends GenericWebService {

	public static String SERVICE_NAME="ScriptSaveProcessorService";
	public GetClosestPharmaciesResponseGetClosestPharmaciesResult getClosestPharmacies(String groupId, String address, String city, String state, String zip, double latitude, double longitude, int numberToReturn) throws HebBusinessException;
    public GetPharmacyResponseGetPharmacyResult getPharmacy(String groupId, String NCPDP_NPI) throws HebBusinessException;
    public String[] getDrugBasicNames(String groupId, String search) throws HebBusinessException;
    public GetDrugLabelNamesResponseGetDrugLabelNamesResult getDrugLabelNames(String groupId, String basicName) throws HebBusinessException;
    public String[] getDrugPrice(String groupId, String NCPDP_NPI, String labelName, String NDC11, double quantity, double unC, double daysSupply) throws HebBusinessException;
    public String[] enrollMember(String groupId, String cardholderId, String firstName, String middleInit, String lastName, String DOB, String relationshipCode, String gender, String effectiveDate, String terminationDate, boolean signature1, boolean signature2, String storeNumber, String address1, String address2, String city, String state, String zip, String phone, String email, String externalId) throws HebBusinessException;
}
