package sfc.integrations.webservices.common.services;

import sfc.integrations.webservices.common.exception.HebBusinessException;

import com.Goldstandard.GSDI.DataContract._2011._3._1.ProductImageResult;
import com.Goldstandard.GSDI.DataContract._2011._3._1.ProductSearchType;

public interface GSDIPhotoService extends GenericWebService {

	public static String SERVICE_NAME="GSDIPhotoService";
	public ProductImageResult searchForDrugProductPhotos(String authenticationKey, ProductSearchType productSearchType, String searchTerm) throws HebBusinessException;
}
