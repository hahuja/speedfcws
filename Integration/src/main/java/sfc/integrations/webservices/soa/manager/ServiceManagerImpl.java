package sfc.integrations.webservices.soa.manager;

import atg.nucleus.GenericService;



/**
 * This class is responsible to return the specific pharmacy service for the operations specified
 */
public class ServiceManagerImpl extends GenericService implements ServiceManager {

	private PharmacyServiceManager pharmacyServiceManager;

	/**
	 * @return the pharmacyServiceManager
	 */
	public PharmacyServiceManager getPharmacyServiceManager() {
		return pharmacyServiceManager;
	}

	/**
	 * @param pharmacyServiceManager the pharmacyServiceManager to set
	 */
	public void setPharmacyServiceManager(
			PharmacyServiceManager pharmacyServiceManager) {
		this.pharmacyServiceManager = pharmacyServiceManager;
	}
	
}
