package sfc.integrations.webservices.soa.manager;

import sfc.integrations.webservices.common.services.CustomerAccountService;
import sfc.integrations.webservices.common.services.GSDIConsDrugInteractionService;
import sfc.integrations.webservices.common.services.GSDIDrugIdentifierService;
import sfc.integrations.webservices.common.services.GSDIPatientEducationService;
import sfc.integrations.webservices.common.services.GSDIPhotoService;
import sfc.integrations.webservices.common.services.HebRxService;
import sfc.integrations.webservices.common.services.ProductionSubscriptionCenterService;
import sfc.integrations.webservices.common.services.ScriptSaveProcessorService;


public interface PharmacyServiceManager {
	
	public CustomerAccountService getCustomerAccountService();
	public GSDIConsDrugInteractionService getGsdiConsDrugInteractionService();
	public GSDIDrugIdentifierService getGsdiDrugIdentifierService();
	public GSDIPatientEducationService getGsdiPatientEducationService();
	public GSDIPhotoService getGsdiPhotoService();
	public HebRxService getHebRxService();
	public ProductionSubscriptionCenterService getProductionSubscriptionCenterService();
	public ScriptSaveProcessorService getScriptSaveProcessorService();
}
