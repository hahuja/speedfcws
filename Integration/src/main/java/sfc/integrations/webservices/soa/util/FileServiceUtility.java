package sfc.integrations.webservices.soa.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;

import sfc.integrations.webservices.common.conf.BaseWebServiceConfiguration;
import sfc.integrations.webservices.common.conf.PharmacyWebServiceConfiguration;
import sfc.integrations.webservices.common.exception.HebBusinessException;
import sfc.integrations.webservices.common.util.RandomStringGenerator;

import com.sfc.ws.test.suite.config.TestConfigLoader;
import com.sfc.ws.test.suite.config.TestConfiguration;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

/**
 * This class implementation is used to XML call web service stub for respective
 * operation. here we we use WebServiceConfiguration object to hold all the
 * configurations related to the service. Using java reflection API this class
 * perform business logic
 * 
 * @author Himkar
 * 
 */
public class FileServiceUtility extends WebServiceUtilityImpl {

	private PharmacyWebServiceConfiguration webServiceConfiguration;
	private HashMap<String,String> serviceMap;
		
	/**
	 * This method is used to call web service stub for given service name and operation 
	 * @param serviceName - service name
	 * @param operationName - operation name
	 * @param searviceRequest - request parameters
	 * @return - service response 
	 * @throws HebBusinessException - exception if there are any
	 */
	@Override
	public Object callWebService(Object serviceName, Object operationName, Object[] serviceRequest) throws HebBusinessException {
		
		final String perfMonitorOpName = serviceName+"-"+operationName+"- callWebService";
		final String key = RandomStringGenerator.generateString();
		String logMessagePrelude = perfMonitorOpName + " " + key;
		this.getLoggerUtility().logMessage(logMessagePrelude, ": Start");
		Object serviceResponse = null;			
		if(this.isConfigurationSucceeded()){
			this.getLoggerUtility().logMessage(logMessagePrelude, ": Configuration succeeded");
			this.getLoggerUtility().logRequestResponse(logMessagePrelude, serviceRequest);
			String fileName=serviceName+"_"+operationName+"_Response";			
			this.getLoggerUtility().logMessage(logMessagePrelude, ": Got fileName"+fileName);			
			TestConfiguration testConfiguration = (TestConfiguration)TestConfigLoader.resolveComponent("/com/sfc/ws/test/suite/config/TestConfiguration");		
			String filePath=testConfiguration.createResponseXMLPath(String.valueOf(serviceName), String.valueOf(operationName));
			this.getLoggerUtility().logMessage(logMessagePrelude, ": Got path of the response xml"+filePath);
			String classFullPackage = getServiceMap().get(operationName);
			this.getLoggerUtility().logMessage(logMessagePrelude, ": Got class of the response"+classFullPackage);
			
			serviceResponse=createTestCaseResponse(filePath, classFullPackage);
		    this.getLoggerUtility().logMessage(logMessagePrelude, ": Got  object of the response"+serviceResponse);
		    this.getLoggerUtility().logRequestResponse(logMessagePrelude, serviceResponse);	
		    this.getLoggerUtility().logMessage(logMessagePrelude, ": operation executed");
		} 		
		this.getLoggerUtility().logMessage(logMessagePrelude, ": End");
		return serviceResponse;		
	}
		
	/**
	 * This method is used to create java request object from given xml path
	 * 
	 * @param requestFilePath - xml file path which need to be mapped with request class object
	 * @param requestClass - fully qualified class name
	 * @return  Object - request object
	 * @throws SpeedFCServiceBusinessException
	 */
	public Object createTestCaseResponse(String responseFilePath, String responseClass) throws HebBusinessException {
		final String perfMonitorOpName = "FileServiceUtility - createTestCaseResponse";
		final String key = RandomStringGenerator.generateString();
		String logMessagePrelude = perfMonitorOpName + " " + key;
		
		this.getLoggerUtility().logMessage(logMessagePrelude, ": Start");
		
		/**
		 * Create parser to convert xml file to java object
		 */
		XStream xmlToJavaObjectConverter = new XStream(new DomDriver());
		BufferedReader filePathReader = null;
		Object searviceResponse = null;
	
		try {
			/**
			 * Retrieve Class object from given qualified class name
			 */
			Class responseClassObject = Class.forName(responseClass);
			String requestClassName = responseClass;
			if(responseClass.lastIndexOf(".") > -1){
				requestClassName = responseClass.subSequence(responseClass.lastIndexOf(".")+1, responseClass.length()).toString();
			}
				
			/**
			 * set aliases to map xml attribute with java objects
			 */
			xmlToJavaObjectConverter.alias(requestClassName, responseClassObject);
			xmlToJavaObjectConverter.alias("subscriptionStatusCode", com.heb.xmlns.ei.Salt2.SubscriptionStatusCode.class);
			/**
			 * read request xml file
			 */
			filePathReader = new BufferedReader(new FileReader(responseFilePath));
			this.getLoggerUtility().logMessage(logMessagePrelude, ": response xml file loaded into reader object");			
			
			/**
			 * convert xml file to respective java object
			 */
			searviceResponse = (Object) xmlToJavaObjectConverter.fromXML(filePathReader);
			this.getLoggerUtility().logMessage(logMessagePrelude, ": response object created - "+searviceResponse);
			
		} catch (FileNotFoundException fileNotFoundException) {
			this.getLoggerUtility().logMessage(logMessagePrelude, ": File can't be located at : "+responseFilePath);			
			throw new HebBusinessException(logMessagePrelude+ ": File can't be located at : "+responseFilePath, "10005", fileNotFoundException );
		}catch (ClassNotFoundException classNotFoundException) {
			this.getLoggerUtility().logMessage(logMessagePrelude, ": specified class can't be found : "+responseFilePath);			
			throw new HebBusinessException(logMessagePrelude+": specified class can't be found : "+responseFilePath, "10006", classNotFoundException);
		}finally{
			try{
				/**
				 * close file reader object to clean memory
				 */
				filePathReader.close();
			}catch(Exception exception){
				this.getLoggerUtility().logMessage(logMessagePrelude, ": Error while closing reader");
			}
		}
		this.getLoggerUtility().logMessage(logMessagePrelude, ": End");
		return searviceResponse;
	}
	/**
	 * This method is used to configure required configuration for the service call
	 * @throws HebBusinessException - if there are any exception
	 */
	public void configureService() throws HebBusinessException {
		
		if(this.getWebServiceConfiguration() == null){ 
			throw new HebBusinessException("Service configuration not found...", "10001");
		}else{
			this.getLoggerUtility().setWebServiceConfiguration(webServiceConfiguration);
			this.setServiceRequestProcessor(this.getWebServiceConfiguration().getPrePostRequestProcessor());
			this.setConfigurationSucceeded(true);
		}
		
		
	}
	/**
	 * This method is used to set respective configuration object to the service utility
	 * @param webServiceConfiguration - service configuration object
	 * @throws HebBusinessException - exception if there are any
	 */
	@Override
	public void setServiceConfiguration(
			BaseWebServiceConfiguration webServiceConfiguration) throws HebBusinessException {
		if(webServiceConfiguration instanceof PharmacyWebServiceConfiguration){
			this.setWebServiceConfiguration((PharmacyWebServiceConfiguration)webServiceConfiguration);
			super.setWebServiceConfiguration(webServiceConfiguration);
		}else{
			throw new HebBusinessException("Configurtion not supported...", "10002");
		}
			
	}
	/**
	 * @return the webServiceConfiguration
	 */
	public PharmacyWebServiceConfiguration getWebServiceConfiguration() {
		return webServiceConfiguration;
	}
	/**
	 * @param webServiceConfiguration the webServiceConfiguration to set
	 */
	public void setWebServiceConfiguration(
			PharmacyWebServiceConfiguration webServiceConfiguration) {
		this.webServiceConfiguration = webServiceConfiguration;
	}
	/**
	 * @return the serviceMap
	 */
	public HashMap<String, String> getServiceMap() {
		return serviceMap;
	}
	/**
	 * @param serviceMap the serviceMap to set
	 */
	public void setServiceMap(HashMap<String, String> serviceMap) {
		this.serviceMap = serviceMap;
	}
}

