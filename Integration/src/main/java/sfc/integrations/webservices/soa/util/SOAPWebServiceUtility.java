package sfc.integrations.webservices.soa.util;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.rmi.RemoteException;

import javax.xml.rpc.Service;

import org.apache.axis.AxisFault;

import sfc.integrations.webservices.common.conf.BaseWebServiceConfiguration;
import sfc.integrations.webservices.common.conf.PharmacyWebServiceConfiguration;
import sfc.integrations.webservices.common.exception.HebBusinessException;
import sfc.integrations.webservices.common.util.RandomStringGenerator;
import sfc.integrations.webservices.common.util.Stopwatch;
import atg.core.util.StringUtils;
import atg.service.perfmonitor.PerfStackMismatchException;
import atg.service.perfmonitor.PerformanceMonitor;

import com.heb.xmlns.ei.ProviderFaultSchema.ProviderSOAPFault;

/**
 * This class implementation is used to call SOAP web service stub for respective
 * operation. here we we use WebServiceConfiguration object to hold all the
 * configurations related to the service. Using java reflection API this class
 * perform business logic
 * 
 * @author Himkar
 * 
 */
public class SOAPWebServiceUtility extends WebServiceUtilityImpl {

	/**
	 * This variable is used to hold webServiceConfiguration
	 */
	private PharmacyWebServiceConfiguration webServiceConfiguration;

	/**
	 * This method is used to call web service stub for given service name and operation 
	 * @param serviceName - service name
	 * @param operationName - operation name
	 * @param searviceRequest - request parameters
	 * @return - service response 
	 * @throws HebBusinessException - exception if there are any
	 */
	@Override
	public Object callWebService(Object serviceName, Object operationName,
			Object[] serviceRequest) throws HebBusinessException {
		Object serviceResponse = null;
		final String perfMonitorOpName = serviceName + "-" + operationName
				+ "- callWebService";
		final String key = RandomStringGenerator.generateString();
		String logMessagePrelude = perfMonitorOpName + " " + key;

		Stopwatch stopwatch = new Stopwatch();

		this.getLoggerUtility().logMessage(logMessagePrelude, ": Start");

		String serviceBindingStubClassName = this.getWebServiceConfiguration()
				.getServiceStubClassName(
						this.getWebServiceConfiguration().getServiceType());
		Class serviceBindingStubClass = null;
		Object serviceBindingStubObject = null;
		Method operationObject = null;
		boolean returnVoid = false;
		Class[] paramArgs = {};
		boolean perfOpCancelled = false;

		try {
			if (PerformanceMonitor.isEnabled()) {
				this.getLoggerUtility().logMessage(logMessagePrelude,
						": PerformanceMonitor Started");
				PerformanceMonitor.startOperation(perfMonitorOpName);
			}
			if (this.isConfigurationSucceeded()) {
				stopwatch.start();
				this.getLoggerUtility().logMessage(logMessagePrelude,
						": Configuration succeeded");
				serviceBindingStubClass = Class
						.forName(serviceBindingStubClassName); 
				this.getLoggerUtility().logMessage(logMessagePrelude,
						": Got web service class");

				serviceBindingStubObject = this.serviceBindingStubObject(
						serviceName, operationName, serviceBindingStubClass);
				if (serviceBindingStubObject != null) {
					this.getLoggerUtility().logMessage(
							logMessagePrelude,
							": Got web service class instance"
									+ serviceBindingStubObject);
					this.getServiceRequestProcessor().preRequestProcessor(
							serviceRequest);
					try {
						if (serviceRequest != null) {
							paramArgs = new Class[serviceRequest.length];
							for (int counter = 0; counter < serviceRequest.length; counter++) {
								paramArgs[counter] = serviceRequest[counter]
										.getClass();
							}
							this.getLoggerUtility()
									.logMessage(logMessagePrelude,
											": retrieving parameterizeed operation from service");
							operationObject = serviceBindingStubClass
									.getDeclaredMethod(
											String.valueOf(operationName),
											paramArgs);
							if (operationObject != null) {
								this.getLoggerUtility().logMessage(
										logMessagePrelude,
										": Got operation "
												+ operationObject
														.getReturnType()
														.getName() + " "
												+ operationObject.getName());
								serviceResponse = operationObject.invoke(
										serviceBindingStubObject,
										serviceRequest);
								this.getLoggerUtility().logMessage(
										logMessagePrelude,
										": Got response from service "
												+ serviceResponse);
							}
						} else {
							this.getLoggerUtility()
									.logMessage(logMessagePrelude,
											": retrieving non parameterizeed operation from service");
							operationObject = serviceBindingStubClass
									.getDeclaredMethod(
											String.valueOf(operationName),
											paramArgs);
							if (operationObject != null) {
								this.getLoggerUtility().logMessage(
										logMessagePrelude,
										": Got operation "
												+ operationObject
														.getReturnType()
														.getName() + " "
												+ operationObject.getName());
								serviceResponse = operationObject.invoke(
										serviceBindingStubObject, null);
								this.getLoggerUtility().logMessage(
										logMessagePrelude,
										": Got response from service "
												+ serviceResponse);
							}
						}
					} catch (InvocationTargetException invocationTargetException) {
						logMessagePrelude = logMessagePrelude
								+ " - InvocationTargetException ";
						this.getLoggerUtility().logMessage(
								logMessagePrelude,
								": here is exception : "
										+ invocationTargetException.toString(),
								invocationTargetException);
						if (invocationTargetException.getTargetException() != null) {
							throw invocationTargetException
									.getTargetException();
						} else {
							throw invocationTargetException;
						}

					}
					this.getServiceRequestProcessor().postRequestProcessor(
							serviceResponse);
				}
				this.getLoggerUtility().logMessage(logMessagePrelude,
						": operation executed");
			}
		} catch (ClassNotFoundException classNotFoundException) {
			if (PerformanceMonitor.isEnabled())
				PerformanceMonitor.cancelOperation(perfMonitorOpName);
			perfOpCancelled = true;
			logMessagePrelude = logMessagePrelude
					+ " - classNotFoundException ";
			this.getLoggerUtility().logError(logMessagePrelude,
					": here is exception : ", classNotFoundException);
			throw new HebBusinessException(logMessagePrelude
					+ " Some thing went wrong, Exception is : "
					+ classNotFoundException.toString(), "10008",
					classNotFoundException);
		} catch (NoSuchMethodException noSuchMethodException) {
			if (PerformanceMonitor.isEnabled())
				PerformanceMonitor.cancelOperation(perfMonitorOpName);
			perfOpCancelled = true;
			logMessagePrelude = logMessagePrelude + " - noSuchMethodException ";
			this.getLoggerUtility().logError(logMessagePrelude,
					": here is exception : ", noSuchMethodException);
			throw new HebBusinessException(logMessagePrelude
					+ " Some thing went wrong, Exception is : "
					+ noSuchMethodException.toString(), "10007",
					noSuchMethodException);
		} catch (SecurityException securityException) {
			if (PerformanceMonitor.isEnabled())
				PerformanceMonitor.cancelOperation(perfMonitorOpName);
			perfOpCancelled = true;
			logMessagePrelude = logMessagePrelude + " - securityException ";
			this.getLoggerUtility().logError(logMessagePrelude,
					": here is exception : ", securityException);
			throw new HebBusinessException(logMessagePrelude
					+ " Some thing went wrong, Exception is : "
					+ securityException.toString(), "10021", securityException);
		} catch (InstantiationException instantiationException) {
			if (PerformanceMonitor.isEnabled())
				PerformanceMonitor.cancelOperation(perfMonitorOpName);
			perfOpCancelled = true;
			logMessagePrelude = logMessagePrelude
					+ " - instantiationException ";
			this.getLoggerUtility().logError(logMessagePrelude,
					": here is exception : ", instantiationException);
			throw new HebBusinessException(logMessagePrelude
					+ " Some thing went wrong, Exception is : "
					+ instantiationException.toString(), "10011",
					instantiationException);
		} catch (IllegalAccessException illegalAccessException) {
			if (PerformanceMonitor.isEnabled())
				PerformanceMonitor.cancelOperation(perfMonitorOpName);
			perfOpCancelled = true;
			logMessagePrelude = logMessagePrelude
					+ " - illegalAccessException ";
			this.getLoggerUtility().logError(logMessagePrelude,
					": here is exception : ", illegalAccessException);
			throw new HebBusinessException(logMessagePrelude
					+ " Some thing went wrong, Exception is : "
					+ illegalAccessException.toString(), "10010",
					illegalAccessException);
		} catch (IllegalArgumentException illegalArgumentException) {
			if (PerformanceMonitor.isEnabled())
				PerformanceMonitor.cancelOperation(perfMonitorOpName);
			perfOpCancelled = true;
			logMessagePrelude = logMessagePrelude
					+ " - illegalArgumentException ";
			this.getLoggerUtility().logError(logMessagePrelude,
					": here is exception : ", illegalArgumentException);
			throw new HebBusinessException(logMessagePrelude
					+ " Some thing went wrong, Exception is : "
					+ illegalArgumentException.toString(), "10020",
					illegalArgumentException);
		} catch (InvocationTargetException invocationTargetException) {
			if (PerformanceMonitor.isEnabled())
				PerformanceMonitor.cancelOperation(perfMonitorOpName);
			perfOpCancelled = true;
			logMessagePrelude = logMessagePrelude
					+ " - invocationTargetException ";
			this.getLoggerUtility().logError(logMessagePrelude,
					": here is exception : ", invocationTargetException);
			throw new HebBusinessException(logMessagePrelude
					+ " Some thing went wrong, Exception is : "
					+ invocationTargetException.toString(), "10009",
					invocationTargetException);
		} catch (ProviderSOAPFault providerSOAPFault) {
			if (PerformanceMonitor.isEnabled())
				PerformanceMonitor.cancelOperation(perfMonitorOpName);
			perfOpCancelled = true;
			logMessagePrelude = logMessagePrelude + " - providerSOAPFault ";
			this.getLoggerUtility().logError(logMessagePrelude,
					": here is exception : ", providerSOAPFault);
			throw new HebBusinessException(logMessagePrelude
					+ " Some thing went wrong, Exception is : "
					+ providerSOAPFault.toString(), "10019", providerSOAPFault);
		} catch (AxisFault axisFault) {
			if (PerformanceMonitor.isEnabled())
				PerformanceMonitor.cancelOperation(perfMonitorOpName);
			perfOpCancelled = true;
			logMessagePrelude = logMessagePrelude + " - axisFault ";
			this.getLoggerUtility().logError(logMessagePrelude,
					": here is exception : ", axisFault);
			throw new HebBusinessException(logMessagePrelude
					+ " Some thing went wrong, Exception is : "
					+ axisFault.toString(), "10018", axisFault);
		} catch (RemoteException remoteException) {
			if (PerformanceMonitor.isEnabled())
				PerformanceMonitor.cancelOperation(perfMonitorOpName);
			perfOpCancelled = true;
			logMessagePrelude = logMessagePrelude + " - remoteException ";
			this.getLoggerUtility().logError(logMessagePrelude,
					": here is exception : ", remoteException);
			throw new HebBusinessException(logMessagePrelude
					+ " Some thing went wrong, Exception is : "
					+ remoteException.toString(), "10017", remoteException);
		} catch (Exception exception) {
			if (PerformanceMonitor.isEnabled())
				PerformanceMonitor.cancelOperation(perfMonitorOpName);
			perfOpCancelled = true;
			logMessagePrelude = logMessagePrelude + " - exception ";
			this.getLoggerUtility().logError(logMessagePrelude,
					": here is exception : ", exception);
			throw new HebBusinessException(logMessagePrelude
					+ " Some thing went wrong, Exception is : "
					+ exception.toString(), "10012", exception);
		} catch (Throwable throwable) {
			if (PerformanceMonitor.isEnabled())
				PerformanceMonitor.cancelOperation(perfMonitorOpName);
			perfOpCancelled = true;
			logMessagePrelude = logMessagePrelude + " - throwable ";
			this.getLoggerUtility().logError(logMessagePrelude,
					": here is exception : ", throwable);
			throw new HebBusinessException(logMessagePrelude
					+ " Some thing went wrong, Exception is : "
					+ throwable.toString(), "10012", throwable);
		} finally {
			try {
				stopwatch.stop();
				if (!perfOpCancelled) {
					if (PerformanceMonitor.isEnabled()) {
						PerformanceMonitor.endOperation(perfMonitorOpName);
					}
				}
			} catch (PerfStackMismatchException e) {
				if (isLoggingWarning()) {
					logWarning(e);
				}
			} catch (Exception exception) {
				logMessagePrelude = logMessagePrelude + " - exception ";
				this.getLoggerUtility().logMessage(logMessagePrelude,
						": here is exception : ", exception);
				throw new HebBusinessException("Service : "
						+ serviceBindingStubClassName
						+ " operation can't be performed...", "10012",
						exception);
			}
		}

		this.getLoggerUtility().logMessage(logMessagePrelude,
				" TimeInMilliSeconds: " + stopwatch.getElapsedTimeMillis());
		this.getLoggerUtility().logMessage(logMessagePrelude,
				": End : " + serviceResponse);
		return serviceResponse;
	}

	/**
	 * This method is use to retrieve binding stub class instance and set all the required configurations like end point, time out and service port etc.
	 * @param serviceName - service name
	 * @param operationName - operation name
	 * @param serviceBindingStubClass - class object of binding stub class
	 * @return - binding stub object with all the configuration
	 * @throws HebBusinessException - if there are any
	 * @throws NoSuchMethodException - if there are any
	 * @throws SecurityException - if there are any
	 * @throws InstantiationException - if there are any
	 * @throws IllegalAccessException - if there are any
	 * @throws IllegalArgumentException - if there are any
	 * @throws InvocationTargetException - if there are any
	 */
	protected Object serviceBindingStubObject(Object serviceName,
			Object operationName, Class serviceBindingStubClass)
			throws HebBusinessException, NoSuchMethodException,
			SecurityException, InstantiationException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException {
		final String perfMonitorOpName = serviceName + "-" + operationName
				+ "- serviceLocatorObject";
		final String key = RandomStringGenerator.generateString();
		String logMessagePrelude = perfMonitorOpName + " " + key;

		this.getLoggerUtility().logMessage(logMessagePrelude, ": Start");
		Object serviceLocatorObject = null;
		String endPontURL = null;
		String servicePort = null;
		String userName = null;
		String password = null;
		if (this.isConfigurationSucceeded() && serviceBindingStubClass != null) {
			endPontURL = this.getWebServiceConfiguration()
					.getServiceEndPontURL(
							this.getWebServiceConfiguration().getServiceType());
			servicePort = this.getWebServiceConfiguration().getServicePORT(
					this.getWebServiceConfiguration().getServiceType());
			this.getLoggerUtility().logMessage(
					logMessagePrelude,
					": Retrieved end point url : " + endPontURL + " : "
							+ servicePort);

			if (!StringUtils.isBlank(endPontURL)) {
				URL endpoint;
				try {
					endpoint = new URL(endPontURL);
				} catch (java.net.MalformedURLException malformedURLException) {
					throw new HebBusinessException("Service name : "
							+ serviceName + " has wrong end point url : "
							+ endPontURL, "10016", malformedURLException);
				}
				this.getLoggerUtility().logMessage(logMessagePrelude,
						": creating instance via perameterized constructor..");
				Constructor constructor = serviceBindingStubClass
						.getConstructor(new Class[] { endpoint.getClass(),
								Service.class });
				serviceLocatorObject = constructor.newInstance(new Object[] {
						endpoint, null });// creates an instance of the class
											// found.
			} else {
				this.getLoggerUtility()
						.logMessage(logMessagePrelude,
								": creating instance via non-perameterized constructor..");
				serviceLocatorObject = serviceBindingStubClass.newInstance();
			}

			if (serviceLocatorObject != null
					&& !StringUtils.isBlank(servicePort)) {
				org.apache.axis.client.Stub stub = (org.apache.axis.client.Stub) serviceLocatorObject;
				stub.setPortName(servicePort);

				this.getLoggerUtility().logMessage(logMessagePrelude,
						": Processing Authentication and utility setup");

				userName = this.getWebServiceConfiguration()
						.getServiceUserName(
								this.getWebServiceConfiguration()
										.getServiceType());
				password = this.getWebServiceConfiguration()
						.getServicePassword(
								this.getWebServiceConfiguration()
										.getServiceType());

				if (!StringUtils.isBlank(userName)
						&& !StringUtils.isBlank(password)) {
					this.getLoggerUtility().logMessage(logMessagePrelude,
							": seting user cradantials for service call");
					stub.setUsername(userName);
					stub.setPassword(password);
				}
				// serviceProxy =
				// this.getWebServiceConfiguration().getServiceProxy(this.getWebServiceConfiguration().getServiceType());
				// serviceProxyPort =
				// this.getWebServiceConfiguration().getServicePORT(this.getWebServiceConfiguration().getServiceType());

				if (this.getWebServiceConfiguration().getServiceTimeOut() > 0) {
					this.getLoggerUtility().logMessage(logMessagePrelude,
							": seting timeout for service call");
					stub.setTimeout(this.getWebServiceConfiguration()
							.getServiceTimeOut());
				}

			}
			this.getLoggerUtility().logMessage(logMessagePrelude,
					": all required setup have been performed");
		}
		this.getLoggerUtility().logMessage(logMessagePrelude,
				": End : " + serviceLocatorObject);
		return serviceLocatorObject;
	}

	/**
	 * This method is used to configure required configuration for the service call
	 * @throws HebBusinessException - if there are any exception
	 */
	@Override
	public void configureService() throws HebBusinessException {
		super.configureService();

	}
	/**
	 * This method is used to set respective configuration object to the service utility
	 * @param webServiceConfiguration - service configuration object
	 * @throws HebBusinessException - exception if there are any
	 */
	@Override
	public void setServiceConfiguration(
			BaseWebServiceConfiguration webServiceConfiguration)
			throws HebBusinessException {
		if (webServiceConfiguration instanceof PharmacyWebServiceConfiguration) {
			this.setWebServiceConfiguration((PharmacyWebServiceConfiguration) webServiceConfiguration);
			super.setWebServiceConfiguration(webServiceConfiguration);
		} else {
			throw new HebBusinessException("Configurtion not supported...",
					"10002");
		}

	}

	/**
	 * @return the webServiceConfiguration
	 */
	public PharmacyWebServiceConfiguration getWebServiceConfiguration() {
		return webServiceConfiguration;
	}

	/**
	 * @param webServiceConfiguration
	 *            the webServiceConfiguration to set
	 */
	public void setWebServiceConfiguration(
			PharmacyWebServiceConfiguration webServiceConfiguration) {
		this.webServiceConfiguration = webServiceConfiguration;
	}

}
