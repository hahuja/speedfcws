package sfc.integrations.webservices.soa.util;

import sfc.integrations.procesor.ServiceRequestProcessor;
import sfc.integrations.webservices.common.conf.BaseWebServiceConfiguration;
import sfc.integrations.webservices.common.exception.HebBusinessException;
import sfc.integrations.webservices.common.util.LoggerUtility;
import sfc.integrations.webservices.common.util.RandomStringGenerator;
import atg.nucleus.GenericService;

/**
 * This class implementation is used to call web service stub for respective
 * operation. here we we use WebServiceConfiguration object to hold all the
 * configurations related to the service. Using java reflection API this class
 * perform business logic
 * 
 * @author Himkar
 * 
 */

public class WebServiceUtilityImpl extends GenericService implements WebServiceUtility {
	
	private boolean logRequest;
	private boolean	logResponse;
	private boolean configurationSucceeded;
	private BaseWebServiceConfiguration webServiceConfiguration;
	private ServiceRequestProcessor serviceRequestProcessor;
	
	private LoggerUtility loggerUtility;
	
	/**
	 * This method is used to call web service stub for given service name and operation 
	 * @param serviceName - service name
	 * @param operationName - operation name
	 * @param searviceRequest - request parameters
	 * @return - service response 
	 * @throws HebBusinessException - exception if there are any
	 */
	@Override
	public Object callWebService(Object serviceName, Object operationName, Object[] serviceRequest)
			throws HebBusinessException {
		
		final String perfMonitorOpName = serviceName+"-"+operationName+"- callWebService";
		final String key = RandomStringGenerator.generateString();
		String logMessagePrelude = perfMonitorOpName + " " + key;
		
		this.getLoggerUtility().logMessage(logMessagePrelude, ": Start");
		
		return null;
	}
	
	/**
	 * This method is used to configure required configuration for the service call
	 * @throws HebBusinessException - if there are any exception
	 */
	public void configureService()
			throws HebBusinessException{
		
		if(this.getWebServiceConfiguration() == null){ 
			throw new HebBusinessException("Service configuration not found...", "10001");
		}else{			
			this.getLoggerUtility().setWebServiceConfiguration(webServiceConfiguration);
			this.setServiceRequestProcessor(this.getWebServiceConfiguration().getPrePostRequestProcessor());
			this.setConfigurationSucceeded(true);			
		}
		
		
	}	
	
	/**
	 * This method is used to set respective configuration object to the service utility
	 * @param webServiceConfiguration - service configuration object
	 * @throws HebBusinessException - exception if there are any
	 */
	@Override
	public void setServiceConfiguration(
			BaseWebServiceConfiguration webServiceConfiguration)throws HebBusinessException {
		
		this.setWebServiceConfiguration(webServiceConfiguration);
	}
	
	/**
	 * @return the logRequest
	 */
	public boolean isLogRequest() {
		return logRequest;
	}

	/**
	 * @param logRequest the logRequest to set
	 */
	public void setLogRequest(boolean logRequest) {
		this.logRequest = logRequest;
	}

	/**
	 * @return the logResponse
	 */
	public boolean isLogResponse() {
		return logResponse;
	}

	/**
	 * @param logResponse the logResponse to set
	 */
	public void setLogResponse(boolean logResponse) {
		this.logResponse = logResponse;
	}

	/**
	 * @return the webServiceConfiguration
	 */
	public BaseWebServiceConfiguration getWebServiceConfiguration() {
		return webServiceConfiguration;
	}

	/**
	 * @param webServiceConfiguration the webServiceConfiguration to set
	 */
	public void setWebServiceConfiguration(
			BaseWebServiceConfiguration webServiceConfiguration) {
		this.webServiceConfiguration = webServiceConfiguration;
	}


	/**
	 * @return the serviceRequestProcessor
	 */
	public ServiceRequestProcessor getServiceRequestProcessor() {
		return serviceRequestProcessor;
	}


	/**
	 * @param serviceRequestProcessor the serviceRequestProcessor to set
	 */
	public void setServiceRequestProcessor(
			ServiceRequestProcessor serviceRequestProcessor) {
		this.serviceRequestProcessor = serviceRequestProcessor;
	}


	/**
	 * @return the configurationSucceeded
	 */
	public boolean isConfigurationSucceeded() {
		return configurationSucceeded;
	}


	/**
	 * @param configurationSucceeded the configurationSucceeded to set
	 */
	public void setConfigurationSucceeded(boolean configurationSucceeded) {
		this.configurationSucceeded = configurationSucceeded;
	}

	/**
	 * @return the loggerUtility
	 */
	public LoggerUtility getLoggerUtility() {
		return loggerUtility;
	}


	/**
	 * @param loggerUtility the loggerUtility to set
	 */
	public void setLoggerUtility(LoggerUtility loggerUtility) {
		this.loggerUtility = loggerUtility;
	}	
}
