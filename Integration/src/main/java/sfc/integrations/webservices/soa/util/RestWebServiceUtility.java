package sfc.integrations.webservices.soa.util;

import sfc.integrations.webservices.common.conf.BaseWebServiceConfiguration;
import sfc.integrations.webservices.common.conf.PharmacyWebServiceConfiguration;
import sfc.integrations.webservices.common.exception.HebBusinessException;

/**
 * This class implementation is used to REST call web service stub for respective
 * operation. here we we use WebServiceConfiguration object to hold all the
 * configurations related to the service. Using java reflection API this class
 * perform business logic
 * 
 * @author Himkar
 * 
 */
public class RestWebServiceUtility extends WebServiceUtilityImpl {

	private PharmacyWebServiceConfiguration webServiceConfiguration;
	
	/**
	 * This method is used to call web service stub for given service name and operation 
	 * @param serviceName - service name
	 * @param operationName - operation name
	 * @param searviceRequest - request parameters
	 * @return - service response 
	 * @throws HebBusinessException - exception if there are any
	 */
	@Override
	public Object callWebService(Object serviceName, Object operationName, Object[] serviceRequest) throws HebBusinessException{
		
		return super.callWebService(serviceName, operationName, serviceRequest);
	}
	
	/**
	 * This method is used to configure required configuration for the service call
	 * @throws HebBusinessException - if there are any exception
	 */
	@Override
	public void configureService()throws HebBusinessException{
		super.configureService();		
	}
	/**
	 * This method is used to set respective configuration object to the service utility
	 * @param webServiceConfiguration - service configuration object
	 * @throws HebBusinessException - exception if there are any
	 */
	@Override
	public void setServiceConfiguration(
			BaseWebServiceConfiguration webServiceConfiguration)throws HebBusinessException {
		if(webServiceConfiguration instanceof PharmacyWebServiceConfiguration){
			this.setWebServiceConfiguration((PharmacyWebServiceConfiguration)webServiceConfiguration);
			super.setWebServiceConfiguration(webServiceConfiguration);
		}else{
			throw new HebBusinessException("Configurtion not supported...", "10002");
		}
			
	}
	/**
	 * @return the webServiceConfiguration
	 */
	public PharmacyWebServiceConfiguration getWebServiceConfiguration() {
		return webServiceConfiguration;
	}
	/**
	 * @param webServiceConfiguration the webServiceConfiguration to set
	 */
	public void setWebServiceConfiguration(
			PharmacyWebServiceConfiguration webServiceConfiguration) {
		this.webServiceConfiguration = webServiceConfiguration;
	}
}
