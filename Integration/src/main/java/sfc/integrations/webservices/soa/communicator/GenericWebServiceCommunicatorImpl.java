package sfc.integrations.webservices.soa.communicator;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import sfc.integrations.webservices.common.conf.BaseWebServiceConfiguration;
import sfc.integrations.webservices.common.exception.HebBusinessException;
import sfc.integrations.webservices.common.util.HebServiceConstants;
import sfc.integrations.webservices.common.util.LoggerUtility;
import sfc.integrations.webservices.common.util.RandomStringGenerator;
import sfc.integrations.webservices.soa.util.WebServiceUtility;
import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
/**
 * The class is responsible to call the actual web service after generating the request. Following are the steps it does for an operation:
 *	a.	Generate the request
 *	b.	Load the configuration from WebServiceConfiguration for a particular service, e.g. timeouts, endPointURL etc.
 *	c.	Call web service utility to call the end point.
 *	d.	Process the response to send it back to the client
 *	e.	Logs the time taken with the request and response and a random number will be appended to both the request and response for a unique identification. 
 *  @author Himkar
 */
public class GenericWebServiceCommunicatorImpl extends GenericService implements GenericWebServiceCommunicator {

	private Map<String, String> serviceUtilityMap;
	private Map<String, String> serviceConfigurationMap;
	private HebServiceConstants serviceConstants;
	private LoggerUtility loggerUtility;
	
	/**
	 * This method is responsible to retrieve value of web service configuration and web service Utility and call WebService
	 * @return Service Response
	 * @throws InstantiationException 
	 * @throws SecurityException 
	 * @throws NoSuchMethodException 
	 * @throws ClassNotFoundException 
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 * @throws IllegalAccessException 
	 */
	@Override
	public Object callServiceCommunicator(Object serviceName, Object operationName, Object[] serviceRequest)
			throws HebBusinessException {
		Object serviceResponse = null;
		final String perfMonitorOpName = serviceName+"-"+operationName+"- callServiceCommunicator";
		final String key = RandomStringGenerator.generateString();
		String logMessagePrelude = perfMonitorOpName + " " + key;
		
		this.getLoggerUtility().logMessage(logMessagePrelude, ": Start", this.isLoggingDebug());
		BaseWebServiceConfiguration webServiceConfiguration = this.getServiceConfiguration(String.valueOf(serviceName));
		this.getLoggerUtility().logMessage(logMessagePrelude, ": Got the web service configuration"+webServiceConfiguration, webServiceConfiguration.isLoggingDebug());
		WebServiceUtility webServiceUtility = this.getServiceUtility(webServiceConfiguration.getServiceType());
		this.getLoggerUtility().logMessage(logMessagePrelude, ": Got the web service utility"+webServiceUtility, webServiceConfiguration.isLoggingDebug());
		
		webServiceUtility.setServiceConfiguration(webServiceConfiguration);
		webServiceUtility.configureService();
		serviceResponse = webServiceUtility.callWebService(serviceName, operationName, serviceRequest);
		this.getLoggerUtility().logMessage(logMessagePrelude, ": operation executed", webServiceConfiguration.isLoggingDebug());
		
		return serviceResponse;
	}
	/**
	 * This method is responsible to retrieve configuration corresponding to the web service name
	 * @param serviceName - Name of the service
	 * @return - service configuration object
	 * @throws HebBusinessException - if there are any exception
	 */
	private BaseWebServiceConfiguration getServiceConfiguration(String serviceName)throws HebBusinessException{
		BaseWebServiceConfiguration webServiceConfiguration = null;
		final String perfMonitorOpName = serviceName+"-"+"- getServiceConfiguration";
		final String key = RandomStringGenerator.generateString();
		String logMessagePrelude = perfMonitorOpName + " " + key;
		
		this.getLoggerUtility().logMessage(logMessagePrelude, ": Start", this.isLoggingDebug());
		
		if(StringUtils.isBlank(serviceName)){
			this.getLoggerUtility().logError(logMessagePrelude, ": Service name can't be null or blank : ", this.isLoggingDebug());
			throw new HebBusinessException("Service name can't be null or blank", "10013");
		}
		if(this.getServiceConfigurationMap() != null){
			if(this.getServiceConfigurationMap().containsKey(serviceName)){
				String configurationPath = this.getServiceConfigurationMap().get(serviceName);
				this.getLoggerUtility().logMessage(logMessagePrelude, ": Got the configuration path"+configurationPath, this.isLoggingDebug());
				
				Object configurationObject = this.getServiceConstants().getComponentInstance(configurationPath)	;
				this.getLoggerUtility().logMessage(logMessagePrelude, ": Got the configuration object"+configurationObject, this.isLoggingDebug());
				
				if(configurationObject != null && configurationObject instanceof BaseWebServiceConfiguration){
					webServiceConfiguration = (BaseWebServiceConfiguration) configurationObject;
					this.getLoggerUtility().logMessage(logMessagePrelude, ": Got the web service configuration"+webServiceConfiguration, this.isLoggingDebug());
				}
			}else{
				this.getLoggerUtility().logError(logMessagePrelude, ": None configuration are registered for this service name : "+serviceName, this.isLoggingDebug());
				throw new HebBusinessException("None Service name are registered in serviceType pool", "10014");
			}
		}else{
			this.getLoggerUtility().logError(logMessagePrelude, ": None configuration are registered in service pool : ", this.isLoggingDebug());
			throw new HebBusinessException("None configuration are registered in serviceType pool", "10014");
		}
		if(webServiceConfiguration == null){
			this.getLoggerUtility().logError(logMessagePrelude, "Service name : "+serviceName+" is not a valid service name", this.isLoggingDebug());
			throw new HebBusinessException("service name : "+serviceName+" is not a valid serviceType name", "10015");
		}
		return webServiceConfiguration;
	}
	/**
	 * This method is responsible to retrieve Service Utility corresponding to Service Type
	 * @param serviceType - service type (MOCK, SOAP or REST)
	 * @return - Utility instance to call service
	 * @throws HebBusinessException - if there are any exception
	 */
	private WebServiceUtility getServiceUtility(String serviceType)throws HebBusinessException{
		WebServiceUtility webService = null;
		final String perfMonitorOpName = serviceType+"-"+"- getServiceUtility";
		final String key = RandomStringGenerator.generateString();
		String logMessagePrelude = perfMonitorOpName + " " + key;
		
		this.getLoggerUtility().logMessage(logMessagePrelude, ": Start", this.isLoggingDebug());
		if(StringUtils.isBlank(serviceType)){
			this.getLoggerUtility().logError(logMessagePrelude, ": Service name can't be null or blank : ", this.isLoggingDebug());
			throw new HebBusinessException("Service type can't be null or blank", "10013");
		}
		if(this.getServiceUtilityMap() != null){
			if(this.getServiceUtilityMap().containsKey(serviceType)){
				String servicePath = this.getServiceUtilityMap().get(serviceType);
				this.getLoggerUtility().logMessage(logMessagePrelude, ": Got the service path"+servicePath, this.isLoggingDebug());
				
				Object serviceObject = this.getServiceConstants().getComponentInstance(servicePath);	
				this.getLoggerUtility().logMessage(logMessagePrelude, ": Got the service object"+serviceObject, this.isLoggingDebug());
				
				if(serviceObject != null && serviceObject instanceof WebServiceUtility){
					webService = (WebServiceUtility) serviceObject;
					this.getLoggerUtility().logMessage(logMessagePrelude, ": Got the web service"+webService, this.isLoggingDebug());
				}
			}else{
				this.getLoggerUtility().logError(logMessagePrelude, ": None configuration are registered for this serviceType name : "+serviceType, this.isLoggingDebug());
				throw new HebBusinessException("None configuration are registered for this serviceType name : "+serviceType, "10014");
			}
		}else{
			this.getLoggerUtility().logError(logMessagePrelude, ": None configuration are registered in serviceType pool : ", this.isLoggingDebug());
			throw new HebBusinessException("None serviceType are registered in serviceType pool", "10014");
		}
		if(webService == null){
			this.getLoggerUtility().logError(logMessagePrelude, "serviceType name : "+serviceType+" is not a valid serviceType name", this.isLoggingDebug());
			throw new HebBusinessException("serviceType name : "+serviceType+" is not a valid serviceType name", "10015");
		}
		this.getLoggerUtility().logMessage(logMessagePrelude, ": End "+webService, this.isLoggingDebug());
		return webService;
	}
	/**
	 * @return the serviceUtilityMap
	 */
	public Map<String, String> getServiceUtilityMap() {
		return serviceUtilityMap;
	}

	/**
	 * @param serviceUtilityMap the serviceUtilityMap to set
	 */
	public void setServiceUtilityMap(
			Map<String, String> serviceUtilityMap) {
		this.serviceUtilityMap = serviceUtilityMap;
	}

	/**
	 * @return the serviceConfigurationMap
	 */
	public Map<String, String> getServiceConfigurationMap() {
		return serviceConfigurationMap;
	}

	/**
	 * @param serviceConfigurationMap the serviceConfigurationMap to set
	 */
	public void setServiceConfigurationMap(
			Map<String, String> serviceConfigurationMap) {
		this.serviceConfigurationMap = serviceConfigurationMap;
	}

	/**
	 * @return the serviceConstants
	 */
	public HebServiceConstants getServiceConstants() {
		return serviceConstants;
	}

	/**
	 * @param serviceConstants the serviceConstants to set
	 */
	public void setServiceConstants(HebServiceConstants serviceConstants) {
		this.serviceConstants = serviceConstants;
	}

	public LoggerUtility getLoggerUtility() {
		return loggerUtility;
	}

	public void setLoggerUtility(LoggerUtility loggerUtility) {
		this.loggerUtility = loggerUtility;
	}
}
