package sfc.integrations.webservices.soa.util;

import sfc.integrations.webservices.common.conf.BaseWebServiceConfiguration;
import sfc.integrations.webservices.common.exception.HebBusinessException;

public interface WebServiceUtility {

	/**
	 * This method is used to call web service stub for given service name and operation 
	 * @param serviceName - service name
	 * @param operationName - operation name
	 * @param searviceRequest - request parameters
	 * @return - service response 
	 * @throws HebBusinessException - exception if there are any
	 */
	public Object callWebService(Object serviceName, Object operationName, Object[] searviceRequest)throws HebBusinessException;
	
	/**
	 * This method is used to configure required configuration for the service call
	 * @throws HebBusinessException - if there are any exception
	 */
	public void configureService()throws HebBusinessException ;
	
	/**
	 * This method is used to set respective configuration object to the service utility
	 * @param webServiceConfiguration - service configuration object
	 * @throws HebBusinessException - exception if there are any
	 */
	public void setServiceConfiguration(BaseWebServiceConfiguration webServiceConfiguration)throws HebBusinessException;
}
