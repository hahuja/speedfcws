package sfc.integrations.webservices.soa.manager;

import sfc.integrations.webservices.common.services.CustomerAccountService;
import sfc.integrations.webservices.common.services.GSDIConsDrugInteractionService;
import sfc.integrations.webservices.common.services.GSDIDrugIdentifierService;
import sfc.integrations.webservices.common.services.GSDIPatientEducationService;
import sfc.integrations.webservices.common.services.GSDIPhotoService;
import sfc.integrations.webservices.common.services.HebRxService;
import sfc.integrations.webservices.common.services.ProductionSubscriptionCenterService;
import sfc.integrations.webservices.common.services.ScriptSaveProcessorService;
import sfc.integrations.webservices.common.util.HebServiceConstants;
import sfc.integrations.webservices.common.util.LoggerUtility;
/**
 * This class is used to retrieve response object from the given service name
 * @author Himkar
 *
 */
public class PharmacyServiceManagerImpl implements	PharmacyServiceManager {

	private CustomerAccountService customerAccountService;
	private GSDIConsDrugInteractionService gsdiConsDrugInteractionService;
	private GSDIDrugIdentifierService gsdiDrugIdentifierService;
	private GSDIPatientEducationService gsdiPatientEducationService;
	private GSDIPhotoService gsdiPhotoService;
	private HebRxService hebRxService;
	private ProductionSubscriptionCenterService productionSubscriptionCenterService;
	private ScriptSaveProcessorService scriptSaveProcessorService;
	
	private HebServiceConstants serviceConstants;
	private LoggerUtility loggerUtility;
	
	
	/**
	 * @return the serviceConstants
	 */
	public HebServiceConstants getServiceConstants() {
		return serviceConstants;
	}

	/**
	 * @param serviceConstants the serviceConstants to set
	 */
	public void setServiceConstants(HebServiceConstants serviceConstants) {
		this.serviceConstants = serviceConstants;
	}

	public LoggerUtility getLoggerUtility() {
		return loggerUtility;
	}

	public void setLoggerUtility(LoggerUtility loggerUtility) {
		this.loggerUtility = loggerUtility;
	}

	/**
	 * @return the customerAccountService
	 */
	public CustomerAccountService getCustomerAccountService() {
		return customerAccountService;
	}

	/**
	 * @param customerAccountService the customerAccountService to set
	 */
	public void setCustomerAccountService(
			CustomerAccountService customerAccountService) {
		this.customerAccountService = customerAccountService;
	}

	/**
	 * @return the gsdiConsDrugInteractionService
	 */
	public GSDIConsDrugInteractionService getGsdiConsDrugInteractionService() {
		return gsdiConsDrugInteractionService;
	}

	/**
	 * @param gsdiConsDrugInteractionService the gsdiConsDrugInteractionService to set
	 */
	public void setGsdiConsDrugInteractionService(
			GSDIConsDrugInteractionService gsdiConsDrugInteractionService) {
		this.gsdiConsDrugInteractionService = gsdiConsDrugInteractionService;
	}

	/**
	 * @return the gsdiDrugIdentifierService
	 */
	public GSDIDrugIdentifierService getGsdiDrugIdentifierService() {
		return gsdiDrugIdentifierService;
	}

	/**
	 * @param gsdiDrugIdentifierService the gsdiDrugIdentifierService to set
	 */
	public void setGsdiDrugIdentifierService(
			GSDIDrugIdentifierService gsdiDrugIdentifierService) {
		this.gsdiDrugIdentifierService = gsdiDrugIdentifierService;
	}

	/**
	 * @return the gsdiPatientEducationService
	 */
	public GSDIPatientEducationService getGsdiPatientEducationService() {
		return gsdiPatientEducationService;
	}

	/**
	 * @param gsdiPatientEducationService the gsdiPatientEducationService to set
	 */
	public void setGsdiPatientEducationService(
			GSDIPatientEducationService gsdiPatientEducationService) {
		this.gsdiPatientEducationService = gsdiPatientEducationService;
	}

	/**
	 * @return the gsdiPhotoService
	 */
	public GSDIPhotoService getGsdiPhotoService() {
		return gsdiPhotoService;
	}

	/**
	 * @param gsdiPhotoService the gsdiPhotoService to set
	 */
	public void setGsdiPhotoService(GSDIPhotoService gsdiPhotoService) {
		this.gsdiPhotoService = gsdiPhotoService;
	}

	/**
	 * @return the hebRxService
	 */
	public HebRxService getHebRxService() {
		return hebRxService;
	}

	/**
	 * @param hebRxService the hebRxService to set
	 */
	public void setHebRxService(HebRxService hebRxService) {
		this.hebRxService = hebRxService;
	}

	/**
	 * @return the productionSubscriptionCenterService
	 */
	public ProductionSubscriptionCenterService getProductionSubscriptionCenterService() {
		return productionSubscriptionCenterService;
	}

	/**
	 * @param productionSubscriptionCenterService the productionSubscriptionCenterService to set
	 */
	public void setProductionSubscriptionCenterService(
			ProductionSubscriptionCenterService productionSubscriptionCenterService) {
		this.productionSubscriptionCenterService = productionSubscriptionCenterService;
	}

	/**
	 * @return the scriptSaveProcessorService
	 */
	public ScriptSaveProcessorService getScriptSaveProcessorService() {
		return scriptSaveProcessorService;
	}

	/**
	 * @param scriptSaveProcessorService the scriptSaveProcessorService to set
	 */
	public void setScriptSaveProcessorService(
			ScriptSaveProcessorService scriptSaveProcessorService) {
		this.scriptSaveProcessorService = scriptSaveProcessorService;
	}

}
