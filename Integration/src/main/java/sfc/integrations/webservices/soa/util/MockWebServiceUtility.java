package sfc.integrations.webservices.soa.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import sfc.integrations.webservices.common.conf.BaseWebServiceConfiguration;
import sfc.integrations.webservices.common.conf.PharmacyWebServiceConfiguration;
import sfc.integrations.webservices.common.exception.HebBusinessException;
import sfc.integrations.webservices.common.util.RandomStringGenerator;

/**
 * This class implementation is used to MOCK call web service stub for respective
 * operation. here we we use WebServiceConfiguration object to hold all the
 * configurations related to the service. Using java reflection API this class
 * perform business logic
 * 
 * @author Himkar
 * 
 */

public class MockWebServiceUtility extends WebServiceUtilityImpl {
	
	private PharmacyWebServiceConfiguration webServiceConfiguration;
	
	/**
	 * This method is used to call web service stub for given service name and operation 
	 * @param serviceName - service name
	 * @param operationName - operation name
	 * @param searviceRequest - request parameters
	 * @return - service response 
	 * @throws HebBusinessException - exception if there are any
	 */
	@Override
	public Object callWebService(Object serviceName, Object operationName, Object[] serviceRequest)
			throws HebBusinessException {
		final String perfMonitorOpName = serviceName+"-"+operationName+"- callWebService";
		final String key = RandomStringGenerator.generateString();
		String logMessagePrelude = perfMonitorOpName + " " + key;
		
		this.getLoggerUtility().logMessage(logMessagePrelude, ": Start");	
		String serviceBindingStubClassName = this.getWebServiceConfiguration().getServiceStubClassName(this.getWebServiceConfiguration().getServiceType());
		Class serviceLocatorClass = null;
		Object serviceLocatorObject = null;
		Object serviceResponse = null;
		Method operationObject = null;
		boolean returnVoid = false;
		Class[] paramArgs = {};
		try{
			if(this.isConfigurationSucceeded()){
				this.getLoggerUtility().logMessage(logMessagePrelude, ": Configuration succeeded");
				serviceLocatorClass = Class.forName(serviceBindingStubClassName); 
				this.getLoggerUtility().logMessage(logMessagePrelude, ": Got web service class"+serviceLocatorClass);				
				serviceLocatorObject = serviceLocatorClass.newInstance();				
				if(serviceLocatorObject != null){
					this.getLoggerUtility().logMessage(logMessagePrelude, ": Got web service class instance"+serviceLocatorObject);
					this.getServiceRequestProcessor().preRequestProcessor(serviceRequest);
					this.getLoggerUtility().logRequestResponse(logMessagePrelude, serviceRequest);					
					if(serviceRequest != null){
						paramArgs = new Class[serviceRequest.length];
						for(int counter = 0; counter < serviceRequest.length; counter++){
							paramArgs[counter]  = serviceRequest[counter].getClass();
						}
						this.getLoggerUtility().logMessage(logMessagePrelude, ": retrieving parameterizeed operation from service");
						operationObject = serviceLocatorClass.getDeclaredMethod(String.valueOf(operationName), paramArgs);							
						if(operationObject != null){
							this.getLoggerUtility().logMessage(logMessagePrelude, ": Got operation "+operationObject.getReturnType().getName()+" "+operationObject.getName());
							serviceResponse = operationObject.invoke(serviceLocatorObject, serviceRequest);
							this.getLoggerUtility().logMessage(logMessagePrelude, ": Got response from service "+serviceResponse);
						}
					}else{
						this.getLoggerUtility().logMessage(logMessagePrelude, ": retrieving non parameterizeed operation from service");
						operationObject = serviceLocatorClass.getDeclaredMethod(String.valueOf(operationName), paramArgs);
						if(operationObject != null){
							this.getLoggerUtility().logMessage(logMessagePrelude, ": Got operation "+operationObject.getReturnType().getName()+" "+operationObject.getName());
							serviceResponse = operationObject.invoke(serviceLocatorObject, null);
							this.getLoggerUtility().logMessage(logMessagePrelude, ": Got response from service "+serviceResponse);
						}
					}
					this.getLoggerUtility().logRequestResponse(logMessagePrelude, serviceResponse);					
					this.getServiceRequestProcessor().postRequestProcessor(serviceResponse);
				}
				this.getLoggerUtility().logMessage(logMessagePrelude, ": operation executed");
			}
		}catch(NoSuchMethodException noSuchMethodException){
			logMessagePrelude = logMessagePrelude +" - noSuchMethodException ";
			this.getLoggerUtility().logMessage(logMessagePrelude, ": here is exception : ", noSuchMethodException);
			throw new HebBusinessException("Operation name : "+operationName+" not found in : "+serviceBindingStubClassName, "10007", noSuchMethodException);
		}catch(ClassNotFoundException classNotFoundException){
			logMessagePrelude = logMessagePrelude +" - classNotFoundException ";
			this.getLoggerUtility().logMessage(logMessagePrelude, ": here is exception : ", classNotFoundException);
			throw new HebBusinessException("Service : "+serviceBindingStubClassName+" class not found in CLASSPATH", "10008", classNotFoundException);
		}catch(InvocationTargetException invocationTargetException){
			logMessagePrelude = logMessagePrelude +" - invocationTargetException ";
			this.getLoggerUtility().logMessage(logMessagePrelude, ": here is exception : ", invocationTargetException);
			throw new HebBusinessException("Operation name : "+operationName+" has issues while access in : "+serviceBindingStubClassName, "10009", invocationTargetException);
		}catch(IllegalAccessException illegalAccessException){
			logMessagePrelude = logMessagePrelude +" - illegalAccessException ";
			this.getLoggerUtility().logMessage(logMessagePrelude, ": here is exception : ", illegalAccessException);
			throw new HebBusinessException("Operation name : "+operationName+" has access restrictions in : "+serviceBindingStubClassName, "10010", illegalAccessException);
		}catch(InstantiationException instantiationException){
			logMessagePrelude = logMessagePrelude +" - instantiationException ";
			this.getLoggerUtility().logMessage(logMessagePrelude, ": here is exception : ", instantiationException);
			throw new HebBusinessException("Service : "+serviceBindingStubClassName+" class can't be Instantiated...", "10011", instantiationException);
		}catch(Exception exception){
			logMessagePrelude = logMessagePrelude +" - exception ";
			this.getLoggerUtility().logMessage(logMessagePrelude, ": here is exception : ", exception);
			throw new HebBusinessException("Service : "+serviceBindingStubClassName+" operation can't be performed...", "10012", exception);
		}
		this.getLoggerUtility().logMessage(logMessagePrelude, ": End "+serviceResponse);	
		return serviceResponse;
	}
	/**
	 * This method is used to configure required configuration for the service call
	 * @throws HebBusinessException - if there are any exception
	 */
	@Override
	public void configureService()
			throws HebBusinessException {
		super.configureService();
		
	}
	/**
	 * This method is used to set respective configuration object to the service utility
	 * @param webServiceConfiguration - service configuration object
	 * @throws HebBusinessException - exception if there are any
	 */
	@Override
	public void setServiceConfiguration(
			BaseWebServiceConfiguration webServiceConfiguration)throws HebBusinessException {
		if(webServiceConfiguration instanceof PharmacyWebServiceConfiguration){
			this.setWebServiceConfiguration((PharmacyWebServiceConfiguration)webServiceConfiguration);
			super.setWebServiceConfiguration(webServiceConfiguration);
		}else{
			throw new HebBusinessException("Configurtion not supported...", "10002");
		}
			
	}
	/**
	 * @return the webServiceConfiguration
	 */
	public PharmacyWebServiceConfiguration getWebServiceConfiguration() {
		return webServiceConfiguration;
	}
	/**
	 * @param webServiceConfiguration the webServiceConfiguration to set
	 */
	public void setWebServiceConfiguration(
			PharmacyWebServiceConfiguration webServiceConfiguration) {
		this.webServiceConfiguration = webServiceConfiguration;
	}
}
