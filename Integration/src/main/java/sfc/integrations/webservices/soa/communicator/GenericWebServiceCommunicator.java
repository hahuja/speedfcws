package sfc.integrations.webservices.soa.communicator;

import sfc.integrations.webservices.common.exception.HebBusinessException;



public interface GenericWebServiceCommunicator {

	public Object callServiceCommunicator(Object serviceName, Object operationName, Object[] requestDTO)throws HebBusinessException;
}
