/**
 * 
 */
package sfc.integrations.procesor;

import sfc.integrations.webservices.common.exception.HebBusinessException;

/**
 * @author dell
 *
 */
public interface ServiceRequestProcessor {
	public Object[] preRequestProcessor(Object[] serviceRequest)throws HebBusinessException;
	public Object postRequestProcessor(Object serviceResponse)throws HebBusinessException;
}
